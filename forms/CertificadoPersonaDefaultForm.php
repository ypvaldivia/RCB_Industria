<?php

namespace app\forms;

use yii;
use yii\base\Model;

class CertificadoPersonaDefaultForm extends Model
{
    public $inspeccion;
    public $persona;
    public $documentacion;
    public $servicio;
    public $expedicion;
    public $vencimiento;
    public $storeEmision;
    public $storeValidez;
    public $funcionario;
    public $control;
    public $nivelAprobacion;

    public function rules()
    {
        return array(
            array('documentacion, servicio, expedicion, vencimiento, funcionario', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'documentacion' => 'Documentaci�n (Regla, Procedimiento, Instrucci�n, Cap�tulo del MAPS u otros)',
            'servicio' => 'Servicio',
            'expedicion' => 'Fecha de Expedici�n',
            'vencimiento' => 'Fecha de Vencimiento',
            'funcionario' => 'Por el RCB',
        );
    }

    public function setMyAttributes($form, $obj)
    {
        $this->documentacion = (isset($form['documentacion'])) ? $form['documentacion'] : null;
        $this->servicio = (isset($form['servicio'])) ? $form['servicio'] : null;
        $this->expedicion = (isset($form['expedicion'])) ? $form['expedicion'] : null;
        $this->vencimiento = (isset($form['vencimiento'])) ? $form['vencimiento'] : null;
        $this->funcionario = (isset($form['funcionario'])) ? $form['funcionario'] : null;
        $this->inspeccion = $obj->inspeccion0;
        $this->persona = $obj->persona0;
        $this->control = 1;
    }
}
