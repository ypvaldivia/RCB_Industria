<?php

namespace app\forms;

use yii\base\Model;
/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 8/24/15
 * Time: 11:25 AM.
 */
class AprobacionPorFuncionarioForm extends Model
{
    public $aprobada;
    public $rechazada;
    public $hallazgos;
    public $inconformidades;
    public $actualizado;

    public function rules()
    {
        return array(
            array('aprobada, rechazada', 'required'),
            array('aprobada, rechazada', 'validateAprobadaRechazada'),
            array(
                'hallazgos, inconformidades',
                'validateHallazgosInconformidades'
            )
        );
    }

    /*
     * Validacion para establecer la inspeccion como aprobada o rechazada
     */
    public function validateAprobadaRechazada($attribute, $params)
    {
        if (!isset($this->$attribute)) {
            //$this->addError($attribute,'Seleccione al menos un inspector actuante.');
        }
    }

    /*
     * Validacion para establecer que exista al menos hallazogos o inconformidades
     */
    public function validateHallazgosInconformidades($attribute, $params)
    {
        if (!isset($this->$attribute)) {
            //$this->addError($attribute,'Seleccione al menos un inspector actuante.');
        }
    }

    public function attributeLabels()
    {
        return array(
            'aprobada' => 'Aprobada',
            'rechazada' => 'Rechazada',
            'hallazgos' => 'Hallazgos',
            'inconformidades' => 'No conformidades',
            'actualizado' => 'Actualizado'
        );
    }

    /*
     * Asigna a cada atributo el valor que fue insertado en el formulario. La variable $form que entra al metodo
     * como parametro es la variable global $_POST['nombre_del_formulario'].
     */
    public function setMyAttributes($form)
    {
        if (isset($form['aprobada']) && $form['aprobada'] == 1) {
            $this->aprobada = $form['aprobada'];
        }
        if (isset($form['rechazada']) && $form['rechazada'] == 0) {
            $this->rechazada = $form['rechazada'];
        }
        if (isset($form['hallazgos'])) {
            $this->hallazgos = $form['hallazgos'];
        }
        if (isset($form['inconformidades'])) {
            $this->inconformidades = $form['inconformidades'];
        }
        if (isset($form['actualizado'])) {
            $this->actualizado = $form['actualizado'];
        }
    }
}
