<?php

namespace app\forms;

use app\models\Inspecciones;
use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 10/6/15
 * Time: 7:59 PM.
 */
class CertificadoInspectorSoldaduraForm extends Model
{
    public $id;//id inspeccion
    public $control;

    //public $numero;
    public $nombre;
    public $carnet;
    public $empresa;
    public $nivel;
    public $alcance;
    public $emision;
    public $vence;
    public $storeEmision;
    public $storeValidez;
    public $nivelAprobacion;
    public $funcionario;

    public function rules()
    {
        return array(
            array('nivel, alcance, emision, vence', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            //'numero' => 'No.',
            'nombre' => 'Que se le otorga al Sr. (a)',
            'empresa' => 'Empresa',
            'nivel' => 'Nivel',
            'alcance' => 'Alcance',
            'emision' => 'Fecha de expedición',
            'vence' => 'Fecha de vencimiento',
            'funcionario' => 'Inspector del RCB',
        );
    }

    public function initForm($inspXper)
    {
        $inspeccion = Inspecciones::findOne($inspXper->inspeccion0->id);
        $this->nombre = $inspXper->persona0->nombre_apellidos;
        $this->carnet = $inspXper->persona0->ci;
        $this->empresa = $inspeccion->entidad0->nombre;
        $this->id = $inspeccion->id;
    }

    public function setMyAttributes($form)
    {
        //$this->numero = (isset($form['numero'])) ? $form['numero'] : null;
        $this->funcionario = (isset($form['funcionario'])) ? $form['funcionario'] : null;
        $this->nivel = (isset($form['nivel'])) ? $form['nivel'] : null;
        $this->alcance = (isset($form['alcance'])) ? $form['alcance'] : null;
        $this->emision = (isset($form['emision'])) ? $form['emision'] : null;
        $this->vence = (isset($form['vence'])) ? $form['vence'] : null;
    }
}
