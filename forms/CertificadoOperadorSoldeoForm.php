<?php

namespace app\forms;

use app\models\Inspecciones;
use app\models\InspeccionesXPersonas;
use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 10/4/15
 * Time: 6:07 PM.
 */
class CertificadoOperadorSoldeoForm extends Model
{
    public $id; //id de la inspeccion
    public $control;

    public $numero;
    public $nombre;
    public $fecha;
    public $lugarNacimiento;
    public $carnet;
    public $procedimientoSoldadura;
    public $empresa;
    public $resultadoExamenTeorico; //aprobado o No requeredido
    public $resultadoExamenPractico;
    public $nivelAprobacion;
    public $funcionario;
    public $nivel;
    //Datos tecnicos.
    public $procesoSoldadura_detalles;
    public $procesoSoldadura_rango;
    public $chapaTubo_detalles;
    public $chapaTubo_rango;
    public $tipoUnion_detalles;
    public $tipoUnion_rango;
    public $grupoMetalBase_detalles;
    public $grupoMetalBase_rango;
    public $tipoMetal_detalles;
    public $tipoMetal_rango;
    public $gasesProteccion;
    public $gasesProteccion_rango;
    public $espesor_detalles;
    public $espesor_rango;
    public $diametro_detalles;
    public $diametro_rango;
    public $posicion_detalles;
    public $posicion_rango;
    public $tecnicaPasada_detalles;
    public $tecnicaPasada_rango;
    //Calificacion basada en:
    public $wps;
    public $ensayoAnterior;
    public $muestraProd;
    public $funcionamiento;
    //Fechas
    public $emision;
    public $validez;
    public $storeEmision;
    public $storeValidez;
    //Ensayos que avalan el certificado.
    public $visual;
    public $radiograficoUltrasonico;
    public $particulasMagneticas;
    public $liquidosPenetrantes;
    public $otros;
    //Simbologia
    public $simbologia;
    //Traccion
    public $temperaturaTr;
    public $realizadoTr;
    public $norefTr;
    //public $ensayosTraccion = array();
    //Doblado
    public $diametroM;
    public $distanciaR;
    public $realizadoDb;
    public $norefDb;
    //Macrografico
    public $realizadoMc;
    public $norefMc;
    //Rotura
    public $temperaturaRt;
    public $realizadoRt;
    public $norefTRt;

    public $ensayoTraccion = array();
    public $ensayoDoblado = array();
    public $ensayoRotura = array();

    public function rules()
    {
        return array(
            array('numero, fecha, procedimientoSoldadura,
            resultadoExamenTeorico', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'numero' => 'No.',
            'fecha' => 'Fecha de nacimiento',
            'lugarNacimiento' => 'Lugar de nacimiento',
            'carnet' => 'No. de carnet de indentidad',
            'procedimientoSoldadura' => 'Procedimiento de soldadura',
            'empresa' => 'Empresa',
            'resultadoExamenTeorico' => 'Resultados del examen teórico (Approbado/No requerido)',
            'resultadoExamenPractico' => 'Resultados del examen práctico',
            'simbologia' => '',
        );
    }

    public function initForm($inspXper)
    {
        $inspeccion = Inspecciones::findOne($inspXper->inspeccion0->id);
        $this->nombre = $inspXper->persona0->nombre_apellidos;
        $this->carnet = $inspXper->persona0->ci;
        $this->empresa = $inspeccion->entidad0->nombre;
        $this->lugarNacimiento = $inspXper->persona0->lugar_nacimiento;
        $this->id = $inspeccion->id;
    }

    public function initAjaxForm($o)
    {
        $ip = InspeccionesXPersonas::findOne($o);
        $inspeccion = Inspecciones::findOne($ip->inspeccion0->id);
        $this->nombre = $ip->persona0->nombre_apellidos;
        $this->carnet = $ip->persona0->ci;
        $this->empresa = $inspeccion->entidad0->nombre;
        $this->lugarNacimiento = $ip->persona0->lugar_nacimiento;
        $this->id = $inspeccion->id;
    }

    public function setAjaxAttributes($formData)
    {
        $this->id = (isset($formData[0]['idinsp'])) ? $formData[0]['idinsp'] : null;
        $this->validez = (isset($formData[0]['validez'])) ? $formData[0]['validez'] : null;
        $this->emision = (isset($formData[0]['emision'])) ? $formData[0]['emision'] : null;
        $this->procedimientoSoldadura = (isset($formData[0]['procedimiento'])) ? $formData[0]['procedimiento'] : '--';
        $this->funcionario = (isset($formData[0]['funcionario'])) ? $formData[0]['funcionario'] : null;
        $this->nivel = (isset($formData[0]['nivel'])) ? $formData[0]['nivel'] : null;
        $this->resultadoExamenTeorico = (isset($formData[0]['examenTeorico'])) ? $formData[0]['examenTeorico'] : null;
        $this->simbologia = (isset($formData[0]['simbologia'])) ? $formData[0]['simbologia'] : null;
        //Datos tecnicos.
        $this->procesoSoldadura_detalles = (isset($formData[0]['proceso']) && $formData[0]['proceso'] != null) ? $formData[0]['proceso'] : '--';
        $this->procesoSoldadura_rango = (isset($formData[0]['procesoRango']) && $formData[0]['procesoRango'] != null) ? $formData[0]['procesoRango'] : '--';
        $this->chapaTubo_detalles = (isset($formData[0]['chapa']) && $formData[0]['chapa'] != null) ? $formData[0]['chapa'] : '--';
        $this->chapaTubo_rango = (isset($formData[0]['chapaRango']) && $formData[0]['chapaRango'] != null) ? $formData[0]['chapaRango'] : '--';
        $this->tipoUnion_detalles = (isset($formData[0]['tipoUnion']) && $formData[0]['tipoUnion'] != null) ? $formData[0]['tipoUnion'] : '--';
        $this->tipoUnion_rango = (isset($formData[0]['tipoUnionRango']) && $formData[0]['tipoUnionRango'] != null) ? $formData[0]['tipoUnionRango'] : '--';
        $this->grupoMetalBase_detalles = (isset($formData[0]['grupoMetalBase']) && $formData[0]['grupoMetalBase'] != null) ? $formData[0]['grupoMetalBase'] : '--';
        $this->grupoMetalBase_rango = (isset($formData[0]['grupoMetalBaseRango']) && $formData[0]['grupoMetalBaseRango'] != null) ? $formData[0]['grupoMetalBaseRango'] : '--';
        $this->tipoMetal_detalles = (isset($formData[0]['tipoMetal']) && $formData[0]['tipoMetal'] != null) ? $formData[0]['tipoMetal'] : '--';
        $this->tipoMetal_rango = (isset($formData[0]['tipoMetalRango']) && $formData[0]['tipoMetalRango'] != null) ? $formData[0]['tipoMetalRango'] : '--';
        $this->gasesProteccion = (isset($formData[0]['gasesProteccion']) && $formData[0]['gasesProteccion'] != null) ? $formData[0]['gasesProteccion'] : '--';
        $this->gasesProteccion_rango = (isset($formData[0]['gasesProteccionRango']) && $formData[0]['gasesProteccionRango'] != null) ? $formData[0]['gasesProteccionRango'] : '--';
        $this->espesor_detalles = (isset($formData[0]['espesor']) && $formData[0]['espesor'] != null) ? $formData[0]['espesor'] : '--';
        $this->espesor_rango = (isset($formData[0]['espesorRango']) && $formData[0]['espesorRango'] != null) ? $formData[0]['espesorRango'] : '--';
        $this->diametro_detalles = (isset($formData[0]['diametro']) && $formData[0]['diametro'] != null) ? $formData[0]['diametro'] : '--';
        $this->diametro_rango = (isset($formData[0]['diametroRango']) && $formData[0]['diametroRango'] != null) ? $formData[0]['diametroRango'] : '--';
        $this->posicion_detalles = (isset($formData[0]['posicion']) && $formData[0]['posicion'] != null) ? $formData[0]['posicion'] : '--';
        $this->posicion_rango = (isset($formData[0]['posicionRango']) && $formData[0]['posicionRango'] != null) ? $formData[0]['posicionRango'] : '--';
        $this->tecnicaPasada_detalles = (isset($formData[0]['tecnicaPasada']) && $formData[0]['tecnicaPasada'] != null) ? $formData[0]['tecnicaPasada'] : '--';
        $this->tecnicaPasada_rango = (isset($formData[0]['tecnicaPasadaRango']) && $formData[0]['tecnicaPasadaRango'] != null) ? $formData[0]['tecnicaPasadaRango'] : '--';
        //Calificacion basada en
        $this->wps = (isset($formData[0]['wps'])) ? $formData[0]['wps'] : '--';
        $this->ensayoAnterior = (isset($formData[0]['anterior'])) ? $formData[0]['anterior'] : '--';
        $this->muestraProd = (isset($formData[0]['muestra'])) ? $formData[0]['muestra'] : '--';
        $this->funcionamiento = (isset($formData[0]['funcionamiento'])) ? $formData[0]['funcionamiento'] : '--';
        //Ensayos que avalan el certificado.
        $this->visual = (isset($formData[0]['visual'])) ? $formData[0]['visual'] : '--';
        $this->radiograficoUltrasonico = (isset($formData[0]['radiografico'])) ? $formData[0]['radiografico'] : '--';
        $this->particulasMagneticas = (isset($formData[0]['particulas'])) ? $formData[0]['particulas'] : '--';
        $this->liquidosPenetrantes = (isset($formData[0]['liquidos'])) ? $formData[0]['liquidos'] : '--';
        $this->otros = (isset($formData[0]['otros'])) ? $formData[0]['otros'] : '--';
        //Traccion
        $this->temperaturaTr = (isset($formData[0]['temperaturaTr'])) ? $formData[0]['temperaturaTr'] : '--';
        $this->realizadoTr = (isset($formData[0]['realizadoTr'])) ? $formData[0]['realizadoTr'] : '--';
        $this->norefTr = (isset($formData[0]['norefTr'])) ? $formData[0]['norefTr'] : '--';
        //Doblado
        $this->realizadoDb = (isset($formData[0]['realizadoDb'])) ? $formData[0]['realizadoDb'] : '--';
        $this->norefDb = (isset($formData[0]['norefDb'])) ? $formData[0]['norefDb'] : '--';
        $this->diametroM = (isset($formData[0]['diametroM'])) ? $formData[0]['diametroM'] : '--';
        $this->distanciaR = (isset($formData[0]['distanciaR'])) ? $formData[0]['distanciaR'] : '--';
        //Macrografico
        $this->realizadoMc = (isset($formData[0]['realizadoMc'])) ? $formData[0]['realizadoMc'] : '--';
        $this->norefMc = (isset($formData[0]['norefMc'])) ? $formData[0]['norefMc'] : '--';
        //Rotura
        $this->temperaturaRt = (isset($formData[0]['temperaturaRt'])) ? $formData[0]['temperaturaRt'] : '--';
        $this->realizadoRt = (isset($formData[0]['realizadoRt'])) ? $formData[0]['realizadoRt'] : '--';
        $this->norefTRt = (isset($formData[0]['norefTRt'])) ? $formData[0]['norefTRt'] : '--';
        //Arreglos para las tablas
        //Traccion
        if (isset($formData[1]) && count($formData[1]) > 0) {
            for ($i = 0;$i < count($formData[1]); ++$i) {
                $element = new EnsayosOperadorSoldeoForm();
                $element->numeroTr = (isset($formData[1][$i]['number'])) ? $formData[1][$i]['number'] : '--';
                $element->probetaTr = (isset($formData[1][$i]['probeta'])) ? $formData[1][$i]['probeta'] : '--';
                $element->seccionTr = (isset($formData[1][$i]['seccion'])) ? $formData[1][$i]['seccion'] : '--';
                $element->limiteTr = (isset($formData[1][$i]['limite'])) ? $formData[1][$i]['limite'] : '--';
                $element->resistenciaTr = (isset($formData[1][$i]['resistencia'])) ? $formData[1][$i]['resistencia'] : '--';
                $element->posicionTr = (isset($formData[1][$i]['posicion'])) ? $formData[1][$i]['posicion'] : '--';
                $element->aspectoTr = (isset($formData[1][$i]['aspecto'])) ? $formData[1][$i]['aspecto'] : '--';

                array_push($this->ensayoTraccion, $element);
            }
        }
        //Doblado
        if (isset($formData[2]) && count($formData[2]) > 0) {
            for ($i = 0;$i < count($formData[2]); ++$i) {
                $element = new EnsayosOperadorSoldeoForm();
                $element->numeroDb = (isset($formData[2][$i]['numberD'])) ? $formData[2][$i]['numberD'] : '--';
                $element->probetaDb = (isset($formData[2][$i]['probetaD'])) ? $formData[2][$i]['probetaD'] : '--';
                $element->posicionDb = (isset($formData[2][$i]['posicionD'])) ? $formData[2][$i]['posicionD'] : '--';
                $element->anguloDb = (isset($formData[2][$i]['angulo'])) ? $formData[2][$i]['angulo'] : '--';
                $element->resultadoDb = (isset($formData[2][$i]['resultadoD'])) ? $formData[2][$i]['resultadoD'] : '--';

                array_push($this->ensayoDoblado, $element);
            }
        }
        //Rotura
        if (isset($formData[3]) && count($formData[3]) > 0) {
            for ($i = 0;$i < count($formData[3]); ++$i) {
                $element = new EnsayosOperadorSoldeoForm();
                $element->numeroRt = (isset($formData[3][$i]['numberR'])) ? $formData[3][$i]['numberR'] : '--';
                $element->probetaRt = (isset($formData[3][$i]['probetaR'])) ? $formData[3][$i]['probetaR'] : '--';
                $element->posicionRt = (isset($formData[3][$i]['posicionR'])) ? $formData[3][$i]['posicionR'] : '--';
                $element->denominacionRt = (isset($formData[3][$i]['denominacionR'])) ? $formData[3][$i]['denominacionR'] : '--';
                $element->resultadoRt = (isset($formData[3][$i]['resultadoR'])) ? $formData[3][$i]['resultadoR'] : '--';
                $element->nivelQRt = (isset($formData[3][$i]['nivelql'])) ? $formData[3][$i]['nivelql'] : '--';

                array_push($this->ensayoRotura, $element);
            }
        }
    }
}
