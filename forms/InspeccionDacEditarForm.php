<?php

namespace app\forms;

use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: G-UX
 * Date: 22/06/2016
 * Time: 1:05.
 */
class InspeccionDacEditarForm extends InspeccionDacForm
{
    public function rules()
    {
        return array(
            array('numero_control, oficina, lugar_inspeccion, homologacion, entidad', 'required'),
            array('numero_control, oficina, lugar_inspeccion, homologacion, detalle_homologacion,
                    nivel, tipo_homologacion, estado, certificado, vigencia, numero_visita, fecha, descripcion,
                    pdf, inspeccion, aprobado, aprobada, rechazada, hallazgos, inconformidades, actualizado',
                'safe', 'on' => 'search', ),
        );
    }
}
