<?php

namespace app\forms;

use app\models\AcreditacionesIndustriales;
use app\models\AcreditacionesInspectores;
use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: geo
 * Date: 18/02/16
 * Time: 14:25.
 */
class VisitaInspeccionPersonaForm extends InspeccionPersonaForm
{
    public function rules()
    {
        return array(
            array('fecha', 'required'),
            array('inspectores_actuantes', 'validateExistInspectoresActuantes'),
            array('inspectores_actuantes', 'validateIsInspectorForThisHomologacion'),
            array('fecha, aprobado', 'safe', 'on' => 'search'),
        );
    }

    /*
     * Validacion de los inspectores actuantes que garantiza que es un inspector hologado para poder acreditar
     * en la acreditacion en cuestion que se lleva a cabo.
     */
    public function validateIsInspectorForThisHomologacion($attribute, $params)
    {
        //($attribute == null || count($attribute) == 0 || count($attribute) == 1)
        if ($this->inspectores_actuantes == null) {
            $this->addError($attribute, 'Inspectores no puede ser nulo.');
        } else {
            $c = 0;
            foreach ($this->inspectores_actuantes as $idInspector) {
                $homologacion = AcreditacionesIndustriales::findOne($this->homologacion);
                if($homologacion->categoria != null){
                    $acreditacion = AcreditacionesInspectores::findOne(array('acreditacion' => $homologacion->categoria, 'inspector' => $idInspector));
                    if ($acreditacion != null && $acreditacion->vencimiento >= date('Y-m-d')) {
                        ++$c;
                    }
                }
            }
            if ($c == 0) {
                $this->addError($attribute,
                    'Debe existir al menos un inspector actuante que sea <strong>inspector y que est&eacute; debidamente acreditado</strong> para esta inspecci&oacute;n.: '.$homologacion->categoria
                );
            }
        }
    }

    /*
     * Asigna a cada atributo el valor que fue insertado en el formulario. La variable $form que entra al metodo
     * como parametro es la variable global $_POST['nombre_del_formulario'].
     */
    public function setMyAttributes($form)
    {
        if (isset($form['id'])) {
            $this->numero_visita = $form['id'];
        }
        if (isset($form['numero_visita'])) {
            $this->numero_visita = $form['numero_visita'];
        }
        if (isset($form['fecha'])) {
            $this->fecha = $form['fecha'];
        }
        if (isset($form['descripcion'])) {
            $this->descripcion = $form['descripcion'];
        }
        if (isset($form['inspeccion'])) {
            $this->inspeccion = $form['inspeccion'];
        }
        if (isset($form['aprobado'])) {
            $this->aprobado = $form['aprobado'];
        }
        if (isset($form['inspectores_actuantes'])) {
            $this->inspectores_actuantes = $form['inspectores_actuantes'];
        }
        if (isset($form['lugar_inspeccion'])) {
            $this->lugar_inspeccion = $form['lugar_inspeccion'];
        }
        if (isset($form['personas_homologar'])) {
            $this->personas_homologar = $form['personas_homologar'];
        }
        if (isset($form['personas_desaprobadas'])) {
            $this->personas_desaprobadas = $form['personas_desaprobadas'];
        }
    }

    /*
 * Asigna a cada atributo el valor que fue insertado en el formulario. La variable $form que entra al metodo
 * como parametro es la variable global $_POST['nombre_del_formulario'].
 */
    public function setMyAttributesByModel($model)
    {
        $this->id = $model->id;
        $this->numero_visita = $model->numero_visita;
        $this->fecha = $model->fecha;
        $this->descripcion = $model->descripcion;
        $this->inspeccion = $model->inspeccion;
        $this->aprobado = $model->aprobado;
        $this->inspectores_actuantes = $model->inspectoresActuantes;
        $this->personas_homologar = $model->personasAprobadas();
        $this->personas_desaprobadas = $model->personasNoAprobadas();

        $this->vigencia = $model->inspeccion0->vigencia;
        $this->lugar_inspeccion = $model->inspeccion0->lugar_inspeccion;
    }

    /*
 * Establece un arreglo para indicar los laboratorios que han sido seleccionados para inspeccion en caso de
 * que se necesite editar la inspeccion.
 */
    public function getInspectoresActuantes()
    {
        $select = array();

        if (isset($this->inspectores_actuantes)) {
            foreach ($this->inspectores_actuantes as $ia) {
                if ($ia->visita == $this->id) {
                    $select[$ia->inspector] = array('selected' => true);
                }
            }
        }

        return $select;
    }
}
