<?php

namespace app\forms;

use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 9/16/15
 * Time: 1:06 PM.
 */
class InspeccionProductoForm extends InspeccionBaseForm
{
    public $productos_homologar;
    public $productos_desaprobados;
    public $aprobados_list;
    public $desaprobados_list;
    public $store_aprobados;
    public $store_desaprobados;

    /*
     * Hereda las reglas de validacion del padre. Solamente hay que declararle las reglas a los atributos
     * particulares de esta clase.
     */
    public function rules()
    {
        $rules = array(
            //array('productos_homologar', 'required'),
            array('store_aprobados', 'validateExistProductoHomologar'),
            array('store_desaprobados', 'validateDesaprobados'),
        );

        return array_merge($rules, parent::rules());
    }

    /*
     * Validacion para que exista al menos un producto a homologar
     */
    public function validateExistProductoHomologar($attribute, $params)
    {
        if ($this->store_aprobados == null && $this->store_aprobados != null) {
            $this->addError($this->productos_homologar, 'Seleccione al menos un producto a homologar.');
        }
    }

    public function validateDesaprobados($attribute, $params)
    {
        if ($this->store_desaprobados != null && $this->store_aprobados != null) {
            foreach ($this->store_desaprobados as $producto) {
                foreach ($this->store_aprobados as $aprobado) {
                    if ($producto == $aprobado) {
                        $this->addError('productos_desaprobados', 'No puede marcar el mismo producto como Aprobado y Desaprobado a la vez');
                        break;
                    }
                }
            }
        }
    }

    /*
     * Hereda del padre todas las etiquetas. Solamente hay que declararle las etiquetas particulares de esta clase
     * en al estructura de del array llave=>valor;
     */
    public function attributeLabels()
    {
        $labels = array(
            'productos_homologar' => 'Objeto de la Homologaci&oacute;n',
            'productos_desaprobados' => 'Productos No Aprobados',
        );

        return array_merge($labels, parent::attributeLabels());
    }

    /*
     * Asigna a cada atributo el valor que fue insertado en el formulario. La variable $form que entra al metodo
     * como parametro es la variable global $_POST['nombre_del_formulario'].
     */
    public function setMyAttributes($form)
    {
        $this->numero_control = (isset($form['numero_control'])) ? $form['numero_control'] : null;
        $this->codigo = (isset($form['codigo'])) ? $form['codigo'] : null;
        $this->entidad = (isset($form['entidad'])) ? $form['entidad'] : null;
        $this->oficina = (isset($form['oficina'])) ? $form['oficina'] : null;
        $this->lugar_inspeccion = (isset($form['lugar_inspeccion'])) ? $form['lugar_inspeccion'] : null;
        $this->homologacion = (isset($form['homologacion'])) ? $form['homologacion'] : null;
        $this->detalle_homologacion = (isset($form['detalle_homologacion'])) ? $form['detalle_homologacion'] : null;
        $this->nivel = (isset($form['nivel'])) ? $form['nivel'] : null;
        $this->tipo_homologacion = (isset($form['tipo_homologacion'])) ? $form['tipo_homologacion'] : null;
        $this->estado = (isset($form['estado'])) ? $form['estado'] : null;
        $this->certificado = (isset($form['certificado'])) ? $form['certificado'] : null;
        $this->vigencia = (isset($form['vigencia'])) ? $form['vigencia'] : null;
        $this->numero_visita = (isset($form['numero_visita'])) ? $form['numero_visita'] : null;
        $this->fecha = (isset($form['fecha'])) ? $form['fecha'] : null;
        $this->descripcion = (isset($form['descripcion'])) ? $form['descripcion'] : null;
        $this->inspectores_actuantes = (isset($form['inspectores_actuantes'])) ? $form['inspectores_actuantes'] : null;
        $this->productos_homologar = (isset($form['productos_homologar'])) ? $form['productos_homologar'] : null;
        $this->productos_desaprobados = (isset($form['productos_desaprobados'])) ? $form['productos_desaprobados'] : null;

        if (isset($form['aprobados_list']) && $form['aprobados_list'] != "") {
            $tmp = explode(',', $form['aprobados_list']);
            $this->store_aprobados = array();
            foreach ($tmp as $m) {
                array_push($this->store_aprobados, intval($m));
            }
        }else{
            $this->store_aprobados = null;
        }

        if (isset($form['desaprobados_list']) && $form['desaprobados_list'] != "") {
            $tmp = explode(',', $form['desaprobados_list']);
            $this->store_desaprobados = array();
            foreach ($tmp as $m) {
                array_push($this->store_desaprobados, intval($m));
            }
        }else{
            $this->store_desaprobados = null;
        }
    }

    /*
     * Asigna los valores de los atributos partiendo del modelo Inspeccion.
     */
    public function setMyAttributesByModel($model, $visita)
    {
        $this->id = $model->id;
        $this->numero_control = $model->numero_control;
        $this->codigo = $model->codigo;
        $this->entidad = $model->entidad;
        $this->oficina = $model->oficina;
        $this->lugar_inspeccion = $model->lugar_inspeccion;
        $this->homologacion = $model->homologacion;
        $this->detalle_homologacion = $model->detalle_homologacion;
        $this->nivel = $model->nivel;
        $this->tipo_homologacion = $model->tipo_homologacion;
        $this->estado = $model->estado;
        $this->certificado = $model->certificado;
        $this->productos_homologar = $model->productosAprobados();
        $this->productos_desaprobados = $model->productosNoAprobados();

        $this->fecha = $visita->fecha;
        $this->numero_visita = $visita->numero_visita;
        $this->aprobado = $visita->aprobado;
        $this->descripcion = $visita->descripcion;
        $this->inspectores_actuantes = $visita->inspectoresActuantes;
    }

    /*
     * Establece un arreglo para indicar los laboratorios que han sido seleccionados para inspeccion en caso de
     * que se necesite editar la inspeccion.
     */
    public function getEntesInspeccionados()
    {
        $select = array();

        if ($this->productos_homologar != null) {
            foreach ($this->productos_homologar as $ph) {
                if ($ph->inspeccion == $this->id) {
                    $select[$ph->medio_izado] = array('selected' => true);
                }
            }
        }

        return $select;
    }
}
