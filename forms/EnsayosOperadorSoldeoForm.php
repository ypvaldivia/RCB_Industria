<?php

namespace app\forms;

use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: geo
 * Date: 11/04/16
 * Time: 20:06.
 */
class EnsayosOperadorSoldeoForm extends Model
{
    public $numeroTr;
    public $probetaTr;
    public $seccionTr;
    public $limiteTr;
    public $resistenciaTr;
    public $posicionTr;
    public $aspectoTr;

    public $numeroDb;
    public $probetaDb;
    public $posicionDb;
    public $anguloDb;
    public $resultadoDb;

    public $numeroRt;
    public $probetaRt;
    public $posicionRt;
    public $denominacionRt;
    public $resultadoRt;
    public $nivelQRt;
}
