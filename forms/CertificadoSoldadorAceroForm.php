<?php

namespace app\forms;

use app\models\Inspecciones;
use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 10/6/15
 * Time: 7:09 PM.
 */
class CertificadoSoldadorAceroForm extends Model
{
    public $id;//id inspeccion
    public $control;

    //public $numero;
    public $nombre;
    public $fecha;
    public $lugarNacimiento;
    public $carnet;
    public $procedimientoSoldadura;//procedimiento de soldadura
    public $empresa;
    public $resultadoExamenTeorico; //aprobado o No requeredido
    public $resultadoExamenPractico;
    public $emision;
    public $validez;
    public $storeEmision;
    public $storeValidez;
    public $nivelAprobacion;
    public $funcionario;
    //Datos tecnicos
    public $procesoSoldadura_detalles;
    public $procesoSoldadura_rango;
    public $chapaTubo_detalles;
    public $chapaTubo_rango;
    public $tipoUnion_detalles;
    public $tipoUnion_rango;
    public $grupoMetalBase_detalles;
    public $grupoMetalBase_rango;
    public $tipoMetal_detalles;
    public $tipoMetal_rango;
    public $gases_detalles;
    public $gases_rango;
    public $espesor_detalles;
    public $espesor_rango;
    public $diametro_detalles;
    public $diametro_rango;
    public $posicion_detalles;
    public $posicion_rango;
    public $resanado_detalles;
    public $resanado_rango;
    //Ensayos que avalan el certificado.
    public $visual;
    public $radiograficoUltrasonico;
    public $particulasMagneticas;
    public $liquidosPenetrantes;
    public $rotura;
    public $doblado;
    public $otros;
    //Simbologia
    public $simbologia;

    public function rules()
    {
        return array(
            array('procedimientoSoldadura, emision, validez', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            //'numero' => 'No.',
            'fecha' => 'Fecha de nacimiento',
            'lugarNacimiento' => 'Lugar de nacimiento',
            'carnet' => 'No. de carnet de indentidad',
            'emision' => 'Fecha de emisión',
            'validez' => 'Período de validez',
            'procedimientoSoldadura' => 'Procedimiento de soldadura',
            'procedimientoSoldadura' => 'Procedimiento de soldadura',
            'empresa' => 'Empresa',
            'resultadoExamenTeorico' => 'Resultados del examen teórico (Approbado/No requerido)',
            'resultadoExamenPractico' => 'Resultados del examen práctico',
            'simbologia' => '',
            'funcionario' => 'Inspector del RCB',
        );
    }

    public function initForm($inspXper)
    {
        $inspeccion = Inspecciones::findOne($inspXper->inspeccion0->id);
        $this->nombre = $inspXper->persona0->nombre_apellidos;
        $this->carnet = $inspXper->persona0->ci;
        $this->empresa = $inspeccion->entidad0->nombre;
        $this->id = $inspeccion->id;
    }

    public function setMyAttributes($form)
    {
        //$this->numero = (isset($form['numero'])) ? $form['numero'] : null;
        $this->funcionario = (isset($form['funcionario'])) ? $form['funcionario'] : null;
        $this->procedimientoSoldadura = (isset($form['procedimientoSoldadura'])) ? $form['procedimientoSoldadura'] : null;
        $this->resultadoExamenTeorico = (isset($form['resultadoExamenTeorico'])) ? $form['resultadoExamenTeorico'] : null;
        $this->resultadoExamenPractico = (isset($form['resultadoExamenPractico'])) ? $form['resultadoExamenPractico'] : null;
        $this->emision = (isset($form['emision'])) ? $form['emision'] : null;
        $this->validez = (isset($form['validez'])) ? $form['validez'] : null;
        //Datos tecnicos.
        $this->procesoSoldadura_detalles = (isset($form['procesoSoldadura_detalles']) && ($form['procesoSoldadura_detalles']) != '') ? $form['procesoSoldadura_detalles'] : '--';
        $this->procesoSoldadura_rango = (isset($form['procesoSoldadura_rango']) && ($form['procesoSoldadura_rango']) != '') ? $form['procesoSoldadura_rango'] : '--';
        $this->chapaTubo_detalles = (isset($form['chapaTubo_detalles']) && ($form['chapaTubo_detalles']) != '') ? $form['chapaTubo_detalles'] : '--';
        $this->chapaTubo_rango = (isset($form['chapaTubo_rango']) && ($form['chapaTubo_rango']) != '') ? $form['chapaTubo_rango'] : '--';
        $this->tipoUnion_detalles = (isset($form['tipoUnion_detalles']) && ($form['tipoUnion_detalles']) != '') ? $form['tipoUnion_detalles'] : '--';
        $this->tipoUnion_rango = (isset($form['tipoUnion_rango']) && ($form['tipoUnion_rango']) != '') ? $form['tipoUnion_rango'] : '--';
        $this->grupoMetalBase_detalles = (isset($form['grupoMetalBase_detalles']) && ($form['grupoMetalBase_detalles']) != '') ? $form['grupoMetalBase_detalles'] : '--';
        $this->grupoMetalBase_rango = (isset($form['grupoMetalBase_rango']) && ($form['grupoMetalBase_rango']) != '') ? $form['grupoMetalBase_rango'] : '--';
        $this->tipoMetal_detalles = (isset($form['tipoMetal_detalles']) && ($form['tipoMetal_detalles']) != '') ? $form['tipoMetal_detalles'] : '--';
        $this->tipoMetal_rango = (isset($form['tipoMetal_rango']) && ($form['tipoMetal_rango']) != '') ? $form['tipoMetal_rango'] : '--';
        $this->gases_detalles = (isset($form['gases_detalles']) && ($form['gases_detalles']) != '') ? $form['gases_detalles'] : '--';
        $this->gases_rango = (isset($form['gases_rango']) && ($form['gases_rango']) != '') ? $form['gases_rango'] : '--';
        $this->espesor_detalles = (isset($form['espesor_detalles']) && ($form['espesor_detalles']) != '') ? $form['espesor_detalles'] : '--';
        $this->espesor_rango = (isset($form['espesor_rango']) && ($form['espesor_rango']) != '') ? $form['espesor_rango'] : '--';
        $this->diametro_detalles = (isset($form['diametro_detalles']) && ($form['diametro_detalles']) != '') ? $form['diametro_detalles'] : '--';
        $this->diametro_rango = (isset($form['diametro_rango']) && ($form['diametro_rango']) != '') ? $form['diametro_rango'] : '--';
        $this->posicion_detalles = (isset($form['posicion_detalles']) && ($form['posicion_detalles']) != '') ? $form['posicion_detalles'] : '--';
        $this->posicion_rango = (isset($form['posicion_rango']) && ($form['posicion_rango']) != '') ? $form['posicion_rango'] : '--';
        $this->resanado_detalles = (isset($form['resanado_detalles']) && ($form['resanado_detalles']) != '') ? $form['resanado_detalles'] : '--';
        $this->resanado_rango = (isset($form['resanado_rango']) && ($form['resanado_rango']) != '') ? $form['resanado_rango'] : '--';
        //Ensayos que avalan el certificado.
        $this->visual = (isset($form['visual']) && ($form['visual']) != '') ? $form['visual'] : '--';
        $this->radiograficoUltrasonico = (isset($form['radiograficoUltrasonico']) && ($form['radiograficoUltrasonico']) != '') ? $form['radiograficoUltrasonico'] : '--';
        $this->particulasMagneticas = (isset($form['particulasMagneticas']) && ($form['particulasMagneticas']) != '') ? $form['particulasMagneticas'] : '--';
        $this->liquidosPenetrantes = (isset($form['liquidosPenetrantes']) && ($form['liquidosPenetrantes']) != '') ? $form['liquidosPenetrantes'] : '--';
        $this->rotura = (isset($form['rotura']) && ($form['rotura']) != '') ? $form['rotura'] : '--';
        $this->doblado = (isset($form['doblado']) && ($form['doblado']) != '') ? $form['doblado'] : '--';
        $this->otros = (isset($form['otros']) && ($form['otros']) != '') ? $form['otros'] : '--';
        //Simbologia
        $this->simbologia = (isset($form['simbologia']) && ($form['simbologia']) != '') ? $form['simbologia'] : '--';
    }
}
