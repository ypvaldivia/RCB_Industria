<?php

namespace app\forms;

use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 8/27/15
 * Time: 3:07 PM.
 */
class InspeccionPersonaEditarForm extends InspeccionPersonaForm
{
    public function rules()
    {
        return array(
            array('numero_control, oficina, entidad, homologacion', 'required'),
        );
    }
}
