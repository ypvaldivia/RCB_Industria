<?php

namespace app\forms;

use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 9/16/15
 * Time: 2:13 PM.
 */
class InspeccionProductoEditarForm extends InspeccionProductoForm
{
    public function rules()
    {
        return array(
            array('numero_control, oficina, lugar_inspeccion, homologacion, entidad', 'required'),
            array('numero_control, oficina, lugar_inspeccion, homologacion, detalle_homologacion,
                    nivel, tipo_homologacion, estado, certificado, vigencia, numero_visita, fecha, descripcion,
                    pdf, inspeccion, aprobado, aprobada, rechazada, hallazgos, inconformidades, actualizado',
                  'safe', 'on' => 'search', ),
        );
    }
}
