<?php

namespace app\forms;

use app\components\CDbCriteria;
use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: geo
 * Date: 16/03/16
 * Time: 21:31.
 */
class BuscaObjetoForm extends Model
{
    public $nombre, $provincia, $entidad, $ci, $inspector;
    public $servicio, $departamento, $propietario;

    //Para libro de Certificados
    public $certificado, $objeto, $homologacion, $mes, $anho;

    private $inidate, $enddate, $convYear;

    //Para las trazas
    public $usuario, $ip, $accion, $inicio, $final;

    public function setMyAttributes($post)
    {
        $this->nombre = isset($post['nombre']) ? $post['nombre'] : null;
        $this->ci = isset($post['ci']) ? $post['ci'] : null;
        $this->inspector = isset($post['inspector'])
            ? $post['inspector']
            : null;
        $this->provincia = isset($post['provincia'])
            ? $post['provincia']
            : null;
        $this->entidad = isset($post['entidad']) ? $post['entidad'] : null;
        $this->servicio = isset($post['servicio']) ? $post['servicio'] : null;
        $this->departamento = isset($post['departamento'])
            ? $post['departamento']
            : null;
        $this->propietario = isset($post['propietario'])
            ? $post['propietario']
            : null;
        $this->certificado = isset($post['certificado'])
            ? $post['certificado']
            : null;
        $this->objeto = isset($post['objeto']) ? $post['objeto'] : null;
        $this->homologacion = isset($post['homologacion'])
            ? $post['homologacion']
            : null;
        $this->mes = isset($post['mes']) ? $post['mes'] : null;
        $this->anho = isset($post['anho']) ? $post['anho'] : null;

        $this->usuario = isset($post['usuario']) ? $post['usuario'] : null;
        $this->ip = isset($post['ip']) ? $post['ip'] : null;
        $this->accion = isset($post['accion']) ? $post['accion'] : null;
        $this->inicio = isset($post['inicio']) ? $post['inicio'] : null;
        $this->final = isset($post['final']) ? $post['final'] : null;
    }

    public function attributeLabels()
    {
        return array(
            'nombre' => Yii::t('app', 'Nombre'),
            'ci' => Yii::t('app', 'Carnet de Identidad'),
            'inspector' => Yii::t('app', 'Inspector'),
            'servicio' => Yii::t('app', 'Servicio'),
            'departamento' => Yii::t('app', 'Departamento'),
            'provincia' => Yii::t('app', 'Provincia'),
            'entidad' => Yii::t('app', 'Entidad'),
            'propietario' => Yii::t('app', 'Propietario'),
            'certificado' => Yii::t('app', 'No. Certificado'),
            'objeto' => Yii::t('app', 'Objeto del Certificado'),
            'homologacion' => Yii::t('app', 'Homologaci&oacute;n'),
            'mes' => Yii::t('app', 'Mes'),
            'anho' => Yii::t('app', 'Año'),

            'usuario' => Yii::t('app', 'Funcionario'),
            'ip' => Yii::t('app', 'Dirección IP'),
            'accion' => Yii::t('app', 'Acción'),
            'inicio' => Yii::t('app', 'Inicio del intervalo'),
            'final' => Yii::t('app', 'Final del intervalo')
        );
    }

    public static function getYearList()
    {
        $y = date('Y');
        $years = array();
        $years[0] = $y;
        for ($i = 1; $i < 4; ++$i) {
            $y = $y - 1;
            $years[$i] = $y;
        }

        return $years;
    }

    public static function getMonthList()
    {
        $months = array();
        $months[0] = 'Enero';
        $months[1] = 'Febrero';
        $months[2] = 'Marzo';
        $months[3] = 'Abril';
        $months[4] = 'Mayo';
        $months[5] = 'Junio';
        $months[6] = 'Julio';
        $months[7] = 'Agosto';
        $months[8] = 'Septiembre';
        $months[9] = 'Octubre';
        $months[10] = 'Noviembre';
        $months[11] = 'Diciembre';

        return $months;
    }

    private function getConvertedYear()
    {
        $tmp = date('Y');
        switch ($this->anho) {
            case 0:
                $this->convYear = $tmp;
                break;
            case 1:
                $this->convYear = $tmp - 1;
                break;
            case 2:
                $this->convYear = $tmp - 2;
                break;
            case 3:
                $this->convYear = $tmp - 3;
                break;
        }
    }

    private function makeDate()
    {
        if ($this->mes != null && $this->anho != null) {
            $this->getConvertedYear();
            switch ($this->mes) {
                case 0:
                    $this->inidate = $this->convYear . '-01-01';
                    $this->enddate = $this->convYear . '-01-31';
                    break;
                case 1:
                    $this->inidate = $this->convYear . '-02-01';
                    $this->enddate = $this->convYear . '-02-28';
                    break;
                case 2:
                    $this->inidate = $this->convYear . '-03-01';
                    $this->enddate = $this->convYear . '-03-31';
                    break;
                case 3:
                    $this->inidate = $this->convYear . '-04-01';
                    $this->enddate = $this->convYear . '-04-30';
                    break;
                case 4:
                    $this->inidate = $this->convYear . '-05-01';
                    $this->enddate = $this->convYear . '-05-31';
                    break;
                case 5:
                    $this->inidate = $this->convYear . '-06-01';
                    $this->enddate = $this->convYear . '-06-30';
                    break;
                case 6:
                    $this->inidate = $this->convYear . '-07-01';
                    $this->enddate = $this->convYear . '-07-31';
                    break;
                case 7:
                    $this->inidate = $this->convYear . '-08-01';
                    $this->enddate = $this->convYear . '-08-31';
                    break;
                case 8:
                    $this->inidate = $this->convYear . '-09-01';
                    $this->enddate = $this->convYear . '-09-30';
                    break;
                case 9:
                    $this->inidate = $this->convYear . '-10-01';
                    $this->enddate = $this->convYear . '-10-31';
                    break;
                case 10:
                    $this->inidate = $this->convYear . '-11-01';
                    $this->enddate = $this->convYear . '-11-30';
                    break;
                case 11:
                    $this->inidate = $this->convYear . '-12-01';
                    $this->enddate = $this->convYear . '-12-31';
                    break;
            }
        } elseif ($this->mes == null && $this->anho != null) {
            $this->getConvertedYear();
            $this->inidate = $this->convYear . '-01-01';
            $this->enddate = $this->convYear . '-12-31';
        } elseif ($this->mes != null && $this->anho == null) {
            $y = date('Y');
            switch ($this->mes) {
                case 0:
                    $this->inidate = $y . '-01-01';
                    $this->enddate = $y . '-01-31';
                    break;
                case 1:
                    $this->inidate = $y . '-02-01';
                    $this->enddate = $y . '-02-28';
                    break;
                case 2:
                    $this->inidate = $y . '-03-01';
                    $this->enddate = $y . '-03-31';
                    break;
                case 3:
                    $this->inidate = $y . '-04-01';
                    $this->enddate = $y . '-04-30';
                    break;
                case 4:
                    $this->inidate = $y . '-05-01';
                    $this->enddate = $y . '-05-31';
                    break;
                case 5:
                    $this->inidate = $y . '-06-01';
                    $this->enddate = $y . '-06-30';
                    break;
                case 6:
                    $this->inidate = $y . '-07-01';
                    $this->enddate = $y . '-07-31';
                    break;
                case 7:
                    $this->inidate = $y . '-08-01';
                    $this->enddate = $y . '-08-31';
                    break;
                case 8:
                    $this->inidate = $y . '-09-01';
                    $this->enddate = $y . '-09-30';
                    break;
                case 9:
                    $this->inidate = $y . '-10-01';
                    $this->enddate = $y . '-10-31';
                    break;
                case 10:
                    $this->inidate = $y . '-11-01';
                    $this->enddate = $y . '-11-30';
                    break;
                case 11:
                    $this->inidate = $y . '-12-01';
                    $this->enddate = $y . '-12-31';
                    break;
            }
        }
    }

    public function execSearchPersonas()
    {
        $criteria = new CDbCriteria();
        $query = '';
        $join = '';
        $keys = array();
        $values = array();
        if ($this->certificado != null) {
            $query = 'certificado LIKE :match ';
            array_push($keys, ':match');
            array_push($values, "%$this->certificado%");
        }
        if ($this->objeto != null) {
            $join =
                'inner join personas_acreditadas on persona = personas_acreditadas.id';
            if ($query != '') {
                $query =
                    $query .
                    ' AND (personas_acreditadas.nombre_apellidos LIKE :p OR personas_acreditadas.ci LIKE :p) ';
            } else {
                $query =
                    '(personas_acreditadas.nombre_apellidos LIKE :p OR personas_acreditadas.ci LIKE :p)';
            }
            array_push($keys, ':p');
            array_push($values, "%$this->objeto%");
        }
        if ($this->homologacion != null) {
            if ($query != '') {
                $query = $query . ' AND homologacion = :h';
            } else {
                $query = 'homologacion = :h ';
            }
            array_push($keys, ':h');
            array_push($values, $this->homologacion);
        }
        if ($this->entidad != null) {
            $join =
                'inner join personas_acreditadas on persona = personas_acreditadas.id';
            if ($query != '') {
                $query = $query . ' AND (personas_acreditadas.entidad = :e) ';
            } else {
                $query = '(personas_acreditadas.entidad = :e)';
            }
            array_push($keys, ':e');
            array_push($values, $this->entidad);
        }
        if ($this->inspector != null) {
            $join = 'inner join personas_acreditadas on persona = personas_acreditadas.id
                    inner join inspecciones_x_personas on inspecciones_x_personas.persona = personas_acreditadas.id
                    inner join inspecciones on inspecciones.id = inspecciones_x_personas.inspeccion
                    inner join visitas_inspecciones on inspecciones.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            if ($query != '') {
                $query = $query . ' AND (funcionarios.codigo = :i) ';
            } else {
                $query = '(funcionarios.codigo = :i)';
            }
            array_push($keys, ':i');
            array_push($values, $this->inspector);
        }
        if ($this->mes != null || $this->anho != null) {
            $this->makeDate();
            if ($query != '') {
                $query = $query . ' AND (emision >= :ini AND emision <= :fin)';
            } else {
                $query = 'emision >= :ini AND emision <= :fin';
            }
            array_push($keys, ':ini');
            array_push($keys, ':fin');
            array_push($values, $this->inidate);
            array_push($values, $this->enddate);
        } else {
            if ($query != '') {
                $query = $query . ' AND vigente > :actual';
            } else {
                $query = 'vigente > :actual';
            }
            array_push($keys, ':actual');
            array_push($values, date('Y-m-d'));
        }
        $parameters = array_combine($keys, $values);
        if ($join != '') {
            $criteria->join = $join;
            $criteria->distinct = true;
        }
        $criteria->condition = $query;
        $criteria->params = $parameters;
        $criteria->order = 'vigente DESC';

        return $criteria;
    }

    public function execSearchServicios()
    {
        $criteria = new CDbCriteria();
        $query = '';
        $join = '';
        $keys = array();
        $values = array();
        if ($this->certificado != null) {
            $query = 'certificado LIKE :match ';
            array_push($keys, ':match');
            array_push($values, "%$this->certificado%");
        }
        if ($this->objeto != null) {
            $join =
                'inner join servicios_laboratorios_acreditados on servicio = servicios_laboratorios_acreditados.id';
            if ($query != '') {
                $query =
                    $query .
                    ' AND servicios_laboratorios_acreditados.servicio LIKE :p ';
            } else {
                $query = 'servicios_laboratorios_acreditados.servicio LIKE :p ';
            }
            array_push($keys, ':p');
            array_push($values, "%$this->objeto%");
        }
        if ($this->homologacion != null) {
            if ($query != '') {
                $query = $query . ' AND homologacion = :e';
            } else {
                $query = 'homologacion = :e ';
            }
            array_push($keys, ':e');
            array_push($values, $this->homologacion);
        }
        if ($this->entidad != null) {
            $join =
                'inner join servicios_laboratorios_acreditados on servicio = servicios_laboratorios_acreditados.id';
            if ($query != '') {
                $query =
                    $query .
                    ' AND (servicios_laboratorios_acreditados.entidad = :n) ';
            } else {
                $query = '(servicios_laboratorios_acreditados.entidad = :n)';
            }
            array_push($keys, ':n');
            array_push($values, $this->entidad);
        }
        if ($this->inspector != null) {
            $join = 'inner join servicios_laboratorios_acreditados on servicio = servicios_laboratorios_acreditados.id
                    inner join inspecciones_x_servicios_productos on inspecciones_x_servicios_productos.servcios = servicios_laboratorios_acreditados.id
                    inner join inspecciones on inspecciones.id = inspecciones_x_servicios_productos.inspeccion
                    inner join visitas_inspecciones on inspecciones.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            if ($query != '') {
                $query = $query . ' AND (funcionarios.codigo = :i) ';
            } else {
                $query = '(funcionarios.codigo = :i)';
            }
            array_push($keys, ':i');
            array_push($values, $this->inspector);
        }
        if ($this->mes != null || $this->anho != null) {
            $this->makeDate();
            if ($query != '') {
                $query =
                    $query .
                    ' AND emision >= :ini AND emision <= :fin AND .vigencia > :actual';
            } else {
                $query =
                    'emision >= :ini AND emision <= :fin AND .vigencia > :actual';
            }
            array_push($keys, ':ini');
            array_push($keys, ':fin');
            array_push($keys, ':actual');
            array_push($values, $this->inidate);
            array_push($values, $this->enddate);
            array_push($values, date('Y-m-d'));
        } else {
            if ($query != '') {
                $query = $query . ' AND vigencia > :actual';
            } else {
                $query = 'vigencia > :actual';
            }
            array_push($keys, ':actual');
            array_push($values, date('Y-m-d'));
        }
        $parameters = array_combine($keys, $values);
        if ($join != '') {
            $criteria->join = $join;
            $criteria->distinct = true;
        }
        $criteria->condition = $query;
        $criteria->params = $parameters;
        $criteria->order = 'vigencia DESC';

        return $criteria;
    }

    public function execSearchProductos()
    {
        $criteria = new CDbCriteria();
        $query = '';
        $join = '';
        $keys = array();
        $values = array();
        if ($this->certificado != null) {
            $query = 'certificado LIKE :match ';
            array_push($keys, ':match');
            array_push($values, "%$this->certificado%");
        }
        if ($this->objeto != null) {
            $join =
                'inner join productos_acreditados on producto = productos_acreditados.id';
            if ($query != '') {
                $query = $query . ' AND productos_acreditados.nombre LIKE :p ';
            } else {
                $query = 'productos_acreditados.nombre LIKE :p';
            }
            array_push($keys, ':p');
            array_push($values, "%$this->objeto%");
        }
        if ($this->homologacion != null) {
            if ($query != '') {
                $query = $query . ' AND homologacion = :e';
            } else {
                $query = 'homologacion = :e ';
            }
            array_push($keys, ':e');
            array_push($values, $this->homologacion);
        }
        if ($this->entidad != null) {
            $join =
                'inner join productos_acreditados on producto = productos_acreditados.id';
            if ($query != '') {
                $query =
                    $query . ' AND (productos_acreditados.propietario = :n) ';
            } else {
                $query = '(productos_acreditados.propietario = :n)';
            }
            array_push($keys, ':n');
            array_push($values, $this->entidad);
        }
        if ($this->inspector != null) {
            $join = 'inner join productos_acreditados on producto = productos_acreditados.id
                    inner join inspecciones_x_productos on inspecciones_x_productos.producto = productos_acreditados.id
                    inner join inspecciones on inspecciones.id = inspecciones_x_productos.inspeccion
                    inner join visitas_inspecciones on inspecciones.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            if ($query != '') {
                $query = $query . ' AND (funcionarios.codigo = :i) ';
            } else {
                $query = '(funcionarios.codigo = :i)';
            }
            array_push($keys, ':i');
            array_push($values, $this->inspector);
        }
        if ($this->mes != null || $this->anho != null) {
            $this->makeDate();
            if ($query != '') {
                $query =
                    $query .
                    ' AND emision >= :ini AND emision <= :fin AND vigencia > :actual';
            } else {
                $query =
                    'emision >= :ini AND emision <= :fin AND vigencia > :actual';
            }
            array_push($keys, ':ini');
            array_push($keys, ':fin');
            array_push($keys, ':actual');
            array_push($values, $this->inidate);
            array_push($values, $this->enddate);
            array_push($values, date('Y-m-d'));
        } else {
            if ($query != '') {
                $query = $query . ' AND vigencia > :actual';
            } else {
                $query = 'vigencia > :actual';
            }
            array_push($keys, ':actual');
            array_push($values, date('Y-m-d'));
        }
        $parameters = array_combine($keys, $values);
        if ($join != '') {
            $criteria->join = $join;
            $criteria->distinct = true;
        }
        $criteria->condition = $query;
        $criteria->params = $parameters;
        $criteria->order = 'vigencia DESC';

        return $criteria;
    }

    public function execSearchDAC()
    {
        $criteria = new CDbCriteria();
        $query = '';
        $join = '';
        $keys = array();
        $values = array();
        if ($this->certificado != null) {
            $query = 'certificado LIKE :match ';
            array_push($keys, ':match');
            array_push($values, "%$this->certificado%");
        }
        if ($this->objeto != null) {
            $join =
                'inner join dacs_acreditados on dac = dacs_acreditados.id';
            if ($query != '') {
                $query = $query . ' AND dacs_acreditados.nombre LIKE :p ';
            } else {
                $query = 'dacs_acreditados.nombre LIKE :p';
            }
            array_push($keys, ':p');
            array_push($values, "%$this->objeto%");
        }
        if ($this->homologacion != null) {
            if ($query != '') {
                $query = $query . ' AND homologacion = :e';
            } else {
                $query = 'homologacion = :e ';
            }
            array_push($keys, ':e');
            array_push($values, $this->homologacion);
        }
        if ($this->entidad != null) {
            $join =
                'inner join dacs_acreditados on dac = dacs_acreditados.id';
            if ($query != '') {
                $query = $query . ' AND (dacs_acreditados.propietario = :n) ';
            } else {
                $query = '(dacs_acreditados.propietario = :n)';
            }
            array_push($keys, ':n');
            array_push($values, $this->entidad);
        }
        if ($this->inspector != null) {
            $join = 'inner join dacs_acreditados on dac = dacs_acreditados.id
                    inner join inspecciones_x_dacs on inspecciones_x_dacs.dac = dacs_acreditados.id
                    inner join inspecciones on inspecciones.id = inspecciones_x_dacs.inspeccion
                    inner join visitas_inspecciones on inspecciones.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            if ($query != '') {
                $query = $query . ' AND (funcionarios.codigo = :i) ';
            } else {
                $query = '(funcionarios.codigo = :i)';
            }
            array_push($keys, ':i');
            array_push($values, $this->inspector);
        }
        if ($this->mes != null || $this->anho != null) {
            $this->makeDate();
            if ($query != '') {
                $query =
                    $query .
                    ' AND emision >= :ini AND emision <= :fin AND vigencia > :actual';
            } else {
                $query =
                    'emision >= :ini AND emision <= :fin AND vigencia > :actual';
            }
            array_push($keys, ':ini');
            array_push($keys, ':fin');
            array_push($keys, ':actual');
            array_push($values, $this->inidate);
            array_push($values, $this->enddate);
            array_push($values, date('Y-m-d'));
        } else {
            if ($query != '') {
                $query = $query . ' AND vigencia > :actual';
            } else {
                $query = 'vigencia > :actual';
            }
            array_push($keys, ':actual');
            array_push($values, date('Y-m-d'));
        }
        $parameters = array_combine($keys, $values);
        if ($join != '') {
            $criteria->join = $join;
            $criteria->distinct = true;
        }
        $criteria->condition = $query;
        $criteria->params = $parameters;
        $criteria->order = 'vigencia DESC';

        return $criteria;
    }

    public function execSearchMIT()
    {
        $criteria = new CDbCriteria();
        $query = '';
        $join = '';
        $keys = array();
        $values = array();
        if ($this->certificado != null) {
            $query = 'certificado LIKE :match ';
            array_push($keys, ':match');
            array_push($values, "%$this->certificado%");
        }
        if ($this->objeto != null) {
            $join =
                'inner join medios_izado_acreditados on medio_izaje = medios_izado_acreditados.id';
            if ($query != '') {
                $query =
                    $query . ' AND medios_izado_acreditados.nombre LIKE :p ';
            } else {
                $query = 'medios_izado_acreditados.nombre LIKE :p';
            }
            array_push($keys, ':p');
            array_push($values, "%$this->objeto%");
        }
        if ($this->entidad != null) {
            $join =
                'inner join medios_izado_acreditados on medio_izaje = medios_izado_acreditados.id';
            if ($query != '') {
                $query =
                    $query .
                    ' AND (medios_izado_acreditados.propietario = :n) ';
            } else {
                $query = '(medios_izado_acreditados.propietario = :n)';
            }
            array_push($keys, ':n');
            array_push($values, $this->entidad);
        }
        if ($this->inspector != null) {
            $join = 'inner join medios_izado_acreditados on medio_izaje = medios_izado_acreditados.id
                    inner join inspecciones_x_medios_izado on inspecciones_x_medios_izado.medio_izado = medios_izado_acreditados.id
                    inner join inspecciones on inspecciones.id = inspecciones_x_medios_izado.inspeccion
                    inner join visitas_inspecciones on inspecciones.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            if ($query != '') {
                $query = $query . ' AND (funcionarios.codigo = :i) ';
            } else {
                $query = '(funcionarios.codigo = :i)';
            }
            array_push($keys, ':i');
            array_push($values, $this->inspector);
        }
        if ($this->mes != null || $this->anho != null) {
            $this->makeDate();
            if ($query != '') {
                $query =
                    $query .
                    ' AND emision >= :ini AND emision <= :fin AND vigencia > :actual';
            } else {
                $query =
                    'emision >= :ini AND emision <= :fin AND vigencia > :actual';
            }
            array_push($keys, ':ini');
            array_push($keys, ':fin');
            array_push($keys, ':actual');
            array_push($values, $this->inidate);
            array_push($values, $this->enddate);
            array_push($values, date('Y-m-d'));
        } else {
            if ($query != '') {
                $query = $query . ' AND vigencia > :actual';
            } else {
                $query = 'vigencia > :actual';
            }
            array_push($keys, ':actual');
            array_push($values, date('Y-m-d'));
        }
        $parameters = array_combine($keys, $values);
        if ($join != '') {
            $criteria->join = $join;
            $criteria->distinct = true;
        }
        $criteria->condition = $query;
        $criteria->params = $parameters;
        $criteria->order = 'vigencia DESC';

        return $criteria;
    }

    public function execSearchEntidades()
    {
        $criteria = new CDbCriteria();
        $query = '';
        $keys = array();
        $values = array();
        if ($this->nombre != null) {
            $query = 'nombre LIKE :match ';
            array_push($keys, ':match');
            array_push($values, "%$this->nombre%");
        }
        if ($this->provincia != null) {
            if ($query != '') {
                $query = $query . ' AND provincia = :p ';
            } else {
                $query = 'provincia = :p';
            }
            array_push($keys, ':p');
            array_push($values, $this->provincia);
        }
        if ($this->nombre == null && $this->provincia == null) {
            $criteria->order = 'nombre ASC';
        } else {
            $parameters = array_combine($keys, $values);
            $criteria->condition = $query;
            $criteria->params = $parameters;
            $criteria->order = 'nombre ASC';
        }

        return $criteria;
    }

    public function execSearchModelPersonas()
    {
        $criteria = new CDbCriteria();
        $query = '';
        $keys = array();
        $values = array();
        if ($this->nombre != null) {
            $query = 'nombre_apellidos LIKE :match ';
            array_push($keys, ':match');
            array_push($values, "%$this->nombre%");
        }
        if ($this->ci != null) {
            if ($query != '') {
                $query = $query . ' AND ci = :c ';
            } else {
                $query = 'ci = :c';
            }
            array_push($keys, ':c');
            array_push($values, $this->ci);
        }
        if ($this->provincia != null) {
            if ($query != '') {
                $query = $query . ' AND provincia = :p ';
            } else {
                $query = 'provincia = :p';
            }
            array_push($keys, ':p');
            array_push($values, $this->provincia);
        }
        if ($this->entidad != null) {
            if ($query != '') {
                $query = $query . ' AND entidad = :e ';
            } else {
                $query = 'entidad = :e';
            }
            array_push($keys, ':e');
            array_push($values, $this->entidad);
        }
        if (
            $this->nombre == null &&
            $this->provincia == null &&
            $this->entidad == null &&
            $this->ci == null
        ) {
            $criteria->order = 'nombre_apellidos ASC';
        } else {
            $parameters = array_combine($keys, $values);
            $criteria->condition = $query;
            $criteria->params = $parameters;
            $criteria->order = 'nombre_apellidos ASC';
        }

        return $criteria;
    }

    public function execSearchModelServLab()
    {
        $criteria = new CDbCriteria();
        $query = '';
        $keys = array();
        $values = array();
        if ($this->servicio != null) {
            $query = 'servicio LIKE :match ';
            array_push($keys, ':match');
            array_push($values, "%$this->servicio%");
        }
        if ($this->departamento != null) {
            if ($query != '') {
                $query = $query . ' AND departamento LIKE :d ';
            } else {
                $query = 'departamento LIKE :d';
            }
            array_push($keys, ':d');
            array_push($values, "%$this->departamento%");
        }
        if ($this->provincia != null) {
            if ($query != '') {
                $query = $query . ' AND provincia = :p ';
            } else {
                $query = 'provincia = :p';
            }
            array_push($keys, ':p');
            array_push($values, $this->provincia);
        }
        if ($this->entidad != null) {
            if ($query != '') {
                $query = $query . ' AND entidad = :p ';
            } else {
                $query = 'entidad = :p';
            }
            array_push($keys, ':p');
            array_push($values, $this->entidad);
        }
        if (
            $this->servicio == null &&
            $this->departamento == null &&
            $this->entidad == null &&
            $this->provincia == null
        ) {
            $criteria->order = 'servicio ASC';
        } else {
            $parameters = array_combine($keys, $values);
            $criteria->condition = $query;
            $criteria->params = $parameters;
            $criteria->order = 'servicio ASC';
        }

        return $criteria;
    }

    public function execSearchModelProductos()
    {
        $criteria = new CDbCriteria();
        $query = '';
        $keys = array();
        $values = array();
        if ($this->nombre != null) {
            $query = 'nombre LIKE :match ';
            array_push($keys, ':match');
            array_push($values, "%$this->nombre%");
        }
        if ($this->provincia != null) {
            if ($query != '') {
                $query = $query . ' AND provincia = :p ';
            } else {
                $query = 'provincia = :p';
            }
            array_push($keys, ':p');
            array_push($values, $this->provincia);
        }
        if ($this->propietario != null) {
            if ($query != '') {
                $query = $query . ' AND propietario = :r ';
            } else {
                $query = 'propietario = :r';
            }
            array_push($keys, ':r');
            array_push($values, $this->propietario);
        }

        if (
            $this->nombre == null &&
            $this->provincia == null &&
            $this->propietario == null
        ) {
            $criteria->order = 'nombre ASC';
        } else {
            $parameters = array_combine($keys, $values);
            $criteria->condition = $query;
            $criteria->params = $parameters;
            $criteria->order = 'nombre ASC';
        }

        return $criteria;
    }

    public function execSearchModelDACs()
    {
        $criteria = new CDbCriteria();
        $query = '';
        $keys = array();
        $values = array();
        if ($this->nombre != null) {
            $query = 'nombre LIKE :match ';
            array_push($keys, ':match');
            array_push($values, "%$this->nombre%");
        }
        if ($this->provincia != null) {
            if ($query != '') {
                $query = $query . ' AND provincia = :p ';
            } else {
                $query = 'provincia = :p';
            }
            array_push($keys, ':p');
            array_push($values, $this->provincia);
        }
        if ($this->propietario != null) {
            if ($query != '') {
                $query = $query . ' AND propietario = :r ';
            } else {
                $query = 'propietario = :r';
            }
            array_push($keys, ':r');
            array_push($values, $this->propietario);
        }

        if (
            $this->nombre == null &&
            $this->provincia == null &&
            $this->propietario == null
        ) {
            $criteria->order = 'nombre ASC';
        } else {
            $parameters = array_combine($keys, $values);
            $criteria->condition = $query;
            $criteria->params = $parameters;
            $criteria->order = 'nombre ASC';
        }

        return $criteria;
    }

    public function execSearchTrazas()
    {
        $criteria = new CDbCriteria();
        $query = '';
        $keys = array();
        $values = array();
        if ($this->usuario != null) {
            $query = 'funcionario = :u ';
            array_push($keys, ':u');
            array_push($values, $this->usuario);
        }
        if ($this->ip != null) {
            if ($query != '') {
                $query = $query . ' AND ip LIKE :p ';
            } else {
                $query = 'ip LIKE :p';
            }
            array_push($keys, ':p');
            array_push($values, "%$this->ip%");
        }
        if ($this->accion != null) {
            if ($query != '') {
                $query = $query . ' AND detalle LIKE :e';
            } else {
                $query = 'detalle LIKE :e ';
            }
            array_push($keys, ':e');
            array_push($values, "%$this->accion%");
        }
        if ($this->inicio != null) {
            if ($query != '') {
                $query = $query . ' AND fecha >= :ini';
            } else {
                $query = 'fecha >= :ini ';
            }
            array_push($keys, ':ini');
            array_push($values, $this->inicio);
        }
        if ($this->final != null) {
            if ($query != '') {
                $query = $query . ' AND fecha <= :fin';
            } else {
                $query = 'fecha <= :fin ';
            }
            array_push($keys, ':fin');
            array_push($values, $this->final);
        }
        $parameters = array_combine($keys, $values);
        $criteria->condition = $query;
        $criteria->params = $parameters;
        $criteria->order = 'fecha DESC';

        return $criteria;
    }
}
