<?php

namespace app\forms;

use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 8/23/15
 * Time: 8:30 AM.
 *
 * Clase base de los formularios de inspeccion.
 */
abstract class InspeccionBaseForm extends Model
{
    /*
     * Atributos de la inspeccion
     */
    public $id;
    public $codigo;
    public $numero_control;
    public $oficina;
    public $entidad;
    public $lugar_inspeccion;
    public $homologacion; //acreditacion_industrial
    public $detalle_homologacion;
    public $nivel;
    public $tipo_homologacion;
    public $estado;
    public $certificado;
    public $vigencia;

    /*
     * Atributos de la visita de la inspeccion
     */
    public $numero_visita;
    public $fecha;
    public $descripcion;
    public $inspeccion;
    public $aprobado;
    public $inspectores_actuantes; //al menos uno debe estar acreditado para acreditar la homologacion en cuestion

    public function rules()
    {
        return array(
            array('numero_control, oficina, lugar_inspeccion, homologacion, tipo_homologacion, fecha, entidad', 'required'),
            array('inspectores_actuantes', 'validateExistInspectoresActuantes'),
            array('inspectores_actuantes', 'validateIsInspectorForThisHomologacion'),
        );
    }

    /*
     * Validacion para que exista a  public 'inspeccion' => nulll menos un inspector actuante
     */
    public function validateExistInspectoresActuantes($attribute, $params)
    {
        if ($this->inspectores_actuantes == null) {
            $this->addError($attribute, 'Seleccione al menos un inspector actuante.');
        }
    }

    /*
     * Validacion de los inspectores actuantes que garantiza que es un inspector hologado para poder acreditar
     * en la acreditacion en cuestion que se lleva a cabo.
     */
    public function validateIsInspectorForThisHomologacion($attribute, $params)
    {
        //($attribute == null || count($attribute) == 0 || count($attribute) == 1)
        if ($this->inspectores_actuantes == null) {
            $this->addError($attribute, 'Inspectores no puede ser nulo.');
        } else {
            $c = 0;

            foreach ($this->inspectores_actuantes as $idInspector) {
                $servicio = AcreditacionesIndustriales::model()->findByPk($this->homologacion);
                if($servicio != null){
                    $acreditacion = AcreditacionesInspectores::model()->find('acreditacion = :a AND inspector = :i',array(':a' => $servicio->categoriasAcreditaciones->id, ':i' => $idInspector));
                    if ($acreditacion != null && $acreditacion->vencimiento >= date('Y-m-d')) {
                        ++$c;
                    }
                }
            }
            if ($c == 0) {
                $this->addError($attribute,
                    'Debe existir al menos un inspector actuante que sea <strong>inspector y que est&eacute; debidamente acreditado</strong> para esta inspecci&oacute;n.'
                );
            }
        }
    }

    public function attributeLabels()
    {
        return array(
            'codigo' => 'C&oacute;digo',
            'numero_control' => 'N&uacute;mero de control',
            'oficina' => 'Dependencia',
            'entidad' => 'Entidad',
            'lugar_inspeccion' => 'Lugar de inspecci&oacute;n',
            'homologacion' => 'Tipo de homologaci&oacute;n',
            'detalle_homologacion' => 'Alcance',
            'nivel' => 'Nivel de aprobaci&oacute;n',
            'tipo_homologacion' => 'Tipo de inspecci&oacute;n',
            'estado' => 'Estado de la inspecci&oacute;n',
            'certificado' => 'Detalles del Tipo de Homologaci&oacute;n',
            'vigencia' => 'Vigencia',

            'numero_visita' => 'N&uacute;mero de visita',
            'fecha' => 'Fecha de visita',
            'descripcion' => 'Resultado de la Inspecci&oacute;n',
            'inspeccion' => 'Inspecci&oacute;n',
            'aprobado' => 'Satisfactoria / No Satisfactoria',
            'inspectores_actuantes' => 'Inspectores actuantes',
        );
    }
}
