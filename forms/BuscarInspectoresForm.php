<?php
namespace app\forms;

use yii;
use yii\base\Model;

class BuscarInspectoresForm extends Model
{
    public $oficina;
    public $servicio;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            array('servicio', 'required'),
            array('servicio, oficina', 'length', 'max' => 500)
        );
    }

    public function attributeLabels()
    {
        return array(
            'oficina' => 'Oficina',
            'servicio' => 'Servicio Homologado'
        );
    }
}
