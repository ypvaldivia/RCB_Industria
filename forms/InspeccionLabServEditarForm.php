<?php

namespace app\forms;

use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 8/27/15
 * Time: 1:25 PM.
 */
class InspeccionLabServEditarForm extends InspeccionLabServForm
{
    public function rules()
    {
        return array(
            array('numero_control, oficina, entidad, homologacion', 'required'),
        );
    }
}
