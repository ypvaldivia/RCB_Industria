<?php

namespace app\forms;

use yii;
use yii\base\Model;

class CertificadoProdServDefaultForm extends Model
{
    public $inspeccion;
    public $objeto;
    public $documentacion;
    public $servicio;
    public $expedicion;
    public $vencimiento;
    public $storeEmision;
    public $storeValidez;
    public $funcionario;
    public $control;
    public $nivelAprobacion;

    public function rules()
    {
        return array(
            array('documentacion, servicio, expedicion, vencimiento, funcionario', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'documentacion' => 'Documentaci&oacute;n (Regla, Procedimiento, Instrucci&oacute;n, Cap&iacute;tulo del MAPS u otros)',
            'servicio' => 'Servicio',
            'expedicion' => 'Fecha de Expedici&oacute;n',
            'vencimiento' => 'Fecha de Vencimiento',
            'funcionario' => 'Por el RCB',
        );
    }

    public function setMyAttributes($form, $obj, $tipo)
    {
        $this->documentacion = (isset($form['documentacion'])) ? $form['documentacion'] : null;
        $this->servicio = (isset($form['servicio'])) ? $form['servicio'] : null;
        $this->expedicion = (isset($form['expedicion'])) ? $form['expedicion'] : null;
        $this->vencimiento = (isset($form['vencimiento'])) ? $form['vencimiento'] : null;
        $this->funcionario = (isset($form['funcionario'])) ? $form['funcionario'] : null;
        $this->inspeccion = $obj->inspeccion0;
        if ($tipo == 2) {
            $this->objeto = $obj->servcios0;
        } elseif ($tipo == 3) {
            $this->objeto = $obj->producto0;
        }
        $this->control = 1;
    }
}
