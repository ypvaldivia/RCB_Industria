<?php

namespace app\forms;

use app\models\AcreditacionesIndustriales;
use app\models\InspectoresActuantes;
use app\repositories\AcreditacionesInspectoresRepository;
use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 8/25/15
 * Time: 10:59 AM.
 */
class VisitaInspeccionForm extends InspeccionBaseForm
{
    public function rules()
    {
        return array(
            array('fecha', 'required'),
            array('inspectores_actuantes', 'validateExistInspectoresActuantes'),
            array('inspectores_actuantes', 'validateIsInspectorForThisHomologacion'),
            array('fecha, aprobado', 'safe', 'on' => 'search'),
        );
    }

    /*
     * Validacion de los inspectores actuantes que garantiza que es un inspector hologado para poder acreditar
     * en la acreditacion en cuestion que se lleva a cabo.
     */
    public function validateIsInspectorForThisHomologacion($attribute, $params)
    {
        //($attribute == null || count($attribute) == 0 || count($attribute) == 1)
        if ($this->inspectores_actuantes == null) {
            $this->addError($attribute, 'Inspectores no puede ser nulo.');
        } else {
            $c = 0;
            foreach ($this->inspectores_actuantes as $idInspector) {
                $homologacion = AcreditacionesIndustriales::findOne($this->homologacion);
                if ($homologacion->categoria != null) {
                    $acreditacion = AcreditacionesInspectoresRepository::findOne(array('acreditacion' => $homologacion->categoria, 'inspector' => $idInspector));
                    if ($acreditacion != null && $acreditacion->vencimiento >= date('Y-m-d')) {
                        ++$c;
                    }
                }
            }
            if ($c == 0) {
                $this->addError(
                    $attribute,
                    'Debe existir al menos un inspector actuante que sea <strong>inspector y que est&eacute; debidamente acreditado</strong> para esta inspecci&oacute;n.: ' . $homologacion->categoria
                );
            }
        }
    }

    /*
     * Asigna a cada atributo el valor que fue insertado en el formulario. La variable $form que entra al metodo
     * como parametro es la variable global $_POST['nombre_del_formulario'].
     */
    public function setMyAttributes($form)
    {
        if (isset($form['numero_visita'])) {
            $this->numero_visita = $form['numero_visita'];
        }
        if (isset($form['fecha'])) {
            $this->fecha = $form['fecha'];
        }
        if (isset($form['descripcion'])) {
            $this->descripcion = $form['descripcion'];
        }
        if (isset($form['inspeccion'])) {
            $this->inspeccion = $form['inspeccion'];
        }
        if (isset($form['aprobado'])) {
            $this->aprobado = $form['aprobado'];
        }
        if (isset($form['inspectores_actuantes'])) {
            $this->inspectores_actuantes = $form['inspectores_actuantes'];
        }
        if (isset($form['lugar_inspeccion'])) {
            $this->lugar_inspeccion = $form['lugar_inspeccion'];
        }
    }

    /*
 * Asigna a cada atributo el valor que fue insertado en el formulario. La variable $form que entra al metodo
 * como parametro es la variable global $_POST['nombre_del_formulario'].
 */
    public function setMyAttributesByModel($model)
    {
        $this->id = $model->id;
        $this->numero_visita = $model->numero_visita;
        $this->fecha = $model->fecha;
        $this->descripcion = $model->descripcion;
        $this->inspeccion = $model->inspeccion;
        $this->aprobado = $model->aprobado;
        $this->inspectores_actuantes = $model->inspectoresActuantes;
        $this->lugar_inspeccion = $model->inspeccion0->lugar_inspeccion;
    }

    /*
 * Establece un arreglo para indicar los laboratorios que han sido seleccionados para inspeccion en caso de
 * que se necesite editar la inspeccion.
 */
    public function getInspectoresActuantes()
    {
        $select = array();
        if (isset($this->inspectores_actuantes)) {
            foreach ($this->inspectores_actuantes as $ia) {
                if (is_object($ia)) {
                    if ($ia->visita == $this->id) {
                        $select[$ia->inspector] = array('selected' => true);
                    }
                } else {
                    $inspector = InspectoresActuantes::findOne($ia);
                    if ($inspector->visita == $this->id) {
                        $select[$inspector->inspector] = array('selected' => true);
                    }
                }
            }
        }

        return $select;
    }
}
