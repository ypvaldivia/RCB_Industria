<?php

namespace app\forms;

use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: yoyito
 * Date: 16/02/2016
 * Time: 10:15.
 */
class BannerForm extends Model
{
    public $imagen;

    public function rules()
    {
        return array(
            array('imagen', 'required'),
            array(
                'imagen',
                'file',
                'types' => 'jpg,jpeg',
                'maxSize' => 756 * 155 * 2,
                'tooLarge' => 'El tama&ntilde;o debe ser inferior a 2MB !!!',
                'on' => 'upload'
            )
        );
    }

    public function attributeLabels()
    {
        return array(
            'imagen' => Yii::t('app', 'Imagen')
        );
    }
}
