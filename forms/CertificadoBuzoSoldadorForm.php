<?php

namespace app\forms;

use app\models\Inspecciones;
use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 10/4/15
 * Time: 6:07 PM.
 */
class CertificadoBuzoSoldadorForm extends Model
{
    public $id; //id de la inspeccion
    public $control;
    public $funcionario;

    public $numero;
    public $nombre;
    public $fecha;
    public $lugarNacimiento;
    public $carnet;
    public $procedimientoSoldadura;
    public $empresa;
    public $resultadoExamenTeorico; //aprobado o No requeredido
    public $resultadoExamenPractico;
    public $nivelAprobacion;
    //Datos tecnicos.
    public $procesoSoldadura_detalles;
    public $procesoSoldadura_rango;
    public $chapaTubo_detalles;
    public $chapaTubo_rango;
    public $tipoUnion_detalles;
    public $tipoUnion_rango;
    public $grupoMetalBase_detalles;
    public $grupoMetalBase_rango;
    public $tipoMetal_detalles;
    public $tipoMetal_rango;
    public $revestimiento_detalles;
    public $revestimiento_rango;
    public $espesor_detalles;
    public $espesor_rango;
    public $diametro_detalles;
    public $diametro_rango;
    public $posicion_detalles;
    public $posicion_rango;
    public $resanado_detalles;
    public $resanado_rango;
    public $profundidad_detalles;
    public $profundidad_rango;
    public $visibilidad_detalles;
    public $visibilidad_rango;
    public $salinidad_detalles;
    public $salinidad_rango;
    public $auxiliares_detalles;
    public $auxiliares_rango;
    //Fechas
    public $emision;
    public $validez;
    public $storeEmision;
    public $storeValidez;
    //Ensayos que avalan el certificado.
    public $visual;
    public $radiograficoUltrasonico;
    public $particulasMagneticas;
    public $liquidosPenetrantes;
    public $macrografia;
    public $fractura;
    public $doblado;
    public $otros;
    //Simbologia
    public $simbologia;

    public function rules()
    {
        return array(
            array('numero, fecha, carnet, procedimientoSoldadura,
            resultadoExamenTeorico', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'numero' => 'No.',
            'fecha' => 'Fecha de nacimiento',
            'lugarNacimiento' => 'Lugar de nacimiento',
            'carnet' => 'No. de carnet de indentidad',
            'procedimientoSoldadura' => 'Procedimiento de soldadura',
            'empresa' => 'Empresa',
            'funcionario' => 'Inspector del RCB',
            'resultadoExamenTeorico' => 'Resultados del examen teórico (Approbado/No requerido)',
            'resultadoExamenPractico' => 'Resultados del examen práctico',
            'simbologia' => '',
        );
    }

    public function initForm($inspXper)
    {
        $inspeccion = Inspecciones::findOne($inspXper->inspeccion0->id);
        $this->nombre = $inspXper->persona0->nombre_apellidos;
        $this->carnet = $inspXper->persona0->ci;
        $this->empresa = $inspeccion->entidad0->nombre;
        $this->lugarNacimiento = $inspXper->persona0->lugar_nacimiento;
        $this->id = $inspeccion->id;
        $this->fecha = $this->setFechaNacimiento($this->carnet);
    }

    private function setFechaNacimiento($ci)
    {
        $date = substr($ci, 0, 6);
        $m = '';
        $month = substr($ci, 2, 2);
        switch ($month) {
            case 01://id = 1 buzo soldador
                $m = 'enero';
                break;
            case 2://id = 2 el de soldadores en acero
                $m = 'febrero';
                break;
            case 3://id = 3 el de inspector de soldadura
                $m = 'marzo';
                break;
            case 4://id = 3 el de inspector de soldadura
                $m = 'abril';
                break;
            case 5://id = 3 el de inspector de soldadura
                $m = 'mayo';
                break;
            case 6://id = 3 el de inspector de soldadura
                $m = 'junio';
                break;
            case 7://id = 3 el de inspector de soldadura
                $m = 'julio';
                break;
            case 8://id = 3 el de inspector de soldadura
                $m = 'agosto';
                break;
            case 9://id = 3 el de inspector de soldadura
                $m = 'septimebre';
                break;
            case 10://id = 3 el de inspector de soldadura
                $m = 'octubre';
                break;
            case 11://id = 3 el de inspector de soldadura
                $m = 'noviembre';
                break;
            case 12://id = 3 el de inspector de soldadura
                $m = 'diciembre';
                break;
        }

        return substr($date, 4, 6).' de '.$m.' del '.substr($date, 0, 2);
    }

    public function setMyAttributes($form)
    {
        //$this->numero = (isset($form['numero'])) ? $form['numero'] : null;
        $this->funcionario = (isset($form['funcionario'])) ? $form['funcionario'] : null;
        $this->validez = (isset($form['validez'])) ? $form['validez'] : null;
        $this->emision = (isset($form['emision'])) ? $form['emision'] : null;
        //$this->lugarNacimiento = (isset($form['lugarNacimiento'])) ? $form['lugarNacimiento'] : null;
        //$this->carnet = (isset($form['carnet'])) ? $form['carnet'] : null;
        $this->procedimientoSoldadura = (isset($form['procedimientoSoldadura'])) ? $form['procedimientoSoldadura'] : null;
        //$this->empresa = (isset($form['empresa'])) ? $form['empresa'] : null;
        $this->resultadoExamenTeorico = (isset($form['resultadoExamenTeorico'])) ? $form['resultadoExamenTeorico'] : null;
        $this->resultadoExamenPractico = (isset($form['resultadoExamenPractico'])) ? $form['resultadoExamenPractico'] : null;
        //Datos tecnicos.
        $this->procesoSoldadura_detalles = (isset($form['procesoSoldadura_detalles']) && ($form['procesoSoldadura_detalles']) != '') ? $form['procesoSoldadura_detalles'] : '--';
        $this->procesoSoldadura_rango = (isset($form['procesoSoldadura_rango']) && ($form['procesoSoldadura_rango']) != '') ? $form['procesoSoldadura_rango'] : '--';
        $this->chapaTubo_detalles = (isset($form['chapaTubo_detalles']) && ($form['chapaTubo_detalles']) != '') ? $form['chapaTubo_detalles'] : '--';
        $this->chapaTubo_rango = (isset($form['chapaTubo_rango']) && ($form['chapaTubo_rango']) != '') ? $form['chapaTubo_rango'] : '--';
        $this->tipoUnion_detalles = (isset($form['tipoUnion_detalles']) && ($form['tipoUnion_detalles']) != '') ? $form['tipoUnion_detalles'] : '--';
        $this->tipoUnion_rango = (isset($form['tipoUnion_rango']) && ($form['tipoUnion_rango']) != '') ? $form['tipoUnion_rango'] : '--';
        $this->grupoMetalBase_detalles = (isset($form['grupoMetalBase_detalles']) && ($form['grupoMetalBase_detalles']) != '') ? $form['grupoMetalBase_detalles'] : '--';
        $this->grupoMetalBase_rango = (isset($form['grupoMetalBase_rango']) && ($form['grupoMetalBase_rango']) != '') ? $form['grupoMetalBase_rango'] : '--';
        $this->tipoMetal_detalles = (isset($form['tipoMetal_detalles']) && ($form['tipoMetal_detalles']) != '') ? $form['tipoMetal_detalles'] : '--';
        $this->tipoMetal_rango = (isset($form['tipoMetal_rango']) && ($form['tipoMetal_rango']) != '') ? $form['tipoMetal_rango'] : '--';
        $this->revestimiento_detalles = (isset($form['revestimiento_detalles']) && ($form['revestimiento_detalles']) != '') ? $form['revestimiento_detalles'] : '--';
        $this->revestimiento_rango = (isset($form['revestimiento_rango']) && ($form['revestimiento_rango']) != '') ? $form['revestimiento_rango'] : '--';
        $this->espesor_detalles = (isset($form['espesor_detalles']) && ($form['espesor_detalles']) != '') ? $form['espesor_detalles'] : '--';
        $this->espesor_rango = (isset($form['espesor_rango']) && ($form['espesor_rango']) != '') ? $form['espesor_rango'] : '--';
        $this->diametro_detalles = (isset($form['diametro_detalles']) && ($form['diametro_detalles']) != '') ? $form['diametro_detalles'] : '--';
        $this->diametro_rango = (isset($form['diametro_rango']) && ($form['diametro_rango']) != '') ? $form['diametro_rango'] : '--';
        $this->posicion_detalles = (isset($form['posicion_detalles']) && ($form['posicion_detalles']) != '') ? $form['posicion_detalles'] : '--';
        $this->posicion_rango = (isset($form['posicion_rango']) && ($form['posicion_rango']) != '') ? $form['posicion_rango'] : '--';
        $this->resanado_detalles = (isset($form['resanado_detalles']) && ($form['resanado_detalles']) != '') ? $form['resanado_detalles'] : '--';
        $this->resanado_rango = (isset($form['resanado_rango']) && ($form['resanado_rango']) != '') ? $form['resanado_rango'] : '--';
        $this->profundidad_detalles = (isset($form['profundidad_detalles']) && ($form['profundidad_detalles']) != '') ? $form['profundidad_detalles'] : '--';
        $this->profundidad_rango = (isset($form['profundidad_rango']) && ($form['profundidad_rango']) != '') ? $form['profundidad_rango'] : '--';
        $this->visibilidad_detalles = (isset($form['visibilidad_detalles']) && ($form['visibilidad_detalles']) != '') ? $form['visibilidad_detalles'] : '--';
        $this->visibilidad_rango = (isset($form['visibilidad_rango']) && ($form['visibilidad_rango']) != '') ? $form['visibilidad_rango'] : '--';
        $this->salinidad_detalles = (isset($form['salinidad_detalles']) && ($form['salinidad_detalles']) != '') ? $form['salinidad_detalles'] : '--';
        $this->salinidad_rango = (isset($form['salinidad_rango']) && ($form['salinidad_rango']) != '') ? $form['salinidad_rango'] : '--';
        $this->auxiliares_detalles = (isset($form['auxiliares_detalles']) && ($form['auxiliares_detalles']) != '') ? $form['auxiliares_detalles'] : '--';
        $this->auxiliares_rango = (isset($form['auxiliares_rango']) && ($form['auxiliares_rango']) != '') ? $form['auxiliares_rango'] : '--';
        //Ensayos que avalan el certificado.
        $this->visual = (isset($form['visual']) && ($form['visual']) != '') ? $form['visual'] : '--';
        $this->radiograficoUltrasonico = (isset($form['radiograficoUltrasonico']) && ($form['radiograficoUltrasonico']) != '') ? $form['radiograficoUltrasonico'] : '--';
        $this->particulasMagneticas = (isset($form['particulasMagneticas']) && ($form['particulasMagneticas']) != '') ? $form['particulasMagneticas'] : '--';
        $this->liquidosPenetrantes = (isset($form['liquidosPenetrantes']) && ($form['liquidosPenetrantes']) != '') ? $form['liquidosPenetrantes'] : '--';
        $this->macrografia = (isset($form['macrografia']) && ($form['macrografia']) != '') ? $form['macrografia'] : '--';
        $this->fractura = (isset($form['fractura']) && ($form['fractura']) != '') ? $form['fractura'] : '--';
        $this->doblado = (isset($form['doblado']) && ($form['doblado']) != '') ? $form['doblado'] : '--';
        $this->otros = (isset($form['otros']) && ($form['otros']) != '') ? $form['otros'] : '--';
        //Simbologia
        $this->simbologia = (isset($form['simbologia']) && ($form['simbologia']) != '') ? $form['simbologia'] : '--';
    }
}
