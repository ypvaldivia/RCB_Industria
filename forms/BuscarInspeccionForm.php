<?php

namespace app\forms;

use app\components\CDbCriteria;
use app\models\AcreditacionesIndustriales;
use app\repositories\AcreditacionesIndustrialesRepository;
use yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 10/14/15
 * Time: 4:08 PM.
 */
class BuscarInspeccionForm extends Model
{
    public $homologacion, $objeto, $control, $estado, $obj, $inspector;

    public function setMyAttributes($post)
    {
        $this->homologacion = isset($post['homologacion'])
            ? $post['homologacion']
            : null;
        $this->objeto = isset($post['objeto']) ? $post['objeto'] : null;
        $this->control = isset($post['control']) ? $post['control'] : null;
        $this->estado = isset($post['estado']) ? $post['estado'] : null;
        $this->inspector = isset($post['inspector'])
            ? $post['inspector']
            : null;

        if ($this->objeto != null || $this->objeto != '') {
            $this->obj = true;
        } else {
            $this->obj = null;
        }
    }

    public function buscarObjetoPersona($post)
    {
        $criteria = new CDbCriteria();
        $query = '';
        $join = '';
        $keys = array();
        $values = array();

        if ($post['objeto'] != null) {
            $tmp = $post['objeto'];
            $join = 'inner join inspecciones_x_personas on t.id = inspecciones_x_personas.inspeccion
                    inner join personas_acreditadas on inspecciones_x_personas.persona = personas_acreditadas.id';
            $query =
                'personas_acreditadas.nombre_apellidos LIKE :o OR personas_acreditadas.ci LIKE :o';
            array_push($keys, ':o');
            array_push($values, "%$tmp%");
        }
        if ($post['control'] != null) {
            if ($join == '') {
                $join =
                    'inner join inspecciones_x_personas on t.id = inspecciones_x_personas.inspeccion';
            }
            if ($query != '') {
                $query = $query . ' AND t.numero_control LIKE :numero_control ';
            } else {
                $query = 't.numero_control LIKE :numero_control';
            }
            array_push($keys, ':numero_control');
            $tmp2 = $post['control'];
            array_push($values, "%$tmp2%");
        }

        if (isset($post['homologacion']) && $post['homologacion'] != '') {
            if ($join == '') {
                $join =
                    'inner join inspecciones_x_personas on t.id = inspecciones_x_personas.inspeccion';
            }
            if ($query != '') {
                $query = $query . ' AND homologacion=:homologacion';
            } else {
                $query = 'homologacion=:homologacion';
            }
            array_push($keys, ':homologacion');
            array_push($values, $post['homologacion']);
        }

        if (isset($post['inspector']) && $post['inspector'] != '') {
            if ($join == '') {
                $join = 'inner join inspecciones_x_personas on t.id = inspecciones_x_personas.inspeccion
                    inner join visitas_inspecciones on t.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            } else {
                $join =
                    $join .
                    ' inner join visitas_inspecciones on t.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            }
            if ($query != '') {
                $query = $query . ' AND funcionarios.codigo=:inspector';
            } else {
                $query = 'funcionarios.codigo=:inspector';
            }
            array_push($keys, ':inspector');
            array_push($values, $post['inspector']);
        }

        $parameters = array_combine($keys, $values);
        if ($join != '') {
            $criteria->join = $join;
            $criteria->distinct = true;
        }
        $criteria->condition = $query;
        $criteria->params = $parameters;
        $criteria->order = 't.id desc';

        return $criteria;
    }

    public function buscarObjetoServicio($post)
    {
        $criteria = new CDbCriteria();
        $query = '';
        $join = '';
        $keys = array();
        $values = array();

        if ($post['objeto'] != null) {
            $tmp = $post['objeto'];
            $join = 'inner join inspecciones_x_servicios_productos on t.id = inspecciones_x_servicios_productos.inspeccion
                    inner join servicios_laboratorios_acreditados on inspecciones_x_servicios_productos.servcios = servicios_laboratorios_acreditados.id';
            $query = 'servicios_laboratorios_acreditados.servicio LIKE :o';
            array_push($keys, ':o');
            array_push($values, "%$tmp%");
        }
        if ($post['control'] != null) {
            if ($join == '') {
                $join =
                    'inner join inspecciones_x_servicios_productos on t.id = inspecciones_x_servicios_productos.inspeccion';
            }
            if ($query != '') {
                $query = $query . ' AND t.numero_control LIKE :numero_control ';
            } else {
                $query = 't.numero_control LIKE :numero_control';
            }
            array_push($keys, ':numero_control');
            $tmp2 = $post['control'];
            array_push($values, "%$tmp2%");
        }

        if (isset($post['homologacion']) && $post['homologacion'] != '') {
            if ($join == '') {
                $join =
                    'inner join inspecciones_x_servicios_productos on t.id = inspecciones_x_servicios_productos.inspeccion';
            }
            if ($query != '') {
                $query = $query . ' AND homologacion=:homologacion';
            } else {
                $query = 'homologacion=:homologacion';
            }
            array_push($keys, ':homologacion');
            array_push($values, $post['homologacion']);
        }

        if (isset($post['inspector']) && $post['inspector'] != '') {
            if ($join == '') {
                $join = 'inner join inspecciones_x_servicios_productos on t.id = inspecciones_x_servicios_productos.inspeccion
                    inner join visitas_inspecciones on t.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            } else {
                $join =
                    $join .
                    ' inner join visitas_inspecciones on t.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            }
            if ($query != '') {
                $query = $query . ' AND funcionarios.codigo=:inspector';
            } else {
                $query = 'funcionarios.codigo=:inspector';
            }
            array_push($keys, ':inspector');
            array_push($values, $post['inspector']);
        }

        $parameters = array_combine($keys, $values);
        if ($join != '') {
            $criteria->join = $join;
            $criteria->distinct = true;
        }
        $criteria->condition = $query;
        $criteria->params = $parameters;
        $criteria->order = 't.id desc';

        return $criteria;
    }

    public function buscarObjetoProducto($post)
    {
        $criteria = new CDbCriteria();
        $query = '';
        $join = '';
        $keys = array();
        $values = array();

        if ($post['objeto'] != null) {
            $tmp = $post['objeto'];
            $join = 'inner join inspecciones_x_productos on t.id = inspecciones_x_productos.inspeccion
                    inner join productos_acreditados on inspecciones_x_productos.producto = productos_acreditados.id';
            $query = 'productos_acreditados.nombre LIKE :o';
            array_push($keys, ':o');
            array_push($values, "%$tmp%");
        }
        if ($post['control'] != null) {
            if ($join == '') {
                $join =
                    'inner join inspecciones_x_productos on t.id = inspecciones_x_productos.inspeccion';
            }
            if ($query != '') {
                $query = $query . ' AND t.numero_control LIKE :numero_control ';
            } else {
                $query = 't.numero_control LIKE :numero_control';
            }
            array_push($keys, ':numero_control');
            $tmp2 = $post['control'];
            array_push($values, "%$tmp2%");
        }

        if (isset($post['homologacion']) && $post['homologacion'] != '') {
            if ($join == '') {
                $join =
                    'inner join productos_acreditados on t.id = productos_acreditados.inspeccion';
            }
            if ($query != '') {
                $query = $query . ' AND homologacion=:homologacion';
            } else {
                $query = 'homologacion=:homologacion';
            }
            array_push($keys, ':homologacion');
            array_push($values, $post['homologacion']);
        }

        if (isset($post['inspector']) && $post['inspector'] != '') {
            if ($join == '') {
                $join = 'inner join productos_acreditados on t.id = productos_acreditados.inspeccion
                    inner join visitas_inspecciones on t.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            } else {
                $join =
                    $join .
                    ' inner join visitas_inspecciones on t.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            }
            if ($query != '') {
                $query = $query . ' AND funcionarios.codigo=:inspector';
            } else {
                $query = 'funcionarios.codigo=:inspector';
            }
            array_push($keys, ':inspector');
            array_push($values, $post['inspector']);
        }

        $parameters = array_combine($keys, $values);
        if ($join != '') {
            $criteria->join = $join;
            $criteria->distinct = true;
        }
        $criteria->condition = $query;
        $criteria->params = $parameters;
        $criteria->order = 't.id desc';

        return $criteria;
    }

    public function buscarProducto($post)
    {
        $cond = 't.id = inspeccionesXProductoses.inspeccion';
        $param = array();
        $criteria = new CDbCriteria();

        if (isset($post['homologacion']) && $post['homologacion'] != '') {
            if ($cond != '') {
                $cond = $cond . ' AND homologacion=:homologacion';
            } else {
                $cond = 'homologacion=:homologacion';
            }
            $param[':homologacion'] = $post['homologacion'];
        }

        if (isset($post['control']) && $post['control'] != '') {
            if ($cond != '') {
                $cond = $cond . ' AND numero_control=:numero_control';
            } else {
                $cond = 'numero_control=:numero_control';
            }
            $param[':numero_control'] = $post['control'];
        }

        if (isset($post['estado']) && $post['estado'] != '') {
            if ($cond != '') {
                $cond = $cond . ' AND estado=:estado';
            } else {
                $cond = 'estado=:estado';
            }
            $param[':estado'] = $post['estado'];
        }

        $criteria->condition = $cond;
        $criteria->params = $param;
        $criteria->order = 't.id desc';

        return $criteria;
    }

    public function buscarObjetoDAC($post)
    {
        $criteria = new CDbCriteria();
        $query = '';
        $join = '';
        $keys = array();
        $values = array();

        if ($post['objeto'] != null) {
            $tmp = $post['objeto'];
            $join = 'inner join inspecciones_x_dacs on t.id = inspecciones_x_dacs.inspeccion
                    inner join dacs_acreditados on inspecciones_x_dacs.dac = dacs_acreditados.id';
            $query = 'dacs_acreditados.nombre LIKE :o';
            array_push($keys, ':o');
            array_push($values, "%$tmp%");
        }
        if ($post['control'] != null) {
            if ($join == '') {
                $join =
                    'inner join inspecciones_x_dacs on t.id = inspecciones_x_dacs.inspeccion';
            }
            if ($query != '') {
                $query = $query . ' AND t.numero_control LIKE :numero_control ';
            } else {
                $query = 't.numero_control LIKE :numero_control';
            }
            array_push($keys, ':numero_control');
            $tmp2 = $post['control'];
            array_push($values, "%$tmp2%");
        }
        $homologacion = AcreditacionesIndustriales::findOne(
            'tipo = :t',
            array(':t' => 4)
        );
        if ($homologacion != null) {
            if ($query != '') {
                $query = $query . ' AND t.homologacion=:homologacion ';
            } else {
                $query = 't.homologacion=:homologacion';
            }
            array_push($keys, ':homologacion');
            array_push($values, $homologacion->id);
        }

        if (isset($post['inspector']) && $post['inspector'] != '') {
            if ($join == '') {
                $join = 'inner join inspecciones_x_dacs on t.id = inspecciones_x_dacs.inspeccion
                    inner join visitas_inspecciones on t.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            } else {
                $join =
                    $join .
                    ' inner join visitas_inspecciones on t.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            }
            if ($query != '') {
                $query = $query . ' AND funcionarios.codigo=:inspector';
            } else {
                $query = 'funcionarios.codigo=:inspector';
            }
            array_push($keys, ':inspector');
            array_push($values, $post['inspector']);
        }

        $parameters = array_combine($keys, $values);
        if ($join != '') {
            $criteria->join = $join;
            $criteria->distinct = true;
        }
        $criteria->condition = $query;
        $criteria->params = $parameters;
        $criteria->order = 't.id desc';

        return $criteria;
    }

    public function buscarObjetoMIT($post)
    {
        $criteria = new CDbCriteria();
        $query = '';
        $join = '';
        $keys = array();
        $values = array();

        if ($post['objeto'] != null) {
            $tmp = $post['objeto'];
            $join = 'inner join inspecciones_x_medios_izado on t.id = inspecciones_x_medios_izado.inspeccion
                    inner join medios_izado_acreditados on inspecciones_x_medios_izado.medio_izado = medios_izado_acreditados.id';
            $query = 'medios_izado_acreditados.nombre LIKE :o';
            array_push($keys, ':o');
            array_push($values, "%$tmp%");
        }
        if ($post['control'] != null) {
            if ($join == '') {
                $join =
                    'inner join inspecciones_x_medios_izado on t.id = inspecciones_x_medios_izado.inspeccion';
            }
            if ($query != '') {
                $query = $query . ' AND t.numero_control LIKE :numero_control ';
            } else {
                $query = 't.numero_control LIKE :numero_control';
            }
            array_push($keys, ':numero_control');
            $tmp2 = $post['control'];
            array_push($values, "%$tmp2%");
        }
        $homologacion = AcreditacionesIndustriales::findOne(
            'tipo = :t',
            array(':t' => 4)
        );
        if ($homologacion != null) {
            if ($query != '') {
                $query = $query . ' AND t.homologacion=:homologacion ';
            } else {
                $query = 't.homologacion=:homologacion';
            }
            array_push($keys, ':homologacion');
            array_push($values, $homologacion->id);
        }

        if (isset($post['inspector']) && $post['inspector'] != '') {
            if ($join == '') {
                $join = 'inner join inspecciones_x_medios_izado on t.id = inspecciones_x_medios_izado.inspeccion
                    inner join visitas_inspecciones on t.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            } else {
                $join =
                    $join .
                    ' inner join visitas_inspecciones on t.id = visitas_inspecciones.inspeccion
                    inner join inspectores_actuantes on visitas_inspecciones.id = inspectores_actuantes.visita
                    inner join funcionarios on inspectores_actuantes.inspector = funcionarios.id';
            }
            if ($query != '') {
                $query = $query . ' AND funcionarios.codigo=:inspector';
            } else {
                $query = 'funcionarios.codigo=:inspector';
            }
            array_push($keys, ':inspector');
            array_push($values, $post['inspector']);
        }

        $parameters = array_combine($keys, $values);
        if ($join != '') {
            $criteria->join = $join;
            $criteria->distinct = true;
        }
        $criteria->condition = $query;
        $criteria->params = $parameters;
        $criteria->order = 't.id desc';

        return $criteria;
    }

    /*
     * tipo = array(0=>'Personas', 1=>'Servicios', 2=>'Productos');
     *
     * Declarado en la clase modelo de AcreditacionesIndustriales
     */
    public function getHomologaciones($objeto)
    {
        switch ($objeto) {
            case 'Personas':
                $result = AcreditacionesIndustrialesRepository::findAll(
                    array('tipo' => 0)
                );
                $data = ArrayHelper::map($result, 'id', 'titulo_inspector');
                break;
            case 'Servicios':
                $result = AcreditacionesIndustriales::findAll(
                    array('tipo' => 1)
                );
                $data = ArrayHelper::map($result, 'id', 'titulo_inspector');
                break;
            case 'Productos':
                $result = AcreditacionesIndustriales::findAll(
                    array('tipo' => 2)
                );
                $data = ArrayHelper::map($result, 'id', 'titulo_inspector');
                break;
            case 'DACs':
                $result = AcreditacionesIndustriales::findAll(
                    array('tipo' => 3)
                );
                $data = ArrayHelper::map($result, 'id', 'titulo_inspector');
                break;
            case 'MITs':
                $result = AcreditacionesIndustriales::findAll(
                    array('tipo' => 4)
                );
                $data = ArrayHelper::map($result, 'id', 'titulo_inspector');
                break;
            default:
                $data = array();
        }

        return $data;
    }
}
