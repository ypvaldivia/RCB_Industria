<?php

namespace app\forms;

use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: g-ux
 * Date: 4/07/16
 * Time: 0:02.
 */
class InspeccionMITEditarForm extends InspeccionMITForm
{
    public function rules()
    {
        return array(
            array('numero_control, oficina, lugar_inspeccion, homologacion, entidad', 'required'),
            array('numero_control, oficina, lugar_inspeccion, homologacion, detalle_homologacion,
                    nivel, tipo_homologacion, estado, certificado, vigencia, numero_visita, fecha, descripcion,
                    inspeccion, aprobado, aprobada, rechazada, hallazgos, inconformidades, actualizado',
                'safe', 'on' => 'search', ),
        );
    }
}
