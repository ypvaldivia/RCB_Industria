<?php

namespace app\forms;

use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: tesa
 * Date: 10/24/15
 * Time: 7:29 AM.
 */
class CertificadosPersonaDefaultProvisionalForm extends Model
{
    public $inspeccion;
    public $persona;
    public $documentacion;
    public $servicio;
    public $expedicion;
    public $funcionario;
    public $control;
    public $nivelAprobacion;

    public function rules()
    {
        return array(
            array('documentacion, servicio, expedicion, funcionario', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'documentacion' => 'Documentacion (Regla, Procedimiento, Instruccion, Capitulo del MAPS u otros)',
            'servicio' => 'Servicio',
            'expedicion' => 'Fecha de Expedicion',
            'funcionario' => 'Por el RCB',
        );
    }

    public function setMyAttributes($form, $obj)
    {
        $this->documentacion = (isset($form['documentacion'])) ? $form['documentacion'] : null;
        $this->servicio = (isset($form['servicio'])) ? $form['servicio'] : null;
        $this->expedicion = (isset($form['expedicion'])) ? $form['expedicion'] : null;
        $this->funcionario = (isset($form['funcionario'])) ? $form['funcionario'] : null;
        $this->inspeccion = $obj->inspeccion0;
        $this->persona = $obj->persona0;
        $this->control = 1;
    }
}
