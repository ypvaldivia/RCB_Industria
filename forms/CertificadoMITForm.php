<?php

namespace app\forms;

use app\models\Inspecciones;
use yii;
use yii\base\Model;

/**
 * Created by PhpStorm.
 * User: geo
 * Date: 25/03/16
 * Time: 12:33.
 */
class CertificadoMITForm extends Model
{
    public $inspeccion;
    public $objeto;
    public $numero;
    public $propietario;
    public $descripcion;
    public $radio_pp_est;
    public $radio_pp_din;
    public $radio_aux_est;
    public $radio_aux_din;
    public $radio_aux1_est;
    public $radio_aux1_din;
    public $carga_pp_est;
    public $carga_pp_din;
    public $carga_aux_est;
    public $carga_aux_din;
    public $carga_aux1_est;
    public $carga_aux1_din;
    public $ci_pp_certificada;
    public $ci_aux_certificada;
    public $ci_aux1_certificada;
    public $capacidad;
    public $expedicion;
    public $vencimiento;
    public $storeEmision;
    public $storeValidez;
    public $funcionario;
    public $control;
    public $nivelAprobacion;
    public $limitacionIzado;
    public $lugar;
    public $flag;
    /* Campos del aval de importacion */
    public $tipo;
    public $importador;
    public $destinatario;
    public $vtraslacion;
    public $vizado;
    public $vdescenso;
    public $vgiro;
    public $htrabajo;
    public $nunidades;

    public function rules()
    {
        return array(
            array('descripcion, radio, carga, vencimiento, funcionario', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'descripcion' => 'Descripción del equipo (marca y su número distintivo)',
            'expedicion' => 'Fecha de Expedici&oacute;n',
            'vencimiento' => 'Fecha de Vencimiento',
            'funcionario' => 'Por el RCB',
        );
    }

    public function initForm($inspXprod)
    {
        $inspeccion = Inspecciones::findOne($inspXprod->inspeccion0->id);
        $this->objeto = $inspXprod->medioIzado->nombre;
        $this->tipo = $inspXprod->medioIzado->tipo0->tipo;
        $this->propietario = $inspXprod->medioIzado->propietario0->nombre;
        $this->capacidad = $inspXprod->medioIzado->ci_nominal;
        if ($inspXprod->numero != null && $inspXprod->numero != '') {
            $this->numero = $inspeccion->numero_control.'-'.$inspXprod->numero;
        } else {
            $this->numero = $inspeccion->numero_control;
        }
    }

    public function initFormDac($inspXprod)
    {
        $inspeccion = Inspecciones::findOne($inspXprod->inspeccion0->id);
        $this->objeto = $inspXprod->dac0->nombre;
        $this->propietario = $inspXprod->dac0->propietario0->nombre;
        $this->capacidad = $inspXprod->dac0->propietario0->nombre;
        if ($inspXprod->numero != null && $inspXprod->numero != '') {
            $this->numero = $inspeccion->numero_control.'-'.$inspXprod->numero;
        } else {
            $this->numero = $inspeccion->numero_control;
        }
    }

    public function setMyAttributes($form, $obj)
    {
        $this->descripcion = (isset($form['descripcion'])) ? $form['descripcion'] : null;
        $this->radio_pp_est = (isset($form['radio_pp_est'])) ? $form['radio_pp_est'] : null;
        $this->radio_pp_din = (isset($form['radio_pp_din'])) ? $form['radio_pp_din'] : null;
        $this->radio_aux_est = (isset($form['radio_aux_est'])) ? $form['radio_aux_est'] : null;
        $this->radio_aux_din = (isset($form['radio_aux_din'])) ? $form['radio_aux_din'] : null;
        $this->radio_aux1_est = (isset($form['radio_aux1_est'])) ? $form['radio_aux1_est'] : null;
        $this->radio_aux1_din = (isset($form['radio_aux1_din'])) ? $form['radio_aux1_din'] : null;
        $this->carga_pp_din = (isset($form['carga_pp_din'])) ? $form['carga_pp_din'] : null;
        $this->carga_pp_est = (isset($form['carga_pp_est'])) ? $form['carga_pp_est'] : null;
        $this->carga_aux_din = (isset($form['carga_aux_din'])) ? $form['carga_aux_din'] : null;
        $this->carga_aux_est = (isset($form['carga_aux_est'])) ? $form['carga_aux_est'] : null;
        $this->carga_aux1_din = (isset($form['carga_aux1_din'])) ? $form['carga_aux1_din'] : null;
        $this->carga_aux1_est = (isset($form['carga_aux1_est'])) ? $form['carga_aux1_est'] : null;
        $this->ci_pp_certificada = (isset($form['ci_pp_certificada'])) ? $form['ci_pp_certificada'] : null;
        $this->ci_aux_certificada = (isset($form['ci_aux_certificada'])) ? $form['ci_aux_certificada'] : null;
        $this->ci_aux1_certificada = (isset($form['ci_aux1_certificada'])) ? $form['ci_aux1_certificada'] : null;
        $this->expedicion = (isset($form['expedicion'])) ? $form['expedicion'] : null;
        $this->funcionario = (isset($form['funcionario'])) ? $form['funcionario'] : null;

        $this->limitacionIzado = (isset($form['limitacionIzado'])) ? $form['limitacionIzado'] : null;
        $this->lugar = (isset($form['lugar'])) ? $form['lugar'] : null;

        $this->importador = (isset($form['importador'])) ? $form['importador'] : null;
        $this->destinatario = (isset($form['destinatario'])) ? $form['destinatario'] : null;
        $this->vtraslacion = (isset($form['vtraslacion'])) ? $form['vtraslacion'] : null;
        $this->vizado = (isset($form['vizado'])) ? $form['vizado'] : null;
        $this->vdescenso = (isset($form['vdescenso'])) ? $form['vdescenso'] : null;
        $this->vgiro = (isset($form['vgiro'])) ? $form['vgiro'] : null;
        $this->htrabajo = (isset($form['htrabajo'])) ? $form['htrabajo'] : null;
        $this->nunidades = (isset($form['nunidades'])) ? $form['nunidades'] : null;

        $this->inspeccion = $obj->inspeccion0;
        $this->control = 1;
    }
}
