<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provincias".
 *
 * @property int $id
 * @property string $nombre
 *
 * @property DacsAcreditados[] $dacsAcreditados
 * @property Entidades[] $entidades
 * @property MediosIzadoAcreditados[] $mediosIzadoAcreditados
 * @property Oficinas[] $oficinas
 * @property PersonasAcreditadas[] $personasAcreditadas
 * @property ProductosAcreditados[] $productosAcreditados
 * @property ServiciosLaboratoriosAcreditados[] $serviciosLaboratoriosAcreditados
 */
class Provincias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[DacsAcreditados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDacsAcreditados()
    {
        return $this->hasMany(DacsAcreditados::className(), ['provincia' => 'id']);
    }

    /**
     * Gets query for [[Entidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntidades()
    {
        return $this->hasMany(Entidades::className(), ['provincia' => 'id']);
    }

    /**
     * Gets query for [[MediosIzadoAcreditados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMediosIzadoAcreditados()
    {
        return $this->hasMany(MediosIzadoAcreditados::className(), ['provincia' => 'id']);
    }

    /**
     * Gets query for [[Oficinas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOficinas()
    {
        return $this->hasMany(Oficinas::className(), ['provincia' => 'id']);
    }

    /**
     * Gets query for [[PersonasAcreditadas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonasAcreditadas()
    {
        return $this->hasMany(PersonasAcreditadas::className(), ['provincia' => 'id']);
    }

    /**
     * Gets query for [[ProductosAcreditados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosAcreditados()
    {
        return $this->hasMany(ProductosAcreditados::className(), ['provincia' => 'id']);
    }

    /**
     * Gets query for [[ServiciosLaboratoriosAcreditados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServiciosLaboratoriosAcreditados()
    {
        return $this->hasMany(ServiciosLaboratoriosAcreditados::className(), ['provincia' => 'id']);
    }
}
