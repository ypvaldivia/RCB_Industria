<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "niveles_inspeccion".
 *
 * @property int $id
 * @property string $nivel
 * @property int $cargo_certificado_definitivo
 *
 * @property AcreditacionesIndustriales[] $acreditacionesIndustriales
 * @property CategoriasMediosIzado[] $categoriasMediosIzados
 * @property Inspecciones[] $inspecciones
 * @property CargosFuncionarios $cargoCertificadoDefinitivo
 */
class NivelesInspeccion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'niveles_inspeccion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nivel', 'cargo_certificado_definitivo'], 'required'],
            [['cargo_certificado_definitivo'], 'integer'],
            [['nivel'], 'string', 'max' => 80],
            [['cargo_certificado_definitivo'], 'exist', 'skipOnError' => true, 'targetClass' => CargosFuncionarios::className(), 'targetAttribute' => ['cargo_certificado_definitivo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nivel' => 'Nivel',
            'cargo_certificado_definitivo' => 'Cargo Certificado Definitivo',
        ];
    }

    /**
     * Gets query for [[AcreditacionesIndustriales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacionesIndustriales()
    {
        return $this->hasMany(AcreditacionesIndustriales::className(), ['niveles_inspeccion_id' => 'id']);
    }

    /**
     * Gets query for [[CategoriasMediosIzados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriasMediosIzados()
    {
        return $this->hasMany(CategoriasMediosIzado::className(), ['nivel_especial' => 'id']);
    }

    /**
     * Gets query for [[Inspecciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspecciones()
    {
        return $this->hasMany(Inspecciones::className(), ['nivel' => 'id']);
    }

    /**
     * Gets query for [[CargoCertificadoDefinitivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCargoCertificadoDefinitivo()
    {
        return $this->hasOne(CargosFuncionarios::className(), ['id' => 'cargo_certificado_definitivo']);
    }

    /*
     * Concatena el nivel de inspeccion con el cargo del funcionario que aprueba para ese nivel de inspeccion.
     * Esta funcion get de Yii facilita el render de la info concatenada entre las dos tablas
     * (nivel_inspeccion y cargos_funcionarios) en la vista cuando necesita en el formulario utilizar
     * un dropDownList.
     */
    public function getFullNivelInspeccion()
    {
        return $this->nivel . ' - ' . $this->cargoCertificadoDefinitivo->cargo;
    }

    public function nivelesInferiores($nivel)
    {
        $resultList = array();
        $tmp = $nivel;
        $i = 0;
        $control = 0;
        $niveles = $this->find();
        while ($control <= 5) {
            foreach ($niveles as $item) {
                ++$control;
                if (
                    $item->cargoCertificadoDefinitivo->superior ==
                    $nivel->cargoCertificadoDefinitivo->id
                ) {
                    if ($item->nivel != '00') {
                        $resultList[$i] = $item;
                    }
                    $nivel = $item;
                    ++$i;
                    $control = 0;
                }
            }
        }

        $resultList = array_reverse($resultList);
        $resultList[$i] = $tmp;

        return $resultList;
    }

    public function nivelInmediatoInferior($cargo)
    {
        $niveles = $this->find();
        if (count($niveles) < 2) {
            return $niveles[0];
        }
        $band = false;
        $i = 0;
        $item = $niveles[$i];
        while (!$band) {
            if ($item->cargoCertificadoDefinitivo->superior == $cargo->id) {
                $band = true;
            } else {
                ++$i;
                $item = $niveles[$i];
            }
        }

        return $item;
    }

    public function getPersonaMayorNivelAprobacionGlobal()
    {
        $niveles = $this->find();
        $best = '';
        $result = null;
        foreach ($niveles as $n) {
            $tmp = $n->nivel;
            if ($best == '') {
                $best = $tmp;
                $result = $n;
            } else {
                if ($best < $tmp) {
                    $best = $tmp;
                    $result = $n;
                }
            }
        }

        return $result->cargoCertificadoDefinitivo->funcionarioses[0];
    }
}
