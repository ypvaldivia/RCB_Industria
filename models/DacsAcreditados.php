<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dacs_acreditados".
 *
 * @property int $id
 * @property string $nombre
 * @property int|null $tipo
 * @property int|null $propietario
 * @property int|null $provincia
 * @property float|null $ct_nominal
 * @property float|null $ct_certificada
 *
 * @property TiposDacs $tipo0
 * @property Entidades $propietario0
 * @property Provincias $provincia0
 * @property InspeccionesXDacs[] $inspeccionesXDacs
 * @property ShortcutHomologacionesDacs[] $shortcutHomologacionesDacs
 */
class DacsAcreditados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dacs_acreditados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['tipo', 'propietario', 'provincia'], 'integer'],
            [['ct_nominal', 'ct_certificada'], 'number'],
            [['nombre'], 'string', 'max' => 250],
            [['tipo'], 'exist', 'skipOnError' => true, 'targetClass' => TiposDacs::className(), 'targetAttribute' => ['tipo' => 'id']],
            [['propietario'], 'exist', 'skipOnError' => true, 'targetClass' => Entidades::className(), 'targetAttribute' => ['propietario' => 'id']],
            [['provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['provincia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'tipo' => 'Tipo',
            'propietario' => 'Propietario',
            'provincia' => 'Provincia',
            'ct_nominal' => 'Ct Nominal',
            'ct_certificada' => 'Ct Certificada',
        ];
    }

    /**
     * Gets query for [[Tipo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipo0()
    {
        return $this->hasOne(TiposDacs::className(), ['id' => 'tipo']);
    }

    /**
     * Gets query for [[Propietario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPropietario0()
    {
        return $this->hasOne(Entidades::className(), ['id' => 'propietario']);
    }

    /**
     * Gets query for [[Provincia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia0()
    {
        return $this->hasOne(Provincias::className(), ['id' => 'provincia']);
    }

    /**
     * Gets query for [[InspeccionesXDacs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccionesXDacs()
    {
        return $this->hasMany(InspeccionesXDacs::className(), ['dac' => 'id']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesDacs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesDacs()
    {
        return $this->hasMany(ShortcutHomologacionesDacs::className(), ['dac' => 'id']);
    }

    public function getFullID()
    {
        return $this->nombre .
            ' (' .
            $this->propietario0->nombre .
            ', ' .
            $this->provincia0->nombre .
            ') ';
    }
}
