<?php

namespace app\models;

use stdClass;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "funcionarios".
 *
 * @property int $id
 * @property string $nombre_apellidos
 * @property string $codigo
 * @property int $oficina
 * @property int|null $categoria
 * @property int $cargo
 * @property string|null $imagen_firma
 * @property int|null $es_inspector
 * @property int|null $usuario
 * @property int|null $inactivo
 *
 * @property AcreditacionesInspectores[] $acreditacionesInspectores
 * @property AprobacionXFuncionarios[] $aprobacionXFuncionarios
 * @property CategoriasFuncionarios $categoria0
 * @property CargosFuncionarios $cargo0
 * @property Oficinas $oficina0
 * @property InspectoresActuantes[] $inspectoresActuantes
 * @property Trazas[] $trazas
 */
class Funcionarios  extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'funcionarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_apellidos', 'codigo', 'oficina', 'cargo'], 'required'],
            [['oficina', 'categoria', 'cargo', 'es_inspector', 'usuario', 'inactivo'], 'integer'],
            [['nombre_apellidos'], 'string', 'max' => 100],
            [['codigo'], 'string', 'max' => 2],
            [['imagen_firma'], 'string', 'max' => 10],
            [['categoria'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriasFuncionarios::className(), 'targetAttribute' => ['categoria' => 'id']],
            [['cargo'], 'exist', 'skipOnError' => true, 'targetClass' => CargosFuncionarios::className(), 'targetAttribute' => ['cargo' => 'id']],
            [['oficina'], 'exist', 'skipOnError' => true, 'targetClass' => Oficinas::className(), 'targetAttribute' => ['oficina' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre_apellidos' => 'Nombre Apellidos',
            'codigo' => 'Codigo',
            'oficina' => 'Oficina',
            'categoria' => 'Categoria',
            'cargo' => 'Cargo',
            'imagen_firma' => 'Imagen Firma',
            'es_inspector' => 'Es Inspector',
            'usuario' => 'Usuario',
            'inactivo' => 'Inactivo',
        ];
    }

    /**
     * Gets query for [[AcreditacionesInspectores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacionesInspectores()
    {
        return $this->hasMany(AcreditacionesInspectores::className(), ['inspector' => 'id']);
    }

    /**
     * Gets query for [[AprobacionXFuncionarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprobacionXFuncionarios()
    {
        return $this->hasMany(AprobacionXFuncionarios::className(), ['funcionario' => 'id']);
    }

    /**
     * Gets query for [[Categoria0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria0()
    {
        return $this->hasOne(CategoriasFuncionarios::className(), ['id' => 'categoria']);
    }

    /**
     * Gets query for [[Cargo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCargo0()
    {
        return $this->hasOne(CargosFuncionarios::className(), ['id' => 'cargo']);
    }

    /**
     * Gets query for [[Oficina0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOficina0()
    {
        return $this->hasOne(Oficinas::className(), ['id' => 'oficina']);
    }

    /**
     * Gets query for [[InspectoresActuantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspectoresActuantes()
    {
        return $this->hasMany(InspectoresActuantes::className(), ['inspector' => 'id']);
    }

    /**
     * Gets query for [[Trazas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrazas()
    {
        return $this->hasMany(Trazas::className(), ['funcionario' => 'id']);
    }

    public $username;
    public $correo;
    public $rol;
    public $password;
    public $password2;


    /**
     * Funcion que devuelve el TOTAL de acreditaciones que tiene un inspector
     * NO lleva parametro puesto que se llama dentro de un bucle.
     *
     * @author Samy Otero <samy.otero@gmail.com>
     */
    public function getTotalAcreditaciones()
    {
        return count($this->acreditacionesInspectores);
    }

    /**
     * Funcion que devuelve el TOTAL de acreditaciones vigentes para un inspector,
     * Se comprueba que no entre en el rango de 3 meses por vencer
     * NO lleva parametro puesto que se llama dentro de un bucle.
     *
     * @author Levian Lara <levitoon@gmail.com>
     */
    public function getTotalAcreditacionesVigentes()
    {
        /*
        $sql = "SELECT
                  funcionarios.nombre_apellidos,
                  funcionarios.codigo,
                  funcionarios.oficina,
                  COUNT(acreditaciones_industriales.codigo) AS ACREDITACIONES,
                  acreditaciones_inspectores.vencimiento
                FROM
                  funcionarios,
                  acreditaciones_industriales,
                  acreditaciones_inspectores
                WHERE
                  funcionarios.id = $this->id AND
                  funcionarios.id = acreditaciones_inspectores.inspector AND
                  acreditaciones_industriales.id = acreditaciones_inspectores.acreditacion AND
                  acreditaciones_inspectores.vencimiento > DATE_ADD(NOW(), INTERVAL 3 MONTH)
                GROUP BY
                  funcionarios.nombre_apellidos,
                  funcionarios.codigo,
                  funcionarios.oficina,
                  acreditaciones_inspectores.vencimiento";
        
        $result = Yii::$app->db->createCommand($sql)->queryAll();
        */

        $sql = "SELECT *
                FROM acreditaciones_inspectores
                WHERE
                  acreditaciones_inspectores.inspector = $this->id AND
                  acreditaciones_inspectores.vencimiento > DATE_ADD(NOW(), INTERVAL 3 MONTH)";

        $result = Yii::$app->db->createCommand($sql)->queryAll();

        return count($result);
    }

    /**
     * Funcion que devuelve el TOTAL de acreditaciones por vencer en un rango de 3 meses,
     * NO lleva parametro puesto que se llama dentro de un bucle.
     *
     * @author Levian Lara <levitoon@gmail.com>
     */
    public function getTotalAcreditacionesPorVencer()
    {
        $sql = "SELECT *
                FROM
                  acreditaciones_inspectores
                WHERE
                  acreditaciones_inspectores.inspector = $this->id AND
                  acreditaciones_inspectores.vencimiento BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)";

        $result = Yii::$app->db->createCommand($sql)->queryAll();

        return count($result);
    }

    /**
     * Funcion que devuelve el TOTAL de acreditaciones vencidas,
     * NO lleva parametro puesto que se llama dentro de un bucle.
     *
     * @author Levian Lara <levitoon@gmail.com>
     */
    public function getTotalAcreditacionesVencidas()
    {
        $sql = "SELECT *
                FROM
                  acreditaciones_inspectores
                WHERE
                  acreditaciones_inspectores.inspector = $this->id AND
                  acreditaciones_inspectores.vencimiento < now()";

        $result = Yii::$app->db->createCommand($sql)->queryAll();

        return count($result);
    }

    /**
     * Funcion que devuelve TODAS las acreditaciones por vencer.
     *
     * @author Levian Lara <levitoon@gmail.com>
     * @parameter  integer $idinspector
     */
    public function getAcreditacionesXvencer1()
    {
        $user = $this->find()
            ->where(['usuario' => Yii::$app->user->id])
            ->one();

        if ($user != null) {
            if ($user->oficina0->oficina_superior != $user->oficina0->id) {
                if ($user->oficina0->hasBoss()) {
                    //Se deben recorrer todas las oficinas inferiores no la primera que se encuentra
                    $infOff = Oficinas::findAll([
                        'oficina_superior' => $user->oficina0->id
                    ]);
                    if ($infOff != null) {
                        $cond =
                            '(funcionarios.oficina = ' . $user->oficina0->id;
                        foreach ($infOff as $o) {
                            $cond =
                                $cond . ' OR funcionarios.oficina = ' . $o->id;
                        }
                        $cond =
                            $cond .
                            ') AND vencimiento BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                    } else {
                        $cond =
                            'funcionarios.oficina = ' .
                            $user->oficina0->id .
                            ' AND vencimiento BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                    }
                } else {
                    $cond =
                        'funcionarios.oficina = ' .
                        $user->oficina0->id .
                        ' AND vencimiento BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                }
            } else {
                $cond =
                    'vencimiento BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
            }

            $query = new Query();

            $query
                ->select([
                    'oficinas.nombre',
                    'funcionarios.nombre_apellidos',
                    'funcionarios.codigo',
                    'categorias_acreditaciones.grupo AS servicio',
                    'acreditaciones_inspectores.vencimiento AS vencimiento'
                ])
                ->from('oficinas')
                ->join('INNER JOIN', 'funcionarios', 'oficinas.id = funcionarios.oficina')
                ->leftJoin(
                    'acreditaciones_inspectores',
                    'acreditaciones_inspectores.inspector = funcionarios.id'
                )
                ->leftJoin(
                    'categorias_acreditaciones',
                    'acreditaciones_inspectores.acreditacion = categorias_acreditaciones.id'
                )
                ->where($cond)
                ->orderBy('oficinas.nombre ASC');

            $result = $query->all();

            if (count($result) > 0) {
                return $this->preparaAcreditacionesVencerVencidas($result);
                //return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function preparaAcreditacionesVencerVencidas($result)
    {
        $prepare = [];
        $oficinas = [];
        $inspectores = [];
        $aux = [];

        foreach ($result as $row) {
            array_push($inspectores, $row['nombre_apellidos']);
        }

        $inspectores = array_values(array_unique($inspectores));

        foreach ($inspectores as $insp) {
            $inspector = new stdClass();
            $inspector->nombre = $insp;
            $inspector->homologaciones = [];

            foreach ($result as $row) {
                if ($insp === $row['nombre_apellidos']) {
                    $inspector->codigo = $row['codigo'];
                    $inspector->oficina = $row['nombre'];
                    $homologacion = new stdClass();
                    $homologacion->servicio = $row['servicio'];
                    $homologacion->vence = $row['vencimiento'];
                    array_push($inspector->homologaciones, $homologacion);
                }
            }
            array_push($aux, $inspector);
        }

        foreach ($result as $row) {
            array_push($oficinas, $row['nombre']);
        }

        $oficinas = array_values(array_unique($oficinas));

        foreach ($oficinas as $oficina) {
            $obj = new stdClass();
            $obj->oficina = $oficina;
            $obj->inspectores = [];

            foreach ($aux as $inspector) {
                if ($oficina === $inspector->oficina) {
                    array_push($obj->inspectores, $inspector);
                }
            }
            array_push($prepare, $obj);
        }

        return $prepare;
    }

    /**
     * Funcion que devuelve TODAS las acreditaciones vencidas.
     *
     * @author Levian Lara <levitoon@gmail.com>
     * @parameter  integer $idinspector
     */
    public function getAcreditacionesVencidas1()
    {
        $user = Funcionarios::findOne(['usuario' => Yii::$app->user->id]);

        if ($user != null) {
            if ($user->oficina0->oficina_superior != $user->oficina0->id) {
                if ($user->oficina0->hasBoss()) {
                    //Se deben recorrer todas las oficinas inferiores no la primera que se encuentra
                    $infOff = Oficinas::findAll([
                        'oficina_superior' => $user->oficina0->id
                    ]);
                    if ($infOff != null) {
                        $cond =
                            '(funcionarios.oficina = ' . $user->oficina0->id;
                        foreach ($infOff as $o) {
                            $cond =
                                $cond . ' OR funcionarios.oficina = ' . $o->id;
                        }
                        $cond = $cond . ') AND vencimiento <= NOW()';
                    } else {
                        $cond =
                            'funcionarios.oficina = ' .
                            $user->oficina0->id .
                            ' AND vencimiento <= NOW()';
                    }
                } else {
                    $cond =
                        'funcionarios.oficina = ' .
                        $user->oficina0->id .
                        ' AND vencimiento <= NOW()';
                }
            } else {
                $cond = 'vencimiento <= NOW()';
            }

            $query = new Query;

            $query
                ->select([
                    'oficinas.nombre',
                    'funcionarios.nombre_apellidos',
                    'funcionarios.codigo',
                    'categorias_acreditaciones.grupo AS servicio',
                    'acreditaciones_inspectores.vencimiento AS vencimiento'
                ])
                ->from('oficinas', 'funcionarios', 'categorias_acreditaciones')
                ->join('INNER JOIN', 'funcionarios', 'oficinas.id = funcionarios.oficina')
                ->leftJoin(
                    'acreditaciones_inspectores',
                    'acreditaciones_inspectores.inspector=funcionarios.id'
                )
                ->leftJoin(
                    'categorias_acreditaciones',
                    'acreditaciones_inspectores.acreditacion = categorias_acreditaciones.id'
                )
                ->where($cond)
                ->orderBy('oficinas.nombre ASC');

            $result = $query->all();

            if (count($result) > 0) {
                return $this->preparaAcreditacionesVencerVencidas($result);
                //return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getFuncionariosOficinaNivel($inspeccion, $item)
    {
        //$prefix = substr($inspeccion->numero_control, 0, 4);
        // $sub = Oficinas::find()->where('oficina_superior = :s AND codigo = :c', array(':s' => $inspeccion->oficina0->id, ':c' => $prefix));
        $sub = Oficinas::findOne(['oficina_superior' => $inspeccion->oficina0->id]);
        if ($item->nivel < '02') {
            $central = Oficinas::findOne(['oficina_superior' => $inspeccion->oficina0->id]);
            if ($sub != null) {
                if ($sub->oficinaSuperior->id != $central->id) {
                    //Si es una suboficina (tercer nivel) debe devolver el especialista de la oficina
                    $found = $this->findAll([
                        'oficina' => $sub->oficinaSuperior->id,
                        'cargo' => $item->cargoCertificadoDefinitivo->id,
                        'inactivo' => 0
                    ]);
                    if ($found != null) {
                        return $found;
                    } else {
                        //Buscar si es especialista y si su oficina pertenece a la dependencia
                        return $this->findAll([
                            'oficina' => $sub->id,
                            'cargo' => $item->cargoCertificadoDefinitivo->id,
                            'inactivo' => 0
                        ]);
                    }
                } else {
                    //Si es una dependencia devolver los especialistas
                    return $this->findAll([
                        'oficina' => $sub->id,
                        'cargo' => $item->cargoCertificadoDefinitivo->id,
                        'inactivo' => 0
                    ]);
                }
            } else {
                return $this->findAll([
                    'oficina' => $inspeccion->oficina0->id,
                    'cargo' => $item->cargoCertificadoDefinitivo->id,
                    'inactivo' => 0
                ]);
            }
        } else {
            $found = $this->findAll([
                'oficina' => $inspeccion->oficina0->id,
                'cargo' => $item->cargoCertificadoDefinitivo->id,
                'inactivo' => 0
            ]);
            if ($found != null) {
                return $found;
            } else {
                //Buscar si es especialista y si su oficina pertenece a la dependencia
                return $this->findAll([
                    'oficina' => $sub->id,
                    'cargo' => $item->cargoCertificadoDefinitivo->id,
                    'inactivo' => 0
                ]);
            }
        }
    }

    public function getCargoFull()
    {
        if (
            $this->cargo0->id == 3 ||
            $this->cargo0->id == 5 ||
            $this->cargo0->id == 6
        ) {
            $tmp = self::find()->where([
                'cargo' => $this->cargo0->id,
                'inactivo' => 1,
                'oficina' => $this->oficina0->id
            ]);
            if ($tmp != null && $this->inactivo != 1) {
                return $this->cargo0->cargo . ' (Por Sustituci&oacute;n)';
            } else {
                return $this->cargo0->cargo;
            }
        } else {
            return $this->cargo0->cargo;
        }
    }

    public function getSustituto()
    {
        $tmp = self::find()->where([
            'cargo' => $this->cargo0->id,
            'inactivo' => 1,
            'oficina' => $this->oficina0->id
        ]);

        return $tmp;
    }

    public function getRevision($userId, $insp)
    {
        $usuario = $this->find()->where(['usuario' => $userId]);
        if ($usuario != null) {
            $inspeccion = Inspecciones::findOne($insp);
            $lastVisit = $inspeccion->getUltimaVisita();
            $inspectores = InspectoresActuantes::findOne(
                'inspector = :i AND visita = :v',
                [':i' => $usuario->id, ':v' => $lastVisit->id]
            );
            if ($inspectores == null) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function getFuncionarioSuperior($funcionario)
    {
        if (
            $funcionario->oficina0->oficinaSuperior->id ==
            $funcionario->oficina0->id
        ) {
            //Esta condicional significa que el usuario esta en COPE
            //Hay que buscar si es el jefe maximo o no
            if (
                $funcionario->cargo0->superior0->id ==
                $funcionario->cargo0->id ||
                $funcionario->cargo0->superior0 == null
            ) {
                //Es el jefe maximo
                return $funcionario;
            } else {
                $jefe = $this->findAll(
                    'cargo = :c AND oficina = :o AND inactivo = :n',
                    [
                        ':c' => $funcionario->cargo0->superior,
                        ':o' => $funcionario->oficina0->id,
                        ':n' => 0
                    ]
                );
                if ($jefe != null) {
                    return $jefe;
                } else {
                    return null;
                }
            }
        }
        $jefe = $this->findAll(
            'cargo = :c AND oficina = :o AND inactivo = :n',
            [
                ':c' => $funcionario->cargo0->superior,
                ':o' => $funcionario->oficina0->id,
                ':n' => 0
            ]
        );
        if ($jefe != null) {
            return $jefe;
        } else {
            //Es una suboficina y el jefe esta en ella misma
            if (
                $funcionario->cargo0->id == 3 &&
                !$funcionario->oficina0->hasBoss()
            ) {
                $cope = Oficinas::find()->where('oficina_superior = id');
                $jefe = $this->findAll(
                    'cargo = :c AND oficina = :o AND inactivo = :n',
                    [
                        ':c' => $funcionario->cargo0->superior,
                        ':o' => $cope->id,
                        ':n' => 0
                    ]
                );
                if ($jefe != null) {
                    return $jefe;
                }
            }
            //Es una suboficina y no tiene jefe o ya es el jefe de la oficina
            //Hay que buscar la oficina superior y en ella los jefes activos
            $jefe = $this->findAll(
                'cargo = :c AND oficina = :o AND inactivo = :n',
                [
                    ':c' => $funcionario->cargo0->superior,
                    ':o' => $funcionario->oficina0->oficinaSuperior->id,
                    ':n' => 0
                ]
            );
            if ($jefe != null) {
                return $jefe;
            } else {
                return null;
            }
        }
    }

    public function getNameFromUser($user)
    {
        $tmp = $this->find()->where('usuario = :u', [':u' => $user]);

        return $tmp != null ? $tmp->nombre_apellidos : 'No definido';
    }

    /**
     * @return array $funcionarios
     */
    public function findAllOrdered()
    {
        return Funcionarios::findAll('nombre_apellidos ASC');
    }
}
