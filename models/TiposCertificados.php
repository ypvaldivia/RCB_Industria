<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipos_certificados".
 *
 * @property int $id
 * @property string $titulo
 * @property string|null $descripcion
 * @property string|null $plantilla_pdf
 *
 * @property AcreditacionesIndustriales[] $acreditacionesIndustriales
 */
class TiposCertificados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipos_certificados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'required'],
            [['descripcion'], 'string'],
            [['titulo'], 'string', 'max' => 100],
            [['plantilla_pdf'], 'string', 'max' => 45],
            [['titulo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'plantilla_pdf' => 'Plantilla Pdf',
        ];
    }

    /**
     * Gets query for [[AcreditacionesIndustriales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacionesIndustriales()
    {
        return $this->hasMany(AcreditacionesIndustriales::className(), ['tipo_certificado' => 'id']);
    }
}
