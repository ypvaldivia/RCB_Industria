<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorias_acreditaciones".
 *
 * @property int $id
 * @property string $grupo
 *
 * @property AcreditacionesIndustriales[] $acreditacionesIndustriales
 * @property AcreditacionesInspectores[] $acreditacionesInspectores
 */
class CategoriasAcreditaciones  extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias_acreditaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['grupo'], 'required'],
            [['grupo'], 'string', 'max' => 100],
            [['grupo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'grupo' => 'Grupo',
        ];
    }

    /**
     * Gets query for [[AcreditacionesIndustriales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacionesIndustriales()
    {
        return $this->hasMany(AcreditacionesIndustriales::className(), ['categoria' => 'id']);
    }

    /**
     * Gets query for [[AcreditacionesInspectores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacionesInspectores()
    {
        return $this->hasMany(AcreditacionesInspectores::className(), ['acreditacion' => 'id']);
    }

    /**
     * Funcion que devuelve TODAS las acreditaciones que no tiene el inspector.
     *
     * @author G-UX
     * @parameter  integer $id
     */
    public function getAcreditacionesFaltantes($id)
    {
        $result = Yii::$app->db
            ->createCommand()
            ->select('*')
            ->from('categorias_acreditaciones')
            ->where(
                'id NOT IN (SELECT acreditaciones_inspectores.acreditacion FROM  acreditaciones_inspectores WHERE inspector = :id)',
                [':id' => $id]
            )
            ->orderBy('categorias_acreditaciones.id')
            ->queryAll();

        return $result;
    }
}
