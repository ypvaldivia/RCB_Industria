<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shortcut_homologaciones_dacs".
 *
 * @property int $id
 * @property int|null $dac
 * @property int|null $homologacion
 * @property int|null $inspeccion
 * @property string|null $certificado
 * @property string|null $emision
 * @property string|null $vigencia
 * @property string|null $anho_vigencia
 *
 * @property DacsAcreditados $dac0
 * @property AcreditacionesIndustriales $homologacion0
 * @property Inspecciones $inspeccion0
 */
class ShortcutHomologacionesDacs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shortcut_homologaciones_dacs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dac', 'homologacion', 'inspeccion'], 'integer'],
            [['emision', 'vigencia', 'anho_vigencia'], 'safe'],
            [['certificado'], 'string', 'max' => 20],
            [['dac'], 'exist', 'skipOnError' => true, 'targetClass' => DacsAcreditados::className(), 'targetAttribute' => ['dac' => 'id']],
            [['homologacion'], 'exist', 'skipOnError' => true, 'targetClass' => AcreditacionesIndustriales::className(), 'targetAttribute' => ['homologacion' => 'id']],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => Inspecciones::className(), 'targetAttribute' => ['inspeccion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dac' => 'Dac',
            'homologacion' => 'Homologacion',
            'inspeccion' => 'Inspeccion',
            'certificado' => 'Certificado',
            'emision' => 'Emision',
            'vigencia' => 'Vigencia',
            'anho_vigencia' => 'Anho Vigencia',
        ];
    }

    /**
     * Gets query for [[Dac0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDac0()
    {
        return $this->hasOne(DacsAcreditados::className(), ['id' => 'dac']);
    }

    /**
     * Gets query for [[Homologacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHomologacion0()
    {
        return $this->hasOne(AcreditacionesIndustriales::className(), ['id' => 'homologacion']);
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(Inspecciones::className(), ['id' => 'inspeccion']);
    }
}
