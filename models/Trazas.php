<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trazas".
 *
 * @property int $id
 * @property string $fecha
 * @property string $ip
 * @property int $funcionario
 * @property string $accion
 * @property string $detalle
 *
 * @property Funcionarios $funcionario0
 */
class Trazas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trazas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'ip', 'funcionario', 'accion', 'detalle'], 'required'],
            [['fecha'], 'safe'],
            [['funcionario'], 'integer'],
            [['ip'], 'string', 'max' => 15],
            [['accion'], 'string', 'max' => 45],
            [['detalle'], 'string', 'max' => 200],
            [['funcionario'], 'exist', 'skipOnError' => true, 'targetClass' => Funcionarios::className(), 'targetAttribute' => ['funcionario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'ip' => 'Ip',
            'funcionario' => 'Funcionario',
            'accion' => 'Accion',
            'detalle' => 'Detalle',
        ];
    }

    /**
     * Gets query for [[Funcionario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFuncionario0()
    {
        return $this->hasOne(Funcionarios::className(), ['id' => 'funcionario']);
    }
}
