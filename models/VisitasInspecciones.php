<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visitas_inspecciones".
 *
 * @property int $id
 * @property int $numero_visita
 * @property string $fecha
 * @property string|null $descripcion
 * @property int $inspeccion
 * @property int|null $aprobado
 *
 * @property InspectoresActuantes[] $inspectoresActuantes
 * @property Inspecciones $inspeccion0
 */
class VisitasInspecciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visitas_inspecciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_visita', 'fecha', 'inspeccion'], 'required'],
            [['numero_visita', 'inspeccion', 'aprobado'], 'integer'],
            [['fecha'], 'safe'],
            [['descripcion'], 'string'],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => Inspecciones::className(), 'targetAttribute' => ['inspeccion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero_visita' => 'Numero Visita',
            'fecha' => 'Fecha',
            'descripcion' => 'Descripcion',
            'inspeccion' => 'Inspeccion',
            'aprobado' => 'Aprobado',
        ];
    }

    /**
     * Gets query for [[InspectoresActuantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspectoresActuantes()
    {
        return $this->hasMany(InspectoresActuantes::className(), ['visita' => 'id']);
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(Inspecciones::className(), ['id' => 'inspeccion']);
    }
}
