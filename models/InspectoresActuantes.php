<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inspectores_actuantes".
 *
 * @property int $id
 * @property int $inspector
 * @property int $visita
 *
 * @property Funcionarios $inspector0
 * @property VisitasInspecciones $visita0
 */
class InspectoresActuantes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspectores_actuantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inspector', 'visita'], 'required'],
            [['inspector', 'visita'], 'integer'],
            [['inspector'], 'exist', 'skipOnError' => true, 'targetClass' => Funcionarios::className(), 'targetAttribute' => ['inspector' => 'id']],
            [['visita'], 'exist', 'skipOnError' => true, 'targetClass' => VisitasInspecciones::className(), 'targetAttribute' => ['visita' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inspector' => 'Inspector',
            'visita' => 'Visita',
        ];
    }

    /**
     * Gets query for [[Inspector0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspector0()
    {
        return $this->hasOne(Funcionarios::className(), ['id' => 'inspector']);
    }

    /**
     * Gets query for [[Visita0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVisita0()
    {
        return $this->hasOne(VisitasInspecciones::className(), ['id' => 'visita']);
    }
}
