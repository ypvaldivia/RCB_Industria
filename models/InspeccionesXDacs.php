<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inspecciones_x_dacs".
 *
 * @property int $id
 * @property int $dac
 * @property int $inspeccion
 * @property string|null $numero
 * @property int $aprobado
 * @property int $aprobado_definitivo
 * @property string|null $cprovisional
 * @property string|null $cdefinitivo
 *
 * @property DacsAcreditados $dac0
 * @property Inspecciones $inspeccion0
 */
class InspeccionesXDacs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspecciones_x_dacs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dac', 'inspeccion', 'aprobado_definitivo'], 'required'],
            [['dac', 'inspeccion', 'aprobado', 'aprobado_definitivo'], 'integer'],
            [['numero'], 'string', 'max' => 3],
            [['cprovisional', 'cdefinitivo'], 'string', 'max' => 100],
            [['dac'], 'exist', 'skipOnError' => true, 'targetClass' => DacsAcreditados::className(), 'targetAttribute' => ['dac' => 'id']],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => Inspecciones::className(), 'targetAttribute' => ['inspeccion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dac' => 'Dac',
            'inspeccion' => 'Inspeccion',
            'numero' => 'Numero',
            'aprobado' => 'Aprobado',
            'aprobado_definitivo' => 'Aprobado Definitivo',
            'cprovisional' => 'Cprovisional',
            'cdefinitivo' => 'Cdefinitivo',
        ];
    }

    /**
     * Gets query for [[Dac0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDac0()
    {
        return $this->hasOne(DacsAcreditados::className(), ['id' => 'dac']);
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(Inspecciones::className(), ['id' => 'inspeccion']);
    }
}
