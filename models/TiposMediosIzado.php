<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipos_medios_izado".
 *
 * @property int $id
 * @property string $tipo
 * @property string|null $descripcion
 * @property int|null $tipo_certificado
 *
 * @property MediosIzadoAcreditados[] $mediosIzadoAcreditados
 */
class TiposMediosIzado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipos_medios_izado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['descripcion'], 'string'],
            [['tipo_certificado'], 'integer'],
            [['tipo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
            'descripcion' => 'Descripcion',
            'tipo_certificado' => 'Tipo Certificado',
        ];
    }

    /**
     * Gets query for [[MediosIzadoAcreditados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMediosIzadoAcreditados()
    {
        return $this->hasMany(MediosIzadoAcreditados::className(), ['tipo' => 'id']);
    }
}
