<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shortcut_homologaciones_productos".
 *
 * @property int $id
 * @property int $homologacion
 * @property int $producto
 * @property int|null $inspeccion
 * @property string|null $certificado
 * @property string|null $emision
 * @property string|null $vigencia
 * @property string|null $anho_vigencia
 *
 * @property AcreditacionesIndustriales $homologacion0
 * @property ProductosAcreditados $producto0
 * @property Inspecciones $inspeccion0
 */
class ShortcutHomologacionesProductos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shortcut_homologaciones_productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['homologacion', 'producto'], 'required'],
            [['homologacion', 'producto', 'inspeccion'], 'integer'],
            [['emision', 'vigencia', 'anho_vigencia'], 'safe'],
            [['certificado'], 'string', 'max' => 20],
            [['homologacion'], 'exist', 'skipOnError' => true, 'targetClass' => AcreditacionesIndustriales::className(), 'targetAttribute' => ['homologacion' => 'id']],
            [['producto'], 'exist', 'skipOnError' => true, 'targetClass' => ProductosAcreditados::className(), 'targetAttribute' => ['producto' => 'id']],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => Inspecciones::className(), 'targetAttribute' => ['inspeccion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'homologacion' => 'Homologacion',
            'producto' => 'Producto',
            'inspeccion' => 'Inspeccion',
            'certificado' => 'Certificado',
            'emision' => 'Emision',
            'vigencia' => 'Vigencia',
            'anho_vigencia' => 'Anho Vigencia',
        ];
    }

    /**
     * Gets query for [[Homologacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHomologacion0()
    {
        return $this->hasOne(AcreditacionesIndustriales::className(), ['id' => 'homologacion']);
    }

    /**
     * Gets query for [[Producto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto0()
    {
        return $this->hasOne(ProductosAcreditados::className(), ['id' => 'producto']);
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(Inspecciones::className(), ['id' => 'inspeccion']);
    }
}
