<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cargos_funcionarios".
 *
 * @property int $id
 * @property string $cargo
 * @property string|null $descripcion
 * @property int|null $superior
 *
 * @property CargosFuncionarios $superior0
 * @property CargosFuncionarios[] $cargosFuncionarios
 * @property Funcionarios[] $funcionarios
 * @property NivelesInspeccion[] $nivelesInspeccions
 */
class CargosFuncionarios  extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cargos_funcionarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cargo'], 'required'],
            [['superior'], 'integer'],
            [['cargo'], 'string', 'max' => 100],
            [['descripcion'], 'string', 'max' => 200],
            [['cargo'], 'unique'],
            [['superior'], 'exist', 'skipOnError' => true, 'targetClass' => CargosFuncionarios::className(), 'targetAttribute' => ['superior' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cargo' => 'Cargo',
            'descripcion' => 'Descripcion',
            'superior' => 'Superior',
        ];
    }

    /**
     * Gets query for [[Superior0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSuperior0()
    {
        return $this->hasOne(CargosFuncionarios::className(), ['id' => 'superior']);
    }

    /**
     * Gets query for [[CargosFuncionarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCargosFuncionarios()
    {
        return $this->hasMany(CargosFuncionarios::className(), ['superior' => 'id']);
    }

    /**
     * Gets query for [[Funcionarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFuncionarios()
    {
        return $this->hasMany(Funcionarios::className(), ['cargo' => 'id']);
    }

    /**
     * Gets query for [[NivelesInspeccions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNivelesInspeccions()
    {
        return $this->hasMany(NivelesInspeccion::className(), ['cargo_certificado_definitivo' => 'id']);
    }

    public function nivelesInferioresCargo($nivel)
    {
        $resultList = [];
        $i = 0;
        $control = 0;
        $cargos = $this->find()->all();
        while ($control <= 25) {
            foreach ($cargos as $item) {
                ++$control;
                if ($item->superior === $nivel) {
                    $resultList[$i] = $item;
                    $nivel = $item->id;
                    ++$i;
                    $control = 0;
                }
            }
        }

        return $resultList;
    }
}
