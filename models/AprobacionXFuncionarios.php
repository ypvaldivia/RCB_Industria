<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aprobacion_x_funcionarios".
 *
 * @property int $id
 * @property int $funcionario
 * @property int|null $inspeccion
 * @property int|null $aprobada
 * @property int|null $rechazada
 * @property string|null $hallazgos
 * @property string|null $inconformidades
 * @property string|null $actualizado
 * @property int|null $valida
 *
 * @property Inspecciones $inspeccion0
 * @property Funcionarios $funcionario0
 */
class AprobacionXFuncionarios  extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aprobacion_x_funcionarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['funcionario'], 'required'],
            [['funcionario', 'inspeccion', 'aprobada', 'rechazada', 'valida'], 'integer'],
            [['hallazgos', 'inconformidades'], 'string'],
            [['actualizado'], 'safe'],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => Inspecciones::className(), 'targetAttribute' => ['inspeccion' => 'id']],
            [['funcionario'], 'exist', 'skipOnError' => true, 'targetClass' => Funcionarios::className(), 'targetAttribute' => ['funcionario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'funcionario' => 'Funcionario',
            'inspeccion' => 'Inspeccion',
            'aprobada' => 'Aprobada',
            'rechazada' => 'Rechazada',
            'hallazgos' => 'Hallazgos',
            'inconformidades' => 'Inconformidades',
            'actualizado' => 'Actualizado',
            'valida' => 'Valida',
        ];
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(Inspecciones::className(), ['id' => 'inspeccion']);
    }

    /**
     * Gets query for [[Funcionario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFuncionario0()
    {
        return $this->hasOne(Funcionarios::className(), ['id' => 'funcionario']);
    }

    public function estadoAprobacion($func, $insp)
    {
        $state = parent::findAll([
            'funcionario' => $func,
            'inspeccion' => $insp
        ]);
        $state_counter = $this->find()
            ->where([
                'funcionario' => $func,
                'inspeccion' => $insp
            ])
            ->count();
        if ($state == null) {
            return "<span class='label label-warning'>Pendiente</span>";
        } else {
            $check = $state[$state_counter - 1];
            if ($check->aprobada == 1 && $check->valida == 1) {
                return "<span class='label label-success'>Aprobada</span>";
            } else {
                if ($check->rechazada == 1) {
                    return "<span class='label label-danger'>Rechazada</span>";
                } else {
                    return "<span class='label label-warning'>Pendiente</span>";
                }
            }
        }
    }

    public function funcAprobacion($func, $insp)
    {
        $state = $this->findAll([
            'funcionario' => $func,
            'inspeccion' => $insp
        ]);
        $state_counter = $this->find()
            ->where([
                'funcionario' => $func,
                'inspeccion' => $insp
            ])
            ->count();
        if ($state == null) {
            return '';
        } else {
            $check = $state[$state_counter - 1];
            if ($check->aprobada == 1) {
                return "<span class='badge badge-success'>&check;</span>";
            } else {
                if ($check->rechazada == 1 || $check->valida == 0) {
                    return "<span class='badge badge-danger'>&cross;</span>";
                } else {
                    return '';
                }
            }
        }
    }

    public function estadoAprobacionFuncionarios($func, $insp)
    {
        $result = "<span class='label label-warning'>Pendiente</span>";
        foreach ($func as $f) {
            $state = $this->findAll(['funcionario' => $f->id, 'inspeccion' => $insp]);
            $state_counter = $this->find()
                ->where(['funcionario' => $f->id, 'inspeccion' => $insp])
                ->count();
            if ($state == null) {
                if (
                    stristr($result, 'Aprobada') == null &&
                    stristr($result, 'Rechazada') == null
                ) {
                    $result =
                        "<span class='label label-warning'>Pendiente</span>";
                }
            } else {
                $check = $state[$state_counter - 1];
                if ($check->aprobada == 1 && $check->valida == 1) {
                    $result =
                        "<span class='label label-success'>Aprobada</span>";
                } else {
                    if ($check->rechazada == 1) {
                        $result =
                            "<span class='label label-danger'>Rechazada</span>";
                    } else {
                        if (
                            stristr($result, 'Aprobada') == null &&
                            stristr($result, 'Rechazada') == null
                        ) {
                            $result =
                                "<span class='label label-warning'>Pendiente</span>";
                        }
                        if ($check->valida == 0) {
                            $result =
                                "<span class='label label-warning'>Pendiente</span>";
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function aprobadaNivelPrevio($insp, $regUser)
    {
        $control = $this->findAll([
            'funcionario' => $regUser->id,
            'inspeccion' => $insp->id,
            'aprobada' => 1,
            'valida' => 1
        ]);
        if ($control != null) {
            //Si ya el funcionario registrado la aprobo
            return false;
        }

        $previousLevel = NivelesInspeccion::nivelInmediatoInferior(
            $regUser->cargo0
        );
        if ($previousLevel->nivel == '00') {
            return true;
        }

        $cargoInferior = CargosFuncionarios::findOne(
            ['superior' => $regUser->cargo0->id]
        );
        if ($regUser->oficina0->id == $regUser->oficina0->oficina_superior) {
            $funcionariosNivel = Funcionarios::findAll([
                'cargo' => $cargoInferior->id,
                'inactivo = 0'
            ]);
            if ($funcionariosNivel == null) {
                return true;
            }
        } else {
            /*$prefix = substr($insp->numero_control, 0, 4);
             $sub = Oficinas::find()->where('oficina_superior = :s AND codigo = :c', array(':s' => $insp->oficina0->id, ':c' => $prefix));*/
            $sub = Oficinas::findOne(['oficina_superior' => $insp->oficina0->id]);
            $funcionariosNivel = Funcionarios::findAll(
                [
                    'and',
                    ['cargo' => $cargoInferior->id],
                    [
                        'or',
                        ['oficina' => $sub->id],
                        ['oficina' => $insp->oficina]
                    ]
                ],
                ['inactivo = 0']
            );
        }

        if ($funcionariosNivel == null) {
            return false;
        }

        foreach ($funcionariosNivel as $f) {
            $state = $this->findAll([
                'funcionario' => $f->id,
                'inspeccion' => $insp->id,
                'aprobada' => 1,
                'valida' => 1
            ]);
            if ($state != null) {
                return true;
            }
        }

        return false;
    }

    public function ultimaAprobacion($id)
    {
        $tmp = $this->findAll('inspeccion = :i', [':i' => $id]);
        $cantidad = count($tmp);
        if ($tmp == null || $cantidad < 1) {
            return null;
        } else {
            return $tmp[$cantidad - 1];
        }
    }

    public function aprobadaEnOficina($id)
    {
        $tmp = $this->ultimaAprobacion($id);
        if ($tmp != null) {
            if ($tmp->funcionario0->cargo0->id >= 3) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
