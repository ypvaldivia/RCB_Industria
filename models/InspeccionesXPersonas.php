<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inspecciones_x_personas".
 *
 * @property int $id
 * @property int $persona
 * @property int $inspeccion
 * @property string|null $numero
 * @property int $aprobada
 * @property int|null $aprobado_definitivo
 * @property string|null $cprovisional
 * @property string|null $cdefinitivo
 *
 * @property PersonasAcreditadas $persona0
 * @property Inspecciones $inspeccion0
 * @property ShortcutHomologacionesPersonas[] $shortcutHomologacionesPersonas
 */
class InspeccionesXPersonas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspecciones_x_personas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['persona', 'inspeccion'], 'required'],
            [['persona', 'inspeccion', 'aprobada', 'aprobado_definitivo'], 'integer'],
            [['numero'], 'string', 'max' => 2],
            [['cprovisional', 'cdefinitivo'], 'string', 'max' => 100],
            [['persona'], 'exist', 'skipOnError' => true, 'targetClass' => PersonasAcreditadas::className(), 'targetAttribute' => ['persona' => 'id']],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => Inspecciones::className(), 'targetAttribute' => ['inspeccion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'persona' => 'Persona',
            'inspeccion' => 'Inspeccion',
            'numero' => 'Numero',
            'aprobada' => 'Aprobada',
            'aprobado_definitivo' => 'Aprobado Definitivo',
            'cprovisional' => 'Cprovisional',
            'cdefinitivo' => 'Cdefinitivo',
        ];
    }

    /**
     * Gets query for [[Persona0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersona0()
    {
        return $this->hasOne(PersonasAcreditadas::className(), ['id' => 'persona']);
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(Inspecciones::className(), ['id' => 'inspeccion']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesPersonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesPersonas()
    {
        return $this->hasMany(ShortcutHomologacionesPersonas::className(), ['inspeccion' => 'id']);
    }
}
