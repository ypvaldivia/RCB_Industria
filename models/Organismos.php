<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organismos".
 *
 * @property int $id
 * @property string $nombre
 * @property string|null $siglas
 *
 * @property Entidades[] $entidades
 */
class Organismos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organismos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 100],
            [['siglas'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'siglas' => 'Siglas',
        ];
    }

    /**
     * Gets query for [[Entidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntidades()
    {
        return $this->hasMany(Entidades::className(), ['organismo' => 'id']);
    }
}
