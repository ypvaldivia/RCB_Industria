<?php

namespace app\models;

use app\components\Slugger;
use Yii;

/**
 * This is the model class for table "acreditaciones_industriales".
 *
 * @property int $id
 * @property string $codigo
 * @property string $titulo_inspector
 * @property string|null $titulo_anuario
 * @property string|null $map
 * @property int $niveles_inspeccion_id
 * @property string $tipo
 * @property int|null $tipo_certificado
 * @property string $slug
 * @property int|null $categoria
 *
 * @property CategoriasAcreditaciones $categoria0
 * @property TiposCertificados $tipoCertificado
 * @property NivelesInspeccion $nivelesInspeccion
 * @property Inspecciones[] $inspecciones
 * @property ShortcutHomologacionesDacs[] $shortcutHomologacionesDacs
 * @property ShortcutHomologacionesMediosIzado[] $shortcutHomologacionesMediosIzados
 * @property ShortcutHomologacionesPersonas[] $shortcutHomologacionesPersonas
 * @property ShortcutHomologacionesProductos[] $shortcutHomologacionesProductos
 * @property ShortcutHomologacionesServiciosProductos[] $shortcutHomologacionesServiciosProductos
 */
class AcreditacionesIndustriales  extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'acreditaciones_industriales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'titulo_inspector', 'niveles_inspeccion_id', 'tipo', 'slug'], 'required'],
            [['map'], 'string'],
            [['niveles_inspeccion_id', 'tipo_certificado', 'categoria'], 'integer'],
            [['codigo', 'tipo'], 'string', 'max' => 45],
            [['titulo_inspector', 'titulo_anuario'], 'string', 'max' => 150],
            [['slug'], 'string', 'max' => 255],
            [['categoria'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriasAcreditaciones::className(), 'targetAttribute' => ['categoria' => 'id']],
            [['tipo_certificado'], 'exist', 'skipOnError' => true, 'targetClass' => TiposCertificados::className(), 'targetAttribute' => ['tipo_certificado' => 'id']],
            [['niveles_inspeccion_id'], 'exist', 'skipOnError' => true, 'targetClass' => NivelesInspeccion::className(), 'targetAttribute' => ['niveles_inspeccion_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'titulo_inspector' => 'Titulo Inspector',
            'titulo_anuario' => 'Titulo Anuario',
            'map' => 'Map',
            'niveles_inspeccion_id' => 'Niveles Inspeccion ID',
            'tipo' => 'Tipo',
            'tipo_certificado' => 'Tipo Certificado',
            'slug' => 'Slug',
            'categoria' => 'Categoria',
        ];
    }

    /**
     * Gets query for [[Categoria0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria0()
    {
        return $this->hasOne(CategoriasAcreditaciones::className(), ['id' => 'categoria']);
    }

    /**
     * Gets query for [[TipoCertificado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoCertificado()
    {
        return $this->hasOne(TiposCertificados::className(), ['id' => 'tipo_certificado']);
    }

    /**
     * Gets query for [[NivelesInspeccion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNivelesInspeccion()
    {
        return $this->hasOne(NivelesInspeccion::className(), ['id' => 'niveles_inspeccion_id']);
    }

    /**
     * Gets query for [[Inspecciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspecciones()
    {
        return $this->hasMany(Inspecciones::className(), ['homologacion' => 'id']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesDacs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesDacs()
    {
        return $this->hasMany(ShortcutHomologacionesDacs::className(), ['homologacion' => 'id']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesMediosIzados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesMediosIzados()
    {
        return $this->hasMany(ShortcutHomologacionesMediosIzado::className(), ['homologacion' => 'id']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesPersonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesPersonas()
    {
        return $this->hasMany(ShortcutHomologacionesPersonas::className(), ['homologacion' => 'id']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesProductos()
    {
        return $this->hasMany(ShortcutHomologacionesProductos::className(), ['homologacion' => 'id']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesServiciosProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesServiciosProductos()
    {
        return $this->hasMany(ShortcutHomologacionesServiciosProductos::className(), ['homologacion' => 'id']);
    }

    private $tipo_inspeccion = [
        0 => 'Personas',
        1 => 'Servicios',
        2 => 'Productos',
        3 => 'Dispositivos Auxiliares de Carga',
        4 => 'Medios de Izado Terrestres'
    ];

    public function init()
    {
        return parent::init();
    }

    /**
     * Funcion que devuelve TODAS las acreditaciones que no tiene el inspector.
     *
     * @author Levian Lara <levitoon@gmail.com>
     * @parameter  integer $id
     */
    public function getAcreditacionesFaltantes($id)
    {
        $result = Yii::$app->db
            ->createCommand()
            ->select('*')
            ->from('acreditaciones_industriales')
            ->where(
                'id NOT IN (SELECT acreditaciones_inspectores.acreditacion FROM  acreditaciones_inspectores WHERE inspector = :id)',
                [':id' => $id]
            )
            ->queryAll();

        return $result;
    }

    /*
     * Devuelve los tipos posibles de acreditaciones industriales.
     */
    public function getTipoAcreditacion()
    {
        return $this->tipo_inspeccion;
    }

    public function getTipoSeleccionado()
    {
        $select = [];

        if (isset($this->tipo)) {
            foreach ($this->tipo_inspeccion as $t) {
                if ($t == $this->tipo) {
                    $select[$t] = ['selected' => true];
                }
            }
        }

        return $select;
    }

    public function getTipo()
    {
        foreach ($this->tipo_inspeccion as $t => $value) {
            if ($t == $this->tipo) {
                return $value;
            }
        }
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        $slugger = new Slugger();
        $this->slug = $slugger->slugify($this->titulo_anuario);
        return true;
    }
}
