<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "anuario".
 *
 * @property int $id
 * @property string $nombre
 * @property string $dir
 * @property string $archivo
 * @property int $orden
 * @property int|null $mit
 */
class Anuario  extends\yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'anuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'dir', 'archivo'], 'required'],
            [['orden', 'mit'], 'integer'],
            [['nombre', 'dir', 'archivo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'dir' => 'Dir',
            'archivo' => 'Archivo',
            'orden' => 'Orden',
            'mit' => 'Mit',
        ];
    }
}
