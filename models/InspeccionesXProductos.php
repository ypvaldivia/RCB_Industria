<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inspecciones_x_productos".
 *
 * @property int $id
 * @property int|null $producto
 * @property int|null $inspeccion
 * @property string|null $numero
 * @property int $aprobado
 * @property int|null $aprobado_definitivo
 * @property string|null $cprovisional
 * @property string|null $cdefinitivo
 *
 * @property ProductosAcreditados $producto0
 * @property Inspecciones $inspeccion0
 */
class InspeccionesXProductos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspecciones_x_productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto', 'inspeccion', 'aprobado', 'aprobado_definitivo'], 'integer'],
            [['numero'], 'string', 'max' => 3],
            [['cprovisional', 'cdefinitivo'], 'string', 'max' => 100],
            [['producto'], 'exist', 'skipOnError' => true, 'targetClass' => ProductosAcreditados::className(), 'targetAttribute' => ['producto' => 'id']],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => Inspecciones::className(), 'targetAttribute' => ['inspeccion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'producto' => 'Producto',
            'inspeccion' => 'Inspeccion',
            'numero' => 'Numero',
            'aprobado' => 'Aprobado',
            'aprobado_definitivo' => 'Aprobado Definitivo',
            'cprovisional' => 'Cprovisional',
            'cdefinitivo' => 'Cdefinitivo',
        ];
    }

    /**
     * Gets query for [[Producto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto0()
    {
        return $this->hasOne(ProductosAcreditados::className(), ['id' => 'producto']);
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(Inspecciones::className(), ['id' => 'inspeccion']);
    }
}
