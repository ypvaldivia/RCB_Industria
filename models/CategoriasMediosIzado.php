<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorias_medios_izado".
 *
 * @property int $id
 * @property string $categoria
 * @property string|null $descripcion
 * @property int $nivel_especial
 * @property int|null $minimo
 * @property int|null $maximo
 *
 * @property NivelesInspeccion $nivelEspecial
 * @property MediosIzadoAcreditados[] $mediosIzadoAcreditados
 */
class CategoriasMediosIzado  extends\yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias_medios_izado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categoria'], 'required'],
            [['descripcion'], 'string'],
            [['nivel_especial', 'minimo', 'maximo'], 'integer'],
            [['categoria'], 'string', 'max' => 50],
            [['nivel_especial'], 'exist', 'skipOnError' => true, 'targetClass' => NivelesInspeccion::className(), 'targetAttribute' => ['nivel_especial' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoria' => 'Categoria',
            'descripcion' => 'Descripcion',
            'nivel_especial' => 'Nivel Especial',
            'minimo' => 'Minimo',
            'maximo' => 'Maximo',
        ];
    }

    /**
     * Gets query for [[NivelEspecial]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNivelEspecial()
    {
        return $this->hasOne(NivelesInspeccion::className(), ['id' => 'nivel_especial']);
    }

    /**
     * Gets query for [[MediosIzadoAcreditados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMediosIzadoAcreditados()
    {
        return $this->hasMany(MediosIzadoAcreditados::className(), ['categoria' => 'id']);
    }
}
