<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorias_funcionarios".
 *
 * @property int $id
 * @property string $categoria
 * @property string|null $descripcion
 *
 * @property Funcionarios[] $funcionarios
 */
class CategoriasFuncionarios  extends\yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias_funcionarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['categoria'], 'required'],
            [['categoria'], 'string', 'max' => 45],
            [['descripcion'], 'string', 'max' => 200],
            [['categoria'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'categoria' => 'Categoria',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Funcionarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFuncionarios()
    {
        return $this->hasMany(Funcionarios::className(), ['categoria' => 'id']);
    }
}
