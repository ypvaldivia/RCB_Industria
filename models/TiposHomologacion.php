<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipos_homologacion".
 *
 * @property int $id
 * @property string $tipo
 * @property string|null $descripcion
 * @property int $genera_certificado
 *
 * @property Inspecciones[] $inspecciones
 */
class TiposHomologacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipos_homologacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['descripcion'], 'string'],
            [['genera_certificado'], 'integer'],
            [['tipo'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
            'descripcion' => 'Descripcion',
            'genera_certificado' => 'Genera Certificado',
        ];
    }

    /**
     * Gets query for [[Inspecciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspecciones()
    {
        return $this->hasMany(Inspecciones::className(), ['tipo_homologacion' => 'id']);
    }

    public function findOnlyMITS()
    {
        return $this->findAll('tipo LIKE :t', array(':t' => '%MIT%'));
    }

    public function findNotMITS()
    {
        return $this->findAll('tipo NOT LIKE :t', array(':t' => '%MIT%'));
    }

    public function findDAC()
    {
        return $this->findAll('descripcion LIKE :t', array(':t' => '%DAC%'));
    }
}
