<?php

namespace app\models;

use app\components\CDbCriteria;
use Yii;

/**
 * This is the model class for table "inspecciones".
 *
 * @property int $id
 * @property string|null $codigo
 * @property string $numero_control
 * @property int $oficina
 * @property string|null $lugar_inspeccion
 * @property int $homologacion
 * @property string|null $detalle_homologacion
 * @property int|null $nivel
 * @property int|null $tipo_homologacion
 * @property int|null $estado
 * @property string|null $certificado
 * @property string|null $vigencia
 * @property int|null $entidad
 * @property int|null $cerrada
 *
 * @property AprobacionXFuncionarios[] $aprobacionXFuncionarios
 * @property Entidades $entidad0
 * @property Oficinas $oficina0
 * @property AcreditacionesIndustriales $homologacion0
 * @property TiposHomologacion $tipoHomologacion
 * @property EstadosInspecciones $estado0
 * @property NivelesInspeccion $nivel0
 * @property InspeccionesXDacs[] $inspeccionesXDacs
 * @property InspeccionesXMediosIzado[] $inspeccionesXMediosIzados
 * @property InspeccionesXPersonas[] $inspeccionesXPersonas
 * @property InspeccionesXProductos[] $inspeccionesXProductos
 * @property InspeccionesXServiciosProductos[] $inspeccionesXServiciosProductos
 * @property ShortcutHomologacionesDacs[] $shortcutHomologacionesDacs
 * @property ShortcutHomologacionesProductos[] $shortcutHomologacionesProductos
 * @property VisitasInspecciones[] $visitasInspecciones
 */
class Inspecciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspecciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_control', 'oficina', 'homologacion'], 'required'],
            [['oficina', 'homologacion', 'nivel', 'tipo_homologacion', 'estado', 'entidad', 'cerrada'], 'integer'],
            [['vigencia'], 'safe'],
            [['codigo'], 'string', 'max' => 15],
            [['numero_control'], 'string', 'max' => 30],
            [['lugar_inspeccion', 'detalle_homologacion'], 'string', 'max' => 150],
            [['certificado'], 'string', 'max' => 200],
            [['numero_control'], 'unique'],
            [['codigo'], 'unique'],
            [['entidad'], 'exist', 'skipOnError' => true, 'targetClass' => Entidades::className(), 'targetAttribute' => ['entidad' => 'id']],
            [['oficina'], 'exist', 'skipOnError' => true, 'targetClass' => Oficinas::className(), 'targetAttribute' => ['oficina' => 'id']],
            [['homologacion'], 'exist', 'skipOnError' => true, 'targetClass' => AcreditacionesIndustriales::className(), 'targetAttribute' => ['homologacion' => 'id']],
            [['tipo_homologacion'], 'exist', 'skipOnError' => true, 'targetClass' => TiposHomologacion::className(), 'targetAttribute' => ['tipo_homologacion' => 'id']],
            [['estado'], 'exist', 'skipOnError' => true, 'targetClass' => EstadosInspecciones::className(), 'targetAttribute' => ['estado' => 'id']],
            [['nivel'], 'exist', 'skipOnError' => true, 'targetClass' => NivelesInspeccion::className(), 'targetAttribute' => ['nivel' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'numero_control' => 'Numero Control',
            'oficina' => 'Oficina',
            'lugar_inspeccion' => 'Lugar Inspeccion',
            'homologacion' => 'Homologacion',
            'detalle_homologacion' => 'Detalle Homologacion',
            'nivel' => 'Nivel',
            'tipo_homologacion' => 'Tipo Homologacion',
            'estado' => 'Estado',
            'certificado' => 'Certificado',
            'vigencia' => 'Vigencia',
            'entidad' => 'Entidad',
            'cerrada' => 'Cerrada',
        ];
    }

    /**
     * Gets query for [[AprobacionXFuncionarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprobacionXFuncionarios()
    {
        return $this->hasMany(AprobacionXFuncionarios::className(), ['inspeccion' => 'id']);
    }

    /**
     * Gets query for [[Entidad0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntidad0()
    {
        return $this->hasOne(Entidades::className(), ['id' => 'entidad']);
    }

    /**
     * Gets query for [[Oficina0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOficina0()
    {
        return $this->hasOne(Oficinas::className(), ['id' => 'oficina']);
    }

    /**
     * Gets query for [[Homologacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHomologacion0()
    {
        return $this->hasOne(AcreditacionesIndustriales::className(), ['id' => 'homologacion']);
    }

    /**
     * Gets query for [[TipoHomologacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipoHomologacion()
    {
        return $this->hasOne(TiposHomologacion::className(), ['id' => 'tipo_homologacion']);
    }

    /**
     * Gets query for [[Estado0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstado0()
    {
        return $this->hasOne(EstadosInspecciones::className(), ['id' => 'estado']);
    }

    /**
     * Gets query for [[Nivel0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNivel0()
    {
        return $this->hasOne(NivelesInspeccion::className(), ['id' => 'nivel']);
    }

    /**
     * Gets query for [[InspeccionesXDacs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccionesXDacs()
    {
        return $this->hasMany(InspeccionesXDacs::className(), ['inspeccion' => 'id']);
    }

    /**
     * Gets query for [[InspeccionesXMediosIzados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccionesXMediosIzados()
    {
        return $this->hasMany(InspeccionesXMediosIzado::className(), ['inspeccion' => 'id']);
    }

    /**
     * Gets query for [[InspeccionesXPersonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccionesXPersonas()
    {
        return $this->hasMany(InspeccionesXPersonas::className(), ['inspeccion' => 'id']);
    }

    /**
     * Gets query for [[InspeccionesXProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccionesXProductos()
    {
        return $this->hasMany(InspeccionesXProductos::className(), ['inspeccion' => 'id']);
    }

    /**
     * Gets query for [[InspeccionesXServiciosProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccionesXServiciosProductos()
    {
        return $this->hasMany(InspeccionesXServiciosProductos::className(), ['inspeccion' => 'id']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesDacs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesDacs()
    {
        return $this->hasMany(ShortcutHomologacionesDacs::className(), ['inspeccion' => 'id']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesProductos()
    {
        return $this->hasMany(ShortcutHomologacionesProductos::className(), ['inspeccion' => 'id']);
    }

    /**
     * Gets query for [[VisitasInspecciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVisitasInspecciones()
    {
        return $this->hasMany(VisitasInspecciones::className(), ['inspeccion' => 'id']);
    }

    public function convertirFecha($date)
    {
        $m = '';
        $month = substr($date, 5, 2);
        switch ($month) {
            case 1: //id = 1 buzo soldador
                $m = 'enero';
                break;
            case 2: //id = 2 el de soldadores en acero
                $m = 'febrero';
                break;
            case 3: //id = 3 el de inspector de soldadura
                $m = 'marzo';
                break;
            case 4: //id = 3 el de inspector de soldadura
                $m = 'abril';
                break;
            case 5: //id = 3 el de inspector de soldadura
                $m = 'mayo';
                break;
            case 6: //id = 3 el de inspector de soldadura
                $m = 'junio';
                break;
            case 7: //id = 3 el de inspector de soldadura
                $m = 'julio';
                break;
            case 8: //id = 3 el de inspector de soldadura
                $m = 'agosto';
                break;
            case 9: //id = 3 el de inspector de soldadura
                $m = 'septimebre';
                break;
            case 10: //id = 3 el de inspector de soldadura
                $m = 'octubre';
                break;
            case 11: //id = 3 el de inspector de soldadura
                $m = 'noviembre';
                break;
            case 12: //id = 3 el de inspector de soldadura
                $m = 'diciembre';
                break;
        }

        return substr($date, 8, 2) . ' de ' . $m . ' de ' . substr($date, 0, 4);
    }

    public function convertirFechaShort($date)
    {
        return substr($date, 8, 2) .
            '/' .
            substr($date, 5, 2) .
            '/' .
            substr($date, 0, 4);
    }

    public function personasAprobadas()
    {
        return InspeccionesXPersonas::findAll(
            'inspeccion = :i AND aprobada = :a',
            [':i' => $this->id, ':a' => 1]
        );
    }

    public function personasNoAprobadas()
    {
        return InspeccionesXPersonas::findAll(
            'inspeccion = :i AND aprobada = :a',
            [':i' => $this->id, ':a' => 0]
        );
    }

    public function productosAprobados()
    {
        return InspeccionesXProductos::findAll(
            'inspeccion = :i AND aprobado = :a',
            array(':i' => $this->id, ':a' => 1)
        );
    }

    public function productosNoAprobados()
    {
        return InspeccionesXProductos::findAll(
            'inspeccion = :i AND aprobado = :a',
            [':i' => $this->id, ':a' => 0]
        );
    }

    public function dacsAprobados()
    {
        return InspeccionesXDacs::findAll(
            'inspeccion = :i AND aprobado = :a',
            [':i' => $this->id, ':a' => 1]
        );
    }

    public function dacsNoAprobados()
    {
        return InspeccionesXDacs::findAll(
            'inspeccion = :i AND aprobado = :a',
            [':i' => $this->id, ':a' => 0]
        );
    }

    public function mitsAprobados()
    {
        return InspeccionesXMediosIzado::findAll(
            'inspeccion = :i AND aprobado = :a',
            [':i' => $this->id, ':a' => 1]
        );
    }

    public function mitsNoAprobados()
    {
        return InspeccionesXMediosIzado::findAll(
            'inspeccion = :i AND aprobado = :a',
            [':i' => $this->id, ':a' => 0]
        );
    }

    public function serviciosAprobados()
    {
        return InspeccionesXServiciosProductos::findAll(
            'inspeccion = :i AND aprobado = :a',
            [':i' => $this->id, ':a' => 1]
        );
    }

    public function serviciosNoAprobados()
    {
        return InspeccionesXServiciosProductos::findAll(
            'inspeccion = :i AND aprobado = :a',
            [':i' => $this->id, ':a' => 0]
        );
    }

    public function getUltimaVisita()
    {
        return $this->visitasInspecciones[count($this->visitasInspecciones) - 1];
    }

    public function getInspectoresActuantes()
    {
        $result = [];
        foreach ($this->visitasInspecciones as $v) {
            $actuantes = InspectoresActuantes::findAll('visita = :v', [':v' => $v->id]);
            if ($actuantes != null) {
                foreach ($actuantes as $a) {
                    $flag = false;
                    foreach ($result as $r) {
                        if ($r->id == $a->inspector0->id) {
                            $flag = true;
                        }
                    }
                    if (!$flag) {
                        array_push($result, $a->inspector0);
                    }
                }
            }
        }
        return $result;
    }

    public function getTieneVisitas()
    {
        return $this->visitasInspecciones != null ? true : false;
    }

    public function getAllPorAprobar($tipo)
    {
        $criteria = new CDbCriteria();
        $criteria->alias = 'i';
        $criteria->condition = 'estado > 1';
        $criteria->group = 'i.estado';
        $criteria->order = 'i.estado DESC';
        $criteria->limit = 10;

        $criteria2 = new CDbCriteria();
        $criteria2->alias = 'ip';
        $criteria2->select = 'inspeccion';

        $InsPer = InspeccionesXPersonas::findAll($criteria2);
        $llavesPer = [];
        foreach ($InsPer as $insp) {
            $llavesPer[] = $insp->inspeccion;
        }

        $InsProd = InspeccionesXProductos::findAll($criteria2);
        $llavesProd = [];
        foreach ($InsProd as $insp) {
            $llavesProd[] = $insp->inspeccion;
        }

        $InsMits = InspeccionesXMediosIzado::findAll($criteria2);
        $llavesMits = [];
        foreach ($InsMits as $insp) {
            $llavesMits[] = $insp->inspeccion;
        }

        $InsDacs = InspeccionesXMediosIzado::findAll($criteria2);
        $llavesDacs = [];
        foreach ($InsDacs as $insp) {
            $llavesDacs[] = $insp->inspeccion;
        }

        $InsServ = InspeccionesXServiciosProductos::findAll($criteria2);
        $llavesServ = [];
        foreach ($InsServ as $insp) {
            $llavesServ[] = $insp->inspeccion;
        }

        switch ($tipo) {
            case 'personas':
                $criteria->addInCondition('i.id', $llavesPer);
                break;
            case 'productos':
                $criteria->addInCondition('i.id', $llavesProd);
                break;
            case 'servicios':
                $criteria->addInCondition('i.id', $llavesServ);
                break;
            case 'mits':
                $criteria->addInCondition('i.id', $llavesMits);
                break;
            case 'dacs':
                $criteria->addInCondition('i.id', $llavesDacs);
                break;
        }

        $inspecciones = self::find()
            ->select([
                'homologacion0.titulo_inspector',
                'oficina0.nombre'
            ])
            ->where($criteria)
            ->all();

        return $inspecciones;
    }

    public function getNivelOficina()
    {
        return NivelesInspeccion::findOne(3);
    }

    public function getNivelAprobacionMIT()
    {
        $categoria =
            $this->inspeccionesXMediosIzados[0]->medioIzado->categoria0;
        if ($categoria != null and $this->homologacion != 94) {
            return $categoria->nivelEspecial;
        } else {
            return NivelesInspeccion::findOne(5);
        }
    }

    public function inspeccionCerrada($tipo)
    {
        $countNoAprobadas = 0;
        $obj = null;
        switch ($tipo) {
            case 'personas':
                $obj = InspeccionesXPersonas::findAll(['inspeccion' => $this->id]);
                if ($obj != null) {
                    foreach ($obj as $i) {
                        if ($i->aprobado_definitivo == 0) {
                            $countNoAprobadas++;
                        }
                    }
                    return $countNoAprobadas == 0;
                } else {
                    return true;
                }
                break;
            case 'productos':
                $obj = InspeccionesXProductos::findAll(['inspeccion' => $this->id]);
                if ($obj != null) {
                    foreach ($obj as $i) {
                        if ($i->aprobado_definitivo == 0) {
                            $countNoAprobadas++;
                        }
                    }
                    return $countNoAprobadas == 0;
                } else {
                    return true;
                }
                break;
            case 'servicios':
                $obj = InspeccionesXServiciosProductos::findAll(
                    ['inspeccion' => $this->id]
                );
                if ($obj != null) {
                    foreach ($obj as $i) {
                        if ($i->aprobado_definitivo == 0) {
                            $countNoAprobadas++;
                        }
                    }
                    return $countNoAprobadas == 0;
                } else {
                    return true;
                }
                break;
            case 'mits':
                $obj = InspeccionesXMediosIzado::findAll(
                    ['inspeccion' => $this->id]
                );
                if ($obj != null) {
                    foreach ($obj as $i) {
                        if ($i->aprobado_definitivo == 0) {
                            $countNoAprobadas++;
                        }
                    }
                    return $countNoAprobadas == 0;
                } else {
                    return true;
                }
                break;
            case 'dacs':
                $obj = InspeccionesXDacs::findAll(['inspeccion' => $this->id]);
                if ($obj != null) {
                    foreach ($obj as $i) {
                        if ($i->aprobado_definitivo == 0) {
                            $countNoAprobadas++;
                        }
                    }
                    return $countNoAprobadas == 0;
                } else {
                    return true;
                }
                break;
        }
    }

    public function containInspector($inspector)
    {
        $inspectorAutenticado = Funcionarios::findOne(['usuario' => $inspector]);
        if ($inspectorAutenticado == null) {
            return false;
        }
        $visitas = VisitasInspecciones::findAll(['inspeccion' => $this->id]);
        if ($visitas == null) {
            return false;
        }
        foreach ($visitas as $v) {
            $actuante = InspectoresActuantes::findOne(
                [
                    'visita' => $v->id,
                    'inspector' => $inspectorAutenticado->id
                ]
            );
            if ($actuante != null) {
                return true;
            }
        }
        return false;
    }
}
