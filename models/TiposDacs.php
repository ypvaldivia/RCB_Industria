<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipos_dacs".
 *
 * @property int $id
 * @property string $tipo
 * @property int $meses_vigencia
 *
 * @property DacsAcreditados[] $dacsAcreditados
 */
class TiposDacs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipos_dacs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo'], 'required'],
            [['meses_vigencia'], 'integer'],
            [['tipo'], 'string', 'max' => 200],
            [['tipo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
            'meses_vigencia' => 'Meses Vigencia',
        ];
    }

    /**
     * Gets query for [[DacsAcreditados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDacsAcreditados()
    {
        return $this->hasMany(DacsAcreditados::className(), ['tipo' => 'id']);
    }
}
