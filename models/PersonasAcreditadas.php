<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personas_acreditadas".
 *
 * @property int $id
 * @property string $nombre_apellidos
 * @property string|null $ci
 * @property string|null $lugar_nacimiento
 * @property string|null $telefono_contacto
 * @property string|null $correo
 * @property int $provincia
 * @property int|null $entidad 479
 * @property string|null $foto
 *
 * @property InspeccionesXPersonas[] $inspeccionesXPersonas
 * @property Entidades $entidad0
 * @property Provincias $provincia0
 * @property ShortcutHomologacionesPersonas[] $shortcutHomologacionesPersonas
 */
class PersonasAcreditadas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personas_acreditadas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_apellidos', 'provincia'], 'required'],
            [['provincia', 'entidad'], 'integer'],
            [['nombre_apellidos'], 'string', 'max' => 200],
            [['ci'], 'string', 'max' => 11],
            [['lugar_nacimiento', 'telefono_contacto', 'correo', 'foto'], 'string', 'max' => 45],
            [['entidad'], 'exist', 'skipOnError' => true, 'targetClass' => Entidades::className(), 'targetAttribute' => ['entidad' => 'id']],
            [['provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['provincia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre_apellidos' => 'Nombre Apellidos',
            'ci' => 'Ci',
            'lugar_nacimiento' => 'Lugar Nacimiento',
            'telefono_contacto' => 'Telefono Contacto',
            'correo' => 'Correo',
            'provincia' => 'Provincia',
            'entidad' => 'Entidad',
            'foto' => 'Foto',
        ];
    }

    /**
     * Gets query for [[InspeccionesXPersonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccionesXPersonas()
    {
        return $this->hasMany(InspeccionesXPersonas::className(), ['persona' => 'id']);
    }

    /**
     * Gets query for [[Entidad0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntidad0()
    {
        return $this->hasOne(Entidades::className(), ['id' => 'entidad']);
    }

    /**
     * Gets query for [[Provincia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia0()
    {
        return $this->hasOne(Provincias::className(), ['id' => 'provincia']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesPersonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesPersonas()
    {
        return $this->hasMany(ShortcutHomologacionesPersonas::className(), ['persona' => 'id']);
    }

    public function getFullPerson()
    {
        return $this->nombre_apellidos . ' (CI: ' . $this->ci . ')';
    }
}
