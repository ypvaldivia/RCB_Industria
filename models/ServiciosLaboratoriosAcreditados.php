<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicios_laboratorios_acreditados".
 *
 * @property int $id
 * @property string $departamento
 * @property int $entidad
 * @property int|null $provincia
 * @property string $servicio
 * @property string|null $certificado
 * @property string|null $vigencia
 *
 * @property InspeccionesXServiciosProductos[] $inspeccionesXServiciosProductos
 * @property Entidades $entidad0
 * @property Provincias $provincia0
 * @property ShortcutHomologacionesServiciosProductos[] $shortcutHomologacionesServiciosProductos
 */
class ServiciosLaboratoriosAcreditados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servicios_laboratorios_acreditados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['departamento', 'entidad', 'servicio'], 'required'],
            [['entidad', 'provincia'], 'integer'],
            [['servicio'], 'string'],
            [['vigencia'], 'safe'],
            [['departamento'], 'string', 'max' => 150],
            [['certificado'], 'string', 'max' => 20],
            [['entidad'], 'exist', 'skipOnError' => true, 'targetClass' => Entidades::className(), 'targetAttribute' => ['entidad' => 'id']],
            [['provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['provincia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'departamento' => 'Departamento',
            'entidad' => 'Entidad',
            'provincia' => 'Provincia',
            'servicio' => 'Servicio',
            'certificado' => 'Certificado',
            'vigencia' => 'Vigencia',
        ];
    }

    /**
     * Gets query for [[InspeccionesXServiciosProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccionesXServiciosProductos()
    {
        return $this->hasMany(InspeccionesXServiciosProductos::className(), ['servcios' => 'id']);
    }

    /**
     * Gets query for [[Entidad0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntidad0()
    {
        return $this->hasOne(Entidades::className(), ['id' => 'entidad']);
    }

    /**
     * Gets query for [[Provincia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia0()
    {
        return $this->hasOne(Provincias::className(), ['id' => 'provincia']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesServiciosProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesServiciosProductos()
    {
        return $this->hasMany(ShortcutHomologacionesServiciosProductos::className(), ['servicio' => 'id']);
    }

    public function getFullID()
    {
        return $this->servicio .
            ', ' .
            $this->departamento .
            ' (' .
            $this->entidad0->nombre .
            ')';
    }
}
