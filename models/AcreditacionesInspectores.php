<?php

namespace app\models;

use DateTime;
use Yii;

/**
 * This is the model class for table "acreditaciones_inspectores".
 *
 * @property int $id
 * @property int $acreditacion
 * @property int $inspector
 * @property string|null $emision
 * @property string $vencimiento
 * @property string|null $actualizado
 *
 * @property Funcionarios $inspector0
 * @property CategoriasAcreditaciones $acreditacion0
 */
class AcreditacionesInspectores  extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'acreditaciones_inspectores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['acreditacion', 'inspector', 'vencimiento'], 'required'],
            [['acreditacion', 'inspector'], 'integer'],
            [['emision', 'vencimiento', 'actualizado'], 'safe'],
            [['inspector'], 'exist', 'skipOnError' => true, 'targetClass' => Funcionarios::className(), 'targetAttribute' => ['inspector' => 'id']],
            [['acreditacion'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriasAcreditaciones::className(), 'targetAttribute' => ['acreditacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'acreditacion' => 'Acreditacion',
            'inspector' => 'Inspector',
            'emision' => 'Emision',
            'vencimiento' => 'Vencimiento',
            'actualizado' => 'Actualizado',
        ];
    }

    /**
     * Gets query for [[Inspector0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspector0()
    {
        return $this->hasOne(Funcionarios::className(), ['id' => 'inspector']);
    }

    /**
     * Gets query for [[Acreditacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAcreditacion0()
    {
        return $this->hasOne(CategoriasAcreditaciones::className(), ['id' => 'acreditacion']);
    }

    /**
     * Funcion usada para resaltar visualmente el estado de una acreditacion para un inspector,
     * NO lleva parametro puesto que se llama dentro de un bucle.
     *
     * @author Levian Lara <levitoon@gmail.com>
     */
    public function getEstado()
    {
        $ahora = date('Y-m-d');
        $tmp = date_create($ahora);
        $tresm = date_add(
            $tmp,
            date_interval_create_from_date_string('+3 months')
        );
        if ($this->vencimiento < $ahora) {
            return 'danger';
        }
        if (
            date_create($this->vencimiento) > $ahora &&
            date_create($this->vencimiento) < $tresm
        ) {
            return 'warning';
        } else {
            return 'success';
        }
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        $this->actualizado = new DateTime();
        return true;
    }
}
