<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inspecciones_x_medios_izado".
 *
 * @property int $id
 * @property int $medio_izado
 * @property int $inspeccion
 * @property string|null $numero
 * @property int $aprobado
 * @property int|null $aprobado_definitivo
 * @property string|null $cprovisional
 * @property string|null $cdefinitivo
 * @property string|null $rig
 *
 * @property MediosIzadoAcreditados $medioIzado
 * @property Inspecciones $inspeccion0
 * @property ShortcutHomologacionesMediosIzado[] $shortcutHomologacionesMediosIzados
 */
class InspeccionesXMediosIzado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspecciones_x_medios_izado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['medio_izado', 'inspeccion'], 'required'],
            [['medio_izado', 'inspeccion', 'aprobado', 'aprobado_definitivo'], 'integer'],
            [['numero'], 'string', 'max' => 3],
            [['cprovisional', 'cdefinitivo'], 'string', 'max' => 100],
            [['rig'], 'string', 'max' => 30],
            [['medio_izado'], 'exist', 'skipOnError' => true, 'targetClass' => MediosIzadoAcreditados::className(), 'targetAttribute' => ['medio_izado' => 'id']],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => Inspecciones::className(), 'targetAttribute' => ['inspeccion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'medio_izado' => 'Medio Izado',
            'inspeccion' => 'Inspeccion',
            'numero' => 'Numero',
            'aprobado' => 'Aprobado',
            'aprobado_definitivo' => 'Aprobado Definitivo',
            'cprovisional' => 'Cprovisional',
            'cdefinitivo' => 'Cdefinitivo',
            'rig' => 'Rig',
        ];
    }

    /**
     * Gets query for [[MedioIzado]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMedioIzado()
    {
        return $this->hasOne(MediosIzadoAcreditados::className(), ['id' => 'medio_izado']);
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(Inspecciones::className(), ['id' => 'inspeccion']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesMediosIzados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesMediosIzados()
    {
        return $this->hasMany(ShortcutHomologacionesMediosIzado::className(), ['inspeccion' => 'id']);
    }
}
