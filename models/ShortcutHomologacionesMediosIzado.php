<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shortcut_homologaciones_medios_izado".
 *
 * @property int $id
 * @property int $homologacion
 * @property int $medio_izaje
 * @property string|null $certificado
 * @property string|null $emision
 * @property string|null $vigencia
 * @property string|null $anho_vigencia
 * @property int|null $inspeccion
 *
 * @property InspeccionesXMediosIzado $inspeccion0
 * @property AcreditacionesIndustriales $homologacion0
 * @property MediosIzadoAcreditados $medioIzaje
 */
class ShortcutHomologacionesMediosIzado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shortcut_homologaciones_medios_izado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['homologacion', 'medio_izaje'], 'required'],
            [['homologacion', 'medio_izaje', 'inspeccion'], 'integer'],
            [['emision', 'vigencia', 'anho_vigencia'], 'safe'],
            [['certificado'], 'string', 'max' => 20],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => InspeccionesXMediosIzado::className(), 'targetAttribute' => ['inspeccion' => 'id']],
            [['homologacion'], 'exist', 'skipOnError' => true, 'targetClass' => AcreditacionesIndustriales::className(), 'targetAttribute' => ['homologacion' => 'id']],
            [['medio_izaje'], 'exist', 'skipOnError' => true, 'targetClass' => MediosIzadoAcreditados::className(), 'targetAttribute' => ['medio_izaje' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'homologacion' => 'Homologacion',
            'medio_izaje' => 'Medio Izaje',
            'certificado' => 'Certificado',
            'emision' => 'Emision',
            'vigencia' => 'Vigencia',
            'anho_vigencia' => 'Anho Vigencia',
            'inspeccion' => 'Inspeccion',
        ];
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(InspeccionesXMediosIzado::className(), ['id' => 'inspeccion']);
    }

    /**
     * Gets query for [[Homologacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHomologacion0()
    {
        return $this->hasOne(AcreditacionesIndustriales::className(), ['id' => 'homologacion']);
    }

    /**
     * Gets query for [[MedioIzaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMedioIzaje()
    {
        return $this->hasOne(MediosIzadoAcreditados::className(), ['id' => 'medio_izaje']);
    }
}
