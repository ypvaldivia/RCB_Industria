<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shortcut_homologaciones_servicios_productos".
 *
 * @property int $id
 * @property int $homologacion
 * @property int $servicio
 * @property string|null $certificado
 * @property string|null $emision
 * @property string|null $vigencia
 * @property string|null $anho_vigencia
 * @property int|null $inspeccion
 *
 * @property InspeccionesXServiciosProductos $inspeccion0
 * @property AcreditacionesIndustriales $homologacion0
 * @property ServiciosLaboratoriosAcreditados $servicio0
 */
class ShortcutHomologacionesServiciosProductos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shortcut_homologaciones_servicios_productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['homologacion', 'servicio'], 'required'],
            [['homologacion', 'servicio', 'inspeccion'], 'integer'],
            [['emision', 'vigencia', 'anho_vigencia'], 'safe'],
            [['certificado'], 'string', 'max' => 20],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => InspeccionesXServiciosProductos::className(), 'targetAttribute' => ['inspeccion' => 'id']],
            [['homologacion'], 'exist', 'skipOnError' => true, 'targetClass' => AcreditacionesIndustriales::className(), 'targetAttribute' => ['homologacion' => 'id']],
            [['servicio'], 'exist', 'skipOnError' => true, 'targetClass' => ServiciosLaboratoriosAcreditados::className(), 'targetAttribute' => ['servicio' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'homologacion' => 'Homologacion',
            'servicio' => 'Servicio',
            'certificado' => 'Certificado',
            'emision' => 'Emision',
            'vigencia' => 'Vigencia',
            'anho_vigencia' => 'Anho Vigencia',
            'inspeccion' => 'Inspeccion',
        ];
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(InspeccionesXServiciosProductos::className(), ['id' => 'inspeccion']);
    }

    /**
     * Gets query for [[Homologacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHomologacion0()
    {
        return $this->hasOne(AcreditacionesIndustriales::className(), ['id' => 'homologacion']);
    }

    /**
     * Gets query for [[Servicio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicio0()
    {
        return $this->hasOne(ServiciosLaboratoriosAcreditados::className(), ['id' => 'servicio']);
    }
}
