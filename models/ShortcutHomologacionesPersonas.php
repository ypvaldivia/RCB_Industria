<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shortcut_homologaciones_personas".
 *
 * @property int $id
 * @property int $homologacion
 * @property int $persona
 * @property string $certificado
 * @property string|null $emision
 * @property string $vigente
 * @property string|null $anho_vigente
 * @property int|null $inspeccion
 *
 * @property InspeccionesXPersonas $inspeccion0
 * @property AcreditacionesIndustriales $homologacion0
 * @property PersonasAcreditadas $persona0
 */
class ShortcutHomologacionesPersonas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shortcut_homologaciones_personas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['homologacion', 'persona', 'certificado', 'vigente'], 'required'],
            [['homologacion', 'persona', 'inspeccion'], 'integer'],
            [['emision', 'vigente', 'anho_vigente'], 'safe'],
            [['certificado'], 'string', 'max' => 20],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => InspeccionesXPersonas::className(), 'targetAttribute' => ['inspeccion' => 'id']],
            [['homologacion'], 'exist', 'skipOnError' => true, 'targetClass' => AcreditacionesIndustriales::className(), 'targetAttribute' => ['homologacion' => 'id']],
            [['persona'], 'exist', 'skipOnError' => true, 'targetClass' => PersonasAcreditadas::className(), 'targetAttribute' => ['persona' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'homologacion' => 'Homologacion',
            'persona' => 'Persona',
            'certificado' => 'Certificado',
            'emision' => 'Emision',
            'vigente' => 'Vigente',
            'anho_vigente' => 'Anho Vigente',
            'inspeccion' => 'Inspeccion',
        ];
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(InspeccionesXPersonas::className(), ['id' => 'inspeccion']);
    }

    /**
     * Gets query for [[Homologacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHomologacion0()
    {
        return $this->hasOne(AcreditacionesIndustriales::className(), ['id' => 'homologacion']);
    }

    /**
     * Gets query for [[Persona0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersona0()
    {
        return $this->hasOne(PersonasAcreditadas::className(), ['id' => 'persona']);
    }
}
