<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entidades".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $siglas
 * @property int|null $organismo
 * @property string|null $persona_contacto
 * @property string|null $telefono_contacto
 * @property string|null $correo
 * @property string|null $direccion
 * @property int|null $provincia
 * @property int|null $oficina_acredita
 *
 * @property DacsAcreditados[] $dacsAcreditados
 * @property Oficinas $oficinaAcredita
 * @property Organismos $organismo0
 * @property Provincias $provincia0
 * @property Inspecciones[] $inspecciones
 * @property MediosIzadoAcreditados[] $mediosIzadoAcreditados
 * @property PersonasAcreditadas[] $personasAcreditadas
 * @property ProductosAcreditados[] $productosAcreditados
 * @property ServiciosLaboratoriosAcreditados[] $serviciosLaboratoriosAcreditados
 */
class Entidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organismo', 'provincia', 'oficina_acredita'], 'integer'],
            [['nombre', 'direccion'], 'string', 'max' => 200],
            [['siglas', 'correo'], 'string', 'max' => 45],
            [['persona_contacto'], 'string', 'max' => 80],
            [['telefono_contacto'], 'string', 'max' => 100],
            [['oficina_acredita'], 'exist', 'skipOnError' => true, 'targetClass' => Oficinas::className(), 'targetAttribute' => ['oficina_acredita' => 'id']],
            [['organismo'], 'exist', 'skipOnError' => true, 'targetClass' => Organismos::className(), 'targetAttribute' => ['organismo' => 'id']],
            [['provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['provincia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'siglas' => 'Siglas',
            'organismo' => 'Organismo',
            'persona_contacto' => 'Persona Contacto',
            'telefono_contacto' => 'Telefono Contacto',
            'correo' => 'Correo',
            'direccion' => 'Direccion',
            'provincia' => 'Provincia',
            'oficina_acredita' => 'Oficina Acredita',
        ];
    }

    /**
     * Gets query for [[DacsAcreditados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDacsAcreditados()
    {
        return $this->hasMany(DacsAcreditados::className(), ['propietario' => 'id']);
    }

    /**
     * Gets query for [[OficinaAcredita]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOficinaAcredita()
    {
        return $this->hasOne(Oficinas::className(), ['id' => 'oficina_acredita']);
    }

    /**
     * Gets query for [[Organismo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrganismo0()
    {
        return $this->hasOne(Organismos::className(), ['id' => 'organismo']);
    }

    /**
     * Gets query for [[Provincia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia0()
    {
        return $this->hasOne(Provincias::className(), ['id' => 'provincia']);
    }

    /**
     * Gets query for [[Inspecciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspecciones()
    {
        return $this->hasMany(Inspecciones::className(), ['entidad' => 'id']);
    }

    /**
     * Gets query for [[MediosIzadoAcreditados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMediosIzadoAcreditados()
    {
        return $this->hasMany(MediosIzadoAcreditados::className(), ['propietario' => 'id']);
    }

    /**
     * Gets query for [[PersonasAcreditadas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonasAcreditadas()
    {
        return $this->hasMany(PersonasAcreditadas::className(), ['entidad' => 'id']);
    }

    /**
     * Gets query for [[ProductosAcreditados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosAcreditados()
    {
        return $this->hasMany(ProductosAcreditados::className(), ['propietario' => 'id']);
    }

    /**
     * Gets query for [[ServiciosLaboratoriosAcreditados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServiciosLaboratoriosAcreditados()
    {
        return $this->hasMany(ServiciosLaboratoriosAcreditados::className(), ['entidad' => 'id']);
    }
}
