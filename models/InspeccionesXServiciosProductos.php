<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inspecciones_x_servicios_productos".
 *
 * @property int $id
 * @property int $servcios
 * @property int $inspeccion
 * @property string|null $numero
 * @property string|null $cprovisional
 * @property string|null $cdefinitivo
 * @property int|null $aprobado
 * @property int|null $aprobado_definitivo
 *
 * @property ServiciosLaboratoriosAcreditados $servcios0
 * @property Inspecciones $inspeccion0
 * @property ShortcutHomologacionesServiciosProductos[] $shortcutHomologacionesServiciosProductos
 */
class InspeccionesXServiciosProductos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspecciones_x_servicios_productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['servcios', 'inspeccion'], 'required'],
            [['servcios', 'inspeccion', 'aprobado', 'aprobado_definitivo'], 'integer'],
            [['numero'], 'string', 'max' => 3],
            [['cprovisional', 'cdefinitivo'], 'string', 'max' => 100],
            [['servcios'], 'exist', 'skipOnError' => true, 'targetClass' => ServiciosLaboratoriosAcreditados::className(), 'targetAttribute' => ['servcios' => 'id']],
            [['inspeccion'], 'exist', 'skipOnError' => true, 'targetClass' => Inspecciones::className(), 'targetAttribute' => ['inspeccion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'servcios' => 'Servcios',
            'inspeccion' => 'Inspeccion',
            'numero' => 'Numero',
            'cprovisional' => 'Cprovisional',
            'cdefinitivo' => 'Cdefinitivo',
            'aprobado' => 'Aprobado',
            'aprobado_definitivo' => 'Aprobado Definitivo',
        ];
    }

    /**
     * Gets query for [[Servcios0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServcios0()
    {
        return $this->hasOne(ServiciosLaboratoriosAcreditados::className(), ['id' => 'servcios']);
    }

    /**
     * Gets query for [[Inspeccion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccion0()
    {
        return $this->hasOne(Inspecciones::className(), ['id' => 'inspeccion']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesServiciosProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesServiciosProductos()
    {
        return $this->hasMany(ShortcutHomologacionesServiciosProductos::className(), ['inspeccion' => 'id']);
    }
}
