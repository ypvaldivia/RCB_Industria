<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "medios_izado_acreditados".
 *
 * @property int $id
 * @property string $nombre
 * @property int|null $tipo
 * @property int|null $categoria
 * @property int $propietario
 * @property int|null $provincia
 * @property float|null $ci_certificada
 * @property float|null $ci_nominal
 * @property float|null $ci_aux_certificada
 * @property float|null $ci_aux_nominal
 * @property float|null $ci_aux1_certificada
 * @property float|null $ci_aux1_nominal
 *
 * @property GaleriaMediosIzado[] $galeriaMediosIzados
 * @property InspeccionesXMediosIzado[] $inspeccionesXMediosIzados
 * @property CategoriasMediosIzado $categoria0
 * @property TiposMediosIzado $tipo0
 * @property Entidades $propietario0
 * @property Provincias $provincia0
 * @property ShortcutHomologacionesMediosIzado[] $shortcutHomologacionesMediosIzados
 */
class MediosIzadoAcreditados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'medios_izado_acreditados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'propietario'], 'required'],
            [['tipo', 'categoria', 'propietario', 'provincia'], 'integer'],
            [['ci_certificada', 'ci_nominal', 'ci_aux_certificada', 'ci_aux_nominal', 'ci_aux1_certificada', 'ci_aux1_nominal'], 'number'],
            [['nombre'], 'string', 'max' => 100],
            [['categoria'], 'exist', 'skipOnError' => true, 'targetClass' => CategoriasMediosIzado::className(), 'targetAttribute' => ['categoria' => 'id']],
            [['tipo'], 'exist', 'skipOnError' => true, 'targetClass' => TiposMediosIzado::className(), 'targetAttribute' => ['tipo' => 'id']],
            [['propietario'], 'exist', 'skipOnError' => true, 'targetClass' => Entidades::className(), 'targetAttribute' => ['propietario' => 'id']],
            [['provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['provincia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'tipo' => 'Tipo',
            'categoria' => 'Categoria',
            'propietario' => 'Propietario',
            'provincia' => 'Provincia',
            'ci_certificada' => 'Ci Certificada',
            'ci_nominal' => 'Ci Nominal',
            'ci_aux_certificada' => 'Ci Aux Certificada',
            'ci_aux_nominal' => 'Ci Aux Nominal',
            'ci_aux1_certificada' => 'Ci Aux1 Certificada',
            'ci_aux1_nominal' => 'Ci Aux1 Nominal',
        ];
    }

    /**
     * Gets query for [[GaleriaMediosIzados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGaleriaMediosIzados()
    {
        return $this->hasMany(GaleriaMediosIzado::className(), ['medio_izaje' => 'id']);
    }

    /**
     * Gets query for [[InspeccionesXMediosIzados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccionesXMediosIzados()
    {
        return $this->hasMany(InspeccionesXMediosIzado::className(), ['medio_izado' => 'id']);
    }

    /**
     * Gets query for [[Categoria0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria0()
    {
        return $this->hasOne(CategoriasMediosIzado::className(), ['id' => 'categoria']);
    }

    /**
     * Gets query for [[Tipo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTipo0()
    {
        return $this->hasOne(TiposMediosIzado::className(), ['id' => 'tipo']);
    }

    /**
     * Gets query for [[Propietario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPropietario0()
    {
        return $this->hasOne(Entidades::className(), ['id' => 'propietario']);
    }

    /**
     * Gets query for [[Provincia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia0()
    {
        return $this->hasOne(Provincias::className(), ['id' => 'provincia']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesMediosIzados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesMediosIzados()
    {
        return $this->hasMany(ShortcutHomologacionesMediosIzado::className(), ['medio_izaje' => 'id']);
    }

    public function getFullID()
    {
        return $this->nombre .
            ' (' .
            $this->propietario0->nombre .
            ', ' .
            $this->provincia0->nombre .
            ') ';
    }
}
