<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "oficinas".
 *
 * @property int $id
 * @property string $nombre
 * @property string $codigo
 * @property string|null $correo
 * @property string|null $direccion
 * @property string|null $telefonos
 * @property int $provincia
 * @property int|null $oficina_superior
 *
 * @property Entidades[] $entidades
 * @property Funcionarios[] $funcionarios
 * @property Inspecciones[] $inspecciones
 * @property Oficinas $oficinaSuperior
 * @property Oficinas[] $oficinas
 * @property Provincias $provincia0
 */
class Oficinas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'oficinas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'codigo', 'provincia'], 'required'],
            [['provincia', 'oficina_superior'], 'integer'],
            [['nombre', 'correo'], 'string', 'max' => 100],
            [['codigo'], 'string', 'max' => 45],
            [['direccion'], 'string', 'max' => 200],
            [['telefonos'], 'string', 'max' => 50],
            [['oficina_superior'], 'exist', 'skipOnError' => true, 'targetClass' => Oficinas::className(), 'targetAttribute' => ['oficina_superior' => 'id']],
            [['provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['provincia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
            'correo' => 'Correo',
            'direccion' => 'Direccion',
            'telefonos' => 'Telefonos',
            'provincia' => 'Provincia',
            'oficina_superior' => 'Oficina Superior',
        ];
    }

    /**
     * Gets query for [[Entidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntidades()
    {
        return $this->hasMany(Entidades::className(), ['oficina_acredita' => 'id']);
    }

    /**
     * Gets query for [[Funcionarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFuncionarios()
    {
        return $this->hasMany(Funcionarios::className(), ['oficina' => 'id']);
    }

    /**
     * Gets query for [[Inspecciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspecciones()
    {
        return $this->hasMany(Inspecciones::className(), ['oficina' => 'id']);
    }

    /**
     * Gets query for [[OficinaSuperior]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOficinaSuperior()
    {
        return $this->hasOne(Oficinas::className(), ['id' => 'oficina_superior']);
    }

    /**
     * Gets query for [[Oficinas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOficinas()
    {
        return $this->hasMany(Oficinas::className(), ['oficina_superior' => 'id']);
    }

    /**
     * Gets query for [[Provincia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia0()
    {
        return $this->hasOne(Provincias::className(), ['id' => 'provincia']);
    }

    public function getInspectionOffices()
    {
        $primaryOffice = $this->find('oficina_superior = id');

        return $this->findAll(
            'oficina_superior = :os AND oficina_superior != id',
            array(':os' => $primaryOffice->id)
        );
    }

    public function getOficinaUsuarioReg()
    {
        $user = Funcionarios::find()->where(['usuario' => Yii::$app->user->id])->one();
        $central = self::find()->where('oficina_superior = id')->one();
        if ($user == null) {
            return $this::find()->where('<>', 'id', $central->id);
        } else {
            if ($user->oficina0->id == $central->id) {
                return $this->findAll('id != :s', array(':s' => $central->id));
            } else {
                if ($user->oficina0->oficinaSuperior->id == $central->id) {
                    return $this->findAll('id = :s', array(
                        ':s' => $user->oficina0->id
                    ));
                } else {
                    return $this->findAll('id = :s', array(
                        ':s' => $user->oficina0->oficinaSuperior->id
                    ));
                }
            }
        }
    }

    public function getFuncionariosOficinaUsuarioReg()
    {
        $user = Funcionarios::find('usuario = :u', array(
            ':u' => Yii::$app->user->id
        ));
        $central = self::find('oficina_superior = id');
        if ($user == null) {
            return Funcionarios::find()->all();
        } else {
            if ($user->oficina0->id == $central->id) {
                return Funcionarios::find()->all();
            } else {
                return Funcionarios::findAll('oficina = :s', array(
                    ':s' => $user->oficina0->id
                ));
            }
        }
    }

    public function hasBoss()
    {
        /*
        $funcionarios = Funcionarios::count('oficina = :o AND inactivo = :i AND cargo = :c',array(':o'=>$this->id,':i'=>0, ':c'=>3));
        if($funcionarios == 0){
            //Buscar en suboficinas
            $oficinas = Oficinas::findAll('oficina_superior = :oi',array(':oi'=>$this->id));
            if($oficinas != null){
                foreach ($oficinas as $o){
                    $f = Funcionarios::findAll('oficina = :t',array(':t'=>$o->id));
                    if($f == null){
                        return false;
                    }else{
                        foreach ($f as $founded){
                            if($founded->cargo0->id == 3){
                                return true;
                            }
                        }
                    }
                }
            }else{
                return false;
            }
        }else{
            //Si tiene oficina superior y no es COPE
            $central = Oficinas::find('oficina_superior == id');
            if($this->oficinaSuperior->id != $central->id){
                return false;
            }
        }
        return true;
        */
        $central = Oficinas::find('oficina_superior = id');
        if ($this->oficinaSuperior->id != $central->id) {
            return false;
        } else {
            return true;
        }
    }
}
