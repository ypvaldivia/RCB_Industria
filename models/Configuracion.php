<?php

namespace app\models;

use Yii;
use yii\swiftmailer\Mailer;

/**
 * This is the model class for table "configuracion".
 *
 * @property int $id
 * @property string $protocolo
 * @property string $servidor
 * @property int $puerto
 * @property int|null $smtp_ssl
 * @property int|null $smtp_auth
 * @property string $usuario
 * @property string|null $password
 * @property string|null $nombre_saliente
 * @property string $correo_saliente
 */
class Configuracion  extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'configuracion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['servidor', 'usuario', 'correo_saliente'], 'required'],
            [['puerto', 'smtp_ssl', 'smtp_auth'], 'integer'],
            [['protocolo'], 'string', 'max' => 10],
            [['servidor', 'usuario', 'password'], 'string', 'max' => 45],
            [['nombre_saliente'], 'string', 'max' => 150],
            [['correo_saliente'], 'string', 'max' => 85],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'protocolo' => 'Protocolo',
            'servidor' => 'Servidor',
            'puerto' => 'Puerto',
            'smtp_ssl' => 'Smtp Ssl',
            'smtp_auth' => 'Smtp Auth',
            'usuario' => 'Usuario',
            'password' => 'Password',
            'nombre_saliente' => 'Nombre Saliente',
            'correo_saliente' => 'Correo Saliente',
        ];
    }

    public function enviarNotificacion($inspeccion)
    {
        $mailerconfig = $this->findByPk(2);
        $mail = new Mailer();
        $mail->setSmtp(
            $mailerconfig->servidor,
            $mailerconfig->puerto,
            $mailerconfig->smtp_protocol,
            $mailerconfig->smtp_auth,
            $mailerconfig->usuario,
            $mailerconfig->password
        );
        //$mail->clearLayout();//if layout is already set in config
        $mail->setFrom('Sistema de notificaciones RCB - Industria');
        //$mail->setSubject($asunto);
        //$mail->setBody($correo);

        //Buscar usuarios que reciben notificaciones - ToDo
        /*
        $users = CrugeStoredUser::model()->findAll('notificated = :n',array(':n'=>1));
        foreach($users as $u){
            $mail->setTo($u->email);
            $mail->send();
        }
        */
    }
}
