<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estados_inspecciones".
 *
 * @property int $id
 * @property string $estado
 * @property string|null $color
 * @property string|null $descripcion
 *
 * @property Inspecciones[] $inspecciones
 */
class EstadosInspecciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estados_inspecciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['estado'], 'required'],
            [['descripcion'], 'string'],
            [['estado'], 'string', 'max' => 50],
            [['color'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'estado' => 'Estado',
            'color' => 'Color',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Inspecciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspecciones()
    {
        return $this->hasMany(Inspecciones::className(), ['estado' => 'id']);
    }
}
