<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "galeria_medios_izado".
 *
 * @property int $id
 * @property int $medio_izaje
 * @property string $imagen
 * @property string|null $descripcion
 *
 * @property MediosIzadoAcreditados $medioIzaje
 */
class GaleriaMediosIzado extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'galeria_medios_izado';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['medio_izaje', 'imagen'], 'required'],
            [['medio_izaje'], 'integer'],
            [['imagen'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 150],
            [['medio_izaje'], 'exist', 'skipOnError' => true, 'targetClass' => MediosIzadoAcreditados::className(), 'targetAttribute' => ['medio_izaje' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'medio_izaje' => 'Medio Izaje',
            'imagen' => 'Imagen',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[MedioIzaje]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMedioIzaje()
    {
        return $this->hasOne(MediosIzadoAcreditados::className(), ['id' => 'medio_izaje']);
    }
}
