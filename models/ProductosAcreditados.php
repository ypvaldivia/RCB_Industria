<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_acreditados".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $propietario
 * @property int|null $provincia
 * @property string|null $descripcion
 *
 * @property InspeccionesXProductos[] $inspeccionesXProductos
 * @property Entidades $propietario0
 * @property Provincias $provincia0
 * @property ShortcutHomologacionesProductos[] $shortcutHomologacionesProductos
 */
class ProductosAcreditados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_acreditados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['propietario', 'provincia'], 'integer'],
            [['descripcion'], 'string'],
            [['nombre'], 'string', 'max' => 200],
            [['propietario'], 'exist', 'skipOnError' => true, 'targetClass' => Entidades::className(), 'targetAttribute' => ['propietario' => 'id']],
            [['provincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincias::className(), 'targetAttribute' => ['provincia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'propietario' => 'Propietario',
            'provincia' => 'Provincia',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[InspeccionesXProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInspeccionesXProductos()
    {
        return $this->hasMany(InspeccionesXProductos::className(), ['producto' => 'id']);
    }

    /**
     * Gets query for [[Propietario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPropietario0()
    {
        return $this->hasOne(Entidades::className(), ['id' => 'propietario']);
    }

    /**
     * Gets query for [[Provincia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincia0()
    {
        return $this->hasOne(Provincias::className(), ['id' => 'provincia']);
    }

    /**
     * Gets query for [[ShortcutHomologacionesProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShortcutHomologacionesProductos()
    {
        return $this->hasMany(ShortcutHomologacionesProductos::className(), ['producto' => 'id']);
    }

    public function getFullID()
    {
        return $this->nombre .
            ' (' .
            $this->propietario0->nombre .
            ', ' .
            $this->provincia0->nombre .
            ') ';
    }
}
