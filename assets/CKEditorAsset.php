<?php

namespace app\assets;

use yii\web\AssetBundle;


class CKEditorAsset extends AssetBundle
{
    public $sourcePath = '@vendor/ckeditor/ckeditor';
    public $css = [
        'styles.css',
    ];
    public $js = [
        'ckeditor.js',
    ];
    public $depends = [
        'app\assets\MaterialProAsset'
    ];
}
