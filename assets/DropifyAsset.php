<?php

namespace app\assets;

use yii\web\AssetBundle;


class DropifyAsset extends AssetBundle
{
    public $sourcePath = '@materialplugins/dropify/dist';
    public $css = [
        'css/dropify.min.css',
    ];
    public $js = [
        'js/dropify.min.js',
    ];
    public $depends = [
        'app\assets\MaterialProAsset'
    ];
}
