<?php

namespace app\assets;

use yii\web\AssetBundle;


class MaterialProAsset extends AssetBundle
{
    public $sourcePath = '@webroot/themes/materialpro';
    public $css = [
        //'assets/plugins/prism/prism.css',
        //'assets/plugins/bootstrap/css/bootstrap.min.css',
        'css/style.css',
        'css/colors/blue.css',
        //'assets/plugins/metismenu/dist/metisMenu.min.css'
    ];
    public $js = [
        //'assets/plugins/jquery/jquery.min.js',
        //'assets/plugins/bootstrap/js/popper.min.js',
        //'assets/plugins/bootstrap/js/bootstrap.min.js',
        'js/jquery.slimscroll.js',
        'js/waves.js',
        //'js/sidebarmenu.js',
        'assets/plugins/sticky-kit-master/dist/sticky-kit.min.js',
        'assets/plugins/sparkline/jquery.sparkline.min.js',
        'js/custom.min.js',
        'assets/plugins/styleswitcher/jQuery.style.switcher.js',
        //'assets/plugins/metismenu/dist/metisMenu.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\web\JqueryAsset',
    ];
}
