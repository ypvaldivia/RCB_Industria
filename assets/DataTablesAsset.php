<?php

namespace app\assets;

use yii\web\AssetBundle;


class DataTablesAsset extends AssetBundle
{
    public $sourcePath = '@materialplugins/DataTablesCompact';
    public $css = [
        //'datatables.net-dt/css/jquery.dataTables.min.css',
        'datatables.min.css',
    ];
    public $js = [
        'datatables.min.js',
        'datatables_es.js',
    ];
    public $depends = [
        'app\assets\MaterialProAsset'
    ];
}
