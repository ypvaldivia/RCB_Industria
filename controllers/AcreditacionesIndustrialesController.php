<?php

namespace app\controllers;

use app\components\Traceador;
use app\models\AcreditacionesIndustriales;
use app\models\ShortcutHomologacionesMediosIzado;
use app\models\ShortcutHomologacionesPersonas;
use app\models\ShortcutHomologacionesServiciosProductos;
use Exception;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;

class AcreditacionesIndustrialesController extends Controller
{
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'estructura';
        Yii::$app->session['submenu'] = '';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'create', 'update', 'batchDelete'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_acreditacionesindustriales_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $data = AcreditacionesIndustriales::find()->all();
        Traceador::crearTraza('Acceso', 'Listado de Servicios del Grupo de Industria');
        return $this->render('index', array('data' => $data));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_acreditacionesindustriales_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new AcreditacionesIndustriales();
        if (isset($_POST['AcreditacionesIndustriales'])) {
            $model->setAttributes($_POST['AcreditacionesIndustriales']);
            $model->niveles_inspeccion_id = $_POST['AcreditacionesIndustriales']['nivelesInspeccion'];
            $model->tipo = $_POST['AcreditacionesIndustriales']['tipo'];
            $model->map = $_POST['AcreditacionesIndustriales']['map'];
            if (isset($_POST['AcreditacionesIndustriales']['tipoCertificado'])) {
                $model->tipoCertificado = $_POST['AcreditacionesIndustriales']['tipoCertificado'];
            }
            try {
                if ($model->save()) {
                    Traceador::crearTraza(
                        'creacion',
                        'Servicio de Industria: ' . $model->titulo_anuario
                    );
                    Yii::$app->session->setFlash('success', 'Se adicionó un elemento exitosamente.');
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['AcreditacionesIndustriales'])) {
            $model->attributes = $_GET['AcreditacionesIndustriales'];
        }
        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_acreditacionesindustriales_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);

        if (isset($_POST['AcreditacionesIndustriales'])) {
            $model->setAttributes($_POST['AcreditacionesIndustriales']);
            $model->niveles_inspeccion_id = $_POST['AcreditacionesIndustriales']['nivelesInspeccion'];
            $model->map = $_POST['AcreditacionesIndustriales']['map'];
            $model->categoria = $_POST['AcreditacionesIndustriales']['categoria'];
            if (isset($_POST['AcreditacionesIndustriales']['tipoCertificado'])) {
                $model->tipoCertificado = $_POST['AcreditacionesIndustriales']['tipoCertificado'];
            }
            try {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Se actualizó el elemento exitosamente.');
                    Traceador::crearTraza(
                        'edicion',
                        'Servicio de Industria: ' . $model->titulo_anuario
                    );
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
                //$this->render('test',array('model'=>$model->getAttributes()));
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                $model->addError('id', $e->getMessage());
            }
        }
        return $this->render('update', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = AcreditacionesIndustriales::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'La página solicitada no existe.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'acreditaciones-industriales-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_acreditacionesindustriales_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $model = $this->loadModel($id);
                    Traceador::crearTraza(
                        'eliminacion',
                        'Servicio de Industria: ' . $model->titulo_anuario
                    );
                    if ($model->tipo == 0) {
                        $tmp = ShortcutHomologacionesPersonas::findAll('homologacion = :h', [':h' => $model->id]);
                        if ($tmp != null) {
                            Yii::$app->session->setFlash('error', 'Esta homologacion tiene certificados de Personas asociados y no puede ser eliminada');
                        } else {
                            try {
                                $model->delete();
                                ++$successCount;
                            } catch (Exception $e) {
                                Yii::$app->session->setFlash('error', 'No se han eliminado los elementos seleccionados: ' . $e->getMessage());
                                break;
                            }
                            Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
                        }
                    } elseif ($model->tipo == 1) {
                        $tmp = ShortcutHomologacionesServiciosProductos::findAll('homologacion = :h', array(':h' => $model->id));
                        if ($tmp != null) {
                            Yii::$app->session->setFlash('error', 'Esta homologacion tiene certificados de Servicios asociados y no puede ser eliminada');
                        } else {
                            try {
                                $model->delete();
                                ++$successCount;
                            } catch (Exception $e) {
                                Yii::$app->session->setFlash('error', 'No se han eliminado los elementos seleccionados: ' . $e->getMessage());
                                break;
                            }
                            Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
                        }
                    } elseif ($model->tipo == 2) {
                        $tmp = ShortcutHomologacionesMediosIzado::findAll('homologacion = :h', array(':h' => $model->id));
                        if ($tmp != null) {
                            Yii::$app->session->setFlash('error', 'Esta homologacion tiene certificados de Productos asociados y no puede ser eliminada');
                        } else {
                            try {
                                $model->delete();
                                ++$successCount;
                            } catch (Exception $e) {
                                Yii::$app->session->setFlash('error', 'No se han eliminado los elementos seleccionados: ' . $e->getMessage());
                                break;
                            }
                            Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
                        }
                    } else {
                        try {
                            $model->delete();
                            ++$successCount;
                        } catch (Exception $e) {
                            Yii::$app->session->setFlash('error', 'No se han eliminado los elementos seleccionados: ' . $e->getMessage());
                            break;
                        }
                        Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
                    }
                }
            }
        } else {
            throw new HttpException(400, 'Petición no válida');
        }
    }
}
