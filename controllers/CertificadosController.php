<?php

namespace app\controllers;

use app\components\Traceador;
use app\forms\CertificadoBuzoSoldadorForm;
use app\forms\CertificadoInspectorSoldaduraForm;
use app\forms\CertificadoMITForm;
use app\forms\CertificadoOperadorSoldeoForm;
use app\forms\CertificadoPersonaDefaultForm;
use app\forms\CertificadoProdServDefaultForm;
use app\forms\CertificadoSoldadorAceroForm;
use app\forms\CertificadosPersonaDefaultProvisionalForm;
use app\models\AprobacionXFuncionarios;
use app\models\InspeccionesXDacs;
use app\models\InspeccionesXMediosIzado;
use app\models\InspeccionesXPersonas;
use app\models\InspeccionesXProductos;
use app\models\InspeccionesXServiciosProductos;
use app\models\ShortcutHomologacionesDacs;
use app\models\ShortcutHomologacionesMediosIzado;
use app\models\ShortcutHomologacionesPersonas;
use app\models\ShortcutHomologacionesProductos;
use app\models\ShortcutHomologacionesServiciosProductos;
use app\models\TiposCertificados;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;

class CertificadosController extends Controller
{
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'inspecciones';
        Yii::$app->session['submenu'] = '';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'certificado', 'certificadosPersonas', 'certificadosPersonasProvisional',
                    'PDFCertificadoPersonasDefault', 'PDFCertificadoPersonasDefaultProvisional',
                    'certificadosProdServDefaultProvisional', 'certificadosProdServDefault',
                    'PDFCertificadoProdServDefaultProvisional', 'PDFCertificadoProdServDefault',
                    'certificadoBuzoSoldador', 'certificadoBuzoSoldadorProvisional',
                    'PDFCertificadoBuzoSoldadorProvisional', 'PDFCertificadoBuzoSoldador',
                    'certificadoSoldadorAcero', 'certificadoSoldadorAcero',
                    'PDFCertificadoSoldadorAcero', 'PDFCertificadoSoldadorAceroProvisional',
                    'certificadoSoldadorAluminio', 'certificadoSoldadorAluminioProvisional',
                    'PDFCertificadoSoldadorAluminio', 'PDFCertificadoSoldadorAluminioProvisional',
                    'certificadoInspectorSoldaduraProvisional', 'certificadoInspectorSoldadura',
                    'PDFCertificadoInspectorSoldadura', 'PDFCertificadoInspectorSoldaduraProvisional',
                    'certificadoDacProvisional', 'certificadoDac',
                    'PDFCertificadoDacProvisional', 'PDFCertificadoDac',
                    'certificadoMIT1Provisional', 'certificadoMIT1',
                    'PDFCertificadoMIT1Provisional', 'PDFCertificadoMIT1',
                    'certificadoMIT2Provisional', 'certificadoMIT2',
                    'PDFCertificadoMIT2Provisional', 'PDFCertificadoMIT2',
                    'certificadoLimitacion', 'PDFCertificadoLimitacion',
                    'certificadosVehiculosLigerosProvisional', 'certificadosVehiculosLigeros',
                    'PDFCertificadoVehiculosLigerosProvisional', 'PDFCertificadoVehiculosLigeros',
                    'certificadosSupervisionCalderasProvisional', 'certificadosSupervisionCalderas',
                    'PDFCertificadoSupervisionCalderasProvisional', 'PDFCertificadoSupervisionCalderas',
                    'ajaxCertificadoOperadorSoldeoProvisional', 'redirectOSProvisional',
                    'ajaxCertificadoOperadorSoldeo', 'redirectOS',
                    'PDFCertificadoOperadorSoldeoProvisional', 'PDFCertificadoOperadorSoldeo',
                    'PDFAvalMIT'
                ),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    private function convertirFecha($date)
    {
        $m = '';
        $month = substr($date, 5, 2);
        switch ($month) {
            case 1:
                $m = 'enero';
                break;
            case 2:
                $m = 'febrero';
                break;
            case 3:
                $m = 'marzo';
                break;
            case 4:
                $m = 'abril';
                break;
            case 5:
                $m = 'mayo';
                break;
            case 6:
                $m = 'junio';
                break;
            case 7:
                $m = 'julio';
                break;
            case 8:
                $m = 'agosto';
                break;
            case 9:
                $m = 'septimebre';
                break;
            case 10:
                $m = 'octubre';
                break;
            case 11:
                $m = 'noviembre';
                break;
            case 12:
                $m = 'diciembre';
                break;
        }

        return substr($date, 8, 2) . ' de ' . $m . ' de ' . substr($date, 0, 4);
    }

    /**
     * Selecciona el tipo de certificado a generar.
     */
    public function actionCertificado($obj, $tipoObj, $tipoCe)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_certificado')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        //Leyenda
        //$tipoObj = ([1]=Personal,[2]=Servicio,[3]=Producto)
        //$tipoCe = (1 = Provisional, 2 = Permanente)
        $inspXObj = null;
        $certificado = null;
        switch ($tipoObj) {
            case 1:
                $inspXObj = InspeccionesXPersonas::findOne($obj);
                $certificado = TiposCertificados::findOne($inspXObj->inspeccion0->homologacion0->tipo_certificado);
                if ($certificado == null) {
                    $certificado = TiposCertificados::findOne(4);
                }
                break;
            case 2:
                $inspXObj = InspeccionesXServiciosProductos::findOne($obj);
                $certificado = TiposCertificados::findOne($inspXObj->inspeccion0->homologacion0->tipo_certificado);
                if ($certificado == null) {
                    $certificado = TiposCertificados::findOne(5);
                }
                break;
            case 3:
                if (is_numeric($obj)) {
                    $inspXObj = InspeccionesXProductos::findOne($obj);
                    $certificado = TiposCertificados::findOne($inspXObj->inspeccion0->homologacion0->tipo_certificado);
                    if ($certificado == null) {
                        $certificado = TiposCertificados::findOne(5);
                    }
                } else {
                    $objects = unserialize($obj);
                    $inspXObj = InspeccionesXProductos::findOne($objects[0]);
                    $certificado = TiposCertificados::findOne($inspXObj->inspeccion0->homologacion0->tipo_certificado);
                    if ($certificado == null) {
                        $certificado = TiposCertificados::findOne(5);
                    }
                    $inspXObj = $objects;
                }
                break;
            case 4:
                if (is_numeric($obj)) {
                    $inspXObj = InspeccionesXDacs::findOne($obj);
                    $certificado = TiposCertificados::findOne(12);
                } else {
                    $objects = unserialize($obj);
                    $certificado = TiposCertificados::findOne(12);
                    $inspXObj = $objects;
                }
                break;
            case 5:
                if (is_numeric($obj)) {
                    $inspXObj = InspeccionesXMediosIzado::findOne($obj);
                    if ($inspXObj->inspeccion0->homologacion == 94) {
                        $certificado = TiposCertificados::findOne(13);
                    } else {
                        $certificado = TiposCertificados::findOne($inspXObj->medioIzado->tipo0->tipo_certificado);
                        if ($certificado == null) {
                            $certificado = TiposCertificados::findOne(11);
                        }
                    }
                } else {
                    $objects = unserialize($obj);
                    $inspXObj = InspeccionesXMediosIzado::findOne($objects[0]);
                    $certificado = TiposCertificados::findOne($inspXObj->medioIzado->tipo0->tipo_certificado);
                    if ($certificado == null) {
                        $certificado = TiposCertificados::findOne(11);
                    }
                    $inspXObj = $objects;
                }
                break;
        }

        //Seleccion del tipo de certificado a generar
        switch ($certificado->id) {
            case 4: //Comun para personal
                if ($tipoCe == 1) {
                    //Provisional
                    $this->certificadoPersonasDefaultProvisional($inspXObj);
                } else {
                    //Permanente
                    $this->certificadoPersonasDefault($inspXObj);
                }
                break;
            case 5: //Comun para Productos y Servicios
                if ($tipoCe == 1) {
                    //Provisional
                    if (is_array($inspXObj)) {
                        $this->certificadoProdServDefaultProvisionalSpecial($inspXObj, $tipoObj);
                    } else {
                        $this->certificadoProdServDefaultProvisional($inspXObj, $tipoObj);
                    }
                } else {
                    //Permanente
                    if (is_array($inspXObj)) {
                        $this->certificadoProdServDefaultSpecial($inspXObj, $tipoObj);
                    } else {
                        $this->certificadoProdServDefault($inspXObj, $tipoObj);
                    }
                }
                break;
            case 1: //Especial de Buzos Soldadores
                if ($tipoCe == 1) {
                    //Provisional
                    $this->certificadoBuzoSoldadorProvisional($inspXObj);
                } else {
                    //Permanente
                    $this->certificadoBuzoSoldador($inspXObj);
                }
                break;
            case 2: //Especial Soldadores de Acero
                if ($tipoCe == 1) {
                    //Provisional
                    $this->certificadoSoldadorAceroProvisional($inspXObj);
                } else {
                    //Permanente
                    $this->certificadoSoldadorAcero($inspXObj);
                }
                break;
            case 6: //Especial Soldadores de Aluminio
                if ($tipoCe == 1) {
                    //Provisional
                    $this->certificadoSoldadorAluminioProvisional($inspXObj);
                } else {
                    //Permanente
                    $this->certificadoSoldadorAluminio($inspXObj);
                }
                break;
            case 3: //Especial Inspectores de Soldadura
                if ($tipoCe == 1) {
                    //Provisional
                    $this->certificadoInspectorSoldaduraProvisional($inspXObj);
                } else {
                    //Permanente
                    $this->certificadoInspectorSoldadura($inspXObj);
                }
                break;
            case 7: //Especial Operadores de Soldeo
                if ($tipoCe == 1) {
                    //Provisional
                    $this->certificadoOperadorSoldeoProvisional($inspXObj);
                } else {
                    //Permanente
                    $this->certificadoOperadorSoldeo($inspXObj);
                }
                break;
            case 8: //Especial Supervision de Calderas
                if ($tipoCe == 1) {
                    //Provisional
                    $this->certificadoSupervisionCalderasProvisional($inspXObj, $tipoObj);
                } else {
                    //Permanente
                    $this->certificadoSupervisionCalderas($inspXObj, $tipoObj);
                }
                break;
            case 9: //Especial Vahiculos Ligeros
                if ($tipoCe == 1) {
                    //Provisional
                    $this->certificadoVehiculosLigerosProvisional($inspXObj, $tipoObj);
                } else {
                    //Permanente
                    $this->certificadoVehiculosLigeros($inspXObj, $tipoObj);
                }
                break;
            case 10: //Especial Montacargas y Traspaleta
                if ($tipoCe == 1) {
                    //Provisional
                    if (is_array($inspXObj)) {
                        $this->certificadoMIT1ProvisionalSpecial($inspXObj);
                    } else {
                        $this->certificadoMIT1Provisional($inspXObj);
                    }
                } else {
                    //Permanente
                    if (is_array($inspXObj)) {
                        $this->certificadoMIT1Special($inspXObj);
                    } else {
                        $this->certificadoMIT1($inspXObj);
                    }
                }
                break;
            case 11: //Especial Gruas, Elevadores...
                if ($tipoCe == 1) {
                    //Provisional
                    if (is_array($inspXObj)) {
                        $this->certificadoMIT2ProvisionalSpecial($inspXObj);
                    } else {
                        $this->certificadoMIT2Provisional($inspXObj);
                    }
                } else {
                    //Permanente
                    if (is_array($inspXObj)) {
                        $this->certificadoMIT2Special($inspXObj);
                    } else {
                        $this->certificadoMIT2($inspXObj);
                    }
                }
                break;
            case 13: //Aval de importacion
                $this->avalImportacion($inspXObj);
                break;
            case 12: //Dacs...
                if ($tipoCe == 1) {
                    //Provisional
                    if (is_array($inspXObj)) {
                        $this->certificadoDacProvisionalSpecial($inspXObj);
                    } else {
                        $this->certificadoDacProvisional($inspXObj);
                    }
                } else {
                    //Permanente
                    if (is_array($inspXObj)) {
                        $this->certificadoDacSpecial($inspXObj);
                    } else {
                        $this->certificadoDac($inspXObj);
                    }
                }
                break;
        }
    }

    /*******************************************/
    /***** CERTIFICADO COMUN PERSONAL *********/
    /*************************************** */
    public function certificadoPersonasDefaultProvisional($o)
    {
        $model = new CertificadosPersonaDefaultProvisionalForm();
        $inspectoresActuantes = $o->inspeccion0->visitasInspecciones[count($o->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;
        $docs = $o->inspeccion0->homologacion0->map;
        if ($docs != null) {
            $model->servicio = $docs;
        }

        if (isset($_POST['FormCertificadosPersonaDefaultProvisional'])) {
            $model->setMyAttributes($_POST['FormCertificadosPersonaDefaultProvisional'], $o);
            $docs = $o->inspeccion0->homologacion0->map;
            if ($docs != null) {
                $model->servicio = $docs;
            }
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('default_personas_provisional', array('model' => $model, 'serv' => $o, 'inspectores' => $inspectores));
    }

    public function actionPDFCertificadoPersonasDefaultProvisional($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadopersonasdefaultprovisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        ob_start();
        $data = $this->renderPartial('PDFCertificadoPersonasDefaultProvisional', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();

        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/personas') . '/';

        if ($inspXper->cprovisional != null && file_exists($path_cert . $inspXper->cprovisional)) {
            unlink($path_cert . $inspXper->cprovisional);
        }
        $inspXper->cprovisional = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de personas provisional: ' . $inspXper->inspeccion0->numero_control
        );
    }

    public function certificadoPersonasDefault($o)
    {
        $model = new CertificadoPersonaDefaultForm();
        $model->control = 0;
        $docs = $o->inspeccion0->homologacion0->map;
        if ($docs != null) {
            $model->servicio = $docs;
        }

        if (isset($_POST['FormCertificadoPersonaDefault'])) {
            $model->setMyAttributes($_POST['FormCertificadoPersonaDefault'], $o);
            $docs = $o->inspeccion0->homologacion0->map;
            if ($docs != null) {
                $model->servicio = $docs;
            }
            $y = substr($model->expedicion, 0, 4);
            $m = substr($model->expedicion, 5, 2);
            $d = substr($model->expedicion, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc;
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $o->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_personas', array('model' => $model, 'serv' => $o));
    }

    public function actionPDFCertificadoPersonasDefault($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadopersonasdefault')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        $certificado = $inspXper->inspeccion0->numero_control . '-' . $inspXper->numero;
        $shortcutmodel = ShortcutHomologacionesPersonas::findOne(
            'inspeccion = :insp AND persona = :d AND certificado = :c',
            array(':insp' => $o, ':d' => $inspXper->persona0->id, ':c' => $certificado)
        );
        if ($shortcutmodel == null) {
            $shortcutmodel = new ShortcutHomologacionesPersonas();
            $shortcutmodel->certificado = $certificado;
            $shortcutmodel->homologacion = $inspXper->inspeccion0->homologacion0->id;
            $shortcutmodel->persona = $inspXper->persona0->id;
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->save();
        } else {
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->update();
        }

        ob_start();
        $data = $this->renderPartial('PDFCertificadoPersonasDefault', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_de_personas_' . rand(1, 50) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/personas') . '/';

        if ($inspXper->cdefinitivo != null && file_exists($path_cert . $inspXper->cdefinitivo)) {
            unlink($path_cert . $inspXper->cdefinitivo);
        }
        $inspXper->cdefinitivo = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de personas definitivo: ' . $inspXper->inspeccion0->numero_control
        );
    }

    /*******************************************/
    /***** BUZOS SOLDADORES  ******************/
    /*************************************** */
    private function certificadoBuzoSoldador($inspXper)
    {
        $model = new CertificadoBuzoSoldadorForm();
        $model->initForm($inspXper);
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $inspXper->inspeccion0->id));
        $model->control = 0;

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }

        if (isset($_POST['FormCertificadoBuzoSoldador'])) {
            $model->setMyAttributes($_POST['FormCertificadoBuzoSoldador']);
            $y = substr($model->emision, 0, 4);
            $m = substr($model->emision, 5, 2);
            $d = substr($model->emision, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->emision;
            $model->storeValidez = $dateVc;
            $model->emision = $this->convertirFecha($model->emision);
            $model->validez = $this->convertirFecha($dateVc);
            $model->control = 1;
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('certif_BuzoSoldador', array('model' => $model, 'serv' => $inspXper));
    }

    private function certificadoBuzoSoldadorProvisional($inspXper)
    {
        $model = new CertificadoBuzoSoldadorForm();
        $model->initForm($inspXper);
        $inspectoresActuantes = $inspXper->inspeccion0->visitasInspecciones[count($inspXper->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoBuzoSoldador'])) {
            $model->setMyAttributes($_POST['FormCertificadoBuzoSoldador']);
            $model->emision = $this->convertirFecha($model->emision);
            $model->control = 1;
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('certifProv_BuzoSoldador', array('model' => $model, 'serv' => $inspXper, 'inspectores' => $inspectores));
    }

    public function actionPDFCertificadoBuzoSoldador($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadobuzosoldador')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        //Actualizar tabla de acceso rapido
        $certificado = $inspXper->inspeccion0->numero_control . '-' . $inspXper->numero;
        $shortcutmodel = ShortcutHomologacionesPersonas::find(
            'inspeccion = :insp AND persona = :d AND certificado = :c',
            array(':insp' => $o, ':d' => $inspXper->persona0->id, ':c' => $certificado)
        );
        if ($shortcutmodel == null) {
            $shortcutmodel = new ShortcutHomologacionesPersonas();
            $shortcutmodel->certificado = $certificado;
            $shortcutmodel->homologacion = $inspXper->inspeccion0->homologacion0->id;
            $shortcutmodel->persona = $inspXper->persona0->id;
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->save();
        } else {
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->update();
        }

        ob_start();
        $data = $this->renderPartial('PDFCertificadoBuzoSoldador', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_buzo_soldador' . rand(1, 50) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/personas') . '/';
        if ($inspXper->cdefinitivo != null && file_exists($path_cert . $inspXper->cdefinitivo)) {
            unlink($path_cert . $inspXper->cdefinitivo);
        }
        $inspXper->cdefinitivo = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de buzo soldador definitivo: ' . $inspXper->inspeccion0->numero_control
        );
    }

    public function actionPDFCertificadoBuzoSoldadorProvisional($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadobuzosoldadorprovisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        ob_start();
        $data = $this->renderPartial('PDFCertificadoBuzoSoldadorProvisional', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_provisional_buzo_soldador' . rand(1, 50) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/personas') . '/';
        if ($inspXper->cprovisional != null && file_exists($path_cert . $inspXper->cprovisional)) {
            unlink($path_cert . $inspXper->cprovisional);
        }
        $inspXper->cprovisional = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de buzo soldador provisional: ' . $inspXper->inspeccion0->numero_control
        );
    }

    /******************************************/
    /****** SOLDADORES DE ACERO **************/
    /*************************************** */
    private function certificadoSoldadorAceroProvisional($inspXper)
    {
        $model = new CertificadoSoldadorAceroForm();
        $model->initForm($inspXper);
        $inspectoresActuantes = $inspXper->inspeccion0->visitasInspecciones[count($inspXper->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoSoldadorAcero'])) {
            $model->setMyAttributes($_POST['FormCertificadoSoldadorAcero']);
            $model->emision = $this->convertirFecha($model->emision);
            $model->control = 1;
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('certifProv_SoldadorAcero', array('model' => $model, 'serv' => $inspXper, 'inspectores' => $inspectores));
    }

    public function actionPDFCertificadoSoldadorAceroProvisional($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadosoldadoraceroprovisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        ob_start();
        $data = $this->renderPartial('PDFCertificadoSoldadorAceroProvisional', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_provisional_soldador_acero' . rand(1, 50) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/personas') . '/';
        if ($inspXper->cprovisional != null && file_exists($path_cert . $inspXper->cprovisional)) {
            unlink($path_cert . $inspXper->cprovisional);
        }
        $inspXper->cprovisional = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de soldador de acero provisional: ' . $inspXper->inspeccion0->numero_control
        );
    }

    private function certificadoSoldadorAcero($inspXper)
    {
        $model = new CertificadoSoldadorAceroForm();
        $model->initForm($inspXper);
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $inspXper->inspeccion0->id));
        $model->control = 0;

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }

        if (isset($_POST['FormCertificadoSoldadorAcero'])) {
            $model->setMyAttributes($_POST['FormCertificadoSoldadorAcero']);
            $y = substr($model->emision, 0, 4);
            $m = substr($model->emision, 5, 2);
            $d = substr($model->emision, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->emision;
            $model->storeValidez = $dateVc;
            $model->emision = $this->convertirFecha($model->emision);
            $model->validez = $this->convertirFecha($dateVc);
            $model->control = 1;
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('certif_SoldadorAcero', array('model' => $model, 'serv' => $inspXper));
    }

    public function actionPDFCertificadoSoldadorAcero($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadosoldadoracero')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        //Actualizar tabla de acceso rapido
        $certificado = $inspXper->inspeccion0->numero_control . '-' . $inspXper->numero;
        $shortcutmodel = ShortcutHomologacionesPersonas::find(
            'inspeccion = :insp AND persona = :d AND certificado = :c',
            array(':insp' => $o, ':d' => $inspXper->persona0->id, ':c' => $certificado)
        );
        if ($shortcutmodel == null) {
            $shortcutmodel = new ShortcutHomologacionesPersonas();
            $shortcutmodel->certificado = $certificado;
            $shortcutmodel->homologacion = $inspXper->inspeccion0->homologacion0->id;
            $shortcutmodel->persona = $inspXper->persona0->id;
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->save();
        } else {
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->update();
        }

        ob_start();
        $data = $this->renderPartial('PDFCertificadoSoldadorAcero', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_soldador_acero' . rand(1, 50) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/personas') . '/';
        if ($inspXper->cdefinitivo != null && file_exists($path_cert . $inspXper->cdefinitivo)) {
            unlink($path_cert . $inspXper->cdefinitivo);
        }
        $inspXper->cdefinitivo = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de soldador de acero definitivo: ' . $inspXper->inspeccion0->numero_control
        );
    }

    /******************************************/
    /****** SOLDADORES DE ALUMINIO************/
    /*************************************** */
    private function certificadoSoldadorAluminioProvisional($inspXper)
    {
        $model = new CertificadoSoldadorAceroForm();
        $model->initForm($inspXper);
        $inspectoresActuantes = $inspXper->inspeccion0->visitasInspecciones[count($inspXper->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoSoldadorAcero'])) {
            $model->setMyAttributes($_POST['FormCertificadoSoldadorAcero']);
            $model->emision = $this->convertirFecha($model->emision);
            $model->control = 1;
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('certifProv_SoldadorAluminio', array('model' => $model, 'serv' => $inspXper, 'inspectores' => $inspectores));
    }

    public function actionPDFCertificadoSoldadorAluminioProvisional($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadosoldadoraluminioprovisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        ob_start();
        $data = $this->renderPartial('PDFCertificadoSoldadorAluminioProvisional', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_provisional_soldador_aluminio' . rand(1, 50) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/personas') . '/';
        if ($inspXper->cprovisional != null && file_exists($path_cert . $inspXper->cprovisional)) {
            unlink($path_cert . $inspXper->cprovisional);
        }
        $inspXper->cprovisional = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de soldador de aluminio provisional: ' . $inspXper->inspeccion0->numero_control
        );
    }

    private function certificadoSoldadorAluminio($inspXper)
    {
        $model = new CertificadoSoldadorAceroForm();
        $model->initForm($inspXper);
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $inspXper->inspeccion0->id));
        $model->control = 0;

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }

        if (isset($_POST['FormCertificadoSoldadorAcero'])) {
            $model->setMyAttributes($_POST['FormCertificadoSoldadorAcero']);
            $y = substr($model->emision, 0, 4);
            $m = substr($model->emision, 5, 2);
            $d = substr($model->emision, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->emision;
            $model->storeValidez = $dateVc;
            $model->emision = $this->convertirFecha($model->emision);
            $model->validez = $this->convertirFecha($dateVc);
            $model->control = 1;
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('certif_SoldadorAluminio', array('model' => $model, 'serv' => $inspXper));
    }

    public function actionPDFCertificadoSoldadorAluminio($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadosoldadoraluminio')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        //Actualizar tabla de acceso rapido
        $certificado = $inspXper->inspeccion0->numero_control . '-' . $inspXper->numero;
        $shortcutmodel = ShortcutHomologacionesPersonas::find(
            'inspeccion = :insp AND persona = :d AND certificado = :c',
            array(':insp' => $o, ':d' => $inspXper->persona0->id, ':c' => $certificado)
        );
        if ($shortcutmodel == null) {
            $shortcutmodel = new ShortcutHomologacionesPersonas();
            $shortcutmodel->certificado = $certificado;
            $shortcutmodel->homologacion = $inspXper->inspeccion0->homologacion0->id;
            $shortcutmodel->persona = $inspXper->persona0->id;
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->save();
        } else {
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->update();
        }

        ob_start();
        $data = $this->renderPartial('PDFCertificadoSoldadorAluminio', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_soldador_aluminio' . rand(1, 50) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/personas') . '/';
        if ($inspXper->cdefinitivo != null && file_exists($path_cert . $inspXper->cdefinitivo)) {
            unlink($path_cert . $inspXper->cdefinitivo);
        }
        $inspXper->cdefinitivo = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de soldador de aluminio definitivo: ' . $inspXper->inspeccion0->numero_control
        );
    }

    /******************************************/
    /***** INSPECTORES DE SOLDADURA ***********/
    /*************************************** */
    private function certificadoInspectorSoldaduraProvisional($inspXper)
    {
        $model = new CertificadoInspectorSoldaduraForm();
        $inspectoresActuantes = $inspXper->inspeccion0->visitasInspecciones[count($inspXper->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoInspectorSoldadura'])) {
            $model->setMyAttributes($_POST['FormCertificadoInspectorSoldadura']);
            $model->emision = $this->convertirFecha($model->emision);
            $model->control = 1;
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }

        return $this->render('certifProv_InspectorSoldadura', array('model' => $model, 'serv' => $inspXper, 'inspectores' => $inspectores));
    }

    public function actionPDFCertificadoInspectorSoldaduraProvisional($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadoinspectorsoldaduraprovisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        ob_start();
        $data = $this->renderPartial('PDFCertificadoInspectorSoldaduraProvisional', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_provisional_InspectorSoldadura' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/personas') . '/';
        if ($inspXper->cprovisional != null && file_exists($path_cert . $inspXper->cprovisional)) {
            unlink($path_cert . $inspXper->cprovisional);
        }
        $inspXper->cprovisional = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de inspector de soldadura provisional: ' . $inspXper->inspeccion0->numero_control
        );
    }

    private function certificadoInspectorSoldadura($inspXper)
    {
        $model = new CertificadoInspectorSoldaduraForm();
        $model->initForm($inspXper);
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $inspXper->inspeccion0->id));
        $model->control = 0;

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }

        if (isset($_POST['FormCertificadoInspectorSoldadura'])) {
            $model->setMyAttributes($_POST['FormCertificadoInspectorSoldadura']);
            $y = substr($model->emision, 0, 4);
            $m = substr($model->emision, 5, 2);
            $d = substr($model->emision, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->emision;
            $model->storeValidez = $dateVc;
            $model->emision = $this->convertirFecha($model->emision);
            $model->vence = $this->convertirFecha($dateVc);
            $model->control = 1;
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('certif_InspectorSoldadura', array('model' => $model, 'serv' => $inspXper));
    }

    public function actionPDFCertificadoInspectorSoldadura($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadoinspectorsoldadura')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        //Actualizar tabla de acceso rapido
        $certificado = $inspXper->inspeccion0->numero_control . '-' . $inspXper->numero;
        $shortcutmodel = ShortcutHomologacionesPersonas::find(
            'inspeccion = :insp AND persona = :d AND certificado = :c',
            array(':insp' => $o, ':d' => $inspXper->persona0->id, ':c' => $certificado)
        );
        if ($shortcutmodel == null) {
            $shortcutmodel = new ShortcutHomologacionesPersonas();
            $shortcutmodel->certificado = $certificado;
            $shortcutmodel->homologacion = $inspXper->inspeccion0->homologacion0->id;
            $shortcutmodel->persona = $inspXper->persona0->id;
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->save();
        } else {
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->update();
        }

        ob_start();
        $data = $this->renderPartial('PDFCertificadoInspectorSoldadura', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_inspector_soldadura' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/personas') . '/';
        if ($inspXper->cdefinitivo != null && file_exists($path_cert . $inspXper->cdefinitivo)) {
            unlink($path_cert . $inspXper->cdefinitivo);
        }
        $inspXper->cdefinitivo = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de inspector de soldadura definitivo: ' . $inspXper->inspeccion0->numero_control
        );
    }

    /******************************************/
    /***** OPERADORES DE SOLDEO ***************/
    /*************************************** */
    public function actionAjaxCertificadoOperadorSoldeoProvisional()
    {
        $request = Yii::$app->getRequest();
        if ($request->isAjaxRequest) {

            //En este arreglo el primer elemento es el formulario
            //El segundo la tabla de Traccion
            //El tercero la tabla de Doblado
            //Finalmente la tabla de Doblado
            $formData = $_POST['formdata'];
            $model = new CertificadoOperadorSoldeoForm();
            $model->initAjaxForm($formData[0]['idinsp']);

            $model->setAjaxAttributes($formData);
            $model->emision = $this->convertirFecha($model->emision);
            $model->control = 1;
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;

            $inspXper = InspeccionesXPersonas::findOne($formData[0]['idinsp']);
            $inspectoresActuantes = $inspXper->inspeccion0->ultimaVisita->inspectoresActuantes;
            $inspectores = array();
            $i = 0;
            foreach ($inspectoresActuantes as $ia) {
                $inspectores[$i] = $ia->inspector0;
                ++$i;
            }
        }
    }

    public function actionRedirectOSProvisional()
    {
        $var = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];
        $inspXper = InspeccionesXPersonas::findOne($var->id);
        $inspectoresActuantes = $inspXper->inspeccion0->visitasInspecciones[count($inspXper->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        return $this->render('certifProv_OperadorSoldeo', array('model' => $var, 'serv' => $inspXper, 'inspectores' => $inspectores));
    }

    private function certificadoOperadorSoldeoProvisional($inspXper)
    {
        $model = new CertificadoOperadorSoldeoForm();
        $model->initForm($inspXper);
        $inspectoresActuantes = $inspXper->inspeccion0->visitasInspecciones[count($inspXper->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        return $this->render('certifProv_OperadorSoldeo', array('model' => $model, 'serv' => $inspXper, 'inspectores' => $inspectores));
    }

    public function actionPDFCertificadoOperadorSoldeoProvisional($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadooperadorsoldeoprovisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        ob_start();
        $data = $this->renderPartial('PDFCertificadoOperadorSoldeoProvisional', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_provisional_OperadorSoldeo' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/personas') . '/';
        if ($inspXper->cprovisional != null && file_exists($path_cert . $inspXper->cprovisional)) {
            unlink($path_cert . $inspXper->cprovisional);
        }
        $inspXper->cprovisional = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de operador de soldeo provisional: ' . $inspXper->inspeccion0->numero_control
        );
    }

    public function actionAjaxCertificadoOperadorSoldeo()
    {
        $request = Yii::$app->getRequest();
        if ($request->isAjaxRequest) {

            //En este arreglo el primer elemento es el formulario
            //El segundo la tabla de Traccion
            //El tercero la tabla de Doblado
            //Finalmente la tabla de Doblado
            $formData = $_POST['formdata'];
            $model = new CertificadoOperadorSoldeoForm();
            $model->initAjaxForm($formData[0]['idinsp']);

            $model->setAjaxAttributes($formData);
            $y = substr($model->emision, 0, 4);
            $m = substr($model->emision, 5, 2);
            $d = substr($model->emision, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->emision;
            $model->storeValidez = $dateVc;
            $model->emision = $this->convertirFecha($model->emision);
            $model->validez = $this->convertirFecha($dateVc);
            $model->control = 1;
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
    }

    public function actionRedirectOS()
    {
        $var = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];
        $inspXper = InspeccionesXPersonas::findOne($var->id);
        $var->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $inspXper->inspeccion0->id));
        $var->control = 1;

        if ($var->nivelAprobacion != null) {
            $var->nivelAprobacion = array_reverse($var->nivelAprobacion);
        }
        return $this->render('certif_OperadorSoldeo', array('model' => $var, 'serv' => $inspXper));
    }

    private function certificadoOperadorSoldeo($inspXper)
    {
        $model = new CertificadoOperadorSoldeoForm();
        $model->initForm($inspXper);
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $inspXper->inspeccion0->id));
        $model->control = 0;

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }

        return $this->render('certif_OperadorSoldeo', array('model' => $model, 'serv' => $inspXper));
    }

    public function actionPDFCertificadoOperadorSoldeo($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadooperadorsoldeo')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXper = InspeccionesXPersonas::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        //Actualizar tabla de acceso rapido
        $certificado = $inspXper->inspeccion0->numero_control . '-' . $inspXper->numero;
        $shortcutmodel = ShortcutHomologacionesPersonas::find(
            'inspeccion = :insp AND persona = :d AND certificado = :c',
            array(':insp' => $o, ':d' => $inspXper->persona0->id, ':c' => $certificado)
        );
        if ($shortcutmodel == null) {
            $shortcutmodel = new ShortcutHomologacionesPersonas();
            $shortcutmodel->certificado = $certificado;
            $shortcutmodel->homologacion = $inspXper->inspeccion0->homologacion0->id;
            $shortcutmodel->persona = $inspXper->persona0->id;
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->save();
        } else {
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigente = $model->storeValidez;
            $shortcutmodel->anho_vigente = substr($model->storeValidez, 0, 4);
            $shortcutmodel->update();
        }

        ob_start();
        $data = $this->renderPartial('PDFCertificadoOperadorSoldeo', array('serv' => $inspXper, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_OperadorSoldeo' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/personas') . '/';
        if ($inspXper->cdefinitivo != null && file_exists($path_cert . $inspXper->cdefinitivo)) {
            unlink($path_cert . $inspXper->cdefinitivo);
        }
        $inspXper->cdefinitivo = $certName;
        $inspXper->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de operadores de soldeo definitivo: ' . $inspXper->inspeccion0->numero_control
        );
    }

    /********************************************************* ****/
    /***** CERTIFICADO COMUN PRODUCTOS Y SERVICIOS ***************/
    /**** *******************************************************/
    public function certificadoProdServDefaultProvisionalSpecial($o, $tipo)
    {
        $first = InspeccionesXProductos::findOne($o[0]);
        $all = array();
        foreach ($o as $object) {
            array_push($all, InspeccionesXProductos::findOne($object));
        }
        $model = new CertificadoProdServDefaultForm();
        $inspectoresActuantes = $first->inspeccion0->visitasInspecciones[count($first->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;
        $docs = $first->inspeccion0->homologacion0->map;
        if ($docs != null) {
            $model->servicio = $docs;
        }
        if (isset($_POST['FormCertificadoProdServDefault'])) {
            $model->setMyAttributes($_POST['FormCertificadoProdServDefault'], $first, $tipo);
            $docs = $first->inspeccion0->homologacion0->map;
            if ($docs != null) {
                $model->servicio = $docs;
            }
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('default_provisional_prod_serv', array('model' => $model, 'serv' => $all, 'inspectores' => $inspectores, 'tipo' => $tipo));
    }

    public function certificadoProdServDefaultProvisional($o, $tipo)
    {
        $model = new CertificadoProdServDefaultForm();
        $inspectoresActuantes = $o->inspeccion0->visitasInspecciones[count($o->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;
        $docs = $o->inspeccion0->homologacion0->map;
        if ($docs != null) {
            $model->servicio = $docs;
        }

        if (isset($_POST['FormCertificadoProdServDefault'])) {
            $model->setMyAttributes($_POST['FormCertificadoProdServDefault'], $o, $tipo);
            $docs = $o->inspeccion0->homologacion0->map;
            if ($docs != null) {
                $model->servicio = $docs;
            }
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('default_provisional_prod_serv', array('model' => $model, 'serv' => $o, 'inspectores' => $inspectores, 'tipo' => $tipo));
    }

    public function actionPDFCertificadoProdServDefaultProvisional($o, $tipo)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadoprodservdefaultprovisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $all = array();
        $this->layout = 'blank';
        if ($tipo == 2) {
            $inspXobj = InspeccionesXServiciosProductos::findOne($o);
        } elseif ($tipo == 3) {
            if (is_numeric($o)) {
                $inspXobj = InspeccionesXProductos::findOne($o);
            } else {
                $objs = unserialize($o);
                foreach ($objs as $object) {
                    array_push($all, InspeccionesXProductos::findOne($object));
                }
            }
        }
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        ob_start();
        if (is_numeric($o)) {
            $data = $this->renderPartial('PDFCertificadoProdServDefaultProvisional', array('serv' => $inspXobj, 'model' => $model, 'tipo' => $tipo), true);
        } else {
            $data = $this->renderPartial('PDFCertificadoProdServDefaultProvisional', array('serv' => $all, 'model' => $model, 'tipo' => $tipo), true);
        }

        ob_get_clean();
        $certName = 'Certificado_de_servicios_productos_provisional_' . rand(1, 100) . '.pdf';
        if ($tipo == 2) {
            $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/servicios') . '/';
        } else {
            $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/productos') . '/';
        }
        if (is_numeric($o)) {
            if ($inspXobj->cprovisional != null && file_exists($path_cert . $inspXobj->cprovisional)) {
                unlink($path_cert . $inspXobj->cprovisional);
            }
            $inspXobj->cprovisional = $certName;
            $inspXobj->update();
        } else {
            foreach ($all as $object) {
                if ($object->cprovisional != null && file_exists($path_cert . $object->cprovisional)) {
                    unlink($path_cert . $object->cprovisional);
                }
                $object->cprovisional = $certName;
                $object->update();
            }
        }

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_servicios_productos_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creación',
            'Certificado de productos y servicios provisional: ' . is_numeric($o) ? $inspXobj->inspeccion0->numero_control : $all[0]->inspeccion0->numero_control
        );
    }

    public function certificadoProdServDefaultSpecial($o, $tipo)
    {
        $all = array();
        foreach ($o as $object) {
            array_push($all, InspeccionesXProductos::findOne($object));
        }
        $model = new CertificadoProdServDefaultForm();
        $model->control = 0;
        $docs = $all[0]->inspeccion0->homologacion0->map;
        if ($docs != null) {
            $model->servicio = $docs;
        }
        if (isset($_POST['FormCertificadoProdServDefault'])) {
            $model->setMyAttributes($_POST['FormCertificadoProdServDefault'], $all[0], $tipo);
            $docs = $all[0]->inspeccion0->homologacion0->map;
            if ($docs != null) {
                $model->servicio = $docs;
            }
            $y = substr($model->expedicion, 0, 4);
            $m = substr($model->expedicion, 5, 2);
            $d = substr($model->expedicion, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc;
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $all[0]->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_prod_serv', array('model' => $model, 'serv' => $all, 'tipo' => $tipo));
    }

    public function certificadoProdServDefault($o, $tipo)
    {
        $model = new CertificadoProdServDefaultForm();
        $model->control = 0;
        $docs = $o->inspeccion0->homologacion0->map;
        if ($docs != null) {
            $model->servicio = $docs;
        }

        if (isset($_POST['FormCertificadoProdServDefault'])) {
            $model->setMyAttributes($_POST['FormCertificadoProdServDefault'], $o, $tipo);
            $docs = $o->inspeccion0->homologacion0->map;
            if ($docs != null) {
                $model->servicio = $docs;
            }
            $y = substr($model->expedicion, 0, 4);
            $m = substr($model->expedicion, 5, 2);
            $d = substr($model->expedicion, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc;
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $o->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_prod_serv', array('model' => $model, 'serv' => $o, 'tipo' => $tipo));
    }

    public function actionPDFCertificadoProdServDefault($o, $tipo)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadoprodservdefault')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];
        if ($tipo == 2) {
            $inspXobj = InspeccionesXServiciosProductos::findOne($o);
            //Actualizar tabla de acceso rapido
            $y = substr($model->expedicion, 0, 4);
            $m = substr($model->expedicion, 5, 2);
            $d = substr($model->expedicion, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $d . '-' . $m;
            $certificado = $inspXobj->inspeccion0->numero_control . '-' . $inspXobj->numero;
            $shortcutmodel = ShortcutHomologacionesServiciosProductos::findOne(
                'inspeccion = :insp AND servicio = :d AND certificado = :c',
                array(':insp' => $o, ':d' => $inspXobj->servcios0->id, ':c' => $certificado)
            );
            if ($shortcutmodel == null) {
                $shortcutmodel = new ShortcutHomologacionesServiciosProductos();
                $shortcutmodel->certificado = $certificado;
                $shortcutmodel->homologacion = $inspXobj->inspeccion0->homologacion0->id;
                $shortcutmodel->servicio = $inspXobj->servcios0->id;
                $shortcutmodel->emision = $model->storeEmision;
                $shortcutmodel->vigencia = $model->storeValidez;
                $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
                $shortcutmodel->save();
            } else {
                $shortcutmodel->emision = $model->storeEmision;
                $shortcutmodel->vigencia = $model->storeValidez;
                $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
                $shortcutmodel->update();
            }
        } elseif ($tipo == 3) {
            $all = array();
            if (is_numeric($o)) {
                array_push($all, InspeccionesXProductos::findOne($o));
            } else {
                $objs = unserialize($o);
                foreach ($objs as $objects) {
                    array_push($all, InspeccionesXProductos::findOne($objects));
                }
            }

            for ($i = 0; $i < count($all); $i++) {
                $certificado = $all[$i]->inspeccion0->numero_control . '-' . $all[$i]->numero;
                $shortcutmodel = ShortcutHomologacionesProductos::findOne(
                    'inspeccion = :insp AND producto = :d AND certificado = :c',
                    array(':insp' => $all[$i]->id, ':d' => $all[$i]->producto0->id, ':c' => $certificado)
                );
                if ($shortcutmodel == null) {
                    $shortcutmodel = new ShortcutHomologacionesProductos();
                    $shortcutmodel->certificado = $certificado;
                    $shortcutmodel->homologacion = $all[$i]->inspeccion0->homologacion0->id;
                    $shortcutmodel->producto = $all[$i]->producto0->id;
                    $shortcutmodel->emision = $model->storeEmision;
                    $shortcutmodel->vigencia = $model->storeValidez;
                    $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
                    $shortcutmodel->save();
                } else {
                    $shortcutmodel->emision = $model->storeEmision;
                    $shortcutmodel->vigencia = $model->storeValidez;
                    $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
                    $shortcutmodel->update();
                }
            }
        }

        ob_start();
        $data = $this->renderPartial('PDFCertificadoProdServDefault', array('serv' => $all, 'model' => $model, 'tipo' => $tipo), true);
        ob_get_clean();
        $certName = 'Certificado_de_servicios_productos_' . rand(1, 100) . '.pdf';
        if ($tipo == 2) {
            $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/servicios') . '/';
        } else {
            $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/productos') . '/';
        }

        if (is_numeric($o)) {
            if ($all[0]->cdefinitivo != null && file_exists($path_cert . $all[0]->cdefinitivo)) {
                unlink($path_cert . $all[0]->cdefinitivo);
            }
            $all[0]->cdefinitivo = $certName;
            $all[0]->update();
        } else {
            foreach ($all as $object) {
                if ($object->cdefinitivo != null && file_exists($path_cert . $object->cdefinitivo)) {
                    unlink($path_cert . $object->cdefinitivo);
                }
                $object->cdefinitivo = $certName;
                $object->update();
            }
        }

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_productos_servicios_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de prodcutos y servicios definitivo: ' . $all[0]->inspeccion0->numero_control
        );
    }

    /********************************************************* ****/
    /***** CERTIFICADO COMUN PRODUCTOS Y SERVICIOS ***************/
    /**** *******************************************************/
    public function certificadoVehiculosLigerosProvisional($o, $tipo)
    {
        $model = new CertificadoProdServDefaultForm();
        $inspectoresActuantes = $o->inspeccion0->visitasInspecciones[count($o->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoProdServDefault'])) {
            $model->setMyAttributes($_POST['FormCertificadoProdServDefault'], $o, $tipo);
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('default_provisional_vehiculos_ligeros', array('model' => $model, 'serv' => $o, 'inspectores' => $inspectores, 'tipo' => $tipo));
    }

    public function actionPDFCertificadoVehiculosLigerosProvisional($o, $tipo)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadovehiculosligerosprovisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXobj = InspeccionesXProductos::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        ob_start();
        $data = $this->renderPartial('PDFCertificadoVehiculosLigerosProvisional', array('serv' => $inspXobj, 'model' => $model, 'tipo' => $tipo), true);
        ob_get_clean();
        $certName = 'Certificado_de_vehiculos_ligeros_provisional_' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/productos') . '/';
        if ($inspXobj->cprovisional != null && file_exists($path_cert . $inspXobj->cprovisional)) {
            unlink($path_cert . $inspXobj->cprovisional);
        }
        $inspXobj->cprovisional = $certName;
        $inspXobj->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de vehiculos ligeros provisional: ' . $inspXobj->inspeccion0->numero_control
        );
    }

    public function certificadoVehiculosLigeros($o, $tipo)
    {
        $model = new CertificadoProdServDefaultForm();
        $model->control = 0;

        if (isset($_POST['FormCertificadoProdServDefault'])) {
            $model->setMyAttributes($_POST['FormCertificadoProdServDefault'], $o, $tipo);
            $y = substr($model->expedicion, 0, 4);
            $m = substr($model->expedicion, 5, 2);
            $d = substr($model->expedicion, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc;
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $o->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_vehiculos_ligeros', array('model' => $model, 'serv' => $o, 'tipo' => $tipo));
    }

    public function actionPDFCertificadoVehiculosLigeros($o, $tipo)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadovehiculosligeros')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];
        $inspXobj = InspeccionesXProductos::findOne($o);
        //Actualizar tabla de acceso rapido
        $y = substr($model->expedicion, 0, 4);
        $m = substr($model->expedicion, 5, 2);
        $d = substr($model->expedicion, 8, 2);
        $yy = $y + 3;
        $dateVc = $yy . '-' . $d . '-' . $m;
        $certificado = $inspXobj->inspeccion0->numero_control . '-' . $inspXobj->numero;
        $shortcutmodel = ShortcutHomologacionesProductos::find(
            'inspeccion = :insp AND producto = :d AND certificado = :c',
            array(':insp' => $o, ':d' => $inspXobj->producto0->id, ':c' => $certificado)
        );
        if ($shortcutmodel == null) {
            $shortcutmodel = new ShortcutHomologacionesProductos();
            $shortcutmodel->certificado = $certificado;
            $shortcutmodel->homologacion = $inspXobj->inspeccion0->homologacion0->id;
            $shortcutmodel->producto = $inspXobj->producto0->id;
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigencia = $model->storeValidez;
            $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
            $shortcutmodel->save();
        } else {
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigencia = $model->storeValidez;
            $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
            $shortcutmodel->update();
        }

        ob_start();
        $data = $this->renderPartial('PDFCertificadoVehiculosLigeros', array('serv' => $inspXobj, 'model' => $model, 'tipo' => $tipo), true);
        ob_get_clean();
        $certName = 'Certificado_de_vehiculos_ligeros_' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/productos') . '/';
        if ($inspXobj->cdefinitivo != null && file_exists($path_cert . $inspXobj->cdefinitivo)) {
            unlink($path_cert . $inspXobj->cdefinitivo);
        }
        $inspXobj->cdefinitivo = $certName;
        $inspXobj->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de vehiculos ligeros dfinitivo: ' . $inspXobj->inspeccion0->numero_control
        );
    }

    /********************************************************* ****/
    /***** CERTIFICADO CALDERAS EN EXPLOTACION *******************/
    /**** *******************************************************/
    public function certificadoSupervisionCalderasProvisional($o, $tipo)
    {
        $model = new CertificadoProdServDefaultForm();
        $inspectoresActuantes = $o->inspeccion0->visitasInspecciones[count($o->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoProdServDefault'])) {
            $model->setMyAttributes($_POST['FormCertificadoProdServDefault'], $o, $tipo);
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('default_provisional_supervision_calderas', array('model' => $model, 'serv' => $o, 'inspectores' => $inspectores, 'tipo' => $tipo));
    }

    public function actionPDFCertificadoSupervisionCalderasProvisional($o, $tipo)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadosupervisioncalderasprovisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXobj = InspeccionesXProductos::findOne($o);

        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        ob_start();
        $data = $this->renderPartial('PDFCertificadoSupervisionCalderasProvisional', array('serv' => $inspXobj, 'model' => $model, 'tipo' => $tipo), true);
        ob_get_clean();
        $certName = 'Certificado_de_supervision_calderas_provisional_' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/productos') . '/';
        if ($inspXobj->cprovisional != null && file_exists($path_cert . $inspXobj->cprovisional)) {
            unlink($path_cert . $inspXobj->cprovisional);
        }
        $inspXobj->cprovisional = $certName;
        $inspXobj->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de Supervision Tecnica de calderas en Explotacion provisional: ' . $inspXobj->inspeccion0->numero_control
        );
    }

    public function certificadoSupervisionCalderas($o, $tipo)
    {
        $model = new CertificadoProdServDefaultForm();
        $model->control = 0;

        if (isset($_POST['FormCertificadoProdServDefault'])) {
            $model->setMyAttributes($_POST['FormCertificadoProdServDefault'], $o, $tipo);
            $y = substr($model->expedicion, 0, 4);
            $m = substr($model->expedicion, 5, 2);
            $d = substr($model->expedicion, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc;
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $o->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_supervision_calderas', array('model' => $model, 'serv' => $o, 'tipo' => $tipo));
    }

    public function actionPDFCertificadoSupervisionCalderas($o, $tipo)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadosupervisioncalderas')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];
        $inspXobj = InspeccionesXProductos::findOne($o);
        //Actualizar tabla de acceso rapido
        $y = substr($model->expedicion, 0, 4);
        $m = substr($model->expedicion, 5, 2);
        $d = substr($model->expedicion, 8, 2);
        $yy = $y + 3;
        $dateVc = $yy . '-' . $d . '-' . $m;
        $certificado = $inspXobj->inspeccion0->numero_control . '-' . $inspXobj->numero;
        $shortcutmodel = ShortcutHomologacionesProductos::find(
            'inspeccion = :insp AND producto = :d AND certificado = :c',
            array(':insp' => $o, ':d' => $inspXobj->producto0->id, ':c' => $certificado)
        );
        if ($shortcutmodel == null) {
            $shortcutmodel = new ShortcutHomologacionesProductos();
            $shortcutmodel->certificado = $inspXobj->inspeccion0->numero_control . '-' . $inspXobj->numero;
            $shortcutmodel->homologacion = $inspXobj->inspeccion0->homologacion0->id;
            $shortcutmodel->medio_izaje = $inspXobj->medioIzado->id;
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigencia = $model->storeValidez;
            $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
            $shortcutmodel->save();
        } else {
            $shortcutmodel->emision = $model->storeEmision;
            $shortcutmodel->vigencia = $model->storeValidez;
            $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
            $shortcutmodel->update();
        }

        ob_start();
        $data = $this->renderPartial('PDFCertificadoSupervisionCalderas', array('serv' => $inspXobj, 'model' => $model, 'tipo' => $tipo), true);
        ob_get_clean();
        $certName = 'Certificado_de_supervision_calderas_' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/productos') . '/';
        if ($inspXobj->cdefinitivo != null && file_exists($path_cert . $inspXobj->cdefinitivo)) {
            unlink($path_cert . $inspXobj->cdefinitivo);
        }
        $inspXobj->cdefinitivo = $certName;
        $inspXobj->update();

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_personas_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado de Supervision Tecnica de calderas en Explotacion definitivo: ' . $inspXobj->inspeccion0->numero_control
        );
    }

    /*********************************************************** **/
    /************* CERTIFICADOS DE SEGURIDAD TECNICA *************/
    /************************************************************/
    public function certificadoDacProvisional($o)
    {
        $model = new CertificadoMITForm();
        $model->flag = false;
        $inspectoresActuantes = $o->inspeccion0->visitasInspecciones[count($o->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->flag = true;
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $o);
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('default_provisional_dac', array('model' => $model, 'serv' => $o, 'inspectores' => $inspectores));
    }

    public function certificadoDacProvisionalSpecial($o)
    {
        $all = array();
        foreach ($o as $object) {
            array_push($all, InspeccionesXDacs::findOne($object));
        }
        $model = new CertificadoMITForm();
        $model->flag = false;
        $inspectoresActuantes = $all[0]->inspeccion0->visitasInspecciones[count($all[0]->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->flag = true;
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $all[0]);
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('default_provisional_dac_unique', array('model' => $model, 'serv' => $all, 'inspectores' => $inspectores));
    }

    public function certificadoDac($o)
    {
        $model = new CertificadoMITForm();
        $model->control = 0;
        $model->flag = false;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $o);
            $model->flag = true;
            $tmp = date_create($model->expedicion);
            $vigencia = $o->dac0->tipo0->meses_vigencia;
            $vigencia = '+' . $vigencia . ' months';
            $dateVc = date_add($tmp, date_interval_create_from_date_string($vigencia));
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc->format('Y-m-d');
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc->format('Y-m-d'));
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }

        $model->nivelAprobacion = AprobacionXFuncionarios::findAll('inspeccion = :i', array(':i' => $o->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_dac', array('model' => $model, 'serv' => $o));
    }

    public function certificadoDacSpecial($o)
    {
        $all = array();
        foreach ($o as $object) {
            array_push($all, InspeccionesXDacs::findOne($object));
        }
        $model = new CertificadoMITForm();
        $model->control = 0;
        $model->flag = false;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $all[0]);
            $model->flag = true;
            $tmp = date_create($model->expedicion);
            $vigencia = ($all[0]->dac0->tipo0 != null) ? $all[0]->dac0->tipo0->meses_vigencia : 12;
            $vigencia = '+' . $vigencia . ' months';
            $dateVc = date_add($tmp, date_interval_create_from_date_string($vigencia));
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc->format('Y-m-d');
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc->format('Y-m-d'));
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }

        $model->nivelAprobacion = AprobacionXFuncionarios::findAll('inspeccion = :i', array(':i' => $all[0]->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_dac_special', array('model' => $model, 'serv' => $all));
    }

    public function actionPDFCertificadoDacProvisional($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadodacprovisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];
        $all = array();
        if (is_numeric($o)) {
            array_push($all, InspeccionesXDacs::findOne($o));
            $mit = $all[0]->dac0;
            $mit->ct_certificada = $model->ci_pp_certificada;
            $mit->update();
        } else {
            $objs = unserialize($o);
            foreach ($objs as $objects) {
                $inspXobj = InspeccionesXDacs::findOne($objects);
                array_push($all, $inspXobj);
                $mit = $inspXobj->dac0;
                $mit->ct_certificada = $model->ci_pp_certificada;
                $mit->update();
            }
        }

        ob_start();
        if (is_numeric($o)) {
            $data = $this->renderPartial('PDFCertificadoDacProvisional', array('serv' => $o, 'model' => $model), true);
        } else {
            $data = $this->renderPartial('PDFCertificadoDacProvisionalSpecial', array('serv' => $all, 'model' => $model), true);
        }
        ob_get_clean();
        $certName = 'Certificado_de_DAC_provisional_' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/dacs') . '/';
        if ($all[0]->cprovisional != null && file_exists($path_cert . $all[0]->cprovisional)) {
            unlink($path_cert . $all[0]->cprovisional);
        }
        foreach ($all as $dac) {
            $dac->cprovisional = $certName;
            $dac->update();
        }
        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_dacs_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creación',
            'Certificado único de Dispositivo Auxiliar de Carga provisional: ' . $all[0]->inspeccion0->numero_control
        );
    }

    public function actionPDFCertificadoDac($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadodac')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        $all = array();
        if (is_numeric($o)) {
            array_push($all, InspeccionesXDacs::findOne($o));
            $mit = $all[0]->dac0;
            $mit->ct_certificada = $model->ci_pp_certificada;
            $mit->update();
        } else {
            $objs = unserialize($o);
            foreach ($objs as $objects) {
                $inspXobj = InspeccionesXDacs::findOne($objects);
                array_push($all, $inspXobj);
                $mit = $inspXobj->dac0;
                $mit->ct_certificada = $model->ci_pp_certificada;
                $mit->update();
            }
        }

        for ($i = 0; $i < count($all); $i++) {
            //Actualizar tabla de acceso rapido
            $certificado = $all[$i]->inspeccion0->numero_control . '-' . $all[$i]->numero;
            $shortcutmodel = ShortcutHomologacionesDacs::find(
                'inspeccion = :insp AND dac = :d AND certificado = :c',
                array(':insp' => $all[$i]->id, ':d' => $all[$i]->dac0->id, ':c' => $certificado)
            );
            if ($shortcutmodel == null) {
                $shortcutmodel = new ShortcutHomologacionesDacs();
                $shortcutmodel->certificado = $certificado;
                $shortcutmodel->homologacion = $all[$i]->inspeccion0->homologacion0->id;
                $shortcutmodel->dac = $all[$i]->dac0->id;
                $shortcutmodel->emision = $model->storeEmision;
                $shortcutmodel->vigencia = $model->storeValidez;
                $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
                $shortcutmodel->save();
            } else {
                $shortcutmodel->emision = $model->storeEmision;
                $shortcutmodel->vigencia = $model->storeValidez;
                $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
                $shortcutmodel->update();
            }
        }

        ob_start();
        if (is_numeric($o)) {
            $data = $this->renderPartial('PDFCertificadoDac', array('serv' => $o, 'model' => $model), true);
        } else {
            $data = $this->renderPartial('PDFCertificadoDacSpecial', array('serv' => $all, 'model' => $model), true);
        }

        ob_get_clean();
        $certName = 'Certificado_de_DAC_' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/dacs') . '/';
        if ($all[0]->cdefinitivo != null && file_exists($path_cert . $all[0]->cdefinitivo)) {
            unlink($path_cert . $all[0]->cdefinitivo);
        }

        foreach ($all as $dac) {
            $dac->cdefinitivo = $certName;
            $dac->update();
        }

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_dac_definitivo_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado definitivo único de Dispositivo Auxiliar de Carga: ' . $all[0]->inspeccion0->numero_control
        );
    }

    /*********************************************************** **/
    /***** CERTIFICADO MIT MONTACARGAS Y TRASPALETAS *************/
    /************************************************************/
    public function certificadoMIT1Provisional($o)
    {
        $model = new CertificadoMITForm();
        $model->flag = false;
        $inspectoresActuantes = $o->inspeccion0->visitasInspecciones[count($o->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->flag = true;
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $o);
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('default_provisional_MIT1', array('model' => $model, 'serv' => $o, 'inspectores' => $inspectores));
    }

    public function certificadoMIT1ProvisionalSpecial($o)
    {
        $all = array();
        foreach ($o as $object) {
            array_push($all, InspeccionesXMediosIzado::findOne($object));
        }
        $model = new CertificadoMITForm();
        $model->flag = false;
        $inspectoresActuantes = $all[0]->inspeccion0->visitasInspecciones[count($all[0]->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->flag = true;
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $all[0]);
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('default_provisional_mit_unique', array('model' => $model, 'serv' => $all, 'inspectores' => $inspectores));
    }

    public function certificadoMIT1($o)
    {
        $model = new CertificadoMITForm();
        $model->control = 0;
        $model->flag = false;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $o);
            $model->flag = true;
            $y = substr($model->expedicion, 0, 4);
            $m = substr($model->expedicion, 5, 2);
            $d = substr($model->expedicion, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc;
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $o->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_MIT1', array('model' => $model, 'serv' => $o));
    }

    public function avalImportacion($o)
    {
        $model = new CertificadoMITForm();
        $model->control = 0;
        $model->flag = false;
        $model->initForm($o);

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $o);
            $model->flag = true;
            $y = substr($model->expedicion, 0, 4);
            $m = substr($model->expedicion, 5, 2);
            $d = substr($model->expedicion, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc;
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $o->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_aval', array('model' => $model, 'serv' => $o));
    }

    public function actionPDFAvalMIT($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfavalmit')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];
        $insp = InspeccionesXMediosIzado::findOne($o);
        $mit = $insp->medioIzado;
        $mit->ci_certificada = $model->ci_pp_certificada;
        if ($model->ci_aux_certificada != null) {
            $mit->ci_aux_certificada = $model->ci_aux_certificada;
        }
        if ($model->ci_aux1_certificada != null) {
            $mit->ci_aux1_certificada = $model->ci_aux1_certificada;
        }
        $mit->update();

        ob_start();
        $data = $this->renderPartial('PDFAvalMIT', array('serv' => $insp, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Aval_importacion_de_MIT_' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/mits') . '/';

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Aval_importacion_de_MIT_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creación',
            'Aval de importación de Medios de Izado Terrestres: ' . $insp->inspeccion0->numero_control
        );
    }

    public function certificadoMIT1Special($o)
    {
        $all = array();
        foreach ($o as $object) {
            array_push($all, InspeccionesXMediosIzado::findOne($object));
        }
        $model = new CertificadoMITForm();
        $model->control = 0;
        $model->flag = false;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $all[0]);
            $model->flag = true;
            $y = substr($model->expedicion, 0, 4);
            $m = substr($model->expedicion, 5, 2);
            $d = substr($model->expedicion, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc;
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }

        $model->nivelAprobacion = AprobacionXFuncionarios::findAll('inspeccion = :i', array(':i' => $all[0]->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_mit_unique', array('model' => $model, 'serv' => $all));
    }

    public function actionPDFCertificadoMIT1Provisional($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadomit1provisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];
        $all = array();
        if (is_numeric($o)) {
            array_push($all, InspeccionesXMediosIzado::findOne($o));
            $mit = $all[0]->medioIzado;
            $mit->ct_certificada = $model->ci_pp_certificada;
            if ($model->ci_aux_certificada != null) {
                $mit->ci_aux_certificada = $model->ci_aux_certificada;
            }
            if ($model->ci_aux1_certificada != null) {
                $mit->ci_aux1_certificada = $model->ci_aux1_certificada;
            }
            $mit->update();
        } else {
            $objs = unserialize($o);
            foreach ($objs as $objects) {
                $inspXobj = InspeccionesXMediosIzado::findOne($objects);
                array_push($all, $inspXobj);
                $mit = $inspXobj->medioIzado;
                $mit->ci_certificada = $model->ci_pp_certificada;
                if ($model->ci_aux_certificada != null) {
                    $mit->ci_aux_certificada = $model->ci_aux_certificada;
                }
                if ($model->ci_aux1_certificada != null) {
                    $mit->ci_aux1_certificada = $model->ci_aux1_certificada;
                }
                $mit->update();
            }
        }

        ob_start();
        if (is_numeric($o)) {
            $data = $this->renderPartial('PDFCertificadoMIT1Provisional', array('serv' => $o, 'model' => $model), true);
        } else {
            $data = $this->renderPartial('PDFCertificadoMITProvisionalUnique', array('serv' => $all, 'model' => $model), true);
        }

        ob_get_clean();
        $certName = 'Certificado_de_MIT_provisional_' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/mits') . '/';
        if ($all[0]->cprovisional != null && file_exists($path_cert . $all[0]->cprovisional)) {
            unlink($path_cert . $all[0]->cprovisional);
        }
        foreach ($all as $mit) {
            $mit->cprovisional = $certName;
            $mit->update();
        }

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_mits_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado único de Medios de Izado Terrestres provisional: ' . $all[0]->inspeccion0->numero_control
        );
    }

    public function actionPDFCertificadoMIT1($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadomit1')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];
        $all = array();
        if (is_numeric($o)) {
            array_push($all, InspeccionesXMediosIzado::findOne($o));
            $mit = $all[0]->medioIzado;
            $mit->ci_certificada = $model->ci_pp_certificada;
            if ($model->ci_aux_certificada != null) {
                $mit->ci_aux_certificada = $model->ci_aux_certificada;
            }
            if ($model->ci_aux1_certificada != null) {
                $mit->ci_aux1_certificada = $model->ci_aux1_certificada;
            }
            $mit->update();
        } else {
            $objs = unserialize($o);
            foreach ($objs as $objects) {
                $inspXobj = InspeccionesXMediosIzado::findOne($objects);
                array_push($all, $inspXobj);
                $mit = $inspXobj->medioIzado;
                $mit->ci_certificada = $model->ci_pp_certificada;
                if ($model->ci_aux_certificada != null) {
                    $mit->ci_aux_certificada = $model->ci_aux_certificada;
                }
                if ($model->ci_aux1_certificada != null) {
                    $mit->ci_aux1_certificada = $model->ci_aux1_certificada;
                }
                $mit->update();
            }
        }

        for ($i = 0; $i < count($all); $i++) {
            //Actualizar tabla de acceso rapido
            $certificado = $all[$i]->inspeccion0->numero_control . '-' . $all[$i]->numero;
            $shortcutmodel = ShortcutHomologacionesMediosIzado::find(
                'inspeccion = :insp AND medio_izaje = :d AND certificado = :c',
                array(':insp' => $all[$i]->id, ':d' => $all[$i]->medioIzado->id, ':c' => $certificado)
            );
            if ($shortcutmodel == null) {
                $shortcutmodel = new ShortcutHomologacionesMediosIzado();
                $shortcutmodel->certificado = $certificado;
                $shortcutmodel->homologacion = $all[$i]->inspeccion0->homologacion0->id;
                $shortcutmodel->medio_izaje = $all[$i]->medioIzado->id;
                $shortcutmodel->emision = $model->storeEmision;
                $shortcutmodel->vigencia = $model->storeValidez;
                $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
                $shortcutmodel->save();
            } else {
                $shortcutmodel->emision = $model->storeEmision;
                $shortcutmodel->vigencia = $model->storeValidez;
                $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
                $shortcutmodel->update();
            }
        }

        ob_start();
        if (is_numeric($o)) {
            $data = $this->renderPartial('PDFCertificadoMIT1', array('serv' => $all[0], 'model' => $model), true);
        } else {
            $data = $this->renderPartial('PDFCertificadoMITUnique', array('serv' => $all, 'model' => $model), true);
        }
        ob_get_clean();
        $certName = 'Certificado_de_MIT_' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/mits') . '/';
        if ($all[0]->cdefinitivo != null && file_exists($path_cert . $all[0]->cdefinitivo)) {
            unlink($path_cert . $all[0]->cdefinitivo);
        }
        foreach ($all as $mit) {
            $mit->cdefinitivo = $certName;
            $mit->update();
        }

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_mit_definitivo_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado definitivo único de Medios de Izado Terrestres: ' . $all[0]->inspeccion0->numero_control
        );
    }

    /**************************************************************/
    /***** CERTIFICADO MIT GRUAS, ELEVADORES, ETC ****************/
    /************************************************************/
    public function certificadoMIT2Provisional($o)
    {
        $model = new CertificadoMITForm();
        $model->flag = false;
        $inspectoresActuantes = $o->inspeccion0->visitasInspecciones[count($o->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->flag = true;
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $o);
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('default_provisional_MIT2', array('model' => $model, 'serv' => $o, 'inspectores' => $inspectores));
    }

    public function certificadoMIT2ProvisionalSpecial($o)
    {
        $all = array();
        foreach ($o as $object) {
            array_push($all, InspeccionesXMediosIzado::findOne($object));
        }
        $model = new CertificadoMITForm();
        $model->flag = false;
        $inspectoresActuantes = $all[0]->inspeccion0->visitasInspecciones[count($all[0]->inspeccion0->visitasInspecciones) - 1]->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->flag = true;
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $all[0]);
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('default_provisional_mit_unique', array('model' => $model, 'serv' => $all, 'inspectores' => $inspectores));
    }

    public function certificadoMIT2($o)
    {
        $model = new CertificadoMITForm();
        $model->control = 0;
        $model->flag = false;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $o);
            $model->flag = true;
            $y = substr($model->expedicion, 0, 4);
            $m = substr($model->expedicion, 5, 2);
            $d = substr($model->expedicion, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc;
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        $model->nivelAprobacion = AprobacionXFuncionarios::findAll(array('inspeccion' => $o->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_MIT2', array('model' => $model, 'serv' => $o));
    }

    public function certificadoMIT2Special($o)
    {
        $all = array();
        foreach ($o as $object) {
            array_push($all, InspeccionesXMediosIzado::findOne($object));
        }
        $model = new CertificadoMITForm();
        $model->control = 0;
        $model->flag = false;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $all[0]);
            $model->flag = true;
            $y = substr($model->expedicion, 0, 4);
            $m = substr($model->expedicion, 5, 2);
            $d = substr($model->expedicion, 8, 2);
            $yy = $y + 3;
            $dateVc = $yy . '-' . $m . '-' . $d;
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc;
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc);
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }

        $model->nivelAprobacion = AprobacionXFuncionarios::findAll('inspeccion = :i', array(':i' => $all[0]->inspeccion0->id));

        if ($model->nivelAprobacion != null) {
            $model->nivelAprobacion = array_reverse($model->nivelAprobacion);
        }
        return $this->render('default_mit_unique', array('model' => $model, 'serv' => $all));
    }

    public function actionPDFCertificadoMIT2Provisional($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadomit2provisional')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];
        $all = array();
        if (is_numeric($o)) {
            array_push($all, InspeccionesXMediosIzado::findOne($o));
            $mit = $all[0]->medioIzado;
            $mit->ct_certificada = $model->ci_pp_certificada;
            if ($model->ci_aux_certificada != null) {
                $mit->ci_aux_certificada = $model->ci_aux_certificada;
            }
            if ($model->ci_aux1_certificada != null) {
                $mit->ci_aux1_certificada = $model->ci_aux1_certificada;
            }
            $mit->update();
        } else {
            $objs = unserialize($o);
            foreach ($objs as $objects) {
                $inspXobj = InspeccionesXMediosIzado::findOne($objects);
                array_push($all, $inspXobj);
                $mit = $inspXobj->medioIzado;
                $mit->ct_certificada = $model->ci_pp_certificada;
                if ($model->ci_aux_certificada != null) {
                    $mit->ci_aux_certificada = $model->ci_aux_certificada;
                }
                if ($model->ci_aux1_certificada != null) {
                    $mit->ci_aux1_certificada = $model->ci_aux1_certificada;
                }
                $mit->update();
            }
        }

        ob_start();
        $data = $this->renderPartial('PDFCertificadoMIT2Provisional', array('serv' => $all, 'model' => $model), true);
        ob_get_clean();
        $certName = 'Certificado_de_MIT_provisional_' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/mits') . '/';
        if ($all[0]->cprovisional != null && file_exists($path_cert . $all[0]->cprovisional)) {
            unlink($path_cert . $all[0]->cprovisional);
        }
        foreach ($all as $mit) {
            $mit->cprovisional = $certName;
            $mit->update();
        }

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_mits_provisional_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creacion',
            'Certificado único de Medios de Izado Terrestres provisional: ' . $all[0]->inspeccion0->numero_control
        );
    }

    public function actionPDFCertificadoMIT2($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadomit2')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];
        $all = array();
        if (is_numeric($o)) {
            array_push($all, InspeccionesXMediosIzado::findOne($o));
            $mit = $all[0]->medioIzado;
            $mit->ci_certificada = $model->ci_pp_certificada;
            if ($model->ci_aux_certificada != null) {
                $mit->ci_aux_certificada = $model->ci_aux_certificada;
            }
            if ($model->ci_aux1_certificada != null) {
                $mit->ci_aux1_certificada = $model->ci_aux1_certificada;
            }
            $mit->update();
        } else {
            $objs = unserialize($o);
            foreach ($objs as $objects) {
                $inspXobj = InspeccionesXMediosIzado::findOne($objects);
                array_push($all, $inspXobj);
                $mit = $inspXobj->medioIzado;
                $mit->ci_certificada = $model->ci_pp_certificada;
                if ($model->ci_aux_certificada != null) {
                    $mit->ci_aux_certificada = $model->ci_aux_certificada;
                }
                if ($model->ci_aux1_certificada != null) {
                    $mit->ci_aux1_certificada = $model->ci_aux1_certificada;
                }
                $mit->update();
            }
        }

        for ($i = 0; $i < count($all); $i++) {
            //Actualizar tabla de acceso rapido
            $certificado = $all[$i]->inspeccion0->numero_control . '-' . $all[$i]->numero;
            $shortcutmodel = ShortcutHomologacionesMediosIzado::find(
                'inspeccion = :insp AND medio_izaje = :d AND certificado = :c',
                array(':insp' => $all[$i]->id, ':d' => $all[$i]->medioIzado->id, ':c' => $certificado)
            );
            if ($shortcutmodel == null) {
                $shortcutmodel = new ShortcutHomologacionesMediosIzado();
                $shortcutmodel->certificado = $certificado;
                $shortcutmodel->homologacion = $all[$i]->inspeccion0->homologacion0->id;
                $shortcutmodel->medio_izaje = $all[$i]->medioIzado->id;
                $shortcutmodel->emision = $model->storeEmision;
                $shortcutmodel->vigencia = $model->storeValidez;
                $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
                $shortcutmodel->save();
            } else {
                $shortcutmodel->emision = $model->storeEmision;
                $shortcutmodel->vigencia = $model->storeValidez;
                $shortcutmodel->anho_vigencia = substr($model->storeValidez, 0, 4);
                $shortcutmodel->update();
            }
        }

        ob_start();
        if (is_numeric($o)) {
            $data = $this->renderPartial('PDFCertificadoMIT2', array('serv' => $all[0], 'model' => $model), true);
        } else {
            $data = $this->renderPartial('PDFCertificadoMITUnique', array('serv' => $all, 'model' => $model), true);
        }
        ob_get_clean();
        $certName = 'Certificado_de_MIT_' . rand(1, 100) . '.pdf';
        $path_cert = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/mits') . '/';
        if ($all[0]->cdefinitivo != null && file_exists($path_cert . $all[0]->cdefinitivo)) {
            unlink($path_cert . $all[0]->cdefinitivo);
        }
        foreach ($all as $mit) {
            $mit->cdefinitivo = $certName;
            $mit->update();
        }

        $filepdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $filepdf->pdf->SetDisplayMode('real');
        $filepdf->WriteHTML($data, isset($_GET['vuehtml']));
        $filepdf->Output($path_cert . $certName, 'F');

        $htmlpdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $htmlpdf->pdf->SetDisplayMode('real');
        $htmlpdf->WriteHTML($data, isset($_GET['vuehtml']));
        $certName = 'Certificado_de_mit_definitivo_' . rand(1, 50) . '.pdf';
        $htmlpdf->Output($certName, 'D');

        Traceador::crearTraza(
            'Creación',
            'Certificado definitivo único de Medios de Izado Terrestres: ' . $all[0]->inspeccion0->numero_control
        );
    }

    /************ ******************************/
    /***** CERTIFICADO LIMITACION MIT *********/
    /*************************************** */
    public function actionCertificadoLimitacion($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_certificadolimitacion')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new CertificadoMITForm();
        $inspXObj = InspeccionesXMediosIzado::findOne($o);
        $model->initForm($inspXObj);
        $inspectoresActuantes = $inspXObj->inspeccion0->getUltimaVisita()->inspectoresActuantes;
        $inspectores = array();
        $i = 0;
        foreach ($inspectoresActuantes as $ia) {
            $inspectores[$i] = $ia->inspector0;
            ++$i;
        }
        $model->control = 0;

        if (isset($_POST['FormCertificadoMIT'])) {
            $model->setMyAttributes($_POST['FormCertificadoMIT'], $inspXObj);
            $dateVc = date_add(date_create_from_format('Y-m-d', $model->expedicion), date_interval_create_from_date_string('+3 months'));
            $model->storeEmision = $model->expedicion;
            $model->storeValidez = $dateVc->format('Y-m-d');
            $model->expedicion = $this->convertirFecha($model->expedicion);
            $model->vencimiento = $this->convertirFecha($dateVc->format('Y-m-d'));
            $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'] = $model;
        }
        return $this->render('limitacion_MIT', array('model' => $model, 'serv' => $inspXObj, 'inspectores' => $inspectores));
    }

    public function actionPDFCertificadoLimitacion($o)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_certificados_pdfcertificadolimitacion')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $inspXObj = InspeccionesXMediosIzado::findOne($o);
        $model = $_SESSION[Yii::$app->user->id . '-' . Yii::$app->user->um->loadUserById(Yii::$app->user->id, true)->username . '-certificado'];

        ob_start();
        $data = $this->renderPartial('PDFCertificadoLimitacion', array('serv' => $inspXObj, 'model' => $model), true);
        ob_get_clean();

        $html2pdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->WriteHTML($data, isset($_GET['vuehtml']));
        $html2pdf->Output('Certificado_de_limitacion_' . rand(1, 50) . '.pdf', 'D');
        Traceador::crearTraza(
            'Creacion',
            'Certificado de limitacion de MIT: ' . $inspXObj->inspeccion0->numero_control
        );
    }
}
