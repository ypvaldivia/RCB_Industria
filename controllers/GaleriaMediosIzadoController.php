<?php

namespace app\controllers;

use app\components\Traceador;
use app\models\GaleriaMediosIzado;
use app\models\MediosIzadoAcreditados;
use Exception;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

class GaleriaMediosIzadoController extends Controller
{
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete', 'batchDelete'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_galeriamediosizado_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $data = GaleriaMediosIzado::find()->all();
        return $this->render('index', array('data' => $data));
    }

    public function actionCreate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_galeriamediosizado_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new GaleriaMediosIzado();
        $medio = MediosIzadoAcreditados::findOne($id);
        $fotos = GaleriaMediosIzado::findAll(['medio_izaje' => $id]);
        $path_picture = realpath(Yii::getAlias('@web') . '/uploads/images/medios_izado') . '/';

        if (isset($_POST['GaleriaMediosIzado'])) {
            $model->setAttributes($_POST['GaleriaMediosIzado']);
            $model->medio_izaje = $id;

            if (isset($_POST['GaleriaMediosIzado']['medioIzaje'])) {
                $model->medioIzaje = $_POST['GaleriaMediosIzado']['medioIzaje'];
            }

            $rnd = rand(0, 9999);
            $uploadedFile = UploadedFile::getInstance($model, 'imagen');

            try {
                if (!empty($uploadedFile)) {
                    $fileName = 'medio-' . "{$rnd}" . '.' . $uploadedFile->extensionName;
                    $uploadedFile->saveAs($path_picture . $fileName);
                    $model->imagen = $fileName;
                } else {
                    $model->imagen = null;
                }

                if ($model->save()) {
                    Traceador::crearTraza(
                        'Creacion',
                        'Imagen de galeria de medio de izado: ' . $_POST['GaleriaMediosIzado']['medioIzaje']['nombre']
                    );
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('galeriamediosizado/create&id=' . $model->medio_izaje));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['GaleriaMediosIzado'])) {
            $model->attributes = $_GET['GaleriaMediosIzado'];
        }

        return $this->render('create', array('model' => $model, 'medio' => $medio, 'fotos' => $fotos));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_galeriamediosizado_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $mi = MediosIzadoAcreditados::findOne($model->medio_izaje);

        $tmpmodel = $this->loadModel($id);
        $path_picture = realpath(Yii::getAlias('@web') . '/uploads/images/medios_izado') . '/';

        if (isset($_POST['GaleriaMediosIzado'])) {
            $model->setAttributes($_POST['GaleriaMediosIzado']);
            //$model->medioIzaje = $_POST['GaleriaMediosIzado']['medioIzaje'];
            $model->medio_izaje = $mi->id;

            $rnd = rand(0, 9999);
            $uploadedFile = UploadedFile::getInstance($model, 'imagen');
            try {
                if (!empty($uploadedFile)) {
                    //si el campo de la imagen está vacio o es null

                    if ($model->imagen != null) {
                        unlink($path_picture . $model->imagen);
                    }
                    $fileName = 'medio-' . "{$rnd}" . '.' . $uploadedFile->extensionName;
                    $uploadedFile->saveAs($path_picture . $fileName);
                    $model->imagen = $fileName;
                } else {
                    $model->imagen = $tmpmodel->imagen;
                }

                if ($model->save()) {
                    Traceador::crearTraza(
                        'Edicion',
                        'Imagen de galeria de medio de izado: ' . $_POST['GaleriaMediosIzado']['medioIzaje']['nombre']
                    );
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('galeriamediosizado/create&id=' . $mi->id));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        }

        return $this->render('update', array('model' => $model, 'mi' => $mi));
    }

    public function actionDelete($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_galeriamediosizado_delete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $path_picture = realpath(Yii::getAlias('@web') . '/uploads/images/medios_izado') . '/';

        try {
            if ($model->imagen != null) {
                unlink($path_picture . $model->imagen);
            }
            $model = $this->loadModel($id);
            Traceador::crearTraza('eliminacion', 'Imagen de galeria de medio de izaje: ' . $model->medio_izaje->nombre);
            $model->delete();
            if (isset($_GET['returnUrl'])) {
                return $this->redirect($_GET['returnUrl']);
            } else {
                return $this->redirect(array('galeriamediosizado/create&id=' . $model->medio_izaje));
            }
        } catch (Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }

        if (!Yii::$app->getRequest()->getIsAjaxRequest()) {
            return $this->redirect(array('index'));
        }
    }

    public function loadModel($id)
    {
        $model = GaleriaMediosIzado::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'galeria-medios-izado-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_galeriamediosizado_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $this->loadModel($id)->delete();
                    ++$successCount;
                }
                Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
            }
        } else {
            throw new HttpException(400, 'Petición no válida');
        }
    }

    public function actions()
    {
        return array(
            'toggle' => array(
                'class' => 'bootstrap.actions.TbToggleAction',
                'modelName' => 'GaleriaMediosIzado',
            ),
        );
    }
}
