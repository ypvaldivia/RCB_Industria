<?php

namespace app\controllers;

use app\components\Traceador;
use app\models\AcreditacionesInspectores;
use app\models\CategoriasAcreditaciones;
use Exception;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;

class CategoriasAcreditacionesController extends Controller
{


    //public $layout='main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */

    public function accessRules()
    {
        Yii::$app->session['menu'] = 'estructura';
        Yii::$app->session['submenu'] = '';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'create', 'update', 'batchDelete'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_categoriasacreditaciones_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $data = CategoriasAcreditaciones::find()->orderBy('id')->all();
        Traceador::crearTraza('Acceso', 'Listado de Acreditaciones Industriales');
        return $this->render('index', array('data' => $data));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_categoriasacreditaciones_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new CategoriasAcreditaciones();
        if (isset($_POST['CategoriasAcreditaciones'])) {
            $model->setAttributes($_POST['CategoriasAcreditaciones']);
            try {
                if ($model->save()) {
                    Traceador::crearTraza(
                        'creacion',
                        'Acreditación Industrial: ' . $model->grupo
                    );
                    Yii::$app->session->setFlash('success', 'Se adicionó un elemento exitosamente.');
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['CategoriasAcreditaciones'])) {
            Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
            $model->attributes = $_GET['CategoriasAcreditaciones'];
        }
        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {

        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_categoriasacreditaciones_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }

        $model = $this->loadModel($id);

        if (isset($_POST['CategoriasAcreditaciones'])) {
            $model->setAttributes($_POST['CategoriasAcreditaciones']);
            try {
                if ($model->save()) {
                    Traceador::crearTraza(
                        'edicion',
                        'Acreditación Industrial: ' . $model->grupo
                    );
                    Yii::$app->session->setFlash('success', 'Se actualizó el elemento exitosamente.');
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                $model->addError('id', $e->getMessage());
            }
        }
        return $this->render('update', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = CategoriasAcreditaciones::findOne($id);
        if ($model === null)
            throw new HttpException(404, Yii::t('app', 'La página solicitada no existe.'));
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'categorias-acreditaciones-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {

        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_categoriasacreditaciones_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $model = $this->loadModel($id);
                    $ac = AcreditacionesInspectores::findAll('acreditacion = :a', array(':a' => $model->id));
                    if ($ac != null) {
                        Yii::$app->session->setFlash('error', 'Esta homologación tiene acreditaciones de inspectores asociadas y no puede ser eliminada');
                    } else {
                        try {
                            $model->delete();
                            $successCount++;
                            Traceador::crearTraza(
                                'eliminacion',
                                'Acreditación Industrial: ' . $model->grupo
                            );
                        } catch (Exception $e) {
                            Yii::$app->session->setFlash('error', 'No se han eliminado los elementos seleccionados: ' . $e->getMessage());
                            break;
                        }
                        Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
                    }
                }
            }
        } else {
            throw new HttpException(400, 'Petición no válida');
        }
    }
}
