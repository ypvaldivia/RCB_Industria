<?php

namespace app\controllers;

use app\components\Traceador;
use app\models\CargosFuncionarios;
use Exception;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;

class CargosFuncionariosController extends Controller
{
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'estructura';
        Yii::$app->session['submenu'] = 'funcionarios';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'create', 'update', 'delete', 'batchDelete', 'toggle'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_cargosfuncionarios_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $data = CargosFuncionarios::find()->all();
        return $this->render('index', array('data' => $data));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_cargosfuncionarios_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new CargosFuncionarios();
        if (isset($_POST['CargosFuncionarios'])) {
            $model->setAttributes($_POST['CargosFuncionarios']);

            try {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Se adicionó un elemento exitosamente.');
                    Traceador::crearTraza(
                        'creacion',
                        'Creado cargo de funcionario: ' . $_POST['CargosFuncionarios']['cargo']
                    );
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['CargosFuncionarios'])) {
            $model->attributes = $_GET['CargosFuncionarios'];
        }
        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_cargosfuncionarios_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);

        if (isset($_POST['CargosFuncionarios'])) {
            $model->setAttributes($_POST['CargosFuncionarios']);
            try {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Se actualizó el elemento exitosamente.');
                    Traceador::crearTraza(
                        'edicion',
                        'Editado cargo de funcionario: ' . $_POST['CargosFuncionarios']['cargo']
                    );
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                $model->addError('id', $e->getMessage());
            }
        }
        return $this->render('update', array('model' => $model));
    }

    public function actionDelete($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_cargosfuncionarios_delete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        if (Yii::$app->request->isPostRequest) {
            try {
                $model = $this->loadModel($id);
                Traceador::crearTraza('eliminacion', 'Eliminado cargo de funcionario: ' . $model->cargo);
                $model->delete();
            } catch (Exception $e) {
                throw new HttpException(500, $e->getMessage());
            }

            if (!Yii::$app->getRequest()->getIsAjaxRequest()) {
                return $this->redirect(array('index'));
            }
        } else {
            throw new HttpException(400, Yii::t('app', 'Petición inválida.'));
        }
    }

    public function loadModel($id)
    {
        $model = CargosFuncionarios::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'La página solicitada no existe.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'cargos-funcionarios-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_cargosfuncionarios_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $this->loadModel($id)->delete();
                    ++$successCount;
                }
                Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
            }
        } else {
            throw new HttpException(400, 'Petición no válida');
        }
    }

    public function actions()
    {
        return array(
            'toggle' => array(
                'class' => 'bootstrap.actions.TbToggleAction',
                'modelName' => 'CargosFuncionarios',
            ),
        );
    }
}
