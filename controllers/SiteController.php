<?php

namespace app\controllers;

use Yii;
use Exception;

use yii\web\Controller;
use yii\web\HttpException;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

use yii\data\Pagination;

use app\components\Traceador;
use app\components\CDbCriteria;
use app\forms\BannerForm;
use app\forms\BuscaObjetoForm;
use app\models\ShortcutHomologacionesPersonas;
use app\models\ShortcutHomologacionesServiciosProductos;
use app\models\ShortcutHomologacionesProductos;
use app\models\ShortcutHomologacionesDacs;
use app\models\ShortcutHomologacionesMediosIzado;
use app\models\AcreditacionesInspectores;
use app\models\CargosFuncionarios;
use app\models\Trazas;
use app\models\Oficinas;
use app\models\Funcionarios;
use webvimark\modules\UserManagement\models\forms\LoginForm;
use webvimark\modules\UserManagement\models\UserVisitLog as ModelsUserVisitLog;

class SiteController extends Controller
{
    public $layout = 'main';

    //public $freeAccess = true;
    public $freeAccessActions = ['actionError', 'actionLogin'];

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' =>
                'webvimark\modules\UserManagement\components\GhostAccessControl'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ]
        ];
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        /* if (!Yii::$app->user->can('action_site_index')) {
            throw new HttpException(
                401,
                'Usted no tiene privilegios suficientes para realizar esta acción'
            );
        } */

        $stats = [];
        $lastOps = null;
        $esInspector = false;

        $ultimaSesion = array_reverse(
            ModelsUserVisitLog::find()
                ->where(['user_id' => Yii::$app->user->id])
                ->joinWith(['user u'])
                ->orderBy('u.id DESC')
                ->all()
        );

        $totalPersonas = ShortcutHomologacionesPersonas::find()
            ->where(['>', 'vigente', date('Y-m-d')])
            ->count();

        $totalServicios = ShortcutHomologacionesServiciosProductos::find()
            ->where(['>', 'vigencia', date('Y-m-d')])
            ->count();

        $totalProductos = ShortcutHomologacionesProductos::find()
            ->where(['>', 'vigencia', date('Y-m-d')])
            ->count();

        $totalMedios =
            ShortcutHomologacionesDacs::find()
            ->where(['>', 'vigencia', date('Y-m-d')])
            ->count() +
            ShortcutHomologacionesMediosIzado::find()
            ->where(['>', 'vigencia', date('Y-m-d')])
            ->count();

        $funcionarios = new Funcionarios();
        $porVencer = $funcionarios->getAcreditacionesXvencer1();
        $vencidas = $funcionarios->getAcreditacionesVencidas1();

        $PersXVencer = $this->getPersNextVenc();
        $ServsXVencer = $this->getServNextVenc();
        $ProdsXVencer = $this->getProdNextVenc();
        $DacsXVencer = $this->getDACsNextVenc();
        $MitsXVencer = $this->getMITsNextVenc();

        $stats['totalPersonas'] = $totalPersonas;
        $stats['totalProductos'] = $totalProductos;
        $stats['totalMedios'] = $totalMedios;
        $stats['totalServicios'] = $totalServicios;

        Yii::$app->session['menu'] = 'inicio';
        $user = Funcionarios::find(['usuario' => Yii::$app->user->id])->one();
        if ($user !== null) {
            $acreditacionesUser = AcreditacionesInspectores::findAll([
                'inspector' => Yii::$app->user->id
            ]);
            $oficina = $user->oficina0;

            $tmpSuperior = CargosFuncionarios::find([
                'superior' => $user->cargo0->id
            ]);
            if ($tmpSuperior == null) {
                $esInspector = true;
            }
        } else {
            $acreditacionesUser = null;
            $oficina = null;
        }

        if ($user !== null) {
            $lastOps = Trazas::find()
                ->where(['funcionario' => $user->id])
                ->orderBy('fecha DESC')
                ->limit(10)
                ->all();
        }

        Traceador::crearTraza('Acceso', 'Panel de Administraci&oacute;n');

        return $this->render('index', [
            'stats' => $stats,
            'acreditacionesVencidas' => $vencidas,
            'acreditacionesPorVencer' => $porVencer,
            'oficina' => $oficina,
            'acreditacionesInspector' => $acreditacionesUser,
            'lastOps' => $lastOps,
            'pxv' => $ProdsXVencer,
            'mxv' => $MitsXVencer,
            'dxv' => $DacsXVencer,
            'sxv' => $ServsXVencer,
            'psxv' => $PersXVencer,
            'esInspector' => $esInspector,
            'ultimaSesion' => $ultimaSesion
        ]);
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::$app->errorHandler->error) {
            if (Yii::$app->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                return $this->render('error', $error);
            }
        }
    }

    private function getPersNextVenc()
    {
        $user = Funcionarios::find()
            ->where(['usuario' => Yii::$app->user->id])
            ->one();
        if ($user != null) {
            $office = $user->oficina0->codigo;
            $criteria = new CDbCriteria();

            if ($user->oficina0->oficina_superior != $user->oficina0->id) {
                if ($user->oficina0->hasBoss()) {
                    //Se deben recorrer todas las oficinas inferiores no la primera que se encuentra
                    $keys = [];
                    $values = [];
                    $query = '';
                    $infOff = Oficinas::find()
                        ->where(['oficina_superior' => $user->oficina0->id])
                        ->all();
                    if ($infOff != null) {
                        $i = 0;
                        foreach ($infOff as $o) {
                            if ($query != '') {
                                $query =
                                    $query . ' OR certificado LIKE :m' . $i;
                            } else {
                                $query = '(certificado LIKE :m' . $i;
                            }
                            array_push($keys, ':m' . $i);
                            array_push($values, "%$o->codigo%");
                            $i++;
                        }

                        $query =
                            $query .
                            ' OR certificado LIKE :ppal) AND vigente BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                        array_push($keys, ':ppal');
                        array_push($values, "%$user->oficina0->codigo%");
                        $criteria->condition = $query;
                        $parameters = array_combine($keys, $values);
                        $criteria->params = $parameters;
                    } else {
                        $query =
                            'certificado LIKE :match AND vigente BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                        $criteria->condition = $query;
                        $criteria->params = array(':match' => "%$office%");
                    }
                } else {
                    $query =
                        'certificado LIKE :match AND vigente BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                    $criteria->condition = $query;
                    $criteria->params = array(':match' => "%$office%");
                }
            } else {
                $query =
                    'vigente BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                $criteria->condition = $query;
            }
            $model = ShortcutHomologacionesPersonas::findAll($criteria->scopes);

            return $model;
        } else {
            $criteria = new CDbCriteria();
            $query =
                'vigente BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
            $criteria->condition = $query;
            $criteria->order = 'vigente ASC';
            $model = ShortcutHomologacionesPersonas::findAll($criteria->scopes);

            return $model;
        }
    }

    private function getServNextVenc()
    {
        $user = Funcionarios::find(['usuario' => Yii::$app->user->id])->one();
        if ($user != null) {
            $office = $user->oficina0->codigo;
            $criteria = new CDbCriteria();
            if ($user->oficina0->oficina_superior != $user->oficina0->id) {
                if ($user->oficina0->hasBoss()) {
                    //Se deben recorrer todas las oficinas inferiores no la primera que se encuentra
                    $keys = [];
                    $values = [];
                    $query = '';
                    $infOff = Oficinas::findAll('oficina_superior = :s', array(
                        ':s' => $user->oficina0->id
                    ));
                    if ($infOff != null) {
                        $i = 0;
                        foreach ($infOff as $o) {
                            if ($query != '') {
                                $query =
                                    $query . ' OR certificado LIKE :m' . $i;
                            } else {
                                $query = '(certificado LIKE :m' . $i;
                            }
                            array_push($keys, ':m' . $i);
                            array_push($values, "%$o->codigo%");
                            $i++;
                        }

                        $query =
                            $query .
                            ' OR certificado LIKE :ppal) AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                        array_push($keys, ':ppal');
                        array_push($values, "%$office%");
                        $criteria->condition = $query;
                        $parameters = array_combine($keys, $values);
                        $criteria->params = $parameters;
                    } else {
                        $query =
                            'certificado LIKE :match AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                        $criteria->condition = $query;
                        $criteria->params = array(':match' => "%$office%");
                    }
                } else {
                    $query =
                        'certificado LIKE :match AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                    $criteria->condition = $query;
                    $criteria->params = array(':match' => "%$office%");
                }
            } else {
                $query =
                    'vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                $criteria->condition = $query;
            }
            $criteria->order = 'vigencia ASC';
            $model = ShortcutHomologacionesServiciosProductos::findAll(
                $criteria->scopes
            );

            return $model;
        } else {
            $criteria = new CDbCriteria();
            $query =
                'vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
            $criteria->condition = $query;
            $criteria->order = 'vigencia ASC';
            $model = ShortcutHomologacionesServiciosProductos::findAll(
                $criteria->scopes
            );

            return $model;
        }
    }

    private function getProdNextVenc()
    {
        $user = Funcionarios::find(['usuario' => Yii::$app->user->id])->one();
        if ($user != null) {
            $office = $user->oficina0->codigo;
            $criteria = new CDbCriteria();
            if ($user->oficina0->oficina_superior != $user->oficina0->id) {
                if ($user->oficina0->hasBoss()) {
                    //Se deben recorrer todas las oficinas inferiores no la primera que se encuentra
                    $keys = [];
                    $values = [];
                    $query = '';
                    $infOff = Oficinas::findAll('oficina_superior = :s', array(
                        ':s' => $user->oficina0->id
                    ));
                    if ($infOff != null) {
                        $i = 0;
                        foreach ($infOff as $o) {
                            if ($query != '') {
                                $query =
                                    $query . ' OR certificado LIKE :m' . $i;
                            } else {
                                $query = '(certificado LIKE :m' . $i;
                            }
                            array_push($keys, ':m' . $i);
                            array_push($values, "%$o->codigo%");
                            $i++;
                        }

                        $query =
                            $query .
                            ' OR certificado LIKE :ppal) AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                        array_push($keys, ':ppal');
                        array_push($values, "%$office%");
                        $criteria->condition = $query;
                        $parameters = array_combine($keys, $values);
                        $criteria->params = $parameters;
                    } else {
                        $query =
                            'certificado LIKE :match AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                        $criteria->condition = $query;
                        $criteria->params = array(':match' => "%$office%");
                    }
                } else {
                    $query =
                        'certificado LIKE :match AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                    $criteria->condition = $query;
                    $criteria->params = array(':match' => "%$office%");
                }
            } else {
                $query =
                    'vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                $criteria->condition = $query;
            }
            $criteria->order = 'vigencia ASC';
            $model = ShortcutHomologacionesProductos::findAll(
                $criteria->scopes
            );

            return $model;
        } else {
            $criteria = new CDbCriteria();
            $query =
                'vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
            $criteria->condition = $query;
            $criteria->order = 'vigencia ASC';
            $model = ShortcutHomologacionesProductos::findAll(
                $criteria->scopes
            );

            return $model;
        }
    }

    private function getMITsNextVenc()
    {
        $user = Funcionarios::find(['usuario' => Yii::$app->user->id])->one();
        if ($user != null) {
            $office = $user->oficina0->codigo;
            $criteria = new CDbCriteria();
            if ($user->oficina0->oficina_superior != $user->oficina0->id) {
                if ($user->oficina0->hasBoss()) {
                    //Se deben recorrer todas las oficinas inferiores no la primera que se encuentra
                    $keys = [];
                    $values = [];
                    $query = '';
                    $infOff = Oficinas::findAll('oficina_superior = :s', array(
                        ':s' => $user->oficina0->id
                    ));
                    if ($infOff != null) {
                        $i = 0;
                        foreach ($infOff as $o) {
                            if ($query != '') {
                                $query =
                                    $query . ' OR certificado LIKE :m' . $i;
                            } else {
                                $query = '(certificado LIKE :m' . $i;
                            }
                            array_push($keys, ':m' . $i);
                            array_push($values, "%$o->codigo%");
                            $i++;
                        }

                        $query =
                            $query .
                            ' OR certificado LIKE :ppal) AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                        array_push($keys, ':ppal');
                        array_push($values, "%$office%");
                        $criteria->condition = $query;
                        $parameters = array_combine($keys, $values);
                        $criteria->params = $parameters;
                    } else {
                        $query =
                            'certificado LIKE :match AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                        $criteria->condition = $query;
                        $criteria->params = array(':match' => "%$office%");
                    }
                } else {
                    $query =
                        'certificado LIKE :match AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                    $criteria->condition = $query;
                    $criteria->params = array(':match' => "%$office%");
                }
            } else {
                $query =
                    'vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                $criteria->condition = $query;
            }
            $criteria->order = 'vigencia ASC';
            $model = ShortcutHomologacionesMediosIzado::findAll(
                $criteria->scopes
            );

            return $model;
        } else {
            $criteria = new CDbCriteria();
            $query =
                'vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
            $criteria->condition = $query;
            $criteria->order = 'vigencia ASC';
            $model = ShortcutHomologacionesMediosIzado::findAll(
                $criteria->scopes
            );

            return $model;
        }
    }

    private function getDACsNextVenc()
    {
        $user = Funcionarios::find(['usuario' => Yii::$app->user->id])->one();
        if ($user != null) {
            $office = $user->oficina0->codigo;
            $criteria = new CDbCriteria();
            if ($user->oficina0->oficina_superior != $user->oficina0->id) {
                if ($user->oficina0->hasBoss()) {
                    //Se deben recorrer todas las oficinas inferiores no la primera que se encuentra
                    $keys = [];
                    $values = [];
                    $query = '';
                    $infOff = Oficinas::findAll('oficina_superior = :s', array(
                        ':s' => $user->oficina0->id
                    ));
                    if ($infOff != null) {
                        $i = 0;
                        foreach ($infOff as $o) {
                            if ($query != '') {
                                $query =
                                    $query . ' OR certificado LIKE :m' . $i;
                            } else {
                                $query = '(certificado LIKE :m' . $i;
                            }
                            array_push($keys, ':m' . $i);
                            array_push($values, "%$o->codigo%");
                            $i++;
                        }

                        $query =
                            $query .
                            ' OR certificado LIKE :ppal) AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                        array_push($keys, ':ppal');
                        array_push($values, "%$office%");
                        $criteria->condition = $query;
                        $parameters = array_combine($keys, $values);
                        $criteria->params = $parameters;
                    } else {
                        $query =
                            'certificado LIKE :match AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                        $criteria->condition = $query;
                        $criteria->params = array(':match' => "%$office%");
                    }
                } else {
                    $query =
                        'certificado LIKE :match AND vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                    $criteria->condition = $query;
                    $criteria->params = array(':match' => "%$office%");
                }
            } else {
                $query =
                    'vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
                $criteria->condition = $query;
            }
            $criteria->order = 'vigencia ASC';
            $model = ShortcutHomologacionesDacs::findAll($criteria->scopes);

            return $model;
        } else {
            $criteria = new CDbCriteria();
            $query =
                'vigencia BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 3 MONTH)';
            $criteria->condition = $query;
            $criteria->order = 'vigencia ASC';
            $model = ShortcutHomologacionesDacs::findAll($criteria->scopes);

            return $model;
        }
    }

    /**
     * Displays the login page.
     */
    public function actionLogin()
    {
        $model = new LoginForm();

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                return $this->redirect(Yii::$app->user->returnUrl);
            }
        }
        // display the login form
        return $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect(Yii::$app->homeUrl);
    }

    public function actionChangeBanner()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_site_changebanner')) {
            throw new HttpException(
                401,
                'Usted no tiene privilegios suficientes para realizar esta acción'
            );
        }
        Yii::$app->session['menu'] = 'configuracion';
        //$this->layout = 'main';

        $model = new BannerForm();
        $path_picture = realpath(Yii::getAlias('@web') . '/uploads/images') . '/';

        if (isset($_POST['FormBanner'])) {
            $model->setAttributes($_POST['FormBanner']);
            $uploadedFile = UploadedFile::getInstance($model, 'imagen');

            try {
                if (!empty($uploadedFile)) {
                    if (file_exists($path_picture . 'bannerHeader.jpg')) {
                        unlink($path_picture . 'bannerHeader.jpg');
                    }
                    $uploadedFile->saveAs($path_picture . 'bannerHeader.jpg');
                    $model->imagen = 'bannerHeader.jpg';
                    Yii::$app->session->setFlash(
                        'success',
                        'La imagen se ha modificado exitosamente. Si no puede visualizarla actualice esta ventana del navegador.'
                    );
                    Traceador::crearTraza('Edicion', 'cambiado el banner');
                }
                //$this->redirect(array('changeBanner',array('model'=>$model)));
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('changeBanner', array('model' => $model));
    }

    public function actionLibroCertificados($type)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_site_librocertificados')) {
            throw new HttpException(
                401,
                'Usted no tiene privilegios suficientes para realizar esta acción'
            );
        }
        Yii::$app->session['menu'] = 'libro';
        $searcher = new BuscaObjetoForm();
        if (isset($_POST['FormBuscaObjeto'])) {
            $searcher->setMyAttributes($_POST['FormBuscaObjeto']);
        }
        switch ($type) {
            case 'inspeccionPersona':
                $model = ShortcutHomologacionesPersonas::find();
                break;
            case 'inspeccionLabServ':
                $model = ShortcutHomologacionesServiciosProductos::find();
                break;
            case 'inspeccionProducto':
                $model = ShortcutHomologacionesProductos::find();
                break;
            case 'inspeccionDAC':
                $model = ShortcutHomologacionesDacs::find();
                break;
            case 'inspeccionMIT':
                $model = ShortcutHomologacionesMediosIzado::find();
                break;
        }
        Traceador::crearTraza('Acceso', 'Libro de Certificados de ' . $type);
        $countQuery = clone $model;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 20;
        $data = $model->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('libroCertificados', [
            'model' => $data,
            'pages' => $pages,
            'type' => $type,
            'searcher' => $searcher
        ]);
    }
}
