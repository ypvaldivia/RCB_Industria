<?php

namespace app\controllers;

use app\components\CDbCriteria;
use app\components\Traceador;
use app\forms\BuscaObjetoForm;
use app\models\Entidades;
use Exception;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;

class EntidadesController extends Controller
{
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'objetoInspeccion';
        Yii::$app->session['submenu'] = '';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'create', 'update', 'batchDelete', 'searchItems'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_entidades_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $searcher = new BuscaObjetoForm();
        if (isset($_POST['FormBuscaObjeto'])) {
            $searcher->setMyAttributes($_POST['FormBuscaObjeto']);
            $criteria = $searcher->execSearchEntidades();
            $count = Entidades::findAll($criteria->condition, $criteria->params);
            $pages = new Pagination(Count($count));
            $pages->pageSize = 50;

            $data = Entidades::findAll($criteria->condition, $criteria->params);
        } else {
            $criteria = new CDbCriteria();
            $criteria->order = 'nombre ASC';
            $count = Entidades::findAll($criteria->condition, $criteria->params);
            $pages = new Pagination(Count($count));
            $pages->pageSize = 30;

            $data = Entidades::findAll($criteria->condition, $criteria->params);
        }
        $model = Entidades::find();
        $countQuery = clone $model;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 20;
        $data = $model->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('index', array('data' => $data, 'pages' => $pages, 'searcher' => $searcher));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_entidades_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new Entidades();
        if (isset($_POST['Entidades'])) {
            $model->setAttributes($_POST['Entidades']);

            if (isset($_POST['Entidades']['oficinaAcredita'])) {
                $model->oficinaAcredita = $_POST['Entidades']['oficinaAcredita'];
            }
            if (isset($_POST['Entidades']['organismo0'])) {
                $model->organismo0 = $_POST['Entidades']['organismo0'];
            }
            if (isset($_POST['Entidades']['provincia0'])) {
                $model->provincia0 = $_POST['Entidades']['provincia0'];
            }

            try {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Se adicionó un elemento exitosamente.');
                    Traceador::crearTraza('Creacion', 'Entidad: ' . $model->nombre);
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['Entidades'])) {
            $model->attributes = $_GET['Entidades'];
        }
        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_entidades_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);

        if (isset($_POST['Entidades'])) {
            $model->setAttributes($_POST['Entidades']);
            $model->oficinaAcredita = $_POST['Entidades']['oficinaAcredita'];
            $model->organismo0 = $_POST['Entidades']['organismo0'];
            $model->provincia0 = $_POST['Entidades']['provincia0'];
            try {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Se actualizó el elemento exitosamente.');
                    Traceador::crearTraza('Edicion', 'Entidad: ' . $model->nombre);
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                $model->addError('id', $e->getMessage());
            }
        }
        return $this->render('update', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = Entidades::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'La página solicitada no existe.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'entidades-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_entidades_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $model = $this->loadModel($id);
                    Traceador::crearTraza(
                        'eliminacion',
                        'Fue eliminada la Entidad: ' . $model->nombre
                    );
                    try {
                        $model->delete();
                        ++$successCount;
                    } catch (Exception $e) {
                        Yii::$app->session->setFlash('error', 'Ocurrio un error al intentar eliminar "' . $model->nombre . '" consistente en: "' . $e->getMessage());
                        break;
                    }
                }
                Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente.');
            }
        } else {
            throw new HttpException(400, 'Petición no válida.');
        }
    }

    public function actionSearchItems()
    {
        $search = $_GET['q'];
        $items = Entidades::findAll('nombre LIKE :n', array(':n' => "%$search%"));
        if ($items != null) {
            foreach ($items as $key => $value) {
                $data[] = array('id' => $value['id'], 'text' => $value['nombre']);
            }
        } else {
            $data[] = array('id' => '0', 'text' => 'El elemento no existe');
        }

        echo json_encode($data);
    }
}
