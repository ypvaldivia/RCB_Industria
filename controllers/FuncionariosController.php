<?php

namespace app\controllers;

use app\components\CDbCriteria;
use app\components\Traceador;
use app\forms\BuscarInspectoresForm;
use app\models\Funcionarios;
use DateTime;
use Exception;
use webvimark\modules\UserManagement\components\UserIdentity;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use webvimark\modules\UserManagement\models\User;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

class FuncionariosController extends Controller
{
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'estructura';
        Yii::$app->session['submenu'] = 'funcionarios';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'create', 'update', 'ProximosVencimientos', 'vencimientos', 'batchDelete'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_funcionarios_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new BuscarInspectoresForm();
        if (isset($_POST['FormBuscarInspectores'])) {
            if ($_POST['FormBuscarInspectores']['oficina'] == null && $_POST['FormBuscarInspectores']['servicio'] == null) {
                $data = Funcionarios::find()->all();
            } else {
                $criteria = new CDbCriteria();
                $criteria->alias = 't';
                $criteria->distinct = true;
                $criteria->join = 'JOIN acreditaciones_inspectores ON acreditaciones_inspectores.inspector= t.id';
                if ($_POST['FormBuscarInspectores']['oficina'] != null && $_POST['FormBuscarInspectores']['servicio'] == null) {
                    $criteria->condition = 't.oficina = :o';
                    $criteria->params = array(':o' => $_POST['FormBuscarInspectores']['oficina']);
                } elseif ($_POST['FormBuscarInspectores']['oficina'] == null && $_POST['FormBuscarInspectores']['servicio'] != null) {
                    $criteria->condition = 'acreditaciones_inspectores.acreditacion = :a';
                    $criteria->params = array(':a' => $_POST['FormBuscarInspectores']['servicio']);
                } elseif ($_POST['FormBuscarInspectores']['oficina'] != null && $_POST['FormBuscarInspectores']['servicio'] != null) {
                    $criteria->condition = 't.oficina = :o AND acreditaciones_inspectores.acreditacion = :a';
                    $criteria->params = array(':o' => $_POST['FormBuscarInspectores']['oficina'], ':a' => $_POST['FormBuscarInspectores']['servicio']);
                }
                $data = Funcionarios::findAll($criteria->condition, $criteria->params);
            }
        } else {
            $data = Funcionarios::find()->all();
        }
        return $this->render('index', array('data' => $data, 'model' => $model));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_funcionarios_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new Funcionarios();
        $path_picture = realpath(Yii::getAlias('@web') . '/uploads/images/funcionarios') . '/';

        if (isset($_POST['Funcionarios'])) {
            $model->setAttributes($_POST['Funcionarios']);

            $user = new User();
            $assignedRol = new Role();
            $user->username = $model->username;
            $user->password = sha1($model->password);
            $user->email = $model->correo;
            $user->state = 1;
            $user->authkey = UserIdentity::getAuthKey();
            $user->regdate = new DateTime();


            if ($user->password != $model->password2) {
                Yii::$app->session->setFlash('error', 'Las contraseñas no coinciden');
            }

            if (isset($_POST['Funcionarios']['categoria0'])) {
                $model->categoria0 = $_POST['Funcionarios']['categoria0'];
            }
            if (isset($_POST['Funcionarios']['cargo0'])) {
                $model->cargo0 = $_POST['Funcionarios']['cargo0'];
            }
            if (isset($_POST['Funcionarios']['oficina0'])) {
                $model->oficina0 = $_POST['Funcionarios']['oficina0'];
            }
            if (isset($_POST['Funcionarios']['rol'])) {
                $model->rol = $_POST['Funcionarios']['rol'];
                $assignedRol->itemname = $model->rol;
            }

            $rnd = rand(0, 999);
            $uploadedFile = UploadedFile::getInstance($model, 'imagen_firma');

            try {
                if (!empty($uploadedFile)) {
                    $fileName = 'fun-' . "{$rnd}" . '.' . $uploadedFile->extensionName;
                    $uploadedFile->saveAs($path_picture . $fileName);
                    $model->imagen_firma = $fileName;
                } else {
                    $model->imagen_firma = null;
                }

                //Buscar en la misma oficina un jefe activo
                if ($model->cargo == 3 || $model->cargo == 5 || $model->cargo == 5) {
                    $tmp = Funcionarios::find(
                        'cargo = :c AND inactivo = :i AND oficina = :o',
                        array(':c' => $model->cargo, ':i' => 0, ':o' => $model->oficina)
                    );
                    if ($tmp != null) {
                        $model->addError('id', 'Ya existe un Especialista activo con el mismo cargo en esta oficina, debe desactivarlo primero');
                        return $this->render('create', array('model' => $model));
                    }
                } else {
                    if ($user->save()) {
                        $assignedRol->userid = $user->iduser;
                        $model->usuario = $user->iduser;
                        $assignedRol->save();
                        if ($model->save()) {
                            Traceador::crearTraza(
                                'Creacion',
                                'Funcionario: ' . $model->nombre_apellidos
                            );
                            if (isset($_GET['returnUrl'])) {
                                return $this->redirect($_GET['returnUrl']);
                            } else {
                                return $this->redirect(array('index'));
                            }
                        }
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['Funcionarios'])) {
            $model->attributes = $_GET['Funcionarios'];
        }

        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_funcionarios_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $band = true;
        $saved = false;
        $model = $this->loadModel($id);
        $tmpmodel = $this->loadModel($id);
        $path_picture = realpath(Yii::getAlias('@web') . '/uploads/images/funcionarios') . '/';

        if (isset($_POST['Funcionarios'])) {
            $model->setAttributes($_POST['Funcionarios']);
            $model->categoria = $_POST['Funcionarios']['categoria0'];
            $model->cargo = $_POST['Funcionarios']['cargo0'];
            $model->oficina = $_POST['Funcionarios']['oficina0'];
            $model->es_inspector = $_POST['Funcionarios']['es_inspector'];
            $rnd = rand(0, 999);
            $uploadedFile = UploadedFile::getInstance($model, 'imagen_firma');

            if ($model->usuario0 == null) {
                $user = new User();
                $user->username = $model->username;
                $user->password = $model->password;
                $user->email = $model->correo;
                $user->state = 1;
                /* $user->authkey = CrugeUtil::hash($user->username.'-'.$user->password);
                $user->regdate = CrugeUtil::now(); */

                try {
                    //Buscar en la misma oficina un jefe activo
                    if ($_POST['Funcionarios']['cargo0'] == 3 || $_POST['Funcionarios']['cargo0'] == 5 || $_POST['Funcionarios']['cargo0'] == 6) {
                        $tmp = Funcionarios::find('cargo = :c AND inactivo = :i AND oficina = :o', array(':c' => $model->cargo0->id, ':i' => 0, ':o' => $model->oficina0->id));
                        if ($tmp != null) {
                            $model->addError('id', 'Ya existe un Especialista activo con el mismo cargo en esta oficina, debe desactivarlo primero');
                            return $this->render('update', array('model' => $model));
                        } else {
                            if ($user->save()) {
                                $model->usuario = $user->iduser;
                            }
                        }
                    } else {
                        if ($user->save()) {
                            $model->usuario = $user->iduser;
                        }
                    }
                } catch (Exception $e) {
                    $model->addError('id', $e->getMessage());
                    $band = false;
                }
            }
            if ($band == true) {
                try {
                    if (!empty($uploadedFile)) {
                        //si el campo de la imagen está vacio o es null

                        if ($model->imagen_firma != null) {
                            unlink($path_picture . $model->imagen_firma);
                        }
                        $fileName = 'fun-' . "{$rnd}" . '.' . $uploadedFile->extensionName;
                        $uploadedFile->saveAs($path_picture . $fileName);
                        $model->imagen_firma = $fileName;
                    } else {
                        $model->imagen_firma = $tmpmodel->imagen_firma;
                    }
                    //Buscar en la misma oficina un jefe activo
                    if ($_POST['Funcionarios']['cargo0'] == 3 || $_POST['Funcionarios']['cargo0'] == 5 || $_POST['Funcionarios']['cargo0'] == 6) {
                        $tmp = Funcionarios::find('cargo = :c AND inactivo = :i AND oficina = :o', array(':c' => $model->cargo0->id, ':i' => 0, ':o' => $model->oficina0->id));
                        if ($tmp != null && $model->id != $tmp->id) {
                            $model->addError('id', 'Ya existe un Especialista activo con el mismo cargo en esta oficina, debe desactivarlo primero');
                        } else {
                            if ($model->update()) {
                                Traceador::crearTraza(
                                    'Edicion',
                                    'Funcionario: ' . $model->nombre_apellidos
                                );
                                if (isset($_GET['returnUrl'])) {
                                    return $this->redirect($_GET['returnUrl']);
                                } else {
                                    return $this->redirect(array('index'));
                                }
                            }
                        }
                    } else {
                        if ($model->update()) {
                            Traceador::crearTraza(
                                'Edicion',
                                'Funcionario: ' . $model->nombre_apellidos
                            );
                            if (isset($_GET['returnUrl'])) {
                                return $this->redirect($_GET['returnUrl']);
                            } else {
                                return $this->redirect(array('index'));
                            }
                        }
                    }
                } catch (Exception $e) {
                    $model->addError('id', $e->getMessage());
                }
            }
        }

        return $this->render('update', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = Funcionarios::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'funcionarios-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_funcionarios_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $candidate = $this->loadModel($id);
                    //Delete Assigned roles                    
                    $tmpRoles = Role::getUserRoles($candidate->usuario->id);
                    if ($tmpRoles != null) {
                        foreach ($tmpRoles as $rol) {
                            $rol->delete();
                        }
                    }
                    //Delete User
                    $tmpUser = UserIdentity::findOne(['id' => $candidate->usuario->id]);
                    if ($tmpUser != null) {
                        $tmpUser->delete();
                    }
                    //Delete picture
                    if ($candidate->imagen_firma != null) {
                        $path_picture = realpath(Yii::getAlias('@web') . '/uploads/images/funcionarios') . '/';
                        unlink($path_picture . $candidate->imagen_firma);
                    }
                    //Delete item
                    $candidate->delete();
                    Traceador::crearTraza(
                        'Eliminacion',
                        'Funcionario: ' . $candidate->nombre_apellidos
                    );
                    ++$successCount;
                }
                Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
            }
        } else {
            throw new HttpException(500, 'Petición no válida');
        }
    }

    public function actions()
    {
        return array(
            'toggle' => array(
                'class' => 'bootstrap.actions.TbToggleAction',
                'modelName' => 'Funcionarios',
            ),
        );
    }

    /**
     * Proximos vencimientos de un funcionario(inspector).
     */
    public function actionProximosVencimientos()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_funcionarios_proximosvencimientos')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        Yii::$app->session['menu'] = 'estructura';
        Yii::$app->session['submenu'] = 'inspectores';

        //$porVencer=Funcionarios::getAcreditacionesXvencer();
        $funRepo = new Funcionarios();
        $porVencer = $funRepo->getAcreditacionesXvencer1();

        return $this->render('proximosVencimientos', array(
            'porVencer' => $porVencer,
        ));
    }

    public function actionVencimientos()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_funcionarios_vencimientos')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        Yii::$app->session['menu'] = 'estructura';
        Yii::$app->session['submenu'] = 'inspectores';

        //$vencidas = Funcionarios::getAcreditacionesVencidas();
        $funRepo = new Funcionarios();
        $vencidas = $funRepo->getAcreditacionesVencidas1();

        return $this->render('vencimientos', array(
            'vencidas' => $vencidas,
        ));
    }
}
