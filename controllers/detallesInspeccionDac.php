<?php

namespace app\controllers;

use app\models\Funcionarios;
use app\models\Inspecciones;
use Yii;

$cantidadVisitas = count($model->visitasInspecciones);
$ultimaVisita = $model->visitasInspecciones[$cantidadVisitas - 1];
?>
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Inspecciones de Dispositivos Auxiliares de Carga <small> Gesti&oacute;n de la informaci&oacute;n de Inspecciones</small>
        </h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="<?php echo Yii::$app->createUrl('site/index') ?>">Incio</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <i class="icon-barcode"></i>
                <a href="<?php echo Yii::$app->createUrl('inspecciones/indexDacs') ?>">Inspecciones - </a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="#">Dispositivos Auxiliares de Carga -</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="#">Detalles de Inspecci&oacute;n</a>
            </li>
            <li class="btn-group">
                <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                    <span>Men&uacute;</span> <i class="icon-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <?php if ($model->estado != 4 && $model->estado != 5) { ?>
                        <li>
                            <a href="<?php echo Yii::$app->createUrl('inspecciones/editarDac', array('id' => $model->id)) ?>">
                                <i class="icon-edit"></i> Modificar esta Inspecci&oacute;n&nbsp;&nbsp;
                            </a>
                        </li>
                    <?php } ?>
                    <li>
                        <a href="<?php echo Yii::$app->createUrl('inspecciones/imprimirDetalles', array('id' => $model->id, 'formType' => 'inspeccionDac')) ?>" class="btnPrint">
                            <i class="icon-print"></i> Imprimir este Informe
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo Yii::$app->createUrl('inspecciones/PDFDetalles', array('id' => $model->id, 'formType' => 'inspeccionDac')) ?>">
                            <i class="icon-file-alt"></i> Generar PDF
                        </a>
                    </li>
                    <?php if ($ultimaVisita->aprobado == 1 && $model->estado != 4 && $model->estado != 5) { ?>
                        <?php if ($model->cerrada == 1) { ?>
                            <li>
                                <a href="<?php echo Yii::$app->createUrl('inspecciones/aprobaciones', array('id' => $model->id, 'formType' => 'inspeccionDac')) ?>">
                                    <i class="icon-user"></i>Aprobaci&oacute;n por niveles
                                </a>
                            </li>
                        <?php } else { ?>
                            <li>
                                <a href="<?php echo Yii::$app->createUrl('inspecciones/cerrarInspeccionDac', array('id' => $model->id)) ?>">
                                    <i class="icon-search"></i>Enviar a Revisi&oacute;n
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>

                    <li class="divider"></li>
                    <li>
                        <a href="<?php echo Yii::$app->createUrl('inspecciones/indexDacs') ?>">
                            <i class="icon-barcode"></i> &Iacute;ndice de Inspecciones
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php } ?>

<!-- BEGIN PAGE CONTENT-->
<div class="container">
    <div class="row>
        <div class=" col-xs-12 column sortable">
        <!-- BEGIN card card-->
        <?php $estado = 'Inspecci&oacute;n ' . $model->estado0->estado; ?>
        <div class=" card box <?php echo $model->estado0->color; ?>">
            <div class="card-header">
                <div class="caption">
                    <i class="icon-barcode"></i><?php echo $estado; ?>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="card-body">
                <div class="container">
                    <div class="container">
                        <div class="col-md-12 row">
                            <center>
                                <img src="<?php echo Yii::$app->baseUrl ?>/images/bannerHeader.jpg" width="100%">
                                <br /><br />
                                <div class="row">
                                    <div class="col-md-10">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-2">
                                        <span class="pull-right">
                                            <h5>R7/PC401&nbsp;&nbsp;&nbsp;</h5>
                                        </span>
                                    </div>
                                </div>
                                <h2>Informe de Inspecci&oacute;n</h2>
                            </center>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-10">
                                <h5><b>Entidad:</b> <?php echo $model->entidad0->nombre; ?></h5>
                            </div>
                            <div class="col-md-2">
                                <h5>
                                    <center><b>N&deg;:</b> <?php echo $model->numero_control; ?></center>
                                </h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h5><b>Homologaci&oacute;n:</b>
                                    <?php
                                    if ($model->homologacion0->titulo_anuario == null) {
                                        echo $model->homologacion0->titulo_inspector;
                                    } else {
                                        echo $model->homologacion0->titulo_anuario;
                                    }
                                    if ($model->certificado != null) {
                                        echo '  (';
                                        echo $model->certificado;
                                        echo ')';
                                    }
                                    ?>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <h5><b>Dependencia:</b> <?php echo $model->oficina0->codigo; ?></h5>
                            </div>
                            <div class="col-md-6">
                                <h5><b>Tipo de Inspecci&oacute;n:</b> <?php echo $model->tipoHomologacion->tipo; ?></h5>
                            </div>
                            <div class="col-md-2">
                                <h5>
                                    <center><b>N&deg; Visita:</b> <?php echo $cantidadVisitas; ?></center>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <h5><b>Fecha:</b> <?php echo Inspecciones::convertirFecha($ultimaVisita->fecha); ?></h5>
                            </div>
                            <div class="col-md-8">
                                <?php if (isset($model->lugar_inspeccion)) { ?>
                                    <h5><b>Lugar de inspecci&oacute;n:</b> <?php echo $model->lugar_inspeccion ?></h5>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <?php if (isset($model->inspeccionesXDacses)) { ?>
                        <div class="row">
                            <div class="col-md-12 headline">
                                <h4>
                                    DACs Inspeccionados:
                                    <span class="label label-info"><?php echo count($model->inspeccionesXDacses); ?></span>
                                    <div class="pull-right">
                                        <?php if ($model->cerrada == 1 && $model->estado != 2) { ?>
                                            <a href="<?php echo Yii::$app->createUrl('inspecciones/aprobaciones', array('id' => $model->id, 'formType' => 'inspeccionDac')) ?>" class="btn default btn-sm blue"><i class="icon-file"></i> Aprobaci&oacute;n por niveles
                                            </a>
                                        <?php } elseif ($model->estado != 2 && $model->estado != 3 && Funcionarios::getRevision(Yii::$app->user->id, $model->id)) { ?>
                                            <a href="<?php echo Yii::$app->createUrl('inspecciones/cerrarInspeccionDAC', array('id' => $model->id)) ?>" class="btn default btn-sm green"><i class="icon-search"></i> Informe completo, enviar a Revisi&oacute;n
                                            </a>
                                        <?php } elseif (($model->cerrada == 0 && $model->estado == 3)) { ?>
                                            <a href="<?php echo Yii::$app->createUrl('inspecciones/aprobaciones', array('id' => $model->id, 'formType' => 'inspeccionDac')) ?>" class="btn default btn-sm blue"><i class="icon-file"></i> Aprobaci&oacute;n por niveles
                                            </a>
                                            <?php if (Funcionarios::getRevision(Yii::$app->user->id, $model->id)) { ?>
                                                <a href="<?php echo Yii::$app->createUrl('inspecciones/cerrarInspeccionDAC', array('id' => $model->id)) ?>" class="btn default btn-sm green"><i class="icon-search"></i> Informe completo, enviar a Revisi&oacute;n
                                                </a>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </h4>
                            </div>
                        </div>
                        <br />
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-hover table-advance">
                                <thead>
                                    <th>N&deg;</th>
                                    <th>Nombre</th>
                                    <th>Propietario</th>
                                    <th>Certificado</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($model->inspeccionesXDacses as $prod) {
                                    ?>
                                        <tr>
                                            <td style="vertical-align: middle;"><?php echo $prod->numero ?></td>
                                            <td style="vertical-align: middle;"><?php echo $prod->dac0->nombre ?></td>
                                            <td style="vertical-align: middle;"><?= ($prod->dac0->propietario0 != null) ? $prod->dac0->propietario0->nombre : "Sin Definir" ?></td>
                                            <td style="vertical-align: middle;">
                                                <?php
                                                if ($prod->cprovisional != null) {
                                                    if ($prod->cdefinitivo != null) {
                                                ?>
                                                        <a href="<?php echo Yii::$app->baseUrl; ?>/certificados/definitivos/dacs/<?php echo $prod->cdefinitivo; ?>"><?= $funcionario->cdefinitivo; ?></a>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <a href="<?php echo Yii::$app->baseUrl; ?>/certificados/provisionales/dacs/<?= $prod->cprovisional; ?>" target="_blank"><?= $funcionario->cprovisional; ?></a>
                                                <?php
                                                    }
                                                } else {
                                                    echo "No se ha generado ningún certificado";
                                                }
                                                ?>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <?php if ($ultimaVisita->aprobado == 1 && $model->tipoHomologacion->genera_certificado == 1) { ?>
                                                    <?php if ($model->estado == 4 || $model->estado == 5) { ?>
                                                        <?php if ($prod->aprobado == 1) { ?>
                                                            <a href="<?php echo Yii::$app->createUrl('certificados/certificado', array('obj' => $prod->id, 'tipoObj' => 4, 'tipoCe' => 2)) ?>" class="btn default btn-sm blue" target="_blank"><i class="icon-file"></i> Generar Certificado Definitivo
                                                            </a>
                                                        <?php } else { ?>
                                                            <span class="label label-danger">Desaprobado</span>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <?php if ($prod->aprobado == 1) { ?>
                                                            <a href="<?php echo Yii::$app->createUrl('certificados/certificado', array('obj' => $prod->id, 'tipoObj' => 4, 'tipoCe' => 1)) ?>" class="btn default btn-sm blue" target="_blank"><i class="icon-file"></i> Generar Certificado Provisional
                                                            </a>
                                                        <?php } else { ?>
                                                            <span class="label label-danger">Desaprobado</span>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <?php if ($model->tipoHomologacion->genera_certificado == 0) { ?>
                                                        <span class="label label-info">No requiere certificado</span>
                                                    <?php } else { ?>
                                                        <span class="label label-warning">Pendiente</span>
                                                <?php }
                                                } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                    <!-- BEGIN PAGE TIMELINE-->
                    <div class="row">
                        <div class="col-md-12 headline">
                            <h4>
                                Visitas Realizadas:
                                <span class="label label-info"><?php echo $cantidadVisitas; ?></span>
                                <?php if ($ultimaVisita->aprobado == 0) { ?>
                                    <a href="<?php echo Yii::$app->createUrl('inspecciones/createVisitaDAC', array('id' => $model->id)) ?>" class="btn default btn green-stripe pull-right"><i class="icon-plus"></i> Registrar nueva visita
                                    </a>
                                <?php } ?>
                            </h4>
                            <br />
                            <div class="table-toolbar">
                                <div class="news-blocks fade in">
                                    <button class="close" data-dismiss="alert" type="button"></button>
                                    <div class="row">
                                        <div class="col-md-1">
                                            <h4 class="alert-heading">Leyenda:</h4>
                                        </div>
                                        <div class="col-md-11">
                                            <span class="label label-danger">
                                                <i class="icon-remove"></i> Rechazadas</span>&nbsp;
                                            <span class="label label-success">
                                                <i class="icon-ok"></i> Aprobadas</span> |
                                            <span class="">
                                                <i class="icon-user"></i> Es Inspector</span>&nbsp;
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="timeline">
                                <?php foreach (array_reverse($model->visitasInspecciones) as $visita) { ?>
                                    <li <?php if ($visita->aprobado == 0) {
                                            echo 'class="timeline-red"';
                                        } else {
                                            echo 'class="timeline-green"';
                                        } ?>>
                                        <div class="timeline-time">
                                            <span class="date"><?php echo $visita->fecha; ?></span>
                                            <span class="time"><?php echo $visita->numero_visita; ?></span>
                                        </div>
                                        <div class="timeline-icon"> <?php if ($visita->aprobado == 0) {
                                                                        echo '<i class="icon-remove"></i>';
                                                                    } else {
                                                                        echo '<i class="icon-ok"></i>';
                                                                    } ?> </i></div>
                                        <div class="timeline-body">
                                            <h4>Inspectores Actuantes:</h4>
                                            <?php if ($model->estado != 4 && $model->estado != 5) { ?>
                                                <a href="<?php echo Yii::$app->createUrl('inspecciones/updateVisitaDAC', array('id' => $model->id, 'v' => $visita->id)) ?>" class="btn default btn-sm gray pull-right"><i class="icon-edit"></i> Editar esta visita
                                                </a>
                                            <?php } ?>
                                            <div class="timeline-content">

                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <ul>
                                                            <?php foreach ($visita->inspectoresActuantes as $inspector) { ?>
                                                                <li style="list-style: none !important;">
                                                                    <?php if ($inspector->inspector0->es_inspector == 1) { ?>
                                                                        <span class=""><i class="icon-user"></i></span>
                                                                    <?php } else { ?>
                                                                        <span class=""><i class="icon-eye-open"></i></span>
                                                                    <?php } ?>
                                                                    <?php echo $inspector->inspector0->nombre_apellidos; ?> [ <?php echo $inspector->inspector0->codigo; ?> ]
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <?php if (isset($visita->descripcion) && $visita->descripcion != "") { ?>
                                                            <div id="full-<?php echo $visita->id ?>" class="modal fade" aria-hidden="true" tabindex="-1" style="display: none;">
                                                                <div class="modal-dialog modal-full">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                            <h4 class="modal-title" style="color: #444">Resultado de la Inspecci&oacute;n</h4>
                                                                        </div>
                                                                        <div class="modal-body" style="color: #444">
                                                                            <?php echo $visita->descripcion ?>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn default" data-dismiss="modal">Cerrar</button>
                                                                        </div>
                                                                    </div>
                                                                    <!-- /.modal-content -->
                                                                </div>
                                                            </div>
                                                        <?php  }  ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                            <div class="timeline-footer">
                                                <button type="button" href="#full-<?php echo $visita->id ?>" data-original-title="Resultado de esta visita" data-placement="left" data-toggle="modal" class="btn btn-default btn-block tooltips">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                    Leer el Resultado de la Inspecci&oacute;n
                                                </button>
                                            </div>
                                    </li>

                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <!-- END PAGE TIMELINE-->
                    <div class="clearfix"></div>
                    <hr>
                    <div class="col-xs-12">
                        <?php if ($ultimaVisita->aprobado == 0) { ?>
                            <a href="<?php echo Yii::$app->createUrl('inspecciones/createVisitaDac', array('id' => $model->id)) ?>" class="btn default btn-sm green"><i class="icon-plus"></i> Registrar nueva visita
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- END card card-->
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
</div>

<!-- END EXAMPLE TABLE card-->
<?php $this->beginClip('CSS_INCLUDES') ?>
<link rel="stylesheet" href="<?php echo Yii::$app->theme->baseUrl ?>//assets/css/pages/profile.css" />
<link rel="stylesheet" href="<?php echo Yii::$app->theme->baseUrl ?>/assets/css/custom.css" />
<link rel="stylesheet" href="<?php echo Yii::$app->theme->baseUrl ?>/assets/css/pages/timeline.css" />
<?php $this->endClip() ?>


<?php $this->beginClip('JS_INCLUDES') ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo Yii::$app->theme->baseUrl ?>/assets/plugins/jquery-print-page/jquery.printPage.js"></script>
<script src="<?php echo Yii::$app->theme->baseUrl ?>/assets/scripts/custom.js"></script>

<?php $this->endClip() ?>

<?php $this->beginClip('JS_READY') ?>
Custom.InitPrint();
<?php $this->endClip() ?>