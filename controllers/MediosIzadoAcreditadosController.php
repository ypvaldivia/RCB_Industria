<?php

namespace app\controllers;

use app\components\CDbCriteria;
use app\components\Traceador;
use app\forms\BuscaObjetoForm;
use app\models\AprobacionXFuncionarios;
use app\models\GaleriaMediosIzado;
use app\models\Inspecciones;
use app\models\InspeccionesXMediosIzado;
use app\models\MediosIzadoAcreditados;
use app\models\ShortcutHomologacionesMediosIzado;
use app\models\VisitasInspecciones;
use Exception;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;

class MediosIzadoAcreditadosController extends Controller
{
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'objetoInspeccion';
        Yii::$app->session['submenu'] = 'mediosIzado';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'create', 'update', 'admin', 'delete', 'batchDelete', 'searchItems', 'historical'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_mediosizadoacreditados_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $form = new BuscaObjetoForm();
        if (isset($_POST['FormBuscaObjeto'])) {
            $form->setMyAttributes($_POST['FormBuscaObjeto']);
            $criteria = $form->execSearchModelProductos();
            $count = MediosIzadoAcreditados::findAll($criteria->condition, $criteria->params);
            $pages = new Pagination(Count($count));
            $pages->pageSize = 50;

            $data = MediosIzadoAcreditados::findAll($criteria->condition, $criteria->params);
        } else {
            $criteria = new CDbCriteria();
            $criteria->order = 'nombre ASC';
            $count = MediosIzadoAcreditados::findAll($criteria->condition, $criteria->params);
            $pages = new Pagination(Count($count));
            $pages->pageSize = 30;

            $data = MediosIzadoAcreditados::findAll($criteria->condition, $criteria->params);
        }
        $model = MediosIzadoAcreditados::find();
        $countQuery = clone $model;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 20;
        $data = $model->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('index', ['data' => $data, 'pages' => $pages, 'model' => $form]);
    }

    public function actionHistorical($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_mediosizadoacreditados_historical')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }

        //ToDo: revisar que el no de inspeccion coincida
        $data = $this->loadModel($id);
        $certificaciones = ShortcutHomologacionesMediosIzado::findAll('medio_izaje = :p', array(':p' => $id));
        return $this->render('historico', array('data' => $data, 'certificaciones' => $certificaciones));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_mediosizadoacreditados_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new MediosIzadoAcreditados();
        if (isset($_POST['MediosIzadoAcreditados'])) {
            $model->setAttributes($_POST['MediosIzadoAcreditados']);

            if (isset($_POST['MediosIzadoAcreditados']['es_producto'])) {
                $model->es_producto = $_POST['MediosIzadoAcreditados']['es_producto'];
            }
            if (isset($_POST['MediosIzadoAcreditados']['tipo0'])) {
                $model->tipo0 = $_POST['MediosIzadoAcreditados']['tipo0'];
            }
            if (isset($_POST['MediosIzadoAcreditados']['propietario'])) {
                $model->propietario0 = $_POST['MediosIzadoAcreditados']['propietario'];
            }
            if (isset($_POST['MediosIzadoAcreditados']['provincia0'])) {
                $model->provincia0 = $_POST['MediosIzadoAcreditados']['provincia0'];
            }
            if (isset($_POST['MediosIzadoAcreditados']['categoria0'])) {
                $model->categoria0 = $_POST['MediosIzadoAcreditados']['categoria0'];
            }

            try {
                if ($model->save()) {
                    Traceador::crearTraza('Creacion', 'Medio de izado acreditado: ' . $model->nombre);
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['MediosIzadoAcreditados'])) {
            $model->attributes = $_GET['MediosIzadoAcreditados'];
        }

        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_mediosizadoacreditados_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);

        if (isset($_POST['MediosIzadoAcreditados'])) {
            $model->setAttributes($_POST['MediosIzadoAcreditados']);
            $model->tipo0 = $_POST['MediosIzadoAcreditados']['tipo0'];
            $model->propietario0 = $_POST['MediosIzadoAcreditados']['propietario'];
            $model->provincia0 = $_POST['MediosIzadoAcreditados']['provincia0'];
            $model->categoria0 = $_POST['MediosIzadoAcreditados']['categoria0'];
            try {
                if ($model->save()) {
                    Traceador::crearTraza('Edicion', 'Medio de izado acreditado: ' . $model->nombre);
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        }

        return $this->render('update', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = MediosIzadoAcreditados::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'medios-izado-acreditados-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_mediosizadoacreditados_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $path_picture = realpath(Yii::getAlias('@web') . '/uploads/images/medios_izado') . '/';
        $successCount = 0;
        $band = false;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $model = $this->loadModel($id);
                    try {
                        $tmpGallery = GaleriaMediosIzado::findAll('medio_izaje = :mi', array(':mi' => $model->id));
                        if ($tmpGallery != null) {
                            foreach ($tmpGallery as $foto) {
                                if ($foto->imagen != null) {
                                    unlink($path_picture . $foto->imagen);
                                }
                                $foto->delete();
                            }
                        }
                        $tmpInsp = InspeccionesXMediosIzado::findAll('medio_izado = :mi', array(':mi' => $model->id));
                        if ($tmpInsp != null) {
                            foreach ($tmpInsp as $inspeccion) {
                                $tmpCertificados = ShortcutHomologacionesMediosIzado::findAll('inspeccion = :insp', array(':insp' => $inspeccion->id));
                                if ($tmpCertificados != null) {
                                    foreach ($tmpCertificados as $certificado) {
                                        $certificado->delete();
                                    }
                                }

                                /*
                                 * Antes de borrar el elemento de Inspeccion por MI debe comprobarse que no es el último de la
                                 * inspección padre y si lo es deben borrarse tanto la inspección como los elementos asociados
                                 * de visitas y aprobación por niveles
                                 */

                                $father = $inspeccion->inspeccion0->id;
                                $counter = InspeccionesXMediosIzado::findAll('inspeccion = :f AND medio_izado != :mi', array(':f' => $father, ':mi' => $model->id))->count();
                                if ($counter == 0) {
                                    $visitasAsoc = VisitasInspecciones::findAll('inspeccion = :f', array(':f' => $father));
                                    if ($visitasAsoc != null) {
                                        foreach ($visitasAsoc as $v) {
                                            $v->delete();
                                        }
                                    }
                                    $aprobaciones = AprobacionXFuncionarios::findAll('inspeccion = :f', array(':f' => $father));
                                    if ($aprobaciones != null) {
                                        foreach ($aprobaciones as $a) {
                                            $a->delete();
                                        }
                                    }
                                }
                                $inspeccion->delete();
                                Inspecciones::delete($father);
                            }
                        }
                        $model->delete();
                        Traceador::crearTraza(
                            'Eliminacion',
                            'Productos y Medios de Izado Terrestres: ' . $model->nombre
                        );
                        ++$successCount;
                    } catch (Exception $e) {
                        $band = true;
                        Yii::$app->session->setFlash('error', $e->getMessage());
                    }
                }
                if (!$band) {
                    Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' elementos exitosamente');
                }
            }
        } else {
            throw new HttpException(400, 'Petición no válida');
        }
    }

    public function actionSearchItems()
    {
        $search = $_GET['q'];
        $items = MediosIzadoAcreditados::findAll('nombre LIKE :n', array(':n' => "%$search%"));
        if ($items != null) {
            foreach ($items as $key => $value) {
                $data[] = array('id' => $value['id'], 'text' => $value['nombre']);
            }
        } else {
            $data[] = array('id' => '0', 'text' => 'El elemento no existe');
        }

        echo json_encode($data);
    }

    public function actions()
    {
        return array(
            'toggle' => array(
                'class' => 'bootstrap.actions.TbToggleAction',
                'modelName' => 'MediosIzadoAcreditados',
            ),
        );
    }
}
