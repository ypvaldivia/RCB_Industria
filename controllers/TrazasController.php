<?php

namespace app\controllers;

use app\components\CDbCriteria;
use app\components\Traceador;
use app\forms\BuscaObjetoForm;
use app\models\Trazas;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;

class TrazasController extends Controller
{
    //public $layout='main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'configuracion';
        Yii::$app->session['submenu'] = 'seguridad';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_trazas_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $searcher = new BuscaObjetoForm();
        if (isset($_POST['BuscaObjetoForm'])) {
            $searcher->setMyAttributes($_POST['BuscaObjetoForm']);
            $criteria = $searcher->execSearchTrazas();
            $count = Trazas::findAll($criteria->condition, $criteria->params);
            $pages = new Pagination(Count($count));
            $pages->pageSize = 100;

            $data = Trazas::findAll($criteria->condition, $criteria->params);
        } else {
            $criteria = new CDbCriteria();
            $criteria->order = 'fecha DESC';
            $count = Trazas::findAll($criteria->condition, $criteria->params);
            $pages = new Pagination(Count($count));
            $pages->pageSize = 30;

            $data = Trazas::findAll($criteria->condition, $criteria->params);
        }
        Traceador::crearTraza('Acceso', 'Sistema de trazas');
        $model = Trazas::find();
        $countQuery = clone $model;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 20;
        $data = $model->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('index', array('data' => $data, 'pages' => $pages, 'searcher' => $searcher));
    }

    public function loadModel($id)
    {
        $model = Trazas::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'La página solicitada no existe.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'trazas-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }
}
