<?php

namespace app\controllers;

use app\components\Traceador;
use app\models\MediosIzadoAcreditados;
use app\models\TiposMediosIzado;
use Exception;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;

class TiposMediosIzadoController extends Controller
{
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'objetoInspeccion';
        Yii::$app->session['submenu'] = 'mediosIzado';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'create', 'update', 'batchDelete', 'toggle'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_tiposmediosizado_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $data = TiposMediosIzado::find()->all();
        return $this->render('index', array('data' => $data));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_tiposmediosizado_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new TiposMediosIzado();
        if (isset($_POST['TiposMediosIzado'])) {
            $model->setAttributes($_POST['TiposMediosIzado']);

            if (isset($_POST['TiposMediosIzado']['tipo_certificado'])) {
                $model->tipo_certificado = $_POST['TiposMediosIzado']['tipo_certificado'];
            }

            try {
                if ($model->save()) {
                    Traceador::crearTraza('Creacion', 'Tipo de medios de izado: ' . $_POST['TiposMediosIzado']['tipo']);
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['TiposMediosIzado'])) {
            $model->attributes = $_GET['TiposMediosIzado'];
        }

        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_tiposmediosizado_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);

        if (isset($_POST['TiposMediosIzado'])) {
            $model->setAttributes($_POST['TiposMediosIzado']);
            if (isset($_POST['TiposMediosIzado']['tipo_certificado'])) {
                $model->tipo_certificado = $_POST['TiposMediosIzado']['tipo_certificado'];
            }
            try {
                if ($model->save()) {
                    Traceador::crearTraza('Edicion', 'Tipo de medios de izado: ' . $_POST['TiposMediosIzado']['tipo']);
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        }

        return $this->render('update', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = TiposMediosIzado::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'tipos-medios-izado-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_tiposmediosizado_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $model = $this->loadModel($id);
                    $tmpMI = MediosIzadoAcreditados::findAll('tipo = :t', array(':t' => $model->id));
                    if ($tmpMI != null) {
                        Yii::$app->session->setFlash('error', 'Existen ' . count($tmpMI) . ' MITs vinculados con los elementos seleccionados que debe modificar antes de eliminarlos');
                        break;
                    } else {
                        $model->delete();
                        Traceador::crearTraza('Eliminacion', 'Tipo de Medio de Izado: ' . $model->tipo);
                        ++$successCount;
                    }
                }
                if ($successCount > 0) {
                    Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
                }
            }
        } else {
            throw new HttpException(400, 'Petición no válida');
        }
    }

    public function actions()
    {
        return array(
            'toggle' => array(
                'class' => 'bootstrap.actions.TbToggleAction',
                'modelName' => 'TiposMediosIzado',
            ),
        );
    }
}
