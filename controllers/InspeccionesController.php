<?php

namespace app\controllers;

use app\components\Cartero;
use app\components\CDbCriteria;
use app\components\Traceador;
use app\forms\BuscarInspeccionForm;
use app\forms\InspeccionDacEditarForm;
use app\forms\InspeccionDacForm;
use app\forms\InspeccionLabServEditarForm;
use app\forms\InspeccionLabServForm;
use app\forms\InspeccionMITEditarForm;
use app\forms\InspeccionMITForm;
use app\forms\InspeccionPersonaEditarForm;
use app\forms\InspeccionPersonaForm;
use app\forms\InspeccionProductoEditarForm;
use app\forms\InspeccionProductoForm;
use app\forms\VisitaInspeccionForm;
use app\forms\VisitaInspeccionPersonaForm;
use app\models\AprobacionXFuncionarios;
use app\models\CategoriasMediosIzado;
use app\models\DacsAcreditados;
use app\models\Entidades;
use app\models\Funcionarios;
use app\models\Inspecciones;
use app\models\InspeccionesXDacs;
use app\models\InspeccionesXMediosIzado;
use app\models\InspeccionesXPersonas;
use app\models\InspeccionesXProductos;
use app\models\InspeccionesXServiciosProductos;
use app\models\InspectoresActuantes;
use app\models\MediosIzadoAcreditados;
use app\models\Oficinas;
use app\models\PersonasAcreditadas;
use app\models\ProductosAcreditados;
use app\models\Provincias;
use app\models\ServiciosLaboratoriosAcreditados;
use app\models\ShortcutHomologacionesDacs;
use app\models\ShortcutHomologacionesMediosIzado;
use app\models\ShortcutHomologacionesPersonas;
use app\models\ShortcutHomologacionesProductos;
use app\models\ShortcutHomologacionesServiciosProductos;
use app\models\TiposDacs;
use app\models\TiposMediosIzado;
use app\models\VisitasInspecciones;
use Exception;
use stdClass;
use webvimark\modules\UserManagement\models\User;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

class InspeccionesController extends Controller
{
    public $layout = 'main';
    public $pageSize = 1;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'inspecciones';
        Yii::$app->session['submenu'] = '';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array(
                    'index', 'indexPersonas', 'indexLabServ', 'indexProductos', 'indexDacs', 'indexMITs',
                    'create', 'update', 'delete', 'aprobaciones', 'certificados', 'imprimirDetalles', 'PDFDetalles',
                    'createInspeccionPersona', 'createInspeccionLabServ', 'createInspeccionProducto', 'createInspeccionDac', 'createInspeccionMIT',
                    'detallesPersonas', 'detallesLabServ', 'detallesProductos', 'detallesDac', 'detallesMit',
                    'createVisitaPersona', 'createVisitaLabServ', 'createVisitaProducto', 'createVisitaDac', 'createVisitaMIT',
                    'updateVisitaPersonas', 'updateVisitaLabServ', 'updateVisitaProductos', 'updateVisitaDac', 'updateVisitaMIT',
                    'editarLabServ', 'editarPersonas', 'editarProducto', 'editarDAC', 'editarMIT',
                    'createPersonaAcreditada', 'createProductoAcreditado', 'createDacAcreditado', 'createMITAcreditado', 'createServicioAcreditado',
                    'cerrarInspeccionPersona', 'cerrarInspeccionLabServ', 'cerrarInspeccionProducto',
                    'cerrarInspeccionDAC', 'cerrarInspeccionMIT', 'eliminarInforme',
                ),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($id, $formType)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        switch ($formType) {
            case 'inspeccionPersona':
                $this->actionDetallesPersonas($id);
                break;
            case 'inspeccionLabServ':
                $this->actionDetallesLabServ($id);
                break;
            case 'inspeccionProducto':
                $this->actionDetallesProductos($id);
                break;
            case 'inspeccionDac':
                $this->actionDetallesDac($id);
                break;
            case 'inspeccionMIT':
                $this->actionDetallesMit($id);
                break;
        }
    }

    /*
     * Carga los datos de las inspecciones realizadas a personas.
     */
    public function actionIndexPersonas()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_indexpersonas')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $user = Funcionarios::findOne('usuario = :u', array(':u' => Yii::$app->user->id));
        $oficina = null;
        $oficinaCentral = Oficinas::findOne('oficina_superior = id');
        if ($user != null) {
            $oficina = $user->oficina0;
        }
        $formType = 'inspeccionPersona';
        $form = new BuscarInspeccionForm();
        $criteria = new CDbCriteria();
        $data = null;

        $criteria->alias = 't';
        $criteria->distinct = true;
        $criteria->join = 'JOIN inspecciones_x_personas ON inspecciones_x_personas.inspeccion = t.id';

        if ($oficina == null || $oficina->id == $oficina->oficinaSuperior->id) {
            //$criteria->condition = 't.id = ip.inspeccion';
        } else {
            if ($oficina->oficinaSuperior->id != $oficinaCentral->id) {
                $criteria->condition = 't.oficina = :o';
                $criteria->params = array(':o' => $oficina->oficinaSuperior->id);
            } else {
                $criteria->condition = 't.oficina = :o';
                $criteria->params = array(':o' => $oficina->id);
            }
        }

        $criteria->order = 't.id desc';
        $criteria->group = 't.id';

        $count = count(Inspecciones::findAll($criteria->condition, $criteria->params));
        $pages = new Pagination($count);
        $pages->pageSize = 6;



        if (isset($_POST['BuscarInspeccionForm'])) {
            $form->setMyAttributes($_POST['BuscarInspeccionForm']);
            $criteria = $form->buscarObjetoPersona($_POST['BuscarInspeccionForm']);
            $data = Inspecciones::findAll($criteria->condition, $criteria->params);
            $count = count(Inspecciones::findAll($criteria->condition, $criteria->params));
            $pages = new Pagination($count);
            $pages->pageSize = 6;
        } else {
            $data = Inspecciones::findAll($criteria->condition, $criteria->params);
        }

        Traceador::crearTraza('Acceso', 'Listado de inspecciones de personas');
        return $this->render('index', array(
            'data' => $data,
            'formType' => $formType,
            'model' => $form,
            'pages' => $pages,
        ));
    }

    /*
     * Carga los datos de las inspeccines realizadas a los laboratorios y servicios.
     */
    public function actionIndexLabServ()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_indexlabserv')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $user = Funcionarios::findOne('usuario = :u', array(':u' => Yii::$app->user->id));
        $oficina = null;
        $oficinaCentral = Oficinas::findOne('oficina_superior = id');
        if ($user != null) {
            $oficina = $user->oficina0;
        }
        $formType = 'inspeccionLabServ';
        $form = new BuscarInspeccionForm();
        $criteria = new CDbCriteria();
        $data = null;

        $criteria->alias = 't';
        $criteria->distinct = true;
        $criteria->join = 'JOIN inspecciones_x_servicios_productos ON inspecciones_x_servicios_productos.inspeccion = t.id';

        if ($oficina == null || $oficina->id == $oficina->oficinaSuperior->id) {
            //$criteria->condition = 't.id = ip.inspeccion';
        } else {
            if ($oficina->oficinaSuperior->id != $oficinaCentral->id) {
                $criteria->condition = 't.oficina = :o';
                $criteria->params = array(':o' => $oficina->oficinaSuperior->id);
            } else {
                $criteria->condition = 't.oficina = :o';
                $criteria->params = array(':o' => $oficina->id);
            }
        }

        $criteria->order = 't.id desc';
        $criteria->group = 't.id';

        $count = count(Inspecciones::findAll($criteria->condition, $criteria->params));
        $pages = new Pagination($count);
        $pages->pageSize = 6;


        if (isset($_POST['BuscarInspeccionForm'])) {
            $form->setMyAttributes($_POST['BuscarInspeccionForm']);
            $criteria = $form->buscarObjetoServicio($_POST['BuscarInspeccionForm']);
            $data = Inspecciones::findAll($criteria->condition, $criteria->params);
            $count = count(Inspecciones::findAll($criteria->condition, $criteria->params));
            $pages = new Pagination($count);
            $pages->pageSize = 6;
        } else {
            $data = Inspecciones::findAll($criteria->condition, $criteria->params);
        }


        Traceador::crearTraza('Acceso', 'Listado de inspecciones de laboratorios y servicios');
        return $this->render('index', array('data' => $data, 'formType' => $formType, 'model' => $form, 'pages' => $pages));
    }

    /*
     * Carga los datos de las inspeccines realizadas a los lproductos.
     */
    public function actionIndexProductos()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_indexproductos')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $user = Funcionarios::findOne('usuario = :u', array(':u' => Yii::$app->user->id));
        $oficina = null;
        $oficinaCentral = Oficinas::findOne('oficina_superior = id');
        if ($user != null) {
            $oficina = $user->oficina0;
        }
        $formType = 'inspeccionProducto';
        $form = new BuscarInspeccionForm();
        $criteria = new CDbCriteria();
        $data = null;

        $criteria->alias = 't';
        $criteria->distinct = true;
        $criteria->join = 'JOIN inspecciones_x_productos ON inspecciones_x_productos.inspeccion = t.id';

        if ($oficina == null || $oficina->id == $oficina->oficinaSuperior->id) {
            //$criteria->condition = 't.id = ip.inspeccion';
        } else {
            if ($oficina->oficinaSuperior->id != $oficinaCentral->id) {
                $criteria->condition = 't.oficina = :o';
                $criteria->params = array(':o' => $oficina->oficinaSuperior->id);
            } else {
                $criteria->condition = 't.oficina = :o';
                $criteria->params = array(':o' => $oficina->id);
            }
        }

        $criteria->order = 't.id desc';
        $criteria->group = 't.id';

        $count = count(Inspecciones::findAll($criteria->condition, $criteria->params));
        $pages = new Pagination($count);
        $pages->pageSize = 6;


        if (isset($_POST['BuscarInspeccionForm'])) {
            $form->setMyAttributes($_POST['BuscarInspeccionForm']);
            $criteria = $form->buscarObjetoProducto($_POST['BuscarInspeccionForm']);
            $data = Inspecciones::findAll($criteria->condition, $criteria->params);
            $count = count(Inspecciones::findAll($criteria->condition, $criteria->params));
            $pages = new Pagination($count);
            $pages->pageSize = 6;
        } else {
            $data = Inspecciones::findAll($criteria->condition, $criteria->params);
        }

        Traceador::crearTraza('Acceso', 'Listado de inspecciones de prodcutos');
        return $this->render('index', array('data' => $data, 'formType' => $formType, 'model' => $form, 'pages' => $pages));
    }

    public function actionIndexDacs()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_indexdacs')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $user = Funcionarios::findOne('usuario = :u', array(':u' => Yii::$app->user->id));
        $oficina = null;
        $oficinaCentral = Oficinas::findOne('oficina_superior = id');
        if ($user != null) {
            $oficina = $user->oficina0;
        }
        $formType = 'inspeccionDac';
        $form = new BuscarInspeccionForm();
        $criteria = new CDbCriteria();
        $data = null;

        $criteria->alias = 't';
        $criteria->distinct = true;
        $criteria->join = 'JOIN inspecciones_x_dacs ON inspecciones_x_dacs.inspeccion = t.id';

        if ($oficina == null || $oficina->id == $oficina->oficinaSuperior->id) {
            //$criteria->condition = 't.id = ip.inspeccion';
        } else {
            if ($oficina->oficinaSuperior->id != $oficinaCentral->id) {
                $criteria->condition = 't.oficina = :o';
                $criteria->params = array(':o' => $oficina->oficinaSuperior->id);
            } else {
                $criteria->condition = 't.oficina = :o';
                $criteria->params = array(':o' => $oficina->id);
            }
        }

        $criteria->order = 't.id desc';
        $criteria->group = 't.id';

        $count = count(Inspecciones::findAll($criteria->condition, $criteria->params));
        $pages = new Pagination($count);
        $pages->pageSize = 6;


        if (isset($_POST['BuscarInspeccionForm'])) {
            $form->setMyAttributes($_POST['BuscarInspeccionForm']);

            $criteria = $form->buscarObjetoDAC($_POST['BuscarInspeccionForm']);
            $data = Inspecciones::findAll($criteria->condition, $criteria->params);
            $count = count(Inspecciones::findAll($criteria->condition, $criteria->params));
            $pages = new Pagination($count);
            $pages->pageSize = 6;
        } else {
            $data = Inspecciones::findAll($criteria->condition, $criteria->params);
        }

        Traceador::crearTraza('Acceso', 'Listado de inspecciones de DACs');
        return $this->render('index', array('data' => $data, 'formType' => $formType, 'model' => $form, 'pages' => $pages));
    }

    public function actionIndexMits()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_indexmits')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $user = Funcionarios::findOne('usuario = :u', array(':u' => Yii::$app->user->id));
        $oficina = null;
        $oficinaCentral = Oficinas::findOne('oficina_superior = id');
        if ($user != null) {
            $oficina = $user->oficina0;
        }
        $formType = 'inspeccionMIT';
        $form = new BuscarInspeccionForm();
        $criteria = new CDbCriteria();
        $data = null;

        $criteria->alias = 't';
        $criteria->distinct = true;
        $criteria->join = 'JOIN inspecciones_x_medios_izado ON inspecciones_x_medios_izado.inspeccion = t.id';

        if ($oficina == null || $oficina->id == $oficina->oficinaSuperior->id) {
            //$criteria->condition = 't.id = ip.inspeccion';
        } else {
            if ($oficina->oficinaSuperior->id != $oficinaCentral->id) {
                $criteria->condition = 't.oficina = :o';
                $criteria->params = array(':o' => $oficina->oficinaSuperior->id);
            } else {
                $criteria->condition = 't.oficina = :o';
                $criteria->params = array(':o' => $oficina->id);
            }
        }

        $criteria->order = 't.id desc';
        $criteria->group = 't.id';

        $count = count(Inspecciones::findAll($criteria->condition, $criteria->params));
        $pages = new Pagination($count);
        $pages->pageSize = 6;


        if (isset($_POST['BuscarInspeccionForm'])) {
            $form->setMyAttributes($_POST['BuscarInspeccionForm']);

            $criteria = $form->buscarObjetoMIT($_POST['BuscarInspeccionForm']);
            $data = Inspecciones::findAll($criteria->condition, $criteria->params);
            $count = count(Inspecciones::findAll($criteria->condition, $criteria->params));
            $pages = new Pagination($count);
            $pages->pageSize = 6;
        } else {
            $data = Inspecciones::findAll($criteria->condition, $criteria->params);
        }
        Traceador::crearTraza('Acceso', 'Listado de inspecciones de MITs');
        return $this->render('index', array('data' => $data, 'formType' => $formType, 'model' => $form, 'pages' => $pages));
    }

    public function actionCerrarInspeccionPersona($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_cerrarinspeccionpersona')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $model->cerrada = 1;
        $formType = 'inspeccionPersona';
        try {
            $model->update();
            Traceador::crearTraza('Informe Inspección', 'Completado el Informe de Inspección de Personal: ' . $model->numero_control);
            Cartero::enviarNotificacionInformeListo(
                'Inspección pendiente de aprobación',
                $formType,
                $model,
                'mail_IPAP'
            );
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage() . ' (' . $e->getCode() . ')');
            $model->addError('id', $e->getMessage());
        }
        return $this->render('detallesInspeccionPersonas', array('model' => $model, 'formType' => $formType));
    }

    public function actionCerrarInspeccionLabServ($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_cerrarinspeccionlabserv')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $model->cerrada = 1;
        $formType = 'inspeccionLabServ';
        try {
            $model->update();
            Traceador::crearTraza('Informe de Inspección', 'Completado el Informe Inspección de servicios: ' . $model->numero_control);
            Cartero::enviarNotificacionInformeListo(
                'Inspección pendiente de aprobación',
                $formType,
                $model,
                'mail_IPAP'
            );
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage() . ' (' . $e->getCode() . ')');
            $model->addError('id', $e->getMessage());
        }
        return $this->render('detallesInspeccionLabServ', array('model' => $model, 'formType' => $formType));
    }

    public function actionCerrarInspeccionProducto($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_cerrarinspeccionproducto')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $model->cerrada = 1;
        $formType = 'inspeccionProducto';
        try {
            $model->update();
            Traceador::crearTraza('Informe de Inspección', 'Completado el Informe Inspección de productos y medios de izado: ' . $model->numero_control);
            Cartero::enviarNotificacionInformeListo(
                'Inspección pendiente de aprobación',
                $formType,
                $model,
                'mail_IPAP'
            );
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage() . ' (' . $e->getCode() . ')');
            $model->addError('id', $e->getMessage());
        }
        return $this->render('detallesInspeccionProductos', array('model' => $model, 'formType' => $formType));
    }

    public function actionCerrarInspeccionDAC($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_cerrarinspecciondac')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $model->cerrada = 1;
        $formType = 'inspeccionDac';
        try {
            $model->update();
            Traceador::crearTraza('Informe de Inspección', 'Completado el Informe Inspección de Dispositivos Auxiliares de Carga: ' . $model->numero_control);
            Cartero::enviarNotificacionInformeListo(
                'Inspección pendiente de aprobación',
                $formType,
                $model,
                'mail_IPAP'
            );
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage() . ' (' . $e->getCode() . ')');
            $model->addError('id', $e->getMessage());
        }
        return $this->render('detallesInspeccionDac', array('model' => $model, 'formType' => $formType));
    }

    public function actionCerrarInspeccionMIT($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_cerrarinspeccionmit')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $model->cerrada = 1;
        $formType = 'inspeccionMIT';
        try {
            $model->update();
            Traceador::crearTraza('Informe de Inspección', 'Completado el Informe Inspección de Medios de Izado: ' . $model->numero_control);
            Cartero::enviarNotificacionInformeListo(
                'Inspección pendiente de aprobación',
                $formType,
                $model,
                'mail_IPAP'
            );
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage() . ' (' . $e->getCode() . ')');
            $model->addError('id', $e->getMessage());
        }
        return $this->render('detallesInspeccionMit', array('model' => $model, 'formType' => $formType));
    }

    /*
     * Detalles de una inspeccion realizada a personas
     */
    public function actionDetallesPersonas($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_detallespersonas')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $formType = 'inspeccionPersona';
        Traceador::crearTraza('Acceso', 'Detalles de la inspeccion personas: ' . $model->numero_control);
        return $this->render('detallesInspeccionPersonas', array('model' => $model, 'formType' => $formType));
    }
    /*
     * Detalles de una inspeccion realizada a laboratorios y servicios
     */
    public function actionDetallesLabServ($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_detalleslabserv')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $formType = 'inspeccionLabServ';
        Traceador::crearTraza('Acceso', 'Detalles de la inspeccion laboratorios y servicios: ' . $model->numero_control);
        return $this->render('detallesInspeccionLabServ', array('model' => $model, 'formType' => $formType));
    }
    /*
     * Detalles de una inspeccion realizada a productos
     */
    public function actionDetallesProductos($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_detallesproductos')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $formType = 'inspeccionProducto';
        Traceador::crearTraza('Acceso', 'Detalles de la inspeccion productos: ' . $model->numero_control);
        return $this->render('detallesInspeccionProductos', array('model' => $model, 'id' => $id, 'formType' => $formType));
    }

    public function actionDetallesDac($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_detallesdacs')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $formType = 'inspeccionDac';
        $objetos = InspeccionesXDacs::findAll('inspeccion = :i', array(':i' => $id));
        Traceador::crearTraza('Acceso', 'Detalles de la inspeccion productos: ' . $model->numero_control);
        return $this->render('detallesInspeccionDac', array('model' => $model, 'id' => $id, 'formType' => $formType, 'objetos' => $objetos));
    }

    public function actionDetallesMit($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_detallesmit')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $formType = 'inspeccionMIT';
        Traceador::crearTraza('Acceso', 'Detalles de la inspeccion productos: ' . $model->numero_control);
        return $this->render('detallesInspeccionMit', array('model' => $model, 'id' => $id, 'formType' => $formType));
    }

    public function actionPDFDetalles($id, $formType)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_pdfdetalles')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'blank';
        $model = $this->loadModel($id);
        ob_start();
        $data = $this->renderPartial('pdfDetalleInspeccion', array('model' => $model, 'formType' => $formType), true);
        ob_get_clean();

        $html2pdf = Yii::$app->ePdf->HTML2PDF('P', 'A4', 'es', true, 'UTF-8', 1);
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->WriteHTML($data, isset($_GET['vuehtml']));
        //$html2pdf->createIndex('Tabla de Contenido', 30, 12, false, false, 2);
        $html2pdf->Output('Informe Inspeccion_' . $model->homologacion0->titulo_anuario . '.pdf', 'D');                // To download PDF
        Traceador::crearTraza('Generacion', 'Generado Informe de Inspeccion: ' . $model->numero_control);
    }

    public function actionImprimirDetalles($id, $formType)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_imprimirdetalles')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'print';
        $model = $this->loadModel($id);
        Traceador::crearTraza('Impresion', 'Impreso detalle de inspeccion: ' . $model->numero_control);
        return $this->render('imprimirDetalleInspeccion', array('model' => $model, 'formType' => $formType));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new Inspecciones();
        if (isset($_POST['Inspecciones'])) {
            $model->setAttributes($_POST['Inspecciones']);

            if (isset($_POST['Inspecciones']['oficina0'])) {
                $model->oficina0 = $_POST['Inspecciones']['oficina0'];
            }
            if (isset($_POST['Inspecciones']['homologacion0'])) {
                $model->homologacion0 = $_POST['Inspecciones']['homologacion0'];
            }
            if (isset($_POST['Inspecciones']['tipoHomologacion'])) {
                $model->tipoHomologacion = $_POST['Inspecciones']['tipoHomologacion'];
            }
            if (isset($_POST['Inspecciones']['estado0'])) {
                $model->estado0 = $_POST['Inspecciones']['estado0'];
            }
            if (isset($_POST['Inspecciones']['nivel0'])) {
                $model->nivel0 = $_POST['Inspecciones']['nivel0'];
            }

            try {
                if ($model->save()) {
                    Traceador::crearTraza(
                        'Creacion',
                        'Creada la inspeccion : ' . $model->numero_control
                    );
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['Inspecciones'])) {
            $model->attributes = $_GET['Inspecciones'];
        }
        return $this->render('create', array('model' => $model));
    }

    /*
     *
     */
    public function actionEliminarInforme($id, $type)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_eliminarinforme')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }

        $model = Inspecciones::findOne($id);
        $visitas = VisitasInspecciones::findAll('inspeccion = :di', array(':di' => $id));
        if ($visitas != null) {
            foreach ($visitas as $v) {
                InspectoresActuantes::deleteAll('visita = :dv', array(':dv' => $v->id));
                $v->delete();
            }
        }

        Traceador::crearTraza(
            'Eliminación',
            'Eliminado el Informe de Inspección  ' . $model->numero_control
        );

        switch ($type) {
            case 'persona':
                $objetos = InspeccionesXPersonas::findAll('inspeccion = :id', array(':id' => $id));
                if ($objetos != null) {
                    foreach ($objetos as $o) {
                        if ($o->cprovisional != null) {
                            $basePath = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/personas') . '/';
                            if (file_exists($basePath . $o->cprovisional)) {
                                unlink($basePath . $o->cprovisional);
                            }
                        }
                        if ($o->cdefinitivo != null) {
                            $basePath = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/personas') . '/';
                            if (file_exists($basePath . $o->cdefinitivo)) {
                                unlink($basePath . $o->cdefinitivo);
                            }
                        }
                        $o->delete();
                    }
                }
                $model->delete();
                Yii::$app->session->setFlash('success', 'Fue eliminado el informe de inspección exitosamente');
                $this->actionIndexPersonas();
                break;
            case 'servicio':
                $objetos = InspeccionesXServiciosProductos::findAll('inspeccion = :id', array(':id' => $id));
                if ($objetos != null) {
                    foreach ($objetos as $o) {
                        if ($o->cprovisional != null) {
                            $basePath = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/servicios') . '/';
                            if (file_exists($basePath . $o->cprovisional)) {
                                unlink($basePath . $o->cprovisional);
                            }
                        }
                        if ($o->cdefinitivo != null) {
                            $basePath = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/servicios') . '/';
                            if (file_exists($basePath . $o->cdefinitivo)) {
                                unlink($basePath . $o->cdefinitivo);
                            }
                        }
                        $o->delete();
                    }
                }
                $model->delete();
                Yii::$app->session->setFlash('success', 'Fue eliminado el informe de inspección exitosamente');
                $this->actionIndexLabServ();
                break;
            case 'producto':
                $objetos = InspeccionesXProductos::findAll('inspeccion = :id', array(':id' => $id));
                if ($objetos != null) {
                    foreach ($objetos as $o) {
                        if ($o->cprovisional != null) {
                            $basePath = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/productos') . '/';
                            if (file_exists($basePath . $o->cprovisional)) {
                                unlink($basePath . $o->cprovisional);
                            }
                        }
                        if ($o->cdefinitivo != null) {
                            $basePath = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/productos') . '/';
                            if (file_exists($basePath . $o->cdefinitivo)) {
                                unlink($basePath . $o->cdefinitivo);
                            }
                        }
                        $o->delete();
                    }
                }
                $model->delete();
                Yii::$app->session->setFlash('success', 'Fue eliminado el informe de inspección exitosamente');
                $this->actionIndexProductos();
                break;
            case 'dac':
                $objetos = InspeccionesXDacs::findAll('inspeccion = :id', array(':id' => $id));
                if ($objetos != null) {
                    foreach ($objetos as $o) {
                        if ($o->cprovisional != null) {
                            $basePath = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/dacs') . '/';
                            if (file_exists($basePath . $o->cprovisional)) {
                                unlink($basePath . $o->cprovisional);
                            }
                        }
                        if ($o->cdefinitivo != null) {
                            $basePath = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/dacs') . '/';
                            if (file_exists($basePath . $o->cdefinitivo)) {
                                unlink($basePath . $o->cdefinitivo);
                            }
                        }
                        $o->delete();
                    }
                }
                $model->delete();
                Yii::$app->session->setFlash('success', 'Fue eliminado el informe de inspección exitosamente');
                $this->actionIndexDacs();
                break;
            case 'mit':
                $objetos = InspeccionesXMediosIzado::findAll('inspeccion = :id', array(':id' => $id));
                if ($objetos != null) {
                    foreach ($objetos as $o) {
                        if ($o->cprovisional != null) {
                            $basePath = realpath(Yii::getAlias('@web') . '/uploads/certificados/provisionales/mits') . '/';
                            if (file_exists($basePath . $o->cprovisional)) {
                                unlink($basePath . $o->cprovisional);
                            }
                        }
                        if ($o->cdefinitivo != null) {
                            $basePath = realpath(Yii::getAlias('@web') . '/uploads/certificados/definitivos/mits') . '/';
                            if (file_exists($basePath . $o->cdefinitivo)) {
                                unlink($basePath . $o->cdefinitivo);
                            }
                        }
                        if ($o->rig != null) {
                            $basePath = realpath(Yii::getAlias('@web') . '/uploads/files') . '/';
                            if (file_exists($basePath . $o->rig)) {
                                unlink($basePath . $o->rig);
                            }
                        }
                        $o->delete();
                    }
                }
                $model->delete();
                Yii::$app->session->setFlash('success', 'Fue eliminado el informe de inspección exitosamente');
                $this->actionIndexMITs();
                break;
        }
    }
    /*
     * Inserta la nueva visita de inspeccion a personas
     */
    public function actionCreateVisitaPersona($id)
    {
        if (!User::hasPermission('action_inspecciones_createvisitapersona')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionPersona';
        $form = new VisitaInspeccionPersonaForm();
        $inspeccion = Inspecciones::findOne($id);

        if (isset($_POST['VisitaInspeccionPersonaForm'])) {
            $form->setMyAttributes($_POST['VisitaInspeccionPersonaForm']);
            $form->homologacion = $inspeccion->homologacion;
            $visitCount = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $inspeccion->id))->count();

            $form->numero_visita = $visitCount - 1;

            if ($inspeccion->cerrada == 1) {
                $inspeccion->cerrada = 0;
            }
            $inspeccion->estado = 1;
            $inspeccion->update();

            if ($visitCount >= 1) {
                //Buscar las aprobaciones anteriores e invalidarlas
                $aprobacionesAnteriores = AprobacionXFuncionarios::findAll('inspeccion = :i', array(':i' => $inspeccion->id));
                if ($aprobacionesAnteriores != null) {
                    foreach ($aprobacionesAnteriores as $a) {
                        $a->valida = 0;
                        $a->update();
                    }
                }
            }

            if ($form->validate()) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    $this->insertVisitaInspeccionPersona($form, $inspeccion);
                    foreach ($inspeccion->inspeccionesXPersonases as $mit) {
                        $ma = 'obj_aprobado' . $mit->persona0->id;
                        if (isset($_POST['VisitaInspeccionForm'][$ma])) {
                            $mit->aprobada = 1;
                        } else {
                            $mit->aprobada = 0;
                        }
                        $mit->update();
                    }
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha creado una visita exitosamente.');
                    Traceador::crearTraza(
                        'Creacion',
                        'Creada visita  ' . $form->numero_visita . ' de la inspeccion de personas: ' . $inspeccion->numero_control
                    );
                    return $this->redirect(array('detallesPersonas', 'id' => $id));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                    $form->addError('id', $e->getMessage());
                    return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
                }
            } else {
                return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
            }
        }
        return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
    }
    /*
     * Inserta la nueva visita de inspeccion a laboratorios y servicios
     */
    public function actionCreateVisitaLabServ($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createvisitalabserv')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionLabServ';
        $form = new VisitaInspeccionForm();

        $inspeccion = Inspecciones::findOne($id);

        if (isset($_POST['VisitaInspeccionForm'])) {
            $form->setMyAttributes($_POST['VisitaInspeccionForm']);
            $form->homologacion = $inspeccion->homologacion;
            $visitCount = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $inspeccion->id))->count();
            $form->numero_visita = $visitCount - 1;

            if ($form->validate()) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    $this->insertVisitaInspeccion($form, $inspeccion);
                    foreach ($inspeccion->inspeccionesXServiciosProductoses as $mit) {
                        $ma = 'obj_aprobado' . $mit->servcios0->id;
                        if (isset($_POST['VisitaInspeccionForm'][$ma])) {
                            $mit->aprobado = 1;
                        } else {
                            $mit->aprobado = 0;
                        }
                        $mit->update();
                    }

                    $inspeccion->estado = 1;
                    $inspeccion->cerrada = 0;
                    $inspeccion->update();

                    if ($visitCount >= 1) {
                        //Buscar las aprobaciones anteriores e invalidarlas
                        $aprobacionesAnteriores = AprobacionXFuncionarios::findAll('inspeccion = :i', array(':i' => $inspeccion->id));
                        if ($aprobacionesAnteriores != null) {
                            foreach ($aprobacionesAnteriores as $a) {
                                $a->valida = 0;
                                $a->update();
                            }
                        }
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha creado una visita exitosamente.');
                    Traceador::crearTraza(
                        'Creacion',
                        'Creada visita  ' . $form->numero_visita . ' de la inspeccion de Servicios: ' . $inspeccion->numero_control
                    );
                    return $this->redirect(array('detallesLabServ', 'id' => $id));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::$app->session->setFlash('error', $e->getMessage());
                    $form->addError('id', $e->getMessage());
                    return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
                }
            } else {
                return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
            }
        } else {
            return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
        }
    }
    /*
     * Inserta la nueva visita de inspeccion a laboratorios y servicios
     */
    public function actionCreateVisitaProducto($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createvisitaproducto')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionProducto';
        $form = new VisitaInspeccionForm();

        $inspeccion = Inspecciones::findOne($id);

        if (isset($_POST['VisitaInspeccionForm'])) {
            $form->setMyAttributes($_POST['VisitaInspeccionForm']);
            $form->homologacion = $inspeccion->homologacion;
            $visitCount = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $inspeccion->id))->count();
            $form->numero_visita = $visitCount - 1;

            if ($form->validate()) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    $this->insertVisitaInspeccion($form, $inspeccion);
                    foreach ($inspeccion->inspeccionesXProductoses as $mit) {
                        $ma = 'obj_aprobado' . $mit->producto0->id;
                        if (isset($_POST['VisitaInspeccionForm'][$ma])) {
                            $mit->aprobado = 1;
                        } else {
                            $mit->aprobado = 0;
                        }
                        $mit->update();
                    }

                    $inspeccion->estado = 1;
                    $inspeccion->cerrada = 0;
                    $inspeccion->update();

                    if ($visitCount >= 1) {
                        //Buscar las aprobaciones anteriores e invalidarlas
                        $aprobacionesAnteriores = AprobacionXFuncionarios::findAll('inspeccion = :i', array(':i' => $inspeccion->id));
                        if ($aprobacionesAnteriores != null) {
                            foreach ($aprobacionesAnteriores as $a) {
                                $a->valida = 0;
                                $a->update();
                            }
                        }
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha creado una visita exitosamente.');
                    Traceador::crearTraza(
                        'Creacion',
                        'Creada visita  ' . $form->numero_visita . ' de la inspeccion de productos: ' . $inspeccion->numero_control
                    );
                    return $this->redirect(array('detallesProductos', 'id' => $id));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                    $form->addError('id', $e->getMessage());
                    return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
                }
            } else {
                return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
            }
        } else {
            return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
        }
    }

    public function actionCreateVisitaDac($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createvisitadac')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionDac';
        $form = new VisitaInspeccionForm();

        $inspeccion = Inspecciones::findOne($id);

        if (isset($_POST['VisitaInspeccionForm'])) {
            $form->setMyAttributes($_POST['VisitaInspeccionForm']);
            $form->homologacion = $inspeccion->homologacion;
            $visitCount = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $inspeccion->id))->count();
            $form->numero_visita = $visitCount - 1;

            if ($form->validate()) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    $this->insertVisitaInspeccion($form, $inspeccion);
                    foreach ($inspeccion->inspeccionesXDacses as $mit) {
                        $ma = 'obj_aprobado' . $mit->dac0->id;
                        if (isset($_POST['VisitaInspeccionForm'][$ma])) {
                            $mit->aprobado = 1;
                        } else {
                            $mit->aprobado = 0;
                        }
                        $mit->update();
                    }
                    $inspeccion->estado = 1;
                    $inspeccion->cerrada = 0;
                    $inspeccion->update();

                    if ($visitCount >= 1) {
                        //Buscar las aprobaciones anteriores e invalidarlas
                        $aprobacionesAnteriores = AprobacionXFuncionarios::findAll('inspeccion = :i', array(':i' => $inspeccion->id));
                        if ($aprobacionesAnteriores != null) {
                            foreach ($aprobacionesAnteriores as $a) {
                                $a->valida = 0;
                                $a->update();
                            }
                        }
                    }

                    if ($visitCount >= 1) {
                        //Buscar las aprobaciones anteriores e invalidarlas
                        $aprobacionesAnteriores = AprobacionXFuncionarios::findAll('inspeccion = :i', array(':i' => $inspeccion->id));
                        if ($aprobacionesAnteriores != null) {
                            foreach ($aprobacionesAnteriores as $a) {
                                $a->valida = 0;
                                $a->update();
                            }
                        }
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha creado una visita exitosamente.');
                    Traceador::crearTraza(
                        'Creacion',
                        'Creada visita  ' . $form->numero_visita . ' de la inspeccion de DACs: ' . $inspeccion->numero_control
                    );
                    return $this->redirect(array('detallesDac', 'id' => $id));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                    $form->addError('id', $e->getMessage());
                    return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
                }
            } else {
                return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
            }
        } else {
            return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
        }
    }

    public function actionCreateVisitaMIT($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createvisitamit')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionMIT';
        $form = new VisitaInspeccionForm();
        $path_rig = realpath(Yii::getAlias('@web') . '/uploads/files') . '/';

        $inspeccion = Inspecciones::findOne($id);

        if (isset($_POST['VisitaInspeccionForm'])) {
            $form->setMyAttributes($_POST['VisitaInspeccionForm']);
            $form->homologacion = $inspeccion->homologacion;
            $visitCount = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $inspeccion->id))->count();
            $form->numero_visita = $visitCount - 1;

            if ($form->validate()) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    $this->insertVisitaInspeccion($form, $inspeccion);
                    /****************************           ******************************/
                    /****************** Modificar el estado de aprobacion y los RIGS ****/
                    /****************************           ******************************/
                    foreach ($inspeccion->inspeccionesXMediosIzados as $mit) {
                        $ma = 'obj_aprobado' . $mit->medioIzado->id;
                        if (isset($_POST['VisitaInspeccionForm'][$ma])) {
                            $mit->aprobado = 1;
                        } else {
                            $mit->aprobado = 0;
                        }
                        $fileN = 'file' . $mit->medioIzado->id;
                        $var = UploadedFile::getInstanceByName("VisitaInspeccionForm[$fileN]");
                        if (!empty($var)) {
                            if ($mit->rig != null) {
                                unlink($path_rig . $mit->rig);
                            }
                            $fileName = 'rig_MIT_' . $mit->medioIzado->id . '-' . $inspeccion->numero_control . '.' . $var->extensionName;
                            $var->saveAs($path_rig . $fileName);
                            $mit->rig = $fileName;
                        }
                        $mit->update();
                    }

                    $inspeccion->estado = 1;
                    $inspeccion->cerrada = 0;
                    $inspeccion->update();

                    if ($visitCount >= 1) {
                        //Buscar las aprobaciones anteriores e invalidarlas
                        $aprobacionesAnteriores = AprobacionXFuncionarios::findAll('inspeccion = :i', array(':i' => $inspeccion->id));
                        if ($aprobacionesAnteriores != null) {
                            foreach ($aprobacionesAnteriores as $a) {
                                $a->valida = 0;
                                $a->update();
                            }
                        }
                    }

                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha creado una visita exitosamente.');
                    Traceador::crearTraza(
                        'Creacion',
                        'Creada visita  ' . $form->numero_visita . ' de la inspeccion de DACs: ' . $inspeccion->numero_control
                    );
                    Yii::$app->session->setFlash('success', 'Fue adicionada correctamente una visita de inspeccion');
                    $id = $inspeccion->id;
                    return $this->redirect(array('detallesMit', 'id' => $id));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                    $form->addError('id', $e->getMessage());
                    return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
                }
            } else {
                return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
            }
            return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
        } else {
            return $this->render('createVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
        }
    }

    public function actionUpdateVisitaPersonas($id, $v)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_updatevisitapersonas')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionPersona';
        $form = new VisitaInspeccionForm();

        $inspeccion = Inspecciones::findOne($id);
        $form->setMyAttributesByModel(VisitasInspecciones::findOne($v));

        if (isset($_POST['VisitaInspeccionForm'])) {
            $form->setMyAttributes($_POST['VisitaInspeccionForm']);
            $form->homologacion = $inspeccion->homologacion;

            if ($form->validate()) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $this->updateVisitaInspeccion($form, $inspeccion, $v);
                    /****************************           ******************************/
                    /****************** Modificar el estado de aprobacion y los RIGS ****/
                    /****************************           ******************************/
                    foreach ($inspeccion->inspeccionesXPersonases as $mit) {
                        $ma = 'obj_aprobado' . $mit->persona0->id;
                        if (isset($_POST['VisitaInspeccionForm'][$ma])) {
                            $mit->aprobada = 1;
                        } else {
                            $mit->aprobada = 0;
                        }
                        $mit->update();
                    }
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha modificado una visita exitosamente.');
                    Traceador::crearTraza(
                        'Edicion',
                        'Editada visita numero (' . $form->numero_visita . ') de la inspeccion de personas: ' . $inspeccion->numero_control
                    );
                    return $this->redirect(array('detallesPersonas', 'id' => $id));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                    $form->addError('id', $e->getMessage());
                    return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
                }
            } else {
                return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'v' => $v, 'formType' => $formType));
            }
        } else {
            return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'v' => $v, 'formType' => $formType));
        }
    }

    /*
     * Actualizar las visitas de inspeccion
     */
    private function updateVisitaInspeccion($form, $inspeccion, $id)
    {
        $visita = VisitasInspecciones::findOne($id);
        //$visita = new VisitaInspeccionForm();
        $visita->setAttributes($this->selectInspeccionTypeForm($form));
        $visita->fecha = $form->fecha;
        $visita->update();

        if ($visita->aprobado == 1) {
            $inspeccion->estado = 1;
        } else {
            $lastVisit = $inspeccion->getUltimaVisita();
            if ($lastVisit->id != $visita->id && $lastVisit->aprobado != 1) {
                $inspeccion->estado = 2;
            }
        }
        $inspeccion->lugar_inspeccion = $form->lugar_inspeccion;
        $inspeccion->update();
        //update Inspectores Actuantes
        if ($form->inspectores_actuantes != null) {
            //delete current inspectores...
            foreach ($visita->inspectoresActuantes as $inspector) {
                $inspectorBorrar = InspectoresActuantes::findOne($inspector->id);
                $inspectorBorrar->delete();
            }

            foreach ($form->inspectores_actuantes as $idInspector) {
                $inspector = new InspectoresActuantes();
                $inspector->visita = $visita->id;
                $inspector->inspector = $idInspector;
                $inspector->save();
            }
        }
    }

    /*
* Actualiza la nueva visita de inspeccion a laboratorios y servicios
*/
    public function actionUpdateVisitaLabServ($id, $v)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_updatevisitalabserv')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionLabServ';
        $form = new VisitaInspeccionForm();

        $inspeccion = Inspecciones::findOne($id);
        $form->setMyAttributesByModel(VisitasInspecciones::findOne($v));

        if (isset($_POST['VisitaInspeccionForm'])) {
            $form->setMyAttributes($_POST['VisitaInspeccionForm']);
            $form->homologacion = $inspeccion->homologacion;

            if ($form->validate()) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $this->updateVisitaInspeccion($form, $inspeccion, $v);
                    /****************************           ******************************/
                    /****************** Modificar el estado de aprobacion *********** ****/
                    /****************************           ******************************/
                    foreach ($inspeccion->inspeccionesXServiciosProductoses as $mit) {
                        $ma = 'obj_aprobado' . $mit->servcios0->id;
                        if (isset($_POST['VisitaInspeccionForm'][$ma])) {
                            $mit->aprobado = 1;
                        } else {
                            $mit->aprobado = 0;
                        }
                        $mit->update();
                    }
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha modificado una visita exitosamente.');
                    Traceador::crearTraza(
                        'Edicion',
                        'Editada visita numero (' . $form->numero_visita . ') de la inspeccion de Laboratorios y servicios: ' . $inspeccion->numero_control
                    );
                    return $this->redirect(array('DetallesLabServ', 'id' => $id));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::$app->session->setFlash('error', $e->getMessage());
                    $form->addError('id', $e->getMessage());
                    return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
                }
            } else {
                return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'v' => $v, 'formType' => $formType));
            }
        } else {
            return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'v' => $v, 'formType' => $formType));
        }
    }

    /*
    * Actualiza la nueva visita de inspeccion a laboratorios y servicios
    */
    public function actionUpdateVisitaProductos($id, $v)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_updatevisitaproductos')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionProducto';
        $form = new VisitaInspeccionForm();

        $inspeccion = Inspecciones::findOne($id);
        $form->setMyAttributesByModel(VisitasInspecciones::findOne($v));

        if (isset($_POST['VisitaInspeccionForm'])) {
            $form->setMyAttributes($_POST['VisitaInspeccionForm']);
            $form->homologacion = $inspeccion->homologacion;

            if ($form->validate()) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $this->updateVisitaInspeccion($form, $inspeccion, $v);
                    /****************************           ******************************/
                    /****************** Modificar el estado de aprobacion y los RIGS ****/
                    /****************************           ******************************/
                    foreach ($inspeccion->inspeccionesXProductoses as $mit) {
                        $ma = 'obj_aprobado' . $mit->producto0->id;
                        if (isset($_POST['VisitaInspeccionForm'][$ma])) {
                            $mit->aprobado = 1;
                        } else {
                            $mit->aprobado = 0;
                        }
                        $mit->update();
                    }
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha modificado una visita exitosamente.');
                    Traceador::crearTraza(
                        'Edicion',
                        'Editada visita numero (' . $form->numero_visita . ') de la inspeccion de productos: ' . $inspeccion->numero_control
                    );
                    return $this->redirect(array('DetallesProductos', 'id' => $id));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                    $form->addError('id', $e->getMessage());
                    return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
                }
            } else {
                return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'v' => $v, 'formType' => $formType));
            }
        } else {
            return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'v' => $v, 'formType' => $formType));
        }
    }

    public function actionUpdateVisitaDac($id, $v)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_updatevisitadac')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionDac';
        $form = new VisitaInspeccionForm();

        $inspeccion = Inspecciones::findOne($id);
        $form->setMyAttributesByModel(VisitasInspecciones::findOne($v));

        if (isset($_POST['VisitaInspeccionForm'])) {
            $form->setMyAttributes($_POST['VisitaInspeccionForm']);
            $form->homologacion = $inspeccion->homologacion;
            $form->id = $v;

            if ($form->validate()) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $this->updateVisitaInspeccion($form, $inspeccion, $v);
                    /****************************           ******************************/
                    /****************** Modificar el estado de aprobacion y los RIGS ****/
                    /****************************           ******************************/
                    foreach ($inspeccion->inspeccionesXDacses as $mit) {
                        $ma = 'obj_aprobado' . $mit->dac0->id;
                        if (isset($_POST['VisitaInspeccionForm'][$ma])) {
                            $mit->aprobado = 1;
                        } else {
                            $mit->aprobado = 0;
                        }
                        $mit->update();
                    }
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha modificado una visita exitosamente.');
                    Traceador::crearTraza(
                        'Edicion',
                        'Editada visita numero (' . $form->numero_visita . ') de la inspeccion del DAC: ' . $inspeccion->numero_control
                    );
                    return $this->redirect(array('detallesDac', 'id' => $id));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                    $form->addError('id', $e->getMessage());
                    return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'formType' => $formType));
                }
            } else {
                return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'v' => $v, 'formType' => $formType));
            }
        } else {
            return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'v' => $v, 'formType' => $formType));
        }
    }

    public function actionUpdateVisitaMIT($id, $v)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_updatevisitadac')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionMIT';
        $form = new VisitaInspeccionForm();
        $path_rig = realpath(Yii::getAlias('@web') . '/uploads/files') . '/';

        $inspeccion = Inspecciones::findOne($id);
        $form->setMyAttributesByModel(VisitasInspecciones::findOne($v));

        if (isset($_POST['VisitaInspeccionForm'])) {
            $form->setMyAttributes($_POST['VisitaInspeccionForm']);
            $form->homologacion = $inspeccion->homologacion;
            $form->id = $v;

            if ($form->validate()) {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $this->updateVisitaInspeccion($form, $inspeccion, $v);
                    /****************************           ******************************/
                    /****************** Modificar el estado de aprobacion y los RIGS ****/
                    /****************************           ******************************/
                    foreach ($inspeccion->inspeccionesXMediosIzados as $mit) {
                        $ma = 'obj_aprobado' . $mit->medioIzado->id;
                        if (isset($_POST['VisitaInspeccionForm'][$ma])) {
                            $mit->aprobado = 1;
                        } else {
                            $mit->aprobado = 0;
                        }
                        $fileN = 'file' . $mit->medioIzado->id;
                        $var = UploadedFile::getInstanceByName("VisitaInspeccionForm[$fileN]");
                        if (!empty($var)) {
                            if ($mit->rig != null) {
                                if (file_exists($path_rig . $mit->rig)) {
                                    unlink($path_rig . $mit->rig);
                                }
                            }
                            $fileName = 'rig_MIT_' . $mit->medioIzado->id . '-' . $inspeccion->numero_control . '.' . $var->extensionName;
                            $var->saveAs($path_rig . $fileName);
                            $mit->rig = $fileName;
                        }
                        $mit->update();
                    }
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha modificado una visita exitosamente.');
                    Traceador::crearTraza(
                        'Edicion',
                        'Editada visita numero (' . $form->numero_visita . ') de la inspeccion del MIT: ' . $inspeccion->numero_control
                    );
                    return $this->redirect(array('detallesMit', 'id' => $id));
                } catch (Exception $e) {
                    $transaction->rollback();
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                    $form->addError('id', $e->getMessage());
                    return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'v' => $v, 'formType' => $formType));
                }
            } else {
                return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'v' => $v, 'formType' => $formType));
            }
        } else {
            return $this->render('updateVisita', array('model' => $form, 'id' => $inspeccion, 'v' => $v, 'formType' => $formType));
        }
    }

    /*
     * Toma los datos necesarios para crear una inspeccion a personas desde el formulario y los prepara para su insercion en
     * cada tabla necesaria.
     *
     * Tablas: [inspeccion, visitas_inspeccion, inspecciones_x_personas]
     */
    public function actionCreateInspeccionPersona()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createinspeccionpersona')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionPersona';
        $indexPage = 'indexPersonas';
        $form = new InspeccionPersonaForm();
        $persona = new PersonasAcreditadas();

        if (isset($_POST['InspeccionPersonaForm'])) {
            $form->setMyAttributes($_POST['InspeccionPersonaForm']);

            if ($form->validate()) {
                /****************************           ******************************/
                /******************Adiciona primero la Inspeccion *********************/
                /*************************            *********************************/
                $inspeccion = new Inspecciones();
                $inspeccion->setAttributes($_POST['InspeccionPersonaForm']);
                $inspeccion->estado = 1;
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $user = Funcionarios::findOne('usuario = :u', array(':u' => Yii::$app->user->id));
                    $oficina = null;
                    if ($user != null) {
                        $oficina = $user->oficina0;
                        if ($oficina->oficina_superior != $oficina->id) {
                            //No es de la principal y toma la oficina del usuario registrado
                            $inspeccion->numero_control = $oficina->codigo . ' ' . $inspeccion->numero_control;
                        } else {
                            //Es de la central y debe tomar el seleccionado
                            $inspeccion->numero_control = $inspeccion->oficina0->codigo . ' ' . $inspeccion->numero_control;
                            $central = Oficinas::findOne('oficina_superior = id');
                            if ($inspeccion->oficina0->oficina_superior !== $central->id) {
                                //Esta en una suboficina y se le asigna la dependencia
                                $inspeccion->oficina0 = $inspeccion->oficina0->oficinaSuperior;
                            }
                        }
                    } else {
                        $inspeccion->numero_control = $inspeccion->oficina0->codigo . ' ' . $inspeccion->numero_control;
                    }
                    $inspeccion->nivel = $inspeccion->homologacion0->niveles_inspeccion_id;
                    $inspeccion->cerrada = 0;

                    if (empty($form->store_aprobados) && !empty($form->store_desaprobados)) {
                        $inspeccion->estado = 2;
                    } else {
                        $inspeccion->estado = 1;
                    }
                    $inspeccion->save();
                    $this->insertVisitaInspeccion($form, $inspeccion);
                    /****************************           ******************************/
                    /************** Adiciona ahora las Inspecciones por Personas ********/
                    /****************************           ******************************/
                    $i = 1;
                    if (isset($form->store_aprobados) && !empty($form->store_aprobados) && $form->store_aprobados != null) {
                        foreach ($form->store_aprobados as $regmit) {
                            $persona = new InspeccionesXPersonas();
                            $persona->persona = $regmit;
                            $persona->inspeccion = $inspeccion->id;
                            if ($i < 10) {
                                $persona->numero = '0' . $i;
                            } else {
                                $persona->numero = $i;
                            }
                            $persona->aprobada = 1;
                            $persona->save();
                            $i++;
                        }
                    }
                    if (isset($form->store_desaprobados) && !empty($form->store_desaprobados)) {
                        foreach ($form->store_desaprobados as $regmit) {
                            $personaD = new InspeccionesXPersonas();
                            $personaD->persona = $regmit;
                            $personaD->inspeccion = $inspeccion->id;
                            if ($i < 10) {
                                $personaD->numero = '0' . $i;
                            } else {
                                $personaD->numero = $i;
                            }
                            $personaD->aprobada = 0;
                            $personaD->save();
                            $i++;
                        }
                    }
                    Traceador::crearTraza('Creacion', 'Creada la inspeccion ' . $inspeccion->numero_control);
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha creado una inspecci&oacute;n exitosamente.');
                    return $this->redirect(array($indexPage));
                } catch (Exception $e) {
                    $transaction->rollback();
                    $form->addError('id', $e->getMessage());
                    Yii::$app->session->setFlash('error', 'Ha ocurrido el siguiente error: ' . $e->getMessage());
                    return $this->render('create', array('model' => $form, 'formType' => $formType, 'persona' => $persona));
                }
            } else {
                return $this->render('create', array('model' => $form, 'formType' => $formType, 'persona' => $persona));
            }
        } else {
            return $this->render('create', array('model' => $form, 'formType' => $formType, 'persona' => $persona));
        }
    }

    /*
     * Toma los datos necesarios para crear una inspeccion a laboratorios y servicios y los prepara para su insercion en
     * cada tabla necesaria.abstract
     *
     * Tablas: [inspeccion, visita_inspeccion, inspecciones_x_servicios_productos]
     */
    public function actionCreateInspeccionLabServ()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createinspeccionlabserv')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionLabServ';
        $indexPage = 'indexLabServ';
        $form = new InspeccionLabServForm();
        $servicio = new ServiciosLaboratoriosAcreditados();

        if (isset($_POST['InspeccionLabServForm'])) {
            $form->setMyAttributes($_POST['InspeccionLabServForm']);


            if ($form->validate()) {
                /****************************           ******************************/
                /******************Adiciona primero la Inspeccion *********************/
                /*************************            *********************************/
                $inspeccion = new Inspecciones();
                $inspeccion->setAttributes($_POST['InspeccionLabServForm']);
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    $user = Funcionarios::findOne('usuario = :u', array(':u' => Yii::$app->user->id));
                    $oficina = null;
                    if ($user != null) {
                        $oficina = $user->oficina0;
                        if ($oficina->oficina_superior != $oficina->id) {
                            //No es de la principal y toma la oficina del usuario registrado
                            $inspeccion->numero_control = $oficina->codigo . ' ' . $inspeccion->numero_control;
                        } else {
                            //Es de la central y debe tomar el seleccionado
                            $inspeccion->numero_control = $inspeccion->oficina0->codigo . ' ' . $inspeccion->numero_control;
                            $central = Oficinas::findOne('oficina_superior = id');
                            if ($inspeccion->oficina0->oficina_superior !== $central->id) {
                                //Esta en una suboficina y se le asigna la dependencia
                                $inspeccion->oficina0 = $inspeccion->oficina0->oficinaSuperior;
                            }
                        }
                    } else {
                        $inspeccion->numero_control = $inspeccion->oficina0->codigo . ' ' . $inspeccion->numero_control;
                    }
                    $inspeccion->nivel = $inspeccion->homologacion0->niveles_inspeccion_id;
                    $inspeccion->cerrada = 0;
                    if (empty($form->store_aprobados) && !empty($form->store_desaprobados)) {
                        $inspeccion->estado = 2;
                    } else {
                        $inspeccion->estado = 1;
                    }
                    $inspeccion->save();
                    $this->insertVisitaInspeccion($form, $inspeccion);
                    /****************************           ******************************/
                    /************** Adiciona ahora las Inspecciones por Productos ********/
                    /****************************           ******************************/
                    $i = 1;
                    if (isset($form->store_aprobados) && !empty($form->store_aprobados) && $form->store_aprobados != null) {
                        foreach ($form->store_aprobados as $regmit) {
                            $servicioS = new InspeccionesXServiciosProductos();
                            $servicioS->servcios0 = ServiciosLaboratoriosAcreditados::findOne($regmit);
                            $servicioS->inspeccion = $inspeccion->id;
                            if ($i < 10) {
                                $servicioS->numero = '0' . $i;
                            } else {
                                $servicioS->numero = $i;
                            }
                            $servicioS->aprobado = 1;
                            $servicioS->save();
                            $i++;
                        }
                    }
                    if (isset($form->store_desaprobados) && !empty($form->store_desaprobados)) {
                        foreach ($form->store_desaprobados as $regmit) {
                            $servicioD = new InspeccionesXServiciosProductos();
                            $servicioD->servcios0 = ServiciosLaboratoriosAcreditados::findOne($regmit);
                            $servicioD->inspeccion = $inspeccion->id;
                            if ($i < 10) {
                                $servicioD->numero = '0' . $i;
                            } else {
                                $servicioD->numero = $i;
                            }
                            $servicioD->aprobado = 0;
                            $servicioD->save();
                            $i++;
                        }
                    }
                    Traceador::crearTraza('Creacion', 'Creada la inspeccion ' . $inspeccion->numero_control);
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha creado una inspecci&oacute;n exitosamente.');
                    return $this->redirect(array($indexPage));
                } catch (Exception $e) {
                    $transaction->rollback();
                    $form->addError('id', $e->getMessage());
                    Yii::$app->session->setFlash('error', 'Ha ocurrido el siguiente error: ' . $e->getMessage());
                    return $this->render('create', array('model' => $form, 'formType' => $formType, 'servicio' => $servicio));
                }
            } else {
                return $this->render('create', array('model' => $form, 'formType' => $formType, 'servicio' => $servicio));
            }
        } else {
            return $this->render('create', array('model' => $form, 'formType' => $formType, 'servicio' => $servicio));
        }
    }

    public function actionCreateInspeccionProducto()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createinspeccionproducto')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionProducto';
        $indexPage = 'indexProductos';
        $form = new InspeccionProductoForm();
        $prod = new ProductosAcreditados();

        if (isset($_POST['InspeccionProductoForm'])) {
            $form->setMyAttributes($_POST['InspeccionProductoForm']);
            if ($form->validate()) {
                /****************************           ******************************/
                /******************Adiciona primero la Inspeccion *********************/
                /*************************            *********************************/
                $inspeccion = new Inspecciones();
                $inspeccion->setAttributes($_POST['InspeccionProductoForm']);
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $user = Funcionarios::findOne('usuario = :u', array(':u' => Yii::$app->user->id));
                    $oficina = null;
                    if ($user != null) {
                        $oficina = $user->oficina0;
                        if ($oficina->oficina_superior != $oficina->id) {
                            //No es de la principal y toma la oficina del usuario registrado
                            $inspeccion->numero_control = $oficina->codigo . ' ' . $inspeccion->numero_control;
                        } else {
                            //Es de la central y debe tomar el seleccionado
                            $inspeccion->numero_control = $inspeccion->oficina0->codigo . ' ' . $inspeccion->numero_control;
                            $central = Oficinas::findOne('oficina_superior = id');
                            if ($inspeccion->oficina0->oficina_superior !== $central->id) {
                                //Esta en una suboficina y se le asigna la dependencia
                                $inspeccion->oficina0 = $inspeccion->oficina0->oficinaSuperior;
                            }
                        }
                    } else {
                        $inspeccion->numero_control = $inspeccion->oficina0->codigo . ' ' . $inspeccion->numero_control;
                    }
                    $inspeccion->nivel = $inspeccion->homologacion0->niveles_inspeccion_id;
                    $inspeccion->cerrada = 0;
                    if (empty($form->store_aprobados) && !empty($form->store_desaprobados)) {
                        $inspeccion->estado = 2;
                    } else {
                        $inspeccion->estado = 1;
                    }
                    $inspeccion->save();
                    $this->insertVisitaInspeccion($form, $inspeccion);
                    /****************************           ******************************/
                    /************** Adiciona ahora las Inspecciones por Productos ********/
                    /****************************           ******************************/
                    $i = 1;
                    if (isset($form->store_aprobados) && !empty($form->store_aprobados) && $form->store_aprobados != null) {
                        foreach ($form->store_aprobados as $regmit) {
                            $producto = new InspeccionesXProductos();
                            $producto->producto = $regmit;
                            $producto->inspeccion = $inspeccion->id;
                            if ($i < 10) {
                                $producto->numero = '0' . $i;
                            } else {
                                $producto->numero = $i;
                            }
                            $producto->aprobado = 1;
                            $producto->save();
                            $i++;
                        }
                    }
                    if (isset($form->store_desaprobados) && !empty($form->store_desaprobados)) {
                        foreach ($form->store_desaprobados as $regmit) {
                            $productoD = new InspeccionesXProductos();
                            $productoD->producto = $regmit;
                            $productoD->inspeccion = $inspeccion->id;
                            if ($i < 10) {
                                $productoD->numero = '0' . $i;
                            } else {
                                $productoD->numero = $i;
                            }
                            $productoD->aprobado = 0;
                            $productoD->save();
                            $i++;
                        }
                    }
                    Traceador::crearTraza('Creacion', 'Creada la inspeccion ' . $inspeccion->numero_control);
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha creado una inspecci&oacute;n exitosamente.');
                    return $this->redirect(array($indexPage));
                } catch (Exception $e) {
                    $transaction->rollback();
                    $form->addError('id', $e->getMessage());
                    Yii::$app->session->setFlash('error', 'Ha ocurrido el siguiente error: ' . $e->getMessage());
                    return $this->render('create', array('model' => $form, 'formType' => $formType, 'producto' => $prod));
                }
            } else {
                return $this->render('create', array('model' => $form, 'formType' => $formType, 'producto' => $prod));
            }
        } else {
            return $this->render('create', array('model' => $form, 'formType' => $formType, 'producto' => $prod));
        }
    }

    public function actionCreateInspeccionDac()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createinspecciondac')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionDac';
        $indexPage = 'indexDacs';
        $form = new InspeccionDacForm();
        $dac = new DacsAcreditados();

        if (isset($_POST['InspeccionDacForm'])) {
            $form->setMyAttributes($_POST['InspeccionDacForm']);
            if ($form->validate()) {
                /****************************           ******************************/
                /******************Adiciona primero la Inspeccion *********************/
                /*************************            *********************************/
                $inspeccion = new Inspecciones();
                $inspeccion->setAttributes($_POST['InspeccionDacForm']);

                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $user = Funcionarios::findOne('usuario = :u', array(':u' => Yii::$app->user->id));
                    $oficina = null;
                    if ($user != null) {
                        $oficina = $user->oficina0;
                        if ($oficina->oficina_superior != $oficina->id) {
                            //No es de la principal y toma la oficina del usuario registrado
                            $inspeccion->numero_control = $oficina->codigo . ' ' . $inspeccion->numero_control;
                        } else {
                            //Es de la central y debe tomar el seleccionado
                            $inspeccion->numero_control = $inspeccion->oficina0->codigo . ' ' . $inspeccion->numero_control;
                            $central = Oficinas::findOne('oficina_superior = id');
                            if ($inspeccion->oficina0->oficina_superior !== $central->id) {
                                //Esta en una suboficina y se le asigna la dependencia
                                $inspeccion->oficina0 = $inspeccion->oficina0->oficinaSuperior;
                            }
                        }
                    } else {
                        $inspeccion->numero_control = $inspeccion->oficina0->codigo . ' ' . $inspeccion->numero_control;
                    }
                    $inspeccion->nivel = $inspeccion->homologacion0->niveles_inspeccion_id;
                    $inspeccion->cerrada = 0;
                    if (empty($form->store_aprobados) && !empty($form->store_desaprobados)) {
                        $inspeccion->estado = 2;
                    } else {
                        $inspeccion->estado = 1;
                    }
                    $inspeccion->save();
                    $this->insertVisitaInspeccion($form, $inspeccion);
                    /****************************           ******************************/
                    /******************Adiciona ahora las Inspecciones por MITs *********/
                    /***************** con los correspondientes documentos **************/
                    /****************************           ******************************/
                    $i = 1;
                    if (isset($form->store_aprobados) && !empty($form->store_aprobados) && $form->store_aprobados != null) {
                        foreach ($form->store_aprobados as $regmit) {
                            $producto = new InspeccionesXDacs();
                            $producto->dac = $regmit;
                            $producto->inspeccion = $inspeccion->id;
                            if ($i < 10) {
                                $producto->numero = '0' . $i;
                            } else {
                                $producto->numero = $i;
                            }
                            $producto->aprobado = 1;
                            $producto->save();
                            $i++;
                        }
                    }
                    if (isset($form->store_desaprobados) && !empty($form->store_desaprobados)) {
                        foreach ($form->store_desaprobados as $regmit) {
                            $productoD = new InspeccionesXDacs();
                            $productoD->dac = $regmit;
                            $productoD->inspeccion = $inspeccion->id;
                            if ($i < 10) {
                                $productoD->numero = '0' . $i;
                            } else {
                                $productoD->numero = $i;
                            }
                            $productoD->aprobado = 0;
                            $productoD->save();
                            $i++;
                        }
                    }
                    Traceador::crearTraza('Creacion', 'Creada la inspeccion ' . $inspeccion->numero_control);
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha creado una inspecci&oacute;n exitosamente.');
                    return $this->redirect(array($indexPage));
                } catch (Exception $e) {
                    $transaction->rollback();
                    $form->addError('id', $e->getMessage());
                    Yii::$app->session->setFlash('error', 'Ha ocurrido el siguiente error: ' . $e->getMessage());
                    return $this->render('create', array('model' => $form, 'formType' => $formType, 'mit' => $dac));
                }
            } else {
                return $this->render('create', array('model' => $form, 'formType' => $formType, 'dac' => $dac));
            }
        } else {
            return $this->render('create', array('model' => $form, 'formType' => $formType, 'dac' => $dac));
        }
    }

    public function actionCreateInspeccionMIT()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createinspeccionmit')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionMIT';
        $indexPage = 'indexMITs';
        $form = new InspeccionMITForm();
        $mit = new MediosIzadoAcreditados();

        if (isset($_POST['InspeccionMITForm'])) {
            if ($_POST['InspeccionMIT_homologacionForm'] = 94) {
                $form->tipo_homologacion = 5;
            }
            $form->setMyAttributes($_POST['InspeccionMITForm']);
            if ($form->validate()) {
                /****************************           ******************************/
                /******************Adiciona primero la Inspeccion *********************/
                /*************************            *********************************/
                $inspeccion = new Inspecciones();
                $inspeccion->setAttributes($_POST['InspeccionMITForm']);

                $transaction = Yii::$app->db->beginTransaction();

                try {
                    $user = Funcionarios::findOne('usuario = :u', array(':u' => Yii::$app->user->id));
                    $oficina = null;
                    if ($user != null) {
                        $oficina = $user->oficina0;
                        if ($oficina->oficina_superior != $oficina->id) {
                            //No es de la principal y toma la oficina del usuario registrado
                            $inspeccion->numero_control = $oficina->codigo . ' ' . $inspeccion->numero_control;
                        } else {
                            //Es de la central y debe tomar el seleccionado
                            $inspeccion->numero_control = $inspeccion->oficina0->codigo . ' ' . $inspeccion->numero_control;
                            $central = Oficinas::findOne('oficina_superior = id');
                            if ($inspeccion->oficina0->oficina_superior !== $central->id) {
                                //Esta en una suboficina y se le asigna la dependencia
                                $inspeccion->oficina0 = $inspeccion->oficina0->oficinaSuperior;
                            }
                        }
                    } else {
                        $inspeccion->numero_control = $inspeccion->oficina0->codigo . ' ' . $inspeccion->numero_control;
                    }
                    $inspeccion->nivel = $inspeccion->homologacion0->niveles_inspeccion_id;
                    $inspeccion->cerrada = 0;
                    if (empty($form->store_aprobados) && !empty($form->store_desaprobados)) {
                        $inspeccion->estado = 2;
                    } else {
                        $inspeccion->estado = 1;
                    }
                    $inspeccion->save();
                    $this->insertVisitaInspeccion($form, $inspeccion);
                    /****************************           ******************************/
                    /******************Adiciona ahora las Inspecciones por MITs *********/
                    /***************** con los correspondientes documentos **************/
                    /****************************           ******************************/
                    $path_rig = realpath(Yii::getAlias('@web') . '/uploads/files') . '/';
                    $i = 1;
                    if (isset($form->store_aprobados) && !empty($form->store_aprobados) && $form->store_aprobados != null) {
                        foreach ($form->store_aprobados as $regmit) {
                            $producto = new InspeccionesXMediosIzado();
                            $producto->medio_izado = $regmit;
                            $producto->inspeccion = $inspeccion->id;
                            if ($i < 10) {
                                $producto->numero = '0' . $i;
                            } else {
                                $producto->numero = $i;
                            }
                            $producto->aprobado = 1;
                            $fileN = 'aprobado' . $regmit;
                            $var = UploadedFile::getInstanceByName("InspeccionMITForm[$fileN]");
                            if (!empty($var)) {
                                $fileName = 'rig_MIT_' . $regmit . '-' . $inspeccion->numero_control . '.' . $var->extensionName;
                                $var->saveAs($path_rig . $fileName);
                                $producto->rig = $fileName;
                            }
                            $producto->save();
                            $i++;
                        }
                    }
                    if (isset($form->store_desaprobados) && !empty($form->store_desaprobados)) {
                        foreach ($form->store_desaprobados as $regmit) {
                            $productoD = new InspeccionesXMediosIzado();
                            $productoD->medio_izado = $regmit;
                            $productoD->inspeccion = $inspeccion->id;
                            if ($i < 10) {
                                $productoD->numero = '0' . $i;
                            } else {
                                $productoD->numero = $i;
                            }
                            $productoD->aprobado = 0;
                            $fileD = 'desaprobado' . $regmit;
                            $vard = UploadedFile::getInstanceByName("InspeccionMITForm[$fileD]");
                            if (!empty($vard)) {
                                $fileNameD = 'rig_MIT_' . $regmit . '-' . $inspeccion->numero_control . '.' . $vard->extensionName;
                                $vard->saveAs($path_rig . $fileNameD);
                                $productoD->rig = $fileNameD;
                            }

                            $productoD->save();
                            $i++;
                        }
                    }
                    Traceador::crearTraza('Creacion', 'Creada la inspeccion ' . $inspeccion->numero_control);
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Se ha creado una inspecci&oacute;n exitosamente.');
                    return $this->redirect(array($indexPage));
                } catch (Exception $e) {
                    $transaction->rollback();
                    $form->addError('id', $e->getMessage());
                    Yii::$app->session->setFlash('error', 'Ha ocurrido el siguiente error: ' . $e->getMessage());
                    return $this->render('create', array('model' => $form, 'formType' => $formType, 'mit' => $mit));
                }
                //$this->insertInspeccion($form, $formType, $indexPage);
            } else {
                return $this->render('create', array('model' => $form, 'formType' => $formType, 'mit' => $mit));
            }
            return $this->render('create', array('model' => $form, 'formType' => $formType, 'mit' => $mit));
        } else {
            return $this->render('create', array('model' => $form, 'formType' => $formType, 'mit' => $mit));
        }
    }

    /*
     * Devuelve el tipo de formulario usado por POST en dependencia de la inspeccion.
     */
    private function selectInspeccionTypeForm($form)
    {
        switch (get_class($form)) {
            case 'InspeccionPersonaForm':
                return $_POST['InspeccionPersonaForm'];
                break;
            case 'InspeccionLabServForm':
                return $_POST['InspeccionLabServForm'];
                break;
            case 'InspeccionProductoForm':
                return $_POST['InspeccionProductoForm'];
                break;
            case 'InspeccionDacForm':
                return $_POST['InspeccionDacForm'];
                break;
            case 'InspeccionMITForm':
                return $_POST['InspeccionMITForm'];
                break;
            case 'InspeccionPersonaEditarForm':
                return $_POST['InspeccionPersonaEditarForm'];
                break;
            case 'InspeccionLabServEditarForm':
                return $_POST['InspeccionLabServEditarForm'];
                break;
            case 'InspeccionProductoEditarForm':
                return $_POST['InspeccionProductoEditarForm'];
                break;
            case 'InspeccionDacEditarForm':
                return $_POST['InspeccionDacEditarForm'];
                break;
            case 'InspeccionMITEditarForm':
                return $_POST['InspeccionMITEditarForm'];
                break;
            case 'VisitaInspeccionForm':
                return $_POST['VisitaInspeccionForm'];
                break;
        }
    }

    /*
 * Inserta los datos en la tabla visita_inspeccion para personal.
 */
    private function insertVisitaInspeccionPersona($form, $inspeccion)
    {
        $visita = new VisitasInspecciones();
        //$visita->setAttributes($this->insertVisitaInspeccionPersonas($form,$inspeccion));
        $visita->numero_visita = count(VisitasInspecciones::findAll('inspeccion = ' . $inspeccion->id)) + 1;
        $visita->fecha = $form->fecha;
        $visita->inspeccion = $inspeccion->id;
        $visita->aprobado = 1;

        $visita->save();

        //Actualizar personal
        if (isset($form->personas_homologar)) {
            $i = 1;
            //Primero limpiar
            $tmp = InspeccionesXPersonas::findAll('inspeccion = :insp AND aprobada = :a', array(':insp' => $inspeccion->id, ':a' => 1));
            foreach ($tmp as $ap) {
                $ap->delete();
            }
            //Luego adicionar
            foreach ($form->personas_homologar as $idPersona) {
                $personaHomologar = new InspeccionesXPersonas();
                $personaHomologar->persona = $idPersona;
                $personaHomologar->inspeccion = $inspeccion->id;
                $personaHomologar->numero = '0' . $i;
                $personaHomologar->aprobada = 1;
                $personaHomologar->save();
                $i++;
            }
        }
        if (isset($form->personas_desaprobadas) && !empty($form->personas_desaprobadas)) {
            //Primero limpiar
            $tmp = InspeccionesXPersonas::findAll('inspeccion = :insp AND aprobada = :a', array(':insp' => $inspeccion->id, ':a' => 0));
            foreach ($tmp as $de) {
                $de->delete();
            }
            //Luego adicionar
            foreach ($form->personas_desaprobadas as $idPersona) {
                $personaD = new InspeccionesXPersonas();
                $personaD->persona = $idPersona;
                $personaD->inspeccion = $inspeccion->id;
                $personaD->numero = '0' . $i;
                $personaD->aprobada = 0;
                $personaD->save();
                $i++;
            }
        }

        $this->insertInspectoresActuantes($form, $visita);
    }

    /*
     * Inserta los datos en la tabla visita_inspeccion.
     */
    private function insertVisitaInspeccion($form, $inspeccion)
    {
        $visita = new VisitasInspecciones();
        $visita->setAttributes($this->selectInspeccionTypeForm($form));
        $visita->numero_visita = VisitasInspecciones::findAll('inspeccion = :n', array(':n' => $inspeccion->id + 1))->count();
        $visita->fecha = $form->fecha;
        $visita->inspeccion = $inspeccion->id;
        $visita->aprobado = 1;
        $visita->save();
        $this->insertInspectoresActuantes($form, $visita);
    }

    /*
     * Inserta los datos en la tabla inspectores_actuantes.
     */
    private function insertInspectoresActuantes($form, $visita)
    {
        if ($form->inspectores_actuantes != null) {
            foreach ($form->inspectores_actuantes as $idInspector) {
                $inspector = new InspectoresActuantes();
                $inspector->visita = $visita->id;
                $inspector->inspector = $idInspector;
                $inspector->save();
            }
        }
    }

    /*
     * Edita la inspeccion a personas
     */
    public function actionEditarPersonas($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_editarpersonas')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionPersona';
        $indexPage = 'detallesPersonas';
        $inspeccion = Inspecciones::findOne($id);
        $lastVisitCount = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $id))->count();
        $visitas = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $id));
        $form = new InspeccionPersonaEditarForm();
        $form->setMyAttributesByModel($inspeccion, $visitas[$lastVisitCount - 1]);

        if (isset($_POST['InspeccionPersonaEditarForm'])) {
            $form->setMyAttributes($_POST['InspeccionPersonaEditarForm']);

            if ($form->validate(array('oficina'))) {
                $this->editarInspeccion($inspeccion, $form, $formType, $indexPage);
            } else {
                $form->setMyAttributesByModel($inspeccion, $visitas[$lastVisitCount - 1]);
                return $this->render('update', array('model' => $form, 'formType' => $formType));
            }
        } else {
            return $this->render('update', array('model' => $form, 'formType' => $formType));
        }
    }

    /*
     * Edita la inspeccion a laboratorios y servicios
     */
    public function actionEditarLabServ($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_editarlabserv')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionLabServ';
        $indexPage = 'detallesLabServ';
        $inspeccion = Inspecciones::findOne($id);
        $lastVisitCount = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $id))->count();
        $visitas = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $id));
        $form = new InspeccionLabServEditarForm();
        $form->setMyAttributesByModel($inspeccion, $visitas[$lastVisitCount - 1]);

        if (isset($_POST['InspeccionLabServEditarForm'])) {
            $form->setMyAttributes($_POST['InspeccionLabServEditarForm']);

            if ($form->validate(array('oficina'))) {
                $this->editarInspeccion($inspeccion, $form, $formType, $indexPage);
            } else {
                $form->setMyAttributesByModel($inspeccion, $visitas[$lastVisitCount - 1]);
                return $this->render('update', array('model' => $form, 'formType' => $formType));
            }
        } else {
            return $this->render('update', array('model' => $form, 'formType' => $formType));
        }
    }

    /*
     * Edita la inspeccion a productos
     */
    public function actionEditarProducto($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_editarproducto')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionProducto';
        $indexPage = 'detallesProductos';
        $inspeccion = Inspecciones::findOne($id);
        $lastVisitCount = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $id))->count();
        $visitas = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $id));
        $form = new InspeccionProductoEditarForm();
        $form->setMyAttributesByModel($inspeccion, $visitas[$lastVisitCount - 1]);

        if (isset($_POST['InspeccionProductoEditarForm'])) {
            $form->setMyAttributes($_POST['InspeccionProductoEditarForm']);

            if ($form->validate(array('oficina'))) {
                $this->editarInspeccion($inspeccion, $form, $formType, $indexPage);
            } else {
                $form->setMyAttributesByModel($inspeccion, $visitas[$lastVisitCount - 1]);
                return $this->render('update', array('model' => $form, 'formType' => $formType));
            }
        } else {
            return $this->render('update', array('model' => $form, 'formType' => $formType));
        }
    }

    public function actionEditarDAC($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_editardac')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionDac';
        $indexPage = 'detallesDac';
        $inspeccion = Inspecciones::findOne($id);
        $lastVisitCount = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $id))->count();
        $visitas = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $id));
        $form = new InspeccionDacEditarForm();
        $form->setMyAttributesByModel($inspeccion, $visitas[$lastVisitCount - 1]);

        if (isset($_POST['InspeccionDacEditarForm'])) {
            $form->setMyAttributes($_POST['InspeccionDacEditarForm']);

            if ($form->validate(array('oficina'))) {
                $this->editarInspeccion($inspeccion, $form, $formType, $indexPage);
            } else {
                $form->setMyAttributesByModel($inspeccion, $visitas[$lastVisitCount - 1]);
                return $this->render('update', array('model' => $form, 'formType' => $formType));
            }
        } else {
            return $this->render('update', array('model' => $form, 'formType' => $formType));
        }
    }

    public function actionEditarMIT($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_editardac')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $formType = 'inspeccionMIT';
        $indexPage = 'detallesMit';
        $inspeccion = Inspecciones::findOne($id);
        $lastVisitCount = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $id))->count();
        $visitas = VisitasInspecciones::findAll('inspeccion = :insp', array(':insp' => $id));
        $form = new InspeccionMITEditarForm();
        $form->setMyAttributesByModel($inspeccion, $visitas[$lastVisitCount - 1]);

        if (isset($_POST['InspeccionMITEditarForm'])) {
            $form->setMyAttributes($_POST['InspeccionMITEditarForm']);

            if ($form->validate(array('oficina'))) {
                $this->editarInspeccion($inspeccion, $form, $formType, $indexPage);
            } else {
                $form->setMyAttributesByModel($inspeccion, $visitas[$lastVisitCount - 1]);
                return $this->render('update', array('model' => $form, 'formType' => $formType));
            }
        } else {
            return $this->render('update', array('model' => $form, 'formType' => $formType));
        }
    }

    /*
     * Metodo encargado de editar las inspecciones. Se auto gestiona la inspecicon a editar en dependencia del formulario
     * que el entre por parametro.
     */
    private function editarInspeccion($inspeccion, $form, $formType, $indexPage)
    {
        $inspeccion->setAttributes($this->selectInspeccionTypeForm($form));

        try {
            $inspeccion->save();
            Yii::$app->session->setFlash('success', 'Se ha actualizado la inspecci&oacute;n <strong>[ No. Control: ' . $inspeccion->numero_control . ' ]</strong> exitosamente.');
            Traceador::crearTraza('Edicion', 'Editada la inspeccion ' . $inspeccion->numero_control);
            return $this->redirect(array($indexPage, 'id' => $inspeccion->id));
        } catch (Exception $e) {
            Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
            $form->addError('id', $e->getMessage());
            return $this->render('create', array('model' => $form, 'formType' => $formType));
        }
    }

    public function loadModel($id)
    {
        $model = Inspecciones::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'inspecciones-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionCreatePersonaAcreditada()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createpersonaacreditada')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $json = new stdClass();

        $request = Yii::$app->getRequest();
        if ($request->isAjaxRequest) {
            $formData = $_POST['formData'];
            $model = new PersonasAcreditadas();
            $model->nombre_apellidos = $formData['nombre_apellidos'];
            $model->ci = $formData['ci'];
            $model->provincia0 = Provincias::findOne($formData['provincia']);
            $model->telefono_contacto = $formData['telefono'];
            $model->correo = $formData['correo'];
            $model->entidad0 = Entidades::findOne($formData['entidad']);

            try {
                $model->save();
                $json->msg = $model->nombre_apellidos;
            } catch (Exception $e) {
                throw new HttpException($e->getCode() . ', ' . $e->getMessage());
            }
        }
        $json_format = json_encode($json);
        echo $json_format;
    }

    public function actionCreateServicioAcreditado()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createservicioacreditado')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $json = new stdClass();

        $request = Yii::$app->getRequest();
        if ($request->isAjaxRequest) {
            $formData = $_POST['formData'];
            $model = new ServiciosLaboratoriosAcreditados();
            $model->servicio = $formData['servicio'];
            $model->departamento = $formData['departamento'];
            $model->provincia0 = Provincias::findOne($formData['provincia']);
            $model->entidad0 = Entidades::findOne($formData['entidad']);

            try {
                $model->save();
                $json->msg = $model->servicio;
            } catch (Exception $e) {
                throw new HttpException($e->getCode() . ', ' . $e->getMessage());
            }
        }
        $json_format = json_encode($json);
        echo $json_format;
    }

    public function actionCreateProductoAcreditado()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createproductoacreditado')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $json = new stdClass();

        $request = Yii::$app->getRequest();
        if ($request->isAjaxRequest) {
            $formData = $_POST['formData'];
            $model = new ProductosAcreditados();
            $model->nombre = $formData['nombre'];
            if (isset($formData['tipo'])) {
                $model->tipo0 = TiposMediosIzado::findOne($formData['tipo']);
            }
            if (isset($formData['provincia'])) {
                $model->provincia0 = Provincias::findOne($formData['provincia']);
            }
            if (isset($formData['propietario'])) {
                $model->propietario0 = Entidades::findOne($formData['propietario']);
            }

            try {
                $model->save();
                $json->msg = $model->nombre;
            } catch (Exception $e) {
                throw new HttpException($e->getCode() . ', ' . $e->getMessage());
            }
        }
        $json_format = json_encode($json);
        echo $json_format;
    }

    public function actionCreateDacAcreditado()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createdacacreditado')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $json = new stdClass();

        $request = Yii::$app->getRequest();
        if ($request->isAjaxRequest) {
            $formData = $_POST['formData'];
            $model = new DacsAcreditados();
            $model->nombre = $formData['nombre'];
            if (isset($formData['tipo'])) {
                $model->tipo0 = TiposDacs::findOne($formData['tipo']);
            }
            if (isset($formData['provincia'])) {
                $model->provincia0 = Provincias::findOne($formData['provincia']);
            }
            if (isset($formData['propietario'])) {
                $model->propietario0 = Entidades::findOne($formData['propietario']);
            }
            if (isset($formData['ct_nominal'])) {
                $model->ct_nominal = $formData['ct_nominal'];
            }

            try {
                $model->save();
                if ($model->tipo0 != null) {
                    $json->msg = $model->tipo0->tipo . ' ' . $model->nombre;
                } else {
                    $json->msg = $model->nombre;
                }
            } catch (Exception $e) {
                throw new HttpException($e->getCode() . ', ' . $e->getMessage());
            }
        }
        $json_format = json_encode($json);
        echo $json_format;
    }

    public function actionCreateMITAcreditado()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_createmitacreditado')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $json = new stdClass();

        $request = Yii::$app->getRequest();
        if ($request->isAjaxRequest) {
            $formData = $_POST['formData'];
            $model = new MediosIzadoAcreditados();
            $model->nombre = $formData['nombre'];
            if (isset($formData['tipo'])) {
                $model->tipo0 = TiposMediosIzado::findOne($formData['tipo']);
            }
            if (isset($formData['categoria'])) {
                $model->categoria0 = CategoriasMediosIzado::findOne($formData['categoria']);
            }
            if (isset($formData['provincia'])) {
                $model->provincia0 = Provincias::findOne($formData['provincia']);
            }
            if (isset($formData['propietario'])) {
                $model->propietario0 = Entidades::findOne($formData['propietario']);
            }
            if (isset($formData['ci_nominal'])) {
                $model->ci_nominal = $formData['ci_nominal'];
            }
            if (isset($formData['ci_aux_nominal'])) {
                $model->ci_aux_nominal = $formData['ci_aux_nominal'];
            }
            if (isset($formData['ci_aux1_nominal'])) {
                $model->ci_aux1_nominal = $formData['ci_aux1_nominal'];
            }

            try {
                $model->save();
                if ($model->tipo0 != null) {
                    $json->msg = $model->tipo0->tipo . ' ' . $model->nombre;
                } else {
                    $json->msg = $model->nombre;
                }
            } catch (Exception $e) {
                throw new HttpException($e->getCode() . ', ' . $e->getMessage());
            }
        }
        $json_format = json_encode($json);
        echo $json_format;
    }

    public function actionAprobaciones($id, $formType)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_inspecciones_aprobaciones')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $inspeccion = Inspecciones::findOne($id);
        $model = new AprobacionXFuncionarios();
        if (isset($_POST['AprobacionXFuncionarios'])) {
            $model->setAttributes($_POST['AprobacionXFuncionarios']);
            $model->actualizado = date('Y-m-d');

            if ($_POST['AprobacionXFuncionarios']['aprobada'] == 1) {
                $model->aprobada = 1;
                $model->rechazada = 0;
            } else {
                $model->aprobada = 0;
                $model->rechazada = 1;
            }
            $model->valida = 1;
            $funcionario = Funcionarios::findOne('usuario = :u', array(':u' => Yii::$app->user->id));
            $model->funcionario = $funcionario->id;
            $model->inspeccion = $id;
            try {
                if ($model->save()) {
                    if ($model->aprobada == 1) {
                        if ($formType == 'inspeccionMIT') {
                            //Depende de la Categoria del MIT
                            $categoria = $inspeccion->inspeccionesXMediosIzados[0]->medioIzado->categoria0;
                            if ($categoria != null and $inspeccion->homologacion0->id != 94) {
                                $cargoAprueba = $categoria->nivelEspecial->id;
                            } else {
                                $cargoAprueba = 5;
                            }
                        } elseif ($formType == 'inspeccionDac') {
                            //Jefe de Oficina
                            $cargoAprueba = 3;
                        } else {
                            //Depende
                            $cargoAprueba = $inspeccion->homologacion0->nivelesInspeccion->cargoCertificadoDefinitivo->id;
                        }
                        if ($funcionario->cargo0->id == $cargoAprueba) {
                            if ($model->hallazgos != null || $model->inconformidades != null) {
                                $inspeccion->estado = 4;
                            } else {
                                $inspeccion->estado = 5;
                            }
                            $inspeccion->update();
                            if ($inspeccion->tipoHomologacion->genera_certificado == 1 && $inspeccion->homologacion != 94) {
                                //Si esta inspeccion es de las que genera certificados
                                //Guardar en Shortcut certificado listo para emitirse
                                //Guardar en dependencia del tipo de inspeccion
                                //Tres anhos
                                $y = date('Y');
                                $m = date('m');
                                $d = date('d');
                                $yy = $y + 3;
                                $dateVc = $yy . '-' . $d . '-' . $m;
                                switch ($formType) {
                                    case 'inspeccionPersona':
                                        $inspPersona = InspeccionesXPersonas::findAll('inspeccion = :i AND aprobada = :a', array(':i' => $id, ':a' => 1));
                                        foreach ($inspPersona as $p) {
                                            if ($p->aprobada == 1) {
                                                $p->aprobado_definitivo = 1;
                                                $p->update();
                                            }
                                            $shortcutmodel = new ShortcutHomologacionesPersonas();
                                            $shortcutmodel->homologacion = $inspeccion->homologacion0->id;
                                            $shortcutmodel->persona = $p->persona0->id;
                                            $shortcutmodel->certificado = $inspeccion->numero_control . '-' . $p->numero;
                                            $shortcutmodel->emision = date('Y-m-d');
                                            $shortcutmodel->vigente = $dateVc;
                                            $shortcutmodel->anho_vigente = $yy;
                                            $shortcutmodel->inspeccion = $p->id;
                                            $shortcutmodel->save();

                                            $p->aprobado_definitivo = 1;
                                            $p->update();
                                        }
                                        break;
                                    case 'inspeccionLabServ':
                                        $inspServicio = InspeccionesXServiciosProductos::findAll('inspeccion = :i AND aprobado = :a', array(':i' => $id, ':a' => 1));
                                        foreach ($inspServicio as $s) {
                                            if ($s->aprobado == 1) {
                                                $s->aprobado_definitivo = 1;
                                                $s->update();
                                            }
                                            $shortcutmodel = new ShortcutHomologacionesServiciosProductos();
                                            $shortcutmodel->homologacion = $inspeccion->homologacion0->id;
                                            $shortcutmodel->servicio = $s->servcios0->id;
                                            $shortcutmodel->certificado = $inspeccion->numero_control . '-' . $s->numero;
                                            $shortcutmodel->emision = date('Y-m-d');
                                            $shortcutmodel->vigencia = $dateVc;
                                            $shortcutmodel->anho_vigencia = $yy;
                                            $shortcutmodel->inspeccion = $s->id;
                                            $shortcutmodel->save();
                                        }
                                        break;
                                    case 'inspeccionProducto':
                                        $inspProducto = InspeccionesXProductos::findAll('inspeccion = :i AND aprobado = :a', array(':i' => $id, ':a' => 1));
                                        foreach ($inspProducto as $pr) {
                                            if ($pr->aprobado == 1) {
                                                $pr->aprobado_definitivo = 1;
                                                $pr->update();
                                            }
                                            $shortcutmodel = new ShortcutHomologacionesProductos();
                                            $shortcutmodel->homologacion = $inspeccion->homologacion0->id;
                                            $shortcutmodel->producto = $pr->producto0->id;
                                            $shortcutmodel->certificado = $inspeccion->numero_control . '-' . $pr->numero;
                                            $shortcutmodel->emision = date('Y-m-d');
                                            $shortcutmodel->vigencia = $dateVc;
                                            $shortcutmodel->anho_vigencia = $yy;
                                            $shortcutmodel->inspeccion = $inspeccion->id;
                                            $shortcutmodel->save();
                                        }
                                        break;
                                    case 'inspeccionDac':
                                        $inspDac = InspeccionesXDacs::findAll('inspeccion = :i AND aprobado = :a', array(':i' => $id, ':a' => 1));
                                        foreach ($inspDac as $d) {
                                            if ($d->aprobado == 1) {
                                                $d->aprobado_definitivo = 1;
                                                $d->update();
                                            }
                                            $shortcutmodel = new ShortcutHomologacionesDacs();
                                            $shortcutmodel->homologacion = $inspeccion->homologacion0->id;
                                            $shortcutmodel->dac = $d->dac0->id;
                                            $shortcutmodel->certificado = $inspeccion->numero_control . '-' . $d->numero;
                                            $shortcutmodel->emision = date('Y-m-d');
                                            $shortcutmodel->vigencia = $dateVc;
                                            $shortcutmodel->anho_vigencia = $yy;
                                            $shortcutmodel->inspeccion = $d->id;
                                            //$shortcutmodel->save();
                                        }
                                        break;
                                    case 'inspeccionMIT':
                                        $inspMIT = InspeccionesXMediosIzado::findAll('inspeccion = :i AND aprobado = :a', array(':i' => $id, ':a' => 1));

                                        foreach ($inspMIT as $m) {
                                            if ($m->aprobado == 1) {
                                                $m->aprobado_definitivo = 1;
                                                $m->update();
                                            }
                                            $shortcutmodel = new ShortcutHomologacionesMediosIzado();
                                            $shortcutmodel->homologacion = $inspeccion->homologacion0->id;
                                            $shortcutmodel->medio_izaje = $m->medioIzado->id;
                                            $shortcutmodel->certificado = $inspeccion->numero_control . '-' . $m->numero;
                                            $shortcutmodel->emision = date('Y-m-d');
                                            $shortcutmodel->vigencia = $dateVc;
                                            $shortcutmodel->anho_vigencia = $yy;
                                            $shortcutmodel->inspeccion = $m->id;
                                            $shortcutmodel->save();
                                        }
                                        break;
                                }
                            }
                        } else {
                            $inspeccion->estado = 1;
                            $inspeccion->update();
                        }
                        Cartero::enviarNotificacionAprobacion('Informe aprobado', $formType, $inspeccion, 'mail_IPAP');
                        Traceador::crearTraza('Aprobación', 'Aprobada la inspección ' . $inspeccion->numero_control . ' por: ' . $funcionario->nombre_apellidos);
                    } elseif ($model->rechazada == 1) {
                        $sp = AprobacionXFuncionarios::findAll('inspeccion = :isp AND funcionario != :f', array(':isp' => $inspeccion->id, ':f' => $funcionario->id));
                        if ($sp != null) {
                            foreach ($sp as $aprobacion) {
                                $aprobacion->valida = 0;
                                $aprobacion->update();
                            }
                        }
                        $inspeccion->cerrada = 0;
                        $inspeccion->estado = 3;
                        $inspeccion->update();
                        Traceador::crearTraza('Rechazo informe', 'No Aprobada la inspeccion ' . $inspeccion->numero_control);

                        Cartero::enviarNotificacionNoAprobacion('Informe de Inspección Rechazado', $formType, $inspeccion, 'mail_IPAP');

                        Yii::$app->session->setFlash('warning', 'Usted rechazó esta inspección');
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        return $this->render('aprobaciones', array('inspeccion' => $inspeccion, 'formType' => $formType, 'model' => $model));
    }
}
