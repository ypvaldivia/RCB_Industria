<?php

namespace app\controllers;

use app\components\Traceador;
use app\forms\BuscaObjetoForm;
use app\models\AprobacionXFuncionarios;
use app\models\Inspecciones;
use app\models\InspeccionesXPersonas;
use app\models\PersonasAcreditadas;
use app\models\ShortcutHomologacionesPersonas;
use app\models\VisitasInspecciones;
use Exception;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

class PersonasAcreditadasController extends Controller
{
    public $layout = 'main';

    //public $freeAccess = true;
    public $freeAccessActions = ['actionError', 'actionLogin'];

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' =>
                'webvimark\modules\UserManagement\components\GhostAccessControl'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ]
        ];
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_personasacreditadas_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $form = new BuscaObjetoForm();
        if (isset($_POST['BuscaObjetoForm'])) {
            $form->setMyAttributes($_POST['BuscaObjetoForm']);
            $criteria = $form->execSearchModelPersonas();
            $data = PersonasAcreditadas::find($criteria->condition, $criteria->params);
        } else {
            //$data = PersonasAcreditadas::findAll($criteria->condition, $criteria->params);
            $data = PersonasAcreditadas::find()->orderBy('nombre_apellidos ASC');
        }
        $model = PersonasAcreditadas::find();
        $countQuery = clone $model;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pages->pageSize = 20;
        $data = $model->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('index', [
            'data' => $data,
            'pages' => $pages,
            'model' => $form
        ]);
    }

    public function actionHistorical($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_personasacreditadas_historical')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }

        //ToDo: revisar que el no de inspeccion coincida
        $data = $this->loadModel($id);
        $certificaciones = ShortcutHomologacionesPersonas::findAll('persona = :p', array(':p' => $id));
        return $this->render('historico', array('data' => $data, 'certificaciones' => $certificaciones));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_personasacreditadas_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new PersonasAcreditadas();
        $path_picture = realpath(Yii::getAlias('@web/uploads/images/personas')) . '/';

        if (isset($_POST['PersonasAcreditadas'])) {
            $model->setAttributes($_POST['PersonasAcreditadas']);

            if (isset($_POST['PersonasAcreditadas']['entidad0'])) {
                $model->entidad = $_POST['PersonasAcreditadas']['entidad0'];
            }
            if (isset($_POST['PersonasAcreditadas']['provincia0'])) {
                $model->provincia = $_POST['PersonasAcreditadas']['provincia0'];
            }

            $rnd = rand(0, 9999);
            $uploadedFile = UploadedFile::getInstance($model, 'foto');

            try {
                if (!empty($uploadedFile)) {
                    $fileName = 'persona-' . "{$rnd}" . '.' . pathinfo($uploadedFile, PATHINFO_EXTENSION);
                    $uploadedFile->saveAs($path_picture . $fileName);
                    $model->foto = $fileName;
                } else {
                    $model->foto = null;
                }

                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Se adicionó un elemento exitosamente.');
                    Traceador::crearTraza('Creacion', 'Nueva persona acreditada : ' . $model->nombre_apellidos);
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['PersonasAcreditadas'])) {
            $model->attributes = $_GET['PersonasAcreditadas'];
        }
        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_personasacreditadas_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $tmpmodel = $this->loadModel($id);
        $path_picture = realpath(Yii::getAlias('@web/uploads/images/personas')) . '/';

        if (isset($_POST['PersonasAcreditadas'])) {
            $model->setAttributes($_POST['PersonasAcreditadas']);
            $model->entidad = $_POST['PersonasAcreditadas']['entidad'];
            $model->provincia = $_POST['PersonasAcreditadas']['provincia'];

            $rnd = rand(0, 9999);
            $uploadedFile = UploadedFile::getInstance($model, 'foto');
            try {
                if (!empty($uploadedFile)) {
                    //si el campo de la imagen está vacio o es null

                    if ($model->foto != null) {
                        unlink($path_picture . $model->foto);
                    }
                    $fileName = 'persona-' . "{$rnd}" . '.' . pathinfo($uploadedFile, PATHINFO_EXTENSION);
                    $uploadedFile->saveAs($path_picture . $fileName);
                    $model->foto = $fileName;
                } else {
                    $model->foto = $tmpmodel->foto;
                }

                if ($model->save()) {
                    Traceador::crearTraza('Edicion', 'Editada la persona acreditada : ' . $model->nombre_apellidos);
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        }

        return $this->render('update', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = PersonasAcreditadas::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'personas-acreditadas-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_personasacreditadas_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $band = false;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $model = $this->loadModel($id);
                    try {
                        $tmpInsp = InspeccionesXPersonas::findAll('persona = :p', array(':p' => $model->id));
                        if ($tmpInsp != null) {
                            foreach ($tmpInsp as $i) {
                                $tmpCertificados = ShortcutHomologacionesPersonas::findAll('inspeccion = :insp', array(':insp' => $i->id));
                                if ($tmpCertificados != null) {
                                    foreach ($tmpCertificados as $certificado) {
                                        $certificado->delete();
                                    }
                                }

                                /*
                                 * Antes de borrar el elemento de Inspeccion por Persona debe comprobarse que no es el último de la
                                 * inspección padre y si lo es deben borrarse tanto la inspección como los elementos asociados
                                 * de visitas y aprobación por niveles
                                 */

                                $father = $i->inspeccion0->id;
                                $counter = InspeccionesXPersonas::findAll('inspeccion = :f AND persona != :mi', array(':f' => $father, ':mi' => $model->id))->count();
                                if ($counter == 0) {
                                    $visitasAsoc = VisitasInspecciones::findAll('inspeccion = :f', array(':f' => $father));
                                    if ($visitasAsoc != null) {
                                        foreach ($visitasAsoc as $v) {
                                            $v->delete();
                                        }
                                    }
                                    $aprobaciones = AprobacionXFuncionarios::findAll('inspeccion = :f', array(':f' => $father));
                                    if ($aprobaciones != null) {
                                        foreach ($aprobaciones as $a) {
                                            $a->delete();
                                        }
                                    }
                                }
                                $i->delete();
                                Inspecciones::delete($father);
                            }
                        }
                        $model->delete();
                        Traceador::crearTraza('Eliminacion', 'Persona: ' . $model->nombre_apellidos);
                        $successCount++;
                    } catch (Exception $e) {
                        $band = true;
                        Yii::$app->session->setFlash('error', $e->getMessage());
                    }
                }
                if (!$band) {
                    Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
                }
            }
        } else {
            throw new HttpException(400, 'Petición no válida');
        }
    }

    public function actionSearchItems()
    {
        $search = $_GET['q'];
        $items = PersonasAcreditadas::findAll('nombre_apellidos LIKE :n OR ci LIKE :n', array(':n' => "%$search%"));
        if ($items != null) {
            foreach ($items as $key => $value) {
                $data[] = array('id' => $value['id'], 'text' => $value['nombre_apellidos'] . ' (' . $value['ci'] . ')');
            }
        } else {
            $data[] = array('id' => '0', 'text' => 'El elemento no existe');
        }

        echo json_encode($data);
    }
}
