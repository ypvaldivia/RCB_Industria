<?php

namespace app\controllers;

use app\components\Traceador;
use app\models\Inspecciones;
use app\models\Oficinas;
use Exception;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;

class OficinasController extends Controller
{
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'estructura';
        Yii::$app->session['submenu'] = '';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'create', 'update', 'batchDelete'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_oficinas_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $data = Oficinas::find()->all();
        return $this->render('index', array('data' => $data));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_oficinas_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new Oficinas();
        if (isset($_POST['Oficinas'])) {
            $model->setAttributes($_POST['Oficinas']);

            if (isset($_POST['Oficinas']['oficinaSuperior'])) {
                $model->oficinaSuperior = $_POST['Oficinas']['oficinaSuperior'];
            }
            if (isset($_POST['Oficinas']['provincia0'])) {
                $model->provincia0 = $_POST['Oficinas']['provincia0'];
            }

            try {
                if ($model->save()) {
                    Traceador::crearTraza('Creacion', 'Oficina : ' . $model->nombre);
                    Yii::$app->session->setFlash('success', 'Se adicionó un elemento exitosamente.');
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['Oficinas'])) {
            $model->attributes = $_GET['Oficinas'];
        }
        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_oficinas_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);

        if (isset($_POST['Oficinas'])) {
            $model->setAttributes($_POST['Oficinas']);
            $model->oficinaSuperior = $_POST['Oficinas']['oficinaSuperior'];
            $model->provincia0 = $_POST['Oficinas']['provincia0'];
            try {
                if ($model->save()) {
                    Traceador::crearTraza('Edicion', 'Oficina : ' . $model->nombre);
                    Yii::$app->session->setFlash('success', 'Se actualizó el elemento exitosamente.');
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                $model->addError('id', $e->getMessage());
            }
        }
        return $this->render('update', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = Oficinas::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'La página solicitada no existe.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'oficinas-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_oficinas_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $model = $this->loadModel($id);
                    $checkInsp = Inspecciones::findAll('oficina = :o', array(':o' => $model->id));
                    if ($checkInsp != null) {
                        Yii::$app->session->setFlash(
                            'error',
                            'Existen ' . count($checkInsp) . ' inspecciones vinculadas a la oficina ' . $model->codigo . ' que debe modificar antes de eliminar este elemento'
                        );
                        $successCount = 0;
                        break;
                    } else {
                        $model->delete();
                        Traceador::crearTraza('Eliminacion', 'Oficina : ' . $model->nombre);
                        ++$successCount;
                    }
                }
                if ($successCount > 0) {
                    Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' elementos exitosamente');
                }
            }
        } else {
            throw new HttpException(400, 'Petición no válida');
        }
    }

    public function actions()
    {
        return array(
            'toggle' => array(
                'class' => 'bootstrap.actions.TbToggleAction',
                'modelName' => 'Oficinas',
            ),
        );
    }
}
