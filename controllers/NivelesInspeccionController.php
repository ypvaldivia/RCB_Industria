<?php

namespace app\controllers;

use app\components\Traceador;
use app\models\AcreditacionesIndustriales;
use app\models\NivelesInspeccion;
use Exception;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;

class NivelesInspeccionController extends Controller
{
    //public $layout='main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'inspecciones';
        Yii::$app->session['submenu'] = '';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'create', 'update', 'batchDelete'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_nivelesinspeccion_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $data = NivelesInspeccion::find()->all();
        return $this->render('index', array('data' => $data));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_nivelesinspeccion_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new NivelesInspeccion();
        if (isset($_POST['NivelesInspeccion'])) {
            $model->setAttributes($_POST['NivelesInspeccion']);

            if (isset($_POST['NivelesInspeccion']['cargoCertificadoDefinitivo'])) {
                $model->cargoCertificadoDefinitivo = $_POST['NivelesInspeccion']['cargoCertificadoDefinitivo'];
            }

            try {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Se adicionó un elemento exitosamente.');
                    Traceador::crearTraza('Creacion', 'Nivel de inspeccion: ' . $model->nivel);
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['NivelesInspeccion'])) {
            Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
            $model->attributes = $_GET['NivelesInspeccion'];
        }
        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_nivelesinspeccion_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);

        if (isset($_POST['NivelesInspeccion'])) {
            $model->setAttributes($_POST['NivelesInspeccion']);
            $model->cargoCertificadoDefinitivo = $_POST['NivelesInspeccion']['cargoCertificadoDefinitivo'];
            try {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Se actualizó el elemento exitosamente.');
                    Traceador::crearTraza('Edicion', 'Nivel de inspeccion: ' . $model->nivel);
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
                $model->addError('id', $e->getMessage());
            }
        }
        return $this->render('update', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = NivelesInspeccion::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'La página solicitada no existe.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'niveles-inspeccion-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_nivelesinspeccion_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $model = $this->loadModel($id);
                    $tmpAc = AcreditacionesIndustriales::findAll('niveles_inspeccion_id = :n', array(':n' => $model->id));
                    if ($tmpAc != null) {
                        Yii::$app->session->setFlash(
                            'error',
                            'Existen ' . count($tmpAc) . ' acreditaciones industriales vinculadas al nivel ' . $model->nivel . ' que debe modificar antes de eliminar este elemento'
                        );
                        $successCount = 0;
                        break;
                    } else {
                        $model->delete();
                        Traceador::crearTraza('Eliminacion', 'Nivel de inspeccion: ' . $model->nivel);
                        ++$successCount;
                    }
                }
                if ($successCount > 0) {
                    Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
                }
            }
        } else {
            throw new HttpException(400, 'Petición no válida');
        }
    }

    public function actions()
    {
        return array(
            'toggle' => array(
                'class' => 'bootstrap.actions.TbToggleAction',
                'modelName' => 'NivelesInspeccion',
            ),
        );
    }
}
