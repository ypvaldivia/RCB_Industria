<?php

namespace app\controllers;

use yii\web\Controller;
use app\components\CDbCriteria;
use app\models\ShortcutHomologacionesDacs;
use app\models\ShortcutHomologacionesMediosIzado;
use app\models\ShortcutHomologacionesPersonas;
use app\models\ShortcutHomologacionesProductos;
use app\models\ShortcutHomologacionesServiciosProductos;
use Yii;

/**
 * Created by PhpStorm.
 * User: G-UX
 * Date: 16/12/2017
 * Time: 11:56 AM
 */

class ActualizadorController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for read operations
            array(
                'ext.starship.RestfullYii.filters.ERestFilter + REST.GET, REST.OPTIONS'
            ),
        );
    }

    public function actions()
    {
        return array(
            'REST.' => 'ext.starship.RestfullYii.actions.ERestActionProvider',
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow', 'actions' => array('REST.GET', 'REST.OPTIONS'),
                'users' => array('*'),
            ),
            array(
                'allow', 'actions' => array('descargarActualizacion'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    private function apiPersonas()
    {
        $result = array();
        $criteria = new CDbCriteria();
        $criteria->select = 'certificado, persona, homologacion, emision, vigente';
        $criteria->join = 'inner join personas_acreditadas on t.persona = personas_acreditadas.id';
        $criteria->distinct = true;
        $criteria->condition = 'vigente > :actual';
        $criteria->params = array(':actual' => date('Y-m-d'));
        $criteria->order = 'vigente DESC';
        $personas = ShortcutHomologacionesPersonas::findAll($criteria->condition, $criteria->params);
        if ($personas != null) {
            foreach ($personas as $p) {
                $result[] = array(
                    'nombre' => $p->persona0->nombre_apellidos,
                    'certificado' => $p->certificado,
                    'provincia' => ($p->persona0->provincia0 != null) ? $p->persona0->provincia0->nombre : "No definida",
                    'entidad' => ($p->persona0->entidad0 != null) ? $p->persona0->entidad0->nombre : "No definida",
                    'homologacion' => $p->homologacion0->titulo_anuario,
                    'emision' => $p->emision,
                    'vigencia' => $p->vigente,
                    'id' => $p->id,
                    'id_persona' => $p->persona0->id,
                    'id_provincia' => ($p->persona0->provincia0 != null) ? $p->persona0->provincia0->id : -1,
                    'id_entidad' => ($p->persona0->entidad0 != null) ? $p->persona0->entidad0->id : -1,
                    'id_homologacion' => $p->homologacion0->id,
                );
            }
        }
        return $result;
    }


    private function apiServicios()
    {
        $result = array();
        $criteria = new CDbCriteria();
        $criteria->select = 't.certificado, t.servicio, homologacion, t.emision, t.vigencia';
        $criteria->join = 'inner join servicios_laboratorios_acreditados on t.servicio = servicios_laboratorios_acreditados.id';
        $criteria->distinct = true;
        $criteria->condition = 't.vigencia > :actual';
        $criteria->params = array(':actual' => date('Y-m-d'));
        $criteria->order = 't.vigencia DESC';
        $servicios = ShortcutHomologacionesServiciosProductos::findAll($criteria->condition, $criteria->params);
        if ($servicios != null) {
            foreach ($servicios as $p) {
                $result[] = array(
                    'nombre' => trim(strip_tags($p->servicio0->servicio)),
                    'certificado' => $p->certificado,
                    'provincia' => ($p->servicio0->provincia0 != null) ? $p->servicio0->provincia0->nombre : "No definido",
                    'entidad' => ($p->servicio0->entidad0 != null) ? $p->servicio0->entidad0->nombre : "No definido",
                    'departamento' => $p->servicio0->departamento,
                    'homologacion' => $p->homologacion0->titulo_anuario,
                    'emision' => $p->emision,
                    'vigencia' => $p->vigencia,
                    'id' => $p->id,
                    'id_servicio' => $p->servicio0->id,
                    'id_provincia' => ($p->servicio0->provincia0 != null) ? $p->servicio0->provincia0->id : -1,
                    'id_entidad' => ($p->servicio0->entidad0 != null) ? $p->servicio0->entidad0->id : -1,
                    'id_homologacion' => $p->homologacion0->id,
                );
            }
        }
        return $result;
    }

    private function apiProductos()
    {
        $result = array();
        $criteria = new CDbCriteria();
        $criteria->select = 't.certificado, producto, homologacion, t.emision, t.vigencia';
        $criteria->join = 'inner join productos_acreditados on t.producto = productos_acreditados.id';
        $criteria->distinct = true;
        $criteria->condition = 't.vigencia > :actual';
        $criteria->params = array(':actual' => date('Y-m-d'));
        $criteria->order = 't.vigencia DESC';
        $productos = ShortcutHomologacionesProductos::findAll($criteria->condition, $criteria->params);
        if ($productos != null) {
            foreach ($productos as $p) {
                $result[] = array(
                    'nombre' => $p->producto0->nombre,
                    'certificado' => $p->certificado,
                    'provincia' => ($p->producto0->provincia0 != null) ? $p->producto0->provincia0->nombre : "No definida",
                    'propietario' => ($p->producto0->propietario0 != null) ? $p->producto0->propietario0->nombre : "No definido",
                    'homologacion' => $p->homologacion0->titulo_anuario,
                    'emision' => $p->emision,
                    'vigencia' => $p->vigencia,
                    'id' => $p->id,
                    'id_producto' => $p->producto0->id,
                    'id_provincia' => ($p->producto0->provincia0 != null) ? $p->producto0->provincia0->id : -1,
                    'id_entidad' => ($p->producto0->propietario0 != null) ? $p->producto0->propietario0->id : -1,
                    'id_homologacion' => $p->homologacion0->id,
                );
            }
        }
        return $result;
    }

    private function apiDACs()
    {
        $result = array();
        $criteria = new CDbCriteria();
        $criteria->select = 't.certificado, dac, homologacion, t.emision, t.vigencia';
        $criteria->join = 'inner join dacs_acreditados on t.dac = dacs_acreditados.id';
        $criteria->distinct = true;
        $criteria->condition = 't.vigencia > :actual';
        $criteria->params = array(':actual' => date('Y-m-d'));
        $criteria->order = 't.vigencia DESC';
        $dacs = ShortcutHomologacionesDacs::findAll($criteria->condition, $criteria->params);
        if ($dacs != null) {
            foreach ($dacs as $p) {
                $result[] = array(
                    'nombre' => $p->dac0->nombre,
                    'certificado' => $p->certificado,
                    'tipo' => ($p->dac0->tipo0 != null) ? $p->dac0->tipo0->tipo : "No definido",
                    'provincia' => ($p->dac0->provincia0 != null) ? $p->dac0->provincia0->nombre : "No definida",
                    'propietario' => ($p->dac0->propietario0 != null) ? $p->dac0->propietario0->nombre : "No definido",
                    'ct_nominal' => $p->dac0->ct_nominal,
                    'ct_certificada' => $p->dac0->ct_certificada,
                    'homologacion' => $p->homologacion0->titulo_anuario,
                    'emision' => $p->emision,
                    'vigencia' => $p->vigencia,
                    'id' => $p->id,
                    'id_dac' => $p->dac0->id,
                    'id_provincia' => ($p->dac0->provincia0 != null) ? $p->dac0->provincia0->id : -1,
                    'id_entidad' => ($p->dac0->propietario0 != null) ? $p->dac0->propietario0->id : -1,
                    'id_homologacion' => $p->homologacion0->id,
                );
            }
        }
        return $result;
    }

    private function apiMITs()
    {
        $result = array();
        $criteria = new CDbCriteria();
        $criteria->select = 't.certificado, medio_izaje, homologacion, t.emision, t.vigencia';
        $criteria->join = 'inner join medios_izado_acreditados on t.medio_izaje = medios_izado_acreditados.id';
        $criteria->distinct = true;
        $criteria->condition = 't.vigencia > :actual';
        $criteria->params = array(':actual' => date('Y-m-d'));
        $criteria->order = 't.vigencia DESC';
        $mits = ShortcutHomologacionesMediosIzado::findAll($criteria->condition, $criteria->params);
        if ($mits != null) {
            foreach ($mits as $p) {
                $result[] = array(
                    'nombre' => $p->medioIzaje->nombre,
                    'certificado' => $p->certificado,
                    'tipo' => ($p->medioIzaje->tipo0 != null) ? $p->medioIzaje->tipo0->tipo : "No definido",
                    'provincia' => ($p->medioIzaje->provincia0 != null) ? $p->medioIzaje->provincia0->nombre : "No definida",
                    'propietario' => ($p->medioIzaje->propietario0 != null) ? $p->medioIzaje->propietario0->nombre : "No definido",
                    'categoria' => ($p->medioIzaje->categoria0 != null) ? $p->medioIzaje->categoria0->categoria : "No definida",
                    'ci_nominal' => $p->medioIzaje->ci_nominal,
                    'ci_certificada' => $p->medioIzaje->ci_certificada,
                    'ci_aux_nominal' => $p->medioIzaje->ci_aux_nominal,
                    'ci_aux_certificada' => $p->medioIzaje->ci_aux_certificada,
                    'ci_aux1_nominal' => $p->medioIzaje->ci_aux1_nominal,
                    'ci_aux1_certificada' => $p->medioIzaje->ci_aux1_certificada,
                    'homologacion' => $p->homologacion0->titulo_anuario,
                    'emision' => $p->emision,
                    'vigencia' => $p->vigencia,
                    'id' => $p->id,
                    'id_mit' => $p->medioIzaje->id,
                    'id_provincia' => ($p->medioIzaje->provincia0 != null) ? $p->medioIzaje->provincia0->id : -1,
                    'id_entidad' => ($p->medioIzaje->propietario0 != null) ? $p->medioIzaje->propietario0->id : -1,
                    'id_homologacion' => $p->homologacion0->id,
                );
            }
        }
        return $result;
    }

    private function createArray()
    {
        $result = array();
        $personas = $this->apiPersonas();
        $servicios = $this->apiServicios();
        $productos = $this->apiProductos();
        $dacs = $this->apiDACs();
        $mits = $this->apiMITs();
        $result[] = array('0' => $personas, '1' => $servicios, '2' => $productos, '3' => $dacs, '4' => $mits);
        echo json_encode($result);
    }

    public function restEvents()
    {
        $this->onRest('post.filter.req.auth.ajax.user', function () {
            if ($this->getAction()->getId() == 'REST.GET') {
                $data = $this->createArray();
                return $this->renderJSON([
                    'type'              => 'rest',
                    'data'              => $data,
                ]);
            }
        });
    }

    public function actionDescargarActualizacion()
    {
        $path = realpath(Yii::getAlias('@web') . '/uploads/files') . '/rcb_update.csv';
        $new_csv = fopen($path, 'w');
        $personas = $this->apiPersonas();
        $servicios = $this->apiServicios();
        $productos = $this->apiProductos();
        $dacs = $this->apiDACs();
        $mits = $this->apiMITs();
        $date = date('Y:m:d');
        fputcsv($new_csv, array('Fecha de Actualización', $date));
        $i = 0;
        fputcsv($new_csv, array('Personas'));
        fputcsv($new_csv, array('Nombre', 'Certificado', 'Provincia', 'Entidad', 'Homologacion', 'Emision', 'Vigencia', 'ID', 'ID Persona', 'ID Provincia', 'ID Entidad', 'ID Homologacion'));
        while ($i < count($personas)) {
            fputcsv($new_csv, $personas[$i]);
            $i++;
        }
        $i = 0;
        fputcsv($new_csv, array('Servicios'));
        fputcsv($new_csv, array('Nombre', 'Certificado', 'Provincia', 'Entidad', 'Departamento', 'Homologacion', 'Emision', 'Vigencia', 'ID', 'ID Servicio', 'ID Provincia', 'ID Entidad', 'ID Homologacion'));
        while ($i < count($servicios)) {
            fputcsv($new_csv, $servicios[$i]);
            $i++;
        }
        $i = 0;
        fputcsv($new_csv, array('Productos'));
        fputcsv($new_csv, array('Nombre', 'Certificado', 'Provincia', 'Propietario', 'Homologacion', 'Emision', 'Vigencia', 'ID', 'ID Producto', 'ID Provincia', 'ID Entidad', 'ID Homologacion'));
        while ($i < count($productos)) {
            fputcsv($new_csv, $productos[$i]);
            $i++;
        }
        $i = 0;
        fputcsv($new_csv, array('DACs'));
        fputcsv($new_csv, array('Nombre', 'Certificado', 'Tipo', 'Provincia', 'Propietario', 'CI Nominal', 'CI Certificada', 'Homologacion', 'Emision', 'Vigencia', 'ID', 'ID DAC', 'ID Provincia', 'ID Entidad', 'ID Homologacion'));
        while ($i < count($dacs)) {
            fputcsv($new_csv, $dacs[$i]);
            $i++;
        }
        $i = 0;
        fputcsv($new_csv, array('MITs'));
        fputcsv($new_csv, array('Nombre', 'Certificado', 'Tipo', 'Provincia', 'Propietario', 'Categoría', 'CI Nominal', 'CI Certificada', 'CI Auxiliar 1 Nominal', 'CI Auxiliar 1 Certificada', 'CI Auxiliar 2 Nominal', 'CI Auxiliar 2 Certificada', 'Homologacion', 'Emision', 'Vigencia', 'ID', 'ID MIT', 'ID Provincia', 'ID Entidad', 'ID Homologacion'));
        while ($i < count($mits)) {
            fputcsv($new_csv, $mits[$i]);
            $i++;
        }
        //fputcsv($new_csv, array('Column 1', 'Column 2', 'Column 3'));
        fclose($new_csv);

        // output headers so that the file is downloaded rather than displayed
        header("Content-type: text/csv");
        header("Content-disposition: attachment; filename = update.rcb");
        readfile($path);
    }
}
