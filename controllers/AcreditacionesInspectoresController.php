<?php

namespace app\controllers;

use app\components\Traceador;
use app\models\AcreditacionesInspectores;
use app\models\CategoriasAcreditaciones;
use app\models\Funcionarios;
use Exception;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;

class AcreditacionesInspectoresController extends Controller
{
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'update', 'batchDelete'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex($idInspector)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_acreditacionesinspectores_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new AcreditacionesInspectores();
        $inspector = Funcionarios::find($idInspector)->one();
        $data = AcreditacionesInspectores::findAll(['inspector' => $idInspector]);

        if (isset($_POST['AcreditacionesInspectores'])) {
            $model->setAttributes($_POST['AcreditacionesInspectores']);
            $y = substr($model->emision, 0, 4);
            $m = substr($model->emision, 5, 2);
            $d = substr($model->emision, 8, 2);
            $yy = $y + 5;
            $model->vencimiento = $yy . '-' . $m . '-' . $d;
            $model->inspector0 = $_POST['idinspector'];
            if (isset($_POST['AcreditacionesInspectores']['acreditacion0'])) {
                $model->acreditacion0 = $_POST['AcreditacionesInspectores']['acreditacion0'];
            }

            try {
                if ($model->save()) {
                    $lastAcred = CategoriasAcreditaciones::find($_POST['AcreditacionesInspectores']['acreditacion0'])->one();

                    Traceador::crearTraza(
                        'Creacion',
                        'Acreditacion de: ' . $inspector->nombre_apellidos . ' para: ' . $lastAcred->grupo
                    );
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('AcreditacionesInspectores/index&idInspector=' . $idInspector));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['AcreditacionesInspectores'])) {
            $model->attributes = $_GET['AcreditacionesInspectores'];
        }

        return $this->render('index', array('data' => $data, 'inspector' => $inspector, 'model' => $model));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_acreditacionesinspectores_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new AcreditacionesInspectores();
        $this->performAjaxValidation($model, 'acreditaciones-inspectores-form');
        if (isset($_POST['AcreditacionesInspectores'])) {
            $model->setAttributes($_POST['AcreditacionesInspectores']);
            $y = substr($model->emision, 0, 4);
            $m = substr($model->emision, 5, 2);
            $d = substr($model->emision, 8, 2);
            $yy = $y + 5;
            $model->vencimiento = $yy . '-' . $m . '-' . $d;

            if (isset($_POST['AcreditacionesInspectores']['inspector0'])) {
                $model->inspector0 = $_POST['AcreditacionesInspectores']['inspector0'];
            }
            if (isset($_POST['AcreditacionesInspectores']['acreditacion0'])) {
                $model->acreditacion0 = $_POST['AcreditacionesInspectores']['acreditacion0'];
            }

            try {
                if ($model->save()) {
                    Traceador::crearTraza(
                        'Adición',
                        'Acreditación de: ' . $model->inspector0->nombre_apellidos . ' para: ' . $model->acreditacion0->grupo
                    );
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        } elseif (isset($_GET['AcreditacionesInspectores'])) {
            $model->attributes = $_GET['AcreditacionesInspectores'];
        }

        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_acreditacionesinspectores_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);
        $this->performAjaxValidation($model, 'acreditaciones-inspectores-form');
        $inspector = Funcionarios::find($model->inspector)->one();

        if (isset($_POST['AcreditacionesInspectores'])) {
            $model->setAttributes($_POST['AcreditacionesInspectores']);
            $y = substr($model->emision, 0, 4);
            $m = substr($model->emision, 5, 2);
            $d = substr($model->emision, 8, 2);
            $yy = $y + 5;
            $model->vencimiento = $yy . '-' . $m . '-' . $d;
            //$model->inspector0 = $inspector->id;
            //$model->acreditacion0 = $_POST['AcreditacionesInspectores']['acreditacion0'];
            try {
                if ($model->save()) {
                    Traceador::crearTraza(
                        'Edición',
                        'Acreditación de: ' . $model->inspector0->nombre_apellidos . ' para: ' . $model->acreditacion0->grupo
                    );
                    return $this->redirect(array('AcreditacionesInspectores/index&idInspector=' . $inspector->id));
                }
            } catch (Exception $e) {
                $model->addError('', $e->getMessage());
            }
        }

        return $this->render('update', array('model' => $model, 'inspector' => $inspector));
    }

    public function actionDelete($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_acreditacionesinspectores_delete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        if (Yii::$app->request->isPostRequest) {
            try {
                $model = $this->loadModel($id);
                Traceador::crearTraza(
                    'Eliminación',
                    'Acreditación del inspector: ' . $model->inspector0->nombre_apellidos . ' para la homlogacion: ' . $model->acreditacion0->grupo
                );
                $model->delete();
            } catch (Exception $e) {
                throw new HttpException(500, $e->getMessage());
            }

            if (!Yii::$app->getRequest()->getIsAjaxRequest()) {
                return $this->redirect(array('index'));
            }
        } else {
            throw new HttpException(400, Yii::t('app', 'Petición inválida.'));
        }
    }

    public function loadModel($id)
    {
        $model = AcreditacionesInspectores::find($id)->one();
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'acreditaciones-inspectores-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_acreditacionesinspectores_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $this->loadModel($id)->delete();
                    ++$successCount;
                }
                Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
            }
        } else {
            throw new HttpException(400, 'Petición no válida');
        }
    }

    public function actions()
    {
        return array(
            'toggle' => array(
                'class' => 'bootstrap.actions.TbToggleAction',
                'modelName' => 'AcreditacionesInspectores',
            ),
        );
    }
}
