<?php

namespace app\controllers;

use Yii;
use Exception;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;
use app\components\Slugger;
use app\components\Traceador;
use app\models\AcreditacionesIndustriales;
use app\models\Anuario;
use app\models\ShortcutHomologacionesDacs;
use app\models\ShortcutHomologacionesMediosIzado;
use app\models\ShortcutHomologacionesPersonas;
use app\models\ShortcutHomologacionesProductos;
use app\models\ShortcutHomologacionesServiciosProductos;
use app\models\Inspecciones;
use webvimark\modules\UserManagement\models\User;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;

class AnuarioController extends Controller
{

    public function actionIndex()
    {
        if (!User::hasPermission('action_anuario_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        Yii::$app->session['menu'] = 'index';
        return $this->render('index');
    }

    public function actionEdicion()
    {
        if (!User::hasPermission('action_anuario_edicion')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        Yii::$app->session['menu'] = 'anuario';
        $data = Anuario::find()->orderBy(['orden' => SORT_ASC])->all();

        Traceador::crearTraza('acceso', 'Edicion de anuario');

        // probando mailer

        Yii::$app->session['menu'] = 'edicion';
        return $this->render('edicion', ['data' => $data]);
    }

    public function actionCreate()
    {
        if (!User::hasPermission('action_anuario_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }

        Yii::$app->session['menu'] = 'anuario';
        $model = new Anuario();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setAttributes($_POST['Anuario']);

            try {
                if ($model->save()) {
                    Yii::$app->session->setFlash('success', 'Se adicionó un elemento exitosamente.');
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('slug', $e->getMessage());
            }
        } elseif (isset($_GET['Anuario'])) {
            Yii::$app->session->setFlash('error', 'Ha ocurrido un error.');
            $model->attributes = $_GET['Anuario'];
        }

        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!User::hasPermission('action_anuario_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        Yii::$app->session['menu'] = 'anuario';
        $model = $this->loadModel($id);

        $fulldir = Yii::getAlias('@webroot/uploads') . $model->dir;

        if (substr_count($fulldir, 'personas') > 0) {
            $tipo = 'personas';
            $ruta = $fulldir . '/enc/';
        }
        if (substr_count($fulldir, 'productos') > 0) {
            $tipo = 'productos';
            $ruta = $fulldir . '/enc/';
        }
        if (substr_count($fulldir, 'servicios') > 0) {
            $tipo = 'servicios';
            $ruta = $fulldir . '/enc/';
        }
        if (substr_count($fulldir, 'mits') > 0) {
            $tipo = 'mits';
            $ruta = $fulldir . '/enc/';
        }
        if (substr_count($fulldir, 'dacs') > 0) {
            $tipo = 'dacs';
            $ruta = $fulldir . '/enc/';
        }
        if (substr_count($fulldir, 'secciones') > 0) {
            $tipo = 'secciones';
            $ruta = $fulldir;
        }
        $contenido = file_get_contents($ruta . $model->archivo);
        if (isset($_POST['encabezado']) && isset($_POST['archivo'])) {

            //reconstruyendo el parcial HTML
            //sacar el slug segun el nombre del encabezado
            $slug = preg_replace('/\\.[^.\\s]{3,4}$/', '', $_POST['archivo']);
            //obteniendo el objeto de acreditacion segun el slug
            $acreditacion = AcreditacionesIndustriales::find('slug = :slug', array(':slug' => $slug));
            file_put_contents($ruta . $model->archivo, $_POST['encabezado']);

            //Corregir el orden
            $element = Anuario::findOne(['orden' => $_POST['orden']]);
            if ($element != null) {
                $model->orden = $element->orden + 1;
                $model->update();
                $toModify = Anuario::findAll(
                    ['AND', ['>=', 'orden', $model->orden], ['<>', 'id', $model->id]]
                );
                if ($toModify != null) {
                    foreach ($toModify as $e) {
                        $e->orden = $e->orden + 1;
                        $e->update();
                    }
                }
            }

            Yii::$app->session->setFlash('success', 'Editado el fragmento correctamente ');
            Traceador::crearTraza('edicion', 'Fragmento de anuario: ' . $model->nombre);
            return $this->redirect(array('edicion'));
        }
        return $this->render('update', array('model' => $model, 'contenido' => $contenido));
    }

    public function actionDelete($id)
    {
        if (!User::hasPermission('action_anuario_delete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        Yii::$app->session['menu'] = 'anuario';
        try {
            $fila = $this->loadModel($id);
            if ($fila->archivo != null && $fila->dir != null) {
                $path_file = realpath(Yii::getAlias('@webroot') . $fila->dir . $fila->archivo);
                if (file_exists($path_file)) {
                    unlink($path_file); // delete file
                }
            }
            $orden = $fila->orden + 1;
            $nextOnes = Anuario::findAll(['>=', 'orden', $orden]);
            if ($nextOnes != null) {
                foreach ($nextOnes as $n) {
                    $n->orden = $n->orden - 1;
                    $n->update();
                }
            }
            $fila->delete();
            Traceador::crearTraza('eliminacion', 'Fragmento anuario: ' . $fila->nombre);
            Yii::$app->session->setFlash('success', 'El fragmento ha sido eliminado satisfactoriamente.');
        } catch (Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }

        if (!Yii::$app->getRequest()->isAjax()) {
            return $this->redirect('edicion');
        }
    }

    public function loadModel($id)
    {
        $model = Anuario::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'La página solicitada no existe.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'anuario-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    /**
     * Funcion encargada de renderiar el administrador de anuario(pendiente a mejorar).
     */
    public function actionAnuario()
    {
        if (!User::hasPermission('action_anuario_anuario')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'main';
        Yii::$app->session['menu'] = 'anuario';

        return $this->render('anuario');
    }

    public function actionDescargar()
    {
        if (!User::hasPermission('action_anuario_descargar')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'main';
        Yii::$app->session['menu'] = 'anuario';
        Traceador::crearTraza('acceso', 'Descargar anuario ');
        return $this->render('descargar');
    }

    public function actionPrintOne($id)
    {
        if (!User::hasPermission('action_anuario_printone')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }

        $this->layout = 'main';
        $archivosAnuario = Anuario::findOne($id);
        $pdf = new Pdf();
        $mpdf = $pdf->api;  // L - landscape, P - portrait

        //$mpdf->SetDisplayMode('fullpage');
        $mpdf->SetCompression(true);
        //$mpdf->useOnlyCoreFonts = true;
        $mpdf->SetTitle('Parcial de anuario ' . $archivosAnuario->nombre); //Titulo de la ventana
        $mpdf->SetSubject('Parcial de anuario');
        $mpdf->SetAuthor('RCB, Sociedad Clasificadora'); //Titulo del autor del archivo
        $mpdf->SetWatermarkImage(
            Yii::getAlias('@webroot/img/bg-anuario.jpg'),
            0.1,
            '',
            [60, 100]
        );
        $mpdf->showWatermarkImage = true;

        $mpdf->SetHTMLHeader(
            '<div><p style="  text-align:left; border-bottom:2px solid black "> <strong>R.C.B </strong> Registro Cubano de Buques</p></div>'
        );
        $mpdf->SetHTMLFooter(
            '<div style="color:black;   text-align:right; border-top:1px solid black " ><span style="color:black; text-align:left; color: #0f4e74 !important;" > RCB <i> Sociedad Clasificadora<i/>  </span> <span style="color:black;" > | {PAGENO}</span> </div>'
        );
        $stylesheet = file_get_contents(Yii::getAlias('@webroot/css/anuario.css'));
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->addPage();
        $mpdf->WriteHTML(file_get_contents(Yii::getAlias('@webroot/uploads') . $archivosAnuario->dir . $archivosAnuario->archivo));

        Traceador::crearTraza('Impresion', 'Fragmento anuario: ' . $archivosAnuario->nombre);
        //aki se debe notificar por correo la generacion del anuario
        $mpdf->Output($archivosAnuario->nombre . '.pdf', 'D');
    }

    /**
     * Funcion encargada de generar el anuario teniendo en cuenta todos los fragmentos HTML obtenidos de la tabla anuario segun su orden.
     * Primaramente importa un pdf con la portada y una pagina para la creacion del indice.
     */
    public function actionContruirAnuario()
    {
        if (!User::hasPermission('action_anuario_construiranuario')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }

        $this->layout = 'main';
        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', '1200');

        $pdf = new Pdf();
        $mpdf = $pdf->api;

        //$mpdf->SetImportUse();
        //$mpdf->SetDisplayMode('fullpage');
        $mpdf->SetCompression(true);
        //$mpdf->useOnlyCoreFonts = true;
        $mpdf->SetTitle(
            'Registro de Homologación y Certificación Industrial de Personas, Productos y Servicios'
        ); //Titulo de la ventana
        $mpdf->SetSubject('Registro de Homologación y Certificación Industrial de Productos, Personas y Servicios');
        $mpdf->SetAuthor('RCB, Sociedad Clasificadora'); //Titulo del autor del archivo
        $mpdf->SetWatermarkImage(
            Yii::getAlias('@webroot/img/bg-anuario.jpg'),
            0.1,
            '',
            array(60, 100)
        );
        $mpdf->showWatermarkImage = true;
        $mpdf->SetProtection(array('copy', 'print'));
        $m = date('Y-m-d');
        $repo = new Inspecciones();
        $cm = $repo->convertirFecha($m);
        $header = '<div class="row" style="border-bottom:2px solid black ">
                        <table style="border: none; width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: left;">
                                    <strong>R.C.B </strong>
                                    Registro Cubano de Buques
                                </td>
                                <td style="width: 50%; text-align: right;">
                                    Hasta el ' . $cm . '
                                </td>
                            </tr>
                        </table>
                  </div>';
        $footer = '<div class="row" style="border-top:2px solid black ">
                        <table style="border: none; width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: left;">
                                    Hasta el ' . $cm . '
                                </td>
                                <td style="width: 50%; text-align: right;">
                                    <span style="color:black; text-align:left; color: #0f4e74 !important;" > RCB <i> Sociedad Clasificadora<i/>
                                    </span> <span style="color:black;" > | {PAGENO}</span>
                                </td>
                            </tr>
                        </table>
                  </div>';
        $mpdf->SetHTMLHeader($header);
        $mpdf->SetHTMLFooter($footer);
        $stylesheet = file_get_contents(Yii::getAlias('@webroot/css/anuario.css'));
        $mpdf->WriteHTML($stylesheet, 1);
        //obteniendo la primera parte del anuario
        $add1 = $mpdf->SetSourceFile(Yii::getAlias('@webroot/uploads/anuario/portada.pdf'));
        for ($i = 1; $i <= $add1; ++$i) {
            if ($i == 3) {
                $mpdf->h2toc = array('H1' => 0, 'H2' => 1, 'H3' => 2);
                $mpdf->h2bookmarks = array('H1' => 0, 'H2' => 1, 'H3' => 2);
                $mpdf->TOCpagebreakByArray(
                    array(
                        'toc-preHTML' => '<h2>Tabla de contenidos</h2><br/>',
                        'toc-bookmarkText' => 'Tabla de contenidos',

                    )
                );
            }
            $import_page = $mpdf->ImportPage($i);
            $mpdf->UseTemplate($import_page);

            if ($i < $add1) {
                $mpdf->AddPage();
            }
        }
        $archivosAnuario = Anuario::find()->orderBy(['orden' => SORT_ASC])->all();

        if ($archivosAnuario !== null) {
            foreach ($archivosAnuario as $archivo) {
                if (substr_count($archivo->dir, 'secciones') == 0) {
                    $mpdf->AddPage();
                }
                $mpdf->WriteHTML(file_get_contents(Yii::getAlias('@webroot/uploads') . $archivo->dir . $archivo->archivo));
            }
        }

        if (file_exists(
            Yii::getAlias('@webroot/uploads/anuario/Anuario-Homologacion-Aprobacion-') . date('Y') . '.pdf'
        )) {
            unlink(Yii::getAlias('@webroot/uploads/anuario/Anuario-Homologacion-Aprobacion-') . date('Y') . '.pdf');
        }

        //aki se debe notificar por correo la generacion del anuario
        $mpdf->Output(
            Yii::getAlias('@webroot/uploads/anuario/Anuario-Homologacion-Aprobacion-') . date('Y') . '.pdf',
            'F'
        );
        Traceador::crearTraza('Contruccion', 'Anuario');
        return $this->redirect(array('descargar'));
    }

    /**
     * Funcion encargada de generar el anuario teniendo en cuenta todos los fragmentos HTML obtenidos de la tabla anuario segun su orden.
     * Primaramente importa un pdf con la portada y una pagina para la creacion del indice.
     */
    public function actionContruirAnuarioMits()
    {
        if (!User::hasPermission('action_anuario_construiranuariomits')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $this->layout = 'main';
        ini_set('memory_limit', '1024M');

        $pdf = new Pdf();
        $mpdf = $pdf->api;

        //$mpdf->SetImportUse();
        //$mpdf->SetDisplayMode('fullpage');
        $mpdf->SetCompression(true);
        //$mpdf->useOnlyCoreFonts = true;
        $mpdf->SetTitle(
            'Registro de Homologación y Certificación Industrial de Medios de Izado'
        ); //Titulo de la ventana
        $mpdf->SetSubject('Registro de Homologación y Certificación Industrial de Medios de Izado');
        $mpdf->SetAuthor('RCB, Sociedad Clasificadora'); //Titulo del autor del archivo
        $mpdf->SetWatermarkImage(
            Yii::getAlias('@webroot/img/bg-anuario.jpg'),
            0.1,
            '',
            array(60, 100)
        );
        $mpdf->showWatermarkImage = true;
        $mpdf->SetProtection(array('copy', 'print'));
        $m = date('Y-m-d');
        $repo = new Inspecciones();
        $cm = $repo->convertirFecha($m);
        $header = '<div class="row" style="border-bottom:2px solid black ">
                        <table style="border: none; width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: left;">
                                    <strong>R.C.B </strong>
                                    Registro Cubano de Buques
                                </td>
                                <td style="width: 50%; text-align: right;">
                                    Hasta el ' . $cm . '
                                </td>
                            </tr>
                        </table>
                  </div>';
        $footer = '<div class="row" style="border-top:2px solid black ">
                        <table style="border: none; width: 100%">
                            <tr>
                                <td style="width: 50%; text-align: left;">
                                    Hasta el ' . $cm . '
                                </td>
                                <td style="width: 50%; text-align: right;">
                                    <span style="color:black; text-align:left; color: #0f4e74 !important;" > RCB <i> Sociedad Clasificadora<i/>
                                    </span> <span style="color:black;" > | {PAGENO}</span>
                                </td>
                            </tr>
                        </table>
                  </div>';
        $mpdf->SetHTMLHeader($header);
        $mpdf->SetHTMLFooter($footer);
        $stylesheet = file_get_contents(Yii::getAlias('@webroot/css/anuario.css'));
        $mpdf->WriteHTML($stylesheet, 1);
        //obteniendo la primera parte del anuario
        $add1 = $mpdf->SetSourceFile(Yii::getAlias('@webroot/uploads/anuario/portadamits.pdf'));
        for ($i = 1; $i <= $add1; ++$i) {
            if ($i == 3) {
                $mpdf->h2toc = array('H1' => 0, 'H2' => 1, 'H3' => 2);
                $mpdf->h2bookmarks = array('H1' => 0, 'H2' => 1, 'H3' => 2);
                $mpdf->TOCpagebreakByArray(
                    array(
                        'toc-preHTML' => '<h2>Tabla de contenidos</h2><br/>',
                        'toc-bookmarkText' => 'Tabla de contenidos',

                    )
                );
            }
            $import_page = $mpdf->ImportPage($i);
            $mpdf->UseTemplate($import_page);

            if ($i < $add1) {
                $mpdf->AddPage();
            }
        }
        $archivosAnuario = Anuario::find()->where('mit = 1')->orderBy(['orden' => SORT_ASC])->all();

        if ($archivosAnuario !== null) {
            foreach ($archivosAnuario as $archivo) {
                if (substr_count($archivo->dir, 'secciones') == 0) {
                    $mpdf->AddPage();
                }
                $mpdf->WriteHTML(file_get_contents(Yii::getAlias('@webroot/uploads') . $archivo->dir . $archivo->archivo));
            }
        }

        if (file_exists(
            Yii::getAlias('@webroot/uploads/anuario/Anuario-MITs-') . date('Y') . '.pdf'
        )) {
            unlink(Yii::getAlias('@webroot/uploads/anuario/Anuario-MITs-') . date('Y') . '.pdf');
        }

        //aki se debe notificar por correo la generacion del anuario
        $mpdf->Output(
            Yii::getAlias('@webroot/uploads/anuario/Anuario-MITs-') . date('Y') . '.pdf',
            'F'
        );
        Traceador::crearTraza('Contruccion', 'Anuario para MITs');
        return $this->redirect(array('descargar'));
    }

    /** Funcion para crear los encabezados de secciones para la confeccion del anuario
     *
     */
    public function actionCrearEncabezados()
    {
        if (!User::hasPermission('action_anuario_crearencabezados')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $listAnuario =  ArrayHelper::map(Anuario::find()->orderBy(['orden' => SORT_ASC])->all(), 'orden', 'nombre');
        ini_set('memory_limit', '1024M');
        //obteniendo el directorio segun el tipo pasado
        $dir = '/anuario/secciones/';
        $fulldir = Yii::getAlias('@uploads') . $dir;
        if (isset($_POST['encabezado']) && isset($_POST['archivo'])) {
            //escribiendo el encabeazado
            //sacar el slug segun el nombre del encabezado
            $slugger = new Slugger();
            $slug = $slugger->slugify($_POST['archivo']);

            if (file_exists($fulldir . $slug . '.html')) {
                fopen($fulldir . $slug . '.html', 'r');
            } else {
                //creando el archivo de encabezado
                fopen($fulldir . $slug . '.html', 'a+');
            }
            file_put_contents($fulldir . $slug . '.html', $_POST['encabezado']);

            //Gestionanado la insercion en la tabla de anuario
            try {
                $band = 0;
                if (isset($_POST['esmit'])) {
                    $band = 1;
                }
                $creado = $this->gestionarTablaAnuario($_POST['archivo'], $dir, $slug . '.html', $band, $_POST['orden']);
                $this->limpiarSobrantes('secciones');
                //creando el flash y redireccionando
                if ($creado) {
                    Yii::$app->session->setFlash('success', 'Encabezado creado satisfactoriamente');
                } else {
                    Yii::$app->session->setFlash('info', 'Encabezado enexistencia');
                }
                Traceador::crearTraza('creacion', 'Creado el encabezado: ' . $_POST['encabezado']);
                return $this->redirect('edicion');
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', $e->getMessage());
                return $this->render('crearEncabezados', ['listAnuario' => $listAnuario]);
            }
        }
        return $this->render('crearEncabezados', ['listAnuario' => $listAnuario]);
    }

    /**
     * Funcion encargada de generar todas los archivos dentro del directorio "anuario/personas"
     * hace llamadas a los metodos de crear los encabezados como los parciales HTML.
     */
    public function actionGenerarPersonas($parcial = false)
    {
        if (!User::hasPermission('action_anuario_generarpersonas')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        ini_set('memory_limit', '1024M');
        $this->layout = 'main';
        $slug = new Slugger();
        $shortPersonas = ShortcutHomologacionesPersonas::find()->groupBy(['homologacion'])->all();
        if ($shortPersonas !== null) {
            foreach ($shortPersonas as $sp) {
                $personas = $this->getPersonalPorAcreditacionAnuario($sp->homologacion);
                $homo = AcreditacionesIndustriales::findOne($sp->homologacion);
                $headerfile = $slug->slugify($homo->titulo_anuario);

                try {
                    if ($parcial === true) {
                        $this->escribirEncabezadoHtml('personas', $headerfile);
                    } else {
                        $this->escribirEncabezadoHtml('personas', $headerfile);
                        $this->escribirParcialHtml('personas', $personas, $homo, $headerfile);
                    }
                } catch (Exception $e) {
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage());
                }
            }
            Traceador::crearTraza('generacion', 'Personas');
            return $this->redirect('edicion');
        }
    }

    /**
     * Funcion encargada de generar todas los archivos dentro del directorio "anuario/productos"
     * hace llamadas a los metodos de crear los encabezados como los parciales HTML.
     */
    public function actionGenerarProductos($parcial = false)
    {
        if (!User::hasPermission('action_anuario_generarproductos')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        ini_set('memory_limit', '1024M');
        $this->layout = 'main';
        $slug = new Slugger();
        $shortProds = ShortcutHomologacionesProductos::find()->orderBy('homologacion')->all();
        if ($shortProds !== null) {
            foreach ($shortProds as $sp) {
                $prods = $this->getProductosPorAcreditacionAnuario($sp->homologacion);

                $homo = AcreditacionesIndustriales::find($sp->homologacion);
                $headerfile = $slug->slugify($homo->titulo_anuario);
                try {
                    if ($parcial === true) {
                        $this->escribirEncabezadoHtml('productos', $headerfile);
                    } else {
                        $this->escribirEncabezadoHtml('productos', $headerfile);
                        $this->escribirParcialHtml('productos', $prods, $homo, $headerfile);
                    }
                } catch (Exception $e) {
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage());
                }
            }
            Traceador::crearTraza('generacion', 'Productos');
            Yii::$app->session->setFlash('warning', 'No se han encontrado resultados');
            return $this->redirect(array('edicion'));
        }
    }

    /**
     * Funcion encargada de generar todas los archivos dentro del directorio "anuario/dacs"
     * hace llamadas a los metodos de crear los encabezados como los parciales HTML.
     */
    public function actionGenerarDacs($parcial = false)
    {
        if (!User::hasPermission('action_anuario_generardacs')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        ini_set('memory_limit', '1024M');
        $this->layout = 'main';
        $slug = new Slugger();
        $shortProds = ShortcutHomologacionesDacs::find()->groupBy('homologacion')->all();
        if ($shortProds !== null) {
            foreach ($shortProds as $sp) {
                $prods = $this->getDACsPorAcreditacionAnuario($sp->homologacion);
                $homo = AcreditacionesIndustriales::findOne($sp->homologacion);
                $headerfile = $slug->slugify('Dispositivos Auxiliares de Carga');
                try {
                    if ($parcial === true) {
                        $this->escribirEncabezadoHtml('dacs', $headerfile);
                    } else {
                        $this->escribirEncabezadoHtml('dacs', $headerfile);
                        $this->escribirParcialHtml('dacs', $prods, $homo, $headerfile);
                    }
                } catch (Exception $e) {
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage());
                }
            }
            Traceador::crearTraza('generacion', 'Productos');
            return $this->redirect(array('edicion'));
        }
    }

    /**
     * Funcion encargada de generar todas los archivos dentro del directorio "anuario/mits"
     * hace llamadas a los metodos de crear los encabezados como los parciales HTML.
     */
    public function actionGenerarMits($parcial = false)
    {
        if (!User::hasPermission('action_anuario_generarmits')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        ini_set('memory_limit', '1024M');
        $this->layout = 'main';
        $slug = new Slugger();
        $shortProds = ShortcutHomologacionesMediosIzado::find()->groupBy('homologacion')->all();
        if ($shortProds !== null) {
            foreach ($shortProds as $sp) {
                $prods = $this->getMITsPorAcreditacionAnuario($sp->homologacion);

                $homo = AcreditacionesIndustriales::findOne($sp->homologacion);
                $headerfile = $slug->slugify('Medios de Izado Terrestres');
                try {
                    if ($parcial === true) {
                        $this->escribirEncabezadoHtml('mits', $headerfile);
                    } else {
                        $this->escribirEncabezadoHtml('mits', $headerfile);
                        $this->escribirParcialHtml('mits', $prods, $homo, $headerfile);
                    }
                } catch (Exception $e) {
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage());
                }
            }
            Traceador::crearTraza('generacion', 'Medios Izado Terrestres');
            return $this->redirect(array('edicion'));
        }
    }

    /**
     * Funcion encargada de generar todas los archivos dentro del directorio "anuario/servicios"
     * hace llamadas a los metodos de crear los encabezados como los parciales HTML.
     */
    public function actionGenerarServicios($parcial = false)
    {
        if (!User::hasPermission('action_anuario_generarservicios')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        ini_set('memory_limit', '1024M');
        $this->layout = 'main';
        $slug = new Slugger();
        $shortServ = ShortcutHomologacionesServiciosProductos::find()->groupBy('homologacion')->all();
        if ($shortServ !== null) {
            foreach ($shortServ as $sp) {
                $servs = $this->getServiciosPorAcreditacionAnuario($sp->homologacion);

                $homo = AcreditacionesIndustriales::findOne($sp->homologacion);
                $headerfile = $slug->slugify($homo->titulo_anuario);
                try {
                    if ($parcial === true) {
                        $this->escribirEncabezadoHtml('servicios', $headerfile);
                    } else {
                        $this->escribirEncabezadoHtml('servicios', $headerfile);
                        $this->escribirParcialHtml('servicios', $servs, $homo, $headerfile);
                    }
                } catch (Exception $e) {
                    Yii::$app->session->setFlash('error', 'Ha ocurrido un error: ' . $e->getMessage());
                }
            }
            Traceador::crearTraza('generacion', 'Servicios');
            return $this->redirect(array('edicion'));
        }
    }

    /**
     * Funcion para hacer una consulta compuesta para obtener todas las personas vinculadas con la tabla
     * de shorcut segun la acreditacion pasada como parametro.
     *
     * @param $acreditacion
     *
     * @return mixed personas
     */
    public function getPersonalPorAcreditacionAnuario($acreditacion)
    {
        $personas = ShortcutHomologacionesPersonas::find()
            ->joinWith('homologacion0')
            ->where(['acreditaciones_industriales.id' => $acreditacion])
            ->select('titulo_anuario')
            ->joinWith('persona0')
            ->select('nombre_apellidos, provincia, entidad')
            ->orderBy(['vigente' => SORT_ASC])
            ->all();
        return $personas;
    }

    /**
     * Funcion para hacer una consulta compuesta para obtener todos los productos vinculados con la tabla
     * de shorcut segun la acreditacion pasada como parametro.
     *
     * @param $acreditacion
     *
     * @return mixed personas
     */
    public function getProductosPorAcreditacionAnuario($acreditacion)
    {
        $prods = ShortcutHomologacionesProductos::find()
            ->joinWith('homologacion0')->where(['id' => $acreditacion])->select('titulo_anuario')
            ->joinWith('producto0')->select('nombre, provincia, propietario')
            ->orderBy(['vigencia' => SORT_ASC])
            ->asArray();

        return $prods;
    }

    /**
     * Funcion para hacer una consulta compuesta para obtener todos los productos vinculados con la tabla
     * de shorcut segun la acreditacion pasada como parametro.
     *
     * @param $acreditacion
     *
     * @return mixed personas
     */
    public function getDACsPorAcreditacionAnuario($acreditacion)
    {
        $prods = ShortcutHomologacionesDacs::find()
            ->joinWith('homologacion0')->where(['id' => $acreditacion])
            ->joinWith('dac0')->where(['id' => $acreditacion])
            ->where(['>', 'vigencia', date('Y-m-d')])
            ->orderBy(['vigencia' => SORT_ASC])
            ->all();

        return $prods;
    }

    /**
     * Funcion para hacer una consulta compuesta para obtener todos los productos vinculados con la tabla
     * de shorcut segun la acreditacion pasada como parametro.
     *
     * @param $acreditacion
     *
     * @return mixed personas
     */
    public function getMitsPorAcreditacionAnuario($acreditacion)
    {
        $prods = ShortcutHomologacionesMediosIzado::find()
            ->joinWith('homologacion0')
            ->joinWith('medioIzaje')
            ->where(['acreditaciones_industriales.id' => $acreditacion])
            ->andWhere(['>', 'vigencia', date('Y-m-d')])
            ->orderBy(['vigencia' => SORT_ASC])
            ->all();
        return $prods;
    }

    /**
     * Funcion para hacer una consulta compuesta para obtener todos los servicios vinculados con la tabla
     * de shorcut segun la acreditacion pasada como parametro.
     *
     * @param $acreditacion
     *
     * @return mixed personas
     */
    public function getServiciosPorAcreditacionAnuario($acreditacion)
    {
        $serv = ShortcutHomologacionesServiciosProductos::find()
            ->joinWith('homologacion0')
            ->joinWith('servicio0')
            ->where(['acreditaciones_industriales.id' => $acreditacion])
            ->andWhere(['>', 'servicios_laboratorios_acreditados.vigencia', date('Y-m-d')])
            ->orderBy(['vigencia' => SORT_ASC])
            ->all();
        return $serv;
    }

    /**
     *  Devuelve el contenido del encabezado para su edicion.
     */
    public function actionGetContenido($tipo, $slug)
    {
        if (!User::hasPermission('action_anuario_getcontenido')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $dir = Yii::getAlias('@webroot') . '/uploads/anuario/' . $tipo . '/enc/';
        if (file_exists($dir . $slug)) {
            $archivo = file_get_contents($dir . $slug);
        } else {
            $archivo = '<h2>No Se encontro el archivo</h2>';
        }
        echo $archivo;
    }

    /**
     * Funcion encargada de escrbir el encabezado  HTML segun el tipo de objeto.
     *
     * @param $tipo especifica el objeto de homologacion a escribir
     * @param $header el nombre de archivo a generar
     *
     * @return bool
     */
    private function escribirEncabezadoHtml($tipo, $header)
    {
        ini_set('memory_limit', '1024M');
        try {
            if (file_exists(Yii::getAlias('@webroot') . '/uploads/anuario/' . $tipo . '/enc/' . $header . '.html')) {
                fopen(Yii::getAlias('@webroot') . '/uploads/anuario/' . $tipo . '/enc/' . $header . '.html', 'r');
            } else {
                //creando el archivo de encabezado
                fopen(Yii::getAlias('@webroot') . '/uploads/anuario/' . $tipo . '/enc/' . $header . '.html', 'a+');
            }

            return true;
        } catch (Exception $e) {
            return false;
        }

        /*
        switch ($tipo) {
            case 'personas':
                if (file_exists(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html')) {
                    fopen(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html', 'r');
                } else {
                    //creando el archivo de encabezado
                    fopen(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html', 'a+');
                }

                return true;
            case 'productos':
                if (file_exists(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html')) {
                    fopen(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html', 'r');
                } else {
                    //creando el archivo de encabezado
                    fopen(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html', 'a+w');
                }

                return true;
            case 'servicios':
                if (file_exists(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html')) {
                    fopen(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html', 'r');
                } else {
                    //creando el archivo de encabezado
                    fopen(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html', 'a+');
                }

                return true;
            case 'mits':
                if (file_exists(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html')) {
                    fopen(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html', 'r');
                } else {
                    //creando el archivo de encabezado
                    fopen(Yii::getAlias('@webroot').'/uploads/anuario/'.$tipo.'/enc/'.$header.'.html', 'a+');
                }

                return true;
        }
        */
    }

    /**
     * Funcion encargada de generar el parcial HTML correspondiente al tipo de objeto y teniendo en cuenta
     * el encabezado a incluir.
     *
     * @param $tipo tipo de homologacion
     * @param $objeto listado de objetos de homologacion
     * @param $homologacion objeto de homologacion en cuestion para escrbir los marcadores
     * @param $header archivo encabezado a incluir
     *
     * @return bool
     */
    private function escribirParcialHtml($tipo, $objeto, $homologacion, $header)
    {
        ini_set('memory_limit', '1024M');
        if ($objeto !== null) {

            //Gestionanado la insercion en la tabla de anuario

            if ($tipo == 'mits') {
                $nombre = 'Medios de Izado Terrestres';
            } elseif ($tipo == 'dacs') {
                $nombre = 'Dispositivos Auxiliares de Carga';
            } else {
                $nombre = $homologacion->titulo_anuario;
            }
            $dir = '/uploads/anuario/' . $tipo . '/';
            $fulldir = Yii::getAlias('@webroot') . $dir;

            //$this->gestionarTablaAnuario($nombre, $dir, $header.'.html');
            switch ($tipo) {
                case 'personas':
                    $this->gestionarTablaAnuario($nombre, $dir, $header . '.html', 0, null);
                    $html = $this->renderPartial(
                        'printPersonasAcreditadas',
                        [
                            'model' => $objeto,
                            'homologacion' => $homologacion->titulo_anuario,
                            'headerfile' => $header,
                        ],
                        true
                    );
                    file_put_contents($fulldir . $header . '.html', $html, FILE_TEXT);
                    Yii::$app->session->setFlash('success', 'Generados los parciales de tipo: ' . $tipo);

                    return true;
                case 'productos':
                    $this->gestionarTablaAnuario($nombre, $dir, $header . '.html', 0, null);
                    $html = $this->renderPartial(
                        'printProductosAcreditados',
                        array(
                            'model' => $objeto,
                            'homologacion' => $homologacion->titulo_anuario,
                            'headerfile' => $header,
                        ),
                        true
                    );
                    file_put_contents($fulldir . $header . '.html', $html, FILE_TEXT);
                    Yii::$app->session->setFlash('success', 'Generados los parciales de tipo: ' . $tipo);

                    return true;
                case 'servicios':
                    $this->gestionarTablaAnuario($nombre, $dir, $header . '.html', 0, null);
                    $html = $this->renderPartial(
                        'printServiciosAcreditados',
                        array(
                            'model' => $objeto,
                            'homologacion' => $homologacion->titulo_anuario,
                            'headerfile' => $header,
                        ),
                        true
                    );
                    file_put_contents($fulldir . $header . '.html', $html, FILE_TEXT);
                    Yii::$app->session->setFlash('success', 'Generados los parciales de tipo: ' . $tipo);

                    return true;
                case 'mits':
                    $this->gestionarTablaAnuario($nombre, $dir, $header . '.html', 1, null);
                    $html = $this->renderPartial(
                        'printMITsAcreditados',
                        array(
                            'model' => $objeto,
                            'homologacion' => $homologacion->titulo_anuario,
                            'headerfile' => $header,
                        ),
                        true
                    );
                    file_put_contents($fulldir . $header . '.html', $html, FILE_TEXT);
                    Yii::$app->session->setFlash('success', 'Generados los parciales de tipo: ' . $tipo);

                    return true;
                case 'dacs':
                    $this->gestionarTablaAnuario($nombre, $dir, $header . '.html', 1, null);
                    $html = $this->renderPartial(
                        'printDACsAcreditados',
                        array(
                            'model' => $objeto,
                            'homologacion' => $homologacion->titulo_anuario,
                            'headerfile' => $header,
                        ),
                        true
                    );
                    file_put_contents($fulldir . $header . '.html', $html, FILE_TEXT);
                    Yii::$app->session->setFlash('success', 'Generados los parciales de tipo: ' . $tipo);

                    return true;
            }
        } else {
            Yii::$app->session->setFlash(
                'error',
                'Ha ocurrido un error: No se puede escribir el Parcial por falta de objeto '
            );
        }

        return false;
    }

    /**
     * Funcion para limpiar el directorio de anuario, util cuando se generan archivos no correctos.
     *
     * @param $tipo para saber el directorio donde limpiar
     */
    private function limpiarDirectorio($tipo)
    {
        $dir = Yii::getAlias('@webroot') . '/uploads/anuario/' . $tipo . '/';
        $dir2 = Yii::getAlias('@webroot') . '/uploads/anuario/' . $tipo . '/enc/';
        $archivos = array_diff(scandir($dir), array('..', '.'));
        $enc = array_diff(scandir($dir2), array('..', '.'));
        foreach ($archivos as $file) { // iterate files
            if (is_file($file)) {
                unlink($file); // delete file
            }
        }

        foreach ($enc as $file) { // iterate files
            if (is_file($file)) {
                unlink($file); // delete file
            }
        }
    }

    /**
     * Funcion encargada de construir un array con los slugs como llaves y los titulos de anuario como valor
     * util para enviar a la vista y para la generacion de los parciales.
     *
     * @param $tipo la carpeta a trabajar
     * @param bool|false $encabezados un parametro de control para saber en ke directorio se esta trabajando
     *
     * @return mixed
     */
    private function getArchivosYnombres($tipo, $encabezados = false)
    {
        if ($encabezados == false) {
            $dir = Yii::getAlias('@webroot') . '/uploads/anuario/' . $tipo . '/';
        } else {
            $dir = Yii::getAlias('@webroot') . '/uploads/anuario/' . $tipo . '/enc/';
        }
        $archivos = array_diff(scandir($dir), array('..', '.'));

        foreach ($archivos as $ar) {
            $slug = preg_replace('/\\.[^.\\s]{3,4}$/', '', $ar);
            try {
                $acreditacion = AcreditacionesIndustriales::find('slug = :slug', array(':slug' => $slug));
                $listado[$ar] = $acreditacion->titulo_anuario;
            } catch (Exception $e) {
                Yii::$app->session->setFlash('error', 'Imposible listar todos los archivos: ' . $e->getMessage());
            }
        }

        return $listado;
    }

    /**
     * Funcion para borrar todos los parciales que no esten en la tabla anuario.
     *
     * @param $tipo para saber en que directorio buscar
     *
     * @return bool verdadero si efectivo
     */
    private function limpiarSobrantes($tipo)
    {
        $fragmentos = Anuario::find()->all();
        $fragmentosEnUso = array();
        foreach ($fragmentos as $f) {
            array_push($fragmentosEnUso, $f->archivo);
        }
        $dirParcial = Yii::getAlias('@webroot') . '/uploads/anuario/' . $tipo . '/';
        $parciales = array_diff(scandir($dirParcial), array('..', '.'));
        $parciales_a_borrar = array_diff($parciales, $fragmentosEnUso);
        foreach ($parciales_a_borrar as $pa) {
            unlink($dirParcial . $pa); // delete file
        }

        if ($tipo != 'secciones') {
            $dirEnc = Yii::getAlias('@webroot') . '/uploads/anuario/' . $tipo . '/enc/';
            $encabezados = array_diff(scandir($dirEnc), array('..', '.'));
            $encabezados_a_borrar = array_diff($encabezados, $fragmentosEnUso);
            foreach ($encabezados_a_borrar as $en) {
                unlink($dirEnc . $en); // delete file
            }
        }

        return true;
    }

    private function gestionarTablaAnuario($nombreP, $dirP, $archivoP, $mit, $orden)
    {
        $anuariohtml = new Anuario();

        $chek = Anuario::findAll(
            ['dir' => $dirP],
            ['archivo' => $archivoP]
        );
        if (!isset($orden) || $orden == null) {
            $lastOrden = Anuario::find()->orderBy(['orden' => SORT_DESC])->one();
            $ordenActual = ($lastOrden !== null) ? $lastOrden->orden + 1 : 1;
        } else {
            $beforElement = Anuario::findOne(['orden' => $orden]);

            $ordenActual = $beforElement->orden + 1;
            $allElements = Anuario::findAll(['orden' => $ordenActual]);
            if ($allElements != null) {
                foreach ($allElements as $e) {
                    $e->orden = $e->orden + 1;
                    $e->update();
                }
            }
        }
        if ($chek === null) {
            $anuariohtml->nombre = $nombreP;
            $anuariohtml->dir = $dirP;
            $anuariohtml->archivo = $archivoP;
            $anuariohtml->orden = $ordenActual;
            $anuariohtml->mit = $mit;

            $anuariohtml->save();
            return true;
        }
        return false;
    }
}
