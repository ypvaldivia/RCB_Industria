<?php

namespace app\controllers;

use app\components\Traceador;
use app\models\CategoriasMediosIzado;
use Exception;
use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use yii\widgets\ActiveForm;

class CategoriasMediosIzadoController extends Controller
{
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     *
     * @return array access control rules
     */
    public function accessRules()
    {
        Yii::$app->session['menu'] = 'objetoInspeccion';
        Yii::$app->session['submenu'] = 'mediosIzado';

        return array(
            array(
                'allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'create', 'update', 'batchDelete'),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_categoriasmediosizado_index')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $data = CategoriasMediosIzado::find()->all();
        return $this->render('index', array('data' => $data));
    }

    public function actionCreate()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_categoriasmediosizado_create')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = new CategoriasMediosIzado();
        if (isset($_POST['CategoriasMediosIzado'])) {
            if (isset($_POST['CategoriasMediosIzado']['nivelEspecial'])) {
                $model->nivel_especial = $_POST['CategoriasMediosIzado']['nivelEspecial'];
            }
            $model->setAttributes($_POST['CategoriasMediosIzado']);

            try {
                if ($model->save()) {
                    Traceador::crearTraza(
                        'creacion',
                        'Creada categoria de medios de izado: ' . $model->categoria
                    );
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        } elseif (isset($_GET['CategoriasMediosIzado'])) {
            $model->attributes = $_GET['CategoriasMediosIzado'];
        }

        return $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id)
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_categoriasmediosizado_update')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $model = $this->loadModel($id);

        if (isset($_POST['CategoriasMediosIzado'])) {
            $model->setAttributes($_POST['CategoriasMediosIzado']);
            if (isset($_POST['CategoriasMediosIzado']['nivelEspecial'])) {
                $model->nivel_especial = $_POST['CategoriasMediosIzado']['nivelEspecial'];
            }
            try {
                if ($model->save()) {
                    Traceador::crearTraza(
                        'edicion',
                        'Editada categoria de medios de izado: ' . $model->categoria
                    );
                    if (isset($_GET['returnUrl'])) {
                        return $this->redirect($_GET['returnUrl']);
                    } else {
                        return $this->redirect(array('index'));
                    }
                }
            } catch (Exception $e) {
                $model->addError('id', $e->getMessage());
            }
        }

        return $this->render('update', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = CategoriasMediosIzado::findOne($id);
        if ($model === null) {
            throw new HttpException(404, Yii::t('app', 'The requested page does not exist.'));
        }

        return $model;
    }

    /**
     * Performs the AJAX validation.
     *
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'categorias-medios-izado-form') {
            echo ActiveForm::validate($model);
            Yii::$app->end();
        }
    }

    public function actionBatchDelete()
    {
        if (!\webvimark\modules\UserManagement\models\User::hasPermission('action_categoriasmediosizado_batchdelete')) {
            throw new HttpException(401, 'Usted no tiene privilegios suficientes para realizar esta acción');
        }
        $successCount = 0;
        $request = Yii::$app->getRequest();
        if ($request->getIsPostRequest()) {
            if (isset($_POST['ids'])) {
                $ids = $_POST['ids'];
                foreach ($ids as $id) {
                    $model = $this->loadModel($id);
                    Traceador::crearTraza(
                        'eliminacion',
                        'Fue eliminada la Categoria de MIT: ' . $model->categoria
                    );
                    try {
                        $model->delete();
                        ++$successCount;
                    } catch (Exception $e) {
                        Yii::$app->session->setFlash('error', 'Ocurrio un error al intentar eliminar "' . $model->categoria . '" consistente en: "' . $e->getMessage());
                        break;
                    }
                }
                Yii::$app->session->setFlash('success', 'Se han eliminado ' . $successCount . ' exitosamente');
            }
        } else {
            throw new HttpException(400, 'Petición no válida');
        }
        return $this->redirect(array('index'));
    }
}
