<?php

Yii::setAlias('@uploads', realpath(dirname(__FILE__) . '/../web/uploads'));
Yii::setAlias('@materialpro', realpath(dirname(__FILE__) . '/../web/themes/materialpro'));
Yii::setAlias('@materialplugins', realpath(dirname(__FILE__) . '/../web/themes/materialpro/assets/plugins'));

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
];
