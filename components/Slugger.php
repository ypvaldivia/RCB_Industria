<?php

namespace app\components;

/**
 * This class is used to provide an example of integrating simple classes as
 * services into a Symfony application.
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class Slugger
{
    public function slugify($string)
    {
        return trim(
            preg_replace('/[^a-z0-9]+/', '-', strtolower(strip_tags($string))),
            '-'
        );
    }

    public function truncate_words($text, $limit, $ellipsis = '...')
    {
        $words = preg_split(
            "/[\n\r\t ]+/",
            $text,
            $limit + 1,
            PREG_SPLIT_NO_EMPTY | PREG_SPLIT_OFFSET_CAPTURE
        );
        if (count($words) > $limit) {
            end($words); //ignore last element since it contains the rest of the string
            $last_word = prev($words);

            $text =
                substr($text, 0, $last_word[1] + strlen($last_word[0])) .
                $ellipsis;
        }

        return $text;
    }
}
