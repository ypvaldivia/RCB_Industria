<?php

namespace app\components;

use Yii;
use webvimark\modules\UserManagement\components\UserIdentity;
use app\models\Funcionarios;
use app\models\Oficinas;
use webvimark\modules\UserManagement\components\UserConfig;

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserComponent extends UserConfig
{
    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     *
     * @return bool whether authentication succeeds.
     */
    public function authenticate()
    {
        $users = array(
            // username => password
            'demo' => 'demo',
            'admin' => 'admin'
        );
        $exception = Yii::$app->errorHandler->exception;

        if (!isset($users[$this->username])) {
            $this->errorCode = $exception->getMessage();
        } elseif ($users[$this->username] !== $this->password) {
            $this->errorCode = $exception->getMessage();
        } else {
            $this->errorCode = $exception->getMessage();
        }

        return !$this->errorCode;
    }

    public static function isBoss()
    {
        $user = Funcionarios::findOne(['usuario' => Yii::$app->user->id]);
        if ($user == null) {
            return true;
        } else {
            $office = $user->oficina0;
            $central = Oficinas::findOne([
                'oficina_superior' => $user->oficina0->id
            ]);
            if ($office->id == $central->id) {
                return true;
            } else {
                return false;
            }
        }
    }
}
