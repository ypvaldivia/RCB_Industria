<?php

namespace app\components;

use Yii;
use Exception;
use yii\swiftmailer\Mailer;

use app\models\AprobacionXFuncionarios;
use app\models\Configuracion;
use app\models\Funcionarios;
use app\models\InspeccionesXDacs;
use app\models\InspeccionesXMediosIzado;
use app\models\InspeccionesXPersonas;
use app\models\InspeccionesXProductos;
use app\models\InspeccionesXServiciosProductos;
use app\models\InspectoresActuantes;
use webvimark\modules\UserManagement\models\User;

/**
 * Created by PhpStorm.
 * User: levian
 * Date: 26/2/2016
 * Time: 16:33.
 */
class Cartero
{
    public static function enviarNotificacionAprobacion(
        $asunto,
        $type,
        $inspeccion,
        $tipo = 'mail_default'
    ) {
        $mailer = new Mailer();
        $mailerconfig = Configuracion::findOne(1);
        $mailer->setSmtp(
            $mailerconfig->servidor,
            $mailerconfig->puerto,
            $mailerconfig->smtp_ssl,
            $mailerconfig->smtp_auth,
            $mailerconfig->usuario,
            $mailerconfig->password
        );

        $detinatarios = array();
        $mailer->setLayout($tipo);
        $mailer->setFrom('Sistema de control RCB :: Grupo de Industria');

        $user = Funcionarios::findOne('usuario = :u', array(
            ':u' => Yii::$app->user->id
        ));
        if ($user == null) {
            Yii::$app->user->setFlash(
                'error',
                'Se produjo el siguiente error enviando la notificación por correo: Usted no aparece registrado como funcionario'
            );
            return false;
        }
        $dest = Funcionarios::getFuncionarioSuperior($user);

        if ($dest != null) {
            if (count($dest) == 1) {
                if ($dest[0]->id == $user->id) {
                    Yii::$app->user->setFlash(
                        'success',
                        'La inspección fue Aprobada por todos los niveles correctamente'
                    );
                    return true;
                }
                $destinatario = User::findAll(
                    $dest[0]->usuario0->iduser
                );
                if ($destinatario != null && $destinatario->email != null) {
                    $mailer->setTo($destinatario->email);
                    $mailer->setSubject($asunto);
                    $subject =
                        'Informe de Inspección pendiente de su aprobación';
                    $descripcion = '';
                    switch ($type) {
                        case 'inspeccionPersona':
                            $search = InspeccionesXPersonas::findAll(
                                'inspeccion = :i',
                                array(':i' => $inspeccion->id)
                            );
                            $objetos = '';
                            if ($search != null) {
                                foreach ($search as $o) {
                                    if ($objetos == '') {
                                        $objetos =
                                            $o->persona0->nombre_apellidos .
                                            '<br/>';
                                    } else {
                                        $objetos =
                                            $objetos .
                                            $o->persona0->nombre_apellidos .
                                            '<br/>';
                                    }
                                }
                                $descripcion =
                                    'El informe de la ' .
                                    $inspeccion->tipoHomologacion->tipo .
                                    ' con No. de Control: ' .
                                    $inspeccion->numero_control .
                                    ', en el apartado de Inspecciones a Personal y relacionado con las personas siguientes: <br/>' .
                                    $objetos .
                                    ' fue revisado y ACEPTADO por el ' .
                                    $user->cargo0->cargo .
                                    ' ' .
                                    $user->nombre_apellidos .
                                    ' quedando pendiente de su APROBACIÓN';
                            } else {
                                Yii::$app->user->setFlash(
                                    'warning',
                                    'Se produjo el siguiente error enviando la notificación por correo: No se encontraron las personas asociadas a esta inspección'
                                );
                                return false;
                            }
                            break;
                        case 'inspeccionLabServ':
                            $search = InspeccionesXServiciosProductos::findAll(
                                'inspeccion = :i',
                                array(':i' => $inspeccion->id)
                            );
                            $objetos = '';
                            if ($search != null) {
                                foreach ($search as $o) {
                                    if ($objetos == '') {
                                        $objetos =
                                            $o->servcios0->servicio . '<br/>';
                                    } else {
                                        $objetos =
                                            $objetos .
                                            $o->servcios0->servicio .
                                            '<br/>';
                                    }
                                }
                                $descripcion =
                                    'El informe de la ' .
                                    $inspeccion->tipoHomologacion->tipo .
                                    ' con No. de Control: ' .
                                    $inspeccion->numero_control .
                                    ', en el apartado de Inspecciones a Servicios y relacionado con los servicios siguientes: <br/>' .
                                    $objetos .
                                    ' fue revisado y ACEPTADO por el ' .
                                    $user->cargo0->cargo .
                                    ' ' .
                                    $user->nombre_apellidos .
                                    ' quedando pendiente de su APROBACIÓN';
                            } else {
                                Yii::$app->user->setFlash(
                                    'warning',
                                    'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los servicios asociados a esta inspección'
                                );
                                return false;
                            }
                            break;
                        case 'inspeccionProducto':
                            $search = InspeccionesXProductos::findAll(
                                'inspeccion = :i',
                                array(':i' => $inspeccion->id)
                            );
                            $objetos = '';
                            if ($search != null) {
                                foreach ($search as $o) {
                                    if ($objetos == '') {
                                        $objetos =
                                            $o->producto0->nombre . '<br/>';
                                    } else {
                                        $objetos =
                                            $objetos .
                                            $o->producto0->nombre .
                                            '<br/>';
                                    }
                                }
                                $descripcion =
                                    'El informe de la ' .
                                    $inspeccion->tipoHomologacion->tipo .
                                    ' con No. de Control: ' .
                                    $inspeccion->numero_control .
                                    ', en el apartado de Inspecciones a Productos y relacionado con los objetos siguientes: <br/>' .
                                    $objetos .
                                    ' fue revisado y ACEPTADO por el ' .
                                    $user->cargo0->cargo .
                                    ' ' .
                                    $user->nombre_apellidos .
                                    ' quedando pendiente de su APROBACIÓN';
                            } else {
                                Yii::$app->user->setFlash(
                                    'warning',
                                    'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los productos asociados a esta inspección'
                                );
                                return false;
                            }
                            break;
                        case 'inspeccionDac':
                            $search = InspeccionesXDacs::findAll(
                                'inspeccion = :i',
                                array(':i' => $inspeccion->id)
                            );
                            $objetos = '';
                            if ($search != null) {
                                foreach ($search as $o) {
                                    if ($objetos == '') {
                                        $objetos = $o->dac0->nombre . '<br/>';
                                    } else {
                                        $objetos =
                                            $objetos .
                                            $o->dac0->nombre .
                                            '<br/>';
                                    }
                                }
                                $descripcion =
                                    'El informe de la ' .
                                    $inspeccion->tipoHomologacion->tipo .
                                    ' con No. de Control: ' .
                                    $inspeccion->numero_control .
                                    ', en el apartado de Inspecciones a DACs y relacionado con los objetos siguientes: <br/>' .
                                    $objetos .
                                    ' fue revisado y ACEPTADO por el ' .
                                    $user->cargo0->cargo .
                                    ' ' .
                                    $user->nombre_apellidos .
                                    ' quedando pendiente de su APROBACIÓN';
                            } else {
                                Yii::$app->user->setFlash(
                                    'warning',
                                    'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los DACs asociados a esta inspección'
                                );
                                return false;
                            }
                            break;
                        case 'inspeccionMIT':
                            $search = InspeccionesXMediosIzado::findAll(
                                'inspeccion = :i',
                                array(':i' => $inspeccion->id)
                            );
                            $objetos = '';
                            if ($search != null) {
                                foreach ($search as $o) {
                                    if ($objetos == '') {
                                        $objetos =
                                            $o->medioIzado->nombre . '<br/>';
                                    } else {
                                        $objetos =
                                            $objetos .
                                            $o->medioIzado->nombre .
                                            '<br/>';
                                    }
                                }
                                $descripcion =
                                    'El informe de la ' .
                                    $inspeccion->tipoHomologacion->tipo .
                                    ' con No. de Control: ' .
                                    $inspeccion->numero_control .
                                    ', en el apartado de Inspecciones a Medios de Izado y relacionado con los objetos siguientes: <br/>' .
                                    $objetos .
                                    ' fue revisado y ACEPTADO por el ' .
                                    $user->cargo0->cargo .
                                    ' ' .
                                    $user->nombre_apellidos .
                                    ' quedando pendiente de su APROBACIÓN';
                            } else {
                                Yii::$app->user->setFlash(
                                    'warning',
                                    'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los MITs asociados a esta inspección'
                                );
                                return false;
                            }
                            break;
                    }

                    $mailer->setData(array(
                        'subject' => $subject,
                        'description' => $descripcion
                    ));
                    try {
                        if ($mailer->send(null)) {
                            $nombre = $dest[0]->nombre_apellidos . '<br/>';
                            array_push($detinatarios, $nombre);
                        }
                    } catch (Exception $e) {
                        Yii::$app->user->setFlash(
                            'warning',
                            'Se produjo el siguiente error enviando la notificación por correo: ' .
                                $e->getMessage()
                        );
                        return false;
                    }
                } else {
                    Yii::$app->user->setFlash(
                        'warning',
                        'Se produjo el siguiente error enviando la notificación por correo: El destinatario de la notificación no posee dirección de correo electrónico definida'
                    );
                    return false;
                }
            } else {
                $mailer->setSubject($asunto);
                $subject = 'Informe de Inspección pendiente de su aprobación';
                $descripcion = '';
                switch ($type) {
                    case 'inspeccionPersona':
                        $search = InspeccionesXPersonas::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos =
                                        $o->persona0->nombre_apellidos .
                                        '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->persona0->nombre_apellidos .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a Personal y relacionado con las personas siguientes: <br/>' .
                                $objetos .
                                ' fue revisado y ACEPTADO por el ' .
                                $user->cargo0->cargo .
                                ' ' .
                                $user->nombre_apellidos .
                                ' quedando pendiente de su APROBACIÓN';
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: No se encontraron las personas asociadas a esta inspección'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionLabServ':
                        $search = InspeccionesXServiciosProductos::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos =
                                        $o->servcios0->servicio . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->servcios0->servicio .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a Servicios y relacionado con los servicios siguientes: <br/>' .
                                $objetos .
                                ' fue revisado y ACEPTADO por el ' .
                                $user->cargo0->cargo .
                                ' ' .
                                $user->nombre_apellidos .
                                ' quedando pendiente de su APROBACIÓN';
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los servicios asociados a esta inspección'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionProducto':
                        $search = InspeccionesXProductos::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos = $o->producto0->nombre . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->producto0->nombre .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a Productos y relacionado con los objetos siguientes: <br/>' .
                                $objetos .
                                ' fue revisado y ACEPTADO por el ' .
                                $user->cargo0->cargo .
                                ' ' .
                                $user->nombre_apellidos .
                                ' quedando pendiente de su APROBACIÓN';
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los productos asociados a esta inspección'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionDac':
                        $search = InspeccionesXDacs::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos = $o->dac0->nombre . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos . $o->dac0->nombre . '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a DACs y relacionado con los objetos siguientes: <br/>' .
                                $objetos .
                                ' fue revisado y ACEPTADO por el ' .
                                $user->cargo0->cargo .
                                ' ' .
                                $user->nombre_apellidos .
                                ' quedando pendiente de su APROBACIÓN';
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los DACs asociados a esta inspección'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionMIT':
                        $search = InspeccionesXMediosIzado::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos = $o->medioIzado->nombre . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->medioIzado->nombre .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a Medios de Izado y relacionado con los objetos siguientes: <br/>' .
                                $objetos .
                                ' fue revisado y ACEPTADO por el ' .
                                $user->cargo0->cargo .
                                ' ' .
                                $user->nombre_apellidos .
                                ' quedando pendiente de su APROBACIÓN';
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los MITs asociados a esta inspección'
                            );
                            return false;
                        }
                        break;
                }

                $mailer->setData(array(
                    'subject' => $subject,
                    'description' => $descripcion
                ));
                try {
                    foreach ($dest as $d) {
                        $destinatario = User::find()->where(
                            $d->usuario0->iduser
                        );
                        if (
                            $destinatario != null &&
                            $destinatario->email != null
                        ) {
                            $mailer->setTo($destinatario->email);
                            if ($mailer->send(null)) {
                                $nombre = $d->nombre_apellidos;
                                array_push($detinatarios, $nombre);
                            }
                        }
                    }
                } catch (Exception $e) {
                    Yii::$app->user->setFlash(
                        'warning',
                        'Se produjo el siguiente error enviando la notificación por correo: ' .
                            $e->getMessage()
                    );
                    return false;
                }
            }
        } else {
            Yii::$app->user->setFlash(
                'warning',
                'Se produjo el siguiente error enviando la notificación por correo: No existe un funcionario registrado como destinatario de la notificación'
            );
            return false;
        }
        Yii::$app->user->setFlash(
            'success',
            'Fueron notificados los siguientes funcionarios: ' .
                '<br/>' .
                implode('<br/>', $detinatarios)
        );
        return true;
    }

    public static function enviarNotificacionNoAprobacion(
        $asunto,
        $type,
        $inspeccion,
        $tipo = 'mail_default'
    ) {
        $mailer = new Mailer();
        $mailerconfig = Configuracion::find()->where(1);
        $mailer->setSmtp(
            $mailerconfig->servidor,
            $mailerconfig->puerto,
            $mailerconfig->smtp_ssl,
            $mailerconfig->smtp_auth,
            $mailerconfig->usuario,
            $mailerconfig->password
        );

        $inspectoresNotificados = array();
        $cuadrosNotificados = array();

        $user = Funcionarios::find('usuario = :u', array(
            ':u' => Yii::$app->user->id
        ));
        if ($user == null) {
            Yii::$app->user->setFlash(
                'warning',
                'Se produjo el siguiente error enviando la notificación por correo: Usted no aparece registrado como funcionario'
            );
            return false;
        }
        $uv = $inspeccion->getUltimaVisita();
        if ($uv != null) {
            $inspectores = InspectoresActuantes::findAll('visita = :vv', array(
                ':vv' => $uv->id
            ));
            $aprobacion = AprobacionXFuncionarios::ultimaAprobacion(
                $inspeccion->id
            );
            $cuadrosAprobaron = array_reverse(
                AprobacionXFuncionarios::findAll('inspeccion = :x', array(
                    ':x' => $inspeccion->id
                ))
            );

            if ($inspectores != null && $aprobacion != null) {
                $subject = 'Informe de Inspección no aprobado';
                $descripcion = '';
                $causas = '';
                if (
                    $aprobacion->hallazgos != null &&
                    $aprobacion->inconformidades != null
                ) {
                    $causas =
                        'Hallazgos: ' .
                        '<br/>' .
                        $aprobacion->hallazgos .
                        '<br/>' .
                        'Inconformidades: ' .
                        '<br/>' .
                        $aprobacion->inconformidades;
                }
                switch ($type) {
                    case 'inspeccionPersona':
                        $search = InspeccionesXPersonas::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos =
                                        $o->persona0->nombre_apellidos .
                                        '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->persona0->nombre_apellidos .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a Personal y relacionado con las personas siguientes: <br/>' .
                                $objetos .
                                ' fue revisado y no fue aceptado por el ' .
                                $user->cargo0->cargo .
                                ' ' .
                                $user->nombre_apellidos .
                                ' encontrándose las siguientes causales: <br/>' .
                                $causas;
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: No se encontraron las personas asociadas a esta inspección'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionLabServ':
                        $search = InspeccionesXServiciosProductos::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos =
                                        $o->servcios0->servicio . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->servcios0->servicio .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a Servicios y relacionado con los servicios siguientes: <br/>' .
                                $objetos .
                                ' fue revisado y no fue aceptado por el ' .
                                $user->cargo0->cargo .
                                ' ' .
                                $user->nombre_apellidos .
                                ' encontrándose las siguientes causales: <br/>' .
                                $causas;
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los servicios asociados a esta inspección'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionProducto':
                        $search = InspeccionesXProductos::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos = $o->producto0->nombre . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->producto0->nombre .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a Productos y relacionado con los objetos siguientes: <br/>' .
                                $objetos .
                                ' fue revisado y no fue aceptado por el ' .
                                $user->cargo0->cargo .
                                ' ' .
                                $user->nombre_apellidos .
                                ' encontrándose las siguientes causales: <br/>' .
                                $causas;
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los productos asociados a esta inspección'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionDac':
                        $search = InspeccionesXDacs::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos = $o->dac0->nombre . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos . $o->dac0->nombre . '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a DACs y relacionado con los objetos siguientes: <br/>' .
                                $objetos .
                                ' fue revisado y no fue aceptado por el ' .
                                $user->cargo0->cargo .
                                ' ' .
                                $user->nombre_apellidos .
                                ' encontrándose las siguientes causales: <br/>' .
                                $causas;
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los DACs asociados a esta inspección'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionMIT':
                        $search = InspeccionesXMediosIzado::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos = $o->medioIzado->nombre . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->medioIzado->nombre .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a Medios de Izado y relacionado con los objetos siguientes: <br/>' .
                                $objetos .
                                ' fue revisado y no fue aceptado por el ' .
                                $user->cargo0->cargo .
                                ' ' .
                                $user->nombre_apellidos .
                                ' encontrándose las siguientes causales: <br/>' .
                                $causas;
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: No se encontraron los MITs asociados a esta inspección'
                            );
                            return false;
                        }
                        break;
                }

                foreach ($inspectores as $dest) {
                    if ($dest->inspector0->usuario0->email != null) {
                        $mailer->setLayout($tipo);
                        $mailer->setFrom(
                            'Sistema de control RCB :: Departamento de Industria'
                        );
                        $mailer->setTo($dest->inspector0->usuario0->email);
                        $mailer->setSubject($asunto);
                        $mailer->setData(array(
                            'subject' => $subject,
                            'description' => $descripcion
                        ));
                        try {
                            if ($mailer->send(null)) {
                                $nombre = $dest->inspector0->nombre_apellidos;
                                array_push($inspectoresNotificados, $nombre);
                            }
                        } catch (Exception $e) {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: ' .
                                    $e->getMessage()
                            );
                            return false;
                        }
                    }
                }

                if ($cuadrosAprobaron != null) {
                    foreach ($cuadrosAprobaron as $ca) {
                        if ($ca->id != $user->id) {
                            $mailer->setLayout($tipo);
                            $mailer->setFrom(
                                'Sistema de control RCB :: Departamento de Industria'
                            );
                            $mailer->setTo($ca->funcionario0->usuario0->email);
                            $mailer->setSubject($asunto);
                            $mailer->setData(array(
                                'subject' => $subject,
                                'description' => $descripcion
                            ));
                            try {
                                if ($mailer->send(null)) {
                                    $nombre =
                                        $ca->funcionario0->nombre_apellidos;
                                    array_push($cuadrosNotificados, $nombre);
                                }
                            } catch (Exception $e) {
                                Yii::$app->user->setFlash(
                                    'warning',
                                    'Se produjo el siguiente error enviando la notificación por correo: ' .
                                        $e->getMessage()
                                );
                                return false;
                            }
                        }
                    }
                } else {
                    $cuadroAnterior = $aprobacion->funcionario0;
                    if ($cuadroAnterior->id != $user->id) {
                        $mailer->setLayout($tipo);
                        $mailer->setFrom(
                            'Sistema de control RCB :: Departamento de Industria'
                        );
                        $mailer->setTo($cuadroAnterior->usuario0->email);
                        $mailer->setSubject($asunto);
                        $mailer->setData(array(
                            'subject' => $subject,
                            'description' => $descripcion
                        ));
                        try {
                            if ($mailer->send(null)) {
                                $nombre = $cuadroAnterior->nombre_apellidos;
                                array_push($cuadrosNotificados, $nombre);
                            }
                        } catch (Exception $e) {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: ' .
                                    $e->getMessage()
                            );
                            return false;
                        }
                    }
                }
            } else {
                Yii::$app->user->setFlash(
                    'warning',
                    'Se produjo el siguiente error enviando la notificación por correo: Error de acceso en la base de datos. Inténtelo luego'
                );
                return false;
            }
        } else {
            Yii::$app->user->setFlash(
                'warning',
                'Se produjo el siguiente error enviando la notificación por correo: No se pudo acceder a la última visita de esta inspección'
            );
            return false;
        }
        if (!empty($cuadrosNotificados)) {
            Yii::$app->user->setFlash(
                'success',
                'Fueron notificados los siguientes Cuadros: ' .
                    '<br/>' .
                    implode('<br/>', $cuadrosNotificados) .
                    '<br/>' .
                    'Fueron notificados los siguientes Inspectores: ' .
                    '<br/>' .
                    implode('<br/>', $inspectoresNotificados) .
                    '<br/>'
            );
        } else {
            Yii::$app->user->setFlash(
                'success',
                'Fueron notificados los siguientes Inspectores: ' .
                    '<br/>' .
                    implode('<br/>', $inspectoresNotificados) .
                    '<br/>'
            );
        }
        return true;
    }

    public static function enviarNotificacionInformeListo(
        $asunto,
        $type,
        $inspeccion,
        $tipo = 'mail_default'
    ) {
        $mailer = new Mailer();
        $mailerconfig = Configuracion::find()->where(1);
        $mailer->setSmtp(
            $mailerconfig->servidor,
            $mailerconfig->puerto,
            $mailerconfig->smtp_ssl,
            $mailerconfig->smtp_auth,
            $mailerconfig->usuario,
            $mailerconfig->password
        );

        $detinatarios = array();
        $mailer->setLayout($tipo);
        $mailer->setFrom('Sistema de control RCB :: Grupo de Industria');

        $user = Funcionarios::find('usuario = :u', array(
            ':u' => Yii::$app->user->id
        ));
        if ($user == null) {
            Yii::$app->user->setFlash(
                'warning',
                'Se produjo el siguiente error enviando la notificación por correo: Usted no aparece registrado como funcionario'
            );
            return false;
        }
        $dest = Funcionarios::getFuncionarioSuperior($user);
        if ($dest != null) {
            if (count($dest) == 1) {
                $destinatario = User::find()->where(
                    $dest[0]->usuario0->iduser
                );
                if ($destinatario != null && $destinatario->email != null) {
                    $mailer->setTo($destinatario->email);
                    $mailer->setSubject($asunto);
                    $subject =
                        'Informe de Inspección pendiente de su aprobación';
                    $descripcion = '';
                    switch ($type) {
                        case 'inspeccionPersona':
                            $search = InspeccionesXPersonas::findAll(
                                'inspeccion = :i',
                                array(':i' => $inspeccion->id)
                            );
                            $objetos = '';
                            if ($search != null) {
                                foreach ($search as $o) {
                                    if ($objetos == '') {
                                        $objetos =
                                            $o->persona0->nombre_apellidos .
                                            '<br/>';
                                    } else {
                                        $objetos =
                                            $objetos .
                                            $o->persona0->nombre_apellidos .
                                            '<br/>';
                                    }
                                }
                                $descripcion =
                                    'El informe de la ' .
                                    $inspeccion->tipoHomologacion->tipo .
                                    ' con No. de Control: ' .
                                    $inspeccion->numero_control .
                                    ', en el apartado de Inspecciones a Personal y relacionado con las personas siguientes: <br/>' .
                                    $objetos .
                                    ' fue elaborado y revisado por el Inspector Actuante ' .
                                    $user->nombre_apellidos .
                                    ' y ahora se está pendiente de su aprobación';
                            } else {
                                Yii::$app->user->setFlash(
                                    'warning',
                                    'Se produjo el siguiente error enviando la notificación por correo: Error de acceso en la base de datos. Inténtelo luego.'
                                );
                                return false;
                            }
                            break;
                        case 'inspeccionLabServ':
                            $search = InspeccionesXServiciosProductos::findAll(
                                'inspeccion = :i',
                                array(':i' => $inspeccion->id)
                            );
                            $objetos = '';
                            if ($search != null) {
                                foreach ($search as $o) {
                                    if ($objetos == '') {
                                        $objetos =
                                            $o->servcios0->servicio . '<br/>';
                                    } else {
                                        $objetos =
                                            $objetos .
                                            $o->servcios0->servicio .
                                            '<br/>';
                                    }
                                }
                                $descripcion =
                                    'El informe de la ' .
                                    $inspeccion->tipoHomologacion->tipo .
                                    ' con No. de Control: ' .
                                    $inspeccion->numero_control .
                                    ', en el apartado de Inspecciones a Servicios y relacionado con los servicios siguientes: <br/>' .
                                    $objetos .
                                    ' fue elaborado y revisado por el Inspector Actuante ' .
                                    $user->nombre_apellidos .
                                    ' y ahora se está pendiente de su aprobación';
                            } else {
                                Yii::$app->user->setFlash(
                                    'warning',
                                    'Se produjo el siguiente error enviando la notificación por correo: Error de acceso en la base de datos. Inténtelo luego.'
                                );
                                return false;
                            }
                            break;
                        case 'inspeccionProducto':
                            $search = InspeccionesXProductos::findAll(
                                'inspeccion = :i',
                                array(':i' => $inspeccion->id)
                            );
                            $objetos = '';
                            if ($search != null) {
                                foreach ($search as $o) {
                                    if ($objetos == '') {
                                        $objetos =
                                            $o->producto0->nombre . '<br/>';
                                    } else {
                                        $objetos =
                                            $objetos .
                                            $o->producto0->nombre .
                                            '<br/>';
                                    }
                                }
                                $descripcion =
                                    'El informe de la ' .
                                    $inspeccion->tipoHomologacion->tipo .
                                    ' con No. de Control: ' .
                                    $inspeccion->numero_control .
                                    ', en el apartado de Inspecciones a Productos y relacionado con los productos siguientes: <br/>' .
                                    $objetos .
                                    ' fue elaborado y revisado por el Inspector Actuante ' .
                                    $user->nombre_apellidos .
                                    ' y ahora se está pendiente de su aprobación';
                            } else {
                                Yii::$app->user->setFlash(
                                    'warning',
                                    'Se produjo el siguiente error enviando la notificación por correo: Error de acceso en la base de datos. Inténtelo luego.'
                                );
                                return false;
                            }
                            break;
                        case 'inspeccionDac':
                            $search = InspeccionesXDacs::findAll(
                                'inspeccion = :i',
                                array(':i' => $inspeccion->id)
                            );
                            $objetos = '';
                            if ($search != null) {
                                foreach ($search as $o) {
                                    if ($objetos == '') {
                                        $objetos = $o->dac0->nombre . '<br/>';
                                    } else {
                                        $objetos =
                                            $objetos .
                                            $o->dac0->nombre .
                                            '<br/>';
                                    }
                                }
                                $descripcion =
                                    'El informe de la ' .
                                    $inspeccion->tipoHomologacion->tipo .
                                    ' con No. de Control: ' .
                                    $inspeccion->numero_control .
                                    ', en el apartado de Inspecciones a DACs y relacionado con los dispositivos siguientes: <br/>' .
                                    $objetos .
                                    ' fue elaborado y revisado por el Inspector Actuante ' .
                                    $user->nombre_apellidos .
                                    ' y ahora se está pendiente de su aprobación';
                            } else {
                                Yii::$app->user->setFlash(
                                    'warning',
                                    'Se produjo el siguiente error enviando la notificación por correo: Error de acceso en la base de datos. Inténtelo luego.'
                                );
                                return false;
                            }
                            break;
                        case 'inspeccionMIT':
                            $search = InspeccionesXMediosIzado::findAll(
                                'inspeccion = :i',
                                array(':i' => $inspeccion->id)
                            );
                            $objetos = '';
                            if ($search != null) {
                                foreach ($search as $o) {
                                    if ($objetos == '') {
                                        $objetos =
                                            $o->medioIzado->nombre . '<br/>';
                                    } else {
                                        $objetos =
                                            $objetos .
                                            $o->medioIzado->nombre .
                                            '<br/>';
                                    }
                                }
                                $descripcion =
                                    'El informe de la ' .
                                    $inspeccion->tipoHomologacion->tipo .
                                    ' con No. de Control: ' .
                                    $inspeccion->numero_control .
                                    'en el apartado de Inspecciones a Productos y Medios de Izado y relacionado con los objetos siguientes: <br/>' .
                                    $objetos .
                                    ' fue elaborado y revisado por el Inspector Actuante ' .
                                    $user->nombre_apellidos .
                                    ' y ahora se está pendiente de su aprobación';
                            } else {
                                Yii::$app->user->setFlash(
                                    'warning',
                                    'Se produjo el siguiente error enviando la notificación por correo: Error de acceso en la base de datos. Inténtelo luego.'
                                );
                                return false;
                            }
                            break;
                    }

                    $mailer->setData(array(
                        'subject' => $subject,
                        'description' => $descripcion
                    ));
                    try {
                        if ($mailer->send(null)) {
                            $nombre = $dest[0]->nombre_apellidos;
                            array_push($detinatarios, $nombre);
                        }
                    } catch (Exception $e) {
                        Yii::$app->user->setFlash(
                            'warning',
                            'Se produjo el siguiente error enviando la notificación por correo: ' .
                                $e->getMessage()
                        );
                        return false;
                    }
                } else {
                    Yii::$app->user->setFlash(
                        'warning',
                        'Se produjo el siguiente error enviando la notificación por correo: El destinatario de la notificación no posee dirección de correo electrónico definida'
                    );
                    return false;
                }
            } else {
                $mailer->setSubject($asunto);
                $subject = 'Informe de Inspección pendiente de su aprobación';
                $descripcion = '';
                switch ($type) {
                    case 'inspeccionPersona':
                        $search = InspeccionesXPersonas::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos =
                                        $o->persona0->nombre_apellidos .
                                        '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->persona0->nombre_apellidos .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a Personal y relacionado con las personas siguientes: <br/>' .
                                $objetos .
                                ' fue elaborado y revisado por el Inspector Actuante ' .
                                $user->nombre_apellidos .
                                ' y ahora se está pendiente de su aprobación';
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: Error de acceso en la base de datos. Inténtelo luego.'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionLabServ':
                        $search = InspeccionesXServiciosProductos::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos =
                                        $o->servcios0->servicio . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->servcios0->servicio .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a Servicios y relacionado con los servicios siguientes: <br/>' .
                                $objetos .
                                ' fue elaborado y revisado por el Inspector Actuante ' .
                                $user->nombre_apellidos .
                                ' y ahora se está pendiente de su aprobación';
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: Error de acceso en la base de datos. Inténtelo luego.'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionProducto':
                        $search = InspeccionesXProductos::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos = $o->producto0->nombre . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->producto0->nombre .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a Productos y relacionado con los productos siguientes: <br/>' .
                                $objetos .
                                ' fue elaborado y revisado por el Inspector Actuante ' .
                                $user->nombre_apellidos .
                                ' y ahora se está pendiente de su aprobación';
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: Error de acceso en la base de datos. Inténtelo luego.'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionDac':
                        $search = InspeccionesXDacs::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos = $o->dac0->nombre . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos . $o->dac0->nombre . '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                ', en el apartado de Inspecciones a DACs y relacionado con los dispositivos siguientes: <br/>' .
                                $objetos .
                                ' fue elaborado y revisado por el Inspector Actuante ' .
                                $user->nombre_apellidos .
                                ' y ahora se está pendiente de su aprobación';
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: Error de acceso en la base de datos. Inténtelo luego.'
                            );
                            return false;
                        }
                        break;
                    case 'inspeccionMIT':
                        $search = InspeccionesXMediosIzado::findAll(
                            'inspeccion = :i',
                            array(':i' => $inspeccion->id)
                        );
                        $objetos = '';
                        if ($search != null) {
                            foreach ($search as $o) {
                                if ($objetos == '') {
                                    $objetos = $o->medioIzado->nombre . '<br/>';
                                } else {
                                    $objetos =
                                        $objetos .
                                        $o->medioIzado->nombre .
                                        '<br/>';
                                }
                            }
                            $descripcion =
                                'El informe de la ' .
                                $inspeccion->tipoHomologacion->tipo .
                                ' con No. de Control: ' .
                                $inspeccion->numero_control .
                                'en el apartado de Inspecciones a Productos y Medios de Izado y relacionado con los objetos siguientes: <br/>' .
                                $objetos .
                                ' fue elaborado y revisado por el Inspector Actuante ' .
                                $user->nombre_apellidos .
                                ' y ahora se está pendiente de su aprobación';
                        } else {
                            Yii::$app->user->setFlash(
                                'warning',
                                'Se produjo el siguiente error enviando la notificación por correo: Error de acceso en la base de datos. Inténtelo luego.'
                            );
                            return false;
                        }
                        break;
                }

                $mailer->setData(array(
                    'subject' => $subject,
                    'description' => $descripcion
                ));
                try {
                    foreach ($dest as $d) {
                        $destinatario = User::find()->where(
                            $d->usuario0->iduser
                        );
                        if (
                            $destinatario != null &&
                            $destinatario->email != null
                        ) {
                            $mailer->setTo($destinatario->email);
                            if ($mailer->send(null)) {
                                $nombre = $d->nombre_apellidos . '<br/>';
                                array_push($detinatarios, $nombre);
                            }
                        }
                    }
                } catch (Exception $e) {
                    Yii::$app->user->setFlash(
                        'warning',
                        'Se produjo el siguiente error enviando la notificación por correo: ' .
                            $e->getMessage()
                    );
                    return false;
                }
            }
        } else {
            Yii::$app->user->setFlash(
                'warning',
                'Se produjo el siguiente error enviando la notificación por correo: No existe un funcionario registrado como destinatario de la notificación'
            );
            return false;
        }
        Yii::$app->user->setFlash(
            'success',
            'Fueron notificados los siguientes funcionarios: ' .
                implode('<br/>', $detinatarios)
        );
        return true;
    }
}
