<?php

namespace app\components;

use yii;
use app\models\Funcionarios;
use app\models\Oficinas;
use app\models\Inspecciones;
use app\models\AprobacionXFuncionarios;

/**
 * Created by PhpStorm.
 * User: geo
 * Date: 21/02/16
 * Time: 11:05.
 */
class Notificaciones
{
    //Seccion para Cuadros
    public function getInspeccionesPendientesAprobacionPersonas()
    {
        $user = Funcionarios::findOne(['usuario' => Yii::$app->user->id]);

        $band = false;
        if ($user == null) {
            return null;
        }

        $cope = Oficinas::findOne(['oficina_superior = id']);

        if ($user->oficina0->id == $user->oficina0->oficinaSuperior->id) {
            $band = true;
            $inspecciones = Inspecciones::find()
                ->with('inspeccionesXPersonas')
                ->where('cerrada = 1 AND (estado = 1 OR estado = 3)')
                ->all();
        } else {
            if ($user->oficina0->oficinaSuperior->id == $cope->id) {
                $inspecciones = Inspecciones::find()
                    ->with('inspeccionesXPersonas')
                    ->where(
                        'oficina = :o AND cerrada = 1 AND (estado = 1 OR estado = 3)',
                        array(':o' => $user->oficina0->id)
                    )
                    ->all();
            } else {
                $inspecciones = Inspecciones::find()
                    ->with('inspeccionesXPersonas')
                    ->where(
                        'oficina = :o AND cerrada = 1 AND (estado = 1 OR estado = 3)',
                        array(':o' => $user->oficina0->oficinaSuperior->id)
                    )
                    ->all();
            }
        }

        $result = array();
        if ($inspecciones != null) {
            foreach ($inspecciones as $i) {
                $AprobacionXFuncionarios = new AprobacionXFuncionarios();
                $ap = AprobacionXFuncionarios::findAll(
                    'inspeccion = :insp AND funcionario = :f AND aprobada = 1 AND valida = 1',
                    array(':insp' => $i->id, ':f' => $user->id)
                );
                if (count($ap) == 0) {
                    if (
                        $band &&
                        $user->cargo0->id != $user->cargo0->superior0->id
                    ) {
                        if (
                            $AprobacionXFuncionarios->aprobadaEnOficina($i->id)
                        ) {
                            array_push($result, $i);
                        }
                    } else {
                        if ($user->cargo0->id != $user->cargo0->superior0->id) {
                            if (
                                $AprobacionXFuncionarios->aprobadaNivelPrevio(
                                    $i,
                                    $user
                                )
                            ) {
                                array_push($result, $i);
                            }
                        } else {
                            if (
                                $AprobacionXFuncionarios->aprobadaEnOficina(
                                    $i->id
                                ) &&
                                $AprobacionXFuncionarios->aprobadaNivelPrevio(
                                    $i,
                                    $user
                                )
                            ) {
                                array_push($result, $i);
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function getInspeccionesPendientesAprobacionSP()
    {
        $user = Funcionarios::find()->where(['usuario' => Yii::$app->user->id])->one();
        $band = false;
        if ($user == null) {
            return null;
        }
        $cope = Oficinas::find()->where('oficina_superior = id')->one();
        if ($user->oficina0->id == $user->oficina0->oficinaSuperior->id) {
            $band = true;
            $inspecciones = Inspecciones::find()
                ->with('inspeccionesXServiciosProductos')
                ->where('cerrada = 1 AND (estado = 1 OR estado = 3)')
                ->all();
        } else {
            if ($user->oficina0->oficinaSuperior->id == $cope->id) {
                $inspecciones = Inspecciones::find()
                    ->with('inspeccionesXServiciosProductos')
                    ->where(
                        'oficina = :o AND cerrada = 1 AND (estado = 1 OR estado = 3)',
                        array(':o' => $user->oficina0->id)
                    )
                    ->all();
            } else {
                $inspecciones = Inspecciones::find()
                    ->with('inspeccionesXServiciosProductoses')
                    ->where(
                        'oficina = :o AND cerrada = 1 AND (estado = 1 OR estado = 3)',
                        array(':o' => $user->oficina0->oficinaSuperior->id)
                    )
                    ->all();
            }
        }

        $result = array();
        if ($inspecciones != null) {
            foreach ($inspecciones as $i) {
                $AprobacionXFuncionarios = new AprobacionXFuncionarios();
                $ap = AprobacionXFuncionarios::findAll(
                    'inspeccion = :insp AND funcionario = :f AND aprobada = 1 AND valida = 1',
                    array(':insp' => $i->id, ':f' => $user->id)
                );
                if (count($ap) == 0) {
                    if (
                        $band &&
                        $user->cargo0->id != $user->cargo0->superior0->id
                    ) {
                        if (
                            $AprobacionXFuncionarios->aprobadaEnOficina($i->id)
                        ) {
                            array_push($result, $i);
                        }
                    } else {
                        if ($user->cargo0->id != $user->cargo0->superior0->id) {
                            if (
                                $AprobacionXFuncionarios->aprobadaNivelPrevio(
                                    $i,
                                    $user
                                )
                            ) {
                                array_push($result, $i);
                            }
                        } else {
                            if (
                                $AprobacionXFuncionarios->aprobadaEnOficina(
                                    $i->id
                                ) &&
                                $AprobacionXFuncionarios->aprobadaNivelPrevio(
                                    $i,
                                    $user
                                )
                            ) {
                                array_push($result, $i);
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function getInspeccionesPendientesAprobacionProd()
    {
        $band = false;
        $user = Funcionarios::find()->where(['usuario' => Yii::$app->user->id])->one();
        if ($user == null) {
            return null;
        }

        $cope = Oficinas::find()->where('oficina_superior = id');
        if ($user->oficina0->id == $user->oficina0->oficinaSuperior->id) {
            $band = true;
            $inspecciones = Inspecciones::find()
                ->with('inspeccionesXProductos')
                ->where(
                    'cerrada = 1 AND (estado = 1 OR estado = 3)'
                )
                ->all();
        } else {
            if ($user->oficina0->oficinaSuperior->id == $cope->id) {
                $inspecciones = Inspecciones::find()
                    ->with('inspeccionesXProductos')
                    ->where(
                        'oficina = :o AND cerrada = 1 AND (estado = 1 OR estado = 3)',
                        array(':o' => $user->oficina0->id)
                    )
                    ->all();
            } else {
                $inspecciones = Inspecciones::find()
                    ->with('inspeccionesXProductoses')
                    ->where(
                        'oficina = :o AND cerrada = 1 AND (estado = 1 OR estado = 3)',
                        array(':o' => $user->oficina0->oficinaSuperior->id)
                    )
                    ->all();
            }
        }

        $result = array();
        if ($inspecciones != null) {
            foreach ($inspecciones as $i) {
                $AprobacionXFuncionarios = new AprobacionXFuncionarios();
                $ap = AprobacionXFuncionarios::findAll(
                    'inspeccion = :insp AND funcionario = :f AND aprobada = 1 AND valida = 1',
                    array(':insp' => $i->id, ':f' => $user->id)
                );
                if (count($ap) == 0) {
                    if (
                        $band &&
                        $user->cargo0->id != $user->cargo0->superior0->id
                    ) {
                        if (
                            $AprobacionXFuncionarios->aprobadaEnOficina($i->id)
                        ) {
                            array_push($result, $i);
                        }
                    } else {
                        if ($user->cargo0->id != $user->cargo0->superior0->id) {
                            if (
                                $AprobacionXFuncionarios->aprobadaNivelPrevio(
                                    $i,
                                    $user
                                )
                            ) {
                                array_push($result, $i);
                            }
                        } else {
                            if (
                                $AprobacionXFuncionarios->aprobadaEnOficina(
                                    $i->id
                                ) &&
                                $AprobacionXFuncionarios->aprobadaNivelPrevio(
                                    $i,
                                    $user
                                )
                            ) {
                                array_push($result, $i);
                            }
                        }
                    }
                }
            }
        } else {
            return null;
        }

        return $result;
    }

    public function getInspeccionesPendientesAprobacionDacs()
    {
        $user = Funcionarios::find()->where(['usuario' => Yii::$app->user->id])->one();
        $band = false;
        if ($user == null) {
            return null;
        }
        //Si esta en COPE
        $cope = Oficinas::find()->where('oficina_superior = id');
        if ($user->oficina0->id == $user->oficina0->oficinaSuperior->id) {
            $band = true;
            $inspecciones = Inspecciones::find()
                ->with('inspeccionesXDacs')
                ->where(
                    'cerrada = 1 AND (estado = 1 OR estado = 3)'
                )
                ->all();
        } else {
            //Si esta en la oficina
            if ($user->oficina0->oficinaSuperior->id == $cope->id) {
                $inspecciones = Inspecciones::find()
                    ->with('inspeccionesXDacs')
                    ->where(
                        'oficina = :o AND cerrada = 1 AND (estado = 1 OR estado = 3)',
                        array(':o' => $user->oficina0->id)
                    )
                    ->all();
            } else {
                //Si esta en una suboficina
                $inspecciones = Inspecciones::find()
                    ->with('inspeccionesXDacses')
                    ->where(
                        'oficina = :o AND cerrada = 1 AND (estado = 1 OR estado = 3)',
                        array(':o' => $user->oficina0->oficinaSuperior->id)
                    )
                    ->all();
            }
        }

        $result = array();
        if ($inspecciones != null) {
            foreach ($inspecciones as $i) {
                $AprobacionXFuncionarios = new AprobacionXFuncionarios();
                $ap = AprobacionXFuncionarios::findAll(
                    'inspeccion = :insp AND funcionario = :f AND aprobada = 1 AND valida = 1',
                    array(':insp' => $i->id, ':f' => $user->id)
                );
                if (count($ap) == 0) {
                    if (
                        $band &&
                        $user->cargo0->id != $user->cargo0->superior0->id
                    ) {
                        if (
                            $AprobacionXFuncionarios->aprobadaEnOficina($i->id)
                        ) {
                            array_push($result, $i);
                        }
                    } else {
                        if ($user->cargo0->id != $user->cargo0->superior0->id) {
                            if (
                                $AprobacionXFuncionarios->aprobadaNivelPrevio(
                                    $i,
                                    $user
                                )
                            ) {
                                array_push($result, $i);
                            }
                        } else {
                            if (
                                $AprobacionXFuncionarios->aprobadaEnOficina(
                                    $i->id
                                ) &&
                                $AprobacionXFuncionarios->aprobadaNivelPrevio(
                                    $i,
                                    $user
                                )
                            ) {
                                array_push($result, $i);
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function getInspeccionesPendientesAprobacionMI()
    {
        $band = false;
        $user = Funcionarios::find()->where(['usuario' => Yii::$app->user->id])->one();
        if ($user == null) {
            return null;
        }

        $cope = Oficinas::find()->where('oficina_superior = id');
        if ($user->oficina0->id == $user->oficina0->oficinaSuperior->id) {
            $band = true;
            $inspecciones = Inspecciones::find()
                ->with('inspeccionesXMediosIzados')
                ->where(
                    'cerrada = 1 AND (estado = 1 OR estado = 3)'
                )
                ->all();
        } else {
            if ($user->oficina0->oficinaSuperior->id == $cope->id) {
                $inspecciones = Inspecciones::find()
                    ->with('inspeccionesXMediosIzados')
                    ->where(
                        'oficina = :o AND cerrada = 1 AND (estado = 1 OR estado = 3)',
                        array(':o' => $user->oficina0->id)
                    )
                    ->all();
            } else {
                $inspecciones = Inspecciones::find()
                    ->with('inspeccionesXMediosIzados')
                    ->where(
                        'oficina = :o AND cerrada = 1 AND (estado = 1 OR estado = 3)',
                        array(':o' => $user->oficina0->oficinaSuperior->id)
                    )
                    ->all();
            }
        }

        $result = array();
        if ($inspecciones != null) {
            $AprobacionXFuncionarios = new AprobacionXFuncionarios();
            foreach ($inspecciones as $i) {
                $ap = AprobacionXFuncionarios::findAll(
                    'inspeccion = :insp AND funcionario = :f AND aprobada = 1 AND valida = 1',
                    array(':insp' => $i->id, ':f' => $user->id)
                );
                if (count($ap) == 0) {
                    if (
                        $band &&
                        $user->cargo0->id != $user->cargo0->superior0->id
                    ) {
                        if (
                            $AprobacionXFuncionarios->aprobadaEnOficina($i->id)
                        ) {
                            array_push($result, $i);
                        }
                    } else {
                        if ($user->cargo0->id != $user->cargo0->superior0->id) {
                            if (
                                $AprobacionXFuncionarios->aprobadaNivelPrevio(
                                    $i,
                                    $user
                                )
                            ) {
                                array_push($result, $i);
                            }
                        } else {
                            if (
                                $AprobacionXFuncionarios->aprobadaEnOficina(
                                    $i->id
                                ) &&
                                $AprobacionXFuncionarios->aprobadaNivelPrevio(
                                    $i,
                                    $user
                                )
                            ) {
                                array_push($result, $i);
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    /*************************************************** **********************/
    /**** Seccion para Inspectores (inspecciones pendientes de cerrar)*********/
    /*************************************************** **********************/

    public function getInspeccionesPorCerrarPersonas()
    {
        $user = Funcionarios::findOne('usuario = :u', array(
            ':u' => Yii::$app->user->id
        ));
        if ($user == null) {
            return null;
        }
        $tmp = $user->oficina0;
        $oficinaCentral = Oficinas::find()->where('oficina_superior = id');
        if ($tmp->oficinaSuperior->id == $oficinaCentral->id) {
            $oficina = $tmp->id;
        } else {
            $oficina = $tmp->oficinaSuperior->id;
        }
        //buscar inspecciones pendientes de esa oficina
        $inspecciones = Inspecciones::find()
            ->with('inspeccionesXPersonas')
            ->where(
                'oficina = :o AND (cerrada = :c OR cerrada IS NULL)',
                array(':o' => $oficina, ':c' => 0)
            )
            ->all();

        return $inspecciones;
    }

    public function getInspeccionesPorCerrarSP()
    {
        $user = Funcionarios::find()->where('usuario = :u', array(
            ':u' => Yii::$app->user->id
        ));
        if ($user == null) {
            return null;
        }
        $tmp = $user->oficina0;
        $oficinaCentral = Oficinas::find()->where('oficina_superior = id');
        if ($tmp->oficinaSuperior->id == $oficinaCentral->id) {
            $oficina = $tmp->id;
        } else {
            $oficina = $tmp->oficinaSuperior->id;
        }
        //buscar inspecciones pendientes de esa oficina
        $inspecciones = Inspecciones::find()
            ->with('inspeccionesXServiciosProductos')
            ->where(
                'oficina = :o AND (cerrada = :c OR cerrada IS NULL)',
                array(':o' => $oficina, ':c' => 0)
            )
            ->all();

        return $inspecciones;
    }

    public function getInspeccionesPorCerrarProd()
    {
        $user = Funcionarios::find()->where('usuario = :u', array(
            ':u' => Yii::$app->user->id
        ));
        if ($user == null) {
            return null;
        }
        $tmp = $user->oficina0;
        $oficinaCentral = Oficinas::find()->where('oficina_superior = id');
        if ($tmp->oficinaSuperior->id == $oficinaCentral->id) {
            $oficina = $tmp->id;
        } else {
            $oficina = $tmp->oficinaSuperior->id;
        }
        //buscar inspecciones pendientes de esa oficina

        $inspecciones = Inspecciones::find()
            ->with('inspeccionesXProductos')
            ->where(
                'oficina = :o AND (cerrada = :c OR cerrada IS NULL)',
                array(':o' => $oficina, ':c' => 0)
            )
            ->all();

        return $inspecciones;
    }

    public function getInspeccionesPorCerrarDacs()
    {
        $user = Funcionarios::find()->where('usuario = :u', array(
            ':u' => Yii::$app->user->id
        ));
        if ($user == null) {
            return null;
        }
        $tmp = $user->oficina0;
        $oficinaCentral = Oficinas::find()->where('oficina_superior = id');
        if ($tmp->oficinaSuperior->id == $oficinaCentral->id) {
            $oficina = $tmp->id;
        } else {
            $oficina = $tmp->oficinaSuperior->id;
        }
        //buscar inspecciones pendientes de esa oficina

        $inspecciones = Inspecciones::find()
            ->with('inspeccionesXDacs')
            ->where(
                'oficina = :o AND (cerrada = :c OR cerrada IS NULL)',
                array(':o' => $oficina, ':c' => 0)
            )
            ->all();

        return $inspecciones;
    }

    public function getInspeccionesPorCerrarMI()
    {
        $user = Funcionarios::find()->where('usuario = :u', array(
            ':u' => Yii::$app->user->id
        ));
        if ($user == null) {
            return null;
        }
        $tmp = $user->oficina0;
        $oficinaCentral = Oficinas::find()->where('oficina_superior = id');
        if ($tmp->oficinaSuperior->id == $oficinaCentral->id) {
            $oficina = $tmp->id;
        } else {
            $oficina = $tmp->oficinaSuperior->id;
        }
        //buscar inspecciones pendientes de esa oficina

        $inspecciones = Inspecciones::find()
            ->with('inspeccionesXMediosIzados')
            ->where(
                'oficina = :o AND (cerrada = :c OR cerrada IS NULL)',
                array(':o' => $oficina, ':c' => 0)
            )
            ->all();

        return $inspecciones;
    }

    //Generales

    public function getCargoUsuarioRegistrado()
    {
        $user = Funcionarios::find()->where(['usuario' => Yii::$app->user->id])->one();
        if ($user == null) {
            return 2;
        } else {
            return $user->cargo0->id;
        }
    }

    public static function checkNextVen($date)
    {
        $cmp = strtotime($date);

        $act = strtotime('+3 months');

        return $cmp < $act && $cmp > strtotime('now');
    }

    public static function checkVen($date)
    {
        $cmp = strtotime($date);

        $act = strtotime('now');

        return $cmp < $act;
    }

    public static function changeDate($date)
    {
        $m = substr($date, 5, 2);
        $d = substr($date, 8, 2);
        $m2 = self::convertMonth($m);

        return $d . ' de ' . $m2;
    }

    private static function convertMonth($month)
    {
        switch ($month) {
            case '01':
                $m = 'enero';
                break;
            case '02':
                $m = 'febrero';
                break;
            case '03':
                $m = 'marzo';
                break;
            case '04':
                $m = 'abril';
                break;
            case '05':
                $m = 'mayo';
                break;
            case '06':
                $m = 'junio';
                break;
            case '07':
                $m = 'julio';
                break;
            case '08':
                $m = 'agosto';
                break;
            case '09':
                $m = 'septimebre';
                break;
            case '10':
                $m = 'octubre';
                break;
            case '11':
                $m = 'noviembre';
                break;
            case '12':
                $m = 'diciembre';
                break;
        }

        return $m;
    }
}
