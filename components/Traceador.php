<?php

namespace app\components;

use yii;
use yii\web\Request;
use yii\db\Expression;

use app\models\Trazas;
use app\models\Funcionarios;

/**
 * Created by PhpStorm.
 * User: levian
 * Date: 26/2/2016
 * Time: 16:33.
 */
class Traceador
{
    private $tracer;

    /**
     * Funcion que crea una traza particular de las acciones sobre los detalles de la aplicacion.
     *
     * @param $accion (posibles valores: 'acceso, edicion, creacion, eliminacion, ')
     * @param $detalle ( referido al metodo que esta llamando a la traza)
     *
     * @return bool
     */
    public static function crearTraza($accion, $detalle)
    {
        if (!Yii::$app->user->isGuest && !Yii::$app->user->isSuperAdmin) {
            $tracer = new Trazas();
            $tracer->accion = $accion;
            $tracer->detalle = $detalle;
            //            $tracer->ip = $_SERVER['REMOTE_ADDR'];
            $tracer->ip = Request::getUserHost();
            $tracer->fecha = new Expression('NOW()');
            $func = Funcionarios::find(['usuario' => Yii::$app->user->id]);
            $tracer->funcionario = $func->id;
            $tracer->save();
        }

        return true;
    }
}
