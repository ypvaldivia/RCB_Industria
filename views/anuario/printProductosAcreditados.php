<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
<page>
    <h2><?php echo $homologacion ?></h2>
    <?php
    $filename = '@web/uploads/anuario/productos/enc/' . $headerfile . '.html';
    if (file_exists($filename)) {
        echo file_get_contents($filename);
    }
    ?>
    <h4 style="text-align: center"><?php echo $homologacion ?></h4>
    <table class="table " id="main_table" style="width: 100%; font-family: calibri">
        <thead>
            <tr style="border-bottom: 1px solid black">
                <th style="text-align:left !important;"> Provincia</th>
                <th style="text-align:left !important;"> Propietario</th>
                <th style="text-align:left !important;"> Producto</th>
                <th style="text-align:left !important;"> Certificado</th>
                <th style="text-align:right !important;"> Vencimiento</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            if ($model !== null) { ?>
                <?php foreach ($model as $value) { ?>
                    <tr class="odd gradeX">
                        <td style="text-align:left !important;"><?= $value->producto0->provincia0->nombre ?? "No definida" ?></td>
                        <td style="text-align:left !important;"><?= $value->producto0->propietario0->nombre ?? "No Definido" ?></td>
                        <td style="text-align:left !important;"><?php echo $value->producto0->nombre ?? '' ?></td>
                        <td style="text-align:left !important;"><?php echo $value->certificado ?></td>
                        <td style="text-align:right !important;"><?php echo $value->vigencia ?></td>
                    </tr>
                <?php $i++;
                } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="5">Sin Valores</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</page>

</html>