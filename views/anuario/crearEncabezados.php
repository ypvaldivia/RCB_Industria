<?php

use app\assets\CKEditorAsset;
use app\assets\Select2Asset;
use yii\helpers\Html;
use yii\helpers\Url;

Select2Asset::register($this);
CKEditorAsset::register($this);

$this->title = 'Anuario';
$this->params['subtitle'] = 'Crear Encabezados';
$this->params['breadcrumbs'][] = [
    'label' => 'Anuario',
    'url' => ['anuario/edicion'],
    'template' => '<i class="mdi mdi-calendar" i></i>&nbsp;{link}&nbsp;/&nbsp;'
];
$this->params['breadcrumbs'][] = 'Crear Encabezados';
?>

<div class="card">
    <div class="card-header">
        <i class="fa fa-reorder"></i> Inspecciones
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
            <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
        </div>
    </div>
    <div class="card-body collapse show">
        <div class="alert alert-info alert-dismissible fade show" role="alert">
            <h4 class="block">Ayuda de Redacci&oacute;n </h4>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <p>
                Por favor tenga en cuenta lo siguiente a la hora de crear o editar contenido dentro del editor.
            </p>
            <ul>
                <li>Use el bot&oacute;n formato para establecer los niveles de texto(Encabezado 1, encabezado 2, etc)</li>
                <li>Jusqtifique todos los parrafos</li>
                <li>Recuerde que el editor se puede visualizar en pantalla completa</li>
            </ul>
        </div>
        <!-- BEGIN FORM-->
        <?= Html::beginForm(); ?>
        <div class="form-body">
            <h2>Creando los archivos de encabezados de secciones</h2>

            <div class="form-group">
                <?= Html::label('Archivo a modificar', 'certificado', ['class' => 'control-label']); ?>
                <?= Html::textInput('archivo', '',  ['class' => 'form-control']); ?>
            </div>
            <div class="form-group">
                <div class="form-check">
                    <?= Html::checkBox('esmit'); ?>
                    <label for="esmit" class="control-label">Encabezado sobre Medios de Izado Terrestres</label>
                </div>
            </div>

            <div class="form-group">
                <?= Html::label('Contenido del fragmento', 'contenido', array('class' => 'control-label')); ?>
                <?= Html::textArea('encabezado', '', array('size' => 60, 'class' => 'ckeditor form-control',)); ?>
            </div>

            <div class="note note-warning">
                Seleccione el elemento del anuario detr&aacute;s del cual se ubicar&aacute; el que est&aacute; editando actualmente
            </div>

            <div class="form-group">
                <?= Html::label('Orden del elemento actual', 'orden', array('class' => 'control-label')); ?>
                <?= Html::dropDownList('orden', null, $listAnuario, ['class' => 'form-control', 'id' => 'orden']); ?>
            </div>

        </div>
        <div class="form-actions fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar cambios</button>
                        <a href="<?= Url::to(['anuario/edicion']) ?>" class="btn btn-secondary">Cancelar</a>
                    </div>
                </div>
            </div>
        </div>
        <?= Html::endForm(); ?>
    </div>
</div>

<?php

$js = <<<JS
    $(document).ready(function() {
        $('#orden').select2({
            placeholder: "Seleccione un elemento",
            allowClear: true
        });
    ClassicEditor.create( $('encabezado'));
    });
JS;
$this->registerJs($js);

?>