<?php

use yii\helpers\Url;

$this->title = 'RCB';
$this->params['subtitle'] = 'Gesti&oacute;n de conformacion del anuario';
$this->params['breadcrumbs'][] = 'Generacion de anuario';
?>


<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-globe"></i>Informacion de anuario
        </h3>
    </div>
    <div class="card-body">
        <p>
            La evaluación de la conformidad es la actividad relacionada con la determinación directa o indirecta del
            cumplimiento de determinados requisitos. No está dirigida a Entidades o Instituciones, sino que es aplicable
            a productos, servicios, procesos y personal calificado; y abarca los aspectos técnico – organizativos
            vinculados a los mismos, que tienen alguna influencia sobre su calidad. Ejemplos típicos los constituyen la
            Homologación y la Aprobación.

            <p />
            <p />
            La Homologación es el reconocimiento formal y documentado que otorga el RCB a un producto, servicio o
            personal calificado que cumpla de forma integral con los requisitos de éste. La Homologación es una variante
            de la Aprobación, aplicado al ámbito del RCB como Sociedad Clasificadora.

        </p>
        <hr />
        <a class="btn btn-default" href="<?= Url::to(['anuario/crear-encabezados']) ?>"><i class="fa fa-edit"></i> Crear encabezados de secciones</a>


        <hr />
        <a class="btn btn-default" href="<?= Url::to(['anuario/modificar-encabezados', 'tipo' => 'personas']) ?>"><i class="fa fa-edit"></i> 1.1 Modificar encabezados de personas</a>

        <a class="btn btn-primary" href="<?= Url::to(['anuario/generar-personas']) ?>"><i class="fa fa-cogs"></i> 1.2 Generar fragmentos de personas</a>
        <hr />

        <a class="btn btn-default" href="<?= Url::to(['anuario/modificar-encabezados', 'tipo' => 'productos']) ?>"><i class="fa fa-edit"></i> 2.1 Modificar encabezados de productos</a>

        <a class="btn btn-primary" href="<?= Url::to(['anuario/generar-productos']) ?>"><i class="fa fa-cogs"></i> 2.2 Generar fragmentos de productos</a>
        <hr />
        <a class="btn btn-default" href="<?= Url::to(['anuario/modificar-encabezados', 'tipo' => 'servicios']) ?>"><i class="fa fa-edit"></i> 3.1 Modificar encabezados de servicios</a>

        <a class="btn btn-primary" href="<?= Url::to(['anuario/generar-servicios']) ?>"><i class="fa fa-cogs"></i> 3.2 Generar fragmentos de servicios</a>
        <hr />
        <a class="btn btn-success" href="<?= Url::to(['anuario/generar-anuario']) ?>"><i class="icon-download-alt"></i> 4 Descargar PDF</a>


    </div>
</div>