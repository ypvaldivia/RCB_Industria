<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
<page>
    <h2><?php echo $homologacion ?></h2>
    <?php
    $filename = '@web/uploads/anuario/servicios/enc/' . $headerfile . '.html';
    if (file_exists($filename)) {
        echo file_get_contents($filename);
    }
    ?>
    <h4 style="text-align: center"><?php echo $homologacion ?></h4>
    <table class="table " id="main_table" style="width: 100%; font-family: calibri">
        <thead>
            <tr style="border-bottom: 1px solid black">
                <th style="text-align:left !important;"> Provincia</th>
                <th style="text-align:left !important;"> Entidad</th>
                <th style="text-align:left !important;"> Servicio</th>
                <th style="text-align:left !important;"> Certificado</th>
                <th style="text-align:right !important;"> Vencimiento</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            if ($model !== null) { ?>
                <?php foreach ($model as $value) { ?>
                    <tr class="odd gradeX">
                        <td style="text-align:left !important;"><?= $value->servicio0->provincia0->nombre ?? "Sin Definir" ?></td>
                        <td style="text-align:left !important;"><?= $value->servicio0->entidad0->nombre ?? "Sin Definir" ?></td>
                        <td style="text-align:left !important;"><?php echo $value->servicio0->servicio ?? '' ?></td>
                        <td style="text-align:left !important;"><?php echo $value->certificado ?></td>
                        <td style="text-align:right !important;"><?php echo $value->vigencia ?></td>
                    </tr>
                <?php $i++;
                } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="5">Sin Valores</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</page>

</html>