<?php

use yii\helpers\Url;

$this->title = 'Anuario';
$this->params['subtitle'] = 'Descargar';
$this->params['breadcrumbs'][] = [
    'label' => 'Anuario',
    'url' => ['anuario/edicion'],
    'template' => '<i class="mdi mdi-calendar" i></i>&nbsp;{link}&nbsp;/&nbsp;'
];
$this->params['breadcrumbs'][] = 'Descargar';
?>

<div class="card shadow mb-4">
    <!-- Card Header - Accordion -->
    <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
        <h6 class="m-0 font-weight-bold text-primary">
            <i class="fa fa-random"></i> Descargar Registro de Homologaci&oacute;n y Certificaci&oacute;n Industrial
        </h6>
    </a>
    <!-- Card Content - Collapse -->
    <div class="collapse show" id="collapseCardExample">
        <div class="card-body">
            <div class="list-group-flush">
                <a class="list-group-item list-group-item-action" target="_blank" href="<?= Yii::getAlias('@web') . '/uploads/anuario/Anuario-Homologacion-Aprobacion-' . date('Y') . '.pdf' ?>">

                    <h4 class="block">Registro General de Productos, Servicios y Personal Homologado</h4>
                    <p>
                        <i class="fa fa-download"></i>
                        Haga Click para obtener el archivo. Esta operaci&oacute;n puede demorar algunos minutos. No cierre el navegador hasta que haya conclu&iacute;do la descarga.
                    </p>
                </a>
                <a class="list-group-item list-group-item-action" target="_blank" href="<?= Yii::getAlias('@web') . '/uploads/anuario/Anuario-MITs-' . date('Y') . '.pdf' ?>">
                    <h4 class="block">Registro de Medios de Izado Terrestres y Dispositivos Auxiliares de Carga</h4>
                    <p>
                        <i class="fa fa-download"></i>
                        Haga Click para obtener el archivo. Esta operaci&oacute;n puede demorar algunos minutos. No cierre el navegador hasta que haya conclu&iacute;do la descarga.
                    </p>
                </a>
                </ul>
            </div>
        </div>
    </div>
</div>