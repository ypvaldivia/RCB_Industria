<?php

use app\assets\DataTablesAsset;
use yii\helpers\Url;

$this->title = 'Anuario';
$this->params['subtitle'] = 'Gesti&oacute;n de la informaci&oacute;n de Anuario';
$this->params['breadcrumbs'][] = 'Anuario';
?>

<div class="card shadow">
    <div class="card-header">
        <i class="fa fa-globe"></i> Gesti&oacute;n de Anuario
        <div class="card-actions">
            <a data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
            <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
        </div>
    </div>
    <div class="card-body collapse show">
        <div class="container">
            <div class="row">
                <div class="btn-group">
                    <a href="<?= Url::to(['anuario/crear-encabezados']) ?>" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo fragmento de anuario</a>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-refresh"></i> Generar Partes
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="<?= Url::to(['anuario/generar-personas']) ?>"><i class="fa fa-user"></i> Personas</a>
                            <a class="dropdown-item" href="<?= Url::to(['anuario/generar-productos']) ?>"><i class="fa fa-wrench"></i> Productos</a>
                            <a class="dropdown-item" href="<?= Url::to(['anuario/generar-mits']) ?>"><i class="fa fa-inbox"></i> MITs</a>
                            <a class="dropdown-item" href="<?= Url::to(['anuario/generar-dacs']) ?>"><i class="fa fa-cogs"></i> DACs</a>
                            <a class="dropdown-item" href="<?= Url::to(['anuario/generar-servicios']) ?>"><i class="fa fa-map-pin"></i> Servicios</a>
                        </div>
                    </div>
                    <a class="btn btn-warning" href="#" data-toggle="modal" data-target="#ayudaModal" class="btn btn-default"><i class="fa fa-question"></i> Ayuda</a>
                </div>
            </div>
            <div class="table-responsive mt-3">
                <table id="main_table" class="table table-bordered table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Orden</th>
                            <th>Nombre</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data as $value) {
                        ?>
                            <tr class="odd gradeX">
                                <td>
                                    <label class="label label-info"> <?php echo $value->id ?></label>
                                </td>
                                <td>
                                    <label class="label label-info"> <?php echo $value->orden ?></label>
                                </td>
                                <td>
                                    <?php if (substr_count($value->dir, "personas") > 0) : ?>
                                        <label class="label label-success">Personas</label>
                                    <?php endif ?>
                                    <?php if (substr_count($value->dir, "productos") > 0) : ?>
                                        <label class="label label-danger">Productos</label>
                                    <?php endif ?>
                                    <?php if (substr_count($value->dir, "mits") > 0) : ?>
                                        <label class="label label-danger">MITs</label>
                                    <?php endif ?>
                                    <?php if (substr_count($value->dir, "dacs") > 0) : ?>
                                        <label class="label label-danger">DACs</label>
                                    <?php endif ?>
                                    <?php if (substr_count($value->dir, "servicios") > 0) : ?>
                                        <label class="label label-warning">Servicios</label>
                                    <?php endif ?>
                                    <?php if (substr_count($value->dir, "secciones") > 0) : ?>
                                        <label class="label label-primary">Secciones</label>
                                    <?php endif ?>

                                    <?php echo $value->nombre ?>
                                </td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a href="<?php echo Url::to(['anuario/update', 'id' => $value->id]) ?>" class="btn btn-info btn-sm" title="Editar"><i class="fa fa-edit"></i></a>
                                        <a href="<?php echo Url::to(['anuario/delete', 'id' => $value->id]) ?>" class="btn btn-danger btn-sm" title="Eliminar"><i class="fa fa-eraser"></i></a>
                                        <a href="<?php echo Url::to(['anuario/print-one', 'id' => $value->id]) ?>" class="btn btn-warning btn-sm" title="Imprimir"><i class="fa fa-print"></i></a>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="btn-group">
        <div class="btn-group">
            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-refresh"></i> Generar Partes
            </button>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="<?= Url::to(['anuario/generar-personas']) ?>"><i class="fa fa-user"></i> Personas</a>
                <a class="dropdown-item" href="<?= Url::to(['anuario/generar-productos']) ?>"><i class="fa fa-wrench"></i> Productos</a>
                <a class="dropdown-item" href="<?= Url::to(['anuario/generar-mits']) ?>"><i class="fa fa-inbox"></i> MITs</a>
                <a class="dropdown-item" href="<?= Url::to(['anuario/generar-dacs']) ?>"><i class="fa fa-cogs"></i> DACs</a>
                <a class="dropdown-item" href="<?= Url::to(['anuario/generar-servicios']) ?>"><i class="fa fa-map-pin"></i> Servicios</a>
            </div>
        </div>
        <a class="btn btn-large btn-success" href="<?= Url::to(['anuario/contruir-anuario']) ?>"> <i class="fa fa-cogs"></i> Contruir Anuario</a>
        <a class="btn btn-large btn-danger" href="<?= Url::to(['anuario/contruir-anuario-mits']) ?>"> <i class="fa fa-cogs"></i> Contruir Anuario para MITs</a>
    </div>
</div>
<!-- Ayuda Modal-->
<div class="modal fade" id="ayudaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ayudaModalLabel">&iquest; Por Qu&eacute; Regenerar ?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>La generaci&oacute;n consiste en popular la tabla de anuario listada en esta p&aacute;gina.</h3>
                <p>Los objetos de homologaci&oacute;n(productos, personas y servicios) se obtienen haciendo consultas a las distintas tablas de la base de datos relacionada con que cuenta esta aplicacion. Por eso una vez crados todos los fragmentos b&aacute;sicos del anuario, es necesario que se llene la tabla de fragmentos de objetos para su posterior edici&oacute;n.</p>
                <p>En esta p&aacute;gina ud puede editar casi todo el anuario, el cual esta formado por los siguientes fragmenos:</p>
                <ol>
                    <li>Portada</li>
                    <li>Tabla de Contenido</li>
                    <li>Fragmentos descriptivos (secciones)</li>
                    <li>Fragmentos de objetos(Personas, Productos, Servicios)</li>
                </ol>
                <p>Solo los dos primeros son fijos pero el resto los puede ordenar seg&uacute;n convenga para as&iacute; poder definir la confecci&oacute;n del anuario.</p>
                <h3>&iquest;Que son los fragmentos de objetos?</h3>
                <p>Los fragmentos de objetos se conforman uniendo un encabezado(editable) junto con la tabla del objeto de homologaci&oacute;n en cuesti&oacute;n.</p>
                <p>En esta p&aacute;gina ud puede editar el encabezado que va a llevar dicha tabla con la relaci&oacute;n del objeto de homologaci&oacute;n, Ordenarlos seg&uacute;n lo desee y conbinarlos con los fragmentos descriptivos para confeccionar el anuario.</p>
                <p>&nbsp;</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Lo entiendo</button>
            </div>
        </div>
    </div>
</div>

<?php
DataTablesAsset::register($this);

$js = <<<JS

    $(document).ready(function() {
        $('#main_table').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ],
            language: esES
        } );
    } );    
    
JS;
$this->registerJs($js); ?>