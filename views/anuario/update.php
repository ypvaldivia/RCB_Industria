<?php

use app\assets\CKEditorAsset;
use app\assets\Select2Asset;
use app\models\Anuario;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Anuarios';
$this->params['subtitle'] = 'Gesti&oacute;n de la informaci&oacute;n de Anuarios';
$this->params['breadcrumbs'][] = [
    'label' => 'Anuario',
    'url' => ['anuario/edicion'],
    'template' => '<i class="mdi mdi-calendar" i></i>&nbsp;{link}&nbsp;/&nbsp;'
];
$this->params['breadcrumbs'][] = 'Actualizar';
?>

<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption">
                    <i class="icon-reorder"></i>
                    <?= $model->nombre ?>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="note note-info">
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                <h4 class="block">Ayuda de Redacci&oacute;n </h4>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <p>
                                    Por favor tenga en cuenta lo siguiente a la hora de crear o editar contenido dentro del editor.
                                </p>
                                <ul>
                                    <li>Use el bot&oacute;n formato para establecer los niveles de texto(Encabezado 1, encabezado 2, etc)</li>
                                    <li>Jusqtifique todos los parrafos</li>
                                    <li>Recuerde que el editor se puede visualizar en pantalla completa</li>
                                </ul>
                            </div>
                        </div>
                        <div class="note note-warning">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                Seleccione el elemento del anuario detr&aacute;s del cual se ubicar&aacute; el que est&aacute; editando actualmente
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- BEGIN FORM-->
                <?php
                $form = ActiveForm::begin([
                    'id' => 'editarFragmento-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => false,
                ]);
                ?>
                <div class="container">
                    <?= Html::errorSummary($model);
                    ?>
                </div>
                <div class="form-body">
                    <div class="form-group">
                        <?= Html::activeLabel($model, 'orden', array('class' => 'control-label')); ?>
                        <?php $selectData =  ArrayHelper::map(Anuario::find()->orderBy('orden')->all(), 'id', 'nombre'); ?>
                        <?= Html::dropDownList('orden', $model->id, $selectData, ['class' => 'form-control', 'id' => 'orden']); ?>

                    </div>
                    <div class="form-group">
                        <?= Html::hiddenInput('dir', $model->dir, array()); ?>
                        <?= Html::hiddenInput('archivo', $model->archivo, array()); ?>
                        <?= Html::textArea('encabezado', $contenido, array('size' => 60, 'class' => 'ckeditor form-control', 'placeholder' => 'Contenido',)); ?>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary"><i class="icon-check"></i> Guardar</button>
                                <a href="<?= Url::to(['anuario/edicion']) ?>" class="btn btn-secondary">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>



<?php
CKEditorAsset::register($this);
Select2Asset::register($this);

$js = <<<JS
        alert('asd');
    $(document).ready(function() {
        $('#orden').select2({
            placeholder: "Seleccione un elemento",
            allowClear: true
        });
    ClassicEditor.create( $('encabezado'));
    });
});
JS;
$this->registerJs($js); ?>