<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
<page>
    <h2>Medios de Izado Terrestres</h2>
    <?php
    $filename = Yii::getAlias('@web') . '/uploads/anuario/mits/enc/' . $headerfile . '.html';
    if (file_exists($filename)) {
        echo file_get_contents($filename);
    }
    ?>
    <h4 style="text-align: center">Medios de Izado Terrestres</h4>
    <table class="table " id="main_table" style="width: 100%; font-family: calibri">
        <thead>
            <tr style="border-bottom: 1px solid black">
                <th style="text-align:left !important;"> Provincia</th>
                <th style="text-align:left !important;"> Propietario</th>
                <th style="text-align:left !important;"> Producto</th>
                <th style="text-align:left !important;"> Cap. Izado (kg)</th>
                <th style="text-align:left !important;"> Certificado</th>
                <th style="text-align:right !important;"> Vencimiento</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            if ($model !== null) { ?>
                <?php foreach ($model as $value) { ?>

                    <tr class="odd gradeX">
                        <td style="text-align:left !important;"><?= $value->medioIzaje->provincia0->nombre ?? "No definida" ?></td>
                        <td style="text-align:left !important;"><?= $value->medioIzaje->propietario0->nombre ?? "No Definido" ?></td>
                        <td style="text-align:left !important;"><?= $value->medioIzaje->nombre ?? '' ?></td>
                        <td style="text-align:left !important;">
                            <?php if ($value->medioIzaje->ci_certificada != null && $value->medioIzaje->ci_certificada > 0) {
                                echo 'GP: ' . $value->medioIzaje->ci_certificada;
                            } ?>
                            <?php if ($value->medioIzaje->ci_aux_certificada != null && $value->medioIzaje->ci_aux_certificada > 0) {
                                echo ', GA: ' . $value->medioIzaje->ci_aux_certificada;
                            } ?>
                            <?php if ($value->medioIzaje->ci_aux1_certificada != null && $value->medioIzaje->ci_aux1_certificada > 0) {
                                echo ', GA 1: ' . $value->medioIzaje->ci_aux1_certificada;
                            } ?>
                        </td>
                        <td style="text-align:left !important;"><?php echo $value->certificado ?></td>
                        <td style="text-align:right !important;"><?php echo $value->vigencia ?></td>
                    </tr>
                <?php $i++;
                } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="5">Sin Valores</td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</page>

</html>