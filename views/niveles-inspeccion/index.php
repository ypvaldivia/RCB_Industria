<?php

use app\assets\DataTablesAsset;
use yii\helpers\Url;

?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Niveles Inspecci&oacute;n <small>Gesti&oacute;n de la informaci&oacute;n de Niveles Inspecci&oacute;n </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="icon-barcode"></i>
    <a href="#">Inspecciones - </a>
    <a href="#">Niveles Inspecci&oacute;n </a>
</li>
<?php $this->endBlock(); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?> <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong>
        <?php echo Yii::$app->session->getFlash('success'); ?> </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?> <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong>
        <?php echo Yii::$app->session->getFlash('error'); ?> </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?> <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong>
        <?php echo Yii::$app->session->getFlash('warning'); ?> </div>
<?php } ?>

<br />
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-globe"></i>Gesti&oacute;n de Niveles Inspecci&oacute;n
        </h3>
    </div>
    <div class="card-body">

        <table class="table table-striped table-bordered table-hover" id="main_table">
            <thead>
                <tr>
                    <th>Nivel</th>
                    <th>Cargo Certificado Definitivo</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $value) {
                ?>
                    <tr class="odd gradeX">
                        <td><?php echo $value->nivel ?></td>
                        <td><?php echo $value->cargo_certificado_definitivo ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
    } );
JS;
$this->registerJs($js); ?>