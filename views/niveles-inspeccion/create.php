<?php

use yii\helpers\Url;

?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Niveles Inspecciones <small>Gesti&oacute;n de la informaci&oacute;n de Niveles Inspecciones </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="icon-barcode"></i>
    <a href="#">Inspecciones - </a>
    <a href="<?php echo Url::to('/NivelesInspeccion/index') ?>">Niveles Inspecciones </a>
    <a href="#">Nuevo </a>
</li>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>