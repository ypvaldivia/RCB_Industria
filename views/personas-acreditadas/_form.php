<?php

use app\assets\DropifyAsset;
use app\assets\Select2Asset;
use app\models\Entidades;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

DropifyAsset::register($this);
Select2Asset::register($this);
?>

<div class="card card-outline-success shadow">
    <div class="card-header">
        <h4 class="text-white">
            <?php if ($model->isNewRecord) { ?>
                <i class="fa fa-plus"></i> Adicionar persona acreditada
            <?php } else { ?>
                <i class="fa fa-edit"></i> Editando persona acreditada ( <?= $model->nombre_apellidos ?? '' ?> )
            <?php  } ?>
            <div class="card-actions">
                <a class="text-white" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize text-white" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close text-white" data-action="close"><i class="ti-close"></i></a>
            </div>
        </h4>
    </div>
    <div class="card-body collapse show">
        <?php
        $form = ActiveForm::begin([
            'enableClientValidation' => true,
            'options' => [
                'class' => 'form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
        ]) ?>
        <?= $form->field($model, 'nombre_apellidos') ?>

        <div class="row">
            <div class="col-3">
                <?= $form->field($model, 'ci') ?>
            </div>
            <div class="col-3">
                <?= $form->field($model, 'telefono_contacto') ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'correo') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'lugar_nacimiento') ?>
                <?= $form->field($model, 'provincia')->dropDownList(ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'), ['class' => 'form-control select2']); ?>
                <?= $form->field($model, 'entidad')->dropDownList(ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'), ['class' => 'form-control select2']); ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'foto')->fileInput(['class' => 'dropify']) ?>
            </div>
        </div>
        <?= Html::submitButton('<i class="ti-save"></i> Guardar', ['class' => 'btn btn-primary']) ?>
        <a href="<?php echo Url::to(['personas-acreditadas/index']) ?>" class="btn btn-secondary">Cancelar</a>
        <?php ActiveForm::end() ?>
    </div>
</div>

<?php
$this->registerJsFile('@web/scripts/form-fileupload.js');
$js = <<<JS
    $(document).ready(function() {
        FormFileUpload.init();
        $('.select2').select2();
    });
JS;
$this->registerJs($js);
?>