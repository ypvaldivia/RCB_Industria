<?php

use app\assets\DataTablesAsset;
use app\models\Entidades;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\LinkPager;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Personas Acreditadas';
$this->params['subtitle'] = 'Gestión de la información de Personas Acreditadas';
$this->params['breadcrumbs'][] = 'Personas Acreditadas';
?>

<div class="card shadow">
    <div class="card-header">
        <i class="fa fa-search"></i> Búsqueda Avanzada
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-plus"></i></a>
            <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
        </div>
    </div>
    <div class="card-body collapse">
        <?php $form = ActiveForm::begin(['options' => ['class' => 'form-inline'],]) ?>

        <?php echo $form->field($model, 'ci', [
            'inputOptions' => ['class' => 'form-control form-control-sm mr-2']
        ])->textInput()->input('ci', ['placeholder' => "Carnet de Identidad"])->label(false); ?>

        <?= $form->field($model, 'nombre', [
            'inputOptions' => ['class' => 'form-control form-control-sm mr-2']
        ])->textInput()->input('nombre', ['placeholder' => "Nombre"])->label(false); ?>

        <?php echo $form->field($model, 'provincia', [
            'inputOptions' => ['class' => 'form-control form-control-sm mr-2']
        ])->dropdownList(
            ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'),
            ['style' => 'width:200px !important', 'prompt' => 'Seleccione una Provincia']
        )->label(false); ?>

        <?php echo $form->field($model, 'entidad', [
            'inputOptions' => ['class' => 'form-control form-control-sm mr-2']
        ])->dropdownList(
            ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'),
            ['style' => 'width:200px !important', 'prompt' => 'Seleccione una Entidad']
        )->label(false); ?>

        <?= Html::submitButton('<i class="ti-search"></i> Buscar', ['class' => 'form-control form-control-sm']) ?>
        <?php ActiveForm::end() ?>
    </div>
</div>

<div class="card card-outline-success shadow">
    <div class="card-header">
        <h6 class="card-title text-white"><i class="fa fa-globe"></i> Gesti&oacute;n de Personas Acreditadas
            <div class="card-actions">
                <a class="text-white" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize text-white" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close text-white" data-action="close"><i class="ti-close"></i></a>
            </div>
        </h6>
    </div>
    <div class="card-body collapse show">
        <div class="btn-group">
            <a href="<?php echo Url::to('/personas-acreditadas/create') ?>" class="btn btn-success">Nuevo <i class="fa fa-plus"></i></a>
            <a id="eliminar" href="#" class="btn btn-danger">Eliminar seleccionados <i class="fa fa-trash"></i></a>
        </div>
        <div class="row">
            <div class="col-md-4 float-left mt-2">
                Mostrando <?= count($data) ?> de <?= $pages->pageCount ?> elementos
            </div>
            <div class="col-md-8">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php echo LinkPager::widget(['pagination' => $pages]); ?>
                </div>
            </div>
        </div>
        <table id="main_table" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="table-checkbox">
                        <input id="select_all" type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                    </th>
                    <th> Nombre Apellidos</th>
                    <th> Provincia</th>
                    <th> Entidad</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $value) {
                ?>
                    <tr class="odd gradeX">
                        <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id ?>" /></td>
                        <td><?php echo $value->nombre_apellidos ?> </td>
                        <td><?php echo $value->provincia0->nombre ?></td>
                        <td><?php echo $value->entidad0->nombre ?? '' ?></td>
                        <td>
                            <div class="btn-group">
                                <a href="<?php echo Url::to(['personas-acreditadas/update', 'id' => $value->id]) ?>" title="Editar" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i></a>
                                <a href="<?php echo Url::to(['personas-acreditadas/historical', 'id' => $value->id]) ?>" title="Histórico" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <div class="row">
            <div class="col-md-4 float-left">
                Mostrando <?= count($data) ?> de <?= $pages->pageCount ?> elementos
            </div>
            <div class="col-md-8">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php echo LinkPager::widget(['pagination' => $pages]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        $('#main_table').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ],
            language: esES,
            paging: false,
            info: false
        } );
    } );  
JS;
$this->registerJs($js); ?>