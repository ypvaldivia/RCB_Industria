<?php

$this->title = 'Personas Acreditadas';
$this->params['subtitle'] = 'Gestión de la información de Personas Acreditadas';
$this->params['breadcrumbs'][] = [
    'label' => 'Personas Acreditadas',
    'url' => ['personas-acreditadas/index'],
    'template' => '<i class="mdi mdi-account" i></i>&nbsp;{link}&nbsp;/&nbsp;'
];
$this->params['breadcrumbs'][] = 'Actualizar Persona';

echo $this->render('_form', ['model' => $model]);
