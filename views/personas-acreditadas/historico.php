<?php

use app\assets\DataTablesAsset;
use yii\helpers\Url;

$this->title = 'Personas Acreditadas';
$this->params['subtitle'] = 'Gestión de la información de Personas Acreditadas';
$this->params['breadcrumbs'][] = [
    'label' => 'Personas Acreditadas',
    'url' => ['personas-acreditadas/index'],
    'template' => '<i class="mdi mdi-account" i></i>&nbsp;{link}&nbsp;/&nbsp;'
];
$this->params['breadcrumbs'][] = 'Hist&oacute;rico de acreditaciones';
?>

<div class="card card-outline-success shadow">
    <div class="card-header">
        <h6 class="text-white"><i class="fa fa-globe"></i> Registro de acreditaciones de <?= $data->nombre_apellidos ?>
            <div class="card-actions">
                <a class="text-white" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize text-white" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close text-white" data-action="close"><i class="ti-close"></i></a>
            </div>
        </h6>
    </div>
    <div class="card-body collapse show">
        <table class="table table-striped table-bordered table-hover" id="main_table">
            <thead>
                <tr>
                    <th>Acreditaci&oacute;n</th>
                    <th>Certificado</th>
                    <th>Fecha de Emisi&oacute;n</th>
                    <th>Vigencia</th>
                    <th>No. Inspecci&oacute;n</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($certificaciones as $c) { ?>
                    <tr class="odd gradeX">
                        <td><?php echo $c->homologacion0->titulo_anuario ?></td>
                        <td><?php echo $c->certificado ?></td>
                        <td><?php echo $c->emision ?></td>
                        <td>
                            <?php if ($c->vigente > date('Y-m-d')) { ?>
                                <label class="label label-success">
                                <?php } else { ?>
                                    <label class="label label-danger">
                                    <?php } ?>
                                    <?php echo $c->vigente ?>
                                    </label>
                        </td>
                        <td><?php if ($c->inspeccion0 != null) echo $c->inspeccion0->inspeccion0->numero_control ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <a href="<?php echo Url::to(['personas-acreditadas/index']) ?>" class="btn btn-primary">Regresar</a>
    </div>
</div>

<?php
DataTablesAsset::register($this);
$this->registerJs('@web/scripts/table-managed.js'); ?>