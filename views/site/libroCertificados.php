<?php

use app\assets\DataTablesAsset;
use app\assets\Select2Asset;
use app\forms\BuscaObjetoForm;
use app\models\AcreditacionesIndustriales;
use app\models\Entidades;
use app\models\Inspecciones;
use yii\bootstrap4\LinkPager;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

DataTablesAsset::register($this);
Select2Asset::register($this);

$this->title = 'Certificados Emitidos';
$this->params['subtitle'] = 'LIBRO DE CONTROL DE CERTIFICADOS EMITIDOS DE INDUSTRIA - R6/PC401';
$this->params['breadcrumbs'][] = 'Libro de Certificados';
?>

<div class="card shadow">
    <div class="card-header">
        <i class="fa fa-search"></i> Búsqueda Avanzada
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-plus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
            <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
        </div>
    </div>
    <div class="card-body collapse">
        <?php $form = ActiveForm::begin([
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'id' => 'login-form',
            'options' => ['class' => 'form-inline'],
        ]) ?>
        <?= $form->field($searcher, 'certificado', [
            'inputOptions' => ['class' => 'form-control form-control-sm mr-2']
        ])->textInput()->input('certificado', ['placeholder' => "Certificado"])->label(false); ?>

        <?= $form->field($searcher, 'objeto', [
            'inputOptions' => ['class' => 'form-control form-control-sm mr-2']
        ])->textInput()->input('objeto', ['placeholder' => "Objeto del certificado"])->label(false); ?>


        <?php if ($type != 'inspeccionMIT' && $type != 'inspeccionDAC') { ?>

            <?= $form->field($searcher, 'homologacion', [
                'inputOptions' => ['class' => 'form-control form-control-sm mr-2 select2']
            ])->dropdownList(
                ArrayHelper::map(AcreditacionesIndustriales::find()->all(), 'id', 'titulo_anuario'),
                ['style' => 'width:200px !important', 'prompt' => 'Seleccione una Acreditacion Industrial']
            )->label(false); ?>

        <?php } ?>


        <?= $form->field($searcher, 'homologacion', [
            'inputOptions' => ['class' => 'form-control form-control-sm mr-2 select2']
        ])->dropdownList(
            ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'),
            ['style' => 'width:200px !important', 'prompt' => 'Seleccione una Entidad']
        )->label(false); ?>

        <?= $form->field($searcher, 'homologacion', [
            'inputOptions' => ['class' => 'form-control form-control-sm mr-2 select2']
        ])->dropdownList(
            BuscaObjetoForm::getMonthList(),
            ['style' => 'width:200px !important', 'prompt' => 'Seleccione un Mes']
        )->label(false); ?>

        <?= $form->field($searcher, 'homologacion', [
            'inputOptions' => ['class' => 'form-control form-control-sm mr-2 select2']
        ])->dropdownList(
            BuscaObjetoForm::getYearList(),
            ['style' => 'width:200px !important', 'prompt' => 'Seleccione un Año']
        )->label(false); ?>

        <?= $form->field($searcher, 'inspector', [
            'inputOptions' => ['class' => 'form-control form-control-sm mr-2 select2']
        ])->textInput()->input('inspector', ['placeholder' => "Código Inspector"])->label(false); ?>


        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary btn-sm']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>

<div class="card card-outline-success shadow">
    <div class="card-header">
        <h6 class="text-white">
            <i class="ti-book"></i>
            Libro de Certificados
            <?php if ($type == 'inspeccionPersona') { ?>
                de Inspecciones a Personas
            <?php }
            if ($type == 'inspeccionLabServ') { ?>
                de Inspecciones a Servicios
            <?php }
            if ($type == 'inspeccionProducto') { ?>
                de Inspecciones a Productos
            <?php }
            if ($type == 'inspeccionDAC') { ?>
                de Inspecciones a Dispositivos Auxiliares de Carga
            <?php }
            if ($type == 'inspeccionMIT') { ?>
                de Inspecciones a Medios de Izado Terrestres
            <?php } ?>

            <div class="card-actions">
                <a class="text-white" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize text-white" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close text-white" data-action="close"><i class="ti-close"></i></a>
            </div>
        </h6>
    </div>
    <div class="card-body collapse show">
        <div class="d-flex flex-row-reverse">
            <?= LinkPager::widget(['pagination' => $pages]); ?>
        </div>
        <div class="d-flex flex-row">
            Mostrando <?= count($model) ?> de <?= $pages->pageCount ?> elementos
        </div>
        <table class="table table-striped table-bordered table-hover" id="main_table">
            <thead>
                <tr>
                    <th rowspan="2" style="text-align: center; font-size: small; vertical-align: middle" width="5%">N&deg;</th>
                    <th rowspan="2" style="text-align: center; font-size: small; vertical-align: middle" width="10%">N&deg; Certificado</th>
                    <th rowspan="2" style="text-align: center; font-size: small; vertical-align: middle" width="20%">Entidad</th>
                    <th rowspan="2" style="text-align: center; font-size: small; vertical-align: middle" width="20%">Objeto del Certificado</th>
                    <th rowspan="2" style="text-align: center; font-size: small; vertical-align: middle" width="5%">C&oacute;digo Inspector</th>
                    <th rowspan="2" style="text-align: center; font-size: small; vertical-align: middle" width="20%">Datos Complementarios</th>
                    <th colspan="2" style="text-align: center; font-size: small" width="10%">Validez</th>
                </tr>
                <tr>
                    <th style="text-align: center; font-size: small">Emitido</th>
                    <th style="text-align: center; font-size: small">Vigente</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = ($pages->page * 30) + 1;
                foreach ($model as $value) {
                ?>
                    <tr class="odd gradeX">
                        <td><?php echo $i ?></td>
                        <td><?php echo $value->certificado ?></td>
                        <?php if ($type == 'inspeccionPersona') { ?>
                            <td><?php echo ($value->persona0->entidad0 != null) ? $value->persona0->entidad0->nombre : "No definida"; ?></td>
                            <td><?php echo $value->persona0->nombre_apellidos ?></td>
                        <?php } elseif ($type == 'inspeccionLabServ') { ?>
                            <td><?php echo ($value->servicio0->entidad0 != null) ? $value->servicio0->entidad0->nombre : "No definida"; ?></td>
                            <td><?php echo substr($value->servicio0->servicio, 0, 100) . '...' ?></td>
                        <?php } elseif ($type == 'inspeccionProducto') { ?>
                            <td><?php if ($value->producto0->propietario0 != null) {
                                    echo $value->producto0->propietario0->nombre;
                                } else {
                                    echo "No Definido";
                                } ?></td>
                            <td><?php echo $value->producto0->nombre ?></td>
                        <?php } elseif ($type == 'inspeccionDAC') { ?>
                            <td><?php if ($value->dac0->propietario0 != null) {
                                    echo $value->dac0->propietario0->nombre;
                                } else {
                                    echo "No Definido";
                                } ?></td>
                            <td><?php echo $value->dac0->nombre ?></td>
                        <?php } elseif ($type == 'inspeccionMIT') { ?>
                            <td><?php if ($value->medioIzaje->propietario0 != null) {
                                    echo $value->medioIzaje->propietario0->nombre;
                                } else {
                                    echo "No Definido";
                                } ?></td>
                            <td><?php echo $value->medioIzaje->nombre ?></td>
                        <?php } ?>
                        <td>
                            <?php
                            if ($type == 'inspeccionProducto' || $type == 'inspeccionDAC') {
                                if ($value->inspeccion0 != null && $value->inspeccion0->getTieneVisitas()) {
                                    foreach ($value->inspeccion0->getInspectoresActuantes() as $inspector) {
                                        echo $inspector->codigo;
                                        echo ', ';
                                    }
                                }
                            } else {
                                if ($value->inspeccion0 != null && $value->inspeccion0->inspeccion0->getTieneVisitas()) {
                                    foreach ($value->inspeccion0->inspeccion0->getInspectoresActuantes() as $inspector) {
                                        echo $inspector->codigo;
                                        echo ', ';
                                    }
                                }
                            }
                            ?>
                        </td>
                        <?php if ($type == 'inspeccionPersona') { ?>
                            <td>
                                <?php
                                echo 'CI: ' . $value->persona0->ci;
                                echo '<br/>';
                                echo $value->homologacion0->titulo_anuario;
                                if ($value->inspeccion0 != null) {
                                    echo '<br/>';
                                    echo $value->inspeccion0->inspeccion0->tipoHomologacion->tipo;
                                }
                                ?>
                            </td>
                        <?php } elseif ($type == 'inspeccionLabServ' || $type == 'inspeccionProducto') { ?>
                            <td>&nbsp;</td>
                        <?php } elseif ($type == 'inspeccionMIT') { ?>
                            <td>
                                <?php if ($value->medioIzaje->ci_certificada != null && $value->medioIzaje->ci_certificada > 0) {
                                    echo 'GP: ' . $value->medioIzaje->ci_certificada . ' Kg';
                                } ?>
                                <?php if ($value->medioIzaje->ci_aux_certificada != null && $value->medioIzaje->ci_aux_certificada > 0) {
                                    echo ', GA: ' . $value->medioIzaje->ci_aux_certificada . ' Kg';
                                } ?>
                                <?php if ($value->medioIzaje->ci_aux1_certificada != null && $value->medioIzaje->ci_aux1_certificada > 0) {
                                    echo ', GA 1: ' . $value->medioIzaje->ci_aux1_certificada . ' Kg';
                                } ?>
                            </td>
                        <?php } elseif ($type == 'inspeccionDAC') { ?>
                            <td><?php if ($value->dac0->ct_certificada != null && $value->dac0->ct_certificada > 0) {
                                    echo $value->dac0->ct_certificada . ' Kg';
                                } ?></td>
                        <?php } ?>
                        <?php $Inspecciones = new Inspecciones(); ?>
                        <?php if ($type == 'inspeccionProducto' || $type == 'inspeccionLabServ' || $type == 'inspeccionMIT' || $type == 'inspeccionDAC') { ?>
                            <td><?php echo $Inspecciones->convertirFechaShort($value->emision); ?></td>
                            <td><?php echo $Inspecciones->convertirFechaShort($value->vigencia); ?></td>
                        <?php } else { ?>
                            <td><?php echo $Inspecciones->convertirFechaShort($value->emision); ?></td>
                            <td><?php echo $Inspecciones->convertirFechaShort($value->vigente); ?></td>
                        <?php } ?>
                    </tr>
                <?php $i++;
                } ?>
            </tbody>
        </table>
        <div class="d-flex flex-row">
            Mostrando <?= count($model) ?> de <?= $pages->pageCount ?> elementos
        </div>
        <div class="d-flex flex-row-reverse">
            <?= LinkPager::widget(['pagination' => $pages]); ?>
        </div>
    </div>
</div>

<?php
$js = <<<JS
    $(document).ready(function() {
        $('#main_table').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'excel', 'pdf', 'print'
            ],
            language: esES,
            paging: false,
            info: false
        } );
    } );  
JS;
$this->registerJs($js); ?>