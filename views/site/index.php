<?php

use app\components\Notificaciones;
use yii\helpers\Url;

$this->title = 'Panel de Control';
$this->params['breadcrumbs'][] = 'Panel de Control';
$this->params['subtitle'] = 'Estad&iacute;sticas y res&uacute;menes';
?>

<h5 class="text-primary"><i class="fa fa-dashboard"></i> Resumen General de Homologaciones</h5>
<hr />

<!-- Panel informativo -->
<div class="row">

    <!-- Personas -->
    <div class="col-lg-3 col-md-6">
        <div class="card shadow">
            <div class="card-body">
                <div class="d-flex flex-row">
                    <div class="round round-lg align-self-center round-primary"><i class="ti-user"></i></div>
                    <div class="m-l-10 align-self-center">
                        <h5 class="text-muted m-b-0">Personas</h5>
                        <h3 class="m-b-0 font-light"><?php echo $stats['totalPersonas']; ?></h3>
                        <a href="<?= Url::to(['site/libro-certificados', 'type' => 'inspeccionPersona']) ?>">Ir a la Sección</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Productos -->
    <div class="col-lg-3 col-md-6">
        <div class="card shadow">
            <div class="card-body">
                <div class="d-flex flex-row">
                    <div class="round round-lg align-self-center round-success"><i class="mdi mdi-barcode-scan"></i></div>
                    <div class="m-l-10 align-self-center">
                        <h5 class="text-muted m-b-0">Productos</h5>
                        <h3 class="m-b-0 font-light"><?php echo $stats['totalProductos']; ?></h3>
                        <a href="<?= Url::to(['/site/libro-certificados', 'type' => 'inspeccionProducto']) ?>">Ir a la Sección</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Servicios -->
    <div class="col-lg-3 col-md-6">
        <div class="card shadow">
            <div class="card-body">
                <div class="d-flex flex-row">
                    <div class="round round-lg align-self-center round-danger"><i class="mdi mdi-cart-outline"></i></div>
                    <div class="m-l-10 align-self-center">
                        <h5 class="text-muted m-b-0">Servicios</h5>
                        <h3 class="m-b-0 font-light"><?php echo $stats['totalServicios']; ?></h3>
                        <a href="<?= Url::to(['site/libro-certificados', 'type' => 'inspeccionLabServ']) ?>">Ir a la Sección</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- DACs y MITs -->
    <div class="col-lg-3 col-md-6">
        <div class="card shadow">
            <div class="card-body">
                <div class="d-flex flex-row">
                    <div class="round round-lg align-self-center round-warning"><i class="mdi mdi-link-variant"></i></div>
                    <div class="m-l-10 align-self-center">
                        <h5 class="text-muted m-b-0">DACs y MITs</h5>
                        <h3 class="m-b-0 font-light"><?php echo $stats['totalMedios']; ?></h3>
                        <a href="<?= Url::to(['site/libro-certificados', 'type' => 'inspeccionMIT']) ?>">Ir a la Sección</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Panel informativo -->

<!-- Acreditaciones y Sesion -->
<div class="card-deck">
    <div class="card card-outline-success shadow">
        <div class="card-header text-white">
            <i class="fa fa-certificate"></i> Estado de sus Acreditaciones (<?= Yii::$app->user->identity->username ?>)
            <div class="card-actions">
                <a class="btn-minimize text-white" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close text-white" data-action="close"><i class="ti-close"></i></a>
            </div>
        </div>
        <div class="card-body scroller">
            <ul class="list-group-flush">
                <?php if ($acreditacionesInspector != null) { ?>
                    <?php foreach ($acreditacionesInspector as $ac) { ?>
                        <li>
                            <div class="col1">
                                <div class="cont">
                                    <div class="cont-col1">
                                        <?php if (
                                            Notificaciones::checkNextVen(
                                                $ac->vencimiento
                                            )
                                        ) { ?>
                                            <div class="label label-sm label-warning">
                                                <i class="icon-warning-sign"></i>
                                            </div>
                                        <?php } elseif (
                                            Notificaciones::checkVen(
                                                $ac->vencimiento
                                            )
                                        ) { ?>
                                            <div class="label label-sm label-danger">
                                                <i class="fa fa-trash"></i>
                                            </div>
                                        <?php } else { ?>
                                            <div class="label label-sm label-info">
                                                <i class="fa fa-check-square-o"></i>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="cont-col2">
                                        <div class="desc">
                                            <?= $ac->acreditacion0->grupo ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col2">
                                <div class="date">
                                    <?= $ac->vencimiento ?>
                                </div>
                            </div>
                        </li>
                    <?php }
                } else { ?>
                    <li class="list-group-item">
                        <i class="fa fa-check-square-o"></i>
                        Usted no tiene acreditaciones registradas
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="card card-outline-warning shadow">
        <div class="card-header text-white">
            <i class="fa fa-lock"></i> Datos de su &uacute;ltima sesi&oacute;n en el sistema
            <div class="card-actions">
                <a class="btn-minimize text-white" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close text-white" data-action="close"><i class="ti-close"></i></a>
            </div>
        </div>
        <div class="card-body scroller">
            <div class="row">
                <div class="col-md-6">
                    <p><strong>Ingreso al sistema</strong></p>
                    <p><?php echo ($ultimaSesion != null) ? date(DATE_RFC822, $ultimaSesion[0]->user->created_at) : 'Primera sesi&oacute;n'; ?></p>
                </div>
                <div class="col-md-6">
                    <p><strong>Salida al sistema</strong></p>
                    <p><?php if (
                            $ultimaSesion != null &&
                            $ultimaSesion[0] != null
                        ) { ?>
                            <?php if (
                                $ultimaSesion[0]->user->updated_at != null
                            ) {
                                echo date(
                                    DATE_RFC822,
                                    $ultimaSesion[0]->user->updated_at
                                );
                            } else {
                                echo 'No se registró la salida';
                            } ?>
                        <?php } else { ?>
                            Primera sesi&oacute;n
                        <?php } ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <p><strong>Oficina</strong></p>
                    <p><?= $oficina != null
                            ? $oficina->codigo
                            : 'Administrador' ?></p>
                </div>
                <div class="col-md-6">
                    <p><strong>IP de acceso</strong></p>
                    <p><?php if (
                            $ultimaSesion != null &&
                            $ultimaSesion[0] != null
                        ) { ?>
                            <?= $ultimaSesion[0]->ip ?>
                        <?php } else { ?>
                            Primera sesi&oacute;n
                        <?php } ?></p>
                </div>
            </div>
            <div class="row">
                <?php if ($lastOps != null) { ?>
                    <ul class="list-group">
                        <p><strong>&Uacute;ltimas operaciones</strong></p>
                        <?php foreach ($lastOps as $op) { ?>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-1"><i class="fa fa-check"></i></div>
                                    <div class="col-lg-7"><?= $op->detalle ?></div>
                                    <div class="col-lg-4"><span class="badge badge-light"><?= $op->fecha ?></span></div>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- Acreditaciones y Sesion -->

<!--Alertas de vencimientos para clientes-->
<div class="card shadow mt-5">
    <div class="card-header">
        <i class="fa fa-check"></i> Alertas de vencimientos para clientes
        <div class="card-actions">
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
            <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
        </div>
        <ul class="nav nav-tabs card-header-tabs mt-2" id="alertvc-list" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#Personal" role="tab" aria-controls="Personal" aria-selected="true">Personal</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#Servicios" role="tab" aria-controls="Servicios" aria-selected="false">Servicios</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#Productos" role="tab" aria-controls="Productos" aria-selected="false">Productos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#Dispositivos" role="tab" aria-controls="Dispositivos" aria-selected="false">Dispositivos Auxiliares</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#Medios" role="tab" aria-controls="Medios" aria-selected="false">Medios de Izado</a>
            </li>
        </ul>
    </div>
    <div class="card-body scroller">
        <div class="tab-content mt-3">
            <div class="tab-pane active" id="Personal" role="tabpanel">
                <?php if ($psxv != null) { ?>
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="badge badge-warning">
                                    <?= count($psxv) ?></span> Certificados de Personal por vencer
                            </h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <?php foreach ($psxv
                                    as $persona) { ?>
                                    <li class="list-group-item">
                                        <div class="task-title">
                                            <span class="label label-sm label-danger"><?= Notificaciones::changeDate(
                                                                                            $persona->vigente
                                                                                        ) ?></span>
                                            <?= $persona
                                                ->persona0
                                                ->nombre_apellidos ?>
                                            <button class="btn popovers" data-trigger="hover" data-container="body" data-placement="bottom" data-content="
                                                E-Mail: <?= $persona->persona0
                                                            ->entidad0 != null
                                                            ? $persona->persona0
                                                            ->entidad0->correo
                                                            : '' ?>&nbsp;&verbar;&nbsp;
                                                Provincia: <?= $persona
                                                                ->persona0->entidad0 != null
                                                                ? $persona->persona0
                                                                ->entidad0->provincia0
                                                                ->nombre
                                                                : '' ?>&nbsp;&verbar;&nbsp;
                                                Telf: <?= $persona->persona0
                                                            ->entidad0 != null
                                                            ? $persona->persona0
                                                            ->entidad0
                                                            ->telefono_contacto
                                                            : '' ?>&nbsp;&verbar;&nbsp;
                                                Contacto: <?= $persona->persona0
                                                                ->entidad0 != null
                                                                ? $persona->persona0
                                                                ->entidad0
                                                                ->persona_contacto
                                                                : '' ?>" data-original-title="<?= $persona
                                                                                                    ->persona0
                                                                                                    ->entidad0 !=
                                                                                                    null
                                                                                                    ? $persona
                                                                                                    ->persona0
                                                                                                    ->entidad0
                                                                                                    ->nombre
                                                                                                    : 'Sin Definir' ?>">
                                                <small><?= $persona
                                                            ->persona0
                                                            ->entidad0 !=
                                                            null
                                                            ? $persona
                                                            ->persona0
                                                            ->entidad0
                                                            ->nombre
                                                            : 'Entidad sin definir' ?></small>
                                            </button>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-warning alert-dismissable">
                        <h5><i class="icon-info-sign"></i> No hay certificados por vencer actualmente</h5>
                    </div>
                <?php } ?>
            </div>

            <div class="tab-pane" id="Servicios" role="tabpanel" aria-labelledby="Servicios-tab">
                <?php if ($sxv != null) { ?>
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="badge badge-warning"><?= count(
                                                                                            $sxv
                                                                                        ) ?></span> Certificados de Servicios por vencer</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <?php foreach ($sxv
                                    as $servicio) { ?>
                                    <li class="list-group-item">
                                        <div class="task-title">
                                            <span class="label label-sm label-danger"><?= Notificaciones::changeDate(
                                                                                            $servicio->vigencia
                                                                                        ) ?></span>
                                            <?= $servicio->servicio0
                                                ->servicio ?>
                                            &nbsp;
                                            <?= $servicio->servicio0
                                                ->departamento ?>
                                            <button class="btn popovers" data-trigger="hover" data-container="body" data-placement="bottom" data-content="
                                                    E-Mail: <?= $servicio
                                                                ->servicio0->entidad0 !=
                                                                null
                                                                ? $servicio->servicio0
                                                                ->entidad0->correo
                                                                : '' ?>&nbsp;&verbar;&nbsp;
                                                    Provincia: <?= $servicio
                                                                    ->servicio0->entidad0 !=
                                                                    null
                                                                    ? $servicio->servicio0
                                                                    ->entidad0
                                                                    ->provincia0->nombre
                                                                    : '' ?>&nbsp;&verbar;&nbsp;
                                                    Telf: <?= $servicio
                                                                ->servicio0->entidad0 !=
                                                                null
                                                                ? $servicio->servicio0
                                                                ->entidad0
                                                                ->telefono_contacto
                                                                : '' ?>&nbsp;&verbar;&nbsp;
                                                    Contacto: <?= $servicio
                                                                    ->servicio0->entidad0 !=
                                                                    null
                                                                    ? $servicio->servicio0
                                                                    ->entidad0
                                                                    ->persona_contacto
                                                                    : '' ?>" data-original-title="<?= $servicio
                                                                                                        ->servicio0
                                                                                                        ->entidad0 !=
                                                                                                        null
                                                                                                        ? $servicio
                                                                                                        ->servicio0
                                                                                                        ->entidad0
                                                                                                        ->nombre
                                                                                                        : 'Sin Definir' ?>">
                                                <small><?= $servicio
                                                            ->servicio0
                                                            ->entidad0 !=
                                                            null
                                                            ? $servicio
                                                            ->servicio0
                                                            ->entidad0
                                                            ->nombre
                                                            : 'Entidad sin definir' ?></small>
                                            </button>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-warning alert-dismissable">
                        <h5><i class="icon-info-sign"></i> No hay certificados por vencer actualmente</h5>
                    </div>
                <?php } ?>
            </div>

            <div class="tab-pane" id="Productos" role="tabpanel" aria-labelledby="Productos-tab">
                <?php if ($pxv != null) { ?>
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="badge badge-warning"><?= count(
                                                                                            $pxv
                                                                                        ) ?></span> Certificados de Productos por vencer</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <?php foreach ($pxv
                                    as $producto) { ?>
                                    <li class="list-group-item">
                                        <div class="task-title">
                                            <span class="label label-sm label-danger"><?= Notificaciones::changeDate(
                                                                                            $producto->vigencia
                                                                                        ) ?></span>
                                            <?= $producto
                                                ->producto0
                                                ->nombre ?>
                                            <button class="btn popovers" data-trigger="hover" data-container="body" data-placement="bottom" data-content="
                                        E-Mail: <?= $producto->producto0
                                                    ->propietario0 != null
                                                    ? $producto->producto0->propietario0
                                                    ->correo
                                                    : '' ?>&nbsp;&verbar;&nbsp;
                                        Provincia: <?= $producto->producto0
                                                        ->propietario0 != null
                                                        ? $producto->producto0->propietario0
                                                        ->provincia0->nombre
                                                        : '' ?>&nbsp;&verbar;&nbsp;
                                        Telf: <?= $producto->producto0
                                                    ->propietario0 != null
                                                    ? $producto->producto0->propietario0
                                                    ->telefono_contacto
                                                    : '' ?>&nbsp;&verbar;&nbsp;
                                        Contacto: <?= $producto->producto0
                                                        ->propietario0 != null
                                                        ? $producto->producto0->propietario0
                                                        ->persona_contacto
                                                        : '' ?>" data-original-title="<?= $producto
                                                                                            ->producto0
                                                                                            ->propietario0 !=
                                                                                            null
                                                                                            ? $producto
                                                                                            ->producto0
                                                                                            ->propietario0
                                                                                            ->nombre
                                                                                            : 'Sin Definir' ?>">
                                                <small><?= $producto
                                                            ->producto0
                                                            ->propietario0 !=
                                                            null
                                                            ? $producto
                                                            ->producto0
                                                            ->propietario0
                                                            ->nombre
                                                            : 'Propietario sin definir' ?></small>
                                            </button>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-warning alert-dismissable">
                        <h5><i class="icon-info-sign"></i> No hay certificados por vencer actualmente</h5>
                    </div>
                <?php } ?>
            </div>

            <div class="tab-pane" id="Dispositivos" role="tabpanel" aria-labelledby="Dispositivos-tab">
                <?php if ($dxv != null) { ?>
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="badge badge-warning"><?= count(
                                                                                            $dxv
                                                                                        ) ?></span> Certificados de Dispositivos Auxiliares de Carga por vencer</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <?php foreach ($dxv
                                    as $dac) { ?>
                                    <li class="list-group-item">
                                        <div class="task-title">
                                            <span class="label label-sm label-danger"><?= Notificaciones::changeDate(
                                                                                            $dac->vigencia
                                                                                        ) ?></span>
                                            <b><?= $dac->dac0
                                                    ->tipo0 != null
                                                    ? $dac->dac0
                                                    ->tipo0->tipo
                                                    : 'Gen&eacute;rico sin definir' ?>: </b> <?= $dac
                                                                                                    ->dac0->nombre ?>
                                            <button class="btn popovers" data-trigger="hover" data-container="body" data-placement="bottom" data-content="
                                                    E-Mail: <?= $dac->dac0
                                                                ->propietario0 != null
                                                                ? $dac->dac0
                                                                ->propietario0
                                                                ->correo
                                                                : '' ?>&nbsp;&verbar;&nbsp;
                                                    Provincia: <?= $dac->dac0
                                                                    ->propietario0 != null
                                                                    ? $dac->dac0
                                                                    ->propietario0
                                                                    ->provincia0->nombre
                                                                    : '' ?>&nbsp;&verbar;&nbsp;
                                                    Telf: <?= $dac->dac0
                                                                ->propietario0 != null
                                                                ? $dac->dac0
                                                                ->propietario0
                                                                ->telefono_contacto
                                                                : '' ?>&nbsp;&verbar;&nbsp;
                                                    Contacto: <?= $dac->dac0
                                                                    ->propietario0 != null
                                                                    ? $dac->dac0
                                                                    ->propietario0
                                                                    ->persona_contacto
                                                                    : '' ?>" data-original-title="<?= $dac
                                                                                                        ->dac0
                                                                                                        ->propietario0 !=
                                                                                                        null
                                                                                                        ? $dac
                                                                                                        ->dac0
                                                                                                        ->propietario0
                                                                                                        ->nombre
                                                                                                        : 'Sin Definir' ?>">
                                                <small><?= $dac
                                                            ->dac0
                                                            ->propietario0 !=
                                                            null
                                                            ? $dac->dac0
                                                            ->propietario0
                                                            ->nombre
                                                            : 'Propietario sin definir' ?></small>
                                            </button>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-warning alert-dismissable">
                        <h5><i class="icon-info-sign"></i> No hay certificados por vencer actualmente</h5>
                    </div>
                <?php } ?>
            </div>

            <div class="tab-pane" id="Medios" role="tabpanel" aria-labelledby="Medios-tab">
                <?php if ($dxv != null) { ?>
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="badge badge-warning"><?= count(
                                                                                            $mxv
                                                                                        ) ?></span> Certificados de Medios de Izado Terrestres por vencer</h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group">
                                <?php foreach ($mxv
                                    as $mit) { ?>
                                    <li class="list-group-item">
                                        <div class="task-title">
                                            <span class="label label-sm label-danger"><?= Notificaciones::changeDate(
                                                                                            $mit->vigencia
                                                                                        ) ?></span>
                                            <b>
                                                <?= $mit
                                                    ->medioIzaje
                                                    ->tipo0 !=
                                                    null
                                                    ? $mit
                                                    ->medioIzaje
                                                    ->tipo0
                                                    ->tipo
                                                    : '' ?>:
                                            </b>
                                            <?= $mit
                                                ->medioIzaje
                                                ->nombre ?> (<?= $mit
                                                                    ->medioIzaje->categoria0 != null
                                                                    ? $mit->medioIzaje->categoria0->categoria
                                                                    : 'Categor&iacute;a no definida' ?>)
                                            <button class="btn popovers" data-trigger="hover" data-container="body" data-placement="bottom" data-content="
                                        E-Mail: <?= $mit->medioIzaje
                                                    ->propietario0 != null
                                                    ? $mit->medioIzaje->propietario0
                                                    ->correo
                                                    : '' ?>&nbsp;&verbar;&nbsp;
                                        Provincia: <?= $mit->medioIzaje
                                                        ->propietario0 != null
                                                        ? $mit->medioIzaje->propietario0
                                                        ->provincia0->nombre
                                                        : '' ?>&nbsp;&verbar;&nbsp;
                                        Telf: <?= $mit->medioIzaje
                                                    ->propietario0 != null
                                                    ? $mit->medioIzaje->propietario0
                                                    ->telefono_contacto
                                                    : '' ?>&nbsp;&verbar;&nbsp;
                                        Contacto: <?= $mit->medioIzaje
                                                        ->propietario0 != null
                                                        ? $mit->medioIzaje->propietario0
                                                        ->persona_contacto
                                                        : '' ?>" data-original-title="<?= $mit
                                                                                            ->medioIzaje
                                                                                            ->propietario0 !=
                                                                                            null
                                                                                            ? $mit
                                                                                            ->medioIzaje
                                                                                            ->propietario0
                                                                                            ->nombre
                                                                                            : 'Sin Definir' ?>">
                                                <small><?= $mit
                                                            ->medioIzaje
                                                            ->propietario0 !=
                                                            null
                                                            ? $mit
                                                            ->medioIzaje
                                                            ->propietario0
                                                            ->nombre
                                                            : 'Sin Definir' ?></small>
                                            </button>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-warning alert-dismissable">
                        <h5><i class="icon-info-sign"></i> No hay certificados por vencer actualmente</h5>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!--Alertas de vencimientos para clientes-->

<!--Estado de acreditaciones de inspectores-->
<?php if (!$esInspector) { ?>
    <div class="card shadow">
        <div class="card-header">
            <i class="fa fa-briefcase"></i>
            Estado de acreditaciones de inspectores <?= 'en ' . $oficina->codigo ?? '' ?>
            <ul class="nav nav-tabs card-header-tabs mt-2" id="inspcope-list" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#Vencimientos3Meses" role="tab" aria-controls="Vencimientos3Meses" aria-selected="true">Vencimientos en 3 meses</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#AcreditacionesXVencer" role="tab" aria-controls="AcreditacionesXVencer" aria-selected="false">Acreditaciones Por Vencer</a>
                </li>
            </ul>
        </div>
        <div class="card-body scroller">
            <div class="tab-content">
                <div class="tab-pane active" id="Vencimientos3Meses" role="tabpanel" aria-labelledby="Vencimientos3Meses-tab">
                    <?php if ($acreditacionesPorVencer != null) { ?>
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title">Acreditaciones Por Vencer</h3>
                            </div>
                            <div class="panel-body">
                                <ul class="list-group">
                                    <?php foreach ($acreditacionesPorVencer
                                        as $oficina) { ?>
                                        <li class="list-group-item"><strong><?php echo $oficina->oficina; ?></strong> <span class="badge badge-warning">
                                                <?php echo count(
                                                    $oficina->inspectores
                                                ); ?></span></li>
                                        <ul class="list-group">
                                            <?php foreach ($oficina->inspectores
                                                as $inspector) { ?>
                                                <li class="list-group-item">
                                                    <strong><?php echo $inspector->nombre; ?> ( <?php echo $inspector->codigo; ?> )</strong>
                                                    <ul class="list-group" style="border-top: 1px solid #DDD; margin-bottom: 2px;">
                                                        <?php foreach ($inspector->homologaciones
                                                            as $homologacion) { ?>
                                                            <li class="list-group-item" style="padding: 3px 15px !important; border:none ">
                                                                <?php echo $homologacion->servicio; ?>
                                                                <span class="badge badge-warning"><?php echo $homologacion->vence; ?></span>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-warning alert-dismissable">
                            <h5><i class="icon-info-sign"></i> No hay acreditaciones por vencer actualmente</h5>
                        </div>
                    <?php } ?>
                </div>

                <div class="tab-pane" id="AcreditacionesXVencer" role="tabpanel" aria-labelledby="AcreditacionesXVencer-tab">
                    <?php if ($acreditacionesVencidas != null) { ?>
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">Acreditaciones Vencidas </h3>
                            </div>
                            <div class="panel-body">
                                <ul class="list-group">
                                    <?php foreach ($acreditacionesVencidas
                                        as $oficina) { ?>
                                        <li class="list-group-item"><strong><?php echo $oficina->oficina; ?></strong> <span class="badge badge-alert">
                                                <?php echo count(
                                                    $oficina->inspectores
                                                ); ?></span></li>
                                        <ul class="list-group">
                                            <?php foreach ($oficina->inspectores
                                                as $inspector) { ?>
                                                <li class="list-group-item">
                                                    <strong><?php echo $inspector->nombre; ?> ( <?php echo $inspector->codigo; ?> )</strong>
                                                    <ul class="list-group" style="border-top: 1px solid #DDD; margin-bottom: 2px;">

                                                        <?php foreach ($inspector->homologaciones
                                                            as $homologacion) { ?>
                                                            <li class="list-group-item" style="padding: 3px 15px !important; border:none ">
                                                                <?php echo $homologacion->servicio; ?>
                                                                <span class="badge badge-alert"><?php echo $homologacion->vence; ?></span>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <h5><i class="icon-warning-sign"></i> No hay acreditaciones vencidas
                                actualmente
                            </h5>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!--Estado de acreditaciones de inspectores-->

<?php

$js = <<<JS
$('#alertvc-list a').on('click', function (e) {
  e.preventDefault()
  $(this).tab('show')
});
$('#inspcope-list a').on('click', function (e) {
  e.preventDefault()
  $(this).tab('show')
});
JS;
$this->registerJs($js);
?>