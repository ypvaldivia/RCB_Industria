<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Error del Sistema
<?php $this->endBlock(); ?>

<br />
<div class="row">
	<div class="col-md-12 page-500">
		<div class=" number">
			<?php echo $code; ?>
		</div>
		<div class=" details">
			<?php
			$errorMainText = "";
			$errorDetail = "";
			switch ($code) {
				case 400:
					$errorMainText = "Petición no válida";
					$errorDetail = "La solicitud contiene una sintaxis incorrecta.";
					break;
				case 401:
					$errorMainText = "No autorizado";
					$errorDetail = "El archivo solicitado requiere autenticación (nombre de usuario y contraseña autorizados).";
					break;
				case 403:
					$errorMainText = "Prohibido";
					$errorDetail = "El servidor no permite acceder el archivo solicitado. ";
					break;
				case 404:
					$errorMainText = "Recurso No Encontrado";
					$errorDetail = "El servidor no pudo encontrar el archivo solicitado";
					break;
				case 500:
					$errorMainText = "Error interno del servidor";
					$errorDetail = "El servidor ha encontrado una condición inesperada y no puede conseguir información especifica acerca de la condición del error.";
					break;
				case 501:
					$errorMainText = "No implementado";
					$errorDetail = "El servidor no apoya el método HTTP enviado por el cliente. El servidor web debe ser actualizado.";
					break;
				case 502:
					$errorMainText = "Puerta de enlace no válida";
					$errorDetail = "Hay mala comunicación de IP entre computadoras en el sistema interno de la red. Puede que su ISP esté sobrecargado o el cortafuegos no funciona correctamente";
					break;
				case 503:
					$errorMainText = "Servicio no disponible";
					$errorDetail = "El servidor no puede manejar solicitudes debido a una sobrecarga temporal o está temporalmente cerrado por mantenimiento.";
					break;
				case 504:
					$errorMainText = "Tiempo de la Puerta de enlace expiró";
					$errorDetail = "Un servidor en la cadena de retransmisiones no recibió un mensaje a tiempo de otro servidor más arriba en la cadena";
					break;
				case 505:
					$errorMainText = "Versión de HTTP no aceptado";
					$errorDetail = "El protocolo no fue especificado correctamente por su computadora. Contacte a su administrador para que la reconfigure.";
					break;
				case 509:
					$errorMainText = "Limite de ancho de banda alcanzado";
					$errorDetail = "El límite de acnho de banda impuesto por el administrador del sistema se ha alcanzado.";
					break;
			}; ?>
			<h3><?= $errorMainText ?></h3>
			<p>
				<?= $errorDetail ?>
			</p>
			<p>
				<?= Html::encode($message); ?><br /><br />
				<button type="button" class="btn btn-primary" onclick="history.back();"><i class="icon-arrow-left"></i> Regresar</button>
			</p>
		</div>
	</div>
</div>