<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Categor&iacute;as del Personal <small>Gesti&oacute;n de la informaci&oacute;n de Categor&iacute;as </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a class="breadcumb-item" href="#"><i class="fa fa-briefcase"></i> Estructuras y Servicios</a>
<a class="breadcumb-item" href="<?php echo Url::to('/CategoriasFuncionarios/index') ?>">Categor&iacute;as </a>
<span class="breadcumb-item active">
    Actualizar categor&iacute;a
</span>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model));
?>