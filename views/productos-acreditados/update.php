<?php

use yii\helpers\Url;

?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Productos Acreditados <small>Gesti&oacute;n de la informaci&oacute;n de Productos Acreditados </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#"><i class="fa fa-cogs"></i>Objetos de Inspecci&oacute;n</a>
<li>
    <a href="<?php echo Url::to('/productos-acreditados/index') ?>">Productos </a>
    <i class="fa fa-angle-right"></i>
</li>
<li class="active">
    Actualizar
</li>

<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model));
?>