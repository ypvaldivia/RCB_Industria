<?php

use app\assets\DataTablesAsset;
use app\models\Entidades;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\LinkPager;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = 'Productos Acreditados';
$this->params['subtitle'] = 'Gesti&oacute;n de la informaci&oacute;n de Productos Acreditados';
$this->params['breadcrumbs'][] = [
    'label' => 'Objetos de Inspecci&oacute;n',
    'url' => ['personas-acreditadas/index'],
    'template' => '<i class="ti-receipt" i></i>&nbsp;{link}&nbsp;/&nbsp;'
];
$this->params['breadcrumbs'][] = 'Productos';
?>

<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fa fa-search"></i> B&uacute;squeda avanzada
        </h3>
    </div>
    <div class="card-body">

        <?php $form = ActiveForm::begin(['options' => ['class' => 'form-inline'],]) ?>

        <div class="form-group mb-2">
            <?= $form->field($model, 'nombre', [
                'inputOptions' => ['class' => 'form-control mr-2']
            ])->textInput()->input('nombre', ['placeholder' => "Nombre"])->label(false); ?>
        </div>

        <div class="form-group mb-2">
            <?php echo $form->field($model, 'provincia', [
                'inputOptions' => ['class' => 'form-control mr-2']
            ])->dropdownList(
                ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'),
                ['style' => 'width:200px !important', 'prompt' => 'Seleccione una Provincia']
            )->label(false); ?>
        </div>

        <div class="form-group mb-2">
            <?php echo $form->field($model, 'propietario', [
                'inputOptions' => ['class' => 'form-control mr-2']
            ])->dropdownList(
                ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'),
                ['style' => 'width:200px !important', 'prompt' => 'Seleccione una Propietario']
            )->label(false); ?>
        </div>

        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary mb-2']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>
<!-- BEGIN EXAMPLE TABLE card-->
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h6 class="card-title"><i class="fa fa-globe"></i>Gesti&oacute;n de Productos Acreditados</h6>
        <div class="card-subtitle">
            Mostrando <?= count($data) ?> de <?= $pages->pageCount ?> elementos
        </div>
    </div>
    <div class="card-body">
        <div class="table-toolbar">
            <div class="btn-group">
                <a href="<?php echo Url::to('/productos-acreditados/create') ?>" class="btn btn-success">Nuevo <i class="fa fa-plus"></i></a>
                <a id="eliminar" href="#" class="btn btn-danger">Eliminar seleccionados <i class="fa fa-trash"></i></a>
            </div>
            <div class="btn-group float-right">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php
                    echo LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <table id="main_table" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="table-checkbox">
                        <input id="select_all" type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                    </th>
                    <th>Nombre</th>
                    <th>Propietario</th>
                    <th>Provincia</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $value) {
                ?>
                    <tr class="odd gradeX">
                        <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id ?>" /></td>
                        <td><?php echo $value->nombre ?></td>
                        <td><?php echo $value->propietario0->nombre ?? '' ?></td>
                        <td><?php echo $value->provincia0->nombre ?? '' ?></td>
                        <td>
                            <a href="<?php echo Url::to(['/productos-acreditados/update', 'id' => $value->id]) ?>" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Editar</a>
                            &nbsp;
                            <a href="<?php echo Url::to(['/productos-acreditados/historical', 'id' => $value->id]) ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Hist&oacute;rico</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <hr />
        <div class="row">
            <div class="col-md-4 float-left">
                Mostrando <?= count($data) ?> de <?= $pages->pageCount ?> elementos
            </div>
            <div class="col-md-8">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php echo LinkPager::widget(['pagination' => $pages,]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
    } );
JS;
$this->registerJs($js); ?>