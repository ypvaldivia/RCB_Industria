<?php

use yii\helpers\Url;

$this->title = 'Productos Acreditados';
$this->params['subtitle'] = 'Gesti&oacute;n de la informaci&oacute;n de Productos Acreditados';
$this->params['breadcrumbs'][] = [
    'label' => 'Objetos de Inspecci&oacute;n',
    'url' => ['personas-acreditadas/index'],
    'template' => '<i class="ti-receipt" i></i>&nbsp;{link}&nbsp;/&nbsp;'
];
$this->params['breadcrumbs'][] = [
    'label' => 'Productos',
    'url' => ['anuario/edicion'],
    'template' => '<i class="ti-receipt" i></i>&nbsp;{link}&nbsp;/&nbsp;'
];
$this->params['breadcrumbs'][] = 'Productos';
?>
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Productos Acreditados <small>Gesti&oacute;n de la informaci&oacute;n de Productos Acreditados </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="fa fa-cogs"></i>
    <a href="#">Objetos de Inspecci&oacute;n</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="<?php echo Url::to('/productos-acreditados/index') ?>">Productos </a>
    <i class="fa fa-angle-right"></i>
</li>
<li class="active">
    Nuevo
</li>

<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>