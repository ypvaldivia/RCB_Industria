<?php

use yii\helpers\Url;

?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Productos Acreditados <small>Gesti&oacute;n de la informaci&oacute;n de Productos Acreditados </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="fa fa-user"></i>
    <a href="#">Objetos de Inspecci&oacute;n</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <i class="fa fa-user"></i>
    <a href="<?php echo Url::to('/productos-acreditados/index') ?>">Productos Acreditados</a>
    <i class="fa fa-angle-right"></i>
</li>
<li class="active">
    Hist&oacute;rico de acreditaciones
</li><?php $this->endBlock(); ?>

<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-globe"></i>Registro de acreditaciones de <?= $data->nombre ?></h3>
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered table-hover" id="main_table">
            <thead>
                <tr>
                    <th>Acreditaci&oacute;n</th>
                    <th>Certificado</th>
                    <th>Fecha de Emisi&oacute;n</th>
                    <th>Vigencia</th>
                    <th>No. Inspecci&oacute;n</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($certificaciones as $c) { ?>
                    <tr class="odd gradeX">
                        <td><?php echo $c->homologacion0->titulo_anuario ?></td>
                        <td><?php echo $c->certificado ?></td>
                        <td><?php echo $c->emision ?></td>
                        <td>
                            <?php if ($c->vigencia > date('Y-m-d')) { ?>
                                <label class="label label-success">
                                <?php } else { ?>
                                    <label class="label label-danger">
                                    <?php } ?>
                                    <?php echo $c->vigencia ?>
                                    </label>
                        </td>
                        <td><?php if ($c->inspeccion0 != null) echo $c->inspeccion0->inspeccion0->numero_control ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<a href="<?php echo Url::to('/productos-acreditados/index') ?>" class="btn btn-primary">Regresar</a>

<?php
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/data-tables/DT_bootstrap.css', Yii::$app->session->get('dependencias'));
?>

<?php

$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/data-tables/jquery.dataTables.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/data-tables/DT_bootstrap.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/htmltable_export/tableExport.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/htmltable_export/html2canvas.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/htmltable_export/jquery.base64.js', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/plugins/htmltable_export/jspdf/libs/sprintf.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/htmltable_export/jspdf/jspdf.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/htmltable_export/jspdf/libs/base64.js', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/scripts/table-managed.js', Yii::$app->session->get('dependencias'));


$js = <<<JS
TableManaged.init(); 
JS;
$this->registerJs($js);
?>