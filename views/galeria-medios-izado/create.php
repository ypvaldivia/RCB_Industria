<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Galeria de Medios de Izado<small> Gesti&oacute;n de la informaci&oacute;n de Galeria de Medios de Izado</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#"><i class="fa fa-cogs"></i>Objetos de Inspecci&oacute;n</a>
<li>
    <a href="<?php echo Url::to('/MediosIzadoAcreditados/index') ?>">Medios de Izado Acreditados </a>
    <a href="#"> Medio: <strong><?php echo $medio->nombre ?> </strong></a>
    <i class="fa fa-angle-right"></i>
</li>
<li class="active">
    Galer&iacute;a
</li>
<?php $this->endBlock(); ?>
<div class="row">
    <div class="col-md-12">

        <div class="panel-group accordion" id="accordion1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="btn btn-primary" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1">
                            <i class="fa fa-plus-sign"></i> Adicionar Nueva Foto
                        </a>
                    </h4>
                </div>
                <div style="height: 0px;" id="collapse_1" class="panel-collapse collapse">
                    <div class="panel-body">
                        <!-- BEGIN FORM-->
                        <?php
                        $form = ActiveForm::begin();
                        ?>
                        <div class="container">
                            <br />
                            <?php echo Html::errorSummary($model);
                            ?>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <?php echo Html::activeLabel($model, 'imagen', array('class' => 'control-label')); ?>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if (!$model->imagen) { ?>
                                                    <img title="" src="<?= Yii::getAlias('@web/themes/sbadmin') ?>/assets/img/no-image.png" alt="NO hay imagen">
                                                <?php } else { ?>
                                                    <img title="" src="<?= Yii::getAlias('@web/uploads') ?>/images/medios_izado/<?php echo $model->imagen ?>" alt="">
                                                <?php } ?>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar Imagen</span>
                                                    <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                    <?= $form->field($model, 'imagen')->fileInput() ?>
                                                </span>
                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Quitar</a>
                                            </div>
                                            <span class="help-block"><?php echo Html::error($model, 'imagen', array('class' => 'text-danger')); ?></span>
                                        </div>
                                        <span class="label label-warning">Importante!</span><span> La previsualizaci&oacute;n de im&aacute;genes solo funciona en navegadores modernos.</span>
                                    </div>
                                </div>
                                <div class="col-lg-9">
                                    <div class="form-group">
                                        <?php echo Html::activeLabel($model, 'descripcion', array('class' => 'control-label')); ?>
                                        <?php echo Html::activeTextArea($model, 'descripcion', array('rows' => 6, 'cols' => 30, 'class' => 'wysihtml5 form-control')); ?>
                                        <span class="help-block"><?php echo Html::error($model, 'descripcion', array('class' => 'text-danger')); ?></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a href="<?php echo Url::to('/galeria-medios-izado/index') ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>

                        <?php ActiveForm::end() ?>
                    </div>
                    <!-- form -->
                </div>
            </div>
        </div>
    </div>

    <hr />

    <div class="card box grey">
        <div class="card-header">
            <div class="caption"><i class="fa fa-camera"></i>Fotos Existentes de <strong><?php echo $medio->nombre; ?></strong></div>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <a href="javascript:;" class="reload"></a>
            </div>
        </div>
        <div class="card-body">

            <?php if ($fotos == null) { ?>
                <div class="note note-danger">
                    <h4 class="block"> Este medio de izaje no contiene fotos a&uacute;n.</h4>
                    <p>
                        Por favor utilice el formuario de arriba para adicionar fotos al medio de izaje <strong><?php echo $medio->nombre; ?></strong>
                    </p>
                </div>

            <?php } else { ?>
                <div class="note note-info">
                    <h4 class="block"> <i class="icon-eye-open"></i> Puede maximizar las fotos haciendo clic sobre ellas....</h4>

                </div>

                <ul class="list-inline">
                    <?php foreach ($fotos as $foto) { ?>
                        <li class="">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="thumbnail" style="max-width: 100%; margin-bottom: 15px;">
                                        <a vdh-1839629148="" class="mix-preview fancybox-button" href="<?= Yii::getAlias('@web/uploads') ?>/images/medios_izado/<?php echo $foto->imagen ?>" title="<?php echo $foto->descripcion ?> " data-rel="fancybox-button">
                                            <img src="<?= Yii::getAlias('@web/uploads') ?>/images/medios_izado/<?php echo $foto->imagen ?>" alt="100%x200" style="width: 100%; height: 200px; display: block;">
                                            <div class="caption">
                                                <hr />
                                                <p>
                                                    <a href="<?php echo Url::to('/galeriamediosizado/update', array('id' => $foto->id)) ?>" class="btn btn-primary"><i class="fa fa-edit"></i> Editar</a>
                                                    <a href="<?php echo Url::to('/galeriamediosizado/delete', array('id' => $foto->id)) ?>" class="btn btn-danger"><i class="icon-remove"></i> Remover</a>
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                            </div>

                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>
    </div>

</div>


<?php

$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/fancybox/source/jquery.fancybox', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/locales/bootstrap-wysihtml5.es-ES.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fancybox/source/jquery.fancybox.pack.js', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));


$js = <<<JS
 FormComponents.init();
JS;
$this->registerJs($js);
?>