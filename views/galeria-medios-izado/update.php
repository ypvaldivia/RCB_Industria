<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Galeria de Medios de Izado<small> Gesti&oacute;n de la informaci&oacute;n de Galeria de Medios de Izado</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#"><i class="fa fa-cogs"></i>Objetos de Inspecci&oacute;n</a>
<li>
    <a href="<?php echo Url::to('/MediosIzadoAcreditados/index') ?>">Medios de Izado Acreditados </a>
    <a href="<?php echo Url::to('/GaleriaMediosIzado/create&id=' . $mi->id) ?>">Medio: <strong><?php echo $mi->nombre ?> </strong></a>
    <i class="fa fa-angle-right"></i>
</li>
<li class="active">
    Editar foto
</li>
<?php $this->endBlock(); ?>

<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title"><i class="icon-reorder"></i>Editando foto para el medio de izado: <strong> <?php echo $mi->nombre ?></strong></h3>
        <a href="javascript:;" class="collapse"></a>
        <a href="#card-config" data-toggle="modal" class="config"></a>
        <a href="javascript:;" class="reload"></a>
    </div>
    <div class="card-body">

        <!-- BEGIN FORM-->
        <?php
        $form = ActiveForm::begin([
            'id' => 'galeria-medios-izado-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'htmlOptions' => ['enctype' => 'multipart/form-data']
        ]);
        ?>
        <div class="container">
            <br />
            <?php echo Html::errorSummary(
                $model,
                'Por favor corrija los siguientes errores de ingreso:',
                null,
                array('class' => 'alert alert-danger')
            );
            ?>
        </div>
        <div class="form-body">
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'imagen', array('class' => 'control-label')); ?>
                        <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <?php if (!$model->imagen) { ?>
                                    <img title="" src="<?= Yii::getAlias('@web/themes/sbadmin') ?>/assets/img/no-image.png" alt="No hay imagen">
                                <?php } else { ?>
                                    <img title="" src="<?= Yii::getAlias('@web/uploads') ?>/images/medios_izado/<?php echo $model->imagen ?>" alt="">
                                <?php } ?>
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            <div>
                                <span class="btn btn-default btn-file">
                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar Imagen</span>
                                    <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                    <?php echo $form->fileField($model, 'imagen', array('id' => 'maxlength_alloptions', 'class' => 'default', 'maxlength' => 45)); ?>
                                </span>
                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Quitar</a>
                            </div>
                            <span class="help-block"><?php echo Html::error($model, 'imagen', array('class' => 'text-danger')); ?></span>
                        </div>
                        <span class="label label-warning">Importante!</span><span> La previsualizaci&oacute;n de im&aacute;genes solo funciona en navegadores modernos.</span>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'descripcion', array('class' => 'control-label')); ?>
                        <?php echo Html::activeTextArea($model, 'descripcion', array('rows' => 6, 'cols' => 30, 'class' => 'wysihtml5 form-control')); ?>
                        <span class="help-block"><?php echo Html::error($model, 'descripcion', array('class' => 'text-danger')); ?></span>
                    </div>

                </div>
            </div>
        </div>

        <hr />
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                <a href="<?php echo Url::to('/galeriamediosizado/index') ?>" class="btn btn-default">Cancelar</a>
            </div>
        </div>

        <?php ActiveForm::end() ?>

    </div>
</div>







<?php

$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/css/pages/portfolio.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/fancybox/source/jquery.fancybox', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/locales/bootstrap-wysihtml5.es-ES.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-mixitup/jquery.mixitup.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/fancybox/source/jquery.fancybox.pack.js', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/portfolio.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
FormComponents.init();
Portfolio.init();
JS;
$this->registerJs($js); ?>