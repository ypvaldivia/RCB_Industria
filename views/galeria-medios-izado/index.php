<?php

use app\assets\DataTablesAsset;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Galeria de Medios de Izado <small> Gesti&oacute;n de la informaci&oacute;n de Galeria de Medios de Izado</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#"><i class="fa fa-cogs"></i>Objetos de Inspecci&oacute;n</a>
<li>
    <a href="#">Galeria Medios Izado </a>
</li>
<?php $this->endBlock(); ?>
<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php } ?>
<br />
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-globe"></i>Gesti&oacute;n de Galeria Medios Izado
        </h3>
    </div>
    <div class="card-body">
        <div class="table-toolbar">
            <div class="btn-group">
                <a href="<?php echo Url::to('/galeriamediosizado/create') ?>" class="btn btn-success">Nuevo <i class="fa fa-plus"></i></a>
                <a id="eliminar" href="#" class="btn btn-danger">Eliminar seleccionados <i class="fa fa-trash"></i></a>
            </div>
            <div class="btn-group float-right">
                <button class="btn dropdown-toggle light-blue" data-toggle="dropdown">Exportar <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu float-right">
                    <li> <a href="#" onClick="$('#main_table').tableExport({type:'png',escape:'false'});"> <i class="fa fa-camera"></i> PNG</a> </li>
                    <li><a href="#" onClick="$('#main_table').tableExport({type:'pdf',escape:'false',htmlContent:'true'});"> <i class="fa fa-file"></i> PDF</a></li>
                    <li><a href="#" onClick="$('#main_table').tableExport({type:'excel',escape:'false'});"> <i class="fa fa-table"></i> XLS</a></li>
                </ul>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="main_table">
            <thead>
                <tr>
                    <th class="table-checkbox">
                        <input id="select_all" type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                    </th>
                    <th> Medio Izaje</th>
                    <th> Imagen</th>
                    <th> Descripcion</th>
                    <th>
                        &nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $value) {
                ?>
                    <tr class="odd gradeX">
                        <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id ?>" /></td>
                        <td><?php echo $value->medio_izaje0 ?></td>
                        <td><?php echo $value->imagen ?></td>
                        <td><?php echo $value->descripcion ?></td>
                        <td>
                            <a href="<?php echo Url::to('/galeriamediosizado/update', array('id' => $value->id)) ?>" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Editar</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
    } );
JS;
$this->registerJs($js); ?>