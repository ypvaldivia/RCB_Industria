<?php

use app\assets\DataTablesAsset;
use app\models\Funcionarios;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\LinkPager;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Trazas
<small>Trazas de operaciones de usuarios</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="fa fa-cogs"></i>
    <a href="#">Configuraci&oacute;m General </a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="">Gesti&oacute;n de Trazas </a>
</li>
<li class="btn-group">
    <button type="button" class="btn btn-primary" onclick="history.back();"><i class="fa fa-backward"></i> Atras
    </button>
</li><?php $this->endBlock(); ?>
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fa fa-search"></i> B&uacute;squeda avanzada
        </h3>
    </div>
    <div class="card-body">
        <?php $form = ActiveForm::begin(['options' => ['class' => 'form-inline'],]) ?>
        <div class="col-md-4">
            <?php echo Html::activeLabel($searcher, 'usuario', array('class' => 'control-label')); ?>
            <?php echo Html::activeDropDownList($searcher, 'usuario', ArrayHelper::map(Funcionarios::find()->all(), 'id', 'nombre_apellidos'), array('class' => 'form-control select2me', 'style' => 'width:200px !important', 'prompt' => '')); ?>
        </div>

        <div class="col-md-4">
            <?php echo Html::activeLabel($searcher, 'ip', array('class' => 'control-label')); ?>
            <?php echo Html::activeTextInput($searcher, 'ip', array('class' => 'form-control', 'placeholder' => '127.0.0.1')); ?>
        </div>

        <div class="col-md-4">
            <?php echo Html::activeLabel($searcher, 'accion', array('class' => 'control-label')); ?>
            <?php echo Html::activeTextInput($searcher, 'accion', array('class' => 'form-control', 'placeholder' => 'Defina la acción a buscar')); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-4">
            <?php echo Html::activeLabel($searcher, 'inicio', array('class' => 'control-label')); ?>
            <?php echo Html::activeTextInput($searcher, 'inicio', array('class' => 'form-control input-large date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
        </div>
        <div class="col-md-4">
            <?php echo Html::activeLabel($searcher, 'final', array('class' => 'control-label')); ?>
            <?php echo Html::activeTextInput($searcher, 'final', array('class' => 'form-control input-large date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
        </div>
        <div class="col-md-4">
            <br />
            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
        </div>
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary mb-2']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>
<br />
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-globe"></i>Registro de Trazas</h3>
        Mostrando <?= count($data) ?> de <?= $pages->pageCount ?> elementos
    </div>
    <div class="card-body">
        <div class="table-toolbar">
            <div class="btn-group float-right">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php echo LinkPager::widget(['pagination' => $pages,]); ?>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="main_table">
            <thead>
                <tr>
                    <th> Fecha</th>
                    <th> Ip</th>
                    <th> Funcionario</th>
                    <th> Detalle</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $value) {
                ?>
                    <tr class="odd gradeX">
                        <td>
                            <time datetime="<?php echo $value->fecha ?>"><?php echo $value->fecha ?></time>
                        </td>
                        <td><?php echo $value->ip ?></td>
                        <td><?php echo $value->funcionario0 ?></td>
                        <td><?php echo $value->detalle ?></td>

                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <hr />
        <div class="row">
            <div class="col-md-4 float-left">
                Mostrando <?= count($data) ?> de <?= $pages->pageCount ?> elementos
            </div>
            <div class="col-md-8">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php echo LinkPager::widget(['pagination' => $pages,]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
    } );
JS;
$this->registerJs($js); ?>