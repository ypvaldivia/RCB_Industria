<?php

use app\models\AcreditacionesIndustriales;
use app\models\Entidades;
use app\models\Provincias;
use app\models\TiposDacs;
use app\models\Oficinas;
use app\models\TiposHomologacion;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="fa fa-plus"></i> Adicionar Dispositivo Auxiliar de Carga</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php $form = ActiveForm::begin(); ?>
                <div class="form-body">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="control-label" for="DacsAcreditados[tipo0]">Gen&eacute;rico</label>
                                <?php echo $form2->dropDownList($dac, 'tipo0', ArrayHelper::map(TiposDacs::find()->all(), 'id', 'tipo'), array('style' => 'width:200px !important', 'prompt' => '', 'class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo $form2->error($dac, 'tipo0', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo $form2->labelEx($dac, 'ct_nominal', array('class' => 'control-label')); ?>
                                <?php echo $form2->textField($dac, 'ct_nominal', array('class' => 'form-control', 'placeholder' => 'ci_nominal')); ?>
                                <span class="help-block"><?php echo $form2->error($dac, 'ct_nominal', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php echo $form2->labelEx($dac, 'nombre', array('class' => 'control-label')); ?>
                                <?php echo $form2->textField($dac, 'nombre', array('class' => 'form-control', 'placeholder' => 'nombre', 'maxlength' => 100)); ?>
                                <span class="help-block"><?php echo $form2->error($dac, 'nombre', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo $form2->labelEx($dac, 'propietario', array('class' => 'control-label')); ?>
                                <?php echo $form2->dropDownList($dac, 'propietario0', ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...', 'required' => 'required')); ?>
                                <span class="help-block"><?php echo $form2->error($dac, 'propietario', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo $form2->labelEx($dac, 'provincia', array('class' => 'control-label')); ?>
                                <?php echo $form2->dropDownList($dac, 'provincia0', ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo $form2->error($dac, 'provincia', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-save" type="button" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a data-dismiss="modal" href="" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>