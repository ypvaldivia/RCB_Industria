<?php

use app\models\Funcionarios;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i><?= "No. Control: " . $id->numero_control ?></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php
                $form = ActiveForm::begin();
                ?> <div class="container">
                    <?php echo Html::errorSummary($model);
                    ?>
                </div>

                <div class="form-body">
                    <h4>Objetos de Inspecci&oacute;n</h4>
                    <?php
                    switch ($formType) {
                        case "inspeccionPersona": ?>
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed table-hover table-advance">
                                    <thead>
                                        <th>N&deg;</th>
                                        <th>Nombre</th>
                                        <th>CI</th>
                                        <th>Aprobaci&oacute;n</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($id->inspeccionesXPersonases as $prod) {
                                        ?>
                                            <tr>
                                                <td style="vertical-align: middle;"><?php echo $prod->numero ?></td>
                                                <td style="vertical-align: middle;"><?php echo $prod->persona0->nombre_apellidos ?></td>
                                                <td style="vertical-align: middle;"><?php echo $prod->persona0->ci ?></td>
                                                <td style="vertical-align: middle;">
                                                    <div class="make-switch switch-small" data-on-label="<i class='icon-ok icon-white'></i>" data-off-label="<i class='icon-remove'></i>">
                                                        <?php $checked = '';
                                                        if ($prod->aprobada == 1) {
                                                            $checked = 'checked';
                                                        } ?>
                                                        <input id="FormVisitaInspeccion_obj_aprobado<?= $prod->persona0->id ?>" name="FormVisitaInspeccion[obj_aprobado<?= $prod->persona0->id ?>]" type="checkbox" <?= $checked ?> class="toggle" />
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php break;
                        case "inspeccionLabServ": ?>
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed table-hover table-advance">
                                    <thead>
                                        <th>N&deg;</th>
                                        <th>Nombre</th>
                                        <th>Departamento</th>
                                        <th>Aprobaci&oacute;n</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($id->inspeccionesXServiciosProductoses as $prod) {
                                        ?>
                                            <tr>
                                                <td style="vertical-align: middle;"><?php echo $prod->numero ?></td>
                                                <td style="vertical-align: middle;"><?php echo $prod->servcios0->servicio ?></td>
                                                <td style="vertical-align: middle;"><?php echo $prod->servcios0->departamento ?></td>
                                                <td style="vertical-align: middle;">
                                                    <div class="make-switch switch-small" data-on-label="<i class='icon-ok icon-white'></i>" data-off-label="<i class='icon-remove'></i>">
                                                        <?php $checked = '';
                                                        if ($prod->aprobado == 1) {
                                                            $checked = 'checked';
                                                        } ?>
                                                        <input id="FormVisitaInspeccion_obj_aprobado<?= $prod->servcios0->id ?>" name="FormVisitaInspeccion[obj_aprobado<?= $prod->servcios0->id ?>]" type="checkbox" <?= $checked ?> class="toggle" />
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php break;
                        case "inspeccionProducto": ?>
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed table-hover table-advance">
                                    <thead>
                                        <th>N&deg;</th>
                                        <th>Nombre</th>
                                        <th>Aprobaci&oacute;n</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($id->inspeccionesXProductoses as $prod) {
                                        ?>
                                            <tr>
                                                <td style="vertical-align: middle;"><?php echo $prod->numero ?></td>
                                                <td style="vertical-align: middle;"><?php echo $prod->producto0->nombre ?></td>
                                                <td style="vertical-align: middle;">
                                                    <div class="make-switch switch-small" data-on-label="<i class='icon-ok icon-white'></i>" data-off-label="<i class='icon-remove'></i>">
                                                        <?php $checked = '';
                                                        if ($prod->aprobado == 1) {
                                                            $checked = 'checked';
                                                        } ?>
                                                        <input id="FormVisitaInspeccion_obj_aprobado<?= $prod->producto0->id ?>" name="FormVisitaInspeccion[obj_aprobado<?= $prod->producto0->id ?>]" type="checkbox" <?= $checked ?> class="toggle" />
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php break;
                        case "inspeccionDac": ?>
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed table-hover table-advance">
                                    <thead>
                                        <th>N&deg;</th>
                                        <th>Nombre</th>
                                        <th>Aprobaci&oacute;n</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($id->inspeccionesXDacses as $prod) {
                                        ?>
                                            <tr>
                                                <td style="vertical-align: middle;"><?php echo $prod->numero ?></td>
                                                <td style="vertical-align: middle;"><?php echo $prod->dac0->nombre ?></td>
                                                <td style="vertical-align: middle;">
                                                    <div class="make-switch switch-small" data-on-label="<i class='icon-ok icon-white'></i>" data-off-label="<i class='icon-remove'></i>">
                                                        <?php $checked = '';
                                                        if ($prod->aprobado == 1) {
                                                            $checked = 'checked';
                                                        } ?>
                                                        <input id="FormVisitaInspeccion_obj_aprobado<?= $prod->dac0->id ?>" name="FormVisitaInspeccion[obj_aprobado<?= $prod->dac0->id ?>]" type="checkbox" <?= $checked ?> class="toggle" />
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php break;
                        case "inspeccionMIT": ?>
                            <div class="table-responsive">
                                <table class="table table-bordered table-condensed table-hover table-advance">
                                    <thead>
                                        <th>N&deg;</th>
                                        <th>Nombre</th>
                                        <th>RIG</th>
                                        <th>Modificar RIG</th>
                                        <th>Aprobaci&oacute;n</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($id->inspeccionesXMediosIzados as $prod) {
                                        ?>
                                            <tr>
                                                <td style="vertical-align: middle;"><?php echo $prod->numero ?></td>
                                                <td style="vertical-align: middle;"><?php echo $prod->medioIzado->nombre ?></td>
                                                <td style="vertical-align: middle;">
                                                    <?php if ($prod->rig != null) { ?>
                                                        <a href="<?= Yii::$app->baseUrl ?>/files/<?= $prod->rig ?>" target="_blank">
                                                            <i class="fa fa-file"></i> <?= $prod->rig ?>
                                                        </a>
                                                    <?php } else {
                                                        echo "Sin fichero adjunto";
                                                    } ?>
                                                </td>
                                                <td style="vertical-align: middle;">
                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <span class="btn btn-default btn-file">
                                                            <span class="fileupload-new"><i class="icon-paper-clip"></i> Buscar</span>
                                                            <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                            <input id="FormVisitaInspeccion_file<?= $prod->medioIzado->id ?>" type="file" name="FormVisitaInspeccion[file<?= $prod->medioIzado->id ?>]" class="default" />
                                                        </span>
                                                        <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                        <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                                    </div>
                                                </td>
                                                <td style="vertical-align: middle;">
                                                    <div class="make-switch switch-small" data-on-label="<i class='icon-ok icon-white'></i>" data-off-label="<i class='icon-remove'></i>">
                                                        <?php $checked = '';
                                                        if ($prod->aprobado == 1) {
                                                            $checked = 'checked';
                                                        } ?>
                                                        <input id="FormVisitaInspeccion_obj_aprobado<?= $prod->medioIzado->id ?>" name="FormVisitaInspeccion[obj_aprobado<?= $prod->medioIzado->id ?>]" type="checkbox" <?= $checked ?> class="toggle" />
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                    <?php break;
                    }
                    ?>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'inspectores_actuantes', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList(
                                    $model,
                                    'inspectores_actuantes',
                                    ArrayHelper::map($Funcionarios->findAllOrdered(), 'id', 'nombre_apellidos'),
                                    array(
                                        //'style' => 'width:200px !important', 'prompt'=>' ',
                                        'id' => 'inspectores_actuantes',
                                        'class' => 'multi-select',
                                        'multiple' => true,
                                    )
                                );  ?>
                                <span class="help-block"><?php echo Html::error($model, 'inspectores_actuantes', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'fecha', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'fecha', array('class' => 'form-control form-control input-medium date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'fecha', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'lugar_inspeccion', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'lugar_inspeccion', array('class' => 'form-control', 'placeholder' => 'lugar_inspeccion', 'maxlength' => 150)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'lugar_inspeccion', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                <h4>Resultado de la Inspecci&oacute;n</h4>
                            </center>
                            <div class="form-group">
                                <?php echo Html::activeTextArea($model, 'descripcion', array('class' => 'ckeditor form-control')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'descripcion', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a href="<?php echo Url::to('/inspecciones/index', array('id' => $id->id, 'formType' => $formType)) ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>


<?php

$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/locales/bootstrap-wysihtml5.es-ES.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));


$js = <<<JS
FormComponents.init();
JS;
$this->registerJs($js);
?>