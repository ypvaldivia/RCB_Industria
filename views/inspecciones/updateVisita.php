<?php

use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
<?php
switch ($formType) {
    case "inspeccionPersona":
?>
        Inspecciones a Personas <small> Gesti&oacute;n de la informaci&oacute;n de Inspecciones</small>
    <?php
        break;
    case "inspeccionLabServ":
    ?> Inspecciones de Servicios <small> Gesti&oacute;n de la informaci&oacute;n de Inspecciones</small> <?php
                                                                                                            break;
                                                                                                        case "inspeccionProducto":
                                                                                                            ?> <a href="#">Productos - </a><i class="fa fa-angle-right"></i> <?php
                                                                                                                                                                                break;
                                                                                                                                                                            case "inspeccionDac":
                                                                                                                                                                                ?> <a href="#">Dispositivos Auxiliares de Carga - </a><i class="fa fa-angle-right"></i> <?php
                                                                                                                                                                                                                                                                        break;
                                                                                                                                                                                                                                                                    case "inspeccionMIT":
                                                                                                                                                                                                                                                                        ?> <a href="#">Medios de Izado Terrestres - </a><i class="fa fa-angle-right"></i> <?php
                                                                                                                                                                                                                                                                                                                                                            break;
                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                            ?>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <?php
    switch ($formType) {
        case "inspeccionPersona":
    ?> <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-personas') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i> <?php
                                                break;
                                            case "inspeccionLabServ":
                                                ?> <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-lab-serv') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i> <?php
                                                break;
                                            case "inspeccionProducto":
                                                ?> <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-productos') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i> <?php
                                                break;
                                            case "inspeccionDac":
                                                ?> <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-dacs') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i> <?php
                                                break;
                                            case "inspeccionMIT":
                                                ?> <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-mits') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i> <?php
                                                break;
                                        }
                                                ?>
</li>
<li>
    <?php
    switch ($formType) {
        case "inspeccionPersona":
    ?> <a href="#">Personas - </a><i class="fa fa-angle-right"></i> <?php
                                                                    break;
                                                                case "inspeccionLabServ":
                                                                    ?> <a href="#">Servicios - </a><i class="fa fa-angle-right"></i> <?php
                                                                                                                                        break;
                                                                                                                                    case "inspeccionProducto":
                                                                                                                                        ?> <a href="#">Productos - </a><i class="fa fa-angle-right"></i> <?php
                                                                                                                                                                                                            break;
                                                                                                                                                                                                        case "inspeccionDac":
                                                                                                                                                                                                            ?> <a href="#">Dispositivos Auxiliares de Carga - </a><i class="fa fa-angle-right"></i> <?php
                                                                                                                                                                                                                                                                                                        break;
                                                                                                                                                                                                                                                                                                    case "inspeccionMIT":
                                                                                                                                                                                                                                                                                                        ?> <a href="#">Medios de Izado Terrestres - </a><i class="fa fa-angle-right"></i> <?php
                                                                                                                                                                                                                                                                                                                                                                                            break;
                                                                                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                                                                                            ?>
</li>
<li>
    <a href="#">Actualizar </a>
</li>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_formVisitaEditar', array('model' => $model, 'buttons' => 'create', 'id' => $id, 'formType' => $formType));
?>