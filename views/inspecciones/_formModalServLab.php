<?php

use app\models\Entidades;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="fa fa-plus"></i> Adicionar Servicio</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php $form = ActiveForm::begin(); ?>
                <div class="form-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php echo $form2->labelEx($servicio, 'departamento', array('class' => 'control-label')); ?>
                                <?php echo $form2->textField($servicio, 'departamento', array('class' => 'form-control', 'placeholder' => 'departamento', 'maxlength' => 150)); ?>
                                <span class="help-block"><?php echo $form2->error($servicio, 'departamento', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo $form2->labelEx($servicio, 'entidad', array('class' => 'control-label')); ?>
                                <?php echo $form2->dropDownList($servicio, 'entidad0', ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo $form2->error($servicio, 'entidad', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo $form2->labelEx($servicio, 'provincia', array('class' => 'control-label')); ?>
                                <?php echo $form2->dropDownList($servicio, 'provincia0', ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo $form2->error($servicio, 'provincia', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php echo $form2->labelEx($servicio, 'servicio', array('class' => 'control-label')); ?>
                                <?php echo $form2->textArea($servicio, 'servicio', array('class' => 'form-control')); ?>
                                <span class="help-block"><?php echo $form2->error($servicio, 'servicio', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-save" type="button" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a data-dismiss="modal" href="" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>