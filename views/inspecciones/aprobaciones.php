<?php

use app\models\AprobacionXFuncionarios;
use app\models\Funcionarios;
use app\models\NivelesInspeccion;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;
?>
<?php
$cantidadVisitas = count($inspeccion->visitasInspecciones);
$ultimaVisita = $inspeccion->visitasInspecciones[$cantidadVisitas - 1];
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
<?php
switch ($formType) {
    case "inspeccionPersona":
?>
        Inspecciones a Personas <small> Aprobaci&oacute;n de Inspecciones</small>
    <?php
        break;
    case "inspeccionLabServ":
    ?> Inspecciones de Servicios <small> Aprobaci&oacute;n de Inspecciones</small>
    <?php
        break;
    case "inspeccionProducto":
    ?> Inspecciones de Productos <small> Aprobaci&oacute;n de Inspecciones</small> <?php                                                                                                                                                                                                                                                                           }
                                                                                    ?>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<?php
switch ($formType) {
    case "inspeccionPersona":
?>
        <li>
            <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-personas') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Inspecciones a Personas -</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Aprobaci&oacute;n de Inspecci&oacute;n</a>
        </li>
        <li class="btn-group">
            <a href="<?php echo Url::to('/inspecciones/detalles-personas', array('id' => $inspeccion->id)) ?>" class="btn btn-primary">
                <i class="icon-arrow-left"></i>
                Regresar
            </a>
        </li>
    <?php
        break;
    case "inspeccionLabServ":
    ?>
        <li>
            <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-lab-serv') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Inspecciones a Servicios -</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Aprobaci&oacute;n de Inspecci&oacute;n</a>
        </li>
        <li class="btn-group">
            <a href="<?php echo Url::to('/inspecciones/detalles-lab-serv', array('id' => $inspeccion->id)) ?>" class="btn btn-primary">
                <i class="icon-arrow-left"></i>
                Regresar
            </a>
        </li>
    <?php
        break;
    case "inspeccionProducto":
    ?> <li>
            <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-productos') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Inspecciones a Productos -</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Aprobaci&oacute;n de Inspecci&oacute;n</a>
        </li>
        <li class="btn-group">
            <a href="<?php echo Url::to('/inspecciones/detallesProductos', array('id' => $inspeccion->id)) ?>" class="btn btn-primary">
                <i class="icon-arrow-left"></i>
                Regresar
            </a>
        </li>
    <?php
        break;
    case "inspeccionDac":
    ?> <li>
            <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-dacs') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Inspecciones a Dispositivos Auxiliares de Carga -</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Aprobaci&oacute;n de Inspecci&oacute;n</a>
        </li>
        <li class="btn-group">
            <a href="<?php echo Url::to('/inspecciones/detallesDac', array('id' => $inspeccion->id)) ?>" class="btn btn-primary">
                <i class="icon-arrow-left"></i>
                Regresar
            </a>
        </li>
    <?php
        break;
    case "inspeccionMIT":
    ?> <li>
            <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-mits') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Inspecciones a Medios de Izado Terrestres -</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Aprobaci&oacute;n de Inspecci&oacute;n</a>
        </li>
        <li class="btn-group">
            <a href="<?php echo Url::to('/inspecciones/detallesMit', array('id' => $inspeccion->id)) ?>" class="btn btn-primary">
                <i class="icon-arrow-left"></i>
                Regresar
            </a>
        </li>
        <?php
        break;
}
        ?><?php $this->endBlock(); ?>

        <?php if (Yii::$app->session->hasFlash('success')) { ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
            </div>
        <?php } else if (Yii::$app->session->hasFlash('error')) { ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
            </div>
        <?php } ?>

        <div class="container">
            <div class="row>
        <div class=" col-md-12">
                <?php $estado = 'Inspecci&oacute;n ' . $inspeccion->estado0->estado; ?>
                <div class=" card box <?php echo $inspeccion->estado0->color; ?>">
                    <div class="card-header">
                        <div class="caption">
                            <i class="icon-barcode"></i><?php echo $estado; ?>
                        </div>
                        <div class="actions">
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10">
                                    <h5><b>Entidad:</b> <?php echo $inspeccion->entidad0->nombre; ?></h5>
                                </div>
                                <div class="col-md-2">
                                    <h5>
                                        <center><b>N&deg;:</b> <?php echo $inspeccion->numero_control; ?></center>
                                    </h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5><b>Homologaci&oacute;n:</b>
                                        <?php
                                        if ($inspeccion->homologacion0->titulo_anuario == null) {
                                            echo $inspeccion->homologacion0->titulo_inspector;
                                        } else {
                                            echo $inspeccion->homologacion0->titulo_anuario;
                                        }
                                        if ($inspeccion->certificado != null) {
                                            echo '  (';
                                            echo $inspeccion->certificado;
                                            echo ')';
                                        }
                                        ?>
                                    </h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <h5><b>Dependencia:</b> <?php echo $inspeccion->oficina0->codigo; ?></h5>
                                </div>
                                <div class="col-md-6">
                                    <h5><b>Tipo de Inspecci&oacute;n:</b> <?php echo $inspeccion->tipoHomologacion->tipo; ?></h5>
                                </div>
                                <div class="col-md-2">
                                    <h5>
                                        <center><b>N&deg; Visita:</b> <?php echo $cantidadVisitas; ?></center>
                                    </h5>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <h5><b>Fecha:</b> <?php echo $ultimaVisita->fecha; ?></h5>
                                </div>
                                <div class="col-md-8">
                                    <?php if (isset($inspeccion->lugar_inspeccion)) { ?>
                                        <h5><b>Lugar de inspecci&oacute;n:</b> <?php echo $inspeccion->lugar_inspeccion ?></h5>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 headline">
                                <h4>
                                    Nivel M&aacute;ximo de Aprobaci&oacute;n:
                                    <?php
                                    switch ($formType) {
                                        case 'inspeccionPersona':
                                    ?>
                                            <span class="label label-info"><?php echo $inspeccion->homologacion0->nivelesInspeccion->getFullNivelInspeccion(); ?></span>
                                        <?php
                                            break;
                                        case 'inspeccionServLab':
                                        ?>
                                            <span class="label label-info"><?php echo $inspeccion->homologacion0->nivelesInspeccion->getFullNivelInspeccion(); ?></span>
                                        <?php
                                            break;
                                        case 'inspeccionProducto':
                                        ?>
                                            <span class="label label-info"><?php echo $inspeccion->homologacion0->nivelesInspeccion->getFullNivelInspeccion(); ?></span>
                                        <?php
                                            break;
                                        case 'inspeccionDac':
                                        ?>
                                            <span class="label label-info"><?php echo $inspeccion->getNivelOficina()->getFullNivelInspeccion(); ?></span>
                                        <?php
                                            break;
                                        case 'inspeccionMIT':
                                        ?>
                                            <span class="label label-info"><?php echo $inspeccion->getNivelAprobacionMIT()->getFullNivelInspeccion() ?></span>
                                    <?php
                                            break;
                                    }
                                    ?>
                                </h4>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed table-hover table-advance">
                                <thead>
                                    <th>Nivel Pendiente</th>
                                    <th>Autoridad</th>
                                    <th>Estado</th>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($formType == "inspeccionDac") {
                                        $niveles = NivelesInspeccion::nivelesInferiores($inspeccion->getNivelOficina());
                                    } elseif ($formType == "inspeccionMIT") {
                                        $niveles = NivelesInspeccion::nivelesInferiores($inspeccion->getNivelAprobacionMIT());
                                        echo ($niveles[0]);
                                    } else {
                                        $niveles = NivelesInspeccion::nivelesInferiores($inspeccion->homologacion0->nivelesInspeccion);
                                    }
                                    $usuarioRegistrado = false;
                                    $funcionarioRegistrado = null;
                                    foreach ($niveles as $item) {
                                    ?>
                                        <tr>
                                            <td style="vertical-align: middle;">
                                                <?php echo $item->getFullNivelInspeccion(); ?>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <?php
                                                if ($item->nivel > '02') {
                                                    $funcionarios = Funcionarios::findAll('cargo = :c AND inactivo = :i', array(':c' => $item->cargoCertificadoDefinitivo->id, ':i' => 0));
                                                } else {
                                                    $funcionarios = $Funcionarios->getFuncionariosOficinaNivel($inspeccion, $item);
                                                }
                                                foreach ($funcionarios as $f) {
                                                    if ($f != NULL) {
                                                        echo $f;
                                                        if (Yii::$app->user->id == $f->usuario) {
                                                            $usuarioRegistrado = true;
                                                            $funcionarioRegistrado = $f;
                                                            echo '&nbsp;&nbsp;';
                                                            echo '<span class="badge badge-success"> Usuario Registrado </span>';
                                                        }
                                                        if (count($funcionarios) > 1) {
                                                            echo "&nbsp;&nbsp;";
                                                            echo AprobacionXFuncionarios::funcAprobacion($f->id, $inspeccion->id);
                                                        }
                                                        echo '<br/>';
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <?php
                                                if (count($funcionarios) > 1) {
                                                    echo AprobacionXFuncionarios::estadoAprobacionFuncionarios($funcionarios, $inspeccion->id);
                                                } else {
                                                    if ($funcionarios != Null) {
                                                        echo AprobacionXFuncionarios::estadoAprobacion($funcionarios[0]->id, $inspeccion->id);
                                                    }
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <?php if ($inspeccion->inspeccionCerrada("productos")) echo "Cerrada"; ?>
                        <?php if ($usuarioRegistrado && AprobacionXFuncionarios::aprobadaNivelPrevio($inspeccion, $funcionarioRegistrado)) { ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card-body form">
                                        <!-- BEGIN FORM-->
                                        <?php
                                        $form = ActiveForm::begin();
                                        ?>
                                        <div class="container">
                                            <br />
                                            <?php echo Html::errorSummary(
                                                $model,
                                                'Por favor corrija los siguientes errores de ingreso:',
                                                null,
                                                array('class' => 'alert alert-danger')
                                            );
                                            ?>
                                        </div>
                                        <div class="form-body">
                                            <center>
                                                <h2>Proceso de Aprobaci&oacute;n</h2>
                                            </center>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <label class=""> </label>
                                                    <div class="radio-list">
                                                        <label class="radio-inline">Aprobada ( &checkmark; )
                                                            <input type="radio" name="AprobacionXFuncionarios[aprobada]" id="aprobada" value="1" checked>
                                                        </label>
                                                        <label class="radio-inline">Rechazada ( &cross; )
                                                            <input type="radio" name="AprobacionXFuncionarios[aprobada]" id="rechazada" value="0">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 float-right">
                                                <h5>&nbsp;</h5>
                                                <center>
                                                    <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                                </center>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3>Hallazgos</h3>
                                                <div class="form-group">
                                                    <?php echo Html::activeTextArea($model, 'hallazgos', array('class' => 'ckeditor form-control')); ?>
                                                    <span class="help-block"><?php echo Html::error($model, 'hallazgos', array('class' => 'text-danger')); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3>No conformidades</h3>
                                                <div class="form-group">
                                                    <?php echo Html::activeTextArea($model, 'inconformidades', array('class' => 'ckeditor form-control')); ?>
                                                    <span class="help-block"><?php echo Html::error($model, 'inconformidades', array('class' => 'text-danger')); ?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions fluid">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                        <?php ActiveForm::end() ?>
                                    </div>
                                    <!-- form -->
                                </div>
                            </div>
                        <?php } else { ?>
                            <span class="label label-danger"> Usted no puede modificar el Estado de Aprobaci&oacute;n de esta inspecci&oacute;n </span>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 headline">
                                <h4>
                                    Revisiones Realizadas:
                                    <span class="label label-info"><?php echo count($inspeccion->aprobacionXFuncionarioses); ?></span>
                                </h4>
                                <br />
                                <div class="table-toolbar">
                                    <div class="news-blocks fade in">
                                        <button class="close" data-dismiss="alert" type="button"></button>
                                        <div class="row">
                                            <div class="col-md-1">
                                                <h4 class="alert-heading">Leyenda:</h4>
                                            </div>
                                            <div class="col-md-11">
                                                <span class="label label-danger">
                                                    <i class="icon-remove"></i> No Conformidad</span>&nbsp;
                                                <span class="label label-warning">
                                                    <i class="icon-ok"></i> Aprobada (Con se&ntilde;alamientos)</span>
                                                <span class="label label-success">
                                                    <i class="icon-ok"></i> Satisfactoria</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="timeline">
                                    <?php $i = count($inspeccion->aprobacionXFuncionarioses);
                                    foreach (array_reverse($inspeccion->aprobacionXFuncionarioses) as $a) { ?>
                                        <li <?php
                                            if ($a->rechazada == 1) {
                                                echo 'class="timeline-red"';
                                            } elseif ($a->aprobada == 1 && ($a->hallazgos != null || $a->inconformidades != null)) {
                                                echo 'class="timeline-yellow"';
                                            } else {
                                                echo 'class="timeline-green"';
                                            } ?>>
                                            <div class="timeline-time">
                                                <span class="date"><?php echo $a->actualizado; ?></span>
                                                <span class="time"><?php echo $i; ?></span>
                                            </div>
                                            <div class="timeline-icon"> <?php if ($a->rechazada == 1) {
                                                                            echo '<i class="icon-remove"></i>';
                                                                        } else {
                                                                            echo '<i class="icon-ok"></i>';
                                                                        } ?> </i></div>
                                            <div class="timeline-body">
                                                <div class="timeline-content">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h4>
                                                                <?php echo $a->funcionario0->nombre_apellidos; ?>
                                                                <br />
                                                                <small style="color: #ffffff">
                                                                    <?php echo $a->funcionario0->cargo0->nivelesInspeccions[0]->getFullNivelInspeccion(); ?>
                                                                </small>
                                                            </h4>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <?php if ((isset($a->hallazgos) && $a->hallazgos != "") || (isset($a->inconformidades) && $a->inconformidades != "")) { ?>
                                                                <button type="button" href="#full-<?php echo $a->id ?>" data-original-title="Resultado de esta revisi&oacute;n" data-placement="left" data-toggle="modal" class="btn btn-default btn-block tooltips">
                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                    Ver Hallazgos e Inconformidades</button>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <?php if ((isset($a->hallazgos) && $a->hallazgos != "") || (isset($a->inconformidades) && $a->inconformidades != "")) { ?>
                                                                <div id="full-<?php echo $a->id ?>" class="modal fade" aria-hidden="true" tabindex="-1" style="display: none;">
                                                                    <div class="modal-dialog modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title" style="color: #444">Detalles de la Revisi&oacute;n</h4>
                                                                            </div>
                                                                            <div class="modal-body" style="color: #444">
                                                                                <?php if (isset($a->hallazgos) && $a->hallazgos != "") { ?>
                                                                                    <div class="row">
                                                                                        <h3>Hallazgos</h3>
                                                                                        <div class="col-md-12">
                                                                                            <?php echo $a->hallazgos ?>
                                                                                        </div>
                                                                                    </div>
                                                                                <?php } ?>
                                                                                <?php if (isset($a->inconformidades) && $a->inconformidades != "") { ?>
                                                                                    <div class="row">
                                                                                        <h3>No Conformidades</h3>
                                                                                        <div class="col-md-12">
                                                                                            <?php echo $a->inconformidades ?>
                                                                                        </div>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                </div>
                                                            <?php }  ?>
                                                        </div>
                                                    </div>
                                                </div>
                                        </li>

                                    <?php $i--;
                                    } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>


        <?php

        $this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/css/pages/profile.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/css/custom.css', Yii::$app->session->get('dependencias'));
        $this->registerCssFile('@web/css/pages/timeline.css', Yii::$app->session->get('dependencias'));

        $this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));

        $this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/bootstrap-wysihtml5/locales/bootstrap-wysihtml5.es-ES.js', Yii::$app->session->get('dependencias'));

        $this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
        $this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));


        $this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));

        $js = <<<JS
        FormComponents.init();

JS;
        $this->registerJs($js);
        ?>