<?php

use app\models\Inspecciones;

?>

<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>


    </page_header>
    <page_footer>
        <br />
        <hr style="margin: 2px !important; color: #CCCCCC; font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">Registro Cubano de Buques R7/PC401</td>
                <td style="text-align: right; width: 50%; color: #666666;">P&aacute;gina [[page_cu]] de [[page_nb]]</td>
            </tr>
        </table>
    </page_footer>

    <!-- BEGIN PAGE -->
    <?php $cantidadVisitas = count($model->visitasInspecciones);
    $ultimaVisita = $model->visitasInspecciones[$cantidadVisitas - 1]; ?>

    <div class="container">
        <table style="width: 100%; border:none;">
            <tr>
                <td style="border: 1px solid black; width: 90%">
                    <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" width="632px">
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="width: 100%">
                    <h2 style="text-align: center;">Informe de Inspecci&oacute;n</h2>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="width: 80%"><b>Entidad:</b> <?php echo $model->entidad0->nombre; ?></td>
                <td style="width: 20%"><b>N&deg;:</b> <?php echo $model->numero_control; ?></td>

            </tr>
        </table>
        <table>
            <tr>
                <td><b>Homologaci&oacute;n:</b>
                    <?php
                    if ($model->homologacion0->titulo_anuario == null) {
                        echo $model->homologacion0->titulo_inspector;
                    } else {
                        echo $model->homologacion0->titulo_anuario;
                    }
                    if ($model->certificado != null) {
                        echo '  (';
                        echo $model->certificado;
                        echo ')';
                    }
                    ?></td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td style="width: 30%">
                    <span class=""><b>Dependencia:</b> <?php echo $model->oficina0->codigo; ?></span>
                </td>
                <td style="width: 50%">
                    <span><b>Tipo de Homologaci&oacute;n:</b> <?php echo $model->tipoHomologacion->tipo; ?></span>
                </td>
                <td style="width: 20%">
                    <span class=""><b>Visita N&deg;:</b> <?php echo $cantidadVisitas; ?></span>
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr>
                <td style="width: 30%"><b>Fecha:</b> <?php echo Inspecciones::convertirFecha($ultimaVisita->fecha); ?></td>
                <td style="width: 70%">
                    <?php if (isset($model->lugar_inspeccion)) { ?>
                        <span class="float-right"><b>Lugar de inspecci&oacute;n:</b> <?php echo $model->lugar_inspeccion ?></span>
                    <?php } ?>
                </td>
            </tr>
        </table>
        <p>&nbsp;</p>

        <!-- Checking Survey Type -->
        <?php

        switch ($formType) {
            case "inspeccionPersona":
                $this->render('_pdfDetallePersona', array('model' => $model));
                break;
            case "inspeccionLabServ":
                $this->render('_pdfDetalleLabServ', array('model' => $model));
                break;
            case "inspeccionProducto":
                $this->render('_pdfDetalleProductos', array('model' => $model));
                break;
            case "inspeccionDac":
                $this->render('_pdfDetalleDac', array('model' => $model));
                break;
            case "inspeccionMIT":
                $this->render('_pdfDetalleMit', array('model' => $model));
                break;
        }

        ?>

        <h3>Resultado de la Inspecci&oacute;n </h3>

        <p align="justify" style="width: 96%; max-width: 97%">
            <?php echo $ultimaVisita->descripcion ?>
        </p>


        <p></p>
        <p></p>
        <p></p>
        <?php
        $inspArray = $ultimaVisita->inspectoresActuantes;
        $inspCant = count($inspArray);
        $w = "style='width: " . ((100 / $inspCant) - 3) . "%'";
        ?>
        <table class="table table-condensed table-my" width="100%">
            <tbody>
                <tr style="height: 3px">
                    <?php foreach ($inspArray as $headval) { ?>
                        <td <?= $w ?>>
                            <p align="center">
                                <span style="text-decoration: overline">
                                    &nbsp;&nbsp;
                                    <?php echo $headval->inspector0->nombre_apellidos; ?>
                                    &nbsp;&nbsp;
                                </span>
                                <?php
                                echo '<br/>';
                                echo $headval->inspector0->categoria0->categoria;
                                ?>
                            </p>
                        </td>
                    <?php } ?>
                </tr>
            </tbody>
        </table>
    </div>
</page>
<!-- END CONTAINER -->