<?php

use app\models\Funcionarios;
use app\models\Inspecciones;
use yii\helpers\Url;
?>

<?php $this->beginClip('TITLE') ?>Informe de Inspecci&oacute;n <?php echo $model->entidad0->nombre; ?><?php $this->endClip() ?>

<?php
$cantidadVisitas = count($model->visitasInspecciones);
$ultimaVisita = $model->visitasInspecciones[$cantidadVisitas - 1];
?>


<div class="container">
    <div class="row">
        <img src="<?= Yii::getAlias('@web/uploads') ?>/images/bannerHeader.jpg" width="872px">
        <br /><br />
        <div class="row">
            <div class="col-md-10">
                &nbsp;
            </div>
            <div class="col-md-2">
                <span class="float-right">
                    <h5>R7/PC401&nbsp;&nbsp;&nbsp;</h5>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <h2 class="tile">Informe de Inspecci&oacute;n</h2>
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-10">
            <span class="float-left"><b>Entidad:</b> <?php echo $model->entidad0->nombre; ?></span>
        </div>
        <div class="col-md-2">
            <span class="float-right"><b>N&deg;:</b> <?php echo $model->numero_control; ?></span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5><b>Homologaci&oacute;n:</b>
                <?php
                if ($model->homologacion0->titulo_anuario == null) {
                    echo $model->homologacion0->titulo_inspector;
                } else {
                    echo $model->homologacion0->titulo_anuario;
                }
                if ($model->certificado != null) {
                    echo '  (';
                    echo $model->certificado;
                    echo ')';
                }
                ?>
            </h5>
        </div>
    </div>

    <table style="border: 0px" width="100%">
        <tr>
            <td style="width: 20%; align-content: center; text-align: left">
                <b>Dependencia:</b> <?php echo $model->oficina0->codigo; ?>
            </td>
            <td style="width: 40%; align-content: center; text-align: center">
                <b>Tipo de Inspecci&oacute;n:</b> <?php echo $model->tipoHomologacion->tipo; ?>
            </td>
            <td style="width: 30%; align-content: center; text-align: right">
                <b>Visita N&deg;:</b> <?php echo $cantidadVisitas; ?>
            </td>
        </tr>
    </table>

    <br />

    <div class="row">
        <div class="col-md-4">
            <span class="float-left"><b>Fecha:</b> <?php echo Inspecciones::convertirFecha($ultimaVisita->fecha); ?></span>
        </div>
        <div class="col-md-8">
            <?php if (isset($model->lugar_inspeccion)) { ?>
                <span class="float-right"><b>Lugar de inspecci&oacute;n:</b> <?php echo $model->lugar_inspeccion ?></span>
            <?php } ?>
        </div>
    </div>
    <p>&nbsp;</p>
    <!-- Checking Survey Type -->
    <?php

    switch ($formType) {
        case "inspeccionPersona":
            $this->render('_pdfDetallePersona', array('model' => $model));
            break;
        case "inspeccionLabServ":
            $this->render('_pdfDetalleLabServ', array('model' => $model));
            break;
        case "inspeccionProducto":
            $this->render('_pdfDetalleProductos', array('model' => $model));
            break;
        case "inspeccionDac":
            $this->render('_pdfDetalleDac', array('model' => $model));
            break;
        case "inspeccionMIT":
            $this->render('_pdfDetalleMit', array('model' => $model));
            break;
    }

    ?>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">
                <h3 class="tile">Resultado de la Inspecci&oacute;n </h3>
            </div>
            <p align="justify">
                <?php echo $ultimaVisita->descripcion ?>
            </p>
        </div>
    </div>

    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <?php
    $inspArray = $ultimaVisita->inspectoresActuantes;
    $inspCant = count($inspArray);
    $w = "width: " . ((100 / $inspCant) - 3) . "%";
    ?>
    <table width="100%">
        <tbody>
            <tr>
                <?php foreach ($inspArray as $headval) { ?>
                    <td style="<?= $w ?>">
                        <p align="center">
                            <span style="text-decoration: overline">
                                &nbsp;&nbsp;
                                <?php echo $headval->inspector0->nombre_apellidos; ?>
                                &nbsp;&nbsp;
                            </span>
                            <?php
                            echo '<br/>';
                            echo $headval->inspector0->categoria0->categoria;
                            ?>
                        </p>
                    </td>
                <?php } ?>
            </tr>
        </tbody>
    </table>

</div>