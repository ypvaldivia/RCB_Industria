<?php

use app\models\AcreditacionesIndustriales;
use app\models\Entidades;
use app\models\Oficinas;
use app\models\TiposHomologacion;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Editar datos de la Inspecci&oacute;n: <?php echo $model->numero_control ?></div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php
                $form = ActiveForm::begin();
                ?> <div class="container">
                    <?php echo Html::errorSummary($model);
                    ?>
                </div>
                <div class="form-body">
                    <img src="<?= Yii::getAlias('@web/uploads') ?>/images/bannerHeader.jpg" width="100%">
                    <div class="row">
                        <div class="col-md-10">
                            &nbsp;
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <span class="float-right">
                                    <h5>R7/PC401&nbsp;&nbsp;&nbsp;</h5>
                                </span>
                            </div>
                        </div>
                    </div>
                    <center>
                        <h2>Informe de Inspecci&oacute;n</h2>
                    </center>
                    <hr />

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'numero_control', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'numero_control', array('class' => 'form-control', 'placeholder' => 'XXXX#####', 'maxlength' => 9)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'numero_control', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'entidad', array('class' => 'control-label')); ?>
                                <br />
                                <h4><?= Entidades::findOne($model->entidad)->nombre  ?></h4>
                                <span class="help-block"><?php echo Html::error($model, 'entidad', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'homologacion', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'homologacion', ArrayHelper::map(AcreditacionesIndustriales::findAll('tipo=2'), 'id', 'titulo_inspector'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'homologacion', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'certificado', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'certificado', array('class' => 'form-control', 'placeholder' => 'Escriba el detalle de este tipo de homologacion...', 'maxlength' => 200)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'certificado', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'oficina', array('class' => 'control-label')); ?>
                                <?php $Oficinas = new Oficinas(); ?>
                                <?php echo Html::activeDropDownList($model, 'oficina', ArrayHelper::map($Oficinas->getOficinaUsuarioReg(), 'id', 'codigo'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'oficina', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'tipo_homologacion', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'tipo_homologacion', ArrayHelper::map(TiposHomologacion::findNotMITS(), 'id', 'tipo'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'tipo_homologacion', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label"> No. &Uacute;ltima Visita</label>
                                <br />
                                <span class="badge badge-success"> <?php echo $model->numero_visita ?> </span>
                            </div>
                        </div>
                    </div>

                    <div class="well block-grey">
                        <h4>Objetos de Homologaci&oacute;n</h4>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-condensed table-hover table-advance">
                                            <thead>
                                                <th>Nombre</th>
                                                <th>Propietario</th>
                                                <th>Provincia</th>
                                                <th>Estado</th>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $list = array_merge($model->productos_homologar, $model->productos_desaprobados);
                                                foreach ($list as $prod) {
                                                ?>
                                                    <tr>
                                                        <td style="vertical-align: middle;"><?php echo $prod->producto0->nombre ?></td>
                                                        <td style="vertical-align: middle;"><?php if ($prod->producto0->propietario0 != null) {
                                                                                                echo $prod->producto0->propietario0->nombre;
                                                                                            } ?></td>
                                                        <td style="vertical-align: middle;"><?php if ($prod->producto0->provincia0 != null) {
                                                                                                echo $prod->producto0->provincia0->nombre;
                                                                                            } ?></td>
                                                        <td style="vertical-align: middle;">
                                                            <?php if ($prod->aprobado) { ?><span class="label label-success">Aprobado</span><?php } else { ?>
                                                                <span class="label label-danger">Desaprobado</span><?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h5>Lugar de la Inspecci&oacute;n</h5>
                                <?php echo Html::activeTextInput($model, 'lugar_inspeccion', array('class' => 'form-control', 'placeholder' => 'Lugar de inspección', 'maxlength' => 150)); ?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h5>Fecha de la &uacute;ltima visita</h5>
                                <?php echo Html::activeTextInput($model, 'fecha', array('class' => 'form-control form-control input-medium date-picker', 'data-date-format' => 'yyyy-mm-dd', 'disabled' => 'disabled')); ?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <hr />

                    <div class="row">
                        <div class="col-md-4">
                            <div class="bg-green" style="padding: 10px">
                                <h4>Inspectores Actuantes</h4>
                                <hr />
                                <?php foreach ($model->inspectores_actuantes as $inspectores) {
                                    echo '<span style="font-size: medium">';
                                    echo '<i class="fa fa-user"></i>  ';
                                    echo $inspectores->inspector0->nombre_apellidos;
                                    echo '</span>';
                                    echo '<br/>';
                                } ?>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php if ($model->aprobado == 1) { ?>
                                            <span class="label label-success">
                                                Esta inspecci&oacute;n fue aprobada durante la &uacute;ltima visita
                                            </span>
                                        <?php } else { ?>
                                            <span class="label label-danger">
                                                Esta inspecci&oacute;n todav&iacute;a no ha sido aprobada
                                            </span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a href="<?php echo Url::to('/inspecciones/eliminarInforme', array('id' => $model->id, 'type' => 'producto')) ?>" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar Informe</a>
                                <a href="<?php echo Url::to('/inspecciones/detallesProductos', array('id' => $model->id)) ?>" class="btn btn-default"><i class="fa fa-backward"></i> Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>


<?php


$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/locales/bootstrap-wysihtml5.es-ES.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));





$js = <<<JS
FormComponents.init();
JS;
$this->registerJs($js);
?>