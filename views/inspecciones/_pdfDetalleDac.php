<?php if (isset($model->inspeccionesXDacses)) { ?>
    <h4> Dispositivos Auxiliares de Carga presentados por la Entidad: </h4>
    <table style="width: 95%; border: solid 1px black;">
        <tr>
            <th style="width: 20%;">No.</th>
            <th style="width: 50%;">Nombre</th>
            <th style="width: 30%;">CI (kg)</th>
        </tr>
        <tbody>
        <?php foreach ($model->inspeccionesXDacses as $prod) { ?>
            <tr>
                <td style="vertical-align: middle;"><?php echo $prod->numero ?></td>
                <td style="vertical-align: middle;"><?php echo $prod->dac0->nombre ?></td>
                <td style="vertical-align: middle;"><?php echo $prod->dac0->ct_nominal ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>