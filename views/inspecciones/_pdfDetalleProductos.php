<?php if (isset($model->inspeccionesXProductoses)) { ?>
    <h4> Productos candidatos: </h4>
    <table style="border: solid 1px black;" width="100%">
        <thead>
        <tr>
            <th style="width: 20%;">No.</th>
            <th style="width: 70%;">Producto</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($model->inspeccionesXProductoses as $prod) { ?>
            <tr>
                <td style="vertical-align: middle;"><?php echo $prod->numero ?></td>
                <td style="vertical-align: middle;"><?php echo $prod->producto0->nombre ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>