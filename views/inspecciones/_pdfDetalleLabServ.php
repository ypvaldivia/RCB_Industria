<?php if (isset($model->inspeccionesXServiciosProductoses)) { ?>
    <h4> Servicios candidatos: </h4>
    <table style="width: 100%; border: solid 1px black;" width="100%">
        <thead>
        <tr>
            <th style="width: 20%">No.</th>
            <th style="width: 40%">Servicio</th>
            <th style="width: 40%">Departamento</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($model->inspeccionesXServiciosProductoses as $servLab) { ?>
            <tr>
                <td style="vertical-align: middle;"><?php echo $servLab->numero ?></td>
                <td style="vertical-align: middle;"><?php echo $servLab->servcios0->servicio ?></td>
                <td style="vertical-align: middle;"><?php echo $servLab->servcios0->departamento ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>