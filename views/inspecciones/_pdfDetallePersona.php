<?php if (isset($model->inspeccionesXPersonases)) { ?>
    <h4> Candidatos Presentados por la Entidad: </h4>
    <table style="width: 100%; border: solid 1px black;">
        <thead>
        <tr>
            <th style="width: 10%;">No.</th>
            <th style="width: 50%;">Nombre</th>
            <th style="width: 40%;">No. Carnet de Identidad</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($model->inspeccionesXPersonases as $funcionario) { ?>
            <tr>
                <td style="vertical-align: middle;"><?php echo $funcionario->numero ?></td>
                <td style="vertical-align: middle;"><?php echo $funcionario->persona0->nombre_apellidos ?></td>
                <td style="vertical-align: middle;"><?php echo $funcionario->persona0->ci ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>