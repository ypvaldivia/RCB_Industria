<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\LinkPager;
use yii\helpers\Url;
?>
<div class="card border-left-primary shadow">
    <div class="card-header">
        <div class="caption">
            <i class="fa fa-search"></i> B&uacute;squeda avanzada
        </div>
        <div class="tools">
            <a class="collapse tooltips" href="javascript:;" data-original-title="Comprimir este cuadro de informaci&oacute;n" data-placement="top"></a>
            <a class="remove tooltips" href="javascript:;" data-original-title="Ocultar este cuadro de informaci&oacute;n" data-placement="top"></a>
        </div>
    </div>
    <div class="card-body">
        <?php $form = ActiveForm::begin(['options' => ['class' => 'form-inline'],]) ?>
        <?php echo Html::activeTextInput($model, 'objeto', array('class' => 'form-control', 'placeholder' => 'Medio de Izado')); ?>
        <?php echo Html::activeTextInput($model, 'control', array('class' => 'form-control', 'placeholder' => 'No. Control')); ?>
        <?php echo Html::activeTextInput($model, 'inspector', array('class' => 'form-control', 'placeholder' => 'Codigo inspector')); ?>
        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Buscar</button>
        <?php ActiveForm::end() ?>
    </div>
</div>
<br />
<div class="table-toolbar">
    <div class="news-blocks fade in">
        <div class="row">
            <div class="col-md-5">
                <h5>Leyenda (Cliente):<h5>
            </div>
            <div class="col-md-7">
                <h5>Leyenda (Interna):<h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <span class="label label-danger"><i class="icon-barcode"></i>
                    Insatisfactoria</span> |
                <span class="label label-success"><i class="icon-barcode"></i>
                    Satisfactoria (pendiente aprobaci&oacute;n)</span>
            </div>
            <div class="col-md-7">
                <span class="label label-default"><i class="icon-barcode"></i>
                    No Conformidad</span> |
                <span class="label label-warning"><i class="icon-barcode"></i>
                    Aprobada Con observaciones</span> |
                <span class="label label-primary"><i class="icon-barcode"></i>
                    Aprobada</span>
            </div>
        </div>
    </div>
</div>
<?php if (empty($data)) { ?>
    <hr />
    <div class="alert alert-info fade in">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button"></button>
        <h2>No existen <strong>Inspecciones </strong> en el sistema.</h2>
    </div>
<?php } ?>
<!-- BEGIN PAGE CONTENT-->
<div class="row" id="sortable_cards">
    <div class="col-xs-12 column sortable">
        <?php
        foreach ($data as $porlet) {
        ?>
            <!-- BEGIN card card-->
            <?php $estado = 'Inspecci&oacute;n ' . $porlet->estado0->estado; ?>
            <div class=" card box <?php echo $porlet->estado0->color; ?>">
                <div class="card-header">
                    <div class="caption">
                        <i class="icon-barcode"></i>Entidad: <em><?php echo $porlet->entidad0->nombre; ?></em>
                    </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse tooltips" data-original-title="Comprimir este cuadro de informaci&oacute;n"></a>
                        <a href="<?php echo Url::to('/inspecciones/detallesMit', array('id' => $porlet->id)) ?>" class="config tooltips" data-original-title="CLICK AQU&Iacute; PARA VER DETALLES DE ESTA INSPECCI&Oacute;N"></a>
                        <a href="javascript:;" class="reload tooltips" data-original-title="Recargar este cuadro de informaci&oacute;n"></a>
                        <a href="javascript:;" class="remove tooltips" data-original-title="Ocultar este cuadro de informaci&oacute;n"></a>
                    </div>
                    <div class="actions">
                    </div>
                </div>
                <div class="card-body">
                    <div class="container">
                        <div class="row portfolio-block">
                            <div class="col-md-12 row">
                                <div class="portfolio-info" style="padding: 10px 15px;">
                                    N&uacute;mero de Control:
                                    <span><?php echo $porlet->numero_control; ?></span>
                                </div>
                                <div class="portfolio-info" style="padding: 10px 15px;">
                                    Dependencia:
                                    <span><?php echo $porlet->oficina0->nombre; ?></span>
                                </div>
                                <div class="portfolio-info" style="padding: 10px 15px;">
                                    <i class="icon-android"></i>
                                    Visita Nº:
                                    <span><?php echo count($porlet->visitasInspecciones); ?></span>
                                </div>
                                <div class="portfolio-info float-right" style="padding: 10px 15px;">
                                    <a href="<?php echo Url::to('/inspecciones/detallesMit', array('id' => $porlet->id)) ?>" class="btn btn-sm btn-flat purple">
                                        <i class="icon- fa fa-search top-news-icon"></i>
                                        Ver Detalles
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="row ">
                            <div class="col-md-6">
                                <div class="top-news margin-top-10">
                                    <a href="#" class="btn btn btn-default">
                                        <span>Homologaci&oacute;n</span>
                                        <em>
                                            <i class="fa fa-file"></i>
                                            <?php
                                            if ($porlet->homologacion0->titulo_anuario == null) {
                                                echo $porlet->homologacion0->titulo_inspector;
                                            } else {
                                                echo $porlet->homologacion0->titulo_anuario;
                                            }

                                            ?>
                                        </em>
                                        <i class="icon- fa fa-file-text top-news-icon"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="top-news margin-top-10">
                                    <a href="#" class="btn btn btn-default">
                                        <span>Tipo de Inspecci&oacute;n:</span>
                                        <em>
                                            <i class="icon-tags"></i>
                                            <?php echo $porlet->tipoHomologacion->tipo; ?>
                                        </em>
                                        <i class="icon- icon-bullhorn top-news-icon"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END card card-->
        <?php
        }
        ?>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="dataTables_paginate paging_bootstrap float-right">
            <?= LinkPager::widget(['pagination' => $pages,]); ?>
        </div>
    </div>
</div>

<?php
$this->registerCssFile('@web/css/custom.css', Yii::$app->session->get('dependencias'));
?>