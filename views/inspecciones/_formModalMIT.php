<?php

use app\models\CategoriasMediosIzado;
use app\models\Entidades;
use app\models\Provincias;
use app\models\TiposMediosIzado;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="fa fa-plus"></i> Adicionar Medio de Izado Terrestre</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php $form = ActiveForm::begin(); ?>
                <div class="form-body">

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="control-label" for="MediosIzadoAcreditados[tipo0]">Gen&eacute;rico</label>
                                <?php echo $form2->dropDownList($mit, 'tipo0', ArrayHelper::map(TiposMediosIzado::find()->all(), 'id', 'tipo'), array('style' => 'width:200px !important', 'prompt' => '', 'class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo $form2->error($mit, 'tipo0', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo $form2->labelEx($mit, 'categoria0', array('class' => 'control-label')); ?>
                                <?php echo $form2->dropDownList($mit, 'categoria0', ArrayHelper::map(CategoriasMediosIzado::find()->all(), 'id', 'categoria'), array('style' => 'width:200px !important', 'prompt' => '', 'class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo $form2->error($mit, 'categoria0', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>

                    <h4>Capacidad de Izado Nominal (kilogramos)</h4>
                    <div class="well">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <?php echo $form2->labelEx($mit, 'ci_nominal', array('class' => 'control-label')); ?>
                                    <?php echo $form2->textField($mit, 'ci_nominal', array('class' => 'form-control', 'placeholder' => 'ci_nominal')); ?>
                                    <span class="help-block"><?php echo $form2->error($mit, 'ci_nominal', array('class' => 'text-danger')); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <?php echo $form2->labelEx($mit, 'ci_aux_nominal', array('class' => 'control-label')); ?>
                                    <?php echo $form2->textField($mit, 'ci_aux_nominal', array('class' => 'form-control', 'placeholder' => 'ci_nominal')); ?>
                                    <span class="help-block"><?php echo $form2->error($mit, 'ci_aux_nominal', array('class' => 'text-danger')); ?></span>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <?php echo $form2->labelEx($mit, 'ci_aux1_nominal', array('class' => 'control-label')); ?>
                                    <?php echo $form2->textField($mit, 'ci_aux1_nominal', array('class' => 'form-control', 'placeholder' => 'ci_nominal')); ?>
                                    <span class="help-block"><?php echo $form2->error($mit, 'ci_aux1_nominal', array('class' => 'text-danger')); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php echo $form2->labelEx($mit, 'nombre', array('class' => 'control-label')); ?>
                                <?php echo $form2->textField($mit, 'nombre', array('class' => 'form-control', 'placeholder' => 'nombre', 'maxlength' => 100)); ?>
                                <span class="help-block"><?php echo $form2->error($mit, 'nombre', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo $form2->labelEx($mit, 'propietario', array('class' => 'control-label')); ?>
                                <?php echo $form2->dropDownList($mit, 'propietario0', ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo $form2->error($mit, 'propietario', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo $form2->labelEx($mit, 'provincia', array('class' => 'control-label')); ?>
                                <?php echo $form2->dropDownList($mit, 'provincia0', ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo $form2->error($mit, 'provincia', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-save" type="button" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a data-dismiss="modal" href="" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>