<?php

use app\models\Funcionarios;
use app\models\Inspecciones;
use yii\helpers\Url;
?>
<?php
$cantidadVisitas = count($model->visitasInspecciones);
$ultimaVisita = $model->getUltimaVisita();
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Inspecciones a Personas <small> Gesti&oacute;n de la informaci&oacute;n de Inspecciones</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="icon-barcode"></i>
    <a href="<?php echo Url::to('/inspecciones/index-personas') ?>">Inspecciones - </a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Inspecciones a Personas -</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Informe de Inspecci&oacute;n</a>
</li>
<li class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
        <span>Men&uacute;</span> <i class="fa fa-angle-down"></i>
    </button>
    <ul class="dropdown-menu float-right" role="menu">
        <?php if ($model->estado != 4 && $model->estado != 5 && $model->containInspector(Yii::$app->user->id)) { ?>
            <li>
                <a href="<?php echo Url::to('/inspecciones/editarPersonas', array('id' => $model->id)) ?>">
                    <i class="fa fa-edit"></i> Modificar esta Inspecci&oacute;n&nbsp;&nbsp;
                </a>
            </li>
        <?php } ?>
        <li>
            <a href="<?php echo Url::to('/inspecciones/imprimirDetalles', array('id' => $model->id, 'formType' => 'inspeccionPersona')) ?>" class="btnPrint">
                <i class="icon-print"></i> Imprimir este Informe
            </a>
        </li>
        <li>
            <a href="<?php echo Url::to('/inspecciones/PDFDetalles', array('id' => $model->id, 'formType' => 'inspeccionPersona')) ?>">
                <i class="fa fa-file-alt"></i> Generar PDF
            </a>
        </li>
        <?php if ($ultimaVisita->aprobado == 1 && $model->estado != 4 && $model->estado != 5) { ?>
            <?php if ($model->cerrada == 1) { ?>
                <li>
                    <a href="<?php echo Url::to('/inspecciones/aprobaciones', array('id' => $model->id, 'formType' => 'inspeccionPersona')) ?>">
                        <i class="fa fa-user"></i>Aprobaci&oacute;n por niveles
                    </a>
                </li>
            <?php } else { ?>
                <li>
                    <a href="<?php echo Url::to('/inspecciones/cerrarInspeccionPersona', array('id' => $model->id)) ?>">
                        <i class="fa fa-search"></i>Enviar a Revisi&oacute;n
                    </a>
                </li>
            <?php } ?>
        <?php } ?>
        <li class="divider"></li>
        <li>
            <a href="<?php echo Url::to('/inspecciones/index-personas') ?>">
                <i class="icon-barcode"></i> &Iacute;ndice de Inspecciones
            </a>
        </li>
    </ul>
</li><?php $this->endBlock(); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php } ?>

<!-- BEGIN PAGE CONTENT-->
<div class="container">
    <div class="row>
        <div class=" col-xs-12 column sortable">
        <!-- BEGIN card card-->
        <?php $estado = 'Inspecci&oacute;n ' . $model->estado0->estado; ?>
        <div class=" card box <?php echo $model->estado0->color; ?>">
            <div class="card-header">
                <div class="caption">
                    <i class="icon-barcode"></i><?php echo $estado; ?>
                </div>
            </div>
            <div class="card-body">


                <div class="container">
                    <div class="col-md-12 row">
                        <center>
                            <img src="<?= Yii::getAlias('@web/uploads') ?>/images/bannerHeader.jpg" width="100%">
                            <br /><br />
                            <div class="row">
                                <div class="col-md-10">
                                    &nbsp;
                                </div>
                                <div class="col-md-2">
                                    <span class="float-right">
                                        <h5>R7/PC401&nbsp;&nbsp;&nbsp;</h5>
                                    </span>
                                </div>
                            </div>
                            <h2>Informe de Inspecci&oacute;n</h2>
                        </center>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-10">
                            <h5><b>Entidad:</b> <?php echo $model->entidad0->nombre; ?></h5>
                        </div>
                        <div class="col-md-2">
                            <h5>
                                <center><b>N&deg;:</b> <?php echo $model->numero_control; ?></center>
                            </h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h5><b>Homologaci&oacute;n:</b>
                                <?php
                                if ($model->homologacion0->titulo_anuario == null) {
                                    echo $model->homologacion0->titulo_inspector;
                                } else {
                                    echo $model->homologacion0->titulo_anuario;
                                }
                                if ($model->certificado != null) {
                                    echo '  (';
                                    echo $model->certificado;
                                    echo ')';
                                }
                                ?>
                            </h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <h5><b>Dependencia:</b> <?php echo $model->oficina0->codigo; ?></h5>
                        </div>
                        <div class="col-md-6">
                            <h5><b>Tipo de Inspecci&oacute;n:</b> <?php echo $model->tipoHomologacion->tipo; ?></h5>
                        </div>
                        <div class="col-md-2">
                            <h5>
                                <center><b>N&deg; Visita:</b> <?php echo $cantidadVisitas; ?></center>
                            </h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <h5><b>Fecha:</b> <?php echo Inspecciones::convertirFecha($ultimaVisita->fecha); ?></h5>
                        </div>
                        <div class="col-md-8">
                            <?php if (isset($model->lugar_inspeccion)) { ?>
                                <h5><b>Lugar de inspecci&oacute;n:</b> <?php echo $model->lugar_inspeccion ?></h5>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <hr />
                <?php if (isset($model->inspeccionesXPersonases)) { ?>
                    <div class="row">
                        <div class="col-md-12 headline">
                            <h4>
                                Personas Evaluadas:
                                <span class="label label-info"><?php echo count($model->inspeccionesXPersonases); ?></span>
                                <div class="float-right">
                                    <?php if (($model->cerrada == 1 && $model->estado != 2)) { ?>
                                        <a href="<?php echo Url::to('/inspecciones/aprobaciones', array('id' => $model->id, 'formType' => 'inspeccionPersona')) ?>" class="btn btn-primary btn-sm"><i class="fa fa-file"></i> Aprobaci&oacute;n por niveles
                                        </a>
                                    <?php } elseif ($model->cerrada == 0 && $model->estado == 1 && $Funcionarios->getRevision(Yii::$app->user->id, $model->id)) { ?>
                                        <a href="<?php echo Url::to('/inspecciones/cerrarInspeccionPersona', array('id' => $model->id)) ?>" class="btn btn-success btn-sm"><i class="fa fa-search"></i> Informe completo, enviar a Revisi&oacute;n
                                        </a>
                                    <?php } elseif (($model->cerrada == 0 && $model->estado == 3)) { ?>
                                        <a href="<?php echo Url::to('/inspecciones/aprobaciones', array('id' => $model->id, 'formType' => 'inspeccionPersona')) ?>" class="btn btn-primary btn-sm"><i class="fa fa-file"></i> Aprobaci&oacute;n por niveles
                                        </a>
                                        <?php if ($Funcionarios->getRevision(Yii::$app->user->id, $model->id)) { ?>
                                            <a href="<?php echo Url::to('/inspecciones/cerrarInspeccionPersona', array('id' => $model->id)) ?>" class="btn btn-success btn-sm"><i class="fa fa-search"></i> Informe completo, enviar a Revisi&oacute;n
                                            </a>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </h4>
                            <br />
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed table-hover table-advance table-my">
                            <thead>
                                <th>N&deg;</th>
                                <th></th>
                                <th>Nombre</th>
                                <th>N&deg; Identificaci&oacute;n</th>
                                <th>Certificado PDF</th>
                                <th>Generar Certificado</th>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($model->inspeccionesXPersonases as $funcionario) {
                                ?>
                                    <tr>
                                        <td style="vertical-align: middle;"><?php echo $funcionario->numero ?></td>
                                        <td style="max-width: 30px;">
                                            <?php if ($funcionario->persona0->foto != "") { ?>
                                                <img class="img-responsive" src="<?= Yii::getAlias('@web/uploads'); ?>/images/personas/<?php echo $funcionario->persona0->foto; ?>">
                                            <?php } else { ?>
                                                <img class="img-responsive" src="<?= Yii::getAlias('@web/themes/sbadmin') ?>/assets/img/no-image.png">
                                            <?php } ?>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <a href="<?php echo Url::to('/personas-acreditadas/update', array('id' => $funcionario->persona0->id)) ?>" target="_blank">
                                                <?php echo $funcionario->persona0->nombre_apellidos ?>
                                            </a>
                                        </td>
                                        <td style="vertical-align: middle;"><?php echo $funcionario->persona0->ci ?></td>
                                        <td style="vertical-align: middle;">
                                            <?php
                                            if ($funcionario->cprovisional != null || $funcionario->cdefinitivo != null) {
                                                if ($funcionario->cdefinitivo != null) {
                                            ?>
                                                    <a href="<?= Yii::getAlias('@web/uploads'); ?>/certificados/definitivos/personas/<?php echo $funcionario->cdefinitivo; ?>"><?= $funcionario->cdefinitivo; ?></a>
                                                <?php
                                                } else {
                                                ?>
                                                    <a href="<?= Yii::getAlias('@web/uploads'); ?>/certificados/provisionales/personas/<?= $funcionario->cprovisional; ?>" target="_blank"><?= $funcionario->cprovisional; ?></a>
                                            <?php
                                                }
                                            } else {
                                                echo "No se ha generado ningún certificado";
                                            }
                                            ?>
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <?php if ($ultimaVisita->aprobado == 1 && $model->tipoHomologacion->genera_certificado == 1) { ?>
                                                <?php if ($model->estado == 4 || $model->estado == 5 || $funcionario->aprobado_definitivo == 1) { ?>
                                                    <?php if ($funcionario->aprobada == 1) { ?>
                                                        <a href="<?php echo Url::to('/certificados/certificado', array('obj' => $funcionario->id, 'tipoObj' => 1, 'tipoCe' => 2)) ?>" class="btn btn-primary btn-sm" target="_blank"><i class="fa fa-file"></i> Definitivo
                                                        </a>
                                                    <?php } else { ?>
                                                        <span class="label label-danger">Desaprobado</span>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <?php if ($funcionario->aprobada == 1) { ?>
                                                        <a href="<?php echo Url::to('/certificados/certificado', array('obj' => $funcionario->id, 'tipoObj' => 1, 'tipoCe' => 1)) ?>" class="btn btn-primary btn-sm" target="_blank"><i class="fa fa-file"></i> Provisional</a>
                                                    <?php } else { ?>
                                                        <span class="label label-danger">Desaprobado</span>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <?php if ($funcionario->aprobada == 1) { ?>
                                                    <?php if ($model->tipoHomologacion->genera_certificado == 0) { ?>
                                                        <span class="label label-info">No requiere certificado</span>
                                                    <?php } else { ?>
                                                        <span class="label label-warning">Pendiente </span>
                                                    <?php }
                                                } else { ?>
                                                    <span class="label label-danger">Desaprobado</span>
                                                <?php } ?>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
                <div class="clearfix"></div>
                <!-- BEGIN PAGE TIMELINE-->
                <div class="row">
                    <div class="col-md-12 headline">
                        <h4>
                            Visitas Realizadas:
                            <span class="label label-success"><?php echo $cantidadVisitas; ?></span>
                            <?php if (!$model->inspeccionCerrada("personas") && $model->containInspector(Yii::$app->user->id)) { ?>
                                <a href="<?php echo Url::to('/inspecciones/createVisitaPersona', array('id' => $model->id)) ?>" class="btn btn-default btn btn-success-stripe float-right"><i class="fa fa-plus"></i> Registrar nueva visita
                                </a>
                            <?php } ?>
                        </h4>
                        <br />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-12">
                        <ul class="timeline">
                            <?php $nv = 0; ?>
                            <?php foreach (array_reverse($model->visitasInspecciones) as $visita) { ?>
                                <li class="timeline-green">
                                    <div class="timeline-time">
                                        <span class="date"><?php echo $visita->fecha; ?></span>
                                        <span class="time"><?php echo $visita->numero_visita; ?></span>
                                    </div>
                                    <div class="timeline-icon"> <?php if ($visita->aprobado == 0) {
                                                                    echo '<i class="icon-remove"></i>';
                                                                } else {
                                                                    echo '<i class="icon-ok"></i>';
                                                                } ?> </i></div>
                                    <div class="timeline-body">
                                        <h4>Inspectores Actuantes:</h4>
                                        <?php if (!$model->inspeccionCerrada("personas") && $model->containInspector(Yii::$app->user->id)) { ?>
                                            <a href="<?php echo Url::to('/inspecciones/updateVisitaPersonas', array('id' => $model->id, 'v' => $visita->id)) ?>" class="btn btn-default btn-sm gray float-right"><i class="fa fa-edit"></i> Editar esta visita
                                            </a>
                                        <?php } ?>
                                        <div class="timeline-content">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <ul>
                                                        <?php foreach ($visita->inspectoresActuantes as $inspector) { ?>
                                                            <li style="list-style: none !important;">
                                                                <?php if ($inspector->inspector0->es_inspector == 1) { ?>
                                                                    <span class=""><i class="fa fa-user"></i></span>
                                                                <?php } else { ?>
                                                                    <span class=""><i class="icon-eye-open"></i></span>
                                                                <?php } ?>
                                                                <?php echo $inspector->inspector0->nombre_apellidos; ?> [ <?php echo $inspector->inspector0->codigo; ?> ]
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </div>
                                                <div class="col-sm-4">
                                                    <?php if (isset($visita->descripcion)  && $visita->descripcion != "") { ?>
                                                        <div id="full-<?php echo $visita->id ?>" class="modal fade" aria-hidden="true" tabindex="-1" style="display: none;">
                                                            <div class="modal-dialog modal-full">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                        <h4 class="modal-title" style="color: #444">Resultado de la Inspecci&oacute;n</h4>
                                                                    </div>
                                                                    <div class="modal-body" style="color: #444">
                                                                        <?php echo $visita->descripcion ?>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                                    </div>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                        </div>
                                                    <?php  }  ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="timeline-footer">
                                            <button type="button" href="#full-<?php echo $visita->id ?>" data-original-title="Resultado de esta visita" data-placement="left" data-toggle="modal" class="btn btn-default btn-block tooltips">
                                                <span class="glyphicon glyphicon-search"></span>
                                                Leer el Resultado de la Inspecci&oacute;n
                                            </button>
                                        </div>
                                    </div>

                                </li>

                            <?php $nv++;
                            } ?>
                        </ul>
                    </div>
                </div>
                <!-- END PAGE TIMELINE-->
                <div class="clearfix"></div>
                <hr>
                <div class="col-xs-12">
                    <?php if (!$model->inspeccionCerrada("personas") && $model->containInspector(Yii::$app->user->id)) { ?>
                        <a href="<?php echo Url::to('/inspecciones/createVisitaPersona', array('id' => $model->id)) ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Registrar nueva visita
                        </a>
                    <?php } ?>
                </div>
                <div>
                    <a href="<?php echo Url::to('/inspecciones/index-personas') ?>" class="btn btn-primary">
                        <i class="icon-arrow-left"></i>
                        Regresar
                    </a>
                </div>
            </div>
        </div>
        <!-- END card card-->
    </div>
</div>
<!-- END PAGE CONTENT-->
</div>


<?php
$this->registerCssFile('@web/css/pages/profile.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/css/custom.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/css/pages/timeline.css', Yii::$app->session->get('dependencias'));
?>



<?php

$this->registerJsFile('@web/plugins/jquery-print-page/jquery.printPage.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));

?>

$js = <<<JS Custom.InitPrint(); ?>