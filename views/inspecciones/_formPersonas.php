<?php

use app\models\Entidades;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption">
                    <?php if ($persona->isNewRecord) { ?>
                        <i class="fa fa-plus"></i> Adicionar persona acreditada

                    <?php } else { ?>
                        <i class="fa fa-edit"></i> Editando persona acreditada ( <?php echo $persona->nombre_apellidos ?> )
                    <?php  } ?>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php $form1 = ActiveForm::begin([
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                    'clientOptions' => array(
                        'validateOnChange' => true,
                        'validateOnSubmit' => true,
                    ),
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                        'action' => '',
                    )
                ]); ?>

                <div class="container">
                    <?php echo $form1->errorSummary(
                        $persona,
                        'Por favor corrija los siguientes errores de ingreso:',
                        null,
                        array('class' => 'alert alert-danger')
                    );
                    ?>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?= Html::activeLabel($persona, 'nombre_apellidos', array('class' => 'control-label')); ?>
                                <?php echo $form1->textField($persona, 'nombre_apellidos', array('class' => 'form-control', 'placeholder' => 'nombre_apellidos', 'maxlength' => 200)); ?>
                                <span class="help-block"><?= Html::error($persona, 'nombre_apellidos', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?= Html::activeLabel($persona, 'telefono_contacto', array('class' => 'control-label')); ?>
                                <?= Html::activeTextInput($persona, 'telefono_contacto', array('class' => 'form-control', 'placeholder' => 'telefono_contacto', 'maxlength' => 45)); ?>
                                <span class="help-block"><?= Html::error($persona, 'telefono_contacto', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?= Html::activeLabel($persona, 'correo', array('class' => 'control-label')); ?>
                                <?php echo $form1->emailField($persona, 'correo', array('class' => 'form-control', 'placeholder' => 'correo', 'maxlength' => 45)); ?>
                                <span class="help-block"><?= Html::error($persona, 'correo', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <?= Html::activeLabel($persona, 'provincia', array('class' => 'control-label')); ?>
                                        <?= Html::activeDropDownList($persona, 'provincia0', ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                        <span class="help-block"><?= Html::error($persona, 'provincia', array('class' => 'text-danger')); ?></span>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <?= Html::activeLabel($persona, 'entidad', array('class' => 'control-label')); ?>
                                        <?= Html::activeDropDownList($persona, 'entidad0', ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                        <span class="help-block"><?= Html::error($persona, 'entidad', array('class' => 'text-danger')); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">

                                        <?= Html::activeLabel($persona, 'foto', array('class' => 'control-label')); ?>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                <?php if (!$persona->foto) { ?>
                                                    <img title="" src="<?= Yii::getAlias('@web/themes/sbadmin') ?>/assets/img/no-image.png" alt="NO hay imagen">

                                                <?php } else { ?>
                                                    <img title="" src="<?= Yii::getAlias('@web/uploads') ?>/images/personas/<?php echo $persona->foto ?>" alt="">
                                                <?php  } ?>
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar Imagen</span>
                                                    <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                    <?php echo $form1->fileField($persona, 'foto', array('id' => 'maxlength_alloptions', 'class' => 'default', 'maxlength' => 45)); ?>
                                                </span>
                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Quitar</a>
                                            </div>
                                            <span class="help-block"><?= Html::error($persona, 'foto', array('class' => 'text-danger')); ?></span>
                                        </div>
                                        <span class="label label-warning">Importante!</span>
                                        <span>La previsualizaci&oacute;n de im&aacute;genes solo funciona en navegadores modernos.</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-save" type="button" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a data-dismiss="modal" href="" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>


<?php


$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));


$js = <<<JS
 FormComponents.init();
 
    $(document).ready(function() {

        var nombre_appellidos = $('#PersonasAcreditadas_nombre_apellidos').val();
        var telefono = $('#PersonasAcreditadas_telefono_contacto').val();
        var correo = $('#PersonasAcreditadas_correo').val();
        var provincia = $('#PersonasAcreditadas_provincia').val();
        var entidad = $('#PersonasAcreditadas_entidad').val();
        var foto = $('#PersonasAcreditadas_foto').val();
        var formData = {
            PersonasAcreditadas: {
                nombre_apellidos: nombre_appellidos,
                telefono: telefono,
                correo: correo,
                provincia: provincia,
                entidad: entidad,
                foto: foto
            }
        };

        $('#btn-save').click(function() {
            $.ajax({
                url: '<?php echo Yii::$app->request->baseUrl; ?>/index.php?r=inspecciones/createPersonaAcreditada',
                type: "POST",
                dataType: "JSON",
                data: formData,
                success: function(json_format) {
                    alert(json_format.msg + ' - ' + json_format.foto);
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
        });

    });
JS;
$this->registerJs($js);
?>