<?php

use yii\helpers\Url;
?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
<?php
switch ($formType) {
    case "inspeccionPersona": ?>
        Inspecciones a Personas <small> Gesti&oacute;n de la informaci&oacute;n de Inspecciones</small>
    <?php
        break;
    case "inspeccionLabServ":
    ?> Inspecciones de Servicios <small> Gesti&oacute;n de la informaci&oacute;n de Inspecciones</small>
    <?php
        break;
    case "inspeccionProducto":
    ?> Inspecciones de Productos <small> Gesti&oacute;n de la informaci&oacute;n de Inspecciones</small>
    <?php
        break;
    case "inspeccionDac":
    ?> Inspecciones de Dispositivos Auxiliares de Carga <small> Gesti&oacute;n de la informaci&oacute;n de Inspecciones</small>
    <?php
        break;
    case "inspeccionMIT":
    ?> Inspecciones de Medios de Izado Terrestres <small> Gesti&oacute;n de la informaci&oacute;n de Inspecciones</small>
<?php
        break;
}
?>

<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="icon-barcode"></i>
    <a href="#">Inspecciones</a>
    <i class="fa fa-angle-right"></i>
</li>
<li class="active">
    <?php
    switch ($formType) {
        case "inspeccionPersona":
    ?> <a href="#"> Personas</a>
        <?php
            break;
        case "inspeccionLabServ":
        ?> <a href="#"> Servicios</a> .
        <?php
            break;
        case "inspeccionProducto":
        ?> <a href="#"> Productos</a>
        <?php
            break;
        case "inspeccionDac":
        ?> <a href="#"> Dispositivos Auxiliares de Carga</a>
        <?php
            break;
        case "inspeccionMIT":
        ?> <a href="#"> Medios de Izado Terrestres</a>
    <?php
            break;
    }
    ?>
</li>
<?php $this->endBlock(); ?>

<li class="btn-group">
    <?php
    switch ($formType) {
        case "inspeccionPersona": ?>
            <a href="<?php echo Url::to('/inspecciones/create-inspeccion-persona') ?>" class="btn btn-primary">
                <span style="color: #FFFFFF"><i class="fa fa-plus"></i> Nueva inspecci&oacute;n</span>
            </a>
        <?php
            break;
        case "inspeccionLabServ": ?>
            <a href="<?php echo Url::to('/inspecciones/create-inspeccion-lab-serv') ?>" id="newInspeccionLabServ" class="btn btn-primary">
                <span style="color: #FFFFFF"><i class="fa fa-plus"></i> Nueva inspecci&oacute;n</span>
            </a>
        <?php
            break;
        case "inspeccionProducto": ?>
            <a href="<?php echo Url::to('/inspecciones/create-inspeccion-producto') ?>" class="btn btn-primary">
                <span style="color: #FFFFFF"><i class="fa fa-plus"></i> Nueva inspecci&oacute;n</span>
            </a>
        <?php break;
        case "inspeccionDac": ?>
            <a href="<?php echo Url::to('/inspecciones/create-inspeccion-dac') ?>" class="btn btn-primary">
                <span style="color: #FFFFFF"><i class="fa fa-plus"></i> Nueva inspecci&oacute;n</span>
            </a>
        <?php break;
        case "inspeccionMIT": ?>
            <a href="<?php echo Url::to('/inspecciones/create-inspeccion-mit') ?>" class="btn btn-primary">
                <span style="color: #FFFFFF"><i class="fa fa-plus"></i> Nueva inspecci&oacute;n</span>
            </a>
    <?php break;
    } ?>
</li>



<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php } ?>

<br />
<br />

<?php

switch ($formType) {
    case "inspeccionPersona":
        echo $this->render('_indexPersona', array(
            'data' => $data, 'model' => $model,
            'pages' => $pages
        ));
        break;
    case "inspeccionLabServ":
        echo $this->render('_indexLabServ', array(
            'data' => $data, 'model' => $model,
            'pages' => $pages
        ));
        break;
    case "inspeccionProducto":
        echo $this->render('_indexProductos', array(
            'data' => $data, 'model' => $model,
            'pages' => $pages
        ));
        break;
    case "inspeccionDac":
        echo $this->render('_indexDacs', array(
            'data' => $data, 'model' => $model,
            'pages' => $pages
        ));
        break;
    case "inspeccionMIT":
        echo $this->render('_indexMITs', array(
            'data' => $data, 'model' => $model,
            'pages' => $pages
        ));
        break;
} ?>