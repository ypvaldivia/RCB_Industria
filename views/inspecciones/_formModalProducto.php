<?php

use app\models\Entidades;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="fa fa-plus"></i> Adicionar Producto</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php $form = ActiveForm::begin(); ?>
                <div class="form-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php echo $form2->labelEx($producto, 'nombre', array('class' => 'control-label')); ?>
                                <?php echo $form2->textField($producto, 'nombre', array('class' => 'form-control', 'placeholder' => 'nombre', 'maxlength' => 100)); ?>
                                <span class="help-block"><?php echo $form2->error($producto, 'nombre', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo $form2->labelEx($producto, 'propietario', array('class' => 'control-label')); ?>
                                <?php echo $form2->dropDownList($producto, 'propietario0', ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo $form2->error($producto, 'propietario', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo $form2->labelEx($producto, 'provincia', array('class' => 'control-label')); ?>
                                <?php echo $form2->dropDownList($producto, 'provincia0', ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo $form2->error($producto, 'provincia', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php echo $form2->labelEx($producto, 'descripcion', array('class' => 'control-label')); ?>
                                <?php echo $form2->textArea($producto, 'descripcion', array('class' => 'form-control', 'placeholder' => 'Descripción de las características del producto', 'maxlength' => 100)); ?>
                                <span class="help-block"><?php echo $form2->error($producto, 'nombre', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-save" type="button" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a data-dismiss="modal" href="" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>