<?php

use yii\helpers\Url;
use app\models\AcreditacionesIndustriales;
use app\models\Funcionarios;
use app\models\Oficinas;
use app\models\TiposHomologacion;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Inspecci&oacute;n a Servicios</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>

            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php $form = ActiveForm::begin(); ?>
                <div class="container">
                    <?php echo Html::errorSummary($model); ?>
                </div>
                <div class="form-body">
                    <img src="<?= Yii::getAlias('@web/uploads/images/bannerHeader.jpg') ?>" width="100%">
                    <div class="row">
                        <div class="col-md-10">
                            &nbsp;
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <span class="float-right">
                                    <h5>R7/PC401&nbsp;&nbsp;&nbsp;</h5>
                                </span>
                            </div>
                        </div>
                    </div>
                    <center>
                        <h2>Informe de Inspecci&oacute;n</h2>
                    </center>
                    <hr />
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'numero_control', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'numero_control', array('class' => 'form-control', 'placeholder' => '#####', 'maxlength' => 5)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'numero_control', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'entidad', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput(
                                    $model,
                                    'entidad',
                                    array(
                                        'id' => 'entidad',
                                        'class' => 'form-control select2',
                                        'multiple' => false,
                                    )
                                );  ?>
                                <span class="help-block"><?php echo Html::error($model, 'entidad', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'homologacion', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'homologacion', ArrayHelper::map(AcreditacionesIndustriales::findAll('tipo=1'), 'id', 'titulo_inspector'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'homologacion', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'certificado', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'certificado', array('class' => 'form-control', 'placeholder' => 'Escriba el detalle del tipo de homologación...', 'maxlength' => 200)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'certificado', array('class' => 'text-danger')); ?> </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'oficina', array('class' => 'control-label')); ?>
                                <?php $Oficinas = new Oficinas();  ?>
                                <?php echo Html::activeDropDownList(
                                    $model,
                                    'oficina',
                                    ArrayHelper::map(
                                        $Oficinas->getOficinaUsuarioReg(),
                                        'id',
                                        'codigo'
                                    ),
                                    [
                                        'class' => 'form-control select2me',
                                        'data-placeholder' => 'Seleccione un elemento...'
                                    ]
                                ); ?>
                                <span class="help-block"><?php echo Html::error($model, 'oficina', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'tipo_homologacion', array('class' => 'control-label')); ?>
                                <?php $TiposHomologacion = new TiposHomologacion();  ?>
                                <?php echo Html::activeDropDownList($model, 'tipo_homologacion', ArrayHelper::map($TiposHomologacion->findNotMITS(), 'id', 'tipo'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'tipo_homologacion', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="control-label"> No. Visita</label>
                                <br />
                                <span class="badge badge-success"> 1 </span>
                            </div>
                        </div>
                    </div>

                    <div class="well block-grey">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="label label-success">Servicios Aprobados</span>
                                <table id="sort_1" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <th style="width: 45%">Servicio</th>
                                        <th style="width: 10%">&nbsp;</th>
                                    </thead>
                                    <tbody id="tb-body">
                                        <tr id="a0">
                                            <td>No hay servicios seleccionados</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <span class="label label-danger">Servicios Desaprobados</span>
                                <table id="sort_2" class="table table-striped table-bordered" width="100%">
                                    <thead>
                                        <th style="width: 45%">Servicio</th>
                                        <th style="width: 10%">&nbsp;</th>
                                    </thead>
                                    <tbody id="tb-body">
                                        <tr id="a0">
                                            <td>No hay servicios seleccionados</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <?php echo Html::activeLabel($model, 'lab_serv_homologar', array('class' => 'control-label')); ?>
                                    <?php echo Html::activeTextInput(
                                        $model,
                                        'lab_serv_homologar',
                                        array(
                                            'id' => 'lab_serv_homologar',
                                            'class' => 'form-control select2 multi-select',
                                            'multiple' => true,
                                            'onclick' => "setServicio(this);"
                                        )
                                    );  ?>
                                    <span class="help-block"><?php echo Html::error($model, 'lab_serv_homologar', array('class' => 'text-danger')); ?></span>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <?php echo Html::activeLabel($model, 'lab_serv_desaprobados', array('class' => 'control-label')); ?>
                                    <?php echo Html::activeTextInput(
                                        $model,
                                        'lab_serv_desaprobados',
                                        array(
                                            'id' => 'lab_serv_desaprobados',
                                            'class' => 'form-control select2 multi-select',
                                            'multiple' => true,
                                            'onclick' => "setServicioDes(this);"
                                        )
                                    );  ?>
                                    <span class="help-block"><?php echo Html::error($model, 'lab_serv_desaprobados', array('class' => 'text-danger')); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <a style="margin:8% 0px " class=" tooltips btn btn-block purple" href="#fullServ" data-original-title="Adicionar un nuevo Servicio que no est&aacute; en la lista" data-toggle="modal"><span class="glyphicon glyphicon-cog"></span> Adicionar Servicio</a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'lugar_inspeccion', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'lugar_inspeccion', array('class' => 'form-control', 'placeholder' => 'lugar_inspeccion', 'maxlength' => 150)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'lugar_inspeccion', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'fecha', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'fecha', array('class' => 'form-control form-control input-medium date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'fecha', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr />

                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                <h3>Resultado de la Inspecci&oacute;n</h3>
                            </center>
                            <div class="form-group">
                                <?php echo Html::activeTextArea($model, 'descripcion', array('class' => 'ckeditor form-control')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'descripcion', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>

                    <hr />

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'inspectores_actuantes', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList(
                                    $model,
                                    'inspectores_actuantes',
                                    ArrayHelper::map($Funcionarios->findAllOrdered(), 'id', 'nombre_apellidos'),
                                    array(
                                        //'style' => 'width:200px !important', 'prompt'=>' ',
                                        'id' => 'inspectores_actuantes',
                                        'class' => 'multi-select',
                                        'multiple' => true,
                                    ),
                                    array('id' => 'inspectores_actuantes')
                                );  ?>
                                <span class="help-block"><?php echo Html::error($model, 'inspectores_actuantes', array('class' => 'text-danger')); ?>
                            </div>
                        </div>
                        <div class="col-md-7">

                        </div>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button id="inspeccion_servicios_submit" type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a href="<?php echo Url::to('/inspecciones/index-lab-serv') ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $form->hiddenField($model, 'aprobados_list'); ?>
                <?php echo $form->hiddenField($model, 'desaprobados_list'); ?>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>

<div id="fullServ" class="modal fade" aria-hidden="true" tabindex="-1" style="display: none;">
    <div class="modal-dialog" style="width: 55%;">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
            </div>
            <div class="modal-body">
                <div class="">
                    <div class="" data-rail-visible1="1" data-always-visible="1">
                        <div class="row">
                            <?php $this->render('_formModalServLab', array('servicio' => $servicio)); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/locales/bootstrap-wysihtml5.es-ES.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-modal/js/bootstrap-modalmanager.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-modal/js/bootstrap-modal.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-validation/dist/jquery.validate.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/ui-extended-modals.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
    FormComponents.init();
    UIExtendedModals.init();
    var url = '<?php echo Yii::$app->request->getBaseUrl(true) ?>/index.php?r=inspecciones/createServicioAcreditado';
    Custom.addModalServicio(url);
    ?>
    var aprobados = [];
    var desaprobados = [];

    function setServicio(select) {
        if (aprobados.length == 0) {
            $('#a0').remove();
        }
        if ($(select).val() != "") {
            $('#sort_1').append(
                '<tr id="a' + $(select).val() + '">' +
                '<td>' + $(select).prev().text() + ' </td>' +
                '<td style="text-align: center;">' +
                '<button type="button" class="btn btn-sm" onclick="removeElementAp(\'' + $(select).val() + '\');"><i class="fa fa-trash"></i></button>' +
                '</td>' +
                '</tr>');
            aprobados.push($(select).val());
            $("#FormInspeccionLabServ_aprobados_list").val(aprobados);
        }
    }

    function setServicioDes(select) {
        if (desaprobados.length == 0) {
            $('#d0').remove();
        }
        if ($(select).val() != "") {
            $('#sort_2').append(
                '<tr id="d' + $(select).val() + '">' +
                '<td>' + $(select).prev().text() + ' </td>' +
                '<td style="text-align: center;">' +
                '<button type="button" class="btn btn-sm" onclick="removeElementDes(\'' + $(select).val() + '\');"><i class="fa fa-trash"></i></button>' +
                '</td>' +
                '</tr>');
            desaprobados.push($(select).val());
            $("#FormInspeccionLabServ_desaprobados_list").val(desaprobados);
        }
    }

    function removeElementAp(id) {
        for (var i = 0; i < aprobados.length; i++) {
            if (aprobados[i] === id) {
                aprobados.splice(i, 1);
            }
        }
        $('#a' + id).remove();
        if (aprobados.length == 0) {
            $('#sort_1').append(
                '<tr id="a0">' +
                '<td>No hay servicios seleccionadas</td>' +
                '<td style="text-align: center;"></td>' +
                '<td style="text-align: center;"></td>' +
                '</tr>');
            $("#FormInspeccionLabServ_aprobados_list").val("");
        } else {
            $("#FormInspeccionLabServ_aprobados_list").val(aprobados);
        }
    }

    function removeElementDes(id) {
        for (var i = 0; i < desaprobados.length; i++) {
            if (desaprobados[i] === id) {
                desaprobados.splice(i, 1);
            }
        }
        $('#d' + id).remove();
        if (desaprobados.length == 0) {
            $('#sort_2').append(
                '<tr id="a0">' +
                '<td>No hay servicios seleccionadas</td>' +
                '<td style="text-align: center;"></td>' +
                '<td style="text-align: center;"></td>' +
                '</tr>');
            $("#FormInspeccionLabServ_desaprobados_list").val("");
        } else {
            $("#FormInspeccionLabServ_desaprobados_list").val(desaprobados);
        }
    }
JS;
$this->registerJs($js);
?>