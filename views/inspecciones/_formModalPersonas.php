<?php

use app\models\Entidades;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption">
                    <i class="fa fa-plus"></i> Adicionar persona acreditada
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php $form1 = ActiveForm::begin(); ?>

                <div class="container">
                    <?php echo $form1->errorSummary($persona); ?>
                </div>
                <div class="form-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?= Html::activeLabel($persona, 'nombre_apellidos', array('class' => 'control-label')); ?>
                                <?= Html::activeTextInput($persona, 'nombre_apellidos', array('class' => 'form-control', 'placeholder' => 'nombre_apellidos', 'maxlength' => 200)); ?>
                                <span class="help-block"><?= Html::error($persona, 'nombre_apellidos', array('class' => 'text-danger')); ?></span>
                            </div>
                            <div class="form-group">
                                <?= Html::activeLabel($persona, 'ci', array('class' => 'control-label')); ?>
                                <?= Html::activeTextInput($persona, 'ci', array('class' => 'form-control', 'placeholder' => 'ci', 'maxlength' => 11)); ?>
                                <span class="help-block"><?= Html::error($persona, 'ci', array('class' => 'text-danger')); ?></span>
                            </div>
                            <div class="form-group">
                                <?= Html::activeLabel($persona, 'telefono_contacto', array('class' => 'control-label')); ?>
                                <?= Html::activeTextInput($persona, 'telefono_contacto', array('class' => 'form-control', 'placeholder' => 'telefono_contacto', 'maxlength' => 45)); ?>
                                <span class="help-block"><?= Html::error($persona, 'telefono_contacto', array('class' => 'text-danger')); ?></span>
                            </div>
                            <div class="form-group">
                                <?= Html::activeLabel($persona, 'correo', array('class' => 'control-label')); ?>
                                <?= Html::activeInput($persona, 'correo', array('class' => 'form-control', 'placeholder' => 'correo', 'maxlength' => 45)); ?>
                                <span class="help-block"><?= Html::error($persona, 'correo', array('class' => 'text-danger')); ?></span>
                            </div>
                            <div class="form-group">
                                <?= Html::activeLabel($persona, 'provincia', array('class' => 'control-label')); ?>
                                <?= Html::activeDropDownList($persona, 'provincia0', ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?= Html::error($persona, 'provincia', array('class' => 'text-danger')); ?></span>
                            </div>
                            <div class="form-group">
                                <?= Html::activeLabel($persona, 'entidad', array('class' => 'control-label')); ?>
                                <?= Html::activeDropDownList($persona, 'entidad0', ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?= Html::error($persona, 'entidad', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-save" type="button" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a data-dismiss="modal" href="" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>

<script>
    $('#PersonasAcreditadas_ci').keydown(function(evt) {
        //var theEvent = evt || window.event;
        var key = evt.keyCode || evt.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]|\W/;
        var regex2 = /[0-9]|\./;
        var keycode = (evt.keyCode ? evt.keyCode : evt.which);
        var txt = $('#PersonasAcreditadas_ci').val();
        if (txt.contains('.')) {
            if (!regex2.test(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
            }
        } else {
            if (!regex.test(key)) {
                evt.returnValue = false;
                if (evt.preventDefault) evt.preventDefault();
            }
        }
    });
</script>