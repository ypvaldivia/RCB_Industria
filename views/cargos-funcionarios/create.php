<?php

use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Cargos Personal RCB <small>Gesti&oacute;n de la informaci&oacute;n de Cargos del Personal </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a class="breadcrumb-item" href="#"><i class="fa fa-briefcase"></i> Estructuras y Servicios - </a>
<a class="breadcrumb-item" href="<?php echo Url::to('/CargosFuncionarios/index') ?>">Cargos del Personal </a>
<span class="breadcrumb-item active">Nuevo cargo</span>

<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>