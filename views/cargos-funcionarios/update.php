<?php

use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Cargos del Personal <small>Gesti&oacute;n de la informaci&oacute;n de Cargos </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a class="breadcumb-item" href="#"><i class="fa fa-briefcase"></i> Estructuras y Servicios</a>
<a class="breadcumb-item" href="<?php echo Url::to('/CargosFuncionarios/index') ?>">Cargos</a>
<span class="breadcumb-item active">Actualizar cargo</span>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model));
?>