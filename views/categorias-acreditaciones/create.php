<?php

use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Acreditaciones Industriales
<small>Gesti&oacute;n de la informaci&oacute;n de Acreditaciones Industriales</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#"><i class="fa fa-briefcase"></i> Estructuras y Servicios</a>
<a href="<?php echo Url::to('/categorias-acreditaciones/index') ?>">Acreditaciones Industriales </a>
<a href="#">Nuevo </a>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>