<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Acreditaciones Industriales
<small>Gesti&oacute;n de la informaci&oacute;n de Acreditaciones Industriales</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a class="breadcumb-item" href="#"><i class="fa fa-briefcase"></i> Estructuras y Servicios - </a>
<a class="breadcumb-item" href="<?php echo Url::to('/CategoriasAcreditaciones/index') ?>">Acreditaciones Industriales </a>
<span class="breadcumb-item active" href="#">Actualizar </span>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model));
?>