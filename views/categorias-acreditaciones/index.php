<?php

use app\assets\DataTablesAsset;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Acreditaciones Industriales
<small>Gesti&oacute;n de la informaci&oacute;n de Acreditaciones Industriales</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a class="breadcumb-item" href="#"><i class="fa fa-briefcase"></i> Estructuras y Servicios - </a>
<a class="breadcumb-item active" href="#">Acreditaciones Industriales </a>
</li><?php $this->endBlock(); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong>
        <?php echo Yii::$app->session->getFlash('success'); ?> </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong>
        <?php echo Yii::$app->session->getFlash('error'); ?> </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong>
        <?php echo Yii::$app->session->getFlash('warning'); ?> </div>
<?php } ?>
<br />
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-globe"></i>Gesti&oacute;n de Acreditaciones Industriales
        </h3>
    </div>
    <div class="card-body">
        <div class="table-toolbar">
            <div class="btn-group">
                <a href="<?php echo Url::to('/categorias-acreditaciones/create') ?>" class="btn btn-success">Nuevo
                    <i class="fa fa-plus"></i></a>
                <a id="eliminar" href="#" class="btn btn-danger">Eliminar seleccionados <i class="fa fa-trash"></i></a>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="main_table">
            <thead>
                <tr>
                    <th class="table-checkbox">
                        <input id="select_all" type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                    </th>
                    <th> Acreditaci&oacute;n</th>
                    <th>
                        &nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $value) {
                ?>
                    <tr class="odd gradeX">
                        <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id ?>" /></td>
                        <td><?php echo $value->grupo ?></td>
                        <td>
                            <a href="<?php echo Url::to(['/categorias-acreditaciones/update', 'id' => $value->id]) ?>" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Editar</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
    } );
JS;
$this->registerJs($js); ?>