<?php

use yii\helpers\Url;

?>
<?php Yii::$app->getComponent("bootstrap"); ?>
<br />
<div class="container">
  <div class="navbar">
    <div class="navbar-inner">
      <ul class="nav">
        <li class="divider-vertical"><a href="<?php echo Url::to('/certificado-persona/index') ?>">Certificado Persona</a></li>
        <li class="divider-vertical"><a href="<?php echo Url::to('/dependencias/index') ?>">Dependencias</a></li>
        <li class="divider-vertical"><a href="<?php echo Url::to('/entidad/index') ?>">Entidades</a></li>
        <li class="divider-vertical"><a href="<?php echo Url::to('/inspectores/index') ?>">Inspectores</a></li>
        <li class="divider-vertical"><a href="<?php echo Url::to('/oficina/index') ?>">Oficinas</a></li>
        <li class="divider-vertical"><a href="<?php echo Url::to('/personal-calificado/index') ?>">PersonalCalificado</a></li>
        <li class="divider-vertical"><a href="<?php echo Url::to('/provincia/index') ?>">Provincia</a></li>
        <li class="divider-vertical"><a href="<?php echo Url::to('/tipo-homologacion/index') ?>">Tipo Homologacion</a></li>
      </ul>
    </div>
  </div>

  <?php echo $content; ?>
</div>