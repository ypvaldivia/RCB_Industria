<?php

use app\assets\MaterialProAsset;
use app\widgets\Alert;
use yii\helpers\Html;

MaterialProAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>

<body class="fix-header card-no-border" id="page-top">
  <?php $this->beginBody() ?>
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
  </div>

  <!-- ============================================================== -->
  <!-- Main wrapper - style you can find in pages.scss -->
  <!-- ============================================================== -->
  <div id="main-wrapper">

    <!-- Topbar -->
    <?= $this->render('_topbar'); ?>
    <!-- End of Topbar -->

    <!-- Sidebar -->
    <?= $this->render('_sidebar'); ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div class="page-wrapper">
      <!-- ============================================================== -->
      <!-- Container fluid  -->
      <!-- ============================================================== -->
      <div class="container-fluid">
        <!-- PageTitle -->
        <?= $this->render('_pageTitle'); ?>
        <!-- Content -->
        <?= $content ?>
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="footer">
        <span>Registro Cubano de Buques. Copyright &copy; <?= date('Y') ?>. Todos los derechos reservados. Cimab.</span>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <?= $this->render('_logout'); ?>

  <?php

  $js = <<<JS
    $('.scroller').slimScroll();
  JS;
  $this->registerJs($js);
  ?>


  <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>