<?php

use yii\bootstrap4\Breadcrumbs;
use yii\helpers\Url;

?>
<div class="row page-titles">
    <div class="col-md-12 col-12 align-self-center">
        <h4 class="text-themecolor">
            <?= $this->title ?? 'RCB' ?>
            <small class="text-muted"><?= isset($this->params['subtitle']) ? $this->params['subtitle'] : 'Industria' ?></small>
        </h4>
    </div>
    <div class="col-md-12 col-12 align-self-center">
        <?= Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Inicio',
                'url' => Url::home(),
                'template' => '<i class="mdi mdi-home" i></i>&nbsp;{link}&nbsp;/&nbsp;'
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
</div>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        <h3 class="text-success"><i class="fa fa-check-circle"></i> Éxito!</h3> <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        <h3 class="text-danger"><i class="fa fa-exclamation-triangle"></i> Error!</h3> <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Alerta!</h3> <?php echo Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('info')) { ?>
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
        <h3 class="text-info"><i class="fa fa-exclamation-triangle"></i> Aviso!</h3> <?php echo Yii::$app->session->getFlash('info'); ?>
    </div>
<?php } ?>