<?php

use app\components\Notificaciones;
use app\models\Funcionarios;
use webvimark\modules\UserManagement\models\User;
use yii\helpers\Url;

$theme = $this->theme;

$n = new Notificaciones();
if ($n->getCargoUsuarioRegistrado() > 1) {
    $p = $n->getInspeccionesPendientesAprobacionPersonas();
    $s = $n->getInspeccionesPendientesAprobacionSP();
    $m = $n->getInspeccionesPendientesAprobacionMI();
    $prd = $n->getInspeccionesPendientesAprobacionProd();
    $da = $n->getInspeccionesPendientesAprobacionDacs();
} else {
    $p = $n->getInspeccionesPorCerrarPersonas();
    $s = $n->getInspeccionesPorCerrarSP();
    $m = $n->getInspeccionesPorCerrarMI();
    $prd = $n->getInspeccionesPorCerrarProd();
    $da = $n->getInspeccionesPorCerrarDacs();
}

$funcionariologueado = Funcionarios::findOne(['usuario' => Yii::$app->user->id]);
?>

<!-- Topbar header - style you can find in pages.scss -->
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light" style="background-color: #00b3e3 !important;">
        <!-- Logo -->
        <div class="navbar-header" style="background-color: #00b3f3 !important;">
            <a class="navbar-brand" href="<?= Url::home() ?>">
                <!-- Logo icon -->

                <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                <!-- Light Logo icon -->
                <img src="<?= $theme->getUrl('img/logo.png') ?>" alt="homepage" class="img-fluid" />

                <!--End Logo icon -->
                <!-- Logo text -->
                <span class="text-light">
                    <img src="<?= $theme->getUrl('img/text-logo.png') ?>" alt="homepage" class="img-fluid" />
                </span>
            </a>
        </div>
        <!-- End Logo -->
        <div class="navbar-collapse">

            <!-- toggle and nav items -->
            <ul class="navbar-nav mr-auto mt-md-0">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
            </ul>

            <!-- User profile and Notifications -->
            <ul class="navbar-nav my-lg-0">

                <!-- Inspecciones Persona -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i>
                        <?php if (count($p) > 0) { ?>
                            <div class="notify">
                                <span class="heartbit"></span>
                                <span class="point"></span>
                            </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                        <ul>
                            <li>
                                <div class="message-center">
                                    <?php
                                    if ($s != null) {
                                        foreach ($p as $persona) { ?>
                                            <!-- Message -->
                                            <a href="<?= Url::to([
                                                            'inspecciones/detalles-personas',
                                                            'id' => $persona->id
                                                        ]) ?>">
                                                <div class="btn btn-danger btn-circle">
                                                    <i class="fa fa-user"></i>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5><?= $persona->numero_control ?></h5>
                                                    <i class="mdi-arrow-right-bold-circle-outline"></i>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                    <?php }
                                    } ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } else { ?>
                    </a>
                <?php } ?>
                </li>
                <!-- End Inspecciones Persona -->

                <!-- Inspecciones Servicio -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-certificate"></i>
                        <?php if (count($s) > 0) { ?>
                            <div class="notify">
                                <span class="heartbit"></span>
                                <span class="point"></span>
                            </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                        <ul>
                            <li>
                                <div class="message-center">
                                    <?php
                                    if ($s != null) {
                                        foreach ($s as $servicio) { ?>
                                            <!-- Message -->
                                            <a href="<?= Url::to([
                                                            '/inspecciones/detalles-lab-serv',
                                                            'id' => $servicio->id
                                                        ]) ?>">
                                                <div class="btn btn-danger btn-circle">
                                                    <i class="fa fa-certificate"></i>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5><?= $servicio->numero_control ?></h5>
                                                    <i class="mdi-arrow-right-bold-circle-outline"></i>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                    <?php }
                                    } ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } else { ?>
                    </a>
                <?php } ?>
                </li>
                <!-- End Inspecciones Servicio -->

                <!-- Inspecciones Producto -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fire-extinguisher"></i>
                        <?php if (count($prd) > 0) { ?>
                            <div class="notify">
                                <span class="heartbit"></span>
                                <span class="point"></span>
                            </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                        <ul>
                            <li>
                                <div class="message-center">
                                    <?php
                                    if ($prd != null) {
                                        foreach ($prd as $producto) { ?>
                                            <!-- Message -->
                                            <a href="<?= Url::to([
                                                            'inspecciones/detallesProductos',
                                                            'id' => $producto->id
                                                        ]) ?>">
                                                <div class="btn btn-danger btn-circle">
                                                    <i class="fa fa-fire-extinguisher"></i>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5><?= $producto->numero_control ?></h5>
                                                    <i class="mdi-arrow-right-bold-circle-outline"></i>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                    <?php }
                                    } ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } else { ?>
                    </a>
                <?php } ?>
                </li>
                <!-- End Inspecciones Producto -->

                <!-- Inspecciones DACs -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-gear"></i>
                        <?php if (count($da) > 0) { ?>
                            <div class="notify">
                                <span class="heartbit"></span>
                                <span class="point"></span>
                            </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                        <ul>
                            <li>
                                <div class="message-center">
                                    <?php
                                    if ($da != null) {
                                        foreach ($da as $dac) { ?>
                                            <!-- Message -->
                                            <a href="<?= Url::to([
                                                            'inspecciones/detalles-dac',
                                                            'id' => $dac->id
                                                        ]) ?>">
                                                <div class="btn btn-danger btn-circle">
                                                    <i class="fa fa-gear"></i>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5><?= $dac->numero_control ?></h5>
                                                    <i class="mdi-arrow-right-bold-circle-outline"></i>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                    <?php }
                                    } ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } else { ?>
                    </a>
                <?php } ?>
                </li>
                <!-- End Inspecciones DACs -->

                <!-- Inspecciones MITs -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-cogs"></i>
                        <?php if (count($m) > 0) { ?>
                            <div class="notify">
                                <span class="heartbit"></span>
                                <span class="point"></span>
                            </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right mailbox scale-up">
                        <ul>
                            <li>
                                <div class="message-center">
                                    <?php
                                    if ($m != null) {
                                        foreach ($m as $medio) { ?>
                                            <!-- Message -->
                                            <a href="<?= Url::to([
                                                            'inspecciones/detalles-mit',
                                                            'id' => $medio->id
                                                        ]) ?>">
                                                <div class="btn btn-danger btn-circle">
                                                    <i class="fa fa-cogs"></i>
                                                </div>
                                                <div class="mail-contnet">
                                                    <h5><?= $medio->numero_control ?></h5>
                                                    <i class="mdi-arrow-right-bold-circle-outline"></i>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                    <?php }
                                    } ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                <?php } else { ?>
                    </a>
                <?php } ?>
                </li>
                <!-- End Inspecciones MITs -->

                <!-- Profile -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?= $theme->getUrl('img/avatar.png') ?>" alt="user" class="profile-pic" />
                        <small><?= $funcionariologueado->nombre_apellidos ?></small>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right scale-up">
                        <ul class="dropdown-user">
                            <li><a href="<?= Url::to(['user-management/auth/change-own-password']) ?>"><i class="ti-settings"></i> Cambiar Contraseña</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" id="trigger_fullscreen"><i class="mdi mdi-arrow-expand-all"></i> Pantalla Completa</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#" data-toggle="modal" data-target="#logoutModal"><i class="fa fa-power-off"></i> Cerrar sesión</a></li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>
    </nav>
</header>
<!-- End Topbar header -->