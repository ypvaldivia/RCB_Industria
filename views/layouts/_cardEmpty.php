<div class="card shadow">
    <div class="card-header">
        <i class="fa fa-reorder"></i> Basica
        <div class="card-actions">
            <a class="" data-action="collapse"><i class="ti-minus"></i></a>
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
            <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
        </div>
    </div>
    <div class="card-body collapse show">

    </div>
</div>

<div class="card card-outline-success shadow">
    <div class="card-header">
        <h6 class="text-white"><i class="fa fa-reorder"></i> ConColor
            <div class="card-actions">
                <a class="text-white" data-action="collapse"><i class="ti-minus"></i></a>
                <a class="btn-minimize text-white" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                <a class="btn-close text-white" data-action="close"><i class="ti-close"></i></a>
            </div>
        </h6>
    </div>
    <div class="card-body collapse show">

    </div>
</div>

<div class="card shadow">
    <div class="card-header">
        <i class="fa fa-reorder"></i> ConScroller
        <div class="card-actions">
            <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
            <a class="btn-close" data-action="close"><i class="ti-close"></i></a>
        </div>
    </div>
    <div class="card-body scroller">

    </div>
</div>