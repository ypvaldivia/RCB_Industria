<?php

use yii\helpers\Url;

$theme = $this->theme;
?>
<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav px-0">
            <ul id="sidebarnav">
                <!-- Nav Item -->
                <li>
                    <a class="has-arrow" href="#" data-toggle="collapse" data-target="#anuario">
                        <i class="ti-calendar"></i>
                        <span>Anuario</span>
                    </a>
                    <ul id="anuario" class="collapse" aria-labelledby="headingTwo" data-parent="#anuario">
                        <li><a class="text-muted" href="<?= Url::to(['anuario/edicion']) ?>">Administrar</a></li>
                        <li><a class="collapse-item" href="<?= Url::to(['anuario/descargar']) ?>">Descargar</a></li>
                    </ul>
                </li>

                <!-- Nav Item -->
                <li class="nav-item">
                    <a class="has-arrow" href="#" data-toggle="collapse" data-target="#librocertificados">
                        <i class="ti-book"></i>
                        <span>Libro de Certificados</span>
                    </a>
                    <ul id="librocertificados" class="collapse" aria-labelledby="headingTwo" data-parent="#librocertificados">
                        <li><a href="<?= Url::to(['site/libro-certificados', 'type' => 'inspeccionPersona']) ?>">Personas</a></li>
                        <li><a href="<?= Url::to(['site/libro-certificados', 'type' => 'inspeccionLabServ']) ?>">Servicios</a></li>
                        <li><a href="<?= Url::to(['site/libro-certificados', 'type' => 'inspeccionProducto']) ?>">Productos</a></li>
                        <li><a href="<?= Url::to(['site/libro-certificados', 'type' => 'inspeccionDAC']) ?>">DACs</a></li>
                        <li><a href="<?= Url::to(['site/libro-certificados', 'type' => 'inspeccionMIT']) ?>">Medios Izado</a></li>
                    </ul>
                </li>

                <!-- Nav Item -->
                <li class="nav-item">
                    <a class="has-arrow" href="#" data-toggle="collapse" data-target="#objetosinspeccion">
                        <i class="ti-receipt"></i>
                        <span>Objetos de Inspección</span>
                    </a>
                    <ul id="objetosinspeccion" class="collapse" aria-labelledby="headingTwo" data-parent="#objetosinspeccion">
                        <li><a href="<?= Url::to(['personas-acreditadas/index']) ?>">Personas</a></li>
                        <li><a href="<?= Url::to(['servicios-laboratorios-acreditados/index']) ?>">Servicios</a></li>
                        <li><a href="<?= Url::to(['productos-acreditados/index']) ?>">Productos</a></li>
                        <li><a href="<?= Url::to(['dacs-acreditados/index']) ?>">DACs</a></li>
                        <li><a href="<?= Url::to(['medios-izado-acreditados/index']) ?>">Medios de Izado</a></li>
                        <li><a href="<?= Url::to(['categorias-medios-izado/index']) ?>">Categorías MIT</a></li>
                        <li><a href="<?= Url::to(['tipos-dacs/index']) ?>">Tipos de DACs</a></li>
                        <li><a href="<?= Url::to(['tipos-medios-izado/index']) ?>">Tipos de MIT</a></li>
                        <li><a href="<?= Url::to(['entidades/index']) ?>">Entidades</a></li>
                        <li><a href="<?= Url::to(['organismos/index']) ?>">Organismos</a></li>
                    </ul>
                </li>

                <!-- Nav Item -->
                <li class="nav-item">
                    <a class="has-arrow" href="#" data-toggle="collapse" data-target="#inspecciones">
                        <i class="ti-agenda"></i>
                        <span>Inspecciones</span>
                    </a>
                    <ul id="inspecciones" class="collapse" aria-labelledby="headingTwo" data-parent="#inspecciones">
                        <li><a href="<?= Url::to(['inspecciones/index-personas']) ?>">Personas</a></li>
                        <li><a href="<?= Url::to(['inspecciones/index-lab-serv']) ?>">Servicios</a></li>
                        <li><a href="<?= Url::to(['inspecciones/index-productos']) ?>">Productos</a></li>
                        <li><a href="<?= Url::to(['inspecciones/index-dacs']) ?>">DACs</a></li>
                        <li><a href="<?= Url::to(['inspecciones/index-mits']) ?>">MITs</a></li>
                        <li><a href="<?= Url::to(['niveles-inspeccion/index']) ?>">Niveles de Aprobación</a></li>
                        <li><a href="<?= Url::to(['tipos-homologacion/index']) ?>">Tipos de Inspección</a></li>
                        <li><a href="<?= Url::to(['tipos-certificados/index']) ?>">Tipos de Certificados</a></li>
                    </ul>
                </li>


                <!-- Nav Item -->
                <li class="nav-item">
                    <a class="has-arrow" href="#" data-toggle="collapse" data-target="#estructuraservicios">
                        <i class="ti-archive"></i>
                        <span>Estructura y Servicios</span>
                    </a>
                    <ul id="estructuraservicios" class="collapse" aria-labelledby="headingTwo" data-parent="#estructuraservicios">
                        <!-- Nav Item -->
                        <li class="nav-item">
                            <a class="has-arrow" href="#" data-toggle="collapse" data-target="#personalrcb">
                                <i class="ti-user"></i>
                                <span>Personal RCB</span>
                            </a>
                            <ul id="personalrcb" class="collapse" aria-labelledby="headingTwo" data-parent="#personalrcb">
                                <li><a href="<?= Url::to(['funcionarios/index']) ?>">Personal RCB</a></li>
                                <li><a href="<?= Url::to(['cargos-funcionarios/index']) ?>">Cargos del Personal</a></li>
                                <li><a href="<?= Url::to(['categorias-funcionarios/index']) ?>">Categorías del Personal</a></li>
                            </ul>
                        </li>

                        <!-- Nav Item -->
                        <li class="nav-item">
                            <a class="has-arrow" href="#" data-toggle="collapse" data-target="#alertaspersonal">
                                <i class="ti-alert"></i>
                                <span>Alertas para Personal</span>
                            </a>
                            <ul id="alertaspersonal" class="collapse" aria-labelledby="headingTwo" data-parent="#alertaspersonal">
                                <li><a href="<?= Url::to(['funcionarios/proximos-vencimientos']) ?>">Próximos Vencimientos</a></li>
                                <li><a href="<?= Url::to(['funcionarios/vencimientos']) ?>">Vencimientos</a></li>
                            </ul>
                        </li>

                        <!-- Nav Item -->
                        <li class="nav-item">
                            <a href="<?= Url::to(['acreditaciones-industriales/index']) ?>">
                                <span>Servicios de Industria</span></a>
                        </li>

                        <!-- Nav Item -->
                        <li class="nav-item"><a href="<?= Url::to(['categorias-acreditaciones/index']) ?>">Acreditaciones Industriales</a></li>

                        <!-- Nav Item -->
                        <li class="nav-item"><a href="<?= Url::to(['oficinas/index']) ?>">Oficinas</a></li>

                        <!-- Nav Item -->
                        <li class="nav-item"><a href="<?= Url::to(['provincias/index']) ?>">Provincias</a></li>

                        <!-- Nav Item -->
                        <li class="nav-item"><a href="<?= Url::to(['estados-inspecciones/index']) ?>">Estados Inspecciones</a></li>
                    </ul>
                </li>

                <!-- Nav Item -->
                <li class="nav-item">
                    <a class="has-arrow" href="#" data-toggle="collapse" data-target="#configuracion">
                        <i class="ti-settings"></i>
                        <span>Configuración General</span>
                    </a>
                    <ul id="configuracion" class="collapse" aria-labelledby="headingTwo" data-parent="#configuracion">
                        <!-- Nav Item -->
                        <li class="nav-item">
                            <a href="<?= Url::to(['site/change-banner']) ?>">
                                <i class="ti-share-alt"></i>
                                <span>Modificar banner</span></a>
                        </li>

                        <!-- Nav Item -->
                        <li class="nav-item">
                            <a href="<?= Url::to(['configuracion/update', 'id' => Yii::$app->user->id]) ?>">
                                <i class="ti-email"></i>
                                <span>Servidor de correo</span></a>
                        </li>

                        <!-- Nav Item -->
                        <li class="nav-item">
                            <a href="<?= Url::to(['backup']) ?>">
                                <i class="ti-stack-overflow"></i>
                                <span>Salva de Base de Datos</span></a>
                        </li>

                        <!-- Nav Item -->
                        <li class="nav-item">
                            <a href="<?= Url::to(['actualizador/descargar-actualizacion']) ?>">
                                <i class="ti-android"></i>
                                <span>Actualización APK</span></a>
                        </li>

                        <!-- Nav Item -->
                        <li class="nav-item">
                            <a class="has-arrow" href="#" data-toggle="collapse" data-target="#seguridad">
                                <i class="ti-key"></i>
                                <span>Seguridad</span>
                            </a>
                            <ul id="seguridad" class="collapse" aria-labelledby="headingTwo" data-parent="#seguridad">
                                <li><a href="<?= Url::to(['user-management/user']) ?>">Usuarios</a></li>
                                <li><a href="<?= Url::to(['user-management/role']) ?>">Roles de usuario</a></li>
                                <li><a href="<?= Url::to(['user-management/permission']) ?>">Permisos por Roles</a></li>
                                <li><a href="<?= Url::to(['user-management/auth-item-group']) ?>">Grupos de Permisos</a></li>
                                <li><a href="<?= Url::to(['user-management/user-visit-log']) ?>">Trazas de usuarios</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <!-- Nav Item -->
                <li class="nav-item">
                    <a class="has-arrow" href="#" data-toggle="collapse" data-target="#Manual">
                        <i class="ti-help"></i>
                        <span>Manual de usuario</span>
                    </a>
                    <ul id="Manual" class="collapse" aria-labelledby="headingTwo" data-parent="#Manual">
                        <li><a href="<?= Yii::getAlias('@web/uploads/files/Manual-RCB.chm') ?>">Descargar</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</aside>