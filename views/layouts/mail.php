<html>

<head>
	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
</head>

<body>
	<style type="text/css">
		.container {
			margin: 10px auto;
			border: 1px solid #eeeeee;
			padding: 10px;
		}

		h1,
		h2,
		h3 {
			color: #0782C1 !important;
			font-weight: bold;
			margin-bottom: 0;
		}

		h1 {
			font-size: 16pt;
		}

		h2 {
			font-size: 14pt;
		}

		h3 {
			font-size: 12pt;
		}

		body,
		p {
			font-size: 11pt !important;
			font-family: "Calibri";
		}

		h4 {
			font-size: 12pt;
			margin-bottom: 0;
			font-family: "Calibri";
			font-weight: 500;
		}
	</style>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<img src="bannerHeader.jpg" alt="banner" style="margin-left: 5px;" />
				<h1> Sistema de notificaci&oacute;n de alertas</h1>
				<h4><?php if (isset($data['subject'])) echo $data['subject'];  ?></h4>
				<p><?php if (isset($data['description'])) echo $data['description'];  ?></p>
				<hr />
			</div>
		</div>
	</div>
</body>

</html>