<?php

use yii\helpers\Url;

?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Tipos Homologaci&oacute;n <small>Gesti&oacute;n de la informaci&oacute;n de Tipos Homologaci&oacute;n</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="icon-barcode"></i>
    <a href="#">Inspecciones - </a>
    <a href="<?php echo Url::to('/TiposHomologacion/index') ?>">Tipos Homologaci&oacute;n </a>
    <a href="#">Nuevo </a>
</li>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>