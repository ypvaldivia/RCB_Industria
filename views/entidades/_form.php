<?php

use app\models\Oficinas;
use app\models\Organismos;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Entidades</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php
                $form = ActiveForm::begin([
                    'id' => 'entidades-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                ]);
                ?> <div class="container">
                    <?php echo Html::errorSummary($model);
                    ?>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'nombre', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'nombre', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'nombre', 'maxlength' => 200)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'nombre', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'siglas', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'siglas', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'siglas', 'maxlength' => 150)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'siglas', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'persona_contacto', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'persona_contacto', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'persona_contacto', 'maxlength' => 100)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'persona_contacto', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'telefono_contacto', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'telefono_contacto', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'telefono_contacto', 'maxlength' => 100)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'telefono_contacto', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'correo', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'correo', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'correo', 'maxlength' => 100)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'correo', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'direccion', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'direccion', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'Dirección de la entidad', 'maxlength' => 200)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'direccion', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'organismo', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'organismo0', ArrayHelper::map(Organismos::find()->all(), 'id', 'nombre'), array('class' => 'form-control input-xlarge select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'organismo', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'provincia', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'provincia0', ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'), array('class' => 'form-control input-xlarge select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'provincia', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'oficina_acredita', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'oficinaAcredita', ArrayHelper::map(Oficinas::find()->all(), 'id', 'nombre'), array('class' => 'form-control input-xlarge select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'oficina_acredita', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a href="<?php echo Url::to('/entidades/index') ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>


<?php


$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/plugins/jquery-validation/dist/jquery.validate.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-validation/dist/additional-methods.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/form-validation-entidades.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
FormComponents.init();
JS;
$this->registerJs($js);
?>