<?php

use yii\helpers\Url;

$this->beginBlock('block_titulo_pagina'); ?>
Entidades <small>Gesti&oacute;n de la informaci&oacute;n de Entidades</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="fa fa-user"></i>
    <a href="#">Objetos de Inspecci&oacute;n - </a>
    <a href="<?php echo Url::to('/Entidades/index') ?>">Entidades </a>
    <a href="#">Nuevo </a>
</li>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>