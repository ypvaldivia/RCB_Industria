<?php

use app\assets\DataTablesAsset;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\LinkPager;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Entidades <small>Gesti&oacute;n de la informaci&oacute;n de Entidades</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a class="breadcumb-item" href="#"> Objetos de Inspecci&oacute;n</a>
<span class="breadcumb-item active" href="#">Entidades </span>
<?php $this->endBlock(); ?>
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fa fa-search"></i> B&uacute;squeda avanzada
        </h3>
    </div>
    <div class="card-body">
        <?php $form = ActiveForm::begin(['options' => ['class' => 'form-inline'],]) ?>

        <div class="col-md-4">
            <?php echo Html::activeTextInput($searcher, 'nombre', array('class' => 'form-control', 'placeholder' => 'Nombre de la entidad')); ?>
        </div>

        <div class="col-md-4">
            <?php echo Html::activeDropDownList($searcher, 'provincia', ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'), array('class' => 'form-control select2me', 'style' => 'width:200px !important', 'prompt' => 'Seleccione la Provincia')); ?>
        </div>
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary mb-2']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>
<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php } ?>

<br />
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-globe"></i>Gesti&oacute;n de Entidades</h3>
        Mostrando <?= count($data) ?> de <?= $pages->pageCount ?> elementos
    </div>
    <div class="card-body">
        <div class="table-toolbar">
            <div class="btn-group" style="padding-top: 8px">
                <a href="<?php echo Url::to('/entidades/create') ?>" class="btn btn-success">Nuevo <i class="fa fa-plus"></i></a>
                <a id="eliminar" href="#" class="btn btn-danger">Eliminar seleccionados <i class="fa fa-trash"></i></a>
            </div>
            <div class="btn-group float-right">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php echo LinkPager::widget(['pagination' => $pages,]); ?>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="main_table">
            <thead>
                <tr>
                    <th class="table-checkbox">
                        <input id="select_all" type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                    </th>
                    <th>Nombre</th>
                    <th>Organismo</th>
                    <th>Provincia</th>
                    <th>Correo </th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $value) {
                ?>
                    <tr class="odd gradeX">
                        <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id ?>" /></td>
                        <td><?php echo $value->nombre ?></td>
                        <td><?php echo $value->organismo0->nombre ?? '' ?></td>
                        <td><?php echo $value->provincia0->nombre ?? '' ?></td>
                        <td><?php echo $value->correo ?></td>
                        <td>
                            <a href="<?php echo Url::to(['/entidades/update', 'id' => $value->id]) ?>" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Editar</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <hr />
        <div class="row">
            <div class="col-md-4 float-left">
                Mostrando <?= count($data) ?> de <?= $pages->pageCount ?> elementos
            </div>
            <div class="col-md-8">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php echo LinkPager::widget(['pagination' => $pages,]); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
    } );
JS;
$this->registerJs($js); ?>