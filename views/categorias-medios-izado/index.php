<?php

use app\assets\DataTablesAsset;
use app\assets\SBAdminAsset;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Servicios
<small>Gesti&oacute;n de la informaci&oacute;n de Personas Acreditadas</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<span class="breadcrumb-item active">Categor&iacute;as de Medios de Izado</span>
<?php $this->endBlock(); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php } ?>

<br />
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title text-primary">
            <i class="fa fa-globe"></i>
            Gesti&oacute;n de Categor&iacute;as de Medios de Izado
        </h3>
    </div>
    <div class="card-body">
        <div class="container">
            <div class="btn-group">
                <a href="<?php echo Url::to('/categorias-medios-izado/create') ?>" class="btn btn-success">Nuevo <i class="fa fa-plus"></i></a>
                <a id="eliminar" href="#" class="btn btn-danger">Eliminar seleccionados <i class="fa fa-trash"></i></a>
            </div>
            <div class="btn-group float-right" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Exportar
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="#" onClick="$('#main_table').tableExport({type:'png',escape:'false'});"> <i class="fa fa-camera"></i> PNG</a>
                    <a class="dropdown-item" href="#" onClick="$('#main_table').tableExport({type:'pdf',escape:'false',htmlContent:'true'});"> <i class="fa fa-file"></i> PDF</a>
                    <a class="dropdown-item" href="#" onClick="$('#main_table').tableExport({type:'excel',escape:'false'});"> <i class="fa fa-table"></i> XLS</a>
                </div>
            </div>
        </div>
        <br />
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="main_table">
                <thead>
                    <tr>
                        <th class="table-checkbox">
                            <input id="select_all" type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                        </th>
                        <th> Categor&iacute;a</th>
                        <th> Descripcion</th>
                        <th>
                            &nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($data as $value) {
                    ?>
                        <tr class="odd gradeX">
                            <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id ?>" /></td>
                            <td><?php echo $value->categoria ?></td>
                            <td style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis; max-width: 300px"><?php echo $value->descripcion ?></td>
                            <td>
                                <a href="<?php echo Url::to(['/categorias-medios-izado/update', 'id' => $value->id]) ?>" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> Editar</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
    } );
JS;
$this->registerJs($js); ?>