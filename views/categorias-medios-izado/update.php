<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Categor&iacute;as de Medios de Izado<small> Gesti&oacute;n de la informaci&oacute;n de Categor&iacute;as de Medios de Izado</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#" class="breadcrumb-item"><i class="fa fa-cogs"></i> Objetos de Inspecci&oacute;n</a>
<a href="<?php echo Url::to('/categorias-medios-izado/index') ?>" class="breadcrumb-item"><i class="fa fa-cogs"></i> Categor&iacute;as de Medios de Izado</a>
<span class="breadcrumb-item active">Actualizar</span>
<?php $this->endBlock(); ?>

<?= $this->render('_form', ['model' => $model]); ?>