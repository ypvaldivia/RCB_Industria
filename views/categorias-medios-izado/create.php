<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Categor&iacute;as de Medios de Izado<small> Gesti&oacute;n de la informaci&oacute;n de Categor&iacute;as de Medios de Izado</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#"><i class="fa fa-cogs"></i>Objetos de Inspecci&oacute;n</a>
<a href="<?php echo Url::to('/categorias-medios-izado/index') ?>">Categor&iacute;as de Medios de Izado </a>
<span class="breadcumb-item active">Nueva</span>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>