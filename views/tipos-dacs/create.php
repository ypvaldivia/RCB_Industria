<?php

use yii\helpers\Url;

?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Tipos Dacs <small>Gesti&oacute;n de la informaci&oacute;n de Tipos Dacs </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="fa fa-cogs"></i>
    <a href="#">Objetos de Inspecci&oacute;n</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="<?php echo Url::to('/tipos-dacs/index') ?>">Tipos de Medios de Izado</a>
    <i class="fa fa-angle-right"></i>
</li>
<li class="active">
    Nuevo
</li>

<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>