<?php

use app\models\Entidades;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption">
                    <?php if ($model->isNewRecord) { ?>
                        <i class="fa fa-plus"></i> Nuevo Servicio

                    <?php } else { ?>
                        <i class="fa fa-edit"></i> Actualizar Servicio ( <?php echo $model->departamento ?> )
                    <?php } ?>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php
                $form = ActiveForm::begin([
                    'id' => 'servicios-laboratorios-acreditados-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                ]);
                ?>
                <div class="container">
                    <?php echo Html::errorSummary($model); ?>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'departamento', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'departamento', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'departamento', 'maxlength' => 150)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'departamento', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'entidad', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'entidad0', ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'entidad', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'provincia', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'provincia0', ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'), array('class' => 'form-control  select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'provincia', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'servicio', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextArea($model, 'servicio', array('class' => 'form-control')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'servicio', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a href="<?php echo Url::to('/servicios-laboratorios-acreditados/index') ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>