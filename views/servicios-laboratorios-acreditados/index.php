<?php

use app\assets\DataTablesAsset;
use app\assets\SBAdminAsset;
use app\models\Entidades;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\bootstrap4\LinkPager;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Servicios
<small>Gesti&oacute;n de la informaci&oacute;n de Personas Acreditadas</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#" class="breadcrumb-item">
    <i class="fa fa-user"></i>
    Objetos de Inspecci&oacute;n</a>
<span class="breadcrumb-item active">Servicios</span>
<?php $this->endBlock(); ?>


<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fa fa-search"></i> B&uacute;squeda avanzada
    </div>
    <div class="card-body">
        <?php
        $form = ActiveForm::begin([
            'options' => ['class' => 'form-inline'],
        ]) ?>

        <div class="form-group mb-2">
            <?php echo $form->field($model, 'servicio', [
                'inputOptions' => ['class' => 'form-control mr-2']
            ])->textInput()->input('servicio', ['placeholder' => "Servicio"])->label(false); ?>
        </div>
        <div class="form-group mb-2">
            <?= $form->field($model, 'departamento', [
                'inputOptions' => ['class' => 'form-control mr-2']
            ])->textInput()->input('departamento', ['placeholder' => "departamento"])->label(false); ?>
        </div>
        <div class="form-group mb-2">
            <?php echo $form->field($model, 'provincia', [
                'inputOptions' => ['class' => 'form-control mr-2']
            ])->dropdownList(
                ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'),
                ['style' => 'width:200px !important', 'prompt' => 'Seleccione una Provincia']
            )->label(false); ?>
        </div>
        <div class="form-group mb-2">
            <?php echo $form->field($model, 'entidad', [
                'inputOptions' => ['class' => 'form-control mr-2']
            ])->dropdownList(
                ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'),
                ['style' => 'width:200px !important', 'prompt' => 'Seleccione una Entidad']
            )->label(false); ?>
        </div>
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary mb-2']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php } ?>
<br />
<!-- BEGIN EXAMPLE TABLE card-->
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-globe"></i>Gesti&oacute;n de Servicios
    </div>
    <div class="float-right">
        Mostrando <?= count($data) ?> de <?= $pages->pageCount ?> elementos
    </div>
    <div class="card-body">
        <div class="table-toolbar">
            <div class="btn-group" style="padding-top: 8px">
                <a href="<?php echo Url::to('/servicios-laboratorios-acreditados/create') ?>" class="btn btn-success">Nuevo <i class="fa fa-plus"></i></a>
                <a id="eliminar" href="#" class="btn btn-danger">Eliminar seleccionados <i class="fa fa-trash"></i></a>
            </div>
            <div class="btn-group float-right">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php echo LinkPager::widget(['pagination' => $pages]); ?>
                </div>
            </div>
        </div>
        <table id="main_table" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="table-checkbox">
                        <input id="select_all" type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                    </th>
                    <th>Servicio</th>
                    <th>Entidad</th>
                    <th>Provincia</th>
                    <th style="width: 20%"> &nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $value) {
                ?>
                    <tr class="odd gradeX">
                        <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id ?>" /></td>
                        <td>
                            <button class="btn popovers" data-trigger="hover" data-container="body" data-placement="bottom" data-content="Servicio: <?= $value->servicio->nombre ?? '' ?>&nbsp;&verbar;&nbsp;Departamento: <?= $value->departamento ?>" data-original-title="<?= $value->entidad0->nombre ?? '' ?>">
                                <medium><?php echo substr($value->servicio, 0, 80) . '...' ?></medium>
                            </button>
                        </td>
                        <td><?php echo $value->entidad0->nombre ?? '' ?></td>
                        <td><?php echo $value->provincia0->nombre ?? '' ?></td>
                        <td>
                            <a href="<?php echo Url::to(['/servicios-laboratorios-acreditados/update', 'id' => $value->id]) ?>" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Editar</a>
                            &nbsp;
                            <a href="<?php echo Url::to(['/servicios-laboratorios-acreditados/historical', 'id' => $value->id]) ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Hist&oacute;rico</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <hr />
        <div class="row">
            <div class="col-md-4 float-left">
                Mostrando <?= count($data) ?> de <?= $pages->pageCount ?> elementos
            </div>
            <div class="col-md-8">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php echo LinkPager::widget(['pagination' => $pages]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );

        $('#eliminar').click(function(values){
        var ids = new Array();
        var ck = $('#main_table_servicios').find('input[type="checkbox"]:checked');
        if(ck.length > 0 && confirm("Se dispone a eliminar "+ck.length+" registros")){
            for(i=0; i<ck.length; i++){
                ids.push(ck[i].value);
            };
            $.ajax({
                type: "POST",
                url: "<?php echo CHtml::normalizeUrl(array('batchDelete'));?>",
                data: {"ids":ids},
                    success: function(data){
                        window.location.reload(true);
                    },
            });
            }
        });

        $('#eliminar').addClass('disabled');
        $("input[type='checkbox']").change(function() {
            if($("input[type='checkbox']").is(":checked")){
                $('#eliminar').removeClass('disabled');
            }else{
                $('#eliminar').addClass('disabled');
            }
        });

        $('#select_all').change(function(event) {
            if(this.checked) {
                $('.checkboxes').each(function() {
                    this.checked == false;
                    this.click();
                });
            }else {
                $('.checkboxes').each(function() {
                    this.checked = true;
                    this.click();
                });
            }
        });
    } );
JS;
$this->registerJs($js); ?>