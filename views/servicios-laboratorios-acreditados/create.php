<?php

use yii\helpers\Url;

?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Servicios <small> Gesti&oacute;n de la informaci&oacute;n de Servicios</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="fa fa-user"></i>
    <a href="#">Objetos de Inspecci&oacute;n</a>
    <a href="<?php echo Url::to('/servicios-laboratorios-acreditados/index') ?>">Servicios </a>
    <i class="fa fa-angle-right"></i>
</li>
<li class="active">
    Nuevo
</li>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>