<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Estados Inspecciones <small>Gesti&oacute;n de la informaci&oacute;n de Estados Inspecciones </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#"><i class="fa fa-cogs"></i>Secci&oacute;n -modificar- </a>
<li>
    <a href="<?php echo Url::to('/EstadosInspecciones/index') ?>">Estados Inspecciones </a>
    <a href="#">Actualizar </a>
</li>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model));
?>