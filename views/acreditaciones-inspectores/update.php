<?php

use yii\helpers\Url;

?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Acreditaciones de Inspectores <small> Gesti&oacute;n de la informaci&oacute;n de Acreditaciones de Inspectores</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#" class="breadcrumb-item"><i class="fa fa-cogs"></i> Estructura y Servicios</a>
<a href="<?php echo Url::to('/funcionarios/index') ?>" class="breadcrumb-item">
    <i class="fa fa-cogs"></i> Personal RCB</a>
<a href="<?php echo Url::to(['/acreditaciones-inspectores/index', 'idInspector' => $inspector->id]) ?>" class="breadcrumb-item">
    <i class="fa fa-user"></i> Acreditaciones del Inspector: <strong><?php echo $inspector->nombre_apellidos ?> </strong> </a></a>
<span class="breadcrumb-item active">Actualizar Acreditaci&oacute;n</span>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'inspector' => $inspector));
?>