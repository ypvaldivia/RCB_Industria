<?php

use app\assets\DataTablesAsset;
use app\models\CategoriasAcreditaciones as ModelsCategoriasAcreditaciones;
use yii\helpers\Url;
use app\models\CategoriasAcreditaciones;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Acreditaciones de Inspectores <small> Gesti&oacute;n de la informaci&oacute;n de Acreditaciones de Inspectores</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#" class="breadcrumb-item"><i class="fa fa-cogs"></i> Estructura y Servicios</a>
<a href="<?php echo Url::to('/funcionarios/index') ?>" class="breadcrumb-item">
    <i class="fa fa-cogs"></i> Personal RCB</a>
<span class="breadcrumb-item active">Acreditaciones del Inspector: <strong><?php echo $inspector->nombre_apellidos ?> </strong></span>
<?php $this->endBlock(); ?>


<li class="float-right">
    <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
        <i class="icon-calendar"></i>

        setlocale(LC_ALL,'es_ES');
        $date = DateTime::createFromFormat('d/m/Y', date('d/m/Y'));
        echo strftime('%A, %d de %B de %G',$date->getTimestamp()); </div>
</li>
<?php $this->endBlock(); ?>
<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php } ?>
<br />
<div class="alert alert-info alert-dismissable">
    <h4> <i class="icon-info-sign"></i> Use la caja de abajo para adicionar una nueva acreditacion</h4>
</div>
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-globe"></i>Gesti&oacute;n de Acreditaciones del Inspector: <strong><?php echo $inspector->nombre_apellidos ?> </strong></h3>
    </div>
    <div class="card-body">
        <div class="table-toolbar">
            <div class="btn-group">
                <a id="eliminar" href="#" class="btn btn-danger">Eliminar seleccionados <i class="fa fa-trash"></i></a>

            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="main_table">
            <thead>
                <tr>
                    <th class="table-checkbox">
                        <input id="select_all" type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                    </th>
                    <th>Estado</th>
                    <th>Acreditacion</th>
                    <th>Emision</th>
                    <th>Vencimiento</th>

                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $value) {
                ?>
                    <tr class="odd gradeX">
                        <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id ?>" /></td>
                        <td><span class="label label-<?php echo $value->getEstado() ?>"></span></td>
                        <td><?php echo $value->acreditacion0->grupo ?? '' ?></td>
                        <td><?php echo $value->emision ?></td>
                        <td><?php echo $value->vencimiento ?></td>

                        <td>
                            <a href="<?php echo Url::to('/acreditacionesInspectores/update', array('id' => $value->id)) ?>" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Editar</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<hr />
<div class="card box red">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-plus-sign-alt"></i>Adicionar nueva acreditaci&oacute;n para <strong><?php echo $inspector->nombre_apellidos ?> </strong></h3>
    </div>
    <div class="card-body">

        <?php
        $form = ActiveForm::begin([
            'id' => 'acreditaciones-inspectores-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => true,
        ]);
        ?> <div class="container">
            <br />
            <?php echo Html::errorSummary($model, 'Por favor corrija los siguientes errores de ingreso:', null,  array('class' => 'alert alert-danger')); ?>
        </div>
        <div class="form-body">
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'acreditacion', array('class' => 'control-label')); ?>
                        <?php echo Html::activeDropDownList($model, 'acreditacion0', ArrayHelper::map(ModelsCategoriasAcreditaciones::getAcreditacionesFaltantes($inspector->id), 'id', 'grupo'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                        <span class="help-block"><?php echo Html::error($model, 'acreditacion', array('class' => 'text-danger')); ?></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'emision', array('class' => 'control-label')); ?>
                        <?php echo Html::activeTextInput($model, 'emision', array('class' => 'form-control form-control input-medium date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                        <span class="help-block"><?php echo Html::error($model, 'emision', array('class' => 'text-danger')); ?></span>
                    </div>
                </div>
            </div>
            <input type="hidden" value="<?php echo $inspector->id ?>" name="idinspector" id="idinspector" />

        </div>

        <div class="form-actions fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                        <a href="<?php echo Url::to(['/acreditaciones-inspectores/index', 'idInspector' => $inspector->id]) ?>" class="btn btn-default">Cancelar</a>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>

<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
    } );
JS;
$this->registerJs($js); ?>