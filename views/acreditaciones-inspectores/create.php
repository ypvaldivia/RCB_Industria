<?php

use yii\helpers\Url;

?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Acreditaciones Inspectores<small>Gesti&oacute;n de la informaci&oacute;n de Acreditaciones Inspectores</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#" class="breadcrumb-item"><i class="fa fa-cogs"></i> Estructura y Servicios</a>
<a href="<?php echo Url::to('/acreditaciones-inspectores/index') ?>" class="breadcrumb-item">
    <i class="fa fa-cogs"></i> Acreditaciones Inspectores</a>
<span class="breadcrumb-item active">Nuevo</span>
<?php $this->endBlock(); ?>

<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>