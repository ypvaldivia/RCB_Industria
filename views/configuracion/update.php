<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Configuracions <small>Gesti&oacute;n de la informaci&oacute;n de Configuracions </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="fa fa-cogs"></i>
    <a href="#">Configuraci&oacute;n General </a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Servidor de Correo </a>
</li>

</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
</div>
</div>
<?php if (Yii::$app->session->hasFlash('success')) { ?> <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong>
        <?php echo Yii::$app->session->getFlash('success'); ?> </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?> <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong>
        <?php echo Yii::$app->session->getFlash('error'); ?> </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?> <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong>
        <?php echo Yii::$app->session->getFlash('warning'); ?> </div>
<?php } ?>
<!-- END PAGE HEADER-->
<?php
echo $this->render('_form', array('model' => $model));
?>