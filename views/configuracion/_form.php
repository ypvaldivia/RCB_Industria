<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Configuracion</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php
                $form = ActiveForm::begin([
                    'id' => 'configuracion-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                ]);
                ?> <div class="container">
                    <?php echo Html::errorSummary($model);
                    ?>
                </div>
                <div class="form-body">

                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'protocolo', array('class' => 'control-label')); ?>
                        <?php echo Html::activeTextInput($model, 'protocolo', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'protocolo', 'maxlength' => 10)); ?>
                        <span class="help-block"><?php echo Html::error($model, 'protocolo', array('class' => 'text-danger')); ?>
                        </span>
                    </div>


                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'servidor', array('class' => 'control-label')); ?>
                        <?php echo Html::activeTextInput($model, 'servidor', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'servidor', 'maxlength' => 45)); ?>
                        <span class="help-block"><?php echo Html::error($model, 'servidor', array('class' => 'text-danger')); ?>
                        </span>
                    </div>


                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'puerto', array('class' => 'control-label')); ?>
                        <?php echo Html::activeTextInput($model, 'puerto', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'puerto', 'maxlength' => 3)); ?>
                        <span class="help-block"><?php echo Html::error($model, 'puerto', array('class' => 'text-danger')); ?>
                        </span>
                    </div>


                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'smtp_ssl', array('class' => 'control-label')); ?>
                        <?php echo Html::activeCheckbox($model, 'smtp_ssl'); ?>
                        <span class="help-block"><?php echo Html::error($model, 'smtp_ssl', array('class' => 'text-danger')); ?>
                        </span>
                    </div>


                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'smtp_auth', array('class' => 'control-label')); ?>
                        <?php echo Html::activeCheckbox($model, 'smtp_auth'); ?>
                        <span class="help-block"><?php echo Html::error($model, 'smtp_auth', array('class' => 'text-danger')); ?>
                        </span>
                    </div>


                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'usuario', array('class' => 'control-label')); ?>
                        <?php echo Html::activeTextInput($model, 'usuario', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'usuario', 'maxlength' => 45)); ?>
                        <span class="help-block"><?php echo Html::error($model, 'usuario', array('class' => 'text-danger')); ?>
                        </span>
                    </div>


                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'password', array('class' => 'control-label')); ?>
                        <?php echo Html::activePasswordInput($model, 'password', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'password', 'maxlength' => 45)); ?>
                        <span class="help-block"><?php echo Html::error($model, 'password', array('class' => 'text-danger')); ?>
                        </span>
                    </div>


                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'nombre_saliente', array('class' => 'control-label')); ?>
                        <?php echo Html::activeTextInput($model, 'nombre_saliente', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'nombre_saliente', 'maxlength' => 150)); ?>
                        <span class="help-block"><?php echo Html::error($model, 'nombre_saliente', array('class' => 'text-danger')); ?>
                        </span>
                    </div>


                    <div class="form-group">
                        <?php echo Html::activeLabel($model, 'correo_saliente', array('class' => 'control-label')); ?>
                        <?php echo Html::activeTextInput($model, 'correo_saliente', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'correo_saliente', 'maxlength' => 85)); ?>
                        <span class="help-block"><?php echo Html::error($model, 'correo_saliente', array('class' => 'text-danger')); ?>
                        </span>
                    </div>

                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a href="<?php echo Url::to('/configuracion/index') ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>


<?php


$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));





$js = <<<JS
 FormComponents.init();
JS;
$this->registerJs($js);
?>