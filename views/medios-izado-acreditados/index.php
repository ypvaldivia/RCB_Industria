<?php

use app\assets\DataTablesAsset;
use app\assets\SBAdminAsset;
use app\models\Entidades;
use app\models\Provincias;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\LinkPager;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
MIT Acreditados
<small>Gesti&oacute;n de la informaci&oacute;n de Medios de Izado Terrestres</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#" class="breadcrumb-item"><i class="fa fa-user"></i> Objetos de Inspecci&oacute;n</a>
<span class="breadcrumb-item active">Medios de Izado Terrestres</span>
<?php $this->endBlock(); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php } ?>

<div class="card border-left-primary shadow">
    <a href="#collapseBusqueda" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseBusqueda">
        <h6 class="m-0 font-weight-bold text-primary">
            <i class="fa fa-search"></i> B&uacute;squeda avanzada
        </h6>
    </a>

    <div class="card-body collapse show my-3" id="collapseBusqueda">
        <?php $form = ActiveForm::begin(['options' => ['class' => 'form-inline'],]) ?>
        <div class="form-group mb-2">
            <?= $form->field($model, 'nombre', [
                'inputOptions' => ['class' => 'form-control mr-2']
            ])->textInput()->input('nombre', ['placeholder' => "Nombre"])->label(false); ?>
        </div>

        <div class="form-group mb-2">
            <?php echo $form->field($model, 'provincia', [
                'inputOptions' => ['class' => 'form-control mr-2']
            ])->dropdownList(
                ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'),
                ['style' => 'width:200px !important', 'prompt' => 'Seleccione una Provincia']

            )->label(false); ?>
        </div>

        <div class="form-group mb-2">
            <?php echo $form->field($model, 'propietario', [
                'inputOptions' => ['class' => 'form-control mr-2']
            ])->dropdownList(
                ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'),
                ['style' => 'width:200px !important', 'prompt' => 'Seleccione una Propietario']
            )->label(false); ?>
        </div>


        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary mb-2']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>

<br />

<br />
<div class="card border-left-primary shadow">
    <a href="#collapseTabla" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseTabla">
        <h6 class="m-0 font-weight-bold text-primary">
            <i class="fa fa-globe"></i> Gesti&oacute;n de Productos y Medios de Izado Terrestres
        </h6>
    </a>

    <div class="card-body collapse show" id="collapseTabla">
        <div class="container">
            <div class="btn-group" style="padding-top: 8px">
                <a href="<?php echo Url::to('/medios-izado-acreditados/create') ?>" class="btn btn-success">Nuevo <i class="fa fa-plus"></i></a>
                <a id="eliminar" href="#" class="btn btn-danger">Eliminar seleccionados <i class="fa fa-trash"></i></a>
            </div>
            <div class="btn-group float-right">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php echo LinkPager::widget(['pagination' => $pages,]); ?>
                </div>
            </div>
        </div>
        <br />
        <table id="main_table" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th class="table-checkbox">
                        <input id="select_all" type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                    </th>
                    <th>Nombre</th>
                    <th>Propietario</th>
                    <th>Gen&eacute;rico</th>
                    <th style="width: 25%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $value) {
                ?>
                    <tr class="odd gradeX">
                        <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id ?>" /></td>
                        <td><?php echo $value->nombre ?></td>
                        <td><?php echo $value->propietario0->nombre ?? '' ?></td>
                        <td><?php echo $value->tipo0->nombre ?? '' ?></td>
                        <td>
                            <a href="<?php echo Url::to(['/medios-izado-acreditados/update', 'id' => $value->id]) ?>" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Editar</a>
                            <a href="<?php echo Url::to(['/galeria-medios-izado/create', 'id' => $value->id]) ?>" class="btn btn-primary btn-sm"><i class="fa fa-camera"></i> Fotos</a>
                            &nbsp;
                            <a href="<?php echo Url::to(['/medios-izado-acreditados/historical', 'id' => $value->id]) ?>" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Hist&oacute;rico</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <hr />
        <div class="row">
            <div class="col-md-4 float-left">
                Mostrando <?= count($data) ?> de <?= $pages->pageCount ?> elementos
            </div>
            <div class="col-md-8">
                <div class="dataTables_paginate paging_bootstrap float-right">
                    <?php echo LinkPager::widget(['pagination' => $pages,]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
    } );
JS;
$this->registerJs($js); ?>