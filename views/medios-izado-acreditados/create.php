<?php

use yii\helpers\Url;

?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
MIT Acreditados <small>Gesti&oacute;n de la informaci&oacute;n de Medios de Izado Terrestres</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#"><i class="fa fa-cogs"></i>Objetos de Inspecci&oacute;n</a>
<li>
    <a href="<?php echo Url::to('/MediosIzadoAcreditados/index') ?>">Medios de Izado Terrestres </a>
    <i class="fa fa-angle-right"></i>
</li>
<li class="active">
    Nuevo
</li>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>