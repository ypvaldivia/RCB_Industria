<?php

use app\models\Entidades;
use app\models\Provincias;
use app\models\TiposDacs;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Dacs Acreditados</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php
                $form = ActiveForm::begin([
                    'id' => 'dacs-acreditados-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                ]);
                ?> <div class="container">
                    <?php echo Html::errorSummary($model); ?>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'nombre', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'nombre', array('class' => 'form-control', 'placeholder' => 'descripcion', 'maxlength' => 250)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'nombre', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="control-label" for="DacsAcreditados[tipo0]">Gen&eacute;rico</label>
                                <?php echo Html::activeDropDownList($model, 'tipo0', ArrayHelper::map(TiposDacs::find()->all(), 'id', 'tipo'), array('class' => 'form-control input-xlarge select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'tipo0', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'propietario', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'propietario0', ArrayHelper::map(Entidades::find()->all(), 'id', 'nombre'), array('class' => 'form-control input-xlarge select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'propietario', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'provincia', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'provincia0', ArrayHelper::map(Provincias::find()->all(), 'id', 'nombre'), array('class' => 'form-control input-xlarge select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'provincia', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'ct_nominal', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'ct_nominal', array('id' => 'maxlength_alloptions', 'class' => 'form-control', 'placeholder' => 'ct_nominal')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'ct_nominal', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a href="<?php echo Url::to('/dacs-acreditados/index') ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>