<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
DACs Acreditados <small>Gesti&oacute;n de la informaci&oacute;n de Dispositivos Auxiliares de Carga </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="fa fa-cogs"></i>
    <a href="#">Objetos de Inspecci&oacute;n</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="<?php echo Url::to('/dacs-acreditados/index') ?>">Dispositivos Auxiliares de Carga </a>
    <i class="fa fa-angle-right"></i>
</li>
<li class="active">
    Nuevo
</li><?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>