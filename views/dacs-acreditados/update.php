<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
DACs Acreditados <small>Gesti&oacute;n de la informaci&oacute;n de Dispositivos Auxiliares de Carga </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a class="breadcumb-item" href="#"> / Secci&oacute;n modificar </a>
<a class="breadcumb-item" href="<?php echo Url::to('/DacsAcreditados/index') ?>"> / Dacs Acreditadoses </a>
<a class="breadcumb-item active" href="#"> / Actualizar </a>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model));
?>