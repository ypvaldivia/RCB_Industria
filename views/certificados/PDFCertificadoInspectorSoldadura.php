<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R3/PC401</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 100%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter; text-align: center">
                <strong style="font-size: large">
                    CERTIFICADO DE HOMOLOGACI&Oacute;N DE INSPECTOR DE SOLDADURA<br />
                    (APPROVAL CERTIFICATE OF WELDING INSPECTOR)
                </strong>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 85%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                    </span>
                </h5>
            </td>
        </tr>
    </table>
    <table style="border:none; padding-top: 0px; padding-bottom: 0px; font-size: 12px" cellpadding="0" cellspacing="5">
        <tr>
            <td style="width: 30%">
                Que se otorga al Sr.:<br />
                <small>That is granted to:</small>
            </td>
            <td style="width: 70%; border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic; font-size: 14px; font-weight: bold"><?php echo $serv->persona0->nombre_apellidos; ?></span>
            </td>
        </tr>
        <tr>
            <td>
                N&deg; de Identidad Permanente:<br />
                <small>(Identification number)</small>
            </td>
            <td style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic"><?php echo $serv->persona0->ci; ?></span>
            </td>
        </tr>
        <tr>
            <td>
                Empresa:<br />
                <small>(Enterprise)</small>
            </td>
            <td style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic"><?php echo $serv->inspeccion0->entidad0->nombre; ?></span>
            </td>
        </tr>
    </table>

    <p align="justify">
        Por <b>aprobar</b> de forma satisfactoria la
        <b>Evaluaci&oacute;n para la Homologaci&oacute;n de Inspectores de Soldadura de Nivel</b> <?= '<span style="text-decoration: underline">' . $model->nivel . '</span>'; ?>
        y cumplir con los requisitos del <b>RCB</b>
        especificados en el PC4-01
        "Procedimiento para la evaluaci&oacute;n de la conformidad de productos, servicios y personal calificado" y en el Cap&iacute;tulo 5.6 "Inspectores de soldadura" del
        "Manual de Aprobaci&oacute;n de Productos, Servicios y Personal Calificado y en la norma UNE 14618:2000 Inspectores
        de Soldadura. Cualificaci&oacute;n y certificaci&oacute;n".
    </p>
    <p align="justify">
        For having approved in a satisfactory way the <i>Evaluation for the Approval of Welding Inspectors</i> of level
        <?= '<span style="text-decoration: underline">' . $model->nivel . '</span>'; ?>
        and fulfill the requirements of the <b>RCB</b>
        specified in PC4-01 "Procedure for the conformity assessment for products, services and qualified personnel's"
        and in the Chapter 5.6 "Inspectors of welding" of the Manual of Approval of Products, Services and Qualified
        Personnel and in the Standard UNE 14618: 2000 Weld Inspector. Qualification and certification
    </p>


    <p align="justify">
        El presente Certificado autoriza al Inspector a realizar trabajos de inspecci&oacute;n a soldaduras con el siguiente alcance:<br />
        <i>The present Certificate authorizes the Welding Inspector to carry out inspection woks to welding with the following
            scope:</i>
        <br />
        <?= $model->alcance; ?>
    </p>

    <table style="border: none">
        <tr>
            <td>
                <table style="border:none">
                    <tr>
                        <td>
                            Fecha de emisi&oacute;n: <br />
                            <small>(Date of issue)</small>
                        </td>
                        <td><br /><i><?php echo $model->emision; ?></i></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="border:none">
                    <tr>
                        <td>
                            Per&iacute;odo de validez:<br />
                            <small>(Validity period)</small>
                        </td>
                        <td><br /><i><?php echo $model->vence; ?></i></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <br /><br /><br />

    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php
                    if ($model->nivelAprobacion != null) {
                        //Validar aqui que el funcionario este activo
                        if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                            echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                        } else {
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                        }
                    }
                    ?>
                </span>
            </td>
        </tr>
    </table>

</page>

<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R3/PC401</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border: 1px">
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CONDICIONES GENERALES<br />
                    <small>General Conditions</small>
                </h4>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter; justify-content: center">
                <ol>
                    <li style="text-align: justify; padding-bottom: 12px">
                        El presente Certificado solo avala Inspecciones de soldadura acordes a los conocimientos t&eacute;cnicos
                        seg&uacute;n el Nivel y el Alcance especificados<br />
                        <small>The present Certificate endorses only welding inspections to the technical knowledge according
                            to the Level and Scope specified.</small>
                    </li>
                    <li style="text-align: justify; padding-bottom: 12px">
                        La vigencia del presente Certificado (3 a&ntilde;os) depender&aacute; del estricto cumplimiento de los siguientes
                        aspectos:<br />
                        <small>The validity of this Certificate (3 years) depends upon the strict fulfillment of the below aspects:</small>
                        <ul>
                            <li style="text-align: justify; padding-bottom: 12px">
                                Del nivel cualitativo a demostrar por el inspector de soldadura en trabajos de inspecci&oacute;n de
                                soldadura habituales.<br />
                                <small>The quality level to be shown by the welding inspector periodical welding inspections.</small>
                            </li>
                            <li style="text-align: justify; padding-bottom: 12px">
                                Del ininterrumpido ejercicio de trabajos en la especialidad de soldadura (En caso contrario, no m&aacute;s
                                de 6 meses).<br />
                                <small>The continuos welding inspections works to be performed (otherwise, not later than 6 months).</small>
                            </li>
                        </ul>
                    </li>
                </ol>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CONFIRMACI&Oacute;N DE LAS INSPECCIONES ANUALES
                </h4>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr style="text-align: center; font-weight: lighter">
                        <td style="width: 20%; text-align: center">Primera Anual</td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Fecha
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Firma del Inspector
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />N&deg; de Control
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Cu&ntilde;o del Inspector
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr style="text-align: center; font-weight: lighter">
                        <td style="width: 20%; text-align: center">Segunda Anual</td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Fecha
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Firma del Inspector
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />N&deg; de Control
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Cu&ntilde;o del Inspector
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</page>