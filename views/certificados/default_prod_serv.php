<?php
$first = is_array($serv) ? $serv[0] : $serv;
?><?php

    use yii\bootstrap4\ActiveForm;
    use yii\helpers\Html;
    use yii\helpers\Url;
    ?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Inspecciones de Productos y Servicios <small> Gesti&oacute;n de Certificados</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="icon-barcode"></i>
    <a href="<?php echo Url::to('/inspecciones/index-productos') ?>">Inspecciones - </a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Productos y Servicios - </a><i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Certificados </a>
</li>
<li class="btn-group">
    <a href="<?php echo Url::to('/inspecciones/detallesProductos', array('id' => $first->inspeccion0->id)) ?>" class="btn btn-primary">
        <span style="color: #FFFFFF"><i class="icon-arrow-left"></i>Atras</span></a>
</li><?php $this->endBlock(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Certtificado de Inspecci&oacute;n</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">

                <div class="container">
                    <div class="form-body">
                        <!-- BEGIN FORM-->
                        <?php
                        $form = ActiveForm::begin([
                            'id' => 'certificadoPersonasProvisional-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'htmlOptions' => ['enctype' => 'multipart/form-data']
                        ]);
                        ?>

                        <center>
                            <img src="<?= Yii::getAlias('@web/uploads') ?>/images/bannerHeader.jpg" width="90%">
                        </center>

                        <div class="row">
                            <div class="col-sm-12">
                                <center>
                                    <h2>
                                        <span style="font-variant: all-small-caps">
                                            CERTIFICADO DE HOMOLOGACI&Oacute;N
                                        </span>
                                    </h2>
                                </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-10">&nbsp;</div>
                            <div class="col-sm-2 float-left">
                                <h5>
                                    N&deg;:
                                    <span style="text-decoration: underline">
                                        <?php echo $first->inspeccion0->numero_control; ?>
                                        <?php
                                        if (!is_array($serv)) {
                                            echo ' - ' . $first->numero;
                                        }
                                        ?>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <h5>
                                    <b>QUE SE OTORGA A:</b>
                                    <span style="text-decoration: underline">
                                        <?php
                                        if ($tipo == 2) {
                                            echo '<br/><br/>';
                                            if (is_array($serv)) {
                                                echo '<br/><center><b>' . $first->servcios0->entidad0->nombre . '</b><br/>' . $first->servcios0->entidad0->direccion . '</center>';
                                            } else {
                                                if ($first->servcios0->departamento != null) {
                                                    echo $first->servcios0->entidad0->nombre . ' - ' .
                                                        $first->servcios0->departamento . ' ' .
                                                        $first->servcios0->servicio;
                                                } else {
                                                    echo $first->servcios0->entidad0->nombre . ' - ' . $first->servcios0->servicio;
                                                }
                                            }
                                        } elseif ($tipo == 3) {
                                            //es un producto
                                            if (is_array($serv)) {
                                                echo '<br/>';
                                                echo '<center>';
                                                echo '<b>' . $first->producto0->propietario0->nombre . '</b>';
                                                echo '<br/>';
                                                echo $first->producto0->propietario0->direccion;
                                                echo '</center>';
                                            } else {
                                                echo $first->producto0->nombre;
                                            }
                                        }
                                        ?>
                                    </span>

                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    Por obtener resultados satisfactorios en las inspecciones realizadas en el proceso de
                                    homologación y cumplir con los requisitos del RCB Sociedad Clasificadora especificados en
                                    <?php
                                    if (isset($model->servicio) && $model->servicio != null) {
                                        echo $model->servicio;
                                    } else {
                                        echo Html::activeTextArea($model, 'servicio', array('class' => 'form-control'));
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>

                        <br />

                        <div class="row">
                            <div class="col-sm-12">
                                <?php if (is_array($serv)) {
                                ?>
                                    <p align="justify">
                                        Se le otorga el presente Certificado de <?= $first->inspeccion0->homologacion0->titulo_anuario ?> a:
                                    </p>
                                    <table width="100%" border="1">
                                        <tr>
                                            <td><b>Nombre comercial</b></td>
                                            <td><b>Descripci&oacute;n</b></td>
                                        </tr>
                                        <?php foreach ($serv as $s) {
                                            if ($s->aprobado == 1) {
                                        ?>
                                                <tr>
                                                    <td><?= $s->producto0->nombre ?></td>
                                                    <td><?= $s->producto0->descripcion ?></td>
                                                </tr>
                                        <?php }
                                        } ?>
                                    </table>
                                <?php
                                } else {
                                ?>
                                    <p align="justify">
                                        Se le otorga el presente Certificado para:
                                    </p>
                                <?php
                                    if (isset($model->documentacion) && $model->documentacion != null) {
                                        echo '<center><h2>';
                                        echo $model->documentacion;
                                        echo '</h2></center>';
                                    } else {
                                        echo Html::activeTextArea($model, 'documentacion', array('class' => 'ckeditor form-control'));
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <br />


                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    El presente Certificado se expide en base al Informe de Inspección N&deg;: &nbsp;
                                    <span style="text-decoration: underline; font-weight: bold">
                                        <?= $first->inspeccion0->numero_control; ?>
                                    </span>
                                    &nbsp; bajo las condiciones establecidas al dorso.
                                </p>
                            </div>
                        </div>

                        <br />

                        <?php if (isset($model->expedicion) && $model->expedicion != null) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                        <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                                    </h5>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-2">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                    </h5>
                                </div>
                                <div class="col-sm-10">
                                    <?php echo Html::activeTextInput($model, 'expedicion', array('class' => 'form-control form-control input-medium date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                                </div>
                            </div>
                        <?php } ?>

                        <br />

                        <?php if (isset($model->vencimiento) && $model->vencimiento != null) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>
                                        Fecha Vencimiento:
                                        <span style="text-decoration: underline"><?php echo $model->vencimiento; ?></span>
                                    </h5>
                                </div>
                            </div>
                        <?php } ?>

                        <br /><br />

                        <div class="row">
                            <div class="col-sm-8">&nbsp;</div>
                            <div class="col-sm-4">
                                <center>
                                    <hr />
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">&nbsp;</div>
                            <div class="col-sm-4">
                                <center>
                                    <?php
                                    if ($model->nivelAprobacion != null) {
                                        //Validar aqui que el funcionario este activo
                                        if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                                            echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                                            echo '<br/>';
                                            echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                                        } else {
                                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                                            echo '<br/>';
                                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                                        }
                                    }
                                    ?>
                                </center>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-1">R2/PC401</div>
                            <div class="col-sm-11">&nbsp; </div>
                        </div>


                    </div>


                    <hr />
                    <center>
                        <h3>CONDICIONES GENERALES</h3>
                    </center>
                    <div class="row">
                        <div class="col-md-12">
                            <ol>
                                <li>
                                    <p align="justify">
                                        La vigencia del Certificado expira en la fecha de vencimiento establecida en el presente
                                        documento siempre que se someta a inspecciones de seguimiento por el RCB con resultados
                                        satisfactorios sobre la base de la verificaci&oacute;n del cumplimiento de los requisitos y condiciones
                                        en los cuales fueron homologados.</p>
                                </li>
                                <li>
                                    <p align="justify">
                                        Las inspecciones de seguimiento anuales se realizar&aacute;n a solicitud del Cliente antes de la fecha
                                        de aniversario de la expedici&oacute;n del Certificado de Homologaci&oacute;n para la confirmaci&oacute;n de su
                                        validez.</p>
                                </li>
                                <li>
                                    <p align="justify">
                                        Las entidades con productos y servicios homologados informar&aacute;n al <b>RCB</b> sobre cualquier cambio o
                                        modificaci&oacute;n que afecte al objeto de homologaci&oacute;n (cambios tecnol&oacute;gicos, especificaciones,
                                        personal, instalaciones, m&eacute;todos de ensayos y calibraci&oacute;n, equipamiento y otros) en un plazo no
                                        mayor de 7 d&iacute;as despu&eacute;s de efectuados dichos cambios o modificaciones.</p>
                                </li>
                                <li>
                                    <p align="justify">
                                        El Certificado pierde su validez si se incumple los requisitos bajo las cuales fue otorgado.</p>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <center>
                        <h3>CONFIRMACI&Oacute;N DE LAS INSPECCIONES ANUALES</h3>
                    </center>
                    <br /><br />
                    <div class="row">
                        <div class="col-md-2">
                            <b>Primera Anual:</b>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>Fecha</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Firma del Inspector</center>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>N&deg; de Control</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Cu&ntilde;o del Inspector</center>
                        </div>
                    </div>
                    <br /><br /><br />
                    <div class="row">
                        <div class="col-md-2">
                            <b>Segunda Anual:</b>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>Fecha</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Firma del Inspector</center>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>N&deg; de Control</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Cu&ntilde;o del Inspector</center>
                        </div>
                    </div>
                </div>
                <?php
                $objs = array();
                $r = null;
                if (is_array($serv)) {
                    foreach ($serv as $m) {
                        array_push($objs, $m->id);
                    }
                    $r = serialize($objs);
                } else {
                    $r = $serv->id;
                }
                ?>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-2">
                                <?php if ($model->control == 0) { ?>
                                    <button id="inspeccion_servicios_submit" type="submit" class="btn btn-primary"><i class="icon-ok"></i>Vista Previa del Certificado</button>
                                <?php } elseif ($model->control == 1) { ?>
                                    <a href="<?php echo Url::to('/certificados/PDFCertificadoProdServDefault', array('o' => $r, 'tipo' => $tipo)) ?>" class="btn btn-success"><i class="fa fa-file"></i> Generar Certificado</a>
                                    |
                                    <a href="<?php echo Url::to('/certificados/certificado', array('obj' => $r, 'tipoObj' => $tipo, 'tipoCe' => 2)) ?>" class="btn btn-danger"><i class="icon-repeat"></i> Corregir Certificado</a>
                                <?php } ?>
                                |
                                <?php if ($tipo == 2) { ?>
                                    <a href="<?php echo Url::to('/inspecciones/detalles-lab-serv', array('id' => $first->inspeccion0->id)) ?>" class="btn btn-default">Cancelar</a>
                                <?php } else { ?>
                                    <a href="<?php echo Url::to('/inspecciones/detallesProductos', array('id' => $first->inspeccion0->id)) ?>" class="btn btn-default">Cancelar</a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
</div>

<?php
$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));

?>

<?php
$this->registerJsFile('@web/plugins/jquery-validation/dist/jquery.validate.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-validation/dist/additional-methods.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));



$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/form-validation-entidades.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-print-page/jquery.printPage.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
FormComponents.certificado(); Custom.InitPrint(); 
JS;
$this->registerJs($js);
?>