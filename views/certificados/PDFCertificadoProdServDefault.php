<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<?php
$first = is_array($serv) ? $serv[0] : $serv;
?>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R2/PC401</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 99%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h3 style="text-align: center;">
                    CERTIFICADO DE HOMOLOGACI&Oacute;N
                </h3>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 85%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $first->inspeccion0->numero_control; ?>
                        <?php
                        if (!is_array($serv)) {
                            echo ' - ' . $first->numero;
                        }
                        ?>
                    </span>
                </h5>
            </td>
        </tr>
        <tr>
            <td style="width: 70%">
                <h5>
                    <b>QUE SE OTORGA A:</b>
                    <span style="text-decoration: underline">
                        <?php
                        if ($tipo == 2) {
                            echo '<br/><br/>';
                            if (is_array($serv)) {
                                echo '<br/><center><b>' . $first->servcios0->entidad0->nombre . '</b><br/>' . $first->servcios0->entidad0->direccion . '</center>';
                            } else {
                                if ($first->servcios0->departamento != null) {
                                    echo $first->servcios0->entidad0->nombre . ' - ' .
                                        $first->servcios0->departamento . ' ' .
                                        $first->servcios0->servicio;
                                } else {
                                    echo $first->servcios0->entidad0->nombre . ' - ' . $first->servcios0->servicio;
                                }
                            }
                        } elseif ($tipo == 3) {
                            //es un producto
                            if (is_array($serv)) {
                                echo '<h4 style="text-align: center;">';
                                echo $first->producto0->propietario0->nombre;
                                echo '<br/>';
                                echo $first->producto0->propietario0->direccion;
                                echo '</h4>';
                            } else {
                                echo $first->producto0->nombre;
                            }
                        }
                        ?>
                    </span>
                </h5>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>

    <p align="justify">
        Por obtener resultados satisfactorios en las inspecciones realizadas en el proceso de
        homologación y cumplir con los requisitos del RCB Sociedad Clasificadora especificados en
        <?= $model->servicio; ?>
    </p>

    <?php if (is_array($serv)) {
    ?>
        <p align="justify">
            Se le otorga el presente Certificado de <?= $first->inspeccion0->homologacion0->titulo_anuario ?> a:
        </p>
        <table width="100%" border="1">
            <tr>
                <td><b>Nombre comercial</b></td>
                <td><b>Descripci&oacute;n</b></td>
            </tr>
            <?php foreach ($serv as $s) { ?>
                <tr>
                    <td><?= $s->producto0->nombre ?></td>
                    <td><?= $s->producto0->descripcion ?></td>
                </tr>
            <?php } ?>
        </table>
    <?php
    } else {
    ?>
        <p align="justify">
            Se le otorga el presente Certificado para:
        </p>
        <?= $model->documentacion; ?>
    <?php } ?>

    <div class="row">
        <div class="col-sm-12">
            <p align="justify">
                El presente Certificado se expide en base al Informe de Inspección N&deg;:
                <span style="text-decoration: underline; font-weight: bold">
                    <?= $first->inspeccion0->numero_control; ?>
                </span>
                bajo las condiciones establecidas al dorso.
            </p>
        </div>
    </div>

    <br />
    <div class="row">
        <div class="col-sm-12">
            <h5>
                Fecha Expedici&oacute;n:
                <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                <br /><br />
                Fecha Vencimiento:
                <span style="text-decoration: underline"><?php echo $model->vencimiento; ?></span>
            </h5>
        </div>
    </div>

    <br />

    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php
                    if ($model->nivelAprobacion != null) {
                        //Validar aqui que el funcionario este activo
                        if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                            echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                        } else {
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                        }
                    }
                    ?>
                </span>
            </td>
        </tr>
    </table>

</page>

<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R2/PC401</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <br /><br />
    <table style="border: 1px">
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CONDICIONES GENERALES
                </h4>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter; justify-content: center">
                <ol>
                    <li style="text-align: justify; padding-bottom: 12px">
                        La vigencia del Certificado expira en la fecha de vencimiento establecida en el presente
                        documento siempre que se someta a inspecciones de seguimiento por el RCB con resultados
                        satisfactorios sobre la base de la verificaci&oacute;n del cumplimiento de los requisitos y condiciones
                        en los cuales fueron homologados.
                    </li>
                    <li style="text-align: justify; padding-bottom: 12px">
                        Las inspecciones de seguimiento anuales se realizar&aacute;n a solicitud del Cliente antes de la fecha
                        de aniversario de la expedici&oacute;n del Certificado de Homologaci&oacute;n para la confirmaci&oacute;n de su
                        validez.
                    </li>
                    <li style="text-align: justify; padding-bottom: 12px">
                        Las entidades con productos y servicios homologados informar&aacute;n al <b>RCB</b> sobre cualquier cambio o
                        modificaci&oacute;n que afecte al objeto de homologaci&oacute;n (cambios tecnol&oacute;gicos, especificaciones,
                        personal, instalaciones, m&eacute;todos de ensayos y calibraci&oacute;n, equipamiento y otros) en un plazo no
                        mayor de 7 d&iacute;as despu&eacute;s de efectuados dichos cambios o modificaciones.
                    </li>
                    <li style="text-align: justify;">
                        El Certificado pierde su validez si se incumple los requisitos bajo las cuales fue otorgado.
                    </li>
                </ol>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CONFIRMACI&Oacute;N DE LAS INSPECCIONES ANUALES
                </h4>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr style="text-align: center; font-weight: lighter">
                        <td style="width: 20%; text-align: center">Primera Anual</td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Fecha
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Firma del Inspector
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />N&deg; de Control
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Cu&ntilde;o del Inspector
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr style="text-align: center; font-weight: lighter">
                        <td style="width: 20%; text-align: center">Segunda Anual</td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Fecha
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Firma del Inspector
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />N&deg; de Control
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Cu&ntilde;o del Inspector
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</page>