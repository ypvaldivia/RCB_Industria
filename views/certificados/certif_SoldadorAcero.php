<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Inspecciones de Personas <small> Gesti&oacute;n de la informaci&oacute;n de Certificados</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="icon-barcode"></i>
    <a href="<?php echo Url::to('/inspecciones/index-personas') ?>">Inspecciones - </a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Personas - </a><i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Certificados </a>
</li>
<li class="btn-group">
    <a href="<?php echo Url::to('/inspecciones/detalles-personas', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-primary">
        <span style="color: #FFFFFF"><i class="icon-arrow-left"></i>Atras</span></a>
</li><?php $this->endBlock(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Certificado de Inspecci&oacute;n</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php
                $form = ActiveForm::begin([
                    'id' => 'certificadoBuzoSoldador-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                ]);
                ?>
                <div class="container">
                    <?php echo Html::errorSummary($model);
                    ?>

                </div>

                <img src="<?= Yii::getAlias('@web/uploads'); ?>/images/bannerHeader.jpg" width="100%">
                <center>
                    <strong style="font-size: large">
                        CERTIFICADO DE HOMOLOGACIÓN DEL SOLDADOR EN ACERO<br />
                        (STEEL WELDER'S APPROVAL CERTIFICATE)
                    </strong>
                </center>

                <div class="form-body">

                    <div class="row">
                        <div class="col-sm-9">&nbsp;</div>
                        <div class="col-sm-3 float-left">
                            <div class="form-group">
                                <h5>
                                    N&deg;:
                                    <span style="text-decoration: underline">
                                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                                    </span>
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <h5 style="text-align: right">
                                Nombre del soldador:<br />
                                <small>(Welder´s Name)</small>
                            </h5>
                        </div>
                        <div class="col-md-9">
                            <h4 style="text-align: left"><?php echo $model->nombre; ?></h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <h5 style="text-align: right">
                                N&deg; de identificación:<br />
                                <small>(Identification number)</small>
                            </h5>
                        </div>
                        <div class="col-md-9">
                            <h5 style="text-align: left"><i>
                                    <?php echo $model->carnet; ?></i>
                            </h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <h5 style="text-align: right">
                                Procedimiento de soldadura:<br />
                                <small>(Welding procedure)</small>
                            </h5>
                        </div>
                        <div class="col-md-3">
                            <?php if (isset($model->procedimientoSoldadura) && $model->procedimientoSoldadura != null) { ?>
                                <h5 style="text-align: left">
                                    <i><?php echo $model->procedimientoSoldadura; ?></i>
                                </h5>
                            <?php } else { ?>
                                <div class="form-group">
                                    <?php echo Html::activeTextInput($model, 'procedimientoSoldadura', array('class' => 'form-control', 'placeholder' => 'Ej: 001')); ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <h5 style="text-align: right">
                                Empresa:<br />
                                <small>(Enterprise)</small>
                            </h5>
                        </div>
                        <div class="col-md-9">
                            <h5 style="text-align: left"><i>
                                    <?php echo $model->empresa; ?></i>
                            </h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-5">
                                <h5 style="text-align: right"><strong>Resultados del Examen Te&oacute;rico: </strong></h5>
                            </div>
                            <?php if ($model->control == 1) { ?>
                                <div class="col-md-7">
                                    <?php if ($model->resultadoExamenTeorico == 0) { ?>
                                        <label class="radio-inline">
                                            <h5>Aprobado<br /><small>(Passed)</small></h5>
                                        </label><u> ( &nbsp; ) </u>
                                        <label class="radio-inline">
                                            <h5>No Requerido<br /><small>(Not required)</small></h5>
                                        </label><u> ( X ) </u>
                                    <?php } ?>
                                    <?php if ($model->resultadoExamenTeorico == 1) { ?>
                                        <label class="radio-inline">
                                            <h5>Aprobado<br /><small>(Passed)</small></h5>
                                        </label><u> ( X ) </u>
                                        <label class="radio-inline">
                                            <h5>No Requerido<br /><small>(Not required)</small></h5>
                                        </label><u> ( &nbsp; ) </u>
                                    <?php } ?>
                                </div>
                            <?php } else { ?>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <div class="radio-list">
                                            <label class="radio-inline">
                                                <input type="radio" name="FormCertificadoSoldadorAcero[resultadoExamenTeorico]" id="aprobada" value="1">
                                                <h5>Aprobado<br /><small>(Passed)</small></h5>
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="FormCertificadoSoldadorAcero[resultadoExamenTeorico]" id="rechazada" value="0" checked>
                                                <h5>No Requerido<br /><small>(Not required)</small></h5>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                <h5><strong>Resultados del Examen Pr&aacute;ctico</strong></h5>
                            </center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                    <th style="text-align: center">
                                        <h5><b>Datos t&eacute;cnicos</b><br />(Technical Data)</h5>
                                    </th>
                                    <th style="text-align: center">
                                        <h5><b>Detalles de la prueba</b><br />(Weld details)</h5>
                                    </th>
                                    <th style="text-align: center">
                                        <h5><b>Rango de homologaci&oacute;n</b><br />(Range of approval)</h5>
                                    </th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: right; width: 20%; align-content: center">
                                            <h5>Proceso de soldadura<br /><small>(Welding process)</small></h5>
                                        </td>
                                        <td style="text-align: center; width: 30%; align-content: center">
                                            <div class="form-group">
                                                <?php if (isset($model->procesoSoldadura_detalles) && $model->procesoSoldadura_detalles != null) { ?>
                                                    <?php echo $model->procesoSoldadura_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'procesoSoldadura_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center; width: 50%; align-content: center">
                                            <div class="form-group">
                                                <?php if (isset($model->procesoSoldadura_rango) && $model->procesoSoldadura_rango != null) { ?>
                                                    <?php echo $model->procesoSoldadura_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'procesoSoldadura_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Chapa o tubo<br /><small>(Plate or pipe)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->chapaTubo_detalles) && $model->chapaTubo_detalles != null) { ?>
                                                    <?php echo $model->chapaTubo_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'chapaTubo_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->chapaTubo_rango) && $model->chapaTubo_rango != null) { ?>
                                                    <?php echo $model->chapaTubo_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'chapaTubo_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Tipos de uni&oacute;n<br /><small>(Joint type)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->tipoUnion_detalles) && $model->tipoUnion_detalles != null) { ?>
                                                    <?php echo $model->tipoUnion_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'tipoUnion_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->tipoUnion_rango) && $model->tipoUnion_rango != null) { ?>
                                                    <?php echo $model->tipoUnion_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'tipoUnion_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Grupos de metal base<br /><small>(Parent metal(s) group(s))</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->grupoMetalBase_detalles) && $model->grupoMetalBase_detalles != null) { ?>
                                                    <?php echo $model->grupoMetalBase_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'grupoMetalBase_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->grupoMetalBase_rango) && $model->grupoMetalBase_rango != null) { ?>
                                                    <?php echo $model->grupoMetalBase_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'grupoMetalBase_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Tipo de metal de aportaci&oacute;n<br /><small>(Filler metal type)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->tipoMetal_detalles) && $model->tipoMetal_detalles != null) { ?>
                                                    <?php echo $model->tipoMetal_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'tipoMetal_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->tipoMetal_rango) && $model->tipoMetal_rango != null) { ?>
                                                    <?php echo $model->tipoMetal_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'tipoMetal_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Espesor de la muestra (mm)<br /><small>(Test piece thickness)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->espesor_detalles) && $model->espesor_detalles != null) { ?>
                                                    <?php echo $model->espesor_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'espesor_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->espesor_rango) && $model->espesor_rango != null) { ?>
                                                    <?php echo $model->espesor_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'espesor_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Di&aacute;metro exterior del tubo<br /><small>(Pipe outside diameter)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->diametro_detalles) && $model->diametro_detalles != null) { ?>
                                                    <?php echo $model->diametro_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'diametro_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->diametro_rango) && $model->diametro_rango != null) { ?>
                                                    <?php echo $model->diametro_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'diametro_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Posici&oacute;n de soldadura<br /><small>(Welding position)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->posicion_detalles) && $model->posicion_detalles != null) { ?>
                                                    <?php echo $model->posicion_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'posicion_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->posicion_rango) && $model->posicion_rango != null) { ?>
                                                    <?php echo $model->posicion_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'posicion_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Resanado/Respaldo<br /><small>(Gouging/backing)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->resanado_detalles) && $model->resanado_detalles != null) { ?>
                                                    <?php echo $model->resanado_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'resanado_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->resanado_rango) && $model->resanado_rango != null) { ?>
                                                    <?php echo $model->resanado_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'resanado_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            <h5>Fecha de emisi&oacute;n <span class="text-danger">*</span>:<br /><small>(Date of issue)</small></h5>
                            <br />
                            <h5>Per&iacute;odo de validez <span class="text-danger">*</span>:<br /><small>(Validity period)</small></h5>
                        </div>
                        <div class="col-sm-4">
                            <?php if (isset($model->emision) && $model->emision != null) { ?>
                                <h5><u><?php echo $model->emision; ?></u></h5>
                            <?php } else { ?>
                                <?php echo Html::activeTextInput($model, 'emision', array('class' => 'form-control form-control date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'emision', array('class' => 'text-danger')); ?></span>
                            <?php } ?>
                            <br />
                            <?php if (isset($model->validez) && $model->validez != null) { ?>
                                <br />
                                <h5><u><?php echo $model->validez; ?></u></h5>
                            <?php } else { ?>
                                <h5><u>Pendiente de definici&oacute;n</u></h5>
                            <?php } ?>
                        </div>
                        <div class="col-sm-6">
                            <br />
                            <center>
                                <hr />
                            </center>
                            <center>
                                <strong>
                                    <?php
                                    if ($model->nivelAprobacion != null) {
                                        //Validar aqui que el funcionario este activo
                                        if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                                            echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                                            echo '<br/>';
                                            echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                                        } else {
                                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                                            echo '<br/>';
                                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                                        }
                                    }
                                    ?>
                                </strong>
                                <h5>
                                    Nombre y firma del jefe de la dependencia del RCB
                                    <br />
                                    <small>
                                        (Manager's name and signature of RCB Organizational Unit)
                                    </small>
                                </h5>
                            </center>
                        </div>
                    </div>
                    <br /><br />

                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                <h5>
                                    <strong>El presente certificado est&aacute; avalado por los siguientes ensayos</strong><br />
                                    (This Certificate has been endorsed by the following tests:)
                                </h5>
                            </center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                    <th style="text-align: center">
                                        <h5><b>Tipo de ensayo</b><br />(Type of test)</h5>
                                    </th>
                                    <th style="text-align: center">
                                        <h5><b>Realizado y aceptable</b><br />(Performed and acceptable)</h5>
                                    </th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: center; width: 40%; align-content: center">
                                            <h5>Visual<br /><small>(Visual)</small></h5>
                                        </td>
                                        <td style="text-align: center; width: 60%; align-content: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->visual) && $model->visual != null) {
                                                    if ($model->visual == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'visual', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>Radiogr&aacute;fico o ultras&oacute;nico<br /><small>(Radiographic or ultrasonic)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->radiograficoUltrasonico) && $model->radiograficoUltrasonico != null) {
                                                    if ($model->radiograficoUltrasonico == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'radiograficoUltrasonico', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>Part&iacute;culas magn&eacute;ticas<br /><small>(Magnetic particles)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->particulasMagneticas) && $model->particulasMagneticas != null) {
                                                    if ($model->particulasMagneticas == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'particulasMagneticas', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>L&iacute;quidos penetrantes<br /><small>(Dye penetrant)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->liquidosPenetrantes) && $model->liquidosPenetrantes != null) {
                                                    if ($model->liquidosPenetrantes == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'liquidosPenetrantes', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>Fractura<br /><small>(Fracture)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->rotura) && $model->rotura != null) {
                                                    if ($model->rotura == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'rotura', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>Doblado<br /><small>(Bend)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->doblado) && $model->doblado != null) {
                                                    if ($model->doblado == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'doblado', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>Otros ensayos<br /><small>(Others additional test)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->otros) && $model->otros != null) {
                                                    if ($model->otros == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'otros', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <center>
                        <h3>
                            Condiciones generales para el soldador homologado<br />
                            <small>General condictions for the welder</small>
                        </h3>
                    </center>
                    <div class="row">
                        <div class="col-md-12">
                            <ul>
                                <li>
                                    <p align="justify">
                                        La vigencia del presente Certificado (3 a&ntilde;os) depender&aacute; del estricto cumplimiento
                                        de los siguientes aspectos:<br />
                                        The validity of the present Certificate (3 years) will depend on the strict execution of the following
                                        aspect:
                                    </p>
                                    <ul>
                                        <li>
                                            <p align="justify">
                                                Del nivel cualitativo a demostrar por el soldador en trabajos de soldaduras habituales.<br />
                                                Of the qualitative level to demonstrate for the welder in habitual welding works.
                                            </p>
                                        </li>
                                        <li>
                                            <p align="justify">
                                                Del ininterrumpido ejercicio de trabajos en la especialidad de soldadura (en caso contrario,
                                                no m&aacute;s de 3 meses de interrupci&oacute;n de los trabajos de soldadura).<br />
                                                Of the uninterrupted exercise of works in the welding specialty (otherwise, not more than 3 months
                                                of interruption in the welding works).
                                            </p>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <p align="justify">
                                        El presente Certificado se basa en las exigencias de la Norma UNE – EN 287 – 1.<br />
                                        De acuerdo y bajo la aprobaci&oacute;n entre las partes podr&aacute;n emplearse otras normas como referencia<br />
                                        The present certifcate is based on the demands of UNE – EN 287 – 1.<br />
                                        In agreement and under approval between the parts other standards as reference will be able to be used
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div><br /><br />

                    <center>
                        <h3>CONFIRMACI&Oacute;N DE LAS INSPECCIONES ANUALES</h3>
                    </center>
                    <br /><br />
                    <div class="row">
                        <div class="col-md-2">
                            <b>Primera Anual:</b>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>Fecha</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Firma del Inspector</center>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>N&deg; de Control</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Cu&ntilde;o del Inspector</center>
                        </div>
                    </div>
                    <br /><br /><br />
                    <div class="row">
                        <div class="col-md-2">
                            <b>Segunda Anual:</b>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>Fecha</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Firma del Inspector</center>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>N&deg; de Control</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Cu&ntilde;o del Inspector</center>
                        </div>
                    </div>

                    <center>
                        <h3>Simbolog&iacute;a Aplicada<br /><small>( Applied Symbols )</small></h3>
                    </center>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php if (isset($model->simbologia)) { ?>
                                    <center>
                                        <p><?php echo $model->simbologia ?></p>
                                    </center>
                                <?php } else { ?>
                                    <?php echo Html::activeLabel($model, 'simbologia', array('class' => 'control-label')); ?>
                                    <?php echo Html::activeTextArea($model, 'simbologia', array('class' => 'ckeditor form-control', 'data-placeholder' => 'Empresa')); ?>
                                    <span class="help-block"><?php echo Html::error($model, 'simbologia', array('class' => 'text-danger')); ?>
                                    </span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-2">
                                <?php if ($model->control == 0) { ?>
                                    <button id="inspeccion_servicios_submit" type="submit" class="btn btn-primary"><i class="icon-ok"></i> Vista Previa del Certificado</button>
                                <?php } elseif ($model->control == 1) { ?>
                                    <a href="<?php echo Url::to('/certificados/PDFCertificadoSoldadorAcero', array('o' => $serv->id)) ?>" class="btn btn-success"><i class="fa fa-file"></i> Generar Certificado</a>
                                    |
                                    <a href="<?php echo Url::to('/certificados/certificado', array('obj' => $serv->id, 'tipoObj' => 1, 'tipoCe' => 1)) ?>" class="btn btn-danger"><i class="icon-repeat"></i> Corregir Certificado</a>
                                <?php } ?>
                                |
                                <a href="<?php echo Url::to('/inspecciones/detalles-personas', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-default"> Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));

$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));

?>

<?php
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-print-page/jquery.printPage.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));


$js = <<<JS
FormComponents.certificado(); Custom.InitPrint(); 
JS;
$this->registerJs($js);
?>