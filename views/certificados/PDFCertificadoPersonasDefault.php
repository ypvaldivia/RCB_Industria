<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R3/PC401</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 99%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CERTIFICADO DE HOMOLOGACI&Oacute;N DE
                    <?php echo strtoupper($serv->inspeccion0->homologacion0->titulo_anuario); ?>
                </h4>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 85%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                    </span>
                </h5>
            </td>
        </tr>
        <tr>
            <td style="width: 70%">
                <h5>
                    Que se otorga al Sr.:
                    <span style="text-decoration: underline"><?php echo $serv->persona0->nombre_apellidos; ?></span>
                    <br /><br />
                    N&deg; de Identidad Permanente:
                    <span style="text-decoration: underline"><?php echo $serv->persona0->ci; ?></span>
                    <br /><br />
                    De la Entidad:
                    <span style="text-decoration: underline"><?php echo $serv->inspeccion0->entidad0->nombre; ?></span>
                </h5>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>

    <p align="justify">
        Por aprobar de forma satisfactoria la evaluaci&oacute;n para la homologaci&oacute;n de
        <span style="text-decoration: underline"><?php echo $serv->inspeccion0->homologacion0->titulo_anuario; ?></span>
        y cumplir con los requisitos del Registro Cubano de Buques
        especificados en
        <?php echo $model->servicio; ?>
        para realizar los servicios de:
        <h3 style="text-align: center;">
            <?php echo $model->documentacion; ?></h3>
    </p>

    <div class="row">
        <div class="col-sm-12">
            <h5>
                Fecha Expedici&oacute;n:
                <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                <br /><br />
                Fecha Vencimiento:
                <span style="text-decoration: underline"><?php echo $model->vencimiento; ?></span>
            </h5>
        </div>
    </div>

    <br /><br /><br />

    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php
                    if ($model->nivelAprobacion != null) {
                        //Validar aqui que el funcionario este activo
                        if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                            echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                        } else {
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                        }
                    }
                    ?>
                </span>
            </td>
        </tr>
    </table>

</page>

<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R3/PC401</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <br /><br />
    <table style="border: 1px">
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CONDICIONES GENERALES
                </h4>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter; justify-content: center">
                <ol>
                    <li style="text-align: justify; padding-bottom: 12px">
                        El presente Certificado ser&aacute; v&aacute;lido solo para la persona nominalizada en &eacute;l.
                    </li>
                    <li style="text-align: justify; padding-bottom: 12px">
                        La vigencia del Certificado expira en la fecha de vencimiento establecida en el presente
                        documento siempre que se someta a inspecciones de seguimiento por el RCB con resultados
                        satisfactorios sobre la base de la verificaci&oacute;n del cumplimiento de los requisitos y condiciones
                        en los cuales fueron homologados.
                    </li>
                    <li style="text-align: justify; padding-bottom: 12px">
                        Las inspecciones de seguimiento anuales se realizar&aacute;n a solicitud del Cliente antes de la fecha
                        de aniversario de la expedici&oacute;n del Certificado de Homologaci&oacute;n para la confirmaci&oacute;n de su
                        validez.
                    </li>
                    <li style="text-align: justify; padding-bottom: 12px">
                        Las entidades con personal homologado informar&aacute;n al <b>RCB</b> sobre cualquier cambio o
                        modificaci&oacute;n que afecte al objeto de homologaci&oacute;n (cambios tecnol&oacute;gicos, especificaciones,
                        personal, instalaciones, m&eacute;todos de ensayos y calibraci&oacute;n, equipamiento y otros) en un plazo no
                        mayor de 7 d&iacute;as despu&eacute;s de efectuados dichos cambios o modificaciones.
                    </li>
                    <li style="text-align: justify;">
                        El Certificado pierde su validez si se incumple los requisitos bajo las cuales fue otorgado.
                    </li>
                </ol>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CONFIRMACI&Oacute;N DE LAS INSPECCIONES ANUALES
                </h4>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr style="text-align: center; font-weight: lighter">
                        <td style="width: 20%; text-align: center">Primera Anual</td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Fecha
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Firma del Inspector
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />N&deg; de Control
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Cu&ntilde;o del Inspector
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr style="text-align: center; font-weight: lighter">
                        <td style="width: 20%; text-align: center">Segunda Anual</td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Fecha
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Firma del Inspector
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />N&deg; de Control
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Cu&ntilde;o del Inspector
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</page>