<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
    }

    table tr {
        margin: 0px auto;
    }

    table td {
        padding: 3px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }

    .tbl,
    .tbl1 {
        border: solid 1px black;
        font-size: 10px;
        width: 100%;
    }

    .tbl td {
        border: solid 1px black;
    }

    .tbl1 td {
        border: solid 1px black;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="3mm" backbottom="3mm" backleft="10mm" backright="10mm">

    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">N&deg;:&nbsp;<?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?></td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="width: 100%; border:none;">
        <tr>
            <td style="border: 1px solid black; width: 100%; align-content: center; text-align: center">
                <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 100%">
                <strong style="font-size: medium">
                    CERTIFICADO PROVISIONAL DE HOMOLOGACI&Oacute;N DEL OPERADOR/AJUSTADOR DE SOLDEO POR RESISTENCIA<br />
                    (INTERIM APPROVAL CERTIFICATE OF WELDING OPERATOR)
                </strong>
            </td>
        </tr>
    </table>

    <table style="border:none; padding-top: 0px; padding-bottom: 0px; font-size: 11px" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 25%">
                &nbsp;
            </td>
            <td style="width: 55%">
                &nbsp;
            </td>
            <td>
                N&deg;:&nbsp;
                <span style="text-decoration: underline">
                    <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                </span>
            </td>
        </tr>
        <tr>
            <td>
                Nombre del operador de soldeo:<br />
                <small>(Welding Operator)</small>
            </td>
            <td colspan="2" style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic; font-size: 14px; font-weight: bold"><?php echo $serv->persona0->nombre_apellidos; ?></span>
            </td>
        </tr>
        <tr>
            <td>
                N&deg; de identificación:<br />
                <small>(Identification number)</small>
            </td>
            <td colspan="2" style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic"><?php echo $serv->persona0->ci; ?></span>
            </td>
        </tr>
        <tr>
            <td>
                Procedimiento de soldadura:<br />
                <small>(Welding procedure)</small>
            </td>
            <td colspan="2" style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic"><?php echo $model->procedimientoSoldadura; ?></span>
            </td>
        </tr>
        <tr>
            <td>
                Empresa:<br />
                <small>(Enterprise)</small>
            </td>
            <td colspan="2" style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic"><?php echo $serv->inspeccion0->entidad0->nombre; ?></span>
            </td>
        </tr>
    </table>

    <br />

    <table style="border:none; padding-top: 0px; padding-bottom: 0px; font-size: 12px" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 40%; text-align: right">
                <strong>Resultados del Examen Te&oacute;rico: </strong><br />
                <small>(Results of the theory test)</small>
            </td>
            <td style="width: 20%; text-align: right">
                Aprobado<br /><small>(Passed)</small>
            </td>
            <td style="width: 5%; text-align: left">
                <?php
                if ($model->resultadoExamenTeorico == 0) {
                    echo '( - )';
                } else {
                    echo '( X )';
                }
                ?>
            </td>
            <td style="width: 20%; text-align: right">
                No Requerido<br /><small>(Not required)</small>
            </td>
            <td style="width: 5%; text-align: left">
                <?php
                if ($model->resultadoExamenTeorico == 0) {
                    echo '( X )';
                } else {
                    echo '( - )';
                }
                ?>
            </td>
            <td style="width: 10%; text-align: left">
                &nbsp;
            </td>
        </tr>
    </table>

    <h5 style="text-align: center"><b>Resultados del Examen Pr&aacute;ctico</b>&nbsp;<small>(Results of the practical test)</small></h5>
    <table class="tbl">
        <tr>
            <td style="text-align: center; width: 40%"><b>Datos t&eacute;cnicos</b><br />(Technical Data)</td>
            <td style="text-align: center; width: 25%"><b>Detalles de la prueba</b><br />(Weld details)</td>
            <td style="text-align: center; width: 35%"><b>Rango de homologaci&oacute;n</b><br />(Range of approval)</td>
        </tr>
        <tr>
            <td>Proceso de soldadura<br /><small>(Welding process)</small></td>
            <td style="text-align: center">
                <?php echo $model->procesoSoldadura_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->procesoSoldadura_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Chapa o tubo<br /><small>(Plate or pipe)</small></td>
            <td style="text-align: center">
                <?php echo $model->chapaTubo_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->chapaTubo_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Tipos de uni&oacute;n<br /><small>(Joint type)</small></td>
            <td style="text-align: center">
                <?php echo $model->tipoUnion_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->tipoUnion_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Grupos de metal base<br /><small>(Parent metal(s) group(s))</small></td>
            <td style="text-align: center">
                <?php echo $model->grupoMetalBase_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->grupoMetalBase_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Tipo de metal de aportaci&oacute;n<br /><small>(Filler metal type)</small></td>
            <td style="text-align: center">
                <?php echo $model->tipoMetal_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->tipoMetal_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Gases de Protecci&oacute;n<br /><small>(Shielding gases)</small></td>
            <td style="text-align: center">
                <?php echo $model->gasesProteccion; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->gasesProteccion_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Espesor de la muestra (mm)<br /><small>(Test piece thickness)</small></td>
            <td style="text-align: center">
                <?php echo $model->espesor_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->espesor_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Di&aacute;metro exterior del tubo<br /><small>(Pipe outside diameter)</small></td>
            <td style="text-align: center">
                <?php echo $model->diametro_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->diametro_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Posici&oacute;n de soldadura<br /><small>(Welding position)</small></td>
            <td style="text-align: center">
                <?php echo $model->posicion_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->posicion_rango; ?>
            </td>
        </tr>
        <tr>
            <td>T&eacute;cnica de pasada &uacute;nica o m&uacute;ltiple<br /><small>(Single or multiple layer)</small></td>
            <td style="text-align: center">
                <?php echo $model->tecnicaPasada_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->tecnicaPasada_rango; ?>
            </td>
        </tr>
    </table>

    <br /><br />

    <table style="border:none; font-size: 12px">
        <tr>
            <td style="width: 50%">

            </td>
            <td style="width: 50%; text-align: center; align-content: center" rowspan="3">
                <br /><br />
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php echo $model->funcionario; ?>
                </span><br />
                Nombre y firma del Inspector del Registro
                <br />
                <small>
                    (RCB Surveyor´s name and signature)
                </small>
            </td>
        </tr>
        <tr>
            <td>
                <table style="border:none">
                    <tr>
                        <td>
                            Fecha de emisi&oacute;n: <br />
                            <small>(Date of issue)</small>
                        </td>
                        <td><br /><i><?php echo $model->emision; ?></i></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="border:none">
                    <tr>
                        <td>
                            Per&iacute;odo de validez:<br />
                            <small>(Validity period)</small>
                        </td>
                        <td>5 meses (hasta la emisi&oacute;n del Certificado definitivo)<br />
                            <i>5 months (until the issue of a long term Certificate)</i></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <br />

</page>

<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>
        <h5 style="text-align: center">
            DORSO DEL CERTIFICADO DE HOMOLOGACI&Oacute;N DE OPERADORES DE SOLDEO (Parte 1)<br />
            <small>Back of approval certificate of welding operator (Part 1)</small>
        </h5>
    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">N&deg;:&nbsp;<?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?></td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <h5 style="text-align: center">
        <b>Calificaci&oacute;n basada en:</b><br />
        <small>Qualification based on:</small>
    </h5>

    <table class="tbl1">
        <tr>
            <td style="text-align: center; width: 60%">Ensayos de procedimiento de soldeo (WPS)<br /><small>Welding procedure test (WPS)</small></td>
            <td style="text-align: center; width: 40%">
                <?php if ($model->wps == 1) {
                    echo 'X';
                } else {
                    echo '-';
                }; ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Ensayos de soldeo anterior a la producci&oacute;n o ensayos de producci&oacute;n<br /><small>Production or Pre-production welding test</small></td>
            <td style="text-align: center">
                <?php if ($model->ensayoAnterior == 1) {
                    echo 'X';
                } else {
                    echo '-';
                } ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Ensayo de muestra de producci&oacute;n<br /><small>Production sample test according</small></td>
            <td style="text-align: center">
                <?php if ($model->muestraProd == 1) {
                    echo 'X';
                } else {
                    echo '-';
                } ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Ensayo de funcionamiento<br /><small>Performance test</small></td>
            <td style="text-align: center">
                <?php if ($model->funcionamiento == 1) {
                    echo 'X';
                } else {
                    echo '-';
                }  ?>
            </td>
        </tr>
    </table>

    <h5 style="text-align: center">
        <b>El presente certificado est&aacute; avalado por los siguientes ensayos</b><br />
        <small>This Certificate has been endorsed by the following tests</small>
    </h5>

    <table class="tbl1">
        <tr>
            <td style="text-align: center; width: 60%"><b>Tipo de ensayo</b><br />(Type of test)</td>
            <td style="text-align: center; width: 40%"><b>Realizado y aceptable</b><br />(Performed and acceptable)</td>
        </tr>
        <tr>
            <td style="text-align: center">Visual<br /><small>(Visual)</small></td>
            <td style="text-align: center">
                <?php if ($model->visual == 1) {
                    echo 'X';
                } else {
                    echo '-';
                }; ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Radiogr&aacute;fico o ultras&oacute;nico<br /><small>(Radiographic or ultrasonic)</small></td>
            <td style="text-align: center">
                <?php if ($model->radiograficoUltrasonico == 1) {
                    echo 'X';
                } else {
                    echo '-';
                } ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Part&iacute;culas magn&eacute;ticas<br /><small>(Magnetic particles)</small></td>
            <td style="text-align: center">
                <?php if ($model->particulasMagneticas == 1) {
                    echo 'X';
                } else {
                    echo '-';
                } ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">L&iacute;quidos penetrantes<br /><small>(Dye penetrant)</small></td>
            <td style="text-align: center">
                <?php if ($model->liquidosPenetrantes == 1) {
                    echo 'X';
                } else {
                    echo '-';
                }  ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Otros ensayos<br /><small>(Others additional test)</small></td>
            <td style="text-align: center">
                <?php if ($model->otros == 1) {
                    echo 'X';
                } else {
                    echo '-';
                }  ?>
            </td>
        </tr>
    </table>

    <p align="center">
        <h5>
            <strong>
                Los ensayos han sido realizados contra el Nivel
                <?php echo $model->nivel; ?>
                de la norma de defectos de soldadura UNE – EN ISO 5817: 2009.<br />
                De acuerdo y bajo la aprobaci&oacute;n entre las partes podr&aacute;n emplearse otras normas como referencia.<br /><br />
                The tests had been performed against the criteria of the Level <?php echo $model->nivel; ?> of the Standard UNE – EN ISO 5817: 2009.<br />
                In agreement and under agreement between the parts other standards as reference will be able to be used.
            </strong>
        </h5>
    </p>

    <h5 style="text-align: center"><b>ENSAYO DE TRACCI&Oacute;N</b><br />(Tensile strength test)</h5>

    <table style="border:none; padding-top: 0px; padding-bottom: 0px; font-size: 12px" cellspacing="0" cellpadding="0">
        <tr>
            <td style="text-align: right; width: 20%">Temperatura del ensayo:<br /><small>Test temperature</small></td>
            <td style="text-align: left; width: 10%"><?= $model->temperaturaTr ?> &deg;C</td>
            <td style="text-align: right; width: 20%">Realizado por:<br /><small>Made by</small></td>
            <td style="text-align: left; width: 20%"><?= $model->realizadoTr ?></td>
            <td style="text-align: right; width: 20%">N&deg; de referencia:<br /><small>Reference No</small></td>
            <td style="text-align: left; width: 10%"><?= $model->norefTr ?></td>
        </tr>
    </table>

    <table class="tbl1">
        <tr>
            <td style="text-align: center; width: 10%; font-weight: bold">N&deg; Ensayo<br /><small>Test N&deg;</small></td>
            <td style="text-align: center; width: 20%; font-weight: bold">Probeta a x b/D (mm)<br /><small>Test piece a x b/D (mm)</small></td>
            <td style="text-align: center; width: 10%; font-weight: bold">Secci&oacute;n<br /><small>Section<br />S0 (mm2)</small></td>
            <td style="text-align: center; width: 15%; font-weight: bold">L&iacute;mite El&aacute;stico<br /><small>Yield strength<br />Re (MPa)</small></td>
            <td style="text-align: center; width: 15%; font-weight: bold">Resistencia a tracci&oacute;n<br /><small>Tensile strength<br />Rm (MPa)</small></td>
            <td style="text-align: center; width: 15%; font-weight: bold">Posici&oacute;n de la rotura<br /><small>Brekeage position</small></td>
            <td style="text-align: center; width: 15%; font-weight: bold">Aspecto de la rotura<br /><small>Brekeage aspect</small></td>
        </tr>
        <?php foreach ($model->ensayoTraccion as $t) { ?>
            <tr>
                <td style="text-align: center;"><?= $t->numeroTr ?></td>
                <td style="text-align: center;"><?= $t->probetaTr ?></td>
                <td style="text-align: center;"><?= $t->seccionTr ?></td>
                <td style="text-align: center;"><?= $t->limiteTr ?></td>
                <td style="text-align: center;"><?= $t->resistenciaTr ?></td>
                <td style="text-align: center;"><?= $t->posicionTr ?></td>
                <td style="text-align: center;"><?= $t->aspectoTr ?></td>
            </tr>
        <?php } ?>
    </table>

    <br />

    <h5 style="text-align: center"><b>ENSAYO DE DOBLADO</b><br />(Bend test)</h5>

    <table style="border:none; padding-top: 0px; padding-bottom: 0px; font-size: 12px" cellspacing="0" cellpadding="0">
        <tr>
            <td style="text-align: right; width: 13%">Realizado por:<br /><small>Made by</small></td>
            <td style="text-align: left; width: 13%; vertical-align: middle"><?= $model->realizadoTr ?></td>
            <td style="text-align: right; width: 14%">N&deg; de referencia:<br /><small>Reference No</small></td>
            <td style="text-align: left; width: 10%; vertical-align: middle"><?= $model->norefTr ?></td>
            <td style="text-align: right; width: 18%">Diámetro mandrino:<br /><small>Mandril diameter</small></td>
            <td style="text-align: left; width: 7%"><?= $model->diametroM ?> mm</td>
            <td style="text-align: right; width: 15%">Distancia rodillos:<br /><small>Rolls distance</small></td>
            <td style="text-align: left; width: 10%"><?= $model->distanciaR ?> mm</td>
        </tr>
    </table>

    <table class="tbl1">
        <tr>
            <td style="text-align: center; width: 10%; font-weight: bold">N&deg; Ensayo<br /><small>Test N&deg;</small></td>
            <td style="text-align: center; width: 30%; font-weight: bold">Probeta a x b/D (mm)<br /><small>Test piece a x b/D (mm)</small></td>
            <td style="text-align: center; width: 20%; font-weight: bold">Posici&oacute;n<br /><small>Position</small></td>
            <td style="text-align: center; width: 20%; font-weight: bold">&Aacute;ngulo de doblado<br /><small>Bending angle</small></td>
            <td style="text-align: center; width: 20%; font-weight: bold">Resultado<br /><small>Result</small></td>
        </tr>
        <?php foreach ($model->ensayoDoblado as $d) { ?>
            <tr>
                <td style="text-align: center;"><?= $d->numeroDb ?></td>
                <td style="text-align: center;"><?= $d->probetaDb ?></td>
                <td style="text-align: center;"><?= $d->posicionDb ?></td>
                <td style="text-align: center;"><?= $d->anguloDb ?></td>
                <td style="text-align: center;"><?= $d->resultadoDb ?></td>
            </tr>
        <?php } ?>
    </table>

</page>

<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>
        <h5 style="text-align: center">
            DORSO DEL CERTIFICADO DE HOMOLOGACI&Oacute;N DE OPERADORES DE SOLDEO (Parte 2)<br />
            <small>Back of approval certificate of welding operator (Part 2)</small>
        </h5>
    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">N&deg;:&nbsp;<?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?></td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <h5 style="text-align: center"><b>ENSAYO MACROGR&Aacute;FICO</b><br />(Macrographic examination)</h5>

    <table style="border:none; padding-top: 0px; padding-bottom: 0px; font-size: 12px" cellspacing="0" cellpadding="0">
        <tr>
            <td style="text-align: right; width: 20%">Realizado por:<br /><small>Made by</small></td>
            <td style="text-align: left; width: 15%"><?= $model->realizadoMc ?></td>
            <td style="text-align: right; width: 20%">N&deg; de referencia:<br /><small>Reference No</small></td>
            <td style="text-align: left; width: 10%"><?= $model->norefMc ?></td>
        </tr>
    </table>

    <br />

    <h5 style="text-align: center"><b>ENSAYO DE ROTURA</b><br />(Fracture test)</h5>

    <table style="border:none; padding-top: 0px; padding-bottom: 0px; font-size: 12px" cellspacing="0" cellpadding="0">
        <tr>
            <td style="text-align: right; width: 20%">Temperatura del ensayo:<br /><small>Test temperature</small></td>
            <td style="text-align: left; width: 10%"><?= $model->temperaturaRt ?> &deg;C</td>
            <td style="text-align: right; width: 20%">Realizado por:<br /><small>Made by</small></td>
            <td style="text-align: left; width: 20%; vertical-align: middle"><?= $model->realizadoRt ?></td>
            <td style="text-align: right; width: 20%">N&deg; de referencia:<br /><small>Reference No</small></td>
            <td style="text-align: left; width: 10%; vertical-align: middle"><?= $model->norefTRt ?></td>
        </tr>
    </table>

    <table class="tbl1">
        <tr>
            <td style="text-align: center; width: 10%; font-weight: bold">N&deg; Ensayo<br /><small>Test N&deg;</small></td>
            <td style="text-align: center; width: 20%; font-weight: bold">Probeta a x b/D (mm)<br /><small>Test piece a x b/D (mm)</small></td>
            <td style="text-align: center; width: 15%; font-weight: bold">Posici&oacute;n<br /><small>Position</small></td>
            <td style="text-align: center; width: 20%; font-weight: bold">Denominaci&oacute;n<br /><small>Denomination</small></td>
            <td style="text-align: center; width: 20%; font-weight: bold">Resultado<br /><small>Result</small></td>
            <td style="text-align: center; width: 15%; font-weight: bold">Nivel de calidad<br /><small>Quality level</small></td>
        </tr>
        <?php foreach ($model->ensayoRotura as $r) { ?>
            <tr>
                <td style="text-align: center;"><?= $r->numeroRt ?></td>
                <td style="text-align: center;"><?= $r->probetaRt ?></td>
                <td style="text-align: center;"><?= $r->posicionRt ?></td>
                <td style="text-align: center;"><?= $r->denominacionRt ?></td>
                <td style="text-align: center;"><?= $r->resultadoRt ?></td>
                <td style="text-align: center;"><?= $r->nivelQRt ?></td>
            </tr>
        <?php } ?>
    </table>

    <h5 style="text-align: center">Simbolog&iacute;a Aplicada</h5>

    <div class="row">
        <div class="col-md-12" style="align-content: center">
            <p style="text-align: left; font-size: 11px; align-content: center;"><?php echo $model->simbologia; ?></p>
        </div>
    </div>
</page>