<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Inspecciones de Personas <small> Gesti&oacute;n de la informaci&oacute;n de Certificados</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="icon-barcode"></i>
    <a href="<?php echo Url::to('/inspecciones/index-personas') ?>">Inspecciones - </a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Personas - </a><i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Certificados </a>
</li>
<li class="btn-group">
    <a href="<?php echo Url::to('/inspecciones/detalles-personas', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-primary">
        <span style="color: #FFFFFF"><i class="icon-arrow-left"></i>Atras</span></a>
</li><?php $this->endBlock(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Certificado de Inspecci&oacute;n</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php
                $form = ActiveForm::begin([
                    'id' => 'certificadoBuzoSoldador-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                ]);
                ?>
                <div class="container">
                    <?php echo Html::errorSummary($model);
                    ?>

                </div>

                <img src="<?= Yii::getAlias('@web/uploads'); ?>/images/bannerHeader.jpg" width="100%">
                <center>
                    <strong style="font-size: large">
                        CERTIFICADO PROVISIONAL DE HOMOLOGACI&Oacute;N DEL OPERADOR/AJUSTADOR DE SOLDEO POR RESISTENCIA<br />
                        (INTERIM APPROVAL CERTIFICATE OF WELDING OPERATOR)
                    </strong>
                </center>

                <div class="form-body">

                    <div class="row">
                        <div class="col-sm-9">&nbsp;</div>
                        <div class="col-sm-3 float-left">
                            <div class="form-group">
                                <h5>
                                    N&deg;:
                                    <span style="text-decoration: underline">
                                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                                    </span>
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <h5 style="text-align: right">
                                Nombre del operador de soldeo:<br />
                                <small>(Welding Operator)</small>
                            </h5>
                        </div>
                        <div class="col-md-9">
                            <h4 style="text-align: left"><?php echo $model->nombre; ?></h4>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <h5 style="text-align: right">
                                N&deg; de identificaci&oacute;n:<br />
                                <small>(Identification number)</small>
                            </h5>
                        </div>
                        <div class="col-md-9">
                            <h5 style="text-align: left"><i>
                                    <?php echo $model->carnet; ?></i>
                            </h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <h5 style="text-align: right">
                                Procedimiento de soldadura:<br />
                                <small>(Welding procedure)</small>
                            </h5>
                        </div>
                        <div class="col-md-3">
                            <?php if (isset($model->procedimientoSoldadura) && $model->procedimientoSoldadura != null) { ?>
                                <h5 style="text-align: left">
                                    <i><?php echo $model->procedimientoSoldadura; ?></i>
                                </h5>
                            <?php } else { ?>
                                <div class="form-group">
                                    <?php echo Html::activeTextInput($model, 'procedimientoSoldadura', array('class' => 'form-control', 'placeholder' => 'Ej: 001')); ?>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <h5 style="text-align: right">
                                Empresa:<br />
                                <small>(Enterprise)</small>
                            </h5>
                        </div>
                        <div class="col-md-9">
                            <h5 style="text-align: left"><i>
                                    <?php echo $model->empresa; ?></i>
                            </h5>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-5">
                                <h5 style="text-align: right"><strong>Resultados del Examen Te&oacute;rico: </strong></h5>
                            </div>
                            <?php if ($model->control == 1) { ?>
                                <div class="col-md-7">
                                    <?php if ($model->resultadoExamenTeorico == 0) { ?>
                                        <label class="radio-inline">
                                            <h5>Aprobado<br /><small>(Passed)</small></h5>
                                        </label><u> ( &nbsp; ) </u>
                                        <label class="radio-inline">
                                            <h5>No Requerido<br /><small>(Not required)</small></h5>
                                        </label><u> ( X ) </u>
                                    <?php } ?>
                                    <?php if ($model->resultadoExamenTeorico == 1) { ?>
                                        <label class="radio-inline">
                                            <h5>Aprobado<br /><small>(Passed)</small></h5>
                                        </label><u> ( X ) </u>
                                        <label class="radio-inline">
                                            <h5>No Requerido<br /><small>(Not required)</small></h5>
                                        </label><u> ( &nbsp; ) </u>
                                    <?php } ?>
                                </div>
                            <?php } else { ?>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <div class="radio-list">
                                            <label class="radio-inline">
                                                <input type="radio" name="FormCertificadoOperadorSoldeo[resultadoExamenTeorico]" id="aprobada" value="1">
                                                <h5>Aprobado<br /><small>(Passed)</small></h5>
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="FormCertificadoOperadorSoldeo[resultadoExamenTeorico]" id="rechazada" value="0" checked>
                                                <h5>No Requerido<br /><small>(Not required)</small></h5>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                <h5><strong>Resultados del Examen Pr&aacute;ctico</strong></h5>
                            </center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                    <th style="text-align: center">
                                        <h5><b>Datos t&eacute;cnicos</b><br />(Technical Data)</h5>
                                    </th>
                                    <th style="text-align: center">
                                        <h5><b>Detalles de la prueba</b><br />(Weld details)</h5>
                                    </th>
                                    <th style="text-align: center">
                                        <h5><b>Rango de homologaci&oacute;n</b><br />(Range of approval)</h5>
                                    </th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: right; width: 20%; align-content: center">
                                            <h5>Proceso de soldadura<br /><small>(Welding process)</small></h5>
                                        </td>
                                        <td style="text-align: center; width: 30%; align-content: center">
                                            <div class="form-group">
                                                <?php if (isset($model->procesoSoldadura_detalles) && $model->procesoSoldadura_detalles != null) { ?>
                                                    <?php echo $model->procesoSoldadura_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'procesoSoldadura_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center; width: 50%; align-content: center">
                                            <div class="form-group">
                                                <?php if (isset($model->procesoSoldadura_rango) && $model->procesoSoldadura_rango != null) { ?>
                                                    <?php echo $model->procesoSoldadura_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'procesoSoldadura_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Chapa o tubo<br /><small>(Plate or pipe)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->chapaTubo_detalles) && $model->chapaTubo_detalles != null) { ?>
                                                    <?php echo $model->chapaTubo_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'chapaTubo_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->chapaTubo_rango) && $model->chapaTubo_rango != null) { ?>
                                                    <?php echo $model->chapaTubo_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'chapaTubo_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Tipos de uni&oacute;n<br /><small>(Joint type)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->tipoUnion_detalles) && $model->tipoUnion_detalles != null) { ?>
                                                    <?php echo $model->tipoUnion_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'tipoUnion_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->tipoUnion_rango) && $model->tipoUnion_rango != null) { ?>
                                                    <?php echo $model->tipoUnion_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'tipoUnion_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Grupos de metal base<br /><small>(Parent metal(s) group(s))</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->grupoMetalBase_detalles) && $model->grupoMetalBase_detalles != null) { ?>
                                                    <?php echo $model->grupoMetalBase_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'grupoMetalBase_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->grupoMetalBase_rango) && $model->grupoMetalBase_rango != null) { ?>
                                                    <?php echo $model->grupoMetalBase_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'grupoMetalBase_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Tipo de metal de aportaci&oacute;n<br /><small>(Filler metal type)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->tipoMetal_detalles) && $model->tipoMetal_detalles != null) { ?>
                                                    <?php echo $model->tipoMetal_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'tipoMetal_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->tipoMetal_rango) && $model->tipoMetal_rango != null) { ?>
                                                    <?php echo $model->tipoMetal_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'tipoMetal_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Gases de Protecci&oacute;n<br /><small>(Shielding gases)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->gasesProteccion) && $model->gasesProteccion != null) { ?>
                                                    <?php echo $model->gasesProteccion; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'gasesProteccion', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->gasesProteccion_rango) && $model->gasesProteccion_rango != null) { ?>
                                                    <?php echo $model->gasesProteccion_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'gasesProteccion_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Espesor de la muestra (mm)<br /><small>(Test piece thickness)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->espesor_detalles) && $model->espesor_detalles != null) { ?>
                                                    <?php echo $model->espesor_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'espesor_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->espesor_rango) && $model->espesor_rango != null) { ?>
                                                    <?php echo $model->espesor_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'espesor_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Di&aacute;metro exterior del tubo<br /><small>(Pipe outside diameter)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->diametro_detalles) && $model->diametro_detalles != null) { ?>
                                                    <?php echo $model->diametro_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'diametro_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->diametro_rango) && $model->diametro_rango != null) { ?>
                                                    <?php echo $model->diametro_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'diametro_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>Posici&oacute;n de soldadura<br /><small>(Welding position)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->posicion_detalles) && $model->posicion_detalles != null) { ?>
                                                    <?php echo $model->posicion_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'posicion_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->posicion_rango) && $model->posicion_rango != null) { ?>
                                                    <?php echo $model->posicion_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'posicion_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right; align-content: center">
                                            <h5>T&eacute;cnica de pasada &uacute;nica o m&uacute;ltiple<br /><small>(Single or multiple layer)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->tecnicaPasada_detalles) && $model->tecnicaPasada_detalles != null) { ?>
                                                    <?php echo $model->tecnicaPasada_detalles; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'tecnicaPasada_detalles', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php if (isset($model->tecnicaPasada_rango) && $model->tecnicaPasada_rango != null) { ?>
                                                    <?php echo $model->tecnicaPasada_rango; ?>
                                                <?php } else { ?>
                                                    <?php echo Html::activeTextInput($model, 'tecnicaPasada_rango', array('class' => 'form-control')); ?>
                                                <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            <h5>Fecha de emisi&oacute;n <span class="text-danger">*</span>:<br /><small>(Date of issue)</small></h5>
                            <br />
                            <h5>Per&iacute;odo de validez:<br /><small>(Validity period)</small></h5>
                        </div>
                        <div class="col-sm-4">
                            <?php if (isset($model->emision) && $model->emision != null) { ?>
                                <h5><u><?php echo $model->emision; ?></u></h5>
                            <?php } else { ?>
                                <?php echo Html::activeTextInput($model, 'emision', array('class' => 'form-control form-control date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'emision', array('class' => 'text-danger')); ?></span>
                            <?php } ?><br /><br />
                            <small>5 meses (hasta la emisi&oacute;n del Certificado definitivo)<br />
                                <i>5 months (until the issue of a long term Certificate)</i></small>
                        </div>
                        <?php if (!isset($model->funcionario) || $model->funcionario == null) { ?>
                            <div class="col-sm-6">
                                <br />
                                <center>
                                    <hr />
                                </center>
                                <center>
                                    <strong>
                                        <?php
                                        echo Html::activeDropDownList($model, 'funcionario', ArrayHelper::map($inspectores, 'nombre_apellidos', 'nombre_apellidos'), array('class' => 'form-control select2me', 'style' => 'width:200px !important', 'prompt' => 'Inspectores actuantes...'));
                                        ?>
                                    </strong>
                                    <h5>
                                        Nombre y firma del Inspector del Registro
                                        <br />
                                        <small>
                                            (RCB Surveyor´s name and signature)
                                        </small>
                                    </h5>
                                </center>
                            </div>
                        <?php } else { ?>
                            <div class="col-sm-6">
                                <br />
                                <center>
                                    <hr />
                                </center>
                                <center>
                                    <strong>
                                        <?php echo $model->funcionario; ?>
                                    </strong>
                                    <h5>
                                        Nombre y firma del Inspector del Registro
                                        <br />
                                        <small>
                                            (RCB Surveyor´s name and signature)
                                        </small>
                                    </h5>
                                </center>
                            </div>
                        <?php } ?>
                    </div>
                    <br />

                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                <h5>
                                    <strong>Calificaci&oacute;n basada en:</strong><br />
                                    (Qualification based on)
                                </h5>
                            </center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="text-align: center; width: 40%; align-content: center">
                                            <h5>Ensayos de procedimiento de soldeo (WPS)<br /><small>Welding procedure test (WPS)</small></h5>
                                        </td>
                                        <td style="text-align: center; width: 60%; align-content: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->wps) && $model->wps != null) {
                                                    if ($model->wps == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'wps', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>Ensayos de soldeo anterior a la producci&oacute;n o ensayos de producci&oacute;n<br /><small>Production or Pre-production welding test</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->ensayoAnterior) && $model->ensayoAnterior != null) {
                                                    if ($model->ensayoAnterior == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'ensayoAnterior', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>Ensayo de muestra de producci&oacute;n<br /><small>Production sample test according</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->muestraProd) && $model->muestraProd != null) {
                                                    if ($model->muestraProd == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'muestraProd', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>Ensayo de funcionamiento<br /><small>Performance test</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->funcionamiento) && $model->funcionamiento != null) {
                                                    if ($model->funcionamiento == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'funcionamiento', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <center>
                                <h5>
                                    <strong>El presente certificado est&aacute; avalado por los siguientes ensayos</strong><br />
                                    (This Certificate has been endorsed by the following tests:)
                                </h5>
                            </center>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                    <th style="text-align: center">
                                        <h5><b>Tipo de ensayo</b><br />(Type of test)</h5>
                                    </th>
                                    <th style="text-align: center">
                                        <h5><b>Realizado y aceptable</b><br />(Performed and acceptable)</h5>
                                    </th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="text-align: center; width: 40%; align-content: center">
                                            <h5>Visual<br /><small>(Visual)</small></h5>
                                        </td>
                                        <td style="text-align: center; width: 60%; align-content: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->visual) && $model->visual != null) {
                                                    if ($model->visual == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'visual', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>Radiogr&aacute;fico o ultras&oacute;nico<br /><small>(Radiographic or ultrasonic)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->radiograficoUltrasonico) && $model->radiograficoUltrasonico != null) {
                                                    if ($model->radiograficoUltrasonico == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'radiograficoUltrasonico', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>Part&iacute;culas magn&eacute;ticas<br /><small>(Magnetic particles)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->particulasMagneticas) && $model->particulasMagneticas != null) {
                                                    if ($model->particulasMagneticas == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'particulasMagneticas', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>L&iacute;quidos penetrantes<br /><small>(Dye penetrant)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->liquidosPenetrantes) && $model->liquidosPenetrantes != null) {
                                                    if ($model->liquidosPenetrantes == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'liquidosPenetrantes', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; align-content: center">
                                            <h5>Otros ensayos<br /><small>(Others additional test)</small></h5>
                                        </td>
                                        <td style="text-align: center">
                                            <div class="form-group">
                                                <?php
                                                if (isset($model->otros) && $model->otros != null) {
                                                    if ($model->otros == 1) {
                                                        echo '<b>X</b>';
                                                    } else {
                                                        echo '<b>-</b>';
                                                    }
                                                } else {
                                                    echo Html::activeCheckbox($model, 'otros', array('class' => 'checkbox'));
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <?php if (isset($model->nivel) && $model->nivel != null) { ?>
                        <div class="row">
                            <p align="center">
                                <strong>
                                    Los ensayos han sido realizados contra el Nivel
                                    <?php echo $model->nivel; ?>
                                    de la norma de defectos de soldadura UNE – EN ISO 5817: 2009.<br />
                                    De acuerdo y bajo la aprobaci&oacute;n entre las partes podr&aacute;n emplearse otras normas como referencia.<br /><br />
                                    The tests had been performed against the criteria of the Level <?php echo $model->nivel; ?> of the Standard UNE – EN ISO 5817: 2009.<br />
                                    In agreement and under agreement between the parts other standards as reference will be able to be used.
                                </strong>
                            </p>
                        </div>
                    <?php } else { ?>
                        <div class="row">
                            <div class="col-sm-4 right">
                                Los ensayos han sido realizados contra el Nivel
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?php echo Html::activeTextInput($model, 'nivel', array('class' => 'form-control')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                de la norma de defectos de soldadura UNE – EN ISO 5817: 2009.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                De acuerdo y bajo la aprobaci&oacute;n entre las partes podr&aacute;n emplearse otras normas como referencia.<br />
                                The tests had been performed against the criteria of the Level ___ of the Standard UNE – EN ISO 5817: 2009.
                                In agreement and under agreement between the parts other standards as reference will be able to be used.
                            </div>
                        </div>
                    <?php } ?>

                    <h4 style="text-align: center"><b>ENSAYO DE TRACCI&Oacute;N</b><br />(Tensile strength test)</h4>

                    <div class="row">
                        <div class="col-md-3">
                            <?php if (isset($model->temperaturaTr) && $model->temperaturaTr != null) { ?>
                                Temperatura del ensayo: <?= $model->temperaturaTr ?> &deg;C
                                <br />
                                <small>Test temperature</small>
                            <?php } else { ?>
                                Temperatura del ensayo (&deg;C):<br />
                                <small>Test temperature</small>
                                <?php echo Html::activeTextInput($model, 'temperaturaTr', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                            <?php if (isset($model->realizadoTr) && $model->realizadoTr != null) { ?>
                                Realizado por: <?= $model->realizadoTr ?>
                                <br />
                                <small>Made by</small>
                            <?php } else { ?>
                                Realizado por:<br />
                                <small>Made by</small>
                                <?php echo Html::activeTextInput($model, 'realizadoTr', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                            <?php if (isset($model->norefTr) && $model->norefTr != null) { ?>
                                N&deg; de referencia: <?= $model->norefTr ?>
                                <br />
                                <small>Reference No</small>
                            <?php } else { ?>
                                N&deg; de referencia:<br />
                                <small>Reference No</small>
                                <?php echo Html::activeTextInput($model, 'norefTr', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                    </div>

                    <br />
                    <?php if ($model->ensayoTraccion == null) { ?>
                        <table class="table table-striped table-bordered table-hover" id="trac_table">
                            <thead>
                                <tr>
                                    <th style="font-size: 12px">N&deg; Ensayo<br /><small>Test N&deg;</small></th>
                                    <th style="font-size: 12px">Probeta a&Cross;b/D (mm)<br /><small>Test piece a&Cross;b/D (mm)</small></th>
                                    <th style="font-size: 12px">Secci&oacute;n<br /><small>Section<br />S0 (mm2)</small></th>
                                    <th style="font-size: 12px">L&iacute;mite El&aacute;stico<br /><small>Yield strength<br />Re (MPa)</small></th>
                                    <th style="font-size: 12px">Resistencia a tracci&oacute;n<br /><small>Tensile strength<br />Rm (MPa)</small></th>
                                    <th style="font-size: 12px">Posici&oacute;n de la rotura<br /><small>Brekeage position</small></th>
                                    <th style="font-size: 12px">Aspecto de la rotura<br /><small>Brekeage aspect</small></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <a class="btn btn-primary" href="#addrehtr" data-toggle="modal">
                            <i class="fa fa-plus"></i> A&ntilde;adir ensayo de tracci&oacute;n
                        </a>
                    <?php } else { ?>
                        <table class="table table-striped table-bordered table-hover" id="trac_table">
                            <thead>
                                <tr>
                                    <th style="font-size: 12px">N&deg; Ensayo<br /><small>Test N&deg;</small></th>
                                    <th style="font-size: 12px">Probeta a&Cross;b/D (mm)<br /><small>Test piece a&Cross;b/D (mm)</small></th>
                                    <th style="font-size: 12px">Secci&oacute;n<br /><small>Section<br />S0 (mm2)</small></th>
                                    <th style="font-size: 12px">L&iacute;mite El&aacute;stico<br /><small>Yield strength<br />Re (MPa)</small></th>
                                    <th style="font-size: 12px">Resistencia a tracci&oacute;n<br /><small>Tensile strength<br />Rm (MPa)</small></th>
                                    <th style="font-size: 12px">Posici&oacute;n de la rotura<br /><small>Brekeage position</small></th>
                                    <th style="font-size: 12px">Aspecto de la rotura<br /><small>Brekeage aspect</small></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($model->ensayoTraccion as $t) { ?>
                                    <tr>
                                        <td><?= $t->numeroTr ?></td>
                                        <td><?= $t->probetaTr ?></td>
                                        <td><?= $t->seccionTr ?></td>
                                        <td><?= $t->limiteTr ?></td>
                                        <td><?= $t->resistenciaTr ?></td>
                                        <td><?= $t->posicionTr ?></td>
                                        <td><?= $t->aspectoTr ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                    <br /><br />

                    <h4 style="text-align: center"><b>ENSAYO DE DOBLADO</b><br />(Bend test)</h4>

                    <div class="row">
                        <div class="col-md-3">
                            <?php if (isset($model->realizadoDb) && $model->realizadoDb != null) { ?>
                                Realizado por: <?= $model->realizadoDb ?>
                                <br />
                                <small>Made by</small>
                            <?php } else { ?>
                                Realizado por:<br />
                                <small>Made by</small>
                                <?php echo Html::activeTextInput($model, 'realizadoDb', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                            <?php if (isset($model->norefDb) && $model->norefDb != null) { ?>
                                N&deg; de referencia: <?= $model->norefDb ?>
                                <br />
                                <small>Reference No</small>
                            <?php } else { ?>
                                N&deg; de referencia:<br />
                                <small>Reference No</small>
                                <?php echo Html::activeTextInput($model, 'norefDb', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                            <?php if (isset($model->diametroM) && $model->diametroM != null) { ?>
                                Diámetro mandrino (mm): <?= $model->diametroM ?> mm
                                <br />
                                <small>Mandril diameter (mm)</small>
                            <?php } else { ?>
                                Diámetro mandrino (mm):<br />
                                <small>Mandril diameter (mm)</small>
                                <?php echo Html::activeTextInput($model, 'diametroM', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                            <?php if (isset($model->distanciaR) && $model->distanciaR != null) { ?>
                                Distancia rodillos: <?= $model->distanciaR ?> mm
                                <br />
                                <small>Rolls distance</small>
                            <?php } else { ?>
                                Distancia rodillos (mm):<br />
                                <small>Rolls distance (mm)</small>
                                <?php echo Html::activeTextInput($model, 'distanciaR', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                    </div>

                    <br />

                    <?php if ($model->ensayoDoblado == null) { ?>
                        <table class="table table-striped table-bordered table-hover" id="doblado_table">
                            <thead>
                                <tr>
                                    <th style="font-size: 12px">N&deg; Ensayo<br /><small>Test N&deg;</small></th>
                                    <th style="font-size: 12px">Probeta a&Cross;b/D (mm)<br /><small>Test piece a&Cross;b/D (mm)</small></th>
                                    <th style="font-size: 12px">Posici&oacute;n<br /><small>Position</small></th>
                                    <th style="font-size: 12px">&Aacute;ngulo de doblado<br /><small>Bending angle</small></th>
                                    <th style="font-size: 12px">Resultado<br /><small>Result</small></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                        </table>
                        <a class="btn btn-primary" href="#adddoblado" data-toggle="modal">
                            <i class="fa fa-plus"></i> A&ntilde;adir ensayo de doblado
                        </a>
                    <?php } else { ?>
                        <table class="table table-striped table-bordered table-hover" id="doblado_table">
                            <thead>
                                <tr>
                                    <th style="font-size: 12px">N&deg; Ensayo<br /><small>Test N&deg;</small></th>
                                    <th style="font-size: 12px">Probeta a&Cross;b/D (mm)<br /><small>Test piece a&Cross;b/D (mm)</small></th>
                                    <th style="font-size: 12px">Posici&oacute;n<br /><small>Position</small></th>
                                    <th style="font-size: 12px">&Aacute;ngulo de doblado<br /><small>Bending angle</small></th>
                                    <th style="font-size: 12px">Resultado<br /><small>Result</small></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($model->ensayoDoblado as $d) { ?>
                                    <tr>
                                        <td><?= $d->numeroDb ?></td>
                                        <td><?= $d->probetaDb ?></td>
                                        <td><?= $d->posicionDb ?></td>
                                        <td><?= $d->anguloDb ?></td>
                                        <td><?= $d->resultadoDb ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                    <br /><br />

                    <h4 style="text-align: center"><b>ENSAYO MACROGR&Aacute;FICO</b><br />(Macrographic examination)</h4>

                    <div class="row">
                        <div class="col-md-3">
                            <?php if (isset($model->realizadoMc) && $model->realizadoMc != null) { ?>
                                Realizado por: <?= $model->realizadoMc ?>
                                <br />
                                <small>Made by</small>
                            <?php } else { ?>
                                Realizado por:<br />
                                <small>Made by</small>
                                <?php echo Html::activeTextInput($model, 'realizadoMc', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                            <?php if (isset($model->norefMc) && $model->norefMc != null) { ?>
                                N&deg; de referencia: <?= $model->norefMc ?>
                                <br />
                                <small>Reference No</small>
                            <?php } else { ?>
                                N&deg; de referencia:<br />
                                <small>Reference No</small>
                                <?php echo Html::activeTextInput($model, 'norefMc', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                    </div>

                    <br />

                    <h4 style="text-align: center"><b>ENSAYO DE ROTURA</b><br />(Fracture test)</h4>

                    <div class="row">
                        <div class="col-md-3">
                            <?php if (isset($model->temperaturaRt) && $model->temperaturaRt != null) { ?>
                                Temperatura del ensayo: <?= $model->temperaturaRt ?> &deg;C
                                <br />
                                <small>Test temperature</small>
                            <?php } else { ?>
                                Temperatura del ensayo (&deg;C):<br />
                                <small>Test temperature</small>
                                <?php echo Html::activeTextInput($model, 'temperaturaRt', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                            <?php if (isset($model->realizadoRt) && $model->realizadoRt != null) { ?>
                                Realizado por: <?= $model->realizadoRt ?>
                                <br />
                                <small>Made by</small>
                            <?php } else { ?>
                                Realizado por:<br />
                                <small>Made by</small>
                                <?php echo Html::activeTextInput($model, 'realizadoRt', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                            <?php if (isset($model->norefTRt) && $model->norefTRt != null) { ?>
                                N&deg; de referencia: <?= $model->norefTRt ?>
                                <br />
                                <small>Reference No</small>
                            <?php } else { ?>
                                N&deg; de referencia:<br />
                                <small>Reference No</small>
                                <?php echo Html::activeTextInput($model, 'norefTRt', array('class' => 'form-control')); ?>
                            <?php } ?>
                        </div>
                    </div>

                    <br />

                    <?php if ($model->ensayoRotura == null) { ?>
                        <table class="table table-striped table-bordered table-hover" id="rotura_table">
                            <thead>
                                <tr>
                                    <th style="font-size: 12px">N&deg; Ensayo<br /><small>Test N&deg;</small></th>
                                    <th style="font-size: 12px">Probeta a&Cross;b/D (mm)<br /><small>Test piece a&Cross;b/D (mm)</small></th>
                                    <th style="font-size: 12px">Posici&oacute;n<br /><small>Position</small></th>
                                    <th style="font-size: 12px">Denominaci&oacute;n<br /><small>Denomination</small></th>
                                    <th style="font-size: 12px">Resultado<br /><small>Result</small></th>
                                    <th style="font-size: 12px">Nivel de calidad<br /><small>Quality level</small></th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                        </table>
                        <a class="btn btn-primary" href="#addrotura" data-toggle="modal">
                            <i class="fa fa-plus"></i> A&ntilde;adir ensayo de rotura
                        </a>
                    <?php } else { ?>
                        <table class="table table-striped table-bordered table-hover" id="rotura_table">
                            <thead>
                                <tr>
                                    <th style="font-size: 12px">N&deg; Ensayo<br /><small>Test N&deg;</small></th>
                                    <th style="font-size: 12px">Probeta a&Cross;b/D (mm)<br /><small>Test piece a&Cross;b/D (mm)</small></th>
                                    <th style="font-size: 12px">Posici&oacute;n<br /><small>Position</small></th>
                                    <th style="font-size: 12px">Denominaci&oacute;n<br /><small>Denomination</small></th>
                                    <th style="font-size: 12px">Resultado<br /><small>Result</small></th>
                                    <th style="font-size: 12px">Nivel de calidad<br /><small>Quality level</small></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($model->ensayoRotura as $r) { ?>
                                    <tr>
                                        <td><?= $r->numeroRt ?></td>
                                        <td><?= $r->probetaRt ?></td>
                                        <td><?= $r->posicionRt ?></td>
                                        <td><?= $r->denominacionRt ?></td>
                                        <td><?= $r->resultadoRt ?></td>
                                        <td><?= $r->nivelQRt ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                    <br /><br />

                    <div id="addrehtr" class="modal fade" aria-hidden="true" tabindex="-1" style="display: none;">
                        <div class="modal-dialog" style="width: 55%;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title">
                                        <h4 style="text-align: left">
                                            Adicionar Ensayo de Tracci&oacute;n
                                            <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                                        </h4>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <div class="" data-rail-visible1="1" data-always-visible="1">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label for="probeta">Probeta a&Cross;b/D (mm)<br /><small>Test piece a&Cross;b/D (mm)</small></label>
                                                    <input id="probeta" type="text" class="form-control">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="seccion">Secci&oacute;n<br /><small>Section [S0 (mm2)]</small></label>
                                                    <input id="seccion" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label for="limite">L&iacute;mite El&aacute;stico<br /><small>Yield strength [Re (MPa)]</small></label>
                                                    <input id="limite" type="text" class="form-control">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="resistencia">Resistencia a tracci&oacute;n<br /><small>Tensile strength [Rm (MPa)]</small></label>
                                                    <input id="resistencia" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label for="posicion">Posici&oacute;n de la rotura<br /><small>Brekeage position</small></label>
                                                    <input id="posicion" type="text" class="form-control">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="aspecto">Aspecto de la rotura<br /><small>Brekeage aspect</small></label>
                                                    <input id="aspecto" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <button id="addrowtr" type="button" class="btn btn-block" data-dismiss="modal" aria-hidden="true"><i class="fa fa-plus"></i> Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="adddoblado" class="modal fade" aria-hidden="true" tabindex="-1" style="display: none;">
                        <div class="modal-dialog" style="width: 55%;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title">
                                        <h4 style="text-align: left">
                                            Adicionar Ensayo de Doblado
                                            <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                                        </h4>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <div class="" data-rail-visible1="1" data-always-visible="1">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label for="probetaD">Probeta a&Cross;b/D (mm)<br /><small>Test piece a&Cross;b/D (mm)</small></label>
                                                    <input id="probetaD" type="text" class="form-control">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="positionD">Posici&oacute;n<br /><small>Position</small></label>
                                                    <input id="positionD" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label for="angulo">&Aacute;ngulo doblado<br /><small>Bending angle</small></label>
                                                    <input id="angulo" type="text" class="form-control">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="resultado">Resultado<br /><small>Result</small></label>
                                                    <input id="resultado" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <button id="addrowdb" type="button" class="btn btn-block" data-dismiss="modal" aria-hidden="true"><i class="fa fa-plus"></i> Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="addrotura" class="modal fade" aria-hidden="true" tabindex="-1" style="display: none;">
                        <div class="modal-dialog" style="width: 55%;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title">
                                        <h4 style="text-align: left">
                                            Adicionar Ensayo de Rotura
                                            <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                                        </h4>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <div class="" data-rail-visible1="1" data-always-visible="1">
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label for="probetaR">Probeta a&Cross;b/D (mm)<br /><small>Test piece a&Cross;b/D (mm)</small></label>
                                                    <input id="probetaR" type="text" class="form-control">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="positionR">Posici&oacute;n<br /><small>Position</small></label>
                                                    <input id="positionR" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label for="denominacionR">Denominaci&oacute;n<br /><small>Denomination</small></label>
                                                    <input id="denominacionR" type="text" class="form-control">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="resultadoR">Resultado<br /><small>Result</small></label>
                                                    <input id="resultadoR" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <label for="nivelql">Nivel de calidad<br /><small>Quality level</small></label>
                                                    <input id="nivelql" type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <button id="addrowrt" type="button" class="btn btn-block" data-dismiss="modal" aria-hidden="true"><i class="fa fa-plus"></i> Adicionar</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <center>
                        <h3>Simbolog&iacute;a Aplicada<br /><small>( Applied Symbols )</small></h3>
                    </center>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php if (isset($model->simbologia)) { ?>
                                    <center>
                                        <p><?php echo $model->simbologia ?></p>
                                    </center>
                                <?php } else { ?>
                                    <?php echo Html::activeLabel($model, 'simbologia', array('class' => 'control-label')); ?>
                                    <?php echo Html::activeTextArea($model, 'simbologia', array('class' => 'ckeditor form-control', 'data-placeholder' => 'Empresa')); ?>
                                    <span class="help-block"><?php echo Html::error($model, 'simbologia', array('class' => 'text-danger')); ?>
                                    </span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-2">
                                <?php if ($model->control == 0) { ?>
                                    <button id="inspeccion_servicios_submit" type="button" class="btn btn-primary"><i class="icon-ok"></i> Vista Previa del Certificado</button>
                                <?php } elseif ($model->control == 1) { ?>
                                    <a href="<?php echo Url::to('/certificados/PDFCertificadoOperadorSoldeoProvisional', array('o' => $serv->id)) ?>" class="btn btn-success"><i class="fa fa-file"></i> Generar Certificado</a>
                                    |
                                    <a href="<?php echo Url::to('/certificados/certificado', array('obj' => $serv->id, 'tipoObj' => 1, 'tipoCe' => 1)) ?>" class="btn btn-danger"><i class="icon-repeat"></i> Corregir Certificado</a>
                                <?php } ?>
                                |
                                <a href="<?php echo Url::to('/inspecciones/detalles-personas', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-default"> Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <input id="inspID" type="hidden" value="<?= $serv->id ?>">
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));

$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));

?>

<?php
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-print-page/jquery.printPage.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/ajaxEnsayosOperadorSoldeo.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
    FormComponents.certificado(); Custom.InitPrint(); 
    var counttr = 0;
    var countdb = 0;
    var countrt = 0;
    var arr = [];
    var tracc = [];
    var bend = [];
    var frac = [];

    //Traccion
    $('#addrowtr').click(function() {
        var element = {
            idcount: 'tr-' + counttr,
            number: counttr + 1,
            probeta: $('#probeta').val(),
            seccion: $('#seccion').val(),
            limite: $('#limite').val(),
            resistencia: $('#resistencia').val(),
            posicion: $('#posicion').val(),
            aspecto: $('#aspecto').val(),
        };
        tracc.push(element);
        counttr++;
        $('#trac_table').append('<tr id="' + element.idcount + '"><td>' + element.number + '</td><td>' + element.probeta + '</td><td>' + element.seccion + '</td><td>' + element.limite + '</td><td>' + element.resistencia + ' </td><td>' + element.posicion + ' </td><td>' + element.aspecto + ' </td><td style="text-align: center;"><button type="button" class="btn btn-mini" onclick="removeElementTracc(\'' + element.idcount + '\');"><i class="fa fa-trash"></i></button></td></tr>');
        emptyTraccForm();
    });

    function removeElementTracc(id) {
        for (var i = 0; i < tracc.length; i++) {
            if (tracc[i].idcount === id) {
                tracc.splice(i, 1);
            }
        }
        $('#' + id).remove();
        counttr--;
        for (var i = 0; i < tracc.length; i++) {
            $('#' + tracc[i].idcount).remove();
            tracc[i].idcount = 'tr-' + i + 1;
            tracc[i].number = i + 1;
            $('#trac_table').append('<tr id="' + tracc[i].idcount + '"><td>' + tracc[i].number + '</td><td>' + tracc[i].probeta + '</td><td>' + tracc[i].seccion + '</td><td>' + tracc[i].limite + '</td><td>' + tracc[i].resistencia + ' </td><td>' + tracc[i].posicion + ' </td><td>' + tracc[i].aspecto + ' </td><td style="text-align: center;"><button type="button" class="btn btn-mini" onclick="removeElementTracc(\'' + tracc[i].idcount + '\');"><i class="fa fa-trash"></i></button></td></tr>');
        }
    }

    function emptyTraccForm() {
        $('#probeta').val('');
        $('#seccion').val('');
        $('#limite').val('');
        $('#resistencia').val('');
        $('#posicion').val('');
        $('#aspecto').val('');
    }

    //Doblado
    $('#addrowdb').click(function() {
        var element = {
            idcount: 'tr-' + countdb,
            numberD: countdb + 1,
            probetaD: $('#probetaD').val(),
            posicionD: $('#positionD').val(),
            angulo: $('#angulo').val(),
            resultadoD: $('#resultado').val(),
        };
        bend.push(element);
        countdb++;
        $('#doblado_table').append('<tr id="' + element.idcount + '"><td>' + element.numberD + '</td><td>' + element.probetaD + '</td><td>' + element.posicionD + '</td><td>' + element.angulo + '</td><td>' + element.resultadoD + ' </td><td style="text-align: center;"><button type="button" class="btn btn-mini" onclick="removeElementDoblado(\'' + element.idcount + '\');"><i class="fa fa-trash"></i></button></td></tr>');
        emptyDbForm();
    });

    function removeElementDoblado(id) {
        for (var i = 0; i < bend.length; i++) {
            if (bend[i].idcount === id) {
                bend.splice(i, 1);
            }
        }
        $('#' + id).remove();
        countdb--;
        for (var i = 0; i < bend.length; i++) {
            $('#' + bend[i].idcount).remove();
            bend[i].idcount = 'tr-' + i + 1;
            bend[i].numberD = i + 1;
            $('#doblado_table').append('<tr id="' + bend[i].idcount + '"><td>' + bend[i].numberD + '</td><td>' + bend[i].probetaD + '</td><td>' + bend[i].posicionD + '</td><td>' + bend[i].angulo + '</td><td>' + bend[i].resultadoD + ' </td><td style="text-align: center;"><button type="button" class="btn btn-mini" onclick="removeElementDoblado(\'' + bend[i].idcount + '\');"><i class="fa fa-trash"></i></button></td></tr>');
        }
    }

    function emptyDbForm() {
        $('#probetaD').val('');
        $('#positionD').val('');
        $('#angulo').val('');
        $('#resultado').val('');
    }

    //Rotura
    $('#addrowrt').click(function() {
        var element = {
            idcount: 'tr-' + countrt,
            numberR: countrt + 1,
            probetaR: $('#probetaR').val(),
            posicionR: $('#positionR').val(),
            denominacionR: $('#denominacionR').val(),
            resultadoR: $('#resultadoR').val(),
            nivelql: $('#nivelql').val(),
        };
        frac.push(element);
        countrt++;
        $('#rotura_table').append('<tr id="' + element.idcount + '"><td>' + element.numberR + '</td><td>' + element.probetaR + '</td><td>' + element.posicionR + '</td><td>' + element.denominacionR + '</td><td>' + element.resultadoR + ' </td><td>' + element.nivelql + ' </td><td style="text-align: center;"><button type="button" class="btn btn-mini" onclick="removeElementRotura(\'' + element.idcount + '\');"><i class="fa fa-trash"></i></button></td></tr>');
        emptyRtForm();
    });

    function removeElementRotura(id) {
        for (var i = 0; i < frac.length; i++) {
            if (frac[i].idcount === id) {
                frac.splice(i, 1);
            }
        }
        $('#' + id).remove();
        countrt--;
        for (var i = 0; i < frac.length; i++) {
            $('#' + frac[i].idcount).remove();
            frac[i].idcount = 'tr-' + i + 1;
            frac[i].numberR = i + 1;
            $('#rotura_table').append('<tr id="' + frac[i].idcount + '"><td>' + frac[i].numberR + '</td><td>' + frac[i].probetaR + '</td><td>' + frac[i].posicionR + '</td><td>' + frac[i].denominacionR + '</td><td>' + frac[i].resultadoR + ' </td><td>' + frac[i].nivelql + ' </td><td style="text-align: center;"><button type="button" class="btn btn-mini" onclick="removeElementRotura(\'' + frac[i].idcount + '\');"><i class="fa fa-trash"></i></button></td></tr>');
        }
    }

    function emptyRtForm() {
        $('#probetaR').val('');
        $('#positionR').val('');
        $('#denominacionR').val('');
        $('#resultadoR').val('');
        $('#nivelql').val('');
    }

    $('#inspeccion_servicios_submit').click(function() {
        var mainform = {
            idinsp: $('#inspID').val(),
            procedimiento: $('#FormCertificadoOperadorSoldeo_procedimientoSoldadura').val(),
            examenTeorico: $('#aprobada:checked').val(),
            proceso: $('#FormCertificadoOperadorSoldeo_procesoSoldadura_detalles').val(),
            procesoRango: $('#FormCertificadoOperadorSoldeo_procesoSoldadura_rango').val(),
            chapa: $('#FormCertificadoOperadorSoldeo_chapaTubo_detalles').val(),
            chapaRango: $('#FormCertificadoOperadorSoldeo_chapaTubo_rango').val(),
            tipoUnion: $('#FormCertificadoOperadorSoldeo_tipoUnion_detalles').val(),
            tipoUnionRango: $('#FormCertificadoOperadorSoldeo_tipoUnion_rango').val(),
            grupoMetalBase: $('#FormCertificadoOperadorSoldeo_grupoMetalBase_detalles').val(),
            grupoMetalBaseRango: $('#FormCertificadoOperadorSoldeo_grupoMetalBase_rango').val(),
            tipoMetal: $('#FormCertificadoOperadorSoldeo_tipoMetal_detalles').val(),
            tipoMetalRango: $('#FormCertificadoOperadorSoldeo_tipoMetal_rango').val(),
            gasesProteccion: $('#FormCertificadoOperadorSoldeo_gasesProteccion').val(),
            gasesProteccionRango: $('#FormCertificadoOperadorSoldeo_gasesProteccion_rango').val(),
            espesor: $('#FormCertificadoOperadorSoldeo_espesor_detalles').val(),
            espesorRango: $('#FormCertificadoOperadorSoldeo_espesor_rango').val(),
            diametro: $('#FormCertificadoOperadorSoldeo_diametro_detalles').val(),
            diametroRango: $('#FormCertificadoOperadorSoldeo_diametro_rango').val(),
            posicion: $('#FormCertificadoOperadorSoldeo_posicion_detalles').val(),
            posicionRango: $('#FormCertificadoOperadorSoldeo_posicion_rango').val(),
            tecnicaPasada: $('#FormCertificadoOperadorSoldeo_tecnicaPasada_detalles').val(),
            tecnicaPasadaRango: $('#FormCertificadoOperadorSoldeo_tecnicaPasada_rango').val(),
            emision: $('#FormCertificadoOperadorSoldeo_emision').val(),
            funcionario: $('#FormCertificadoOperadorSoldeo_funcionario').val(),
            wps: $('#FormCertificadoOperadorSoldeo_wps:checked').val(),
            anterior: $('#FormCertificadoOperadorSoldeo_ensayoAnterior:checked').val(),
            muestra: $('#FormCertificadoOperadorSoldeo_muestraProd:checked').val(),
            funcionamiento: $('#FormCertificadoOperadorSoldeo_funcionamiento:checked').val(),
            visual: $('#FormCertificadoOperadorSoldeo_visual:checked').val(),
            radiografico: $('#FormCertificadoOperadorSoldeo_radiograficoUltrasonico:checked').val(),
            particulas: $('#FormCertificadoOperadorSoldeo_particulasMagneticas:checked').val(),
            liquidos: $('#FormCertificadoOperadorSoldeo_liquidosPenetrantes:checked').val(),
            otros: $('#FormCertificadoOperadorSoldeo_otros:checked').val(),
            nivel: $('#FormCertificadoOperadorSoldeo_nivel').val(),
            temperaturaTr: $('#FormCertificadoOperadorSoldeo_temperaturaTr').val(),
            realizadoTr: $('#FormCertificadoOperadorSoldeo_realizadoTr').val(),
            norefTr: $('#FormCertificadoOperadorSoldeo_norefTr').val(),
            realizadoDb: $('#FormCertificadoOperadorSoldeo_realizadoDb').val(),
            norefDb: $('#FormCertificadoOperadorSoldeo_norefDb').val(),
            diametroM: $('#FormCertificadoOperadorSoldeo_diametroM').val(),
            distanciaR: $('#FormCertificadoOperadorSoldeo_distanciaR').val(),
            realizadoMc: $('#FormCertificadoOperadorSoldeo_realizadoMc').val(),
            norefMc: $('#FormCertificadoOperadorSoldeo_norefMc').val(),
            temperaturaRt: $('#FormCertificadoOperadorSoldeo_temperaturaRt').val(),
            realizadoRt: $('#FormCertificadoOperadorSoldeo_realizadoRt').val(),
            norefTRt: $('#FormCertificadoOperadorSoldeo_norefTRt').val(),
            simbologia: $('#FormCertificadoOperadorSoldeo_simbologia').val(),
        };
        arr = [];
        arr.push(mainform);
        arr.push(tracc);
        arr.push(bend);
        arr.push(frac);
        $.ajax({
            url: '<?php echo Url::to(array('certificados/ajaxCertificadoOperadorSoldeoProvisional')); ?>',
            data: {
                formdata: arr
            },
            dataType: 'JSON',
            type: 'POST',
            success: function(json_format) {
                $(location).attr('href', "<?php echo Url::to('/certificados/redirectOSProvisional'); ?>");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        });
    });
JS;
$this->registerJs($js);
?>