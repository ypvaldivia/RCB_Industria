<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R20/IC403</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php

                            use app\models\Inspecciones;

                            echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 99%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CERTIFICADO DE LIMITACI&Oacute;N DEL MEDIO DE IZADO
                </h4>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 70%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg; de Control:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                    </span>
                </h5>
            </td>
        </tr>
    </table>

    <b>SE CERTIFICA QUE:</b><br /><br />
    <p align="justify">
        El medio de izado <?php echo $serv->medioIzado->nombre; ?> de <?php echo $serv->medioIzado->ci_nominal; ?> Kg
        de capacidad de izado nominal perteneciente a <?php echo $serv->medioIzado->propietario0->nombre; ?>,
        fue inspeccionado el d&iacute;a <?php echo Inspecciones::convertirFecha($serv->inspeccion0->getUltimaVisita()->fecha); ?>
        con el objetivo de realizar una <?php echo $serv->inspeccion0->tipoHomologacion->tipo; ?>.
    </p>
    <p align="justify">
        El medio de izado no obtendr&aacute; el Certificado de Seguridad T&eacute;cnica hasta que no se eliminen las no conformidades descritas
        en el Informe de Inspecci&oacute;n <b><?php echo $serv->inspeccion0->numero_control; ?></b> el cual se adjunta al presente "Certificado de Limitaci&oacute;n".
    </p>
    <p align="justify">
        El mismo podr&aacute; realizar operaciones de carga y descarga durante <b><i>3 meses</i></b> a partir de la fecha de emisi&oacute;n
        del presente documento y bajo las condiciones siguientes:
    </p>

    <p align="justify">
        <i>
            <b>
                La Capacidad de Izado máxima de operación del medio de izado no deberá exceder
                <?= $model->limitacionIzado ?> kilogramos
            </b>
        </i>
    </p>

    <div class="row">
        <div class="col-md-12">
            <h5>Emitido en <?= $model->lugar ?>, el <?= $model->expedicion ?></h5>
            <h5>Vence: <?= $model->vencimiento ?></h5>
        </div>
    </div>

    <br /><br /><br />
    <br /><br /><br />
    <br /><br /><br />
    <br /><br /><br />

    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php echo $model->funcionario; ?>
                    <br />
                    Inspector Actuante
                </span>
            </td>
        </tr>
    </table>

</page>