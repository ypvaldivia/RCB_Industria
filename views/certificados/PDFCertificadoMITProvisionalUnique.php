<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
        table-layout: fixed;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<?php
$first = is_array($serv) ? $serv[0] : $serv;
?>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R2/IC404</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 99%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CERTIFICADO DE HOMOLOGACI&Oacute;N PROVISIONAL<br />
                    <small><i>PROVISIONAL CERTIFICATE OF APPROVAL</i></small>
                </h4>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 80%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $first->inspeccion0->numero_control; ?>
                    </span>
                </h5>
            </td>
        </tr>
        <tr>
            <td style="width: 70%">
                <h5>
                    <b>QUE SE OTORGA A:</b><br />
                    <small><b><i>Granted to:</i></b></small>
                    <span style="text-decoration: underline">
                        <?php
                        echo '<br/>';
                        echo '<h4 style="text-align: center;">';
                        echo '<b>' . $first->medioIzado->propietario0->nombre . '</b>';
                        echo '<br/>';
                        echo $first->medioIzado->propietario0->direccion;
                        echo '</h4>';
                        ?>
                    </span>
                </h5>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>

    <p align="justify">
        Por obtener resultados satisfactorios en las inspecciones realizadas en el proceso de
        homologaci&oacute;n y cumplir con los requisitos del RCB Sociedad Clasificadora especificados en
        P C4-01 "Procedimiento para la evaluaci&oacute;n de la conformidad de productos, servicios y
        personal calificado" en el cap&iacute;tulo 1.1 "Generalidades" del "Manual de aprobaci&oacute;n de
        productos, servicios y personal calificado (MAPS)" y en la Resoluci&oacute;n No. 293-2014, MITRANS:
        "Reglamento para la explotaci&oacute;n de medios de izado"
        <br />
        <small>
            <i>
                For getting satisfactory results in the surveys carried out in the process of approval and complying
                with the requirements of RCB Sociedad Clasificadora specified in
                P C4-01 "Procedure for the assesment of the conformity of products, services and qualified personnel",
                in the chapter 1.1 "General" of the "Manual of approval of of products, services and qualified personnel of RCB (MAPS)"
                and in the Resolution No. 293-2014, MITRANS:
                "Regulation for the operation of hoisting means"
            </i>
        </small>
    </p>

    <p align="justify">
        Se le otorga el presente Certificado al producto "<b>Medio de Izado Terrestre</b>" del tipo:<br />
        <small><i>This certificate is granted to the product "Terrestrial Hoisting Mean" of the type:</i></small>
    </p>
    <table border="1" width="98%">
        <tr>
            <td style="text-align: center; width: 5%">Nº </td>
            <td style="text-align: center; width: 20%">Tipo</td>
            <td style="text-align: center; width: 50%">Modelo </td>
            <td style="text-align: center; width: 25%">Capacidad de trabajo nominal <br /><small>(kilogramos)</small></td>
        </tr>
        <?php
        $i = 1;
        foreach ($serv as $d) {
            if ($d->aprobado == 1) {
        ?>
                <tr>
                    <td style="text-align: center"><?php echo $i; ?> </td>
                    <td style="text-align: center; align-content: center"><?php echo ($d->medioIzado->tipo0 != null) ? $d->medioIzado->tipo0->tipo : ""; ?> </td>
                    <td style="text-align: center; align-content: center"><?php echo $d->medioIzado->nombre; ?> </td>
                    <td style="text-align: center; align-content: center"><?php echo $d->medioIzado->ci_nominal; ?></td>
                </tr>
        <?php $i++;
            }
        } ?>
    </table>

    <p align="justify">
        El presente Certificado se expide en base al Informe de Inspección N&deg;: &nbsp;
        <span style="text-decoration: underline; font-weight: bold">
            <?= $first->inspeccion0->numero_control; ?>
        </span>
        <br />
        <small>
            <i>
                This certificate is issued base on the Survey report N&deg;: &nbsp;
                <span style="text-decoration: underline; font-weight: bold">
                    <?= $first->inspeccion0->numero_control; ?>
                </span>
            </i>
        </small>
    </p>

    <div class="row">
        <div class="col-sm-6">
            <h5>
                Fecha Expedici&oacute;n:
                <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                <br />
                <i>
                    Issue date:
                    <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                </i>
            </h5>
        </div>
        <div class="col-sm-6">
            <table style="border:none;">
                <tr>
                    <td style="width: 60%">
                        &nbsp;
                    </td>
                    <td style="width: 40%; text-align: center; align-content: center">
                        <hr style="width: 100%" />
                        <span style="text-align: center">
                            <?php echo $model->funcionario; ?>
                            <br />
                            Inspector Actuante
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </div>



</page>