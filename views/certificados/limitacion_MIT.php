<?php

use app\models\Inspecciones;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<!-- BEGIN PAGE HEADER-->
<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Inspecciones de Personas <small> Gesti&oacute;n de la informaci&oacute;n de Certificados</small>
            <?php $this->endBlock(); ?>

            <?php $this->beginBlock('block_migas'); ?>
            <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-personas') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Productos y Servicios - </a><i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Certificados </a>
            </li>
            <li class="btn-group">
                <a href="<?php echo Url::to('/inspecciones/detallesProductos', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-primary">
                    <span style="color: #FFFFFF"><i class="icon-arrow-left"></i>Atras</span></a>
            </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Certtificado de Limitaci&oacute;n</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">

                <div class="container">
                    <div class="form-body">
                        <!-- BEGIN FORM-->
                        <?php
                        $form = ActiveForm::begin([
                            'id' => 'certificadoPersonasProvisional-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'htmlOptions' => ['enctype' => 'multipart/form-data']
                        ]);
                        ?>

                        <center>
                            <img src="<?= Yii::getAlias('@web/uploads') ?>/images/bannerHeader.jpg" width="80%">
                        </center>

                        <div class="row">
                            <div class="col-sm-12">
                                <center>
                                    <h2>
                                        <span style="font-variant: all-small-caps">
                                            CERTIFICADO DE LIMITACI&Oacute;N DEL MEDIO DE IZADO
                                        </span>
                                    </h2>
                                </center>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-8">&nbsp;</div>
                            <div class="col-md-4">
                                <h5>
                                    N&deg; de Control:
                                    <span style="text-decoration: underline;">
                                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <span style="font-size: 14px">
                                    <b>SE CERTIFICA QUE:</b><br /><br />
                                    <p align="justify">
                                        El medio de izado <?php echo $serv->medioIzado->nombre; ?> de <?php echo $serv->medioIzado->ci_nominal; ?> Kg
                                        de capacidad de izado nominal perteneciente a <?php echo $serv->medioIzado->propietario0->nombre; ?>,
                                        fue inspeccionado el d&iacute;a <?php echo Inspecciones::convertirFecha($serv->inspeccion0->getUltimaVisita()->fecha); ?>
                                        con el objetivo de realizar una <?php echo $serv->inspeccion0->tipoHomologacion->tipo; ?>.
                                    </p>
                                    <p align="justify">
                                        El medio de izado no obtendr&aacute; el Certificado de Seguridad T&eacute;cnica hasta que no se eliminen las no conformidades descritas
                                        en el Informe de Inspecci&oacute;n <b><?php echo $serv->inspeccion0->numero_control; ?></b> el cual se adjunta al presente "Certificado de Limitaci&oacute;n".
                                    </p>
                                    <p align="justify">
                                        El mismo podr&aacute; realizar operaciones de carga y descarga durante <b><i>3 meses</i></b> a partir de la fecha de emisi&oacute;n
                                        del presente documento y bajo las condiciones siguientes:
                                    </p>
                                </span>
                            </div>
                        </div>

                        <?php if (isset($model->limitacionIzado) && $model->limitacionIzado != null) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <span style="font-size: 14px">
                                        <p align="justify">
                                            <i>
                                                <b>
                                                    La Capacidad de Izado máxima de operación del medio de izado no deberá exceder
                                                    <?= $model->limitacionIzado ?> kilogramos
                                                </b>
                                            </i>
                                        </p>
                                    </span>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <span style="font-size: 14px">
                                        <p align="justify">
                                            <i>
                                                <b>
                                                    La Capacidad de Izado máxima de operación del medio de izado no deberá exceder:
                                                </b>
                                            </i>
                                        </p>
                                        <?php
                                        echo Html::activeTextInput($model, 'limitacionIzado', array('class' => 'form-control form-control input-small'));
                                        ?>
                                        <i><b>kilogramos</b></i>
                                    </span>
                                </div>
                            </div>
                        <?php } ?>


                        <br />

                        <?php if (isset($model->expedicion) && $model->expedicion != null) { ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Emitido en <?= $model->lugar ?>, el <?= $model->expedicion ?></h5>
                                    <h5>Vence: <?= $model->vencimiento ?></h5>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-6 align-left">
                                    <h5>Emitido en:</h5>
                                    <?php
                                    echo Html::activeTextInput($model, 'lugar', array('class' => 'form-control form-control input-medium'));
                                    ?>
                                </div>
                                <div class="col-sm-6 align-left">
                                    Fecha:
                                    <?php
                                    echo Html::activeTextInput($model, 'expedicion', array('class' => 'form-control form-control input-medium date-picker', 'data-date-format' => 'yyyy-mm-dd'));
                                    ?>
                                </div>
                            </div>
                        <?php } ?>

                        <br /><br /><br /><br />


                        <?php if (!isset($model->funcionario) || $model->funcionario == null) { ?>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <hr />
                                    </center>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <?php
                                        echo Html::activeDropDownList($model, 'funcionario', ArrayHelper::map($inspectores, 'nombre_apellidos', 'nombre_apellidos'), array('class' => 'form-control select2me', 'style' => 'width:200px !important', 'prompt' => 'Inspectores actuantes...'));
                                        ?>
                                    </center>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <hr />
                                    </center>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <?php echo $model->funcionario; ?><br />
                                        Inspector Actuante
                                    </center>
                                </div>
                            </div>
                        <?php } ?>


                        <div class="row">
                            <div class="col-sm-1">R20/IC403</div>
                            <div class="col-sm-11">&nbsp; </div>
                        </div>


                    </div>

                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-2">
                                <?php if ($model->control == 0) { ?>
                                    <button id="inspeccion_servicios_submit" type="submit" class="btn btn-primary"><i class="icon-ok"></i>Vista Previa del Certificado</button>
                                <?php } elseif ($model->control == 1) { ?>
                                    <a href="<?php echo Url::to('/certificados/PDFCertificadoLimitacion', array('o' => $serv->id)) ?>" class="btn btn-success"><i class="fa fa-file"></i> Generar Certificado</a>
                                    |
                                    <a href="<?php echo Url::to('/certificados/certificadoLimitacion', array('o' => $serv->id)) ?>" class="btn btn-danger"><i class="icon-repeat"></i> Corregir Certificado</a>
                                <?php } ?>
                                |
                                <a href="<?php echo Url::to('/inspecciones/detallesProductos', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));

?>

<?php
$this->registerJsFile('@web/plugins/jquery-validation/dist/jquery.validate.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-validation/dist/additional-methods.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/form-validation-entidades.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-print-page/jquery.printPage.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
FormComponents.certificado(); Custom.InitPrint(); 
JS;
$this->registerJs($js);
?>