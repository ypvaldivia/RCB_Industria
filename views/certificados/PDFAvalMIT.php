<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R2/IC405</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php

                            use yii\bootstrap4\Html;

                            echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 99%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    AVAL DE IMPORTACI&Oacute;N PARA MEDIOS DE IZADO TERRESTRES
                </h4>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 80%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                    </span>
                </h5>
            </td>
        </tr>
    </table>

    <p align="justify">
        Expedido en virtud de lo dispuesto en el inciso 1 (uno) del art&iacute;culo 71.1 (setenta y uno, punto 1)
        del REGLAMENTO PARA LA EXPLOTACI&Oacute;N DE MEDIOS DE IZADO (Resoluci&oacute;n No. 293/2014 MITRANS),
        con la facultad conferida por el Registro Cubano de Buques, nombrada como la Autoridad Competente del
        referido reglamento de acuerdo a su inciso 11 (once) del art&iacute;culo 7 (siete).
    </p>

    <div class="row">
        <div class="col-sm-12">
            <h5>
                <b>Datos de identificaci&oacute;n del medio de izado terrestre</b>
            </h5>
        </div>
    </div>

    <br />

    <?php $i = 1 ?>
    <table border="1" width="100%">
        <tr>
            <td style="text-align: left; width: 5%"><?= $i ?></td>
            <td style="text-align: left; width: 35%">Tipo de medio de izado </td>
            <td style="text-align: left; width: 60%"><?php echo $model->tipo; ?></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                    echo $i ?></td>
            <td style="text-align: left; width: 35%">Fabricante y modelo</td>
            <td style="text-align: left; width: 60%"><?php echo $model->objeto; ?></td>
        </tr>
        <tr>
            <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                    echo $i ?></td>
            <td style="text-align: left; width: 35%">Importador</td>
            <td style="text-align: left; width: 60%">
                <?= $model->importador; ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                    echo $i ?></td>
            <td style="text-align: left; width: 35%">Destinatario</td>
            <td style="text-align: left; width: 60%">
                <?= $model->destinatario; ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                    echo $i ?></td>
            <td style="text-align: left; width: 35%">Capacidad de trabajo nominal</td>
            <td style="text-align: left; width: 60%"><?php echo $model->capacidad; ?> Kg</td>
        </tr>
        <?php if ($model->vtraslacion != null) { ?>
            <tr>
                <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                        echo $i ?></td>
                <td style="text-align: left; width: 35%">Velocidad de traslaci&oacute;n</td>
                <td style="text-align: left; width: 60%">
                    <?= $model->vtraslacion; ?> m/s
                </td>
            </tr>
        <?php } ?>
        <?php if ($model->vizado != null) { ?>
            <tr>
                <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                        echo $i ?></td>
                <td style="text-align: left; width: 35%">Velocidad de izado</td>
                <td style="text-align: left; width: 60%">
                    <?= $model->vizado; ?> m/s
                </td>
            </tr>
        <?php } ?>
        <?php if ($model->vdescenso != null) { ?>
            <tr>
                <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                        echo $i ?></td>
                <td style="text-align: left; width: 35%">Velocidad de descenso</td>
                <td style="text-align: left; width: 60%">
                    <?= $model->vdescenso; ?> m/s
                </td>
            </tr>
        <?php } ?>
        <?php if ($model->vgiro != null) { ?>
            <tr>
                <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                        echo $i ?></td>
                <td style="text-align: left; width: 35%">Velocidad de giro</td>
                <td style="text-align: left; width: 60%">
                    <?= $model->vgiro; ?>
                </td>
            </tr>
        <?php } ?>
        <?php if ($model->htrabajo != null) { ?>
            <tr>
                <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                        echo $i ?></td>
                <td style="text-align: left; width: 35%">Altura m&aacute;xima de trabajo</td>
                <td style="text-align: left; width: 60%">
                    <?= $model->htrabajo; ?> metros
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                    echo $i ?></td>
            <td style="text-align: left; width: 35%">N&uacute;mero de unidades</td>
            <td style="text-align: left; width: 60%">
                <?= (isset($model->nunidades) && $model->nunidades != null) ? $model->nunidades : Html::activeTextInput($model, 'nunidades', array('class' => 'form-control input-small', 'placeholder' => '#')); ?>
            </td>
        </tr>
    </table>

    <br />

    <div class="row">
        <div class="col-sm-12">
            <p align="justify">
                El presente Aval es v&aacute;lido para importar el tipo de medio de izado en la cantidad que est&aacute; relacionada en
                el inciso <?= $i ?> de la tabla anterior.
            </p>
            <p align="justify">
                El Aval, no garantiza la calidad de las piezas, agregados, dispositivos, u otros elementos que componen
                el medio de izado, as&iacute; como el nivel de calidad del propio medio
            </p>
            <p align="justify">
                La emisi&oacute;n del Aval no implica que el medio de izado se encuentra apto para ser operado, esta condici&oacute;n
                se verificar&aacute; al someterse el medio al cumplimiento de los establecido en la Resoluci&oacute;n 293/2014 del
                MITRANS en cuanto a la entrega del Certificado de Seguridad T&eacute;cnica.
            </p>
        </div>
    </div><br />

    <br />

    <div class="row">
        <div class="col-sm-12">
            <h5>
                Fecha Expedici&oacute;n:
                <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
            </h5>
        </div>
    </div>

    <br />

    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php
                    if ($model->nivelAprobacion != null) {
                        //Validar aqui que el funcionario este activo
                        if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                            echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                        } else {
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                        }
                    }
                    ?>
                </span>
            </td>
        </tr>
    </table>

</page>