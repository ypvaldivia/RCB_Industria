<?php

use app\models\Inspecciones;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<!-- BEGIN PAGE HEADER-->
<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Inspecciones de Personas <small> Gesti&oacute;n de la informaci&oacute;n de Certificados</small>
            <?php $this->endBlock(); ?>

            <?php $this->beginBlock('block_migas'); ?>
            <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-dacs') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Dispositivos Auxiliares de Carga - </a><i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Certificados </a>
            </li>
            <li class="btn-group">
                <a href="<?php echo Url::to('/inspecciones/detallesDac', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-primary">
                    <span style="color: #FFFFFF"><i class="icon-arrow-left"></i>Atras</span></a>
            </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Certtificado de Inspecci&oacute;n</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">

                <div class="container">
                    <div class="form-body">
                        <!-- BEGIN FORM-->
                        <?php
                        $form = ActiveForm::begin([
                            'id' => 'certificadoPersonasProvisional-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'htmlOptions' => ['enctype' => 'multipart/form-data']
                        ]);
                        ?>

                        <center>
                            <img src="<?= Yii::getAlias('@web/uploads') ?>/images/bannerHeader.jpg" width="80%">
                        </center>

                        <div class="row">
                            <div class="col-sm-12">
                                <center>
                                    <h2>
                                        <span style="font-variant: all-small-caps">
                                            CERTIFICADO DE SEGURIDAD T&Eacute;CNICA PROVISIONAL
                                        </span>
                                    </h2>
                                </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-10">&nbsp;</div>
                            <div class="col-sm-2 float-left">
                                <h5>
                                    N&deg;:
                                    <span style="text-decoration: underline">
                                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    Certificado de Seguridad T&eacute;cnica para los Dispositivos Auxiliares de Carga,
                                    expedido en virtud de los dispuesto en
                                    la Resoluci&oacute;n No. 293/2014 del MITRANS "Reglamento para la Explotaci&oacute;n de Medios de
                                    Izado" y el Convenio 152 de la Organizaci&oacute;n Internacional del Trabajo.
                                </p>
                            </div>
                        </div>

                        <br />

                        <table border="1" width="100%">
                            <tr>
                                <td style="text-align: center; width: 40%">Tipo de dispositivo auxiliar de carga </td>
                                <td style="text-align: center; width: 30%">Identificaci&oacute;n del dispositivo</td>
                                <td style="text-align: center; width: 30%">Propietario </td>
                            </tr>
                            <tr>
                                <td style="text-align: center">I </td>
                                <td style="text-align: center">II</td>
                                <td style="text-align: center">III </td>
                            </tr>
                            <tr>
                                <td style="text-align: center"><?php echo $serv->dac0->tipo0->tipo; ?> </td>
                                <td style="text-align: center"><?php echo $serv->dac0->nombre; ?> </td>
                                <td style="text-align: center"><?php if ($serv->dac0->propietario0 != null) {
                                                                    echo $serv->dac0->propietario0->nombre;
                                                                } else {
                                                                    echo "Sin definir";
                                                                } ?> </td>
                            </tr>
                        </table>

                        <br />

                        <table border="1" width="100%">
                            <tr>
                                <td style="text-align: center; width: 25%">Fecha de la inspecci&oacute;n </td>
                                <td style="text-align: center; width: 25%">Lugar de realizaci&oacute;n de la inspecci&oacute;n</td>
                                <td style="text-align: center; width: 25%">Carga de prueba <br /><small>(kilogramos)</small> </td>
                                <td style="text-align: center; width: 25%">Capacidad de trabajo certificada <br /><small>(kilogramos)</small></td>
                            </tr>
                            <tr>
                                <td style="text-align: center">IV </td>
                                <td style="text-align: center">V</td>
                                <td style="text-align: center">VI </td>
                                <td style="text-align: center">VII </td>
                            </tr>
                            <tr>
                                <td style="text-align: center"><?php echo $serv->inspeccion0->getUltimaVisita()->fecha; ?> </td>
                                <td style="text-align: center; align-content: center"><?php echo $serv->inspeccion0->lugar_inspeccion; ?> </td>
                                <td style="text-align: center; align-content: center">
                                    <h5>PRUEBA EST&Aacute;TICA</h5>
                                    <hr />
                                    <center>
                                        <?php
                                        if (isset($model->carga_pp_est) && $model->carga_pp_est != null) {
                                            echo $model->carga_pp_est;
                                        } elseif (!$model->flag) { ?>
                                            <?= Html::activeTextInput($model, 'carga_pp_est', array('class' => 'form-control input-small', 'placeholder' => 'Principal')); ?>
                                        <?php } ?>
                                    </center>
                                    <br /><br />
                                    <h5>PRUEBA DIN&Aacute;MICA</h5>
                                    <hr />
                                    <center>
                                        <?php
                                        if (isset($model->carga_pp_din) && $model->carga_pp_din != null) {
                                            echo $model->carga_pp_din;
                                        } elseif (!$model->flag) {  ?>
                                            <?= Html::activeTextInput($model, 'carga_pp_din', array('class' => 'form-control input-small', 'placeholder' => 'Principal')); ?>
                                        <?php } ?>
                                    </center>
                                    <br />
                                </td>
                                <td style="text-align: center; align-content: center">
                                    <center>
                                        <?php
                                        if (isset($model->ci_pp_certificada) && $model->ci_pp_certificada != null) {
                                            echo $model->ci_pp_certificada;
                                        } elseif (!$model->flag) {  ?>
                                            <?= Html::activeTextInput($model, 'ci_pp_certificada', array('class' => 'form-control input-small', 'placeholder' => 'Principal')); ?>
                                        <?php } ?>
                                    </center>
                                </td>
                            </tr>
                        </table>

                        <br />

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    CERTIFICO QUE:
                                    el <?php echo Inspecciones::convertirFecha($serv->inspeccion0->getUltimaVisita()->fecha); ?>, el dispositivo auxiliar de carga descrito,
                                    fue inspeccionado por una persona competente acreditada y que el
                                    examen al que fue sometido, despu&eacute;s de las correspondientes pruebas,
                                    dio como resultado que hubo de resistir la carga de prueba, sin sufrir
                                    da&ntilde;os, ni deformaciones residuales y que su capacidad de izado certificada
                                    es la que se indica en la columna VII.
                                </p>
                            </div>
                        </div><br />

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    El presente certificado tendr&aacute; una vigencia de <strong>cinco (5)</strong> meses a partir de su expedici&oacute;n,
                                    hasta que sea sustituido por un Certificado de Homologaci&oacute;n definitivo,
                                    que expedir&aacute; la Oficina de Inspecci&oacute;n del
                                    <img src="<?= Yii::getAlias('@web/themes/sbadmin') ?>/assets/img/certificados_inspecciones/logo-cuerpo-cert-rcb.jpg" height="19px">.
                                </p>
                                <p align="justify">
                                    Este Certificado no es v&aacute;lido para realizar operaciones que impliquen la carga de personal.
                                </p>
                            </div>
                        </div><br />

                        <?php if (isset($model->expedicion) && $model->expedicion != null) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                        <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                                    </h5>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-2">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                    </h5>
                                </div>
                                <div class="col-sm-10">
                                    <?php echo Html::activeTextInput($model, 'expedicion', array('class' => 'form-control form-control input-medium date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                                </div>
                            </div>
                        <?php } ?>


                        <br /><br />

                        <?php if (!isset($model->funcionario) || $model->funcionario == null) { ?>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <hr />
                                    </center>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <?php
                                        echo Html::activeDropDownList($model, 'funcionario', ArrayHelper::map($inspectores, 'nombre_apellidos', 'nombre_apellidos'), array('class' => 'form-control select2me', 'style' => 'width:200px !important', 'prompt' => 'Inspectores actuantes...'));
                                        ?>
                                    </center>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <hr />
                                    </center>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <?php echo $model->funcionario; ?><br />
                                        Inspector Actuante
                                    </center>
                                </div>
                            </div>
                        <?php } ?>


                        <div class="row">
                            <div class="col-sm-1">R2/IC404</div>
                            <div class="col-sm-11">&nbsp; </div>
                        </div>


                    </div>

                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-2">
                                <?php if ($model->control == 0) { ?>
                                    <button id="inspeccion_servicios_submit" type="submit" class="btn btn-primary"><i class="icon-ok"></i>Vista Previa del Certificado</button>
                                <?php } elseif ($model->control == 1) { ?>
                                    <a href="<?php echo Url::to('/certificados/PDFCertificadoDacProvisional', array('o' => $serv->id)) ?>" class="btn btn-success"><i class="fa fa-file"></i> Generar Certificado</a>
                                    |
                                    <a href="<?php echo Url::to('/certificados/certificado', array('obj' => $serv, 'tipoObj' => 4, 'tipoCe' => 1)) ?>" class="btn btn-danger"><i class="icon-repeat"></i> Corregir Certificado</a>
                                <?php } ?>
                                |
                                <a href="<?php echo Url::to('/inspecciones/detallesDac', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));

?>

<?php
$this->registerJsFile('@web/plugins/jquery-validation/dist/jquery.validate.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-validation/dist/additional-methods.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/form-validation-entidades.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-print-page/jquery.printPage.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
FormComponents.certificado(); Custom.InitPrint(); 
JS;
$this->registerJs($js);
?>