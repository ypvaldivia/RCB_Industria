<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php
$first = is_array($serv) ? $serv[0] : $serv;
?>

<!-- BEGIN PAGE HEADER-->
<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Inspecciones de Personas <small> Gesti&oacute;n de la informaci&oacute;n de Certificados</small>
            <?php $this->endBlock(); ?>

            <?php $this->beginBlock('block_migas'); ?>
            <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/index-dacs') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Dispositivos Auxiliares de Carga - </a><i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Certificados </a>
            </li>
            <li class="btn-group">
                <a href="<?php echo Url::to('/inspecciones/detallesDac', array('id' => $first->inspeccion0->id)) ?>" class="btn btn-primary">
                    <span style="color: #FFFFFF"><i class="icon-arrow-left"></i>Atras</span></a>
            </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Certtificado de Inspecci&oacute;n</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">

                <div class="container">
                    <div class="form-body">
                        <!-- BEGIN FORM-->
                        <?php
                        $form = ActiveForm::begin([
                            'id' => 'certificadoPersonasProvisional-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'htmlOptions' => array(
                                'enctype' => 'multipart/form-data',
                            ),
                        ]);
                        ?>

                        <center>
                            <img src="<?= Yii::getAlias('@web/uploads') ?>/images/bannerHeader.jpg" width="80%">
                        </center>

                        <div class="row">
                            <div class="col-sm-12">
                                <center>
                                    <h2>
                                        <span style="font-variant: all-small-caps">
                                            CERTIFICADO DE HOMOLOGACI&Oacute;N<br />
                                            <small><i>CERTIFICATE OF APPROVAL</i></small>
                                        </span>
                                    </h2>
                                </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-10">&nbsp;</div>
                            <div class="col-sm-2 float-left">
                                <h5>
                                    N&deg;:
                                    <span style="text-decoration: underline">
                                        <?php echo $first->inspeccion0->numero_control; ?>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <h5>
                                    <b>QUE SE OTORGA A:</b><br />
                                    <small><b><i>Granted to:</i></b></small>
                                    <span style="text-decoration: underline">
                                        <?php
                                        echo '<br/>';
                                        echo '<center>';
                                        echo '<b>' . $first->dac0->propietario0->nombre . '</b>';
                                        echo '<br/>';
                                        echo $first->dac0->propietario0->direccion;
                                        echo '</center>';
                                        ?>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    Por obtener resultados satisfactorios en las inspecciones realizadas en el proceso de
                                    homologaci&oacute;n y cumplir con los requisitos del RCB Sociedad Clasificadora especificados en
                                    P C4-01 "Procedimiento para la evaluaci&oacute;n de la conformidad de productos, servicios y
                                    personal calificado" en el cap&iacute;tulo 1.1 "Generalidades" del "Manual de aprobaci&oacute;n de
                                    productos, servicios y personal calificado (MAPS)" y en la Resoluci&oacute;n No. 293-2014, MITRANS:
                                    "Reglamento para la explotaci&oacute;n de medios de izado"
                                </p>
                                <p align="justify">
                                    <i>
                                        For getting satisfactory results in the surveys carried out in the process of approval and complying
                                        with the requirements of RCB Sociedad Clasificadora specified in
                                        P C4-01 "Procedure for the assesment of the conformity of products, services and qualified personnel",
                                        in the chapter 1.1 "General" of the "Manual of approval of of products, services and qualified personnel of RCB (MAPS)"
                                        and in the Resolution No. 293-2014, MITRANS:
                                        "Regulation for the operation of hoisting means"
                                    </i>
                                </p>
                            </div>
                        </div>

                        <br />

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    Se le otorga el presente Certificado al producto "<b>Dispositivo Auxiliar de Carga</b>" del tipo:<br />
                                    <small><i>This certificate is granted to the product "Auxiliary Loading Device" of the type:</i></small>
                                </p>
                                <table border="1" width="100%">
                                    <tr>
                                        <td style="text-align: center; width: 5%">Nº </td>
                                        <td style="text-align: center; width: 20%">Tipo</td>
                                        <td style="text-align: center; width: 55%">Modelo </td>
                                        <td style="text-align: center; width: 25%">Capacidad de trabajo nominal <br /><small>(kilogramos)</small></td>
                                    </tr>
                                    <?php
                                    $i = 1;
                                    foreach ($serv as $d) {
                                        if ($d->aprobado == 1) {
                                    ?>
                                            <tr>
                                                <td style="text-align: center"><?php echo $i; ?> </td>
                                                <td style="text-align: center; align-content: center"><?php echo ($d->dac0->tipo0 != null) ? $d->dac0->tipo0->tipo : ""; ?> </td>
                                                <td style="text-align: center; align-content: center"><?php echo $d->dac0->nombre; ?> </td>
                                                <td style="text-align: center; align-content: center"><?php echo $d->dac0->ct_nominal; ?></td>
                                            </tr>
                                    <?php $i++;
                                        }
                                    } ?>
                                </table>
                            </div>
                        </div>

                        <br />

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    El presente Certificado se expide en base al Informe de Inspección N&deg;: &nbsp;
                                    <span style="text-decoration: underline; font-weight: bold">
                                        <?= $first->inspeccion0->numero_control; ?>
                                    </span>
                                </p>
                                <p align="justify">
                                    <i>
                                        This certificate is issued base on the Survey report N&deg;: &nbsp;
                                        <span style="text-decoration: underline; font-weight: bold">
                                            <?= $first->inspeccion0->numero_control; ?>
                                        </span>
                                    </i>
                                </p>
                            </div>
                        </div>

                        <br />

                        <?php if (isset($model->expedicion) && $model->expedicion != null) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                        <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                                    </h5>
                                    <h6 style="font-style: italic">
                                        Issue date:
                                        <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                                    </h6>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-2">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                    </h5>
                                </div>
                                <div class="col-sm-10">
                                    <?php echo Html::activeTextInput($model, 'expedicion', array('class' => 'form-control form-control input-medium date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (isset($model->vencimiento) && $model->vencimiento != null) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>
                                        Fecha Vencimiento:
                                        <span style="text-decoration: underline"><?php echo $model->vencimiento; ?></span>
                                    </h5>
                                    <h6 style="font-style: italic">
                                        Expiration date:
                                        <span style="text-decoration: underline"><?php echo $model->vencimiento; ?></span>
                                    </h6>
                                </div>
                            </div>
                        <?php } ?>

                        <br /><br />

                        <div class="row">
                            <div class="col-sm-8">&nbsp;</div>
                            <div class="col-sm-4">
                                <center>
                                    <hr />
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">&nbsp;</div>
                            <div class="col-sm-4">
                                <center>
                                    <?php
                                    if ($model->nivelAprobacion != null) {
                                        //Validar aqui que el funcionario este activo
                                        if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                                            echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                                            echo '<br/>';
                                            echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                                        } else {
                                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                                            echo '<br/>';
                                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                                        }
                                    }
                                    ?>
                                </center>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-1">R2/IC404</div>
                            <div class="col-sm-11">&nbsp; </div>
                        </div>


                    </div>

                </div>

                <hr />
                <center>
                    <h3>CONDICIONES GENERALES</h3>
                </center>
                <div class="row">
                    <div class="col-md-12">
                        <ol>
                            <li>
                                <p align="justify">
                                    El dispositivo auxiliar de carga podr&aacute; ofrecer servicio durante el tiempo de vigencia del Certificado (1 a&ntilde;o).
                                    Se excluyen las eslingas de cable de acero, de material textil y sint&eacute;tico que ser&aacute; de 6 meses.
                                </p>
                            </li>
                            <li>
                                <p align="justify">
                                    Durante el per&iacute;odo de validez del Certificado, si el propietario deseara realizar cualquier modificaci&oacute;n,
                                    cambio, reparaci&oacute;n al dispositivo auxiliar de carga, deber&aacute; informarlo al RCB, previa materializaci&oacute;n
                                    de la actividad.
                                </p>
                            </li>
                            <li>
                                <p align="justify">
                                    Los cables de acero deber&aacute;n ser rechazados por el propietario, en alguna de las siguientes circunstancias:
                                    <ul>
                                        <li>
                                            La disminuci&oacute;n de la secci&oacute;n de un cord&oacute;n, medido sobre un paso de cableado, alcanza el 40% de la secci&oacute;n total del cord&oacute;n.
                                        </li>
                                        <li>
                                            La disminuci&oacute;n del di&aacute;metro del cable en un punto cualquiera, alcanza el 10% de su di&aacute;metro nominal.
                                        </li>
                                        <li>
                                            La p&eacute;rdida de secci&oacute;n del cable por rotura visible de sus alambres, contados sobre una longitud de dos pasos de cableado, alcanza el 20% de la secci&oacute;n total del cable.
                                        </li>
                                    </ul>
                                </p>
                            </li>
                            <li>
                                <p align="justify">
                                    Las eslingas de cables de acero deber&aacute;n ser rechazadas por el propietario, en las siguientes circunstancias:
                                    <ul>
                                        <li>
                                            Al presentar las no conformidades enumeradas en el ac&aacute;pite 3
                                        </li>
                                        <li>
                                            Al presentar los extremos de la gaza un aplastamiento tal que sea al alma textil met&aacute;lica, as&iacute; como alambres partidos.
                                        </li>
                                    </ul>
                                </p>
                            </li>
                            <li>
                                <p align="justify">
                                    El Certificado perder&aacute; su vigencia si se incumplen las condiciones precitadas.
                                </p>
                            </li>
                        </ol>
                    </div>
                </div>

                <center>
                    <h3>RENOVACI&Oacute;N DEL CERTIFICADO</h3>
                </center>
                <br /><br />
                <div class="row">
                    <div class="col-md-2">
                        <b>Primera Renovaci&oacute;n:</b>
                    </div>
                    <div class="col-md-2">
                        <hr />
                        <center>Fecha</center>
                    </div>
                    <div class="col-md-3">
                        <hr />
                        <center>Firma del Inspector</center>
                    </div>
                    <div class="col-md-2">
                        <hr />
                        <center>N&deg; de Control</center>
                    </div>
                    <div class="col-md-3">
                        <hr />
                        <center>Cu&ntilde;o del Inspector</center>
                    </div>
                </div>
                <br /><br /><br />
                <div class="row">
                    <div class="col-md-2">
                        <b>Segunda Renovaci&oacute;n:</b>
                    </div>
                    <div class="col-md-2">
                        <hr />
                        <center>Fecha</center>
                    </div>
                    <div class="col-md-3">
                        <hr />
                        <center>Firma del Inspector</center>
                    </div>
                    <div class="col-md-2">
                        <hr />
                        <center>N&deg; de Control</center>
                    </div>
                    <div class="col-md-3">
                        <hr />
                        <center>Cu&ntilde;o del Inspector</center>
                    </div>
                </div>

                <?php
                $objs = array();
                $r = null;
                if (is_array($serv)) {
                    foreach ($serv as $m) {
                        array_push($objs, $m->id);
                    }
                    $r = serialize($objs);
                } else {
                    $r = $serv->id;
                }
                ?>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-2">
                                <?php if ($model->control == 0) { ?>
                                    <button id="inspeccion_servicios_submit" type="submit" class="btn btn-primary"><i class="icon-ok"></i>Vista Previa del Certificado</button>
                                <?php } elseif ($model->control == 1) { ?>
                                    <a href="<?php echo Url::to('/certificados/PDFCertificadoDac', array('o' => $r)) ?>" class="btn btn-success"><i class="fa fa-file"></i> Generar Certificado</a>
                                    |
                                    <a href="<?php echo Url::to('/certificados/certificado', array('obj' => $r, 'tipoObj' => 4, 'tipoCe' => 2)) ?>" class="btn btn-danger"><i class="icon-repeat"></i> Corregir Certificado</a>
                                <?php } ?>
                                |
                                <a href="<?php echo Url::to('/inspecciones/detallesDac', array('id' => $first->inspeccion0->id)) ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/jquery-validation/dist/jquery.validate.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-validation/dist/additional-methods.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/form-validation-entidades.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-print-page/jquery.printPage.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
FormComponents.certificado(); Custom.InitPrint(); 
JS;
$this->registerJs($js);
?>