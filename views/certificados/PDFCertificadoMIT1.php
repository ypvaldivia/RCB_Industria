<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R17/IC403</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php

                            use app\models\Inspecciones;

                            echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 99%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CERTIFICADO DE SEGURIDAD T&Eacute;CNICA DEFINITIVO<br />
                    PARA MONTACARGAS Y TRASPALETA
                </h4>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 80%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                    </span>
                </h5>
            </td>
        </tr>
    </table>

    <p align="justify">
        Certificado de Seguridad T&eacute;cnica para Montacargas y Traspaleta, expedido en virtud de los dispuesto en
        la Resoluci&oacute;n No. 293/2014 del MITRANS "Reglamento para la Explotaci&oacute;n de Medios de
        Izado" y el Convenio 152 de la Organizaci&oacute;n Internacional del Trabajo.
    </p>

    <div class="row">
        <div class="col-sm-12">
            <h5>
                <b>NOMBRE DEL EQUIPO:</b>
                <span style="text-decoration: underline">
                    <?php echo $serv->medioIzado->tipo0->tipo; ?>
                </span>
            </h5>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <h5>
                <b>PROPIETARIO:</b>
                <span style="text-decoration: underline">
                    <?php echo $serv->medioIzado->propietario0->nombre; ?>
                </span>
            </h5>
        </div>
    </div>

    <br />

    <table border="1" width="100%">
        <tr>
            <td style="text-align: center; max-width: 40%">Descripci&oacute;n del equipo <br /><small>(marca y su n&uacute;mero distintivo)</small> </td>
            <td style="text-align: center; max-width: 30%">Carga de prueba <br /><small>(kilogramos)</small> </td>
            <td style="text-align: center; max-width: 30%">Capacidad de Izado Certificada <br /><small>(kilogramos)</small></td>
        </tr>
        <tr>
            <td style="text-align: center">I </td>
            <td style="text-align: center">II</td>
            <td style="text-align: center">III </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <?php
                if (strchr($serv->medioIzado->nombre, ',')) {
                    $sttemp = str_getcsv($serv->medioIzado->nombre);
                    foreach ($sttemp as $s) {
                        echo $s;
                        echo '<br/>';
                    }
                } else {
                    $sttemp = str_split($serv->medioIzado->nombre, 24);
                    foreach ($sttemp as $s) {
                        echo $s;
                        echo '<br/>';
                    }
                }
                ?>
            </td>
            <td style="text-align: center">
                PRUEBA EST&Aacute;TICA<br />
                Principal: <?= $model->carga_pp_est; ?><br />
                <?php if (isset($model->carga_aux_est) && $model->carga_aux_est != null) { ?>
                    Auxiliar 1: <?= $model->carga_aux_est; ?><br />
                <?php } ?>
                <?php if (isset($model->carga_aux1_est) && $model->carga_aux1_est != null) { ?>
                    Auxiliar 2: <?= $model->carga_aux1_est; ?><br />
                <?php } ?>
                <br />
                PRUEBA DIN&Aacute;MICA<br />
                Principal: <?= $model->carga_pp_din; ?><br />
                <?php if (isset($model->carga_aux_din) && $model->carga_aux_din != null) { ?>
                    Auxiliar 1: <?= $model->carga_aux_din; ?><br />
                <?php } ?>
                <?php if (isset($model->carga_aux1_din) && $model->carga_aux1_din != null) { ?>
                    Auxiliar 2: <?= $model->carga_aux1_din; ?><br />
                <?php } ?>
            </td>
            <td style="text-align: center">
                Principal: <?php echo $model->ci_pp_certificada; ?><br />
                <?php if (isset($model->ci_aux_certificada) && $model->ci_aux_certificada != null) { ?>
                    Auxiliar 1: <?= $model->ci_aux_certificada; ?><br />
                <?php } ?>
                <?php if (isset($model->ci_aux1_certificada) && $model->ci_aux1_certificada != null) { ?>
                    Auxiliar 2: <?= $model->ci_aux1_certificada; ?><br />
                <?php } ?>
            </td>
        </tr>
    </table>

    <br />

    <p align="justify">
        CERTIFICO QUE:
        el <?php echo Inspecciones::convertirFecha($serv->inspeccion0->getUltimaVisita()->fecha); ?>, el medio de izado terrestre descrito,
        fue inspeccionado por una persona competente acreditada y que el
        examen al que fue sometido, despu&eacute;s de las correspondientes pruebas,
        dio como resultado que hubo de resistir la carga de prueba, sin sufrir
        da&ntilde;os, ni deformaciones residuales y que su capacidad de izado certificada
        es la que se indica en la columna III.
    </p>

    <br />
    <div class="row">
        <div class="col-sm-12">
            <h5>
                Fecha Expedici&oacute;n:
                <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                <br /><br />
                Fecha Vencimiento:
                <span style="text-decoration: underline"><?php echo $model->vencimiento; ?></span>
            </h5>
        </div>
    </div>

    <br />

    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php
                    if ($model->nivelAprobacion != null) {
                        //Validar aqui que el funcionario este activo
                        if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                            echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                        } else {
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                        }
                    }
                    ?>
                </span>
            </td>
        </tr>
    </table>

</page>

<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R17/IC403</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <br /><br />
    <table style="border: 1px">
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CONDICIONES GENERALES
                </h4>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter; justify-content: center">
                <ol>
                    <li style="text-align: justify">
                        La capacidad de izado certificada que es la indicada en la columna III,
                        es la m&aacute;xima carga de trabajo a operar.
                    </li>
                    <li style="text-align: justify">
                        El medio de izado podr&aacute; ofrecer servicio durante el tiempo de vigencia del Certificado (3 a&ntilde;os),
                        siempre que se someta a dos inspecciones peri&oacute;dicas anuales por el RCB con resultados satisfactorios,
                        durante el per&iacute;odo de explotaci&oacute;n autorizado. Las inspecciones anuales se realizar&aacute;n, a
                        solicitud del Cliente, dentro del mes anterior a la fecha aniversario de la expedici&oacute;n de los correspondientes
                        Certificados de Seguridad T&eacute;cnica, para la confirmaci&oacute;n de la validez de los mismos.
                    </li>
                    <li style="text-align: justify">
                        El medio de izado se someter&aacute; a la inspecci&oacute;n de renovaci&oacute;n por el RCB al terminar la vigencia
                        del Certificado. Si el resultado fuera satisfactorio, el RCB le expedir&aacute; al propietario un nuevo Certificado
                        de Seguridad T&eacute;cnica por 3 a&ntilde;os.
                    </li>
                    <li style="text-align: justify">
                        Durante el per&iacute;odo de validez del Certificado, si el propietario deseara realizar cualquier modificaci&oacute;n,
                        cambio, reparaciones mayores o capitales al medio de izado, deber&aacute; informarlo al RCB, previa materializaci&oacute;n
                        de la actividad.
                    </li>
                    <li style="text-align: justify">
                        Durante el per&iacute;odo de validez del Certificado, si el medio de izado sufre alguna aver&iacute;a en el mecanismo de izado,
                        giro, cambio de voladizo, brazo o cambio de cables de acero (amante o amantillo), deber&aacute; ser informado al RCB.
                    </li>
                    <li style="text-align: justify">
                        Este Certificado no es v&aacute;lido para realizar operaciones que impliquen la carga de personas.
                    </li>
                    <li style="text-align: justify">
                        El Certificado perder&aacute; su vigencia si se incumplen las condiciones precitadas.
                    </li>
                </ol>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CONFIRMACI&Oacute;N DE LAS INSPECCIONES ANUALES
                </h4>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr style="text-align: center; font-weight: lighter">
                        <td style="width: 20%; text-align: center">Primera Anual</td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Fecha
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Firma del Inspector
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />N&deg; de Control
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Cu&ntilde;o del Inspector
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr style="text-align: center; font-weight: lighter">
                        <td style="width: 20%; text-align: center">Segunda Anual</td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Fecha
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Firma del Inspector
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />N&deg; de Control
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Cu&ntilde;o del Inspector
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</page>