<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Inspecciones de Personas <small> Gesti&oacute;n de la informaci&oacute;n de Certificados</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="icon-barcode"></i>
    <a href="<?php echo Url::to('/inspecciones/index-personas') ?>">Inspecciones - </a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Personas - </a><i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Certificados </a>
</li>
<li class="btn-group">
    <a href="<?php echo Url::to('/inspecciones/detalles-personas', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-primary">
        <span style="color: #FFFFFF"><i class="icon-arrow-left"></i>Atras</span></a>
</li><?php $this->endBlock(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Certtificado de Inspecci&oacute;n</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">

                <div class="container">
                    <div class="form-body">

                        <center>
                            <img src="<?= Yii::getAlias('@web/uploads') ?>/images/bannerHeader.jpg" width="80%">
                        </center>

                        <div class="row">
                            <div class="col-sm-12">
                                <center>
                                    <h2>
                                        <span style="font-variant: all-small-caps">
                                            CERTIFICADO DE HOMOLOGACI&Oacute;N DE
                                            <?php echo strtoupper($serv->inspeccion0->homologacion0->titulo_anuario); ?><br />
                                            PROVISIONAL
                                        </span>
                                    </h2>
                                </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-9">&nbsp;</div>
                            <div class="col-sm-3 float-left">
                                <h5>
                                    N&deg;:
                                    <span style="text-decoration: underline">
                                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <h5>
                                    Que se otorga al Sr.:
                                    <span style="text-decoration: underline"><?php echo $serv->persona0->nombre_apellidos; ?></span>

                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <h5>
                                    N&deg; de Identidad Permanente:
                                    <span style="text-decoration: underline"><?php echo $serv->persona0->ci; ?></span>

                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <h5>
                                    De la Entidad:
                                    <span style="text-decoration: underline"><?php echo $serv->inspeccion0->entidad0->nombre; ?></span>
                                </h5>
                            </div>
                        </div>

                        <br />
                        <!-- BEGIN FORM-->
                        <?php
                        $form = ActiveForm::begin([
                            'id' => 'certificadoPersonasProvisional-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'htmlOptions' => ['enctype' => 'multipart/form-data']
                        ]);
                        ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    Por aprobar de forma satisfactoria la evaluaci&oacute;n para la homologaci&oacute;n de
                                    <span style="text-decoration: underline"><?php echo $serv->inspeccion0->homologacion0->titulo_anuario; ?></span>
                                    y cumplir con los requisitos del
                                    <img src="<?= Yii::getAlias('@web/themes/sbadmin') ?>/assets/img/certificados_inspecciones/logo-cuerpo-cert-rcb.jpg" height="19px">
                                    especificados en
                                    <?php
                                    if (isset($model->servicio) && $model->servicio != null) {
                                        echo $model->servicio;
                                    } else {
                                        echo Html::activeTextArea($model, 'servicio', array('class' => 'form-control'));
                                    }
                                    ?>
                                    para realizar los servicios de:
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php
                                    if (isset($model->documentacion) && $model->documentacion != null) {
                                        echo $model->documentacion;
                                    } else {
                                        echo Html::activeTextArea($model, 'documentacion', array('class' => 'ckeditor form-control'));
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    El presente certificado tendrá una vigencia de <strong>cinco (5)</strong> meses a partir de su expedición,
                                    hasta que sea sustituido por un Certificado de Homologación definitivo,
                                    que expedirá la Oficina de Inspección del
                                    <img src="<?= Yii::getAlias('@web/themes/sbadmin') ?>/assets/img/certificados_inspecciones/logo-cuerpo-cert-rcb.jpg" height="19px">.
                                </p>
                            </div>
                        </div><br />

                        <?php if (isset($model->expedicion) && $model->expedicion != null) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                        <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                                    </h5>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-2">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                    </h5>
                                </div>
                                <div class="col-sm-10">
                                    <?php echo Html::activeTextInput($model, 'expedicion', array('class' => 'form-control form-control input-medium date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <h5>
                                    Fecha de Vencimiento: <i>5 meses (hasta la emisi&oacute;n del Certificado definitivo)</i>
                                </h5>
                            </div>
                        </div>

                        <br /><br />

                        <?php if (!isset($model->funcionario) || $model->funcionario == null) { ?>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <hr />
                                    </center>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <?php
                                        echo Html::activeDropDownList($model, 'funcionario', ArrayHelper::map($inspectores, 'nombre_apellidos', 'nombre_apellidos'), ['class' => 'form-control select2me', 'style' => 'width:200px !important', 'prompt' => 'Inspectores actuantes...']);
                                        ?>
                                    </center>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <hr />
                                    </center>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">&nbsp;</div>
                                <div class="col-sm-4">
                                    <center>
                                        <?php echo $model->funcionario; ?>
                                    </center>
                                </div>
                            </div>
                        <?php } ?>


                        <div class="row">
                            <div class="col-sm-1">R5/PC401</div>
                            <div class="col-sm-11">&nbsp; </div>
                        </div>


                    </div>

                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-2">
                                <?php if ($model->control == 0) { ?>
                                    <button id="inspeccion_servicios_submit" type="submit" class="btn btn-primary"><i class="icon-ok"></i>Vista Previa del Certificado</button>
                                <?php } elseif ($model->control == 1) { ?>
                                    <a href="<?php echo Url::to('/certificados/PDFCertificadoPersonasDefaultProvisional', array('o' => $serv->id)) ?>" class="btn btn-success"><i class="fa fa-file"></i> Generar Certificado</a>
                                    |
                                    <a href="<?php echo Url::to('/certificados/certificado', array('obj' => $serv, 'tipoObj' => 1, 'tipoCe' => 1)) ?>" class="btn btn-danger"><i class="icon-repeat"></i> Corregir Certificado</a>
                                <?php } ?>
                                |
                                <a href="<?php echo Url::to('/inspecciones/detalles-personas', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));

?>

<?php
$this->registerJsFile('@web/plugins/jquery-validation/dist/jquery.validate.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-validation/dist/additional-methods.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));



$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/form-validation-entidades.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-print-page/jquery.printPage.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
FormComponents.certificado(); Custom.InitPrint(); 
JS;
$this->registerJs($js);
?>