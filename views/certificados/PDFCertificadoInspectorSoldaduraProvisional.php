<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R3/PC401</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 100%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter; text-align: center">
                <strong style="font-size: large">
                    CERTIFICADO PROVISIONAL DE HOMOLOGACI&Oacute;N DE INSPECTOR DE SOLDADURA<br />
                    (INTERIM APPROVAL CERTIFICATE OF WELDING INSPECTOR)
                </strong>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 85%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                    </span>
                </h5>
            </td>
        </tr>
    </table>
    <table style="border:none; padding-top: 0px; padding-bottom: 0px; font-size: 12px" cellpadding="0" cellspacing="5">
        <tr>
            <td style="width: 30%">
                Que se otorga al Sr.:<br />
                <small>That is granted to:</small>
            </td>
            <td style="width: 70%; border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic; font-size: 14px; font-weight: bold"><?php echo $serv->persona0->nombre_apellidos; ?></span>
            </td>
        </tr>
        <tr>
            <td>
                N&deg; de Identidad Permanente:<br />
                <small>(Identification number)</small>
            </td>
            <td style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic"><?php echo $serv->persona0->ci; ?></span>
            </td>
        </tr>
        <tr>
            <td>
                Empresa:<br />
                <small>(Enterprise)</small>
            </td>
            <td style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic"><?php echo $serv->inspeccion0->entidad0->nombre; ?></span>
            </td>
        </tr>
    </table>

    <p align="justify">
        Por <b>aprobar</b> de forma satisfactoria la
        <b>Evaluaci&oacute;n para la Homologaci&oacute;n de Inspectores de Soldadura de Nivel</b> <?= '<span style="text-decoration: underline">' . $model->nivel . '</span>'; ?>
        y cumplir con los requisitos del <b>RCB</b>
        especificados en el PC4-01
        "Procedimiento para la evaluaci&oacute;n de la conformidad de productos, servicios y personal calificado" y en el Cap&iacute;tulo 5.6 "Inspectores de soldadura" del
        "Manual de Aprobaci&oacute;n de Productos, Servicios y Personal Calificado y en la norma UNE 14618:2000 Inspectores
        de Soldadura. Cualificaci&oacute;n y certificaci&oacute;n".
    </p>
    <p align="justify">
        For having approved in a satisfactory way the <i>Evaluation for the Approval of Welding Inspectors</i> of level
        <?= '<span style="text-decoration: underline">' . $model->nivel . '</span>'; ?>
        and fulfill the requirements of the <b>RCB</b>
        specified in PC4-01 "Procedure for the conformity assessment for products, services and qualified personnel's"
        and in the Chapter 5.6 "Inspectors of welding" of the Manual of Approval of Products, Services and Qualified
        Personnel and in the Standard UNE 14618: 2000 Weld Inspector. Qualification and certification
    </p>


    <p align="justify">
        El presente Certificado autoriza al Inspector a realizar trabajos de inspecci&oacute;n a soldaduras con el siguiente alcance:<br />
        <i>The present Certificate authorizes the Welding Inspector to carry out inspection woks to welding with the following
            scope:</i>
        <br />
        <?= $model->alcance; ?>
    </p>

    <p align="justify">
        El presente certificado tendr&aacute; una vigencia de <strong>cinco (5)</strong> meses a partir de su expedici&oacute;n,
        hasta que sea sustituido por un Certificado de Homologaci&oacute;n definitivo,
        que expedir&aacute; la Oficina de Inspecci&oacute;n del <b>RCB</b>.
    </p>
    <p align="justify">
        This Certificate will be valid for <strong>five (5)</strong> months from their issue
        until it's replaced by a long term Approval Certificate,
        which will be issued by the Inspection Office of the <b>RCB</b>.
    </p>

    <table style="border: none">
        <tr>
            <td>
                <table style="border:none">
                    <tr>
                        <td>
                            Fecha de emisi&oacute;n: <br />
                            <small>(Date of issue)</small>
                        </td>
                        <td><br /><i><?php echo $model->emision; ?></i></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <br /><br /><br />

    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php echo $model->funcionario; ?>
                    <br />
                    <h5>
                        Nombre y firma del Inspector del Registro:
                        <br />
                        <small>
                            (RCB Surveyor´s name and signature)
                        </small>
                    </h5>
                </span>
            </td>
        </tr>
    </table>

</page>