<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">&nbsp;</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 99%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h3 style="text-align: center;">
                    CERTIFICADO DE SUPERVISI&Oacute;N T&Eacute;CNICA DE CALDERAS
                </h3>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 85%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                    </span>
                </h5>
            </td>
        </tr>
        <tr>
            <td style="width: 70%">
                <h5>
                    <b>SE CERTIFICA QUE:</b>
                    <br />
                    <br />
                    <span style="text-decoration: underline">
                        <?php echo $serv->producto0->propietario0->nombre; ?>
                    </span>
                </h5>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>

    <p align="justify">
        Por obtener resultados satisfactorios en las inspecciones realizadas en el proceso de supervisi&oacute;n t&eacute;cnica
        y cumplir con los requisitos del <b>RCB <i>Sociedad Clasificadora</i></b> especificados en <?= $model->servicio; ?>
    </p>

    <p align="justify">
        Se otorga el presente Certificado para la caldera:
    </p>
    <p style="text-align: center; align-content: center; font-weight: bold">
        <?= strtoupper($serv->producto0->nombre); ?>
    </p>

    <br /><br />

    <p align="justify">
        El presente Certificado se expide en base al Informe de Inspecci&oacute;n N&deg;:
        <span style="text-decoration: underline; font-weight: bold">
            <?= $serv->inspeccion0->numero_control; ?>
        </span>
        , bajo las condiciones establecidas al dorso.
    </p>

    <br /><br /><br />
    <div class="row">
        <div class="col-sm-12">
            <h5>
                Fecha Expedici&oacute;n:
                <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                <br /><br />
                Fecha Vencimiento:
                <span style="text-decoration: underline"><?php echo $model->vencimiento; ?></span>
            </h5>
        </div>
    </div>

    <br /><br /><br />
    <br /><br /><br />
    <br /><br /><br />


    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php
                    if ($model->nivelAprobacion != null) {
                        //Validar aqui que el funcionario este activo
                        if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                            echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                        } else {
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                            echo '<br/>';
                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                        }
                    }
                    ?>
                </span>
            </td>
        </tr>
    </table>

</page>

<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">&nbsp;</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <br /><br />
    <table style="border: 1px">
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CONDICIONES GENERALES
                </h4>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter; justify-content: center">
                <ol>
                    <li style="text-align: justify; padding-bottom: 12px">
                        La vigencia y validez del Certificado expira en la fecha de vencimiento establecida en
                        el propio Certificado.
                    </li>
                    <li style="text-align: justify; padding-bottom: 12px">
                        Las calderas est&aacute;n sujetas a inspecciones peri&oacute;dicas cada 6 meses y anuales por parte del RCB
                        Sociedad Clasificadora con vistas a verificar el cumplimiento de los requisitos y condiciones bajo los
                        cuales recibi&oacute; el Certificado
                    </li>
                    <li style="text-align: justify; padding-bottom: 12px">
                        La entidad cuya caldera es supervisada notificar&aacute; al RCB Sociedad Clasificadora sobre cualquier
                        cambio o modificaci&oacute;n, que afecte a la caldera, en plazo no mayor de 7 d&iacute;as despu&eacute;s de efectuados
                        dichos cambios o modificaciones
                    </li>
                    <li style="text-align: justify; padding-bottom: 12px">
                        El Certificado pierde su validez si se incumple los requisitos bajo las cuales fue otorgado.
                    </li>
                    <li style="text-align: justify;">
                        Cuando el Certificado pierde su validez, por no cumplir las condiciones bajo las cuales fue otorgado o
                        expiraci&oacute;n del plazo de vigencia, el mismo ser&aacute; devuelto al RCB Sociedad Clasificadora.
                    </li>
                </ol>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CONFIRMACI&Oacute;N DE LAS INSPECCIONES ANUALES
                </h4>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr style="text-align: center; font-weight: lighter">
                        <td style="width: 20%; text-align: center">Primera Anual</td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Fecha
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Firma del Inspector
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />N&deg; de Control
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Cu&ntilde;o del Inspector
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr style="text-align: center; font-weight: lighter">
                        <td style="width: 20%; text-align: center">Segunda Anual</td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Fecha
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Firma del Inspector
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />N&deg; de Control
                        </td>
                        <td style="width: 20%; text-align: center">
                            <br /><br /><br />
                            <hr />Cu&ntilde;o del Inspector
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</page>