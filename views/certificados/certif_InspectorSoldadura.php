<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Inspecciones de Personas <small> Gesti&oacute;n de la informaci&oacute;n de Certificados</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="icon-barcode"></i>
    <a href="<?php echo Url::to('/inspecciones/index-personas') ?>">Inspecciones - </a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Personas - </a><i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Certificados </a>
</li>
<li class="btn-group">
    <a href="<?php echo Url::to('/inspecciones/detalles-personas', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-primary">
        <span style="color: #FFFFFF"><i class="icon-arrow-left"></i>Atras</span></a>
</li><?php $this->endBlock(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Certificado de Inspecci&oacute;n</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">

                <div class="container">
                    <div class="form-body">

                        <img src="<?= Yii::getAlias('@web/uploads'); ?>/images/bannerHeader.jpg" width="100%">
                        <center>
                            <strong style="font-size: large">
                                CERTIFICADO DE HOMOLOGACI&Oacute;N DE INSPECTOR DE SOLDADURA<br />
                                (APPROVAL CERTIFICATE OF WELDING INSPECTOR)
                            </strong>
                        </center>

                        <div class="row">
                            <div class="col-sm-9">&nbsp;</div>
                            <div class="col-sm-3 float-left">
                                <h5>
                                    N&deg;:
                                    <span style="text-decoration: underline">
                                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <h5>
                                    Que se otorga al Sr.:<br />
                                    <small>That is granted to:</small>
                                </h5>
                            </div>
                            <div class="col-sm-9">
                                <br />
                                <span style="text-decoration: underline;"><?php echo $serv->persona0->nombre_apellidos; ?></span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <h5>
                                    N&deg; de Identidad Permanente:<br />
                                    <small>(Identification number)</small>
                                </h5>
                            </div>
                            <div class="col-sm-9">
                                <br />
                                <span style="text-decoration: underline"><?php echo $serv->persona0->ci; ?></span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-3">
                                <h5>
                                    Empresa:<br />
                                    <small>(Enterprise)</small>
                                </h5>
                            </div>
                            <div class="col-sm-9">
                                <br />
                                <span style="text-decoration: underline"><?php echo $serv->inspeccion0->entidad0->nombre; ?></span>
                            </div>
                        </div>

                        <br />
                        <!-- BEGIN FORM-->
                        <?php
                        $form = ActiveForm::begin([
                            'id' => 'certificadoPersonasProvisional-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'htmlOptions' => ['enctype' => 'multipart/form-data']
                        ]);
                        ?>

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    Por <b>aprobar</b> de forma satisfactoria la
                                    <span style="text-decoration: underline">Evaluaci&oacute;n para la Homologaci&oacute;n de Inspectores de Soldadura de Nivel</span>
                                    <?php
                                    if (isset($model->nivel) && $model->nivel != null) {
                                        echo '<span style="text-decoration: underline">' . $model->nivel . '</span>';
                                    } else {
                                        echo Html::activeTextInput($model, 'nivel', array('class' => 'form-control', 'placeholder' => 'Ej.: 2', 'maxlength' => 2));
                                    }
                                    ?>
                                    y cumplir con los requisitos del
                                    <img src="<?= Yii::getAlias('@web/themes/sbadmin') ?>/assets/img/certificados_inspecciones/logo-cuerpo-cert-rcb.jpg" height="19px">
                                    especificados en el PC4-01
                                    "Procedimiento para la evaluaci&oacute;n de la conformidad de productos, servicios y personal calificado" y en el Cap&iacute;tulo 5.6 "Inspectores de soldadura" del
                                    "Manual de Aprobaci&oacute;n de Productos, Servicios y Personal Calificado y en la norma UNE 14618:2000 Inspectores
                                    de Soldadura. Cualificaci&oacute;n y certificaci&oacute;n".
                                </p>
                                <p align="justify">
                                    For having approved in a satisfactory way the <i>Evaluation for the Approval of Welding Inspectors</i> of level <?php
                                                                                                                                                    if (isset($model->nivel) && $model->nivel != null) {
                                                                                                                                                        echo $model->nivel;
                                                                                                                                                    } else {
                                                                                                                                                        echo '_________';
                                                                                                                                                    }
                                                                                                                                                    ?>
                                    and fulfill the requirements of the
                                    <img src="<?= Yii::getAlias('@web/themes/sbadmin') ?>/assets/img/certificados_inspecciones/logo-cuerpo-cert-rcb.jpg" height="19px">
                                    specified in PC4-01 "Procedure for the conformity assessment for products, services and qualified personnel's"
                                    and in the Chapter 5.6 "Inspectors of welding" of the Manual of Approval of Products, Services and Qualified
                                    Personnel and in the Standard UNE 14618: 2000 Weld Inspector. Qualification and certification
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    El presente Certificado autoriza al Inspector a realizar trabajos de inspecci&oacute;n a soldaduras con el siguiente alcance:<br />
                                    <i>The present Certificate authorizes the Welding Inspector to carry out inspection woks to welding with the following
                                        scope:</i>
                                    <br />
                                    <?php
                                    if (isset($model->alcance) && $model->alcance != null) {
                                        echo $model->alcance;
                                    } else {
                                        echo Html::activeTextArea($model, 'alcance', array('class' => 'ckeditor form-control'));
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-2">
                                <h5>Fecha de expedici&oacute;n <span class="text-danger">*</span>:<br /><small>(Date of issue)</small></h5>
                                <br />
                                <h5>Per&iacute;odo de validez <span class="text-danger">*</span>:<br /><small>(Validity period)</small></h5>
                            </div>
                            <div class="col-sm-4">
                                <?php if (isset($model->emision) && $model->emision != null) { ?>
                                    <h5><u><?php echo $model->emision; ?></u></h5>
                                <?php } else { ?>
                                    <?php echo Html::activeTextInput($model, 'emision', array('class' => 'form-control form-control date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                                    <span class="help-block"><?php echo Html::error($model, 'emision', array('class' => 'text-danger')); ?></span>
                                <?php } ?>
                                <br />
                                <?php if (isset($model->validez) && $model->validez != null) { ?>
                                    <br />
                                    <h5><u><?php echo $model->validez; ?></u></h5>
                                <?php } else { ?>
                                    <h5><u>Pendiente de definici&oacute;n</u></h5>
                                <?php } ?>
                            </div>
                            <div class="col-sm-6">
                                <br />
                                <center>
                                    <hr />
                                </center>
                                <center>
                                    <strong>
                                        <?php
                                        if ($model->nivelAprobacion != null) {
                                            //Validar aqui que el funcionario este activo
                                            if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                                                echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                                                echo '<br/>';
                                                echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                                            } else {
                                                echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                                                echo '<br/>';
                                                echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                                            }
                                        }
                                        ?>
                                    </strong>
                                    <h5>
                                        Nombre y firma del jefe de la dependencia del RCB
                                        <br />
                                        <small>
                                            (Manager's name and signature of RCB Organizational Unit)
                                        </small>
                                    </h5>
                                </center>
                            </div>
                        </div>
                        <br /><br />


                        <div class="row">
                            <div class="col-sm-1">R5/PC401</div>
                            <div class="col-sm-11">&nbsp; </div>
                        </div>


                    </div>

                    <center>
                        <h3>CONDICIONES GENERALES</h3>
                    </center>
                    <div class="row">
                        <div class="col-md-12">
                            <ol>
                                <li style="text-align: justify; padding-bottom: 12px">
                                    El presente Certificado solo avala Inspecciones de soldadura acordes a los conocimientos t&eacute;cnicos
                                    seg&uacute;n el Nivel y el Alcance especificados<br />
                                    <small>The present Certificate endorses only welding inspections to the technical knowledge according
                                        to the Level and Scope specified.</small>
                                </li>
                                <li style="text-align: justify; padding-bottom: 12px">
                                    La vigencia del presente Certificado (3 a&ntilde;os) depender&aacute; del estricto cumplimiento de los siguientes
                                    aspectos:<br />
                                    <small>The validity of this Certificate (3 years) depends upon the strict fulfillment of the below aspects:</small>
                                    <ul>
                                        <li style="text-align: justify; padding-bottom: 12px">
                                            Del nivel cualitativo a demostrar por el inspector de soldadura en trabajos de inspecci&oacute;n de
                                            soldadura habituales.<br />
                                            <small>The quality level to be shown by the welding inspector periodical welding inspections.</small>
                                        </li>
                                        <li style="text-align: justify; padding-bottom: 12px">
                                            Del ininterrumpido ejercicio de trabajos en la especialidad de soldadura (En caso contrario, no m&aacute;s
                                            de 6 meses).<br />
                                            <small>The continuos welding inspections works to be performed (otherwise, not later than 6 months).</small>
                                        </li>
                                    </ul>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <center>
                        <h3>CONFIRMACI&Oacute;N DE LAS INSPECCIONES ANUALES</h3>
                    </center>
                    <br /><br />
                    <div class="row">
                        <div class="col-md-2">
                            <b>Primera Anual:</b>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>Fecha</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Firma del Inspector</center>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>N&deg; de Control</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Cu&ntilde;o del Inspector</center>
                        </div>
                    </div>
                    <br /><br /><br />
                    <div class="row">
                        <div class="col-md-2">
                            <b>Segunda Anual:</b>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>Fecha</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Firma del Inspector</center>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>N&deg; de Control</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Cu&ntilde;o del Inspector</center>
                        </div>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-2">
                                <?php if ($model->control == 0) { ?>
                                    <button id="inspeccion_servicios_submit" type="submit" class="btn btn-primary"><i class="icon-ok"></i>Vista Previa del Certificado</button>
                                <?php } elseif ($model->control == 1) { ?>
                                    <a href="<?php echo Url::to('/certificados/PDFCertificadoInspectorSoldadura', array('o' => $serv->id)) ?>" class="btn btn-success"><i class="fa fa-file"></i> Generar Certificado</a>
                                    |
                                    <a href="<?php echo Url::to('/certificados/certificado', array('obj' => $serv, 'tipoObj' => 1, 'tipoCe' => 1)) ?>" class="btn btn-danger"><i class="icon-repeat"></i> Corregir Certificado</a>
                                <?php } ?>
                                |
                                <a href="<?php echo Url::to('/inspecciones/detalles-personas', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));

?>

<?php
$this->registerJsFile('@web/plugins/jquery-validation/dist/jquery.validate.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-validation/dist/additional-methods.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));



$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/form-validation-entidades.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-print-page/jquery.printPage.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
FormComponents.certificado(); Custom.InitPrint(); 
JS;
$this->registerJs($js);
?>