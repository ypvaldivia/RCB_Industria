<?php

use app\models\Funcionarios;
use app\models\Inspecciones;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<!-- BEGIN PAGE HEADER-->
<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Inspecciones de Personas <small> Gesti&oacute;n de la informaci&oacute;n de Certificados</small>
            <?php $this->endBlock(); ?>

            <?php $this->beginBlock('block_migas'); ?>
            <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/indexMits') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Medios de Izado - </a><i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Certificados </a>
            </li>
            <li class="btn-group">
                <a href="<?php echo Url::to('/inspecciones/detallesMit', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-primary">
                    <span style="color: #FFFFFF"><i class="icon-arrow-left"></i>Atras</span></a>
            </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Certificado de Inspecci&oacute;n</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">

                <div class="container">
                    <div class="form-body">
                        <!-- BEGIN FORM-->
                        <?php
                        $form = ActiveForm::begin([
                            'id' => 'certificadoPersonasProvisional-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'htmlOptions' => ['enctype' => 'multipart/form-data']
                        ]);
                        ?>

                        <center>
                            <img src="<?= Yii::getAlias('@web/uploads') ?>/images/bannerHeader.jpg" width="80%">
                        </center>

                        <div class="row">
                            <div class="col-sm-12">
                                <center>
                                    <h2>
                                        <span style="font-variant: all-small-caps">
                                            CERTIFICADO DE SEGURIDAD T&Eacute;CNICA DEFINITIVO</br>
                                            PARA MONTACARGAS Y TRASPALETA
                                        </span>
                                    </h2>
                                </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-10">&nbsp;</div>
                            <div class="col-sm-2 float-left">
                                <h5>
                                    N&deg;:
                                    <span style="text-decoration: underline">
                                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    Certificado de Seguridad T&eacute;cnica para Montacargas y Traspaleta, expedido en virtud de los dispuesto en
                                    la Resoluci&oacute;n No. 293/2014 del MITRANS "Reglamento para la Explotaci&oacute;n de Medios de
                                    Izado" y el Convenio 152 de la Organizaci&oacute;n Internacional del Trabajo.
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <h5>
                                    <b>NOMBRE DEL EQUIPO:</b>
                                    <span style="text-decoration: underline">
                                        <?php echo $serv->medioIzado->tipo0->tipo; ?>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <h5>
                                    <b>PROPIETARIO:</b>
                                    <span style="text-decoration: underline">
                                        <?php echo $serv->medioIzado->propietario0->nombre; ?>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <br />

                        <table border="1" width="100%">
                            <tr>
                                <td style="text-align: center; width: 33%">Descripci&oacute;n del equipo <br /><small>(marca y su n&uacute;mero distintivo)</small> </td>
                                <td style="text-align: center; width: 33%">Carga de prueba <br /><small>(Kg)</small> </td>
                                <td style="text-align: center; width: 33%">Capacidad de Izado Certificada <br /><small>(Kg)</small></td>
                            </tr>
                            <tr>
                                <td style="text-align: center">I </td>
                                <td style="text-align: center">II</td>
                                <td style="text-align: center">III </td>
                            </tr>
                            <tr>
                                <td style="text-align: center"><?php echo $serv->medioIzado->nombre; ?> </td>
                                <td style="text-align: center">
                                    <div class="row-fluid">
                                        <h5>PRUEBA EST&Aacute;TICA</h5>
                                    </div>
                                    <hr />
                                    <div class="row-fluid">
                                        <?php
                                        if (isset($model->carga_pp_est) && $model->carga_pp_est != null) {
                                            echo 'GP: ' . $model->carga_pp_est;
                                        } elseif (!$model->flag) { ?>
                                            <div class="col-md-3">
                                                <right>GP:</right>
                                            </div>
                                            <div class="col-md-9">
                                                <left><?= Html::activeTextInput($model, 'carga_pp_est', array('class' => 'form-control input-small', 'placeholder' => 'Principal')); ?></left>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <br /><br />
                                    <div class="row-fluid">
                                        <?php
                                        if (isset($model->carga_aux_est) && $model->carga_aux_est != null) {
                                            echo 'GA 1: ' . $model->carga_aux_est;
                                        } elseif (!$model->flag) { ?>
                                            <div class="col-md-3">
                                                <right>GA 1:</right>
                                            </div>
                                            <div class="col-md-9">
                                                <left><?= Html::activeTextInput($model, 'carga_aux_est', array('class' => 'form-control input-small', 'placeholder' => 'Auxiliar 1')); ?></left>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <br /><br />
                                    <div class="row-fluid">
                                        <?php
                                        if (isset($model->carga_aux1_est) && $model->carga_aux1_est != null) {
                                            echo 'GA 2: ' . $model->carga_aux1_est;
                                        } elseif (!$model->flag) { ?>
                                            <div class="col-md-3">
                                                <right>GA 2:</right>
                                            </div>
                                            <div class="col-md-9">
                                                <left><?= Html::activeTextInput($model, 'carga_aux1_est', array('class' => 'form-control input-small', 'placeholder' => 'Auxiliar 2')); ?></left>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <br />
                                    <hr />
                                    <div class="row-fluid">
                                        <h5>PRUEBA DIN&Aacute;MICA</h5>
                                    </div>
                                    <hr />
                                    <div class="row-fluid">
                                        <?php
                                        if (isset($model->carga_pp_din) && $model->carga_pp_din != null) {
                                            echo 'GP: ' . $model->carga_pp_din;
                                        } elseif (!$model->flag) {  ?>
                                            <div class="col-md-3">
                                                <right>GP:</right>
                                            </div>
                                            <div class="col-md-9">
                                                <left><?= Html::activeTextInput($model, 'carga_pp_din', array('class' => 'form-control input-small', 'placeholder' => 'Principal')); ?></left>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <br /><br />
                                    <div class="row-fluid">
                                        <?php
                                        if (isset($model->carga_aux_din) && $model->carga_aux_din != null) {
                                            echo 'GA 1: ' . $model->carga_aux_din;
                                        } elseif (!$model->flag) {  ?>
                                            <div class="col-md-3">
                                                <right>GA 1:</right>
                                            </div>
                                            <div class="col-md-9">
                                                <left><?= Html::activeTextInput($model, 'carga_aux_din', array('class' => 'form-control input-small', 'placeholder' => 'Auxiliar 1')); ?></left>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <br /><br />
                                    <div class="row-fluid">
                                        <?php
                                        if (isset($model->carga_aux1_din) && $model->carga_aux1_din != null) {
                                            echo 'GA 2: ' . $model->carga_aux1_din;
                                        } elseif (!$model->flag) {  ?>
                                            <div class="col-md-3">
                                                <right>GA 2:</right>
                                            </div>
                                            <div class="col-md-9">
                                                <left><?= Html::activeTextInput($model, 'carga_aux1_din', array('class' => 'form-control input-small', 'placeholder' => 'Auxiliar 2')); ?></left>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <br />
                                </td>
                                <td style="text-align: center">
                                    <div class="row-fluid">
                                        <?php
                                        if (isset($model->ci_pp_certificada) && $model->ci_pp_certificada != null) {
                                            echo 'GP: ' . $model->ci_pp_certificada;
                                        } elseif (!$model->flag) {  ?>
                                            <div class="col-md-3">
                                                <right>GP:</right>
                                            </div>
                                            <div class="col-md-9">
                                                <left><?= Html::activeTextInput($model, 'ci_pp_certificada', array('class' => 'form-control input-small', 'placeholder' => 'Principal')); ?></left>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <br /><br />
                                    <div class="row-fluid">
                                        <?php
                                        if (isset($model->ci_aux_certificada) && $model->ci_aux_certificada != null) {
                                            echo 'GA 1: ' . $model->ci_aux_certificada;
                                        } elseif (!$model->flag) { ?>
                                            <div class="col-md-3">
                                                <right>GA 1:</right>
                                            </div>
                                            <div class="col-md-9">
                                                <left><?= Html::activeTextInput($model, 'ci_aux_certificada', array('class' => 'form-control input-small', 'placeholder' => 'Auxiliar 1')); ?></left>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <br /><br />
                                    <div class="row-fluid">
                                        <?php
                                        if (isset($model->ci_aux1_certificada) && $model->ci_aux1_certificada != null) {
                                            echo 'GA 2: ' . $model->ci_aux1_certificada;
                                        } elseif (!$model->flag) { ?>
                                            <div class="col-md-3">
                                                <right>GA 2:</right>
                                            </div>
                                            <div class="col-md-9">
                                                <left><?= Html::activeTextInput($model, 'ci_aux1_certificada', array('class' => 'form-control input-small', 'placeholder' => 'Auxiliar 2')); ?></left>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </td>
                            </tr>
                        </table>

                        <br />

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    CERTIFICO QUE:
                                    el <?php echo Inspecciones::convertirFecha($serv->inspeccion0->getUltimaVisita()->fecha); ?>, el medio de izado terrestre descrito,
                                    fue inspeccionado por una persona competente acreditada y que el
                                    examen al que fue sometido, despu&eacute;s de las correspondientes pruebas,
                                    dio como resultado que hubo de resistir la carga de prueba, sin sufrir
                                    da&ntilde;os, ni deformaciones residuales y que su capacidad de izado certificada
                                    es la que se indica en la columna III.
                                </p>
                            </div>
                        </div><br />


                        <?php if (isset($model->expedicion) && $model->expedicion != null) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                        <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                                    </h5>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-2">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                    </h5>
                                </div>
                                <div class="col-sm-10">
                                    <?php echo Html::activeTextInput($model, 'expedicion', array('class' => 'form-control form-control input-medium date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (isset($model->vencimiento) && $model->vencimiento != null) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>
                                        Fecha Vencimiento:
                                        <span style="text-decoration: underline"><?php echo $model->vencimiento; ?></span>
                                    </h5>
                                </div>
                            </div>
                        <?php } ?>


                        <br /><br />

                        <div class="row">
                            <div class="col-sm-8">&nbsp;</div>
                            <div class="col-sm-4">
                                <center>
                                    <hr />
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">&nbsp;</div>
                            <div class="col-sm-4">
                                <center>
                                    <?php
                                    if ($model->nivelAprobacion != null) {
                                        if ($model->nivelAprobacion[0]->inspeccion0->nivel == 5) {
                                            $firma = Funcionarios::findOne('cargo = :c', array(':c' => 6));
                                            if ($firma->inactivo == 0) {
                                                echo $firma->nombre_apellidos;
                                                echo '<br/>';
                                                echo $firma->cargo0->cargo;
                                            } else {
                                                echo $firma->getSustituto()->nombre_apellidos;
                                                echo '<br/>';
                                                echo $firma->getSustituto()->cargoFull;
                                            }
                                        } else {
                                            if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                                                echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                                                echo '<br/>';
                                                echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                                            } else {
                                                echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                                                echo '<br/>';
                                                echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                                            }
                                        }
                                    }
                                    ?>
                                </center>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-1">R17/IC403</div>
                            <div class="col-sm-11">&nbsp; </div>
                        </div>

                    </div>

                    <hr />
                    <center>
                        <h3>CONDICIONES GENERALES</h3>
                    </center>
                    <div class="row">
                        <div class="col-md-12">
                            <ol>
                                <li>
                                    <p align="justify">
                                        La capacidad de izado certificada que es la indicada en la columna III,
                                        es la m&aacute;xima carga de trabajo a operar.
                                    </p>
                                </li>
                                <li>
                                    <p align="justify">
                                        El medio de izado podr&aacute; ofrecer servicio durante el tiempo de vigencia del Certificado (3 a&ntilde;os),
                                        siempre que se someta a dos inspecciones peri&oacute;dicas anuales por el RCB con resultados satisfactorios,
                                        durante el per&iacute;odo de explotaci&oacute;n autorizado. Las inspecciones anuales se realizar&aacute;n, a
                                        solicitud del Cliente, dentro del mes anterior a la fecha aniversario de la expedici&oacute;n de los correspondientes
                                        Certificados de Seguridad T&eacute;cnica, para la confirmaci&oacute;n de la validez de los mismos.
                                    </p>
                                </li>
                                <li>
                                    <p align="justify">
                                        El medio de izado se someter&aacute; a la inspecci&oacute;n de renovaci&oacute;n por el RCB al terminar la vigencia
                                        del Certificado. Si el resultado fuera satisfactorio, el RCB le expedir&aacute; al propietario un nuevo Certificado
                                        de Seguridad T&eacute;cnica por 3 a&ntilde;os.
                                    </p>
                                </li>
                                <li>
                                    <p align="justify">
                                        Durante el per&iacute;odo de validez del Certificado, si el propietario deseara realizar cualquier modificaci&oacute;n,
                                        cambio, reparaciones mayores o capitales al medio de izado, deber&aacute; informarlo al RCB, previa materializaci&oacute;n
                                        de la actividad.
                                    </p>
                                </li>
                                <li>
                                    <p align="justify">
                                        Durante el per&iacute;odo de validez del Certificado, si el medio de izado sufre alguna aver&iacute;a en el mecanismo de izado,
                                        giro, cambio de voladizo, brazo o cambio de cables de acero (amante o amantillo), deber&aacute; ser informado al RCB.
                                    </p>
                                </li>
                                <li>
                                    <p align="justify">
                                        Este Certificado no es v&aacute;lido para realizar operaciones que impliquen la carga de personas.
                                    </p>
                                </li>
                                <li>
                                    <p align="justify">
                                        El Certificado perder&aacute; su vigencia si se incumplen las condiciones precitadas.
                                    </p>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <center>
                        <h3>CONFIRMACI&Oacute;N DE LAS INSPECCIONES ANUALES</h3>
                    </center>
                    <br /><br />
                    <div class="row">
                        <div class="col-md-2">
                            <b>Primera Anual:</b>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>Fecha</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Firma del Inspector</center>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>N&deg; de Control</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Cu&ntilde;o del Inspector</center>
                        </div>
                    </div>
                    <br /><br /><br />
                    <div class="row">
                        <div class="col-md-2">
                            <b>Segunda Anual:</b>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>Fecha</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Firma del Inspector</center>
                        </div>
                        <div class="col-md-2">
                            <hr />
                            <center>N&deg; de Control</center>
                        </div>
                        <div class="col-md-3">
                            <hr />
                            <center>Cu&ntilde;o del Inspector</center>
                        </div>
                    </div>

                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-2">
                                <?php if ($model->control == 0) { ?>
                                    <button id="inspeccion_servicios_submit" type="submit" class="btn btn-primary"><i class="icon-ok"></i>Vista Previa del Certificado</button>
                                <?php } elseif ($model->control == 1) { ?>
                                    <a href="<?php echo Url::to('/certificados/PDFCertificadoMIT1', array('o' => $serv->id)) ?>" class="btn btn-success"><i class="fa fa-file"></i> Generar Certificado</a>
                                    |
                                    <a href="<?php echo Url::to('/certificados/certificado', array('obj' => $serv, 'tipoObj' => 3, 'tipoCe' => 2)) ?>" class="btn btn-danger"><i class="icon-repeat"></i> Corregir Certificado</a>
                                <?php } ?>
                                |
                                <a href="<?php echo Url::to('/inspecciones/detallesProductos', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));

?>

<?php
$this->registerJsFile('@web/plugins/jquery-validation/dist/jquery.validate.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-validation/dist/additional-methods.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/form-validation-entidades.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-print-page/jquery.printPage.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
FormComponents.certificado(); Custom.InitPrint(); 
JS;
$this->registerJs($js);
?>