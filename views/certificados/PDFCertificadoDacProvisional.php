<?php

use app\models\Inspecciones;

?>
<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
        table-layout: fixed;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R2/IC404</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 99%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CERTIFICADO DE SEGURIDAD T&Eacute;CNICA PROVISIONAL
                </h4>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 80%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                    </span>
                </h5>
            </td>
        </tr>
    </table>

    <p align="justify">
        Certificado de Seguridad T&eacute;cnica para los Dispositivos Auxiliares de Carga, expedido en virtud de los dispuesto en
        la Resoluci&oacute;n No. 293/2014 del MITRANS "Reglamento para la Explotaci&oacute;n de Medios de
        Izado" y el Convenio 152 de la Organizaci&oacute;n Internacional del Trabajo.
    </p>


    <table border="1" style="width: 450px; table-layout: fixed">
        <tr>
            <td style="text-align: center; max-width: 40%">Tipo de dispositivo auxiliar de carga </td>
            <td style="text-align: center; max-width: 40%">Identificaci&oacute;n del dispositivo</td>
            <td style="text-align: center; max-width: 20%">Propietario </td>
        </tr>
        <tr>
            <td style="text-align: center">I </td>
            <td style="text-align: center">II</td>
            <td style="text-align: center">III </td>
        </tr>
        <tr>
            <td style="text-align: center"><?php echo $serv->dac0->tipo0->tipo; ?> </td>
            <td style="text-align: center;"><?php echo $serv->dac0->nombre; ?> </td>
            <td style="text-align: center"><?php if ($serv->dac0->propietario0 != null) {
                                                echo $serv->dac0->propietario0->nombre;
                                            } else {
                                                echo "Sin definir";
                                            } ?> </td>
        </tr>
    </table>

    <br />

    <table border="1" width="100%">
        <tr>
            <td style="text-align: center; width: 25%">Fecha de la inspecci&oacute;n </td>
            <td style="text-align: center; width: 25%">Lugar de realizaci&oacute;n de la inspecci&oacute;n</td>
            <td style="text-align: center; width: 25%">Carga de prueba <br /><small>(kilogramos)</small> </td>
            <td style="text-align: center; width: 25%">Capacidad de trabajo certificada <br /><small>(kilogramos)</small></td>
        </tr>
        <tr>
            <td style="text-align: center">IV </td>
            <td style="text-align: center">V</td>
            <td style="text-align: center">VI </td>
            <td style="text-align: center">VII </td>
        </tr>
        <tr>
            <td style="text-align: center"><?php echo $serv->inspeccion0->getUltimaVisita()->fecha; ?> </td>
            <td style="text-align: center; align-content: center"><?php echo $serv->inspeccion0->lugar_inspeccion; ?> </td>
            <td style="text-align: center; align-content: center">
                PRUEBA EST&Aacute;TICA
                <br />
                <?php echo $model->carga_pp_est; ?>
                <br /><br />
                PRUEBA DIN&Aacute;MICA
                <br />
                <?= $model->carga_pp_din; ?>
                <br />
            </td>
            <td style="text-align: center; align-content: center">
                <?= $model->ci_pp_certificada; ?>
            </td>
        </tr>
    </table>

    <br />

    <p align="justify">
        CERTIFICO QUE:
        el <?php echo Inspecciones::convertirFecha($serv->inspeccion0->getUltimaVisita()->fecha); ?>, el dispositivo auxiliar de carga descrito,
        fue inspeccionado por una persona competente acreditada y que el
        examen al que fue sometido, despu&eacute;s de las correspondientes pruebas,
        dio como resultado que hubo de resistir la carga de prueba, sin sufrir
        da&ntilde;os, ni deformaciones residuales y que su capacidad de izado certificada
        es la que se indica en la columna VII.
    </p>
    <p align="justify">
        El presente certificado tendr&aacute; una vigencia de <strong>cinco (5)</strong> meses a partir de su expedici&oacute;n,
        hasta que sea sustituido por un Certificado de Homologaci&oacute;n definitivo,
        que expedir&aacute; la Oficina de Inspecci&oacute;n del RCB.
    </p>
    <p align="justify">
        Este Certificado no es v&aacute;lido para realizar operaciones que impliquen la carga de personal.
    </p>

    <div class="row">
        <div class="col-sm-12">
            <h5>
                Fecha Expedici&oacute;n:
                <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
            </h5>
        </div>
    </div>

    <br /><br /><br />

    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php echo $model->funcionario; ?>
                    <br />
                    Inspector Actuante
                </span>
            </td>
        </tr>
    </table>

</page>