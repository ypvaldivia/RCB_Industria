<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
    }

    table tr {
        margin: 0px auto;
    }

    table td {
        padding: 3px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }

    .tbl,
    .tbl1 {
        border: solid 1px black;
        font-size: 11px;
        width: 100%;
    }

    .tbl td {
        border: solid 1px black;
    }

    .tbl1 td {
        border: solid 1px black;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="3mm" backbottom="3mm" backleft="10mm" backright="10mm">

    <table style="width: 100%; border:none;">
        <tr>
            <td style="border: 1px solid black; width: 100%; align-content: center; text-align: center">
                <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 100%">
                <strong style="font-size: large">
                    CERTIFICADO PROVISIONAL DE HOMOLOGACI&Oacute;N DEL SOLDADOR EN ALUMINIO<br />
                    (INTERIM ALUMINIUM WELDER'S APPROVAL CERTIFICATE)
                </strong>
            </td>
        </tr>
    </table>

    <table style="border:none; padding-top: 0px; padding-bottom: 0px; font-size: 12px" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 25%">
                &nbsp;
            </td>
            <td style="width: 55%">
                &nbsp;
            </td>
            <td>
                N&deg;:&nbsp;
                <span style="text-decoration: underline">
                    <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                </span>
            </td>
        </tr>
        <tr>
            <td>
                Nombre del soldador:<br />
                <small>(Welder´s Name)</small>
            </td>
            <td colspan="2" style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic; font-size: 14px; font-weight: bold"><?php echo $serv->persona0->nombre_apellidos; ?></span>
            </td>
        </tr>
        <tr>
            <td>
                N&deg; de identificación:<br />
                <small>(Identification number)</small>
            </td>
            <td colspan="2" style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic"><?php echo $serv->persona0->ci; ?></span>
            </td>
        </tr>
        <tr>
            <td>
                Procedimiento de soldadura:<br />
                <small>(Welding procedure)</small>
            </td>
            <td colspan="2" style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic"><?php echo $model->procedimientoSoldadura; ?></span>
            </td>
        </tr>
        <tr>
            <td>
                Empresa:<br />
                <small>(Enterprise)</small>
            </td>
            <td colspan="2" style="border-bottom: solid 1px black; align-content: left">
                <span style="font-style: italic"><?php echo $serv->inspeccion0->entidad0->nombre; ?></span>
            </td>
        </tr>
    </table>

    <br />

    <table style="border:none; padding-top: 0px; padding-bottom: 0px; font-size: 12px" cellspacing="0" cellpadding="0">
        <tr>
            <td style="width: 40%; text-align: right">
                <strong>Resultados del Examen Te&oacute;rico: </strong><br />
                <small>(Results of the theory test)</small>
            </td>
            <td style="width: 20%; text-align: right">
                Aprobado<br /><small>(Passed)</small>
            </td>
            <td style="width: 5%; text-align: left">
                <?php
                if ($model->resultadoExamenTeorico == 0) {
                    echo '( - )';
                } else {
                    echo '( X )';
                }
                ?>
            </td>
            <td style="width: 20%; text-align: right">
                No Requerido<br /><small>(Not required)</small>
            </td>
            <td style="width: 5%; text-align: left">
                <?php
                if ($model->resultadoExamenTeorico == 0) {
                    echo '( X )';
                } else {
                    echo '( - )';
                }
                ?>
            </td>
            <td style="width: 10%; text-align: left">
                &nbsp;
            </td>
        </tr>
    </table>

    <h5 style="text-align: center"><b>Resultados del Examen Pr&aacute;ctico</b>&nbsp;<small>(Results of the practical test)</small></h5>

    <table class="tbl">
        <tr>
            <td style="text-align: center; width: 40%"><b>Datos t&eacute;cnicos</b><br />(Technical Data)</td>
            <td style="text-align: center; width: 25%"><b>Detalles de la prueba</b><br />(Weld details)</td>
            <td style="text-align: center; width: 35%"><b>Rango de homologaci&oacute;n</b><br />(Range of approval)</td>
        </tr>
        <tr>
            <td>Proceso de soldadura<br /><small>(Welding process)</small></td>
            <td style="text-align: center">
                <?php echo $model->procesoSoldadura_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->procesoSoldadura_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Chapa o tubo<br /><small>(Plate or pipe)</small></td>
            <td style="text-align: center">
                <?php echo $model->chapaTubo_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->chapaTubo_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Tipos de uni&oacute;n<br /><small>(Joint type)</small></td>
            <td style="text-align: center">
                <?php echo $model->tipoUnion_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->tipoUnion_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Grupos de metal base<br /><small>(Parent metal(s) group(s))</small></td>
            <td style="text-align: center">
                <?php echo $model->grupoMetalBase_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->grupoMetalBase_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Tipo de metal de aportaci&oacute;n<br /><small>(Filler metal type)</small></td>
            <td style="text-align: center">
                <?php echo $model->tipoMetal_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->tipoMetal_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Espesor de la muestra (mm)<br /><small>(Test piece thickness)</small></td>
            <td style="text-align: center">
                <?php echo $model->espesor_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->espesor_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Di&aacute;metro exterior del tubo<br /><small>(Pipe outside diameter)</small></td>
            <td style="text-align: center">
                <?php echo $model->diametro_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->diametro_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Posici&oacute;n de soldadura<br /><small>(Welding position)</small></td>
            <td style="text-align: center">
                <?php echo $model->posicion_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->posicion_rango; ?>
            </td>
        </tr>
        <tr>
            <td>Resanado/Respaldo<br /><small>(Gouging/backing)</small></td>
            <td style="text-align: center">
                <?php echo $model->resanado_detalles; ?>
            </td>
            <td style="text-align: center">
                <?php echo $model->resanado_rango; ?>
            </td>
        </tr>
    </table>

    <br />

    <table style="border:none; font-size: 12px">
        <tr>
            <td style="width: 50%">

            </td>
            <td style="width: 50%; text-align: center; align-content: center" rowspan="3">
                <br /><br />
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php echo $model->funcionario; ?>
                </span><br />
                Nombre y firma del Inspector del Registro
                <br />
                <small>
                    (RCB Surveyor´s name and signature)
                </small>
            </td>
        </tr>
        <tr>
            <td>
                <table style="border:none">
                    <tr>
                        <td>
                            Fecha de emisi&oacute;n: <br />
                            <small>(Date of issue)</small>
                        </td>
                        <td><br /><i><?php echo $model->emision; ?></i></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="border:none">
                    <tr>
                        <td>
                            Per&iacute;odo de validez:<br />
                            <small>(Validity period)</small>
                        </td>
                        <td>5 meses (hasta la emisi&oacute;n del Certificado definitivo)<br />
                            <i>5 months (until the issue of a long term Certificate)</i></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <br />

</page>

<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">

    <h5 style="text-align: center">
        <b>El presente certificado est&aacute; avalado por los siguientes ensayos</b><br />
        <small>This Certificate has been endorsed by the following tests</small>
    </h5>

    <table class="tbl1">
        <tr>
            <td style="text-align: center; width: 60%"><b>Tipo de ensayo</b><br />(Type of test)</td>
            <td style="text-align: center; width: 40%"><b>Realizado y aceptable</b><br />(Performed and acceptable)</td>
        </tr>
        <tr>
            <td style="text-align: center">Visual<br /><small>(Visual)</small></td>
            <td style="text-align: center">
                <?php if ($model->visual == 1) {
                    echo 'X';
                } else {
                    echo '-';
                }; ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Radiogr&aacute;fico o ultras&oacute;nico<br /><small>(Radiographic or ultrasonic)</small></td>
            <td style="text-align: center">
                <?php if ($model->radiograficoUltrasonico == 1) {
                    echo 'X';
                } else {
                    echo '-';
                } ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Part&iacute;culas magn&eacute;ticas<br /><small>(Magnetic particles)</small></td>
            <td style="text-align: center">
                <?php if ($model->particulasMagneticas == 1) {
                    echo 'X';
                } else {
                    echo '-';
                } ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">L&iacute;quidos penetrantes<br /><small>(Dye penetrant)</small></td>
            <td style="text-align: center">
                <?php if ($model->liquidosPenetrantes == 1) {
                    echo 'X';
                } else {
                    echo '-';
                }  ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Rotura<br /><small>(Fracture)</small></td>
            <td style="text-align: center">
                <?php if ($model->rotura == 1) {
                    echo 'X';
                } else {
                    echo '-';
                }  ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Doblado<br /><small>(Bend)</small></td>
            <td style="text-align: center">
                <?php if ($model->doblado == 1) {
                    echo 'X';
                } else {
                    echo '-';
                }  ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center">Otros ensayos<br /><small>(Others additional test)</small></td>
            <td style="text-align: center">
                <?php if ($model->otros == 1) {
                    echo 'X';
                } else {
                    echo '-';
                }  ?>
            </td>
        </tr>
    </table>


    <h5 style="text-align: center">Simbolog&iacute;a Aplicada</h5>

    <div class="row">
        <div class="col-md-12" style="align-content: center">
            <p style="text-align: left; font-size: 11px; align-content: center;"><?php echo $model->simbologia; ?></p>
        </div>
    </div>

</page>