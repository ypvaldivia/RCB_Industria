<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R18/IC403</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php

                            use app\models\Inspecciones;

                            echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 99%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CERTIFICADO DE SEGURIDAD T&Eacute;CNICA PROVISIONAL<br />
                    PARA GR&Uacute;AS, APAREJOS, ELEVADORES DE CARGA<br />
                    Y DE EQUIPO AUTOMOTOR Y TRASBORDADOR
                </h4>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 80%">
                &nbsp;
            </td>
            <td>
                <strong>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                    </span>
                </strong>
            </td>
        </tr>
    </table>

    <p align="justify">
        Certificado de Seguridad T&eacute;cnica para Gr&uacute;as, Aparejos, Elevadores de Carga
        y de Equipo Automotor y Transbordador, expedido en virtud de los dispuesto en
        la Resoluci&oacute;n No. 293/2014 del MITRANS "Reglamento para la Explotaci&oacute;n de Medios de
        Izado" y el Convenio 152 de la Organizaci&oacute;n Internacional del Trabajo.
    </p>

    <div class="row">
        <div class="col-sm-12">
            <strong>
                <b>NOMBRE DEL EQUIPO:</b>
                <span style="text-decoration: underline">
                    <?php echo $serv->medioIzado->tipo0->tipo; ?>
                </span>
            </strong>
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-sm-12">
            <strong>
                <b>PROPIETARIO:</b>
                <span style="text-decoration: underline">
                    <?php echo $serv->medioIzado->propietario0->nombre; ?>
                </span>
            </strong>
        </div>
    </div>

    <br />

    <table border="1" width="100%">
        <tr>
            <td style="text-align: center; max-width: 30%;">Descripci&oacute;n del equipo <br /><small>(marca y su n&uacute;mero distintivo)</small> </td>
            <td style="text-align: center; max-width: 30%">Para gr&uacute;as <br /><small>(radio de aplicaci&oacute;n de la carga de prueba)<br />(metros)</small></td>
            <td style="text-align: center; max-width: 20%">Carga de prueba <br /><small>(kilogramos)</small> </td>
            <td style="text-align: center; max-width: 20%">Capacidad de Izado Certificada <br /><small>(para el radio indicado en la columna II)<br />(kilogramos)</small></td>
        </tr>
        <tr>
            <td style="text-align: center">I </td>
            <td style="text-align: center">II</td>
            <td style="text-align: center">III </td>
            <td style="text-align: center">IV </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <?php
                if (strchr($serv->medioIzado->nombre, ',')) {
                    $sttemp = str_getcsv($serv->medioIzado->nombre);
                    foreach ($sttemp as $s) {
                        echo $s;
                        echo '<br/>';
                    }
                } else {
                    $sttemp = str_split($serv->medioIzado->nombre, 24);
                    foreach ($sttemp as $s) {
                        echo $s;
                        echo '<br/>';
                    }
                }
                ?>
            </td>
            <td style="text-align: center; align-content: center">
                PRUEBA EST&Aacute;TICA<br />
                Principal: <?= $model->radio_pp_est; ?><br />
                <?php if (isset($model->radio_aux_est) && $model->radio_aux_est != null) { ?>
                    Auxiliar 1: <?= $model->radio_aux_est; ?><br />
                <?php } ?>
                <?php if (isset($model->radio_aux1_est) && $model->radio_aux1_est != null) { ?>
                    Auxiliar 2: <?= $model->radio_aux1_est; ?><br />
                <?php } ?>
                <br />
                PRUEBA DIN&Aacute;MICA<br />
                Principal: <?= $model->radio_pp_din; ?><br />
                <?php if (isset($model->radio_aux_din) && $model->radio_aux_din != null) { ?>
                    Auxiliar 1: <?= $model->radio_aux_din; ?><br />
                <?php } ?>
                <?php if (isset($model->radio_aux1_din) && $model->radio_aux1_din != null) { ?>
                    Auxiliar 2: <?= $model->radio_aux1_din; ?><br />
                <?php } ?>
            </td>
            <td style="text-align: center">
                PRUEBA EST&Aacute;TICA<br />
                Principal: <?= $model->carga_pp_est; ?><br />
                <?php if (isset($model->carga_aux_est) && $model->carga_aux_est != null) { ?>
                    Auxiliar 1: <?= $model->carga_aux_est; ?><br />
                <?php } ?>
                <?php if (isset($model->carga_aux1_est) && $model->carga_aux1_est != null) { ?>
                    Auxiliar 2: <?= $model->carga_aux1_est; ?><br />
                <?php } ?>
                <br />
                PRUEBA DIN&Aacute;MICA<br />
                Principal: <?= $model->carga_pp_din; ?><br />
                <?php if (isset($model->carga_aux_din) && $model->carga_aux_din != null) { ?>
                    Auxiliar 1: <?= $model->carga_aux_din; ?><br />
                <?php } ?>
                <?php if (isset($model->carga_aux1_din) && $model->carga_aux1_din != null) { ?>
                    Auxiliar 2: <?= $model->carga_aux1_din; ?><br />
                <?php } ?>
            </td>
            <td style="text-align: center">
                Principal: <?php echo $model->ci_pp_certificada; ?><br />
                <?php if (isset($model->ci_aux_certificada) && $model->ci_aux_certificada != null) { ?>
                    Auxiliar 1: <?= $model->ci_aux_certificada; ?><br />
                <?php } ?>
                <?php if (isset($model->ci_aux1_certificada) && $model->ci_aux1_certificada != null) { ?>
                    Auxiliar 2: <?= $model->ci_aux1_certificada; ?><br />
                <?php } ?>
            </td>
        </tr>
    </table>

    <br />

    <p align="justify">
        CERTIFICO QUE:
        el <?php echo Inspecciones::convertirFecha($serv->inspeccion0->getUltimaVisita()->fecha); ?>, el medio de izado terrestre descrito,
        fue inspeccionado por una persona competente acreditada y que el
        examen al que fue sometido, despu&eacute;s de las correspondientes pruebas,
        dio como resultado que hubo de resistir la carga de prueba, sin sufrir
        da&ntilde;os, ni deformaciones residuales y que su capacidad de izado certificada
        es la que se indica en la columna IV.
    </p>
    <p align="justify">
        El presente certificado tendr&aacute; una vigencia de <strong>cinco (5)</strong> meses a partir de su expedici&oacute;n,
        hasta que sea sustituido por un Certificado de Homologaci&oacute;n definitivo,
        que expedir&aacute; la Oficina de Inspecci&oacute;n del RCB.
    </p>
    <p align="justify">
        Este Certificado no es v&aacute;lido para realizar operaciones que impliquen la carga de personal.
    </p>

    <div class="row">
        <div class="col-sm-12">
            <h5>
                Fecha Expedici&oacute;n:
                <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
            </h5>
        </div>
    </div>

    <br />

    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php echo $model->funcionario; ?>
                    <br />
                    Inspector Actuante
                </span>
            </td>
        </tr>
    </table>

</page>