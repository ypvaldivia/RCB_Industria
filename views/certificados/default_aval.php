<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<!-- BEGIN PAGE HEADER-->
<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            Inspecciones de Personas <small> Gesti&oacute;n de la informaci&oacute;n de Certificados</small>
            <?php $this->endBlock(); ?>

            <?php $this->beginBlock('block_migas'); ?>
            <i class="icon-barcode"></i>
            <a href="<?php echo Url::to('/inspecciones/indexMits') ?>">Inspecciones - </a>
            <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Medios de Izado - </a><i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">Certificados </a>
            </li>
            <li class="btn-group">
                <a href="<?php echo Url::to('/inspecciones/detallesMit', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-primary">
                    <span style="color: #FFFFFF"><i class="icon-arrow-left"></i>Atras</span></a>
            </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Certificado de Inspecci&oacute;n</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">

                <div class="container">
                    <div class="form-body">
                        <!-- BEGIN FORM-->
                        <?php
                        $form = ActiveForm::begin([
                            'id' => 'certificadoPersonasProvisional-form',
                            'enableAjaxValidation' => false,
                            'enableClientValidation' => true,
                            'htmlOptions' => ['enctype' => 'multipart/form-data']
                        ]);
                        ?>

                        <center>
                            <img src="<?= Yii::getAlias('@web/uploads') ?>/images/bannerHeader.jpg" width="80%">
                        </center>

                        <div class="row">
                            <div class="col-sm-12">
                                <center>
                                    <h2>
                                        <span style="font-variant: all-small-caps">
                                            AVAL DE IMPORTACI&Oacute;N PARA MEDIOS DE IZADO TERRESTRES
                                        </span>
                                    </h2>
                                </center>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-10">&nbsp;</div>
                            <div class="col-sm-2 float-left">
                                <h5>
                                    N&deg;:
                                    <span style="text-decoration: underline">
                                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                                    </span>
                                </h5>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    Expedido en virtud de lo dispuesto en el inciso 1 (uno) del art&iacute;culo 71.1 (setenta y un punto 1)
                                    del REGLAMENTO PARA LA EXPLOTACI&Oacute;N DE MEDIOS DE IZADO (Resoluci&oacute;n No. 293/2014 MITRANS),
                                    con la facultad conferida por el Registro Cubano de Buques, nombrada como la Autoridad Competente del
                                    referido reglamento de acuerdo a su inciso 11 (once) del art&iacute;culo 7 (siete).
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <h5>
                                    <b>Datos de identificaci&oacute;n del medio de izado terrestre</b>
                                </h5>
                            </div>
                        </div>

                        <br />

                        <?php $i = 1 ?>
                        <table border="1" width="100%">
                            <tr>
                                <td style="text-align: left; width: 5%"><?= $i ?></td>
                                <td style="text-align: left; width: 25%">Tipo de medio de izado </td>
                                <td style="text-align: left; width: 70%"><?php echo $model->tipo; ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                        echo $i ?></td>
                                <td style="text-align: left; width: 25%">Fabricante y modelo</td>
                                <td style="text-align: left; width: 70%"><?php echo $model->objeto; ?></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                        echo $i ?></td>
                                <td style="text-align: left; width: 25%">Importador</td>
                                <td style="text-align: left; width: 70%">
                                    <?= (isset($model->importador) && $model->importador != null) ? $model->importador : Html::activeTextInput($model, 'importador', array('class' => 'form-control input-large', 'placeholder' => 'Importador')); ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                        echo $i ?></td>
                                <td style="text-align: left; width: 25%">Destinatario</td>
                                <td style="text-align: left; width: 70%">
                                    <?= (isset($model->destinatario) && $model->destinatario != null) ? $model->destinatario : Html::activeTextInput($model, 'destinatario', array('class' => 'form-control input-large', 'placeholder' => 'Destinatario')); ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                        echo $i ?></td>
                                <td style="text-align: left; width: 25%">Capacidad de trabajo nominal</td>
                                <td style="text-align: left; width: 70%"><?php echo $model->capacidad; ?> Kg</td>
                            </tr>
                            <?php if (!isset($model->vtraslacion)) { ?>
                                <tr>
                                    <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                            echo $i ?></td>
                                    <td style="text-align: left; width: 25%">Velocidad de traslaci&oacute;n</td>
                                    <td style="text-align: left; width: 70%">
                                        <?= Html::activeTextInput($model, 'vtraslacion', array('class' => 'form-control input-small', 'placeholder' => 'm/s')); ?>
                                    </td>
                                </tr>
                            <?php } else { ?>
                                <?php if ($model->vtraslacion != null) { ?>
                                    <tr>
                                        <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                                echo $i ?></td>
                                        <td style="text-align: left; width: 25%">Velocidad de traslaci&oacute;n</td>
                                        <td style="text-align: left; width: 70%">
                                            <?= $model->vtraslacion; ?> m/s
                                        </td>
                                    </tr>
                            <?php }
                            } ?>
                            <?php if (!isset($model->vizado)) { ?>
                                <tr>
                                    <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                            echo $i ?></td>
                                    <td style="text-align: left; width: 25%">Velocidad de izado</td>
                                    <td style="text-align: left; width: 70%">
                                        <?= Html::activeTextInput($model, 'vizado', array('class' => 'form-control input-small', 'placeholder' => 'm/s')); ?>
                                    </td>
                                </tr>
                            <?php } else { ?>
                                <?php if ($model->vizado != null) { ?>
                                    <tr>
                                        <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                                echo $i ?></td>
                                        <td style="text-align: left; width: 25%">Velocidad de izado</td>
                                        <td style="text-align: left; width: 70%">
                                            <?= $model->vizado; ?> m/s
                                        </td>
                                    </tr>
                            <?php }
                            } ?>
                            <?php if (!isset($model->vdescenso)) { ?>
                                <tr>
                                    <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                            echo $i ?></td>
                                    <td style="text-align: left; width: 25%">Velocidad de descenso</td>
                                    <td style="text-align: left; width: 70%">
                                        <?= Html::activeTextInput($model, 'vdescenso', array('class' => 'form-control input-small', 'placeholder' => 'm/s')); ?>
                                    </td>
                                </tr>
                            <?php } else { ?>
                                <?php if ($model->vdescenso != null) { ?>
                                    <tr>
                                        <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                                echo $i ?></td>
                                        <td style="text-align: left; width: 25%">Velocidad de descenso</td>
                                        <td style="text-align: left; width: 70%">
                                            <?= $model->vdescenso; ?> m/s
                                        </td>
                                    </tr>
                            <?php }
                            } ?>
                            <?php if (!isset($model->vgiro) || $model->vgiro = null) { ?>
                                <tr>
                                    <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                            echo $i ?></td>
                                    <td style="text-align: left; width: 25%">Velocidad de giro</td>
                                    <td style="text-align: left; width: 70%">
                                        <?= Html::activeTextInput($model, 'vgiro', array('class' => 'form-control input-small', 'placeholder' => 'm/s')); ?>
                                    </td>
                                </tr>
                            <?php } else { ?>
                                <?php if ($model->vgiro != null) { ?>
                                    <tr>
                                        <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                                echo $i ?></td>
                                        <td style="text-align: left; width: 25%">Velocidad de giro</td>
                                        <td style="text-align: left; width: 70%">
                                            <?= $model->vgiro; ?> r/s
                                        </td>
                                    </tr>
                            <?php }
                            } ?>
                            <?php if (!isset($model->htrabajo)) { ?>
                                <tr>
                                    <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                            echo $i ?></td>
                                    <td style="text-align: left; width: 25%">Altura m&aacute;xima de trabajo</td>
                                    <td style="text-align: left; width: 70%">
                                        <?= Html::activeTextInput($model, 'htrabajo', array('class' => 'form-control input-small', 'placeholder' => 'metros')); ?>
                                    </td>
                                </tr>
                            <?php } else { ?>
                                <?php if ($model->htrabajo != null) { ?>
                                    <tr>
                                        <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                                echo $i ?></td>
                                        <td style="text-align: left; width: 25%">Altura m&aacute;xima de trabajo</td>
                                        <td style="text-align: left; width: 70%">
                                            <?= $model->htrabajo; ?> metros
                                        </td>
                                    </tr>
                            <?php }
                            } ?>
                            <tr>
                                <td style="text-align: left; width: 5%"><?php $i = $i + 1;
                                                                        echo $i ?></td>
                                <td style="text-align: left; width: 25%">N&uacute;mero de unidades</td>
                                <td style="text-align: left; width: 70%">
                                    <?= (isset($model->nunidades) && $model->nunidades != null) ? $model->nunidades : Html::activeTextInput($model, 'nunidades', array('class' => 'form-control input-small', 'placeholder' => '#')); ?>
                                </td>
                            </tr>
                        </table>

                        <br />

                        <div class="row">
                            <div class="col-sm-12">
                                <p align="justify">
                                    El presente Aval es v&aacute;lido para importar el tipo de medio de izado en la cantidad que est&aacute; relacionada en
                                    el inciso <?= $i ?> de la tabla anterior.
                                </p>
                                <p align="justify">
                                    El Aval, no garantiza la calidad de las piezas, agregados, dispositivos, u otros elementos que componen
                                    el medio de izado, as&iacute; como el nivel de calidad del propio medio
                                </p>
                                <p align="justify">
                                    La emisi&oacute;n del Aval no implica que el medio de izado se encuentra apto para ser operado, esta condici&oacute;n
                                    se verificar&aacute; al someterse el medio al cumplimiento de los establecido en la Resoluci&oacute;n 293/2014 del
                                    MITRANS en cuanto a la entrega del Certificado de Seguridad T&eacute;cnica.
                                </p>
                            </div>
                        </div><br />


                        <?php if (isset($model->expedicion) && $model->expedicion != null) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                        <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                                    </h5>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="row">
                                <div class="col-sm-2">
                                    <h5>
                                        Fecha Expedici&oacute;n:
                                    </h5>
                                </div>
                                <div class="col-sm-10">
                                    <?php echo Html::activeTextInput($model, 'expedicion', array('class' => 'form-control form-control input-medium date-picker', 'data-date-format' => 'yyyy-mm-dd')); ?>
                                </div>
                            </div>
                        <?php } ?>

                        <p align="justify">
                            La vigencia del Aval expira al recibir la Entidad Importadora el medio de izado aprobado.
                        </p>


                        <br /><br />

                        <div class="row">
                            <div class="col-sm-8">&nbsp;</div>
                            <div class="col-sm-4">
                                <center>
                                    <hr />
                                </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">&nbsp;</div>
                            <div class="col-sm-4">
                                <center>
                                    <?php
                                    if ($model->nivelAprobacion != null) {
                                        //Validar aqui que el funcionario este activo
                                        if ($model->nivelAprobacion[0]->funcionario0->inactivo != 1) {
                                            echo $model->nivelAprobacion[0]->funcionario0->nombre_apellidos;
                                            echo '<br/>';
                                            echo $model->nivelAprobacion[0]->funcionario0->cargo0->cargo;
                                        } else {
                                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->nombre_apellidos;
                                            echo '<br/>';
                                            echo $model->nivelAprobacion[0]->funcionario0->getSustituto()->cargoFull;
                                        }
                                    }
                                    ?>
                                </center>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-1">R2/IC405</div>
                            <div class="col-sm-11">&nbsp; </div>
                        </div>

                    </div>

                </div>
                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-2">
                                <?php if ($model->control == 0) { ?>
                                    <button id="inspeccion_servicios_submit" type="submit" class="btn btn-primary"><i class="icon-ok"></i>Vista Previa del Certificado</button>
                                <?php } elseif ($model->control == 1) { ?>
                                    <a href="<?php echo Url::to('/certificados/PDFAvalMIT', array('o' => $serv->id)) ?>" class="btn btn-success"><i class="fa fa-file"></i> Generar Certificado</a>
                                    |
                                    <a href="<?php echo Url::to('/certificados/certificado', array('obj' => $serv, 'tipoObj' => 5, 'tipoCe' => 2)) ?>" class="btn btn-danger"><i class="icon-repeat"></i> Corregir Certificado</a>
                                <?php } ?>
                                |
                                <a href="<?php echo Url::to('/inspecciones/detallesMit', array('id' => $serv->inspeccion0->id)) ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-validation/dist/jquery.validate.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-validation/dist/additional-methods.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/form-validation-entidades.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-print-page/jquery.printPage.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/custom.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
FormComponents.certificado(); Custom.InitPrint(); 
JS;
$this->registerJs($js);
?>