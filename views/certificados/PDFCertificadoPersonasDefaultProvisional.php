<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">R3/PC401</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 100%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h4 style="text-align: center;">
                    CERTIFICADO DE HOMOLOGACI&Oacute;N DE
                    <?php echo strtoupper($serv->inspeccion0->homologacion0->titulo_anuario); ?>
                    <br />
                    PROVISIONAL
                </h4>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 85%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                    </span>
                </h5>
            </td>
        </tr>
        <tr>
            <td style="width: 70%">
                <h5>
                    Que se otorga al Sr.:
                    <span style="text-decoration: underline"><?php echo $serv->persona0->nombre_apellidos; ?></span>
                    <br /><br />
                    N&deg; de Identidad Permanente:
                    <span style="text-decoration: underline"><?php echo $serv->persona0->ci; ?></span>
                    <br /><br />
                    De la Entidad:
                    <span style="text-decoration: underline"><?php echo $serv->inspeccion0->entidad0->nombre; ?></span>
                </h5>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>

    <p align="justify">
        Por aprobar de forma satisfactoria la evaluaci&oacute;n para la homologaci&oacute;n de
        <span style="text-decoration: underline"><?php echo $serv->inspeccion0->homologacion0->titulo_anuario; ?></span>
        y cumplir con los requisitos del Registro Cubano de Buques
        especificados en
        <?php echo $model->servicio; ?>
        para realizar los servicios de:
        <h3 style="text-align: center;">
            <?php echo $model->documentacion; ?></h3>
    </p>


    <div class="row">
        <div class="col-sm-12">
            <p align="justify">
                El presente certificado tendrá una vigencia de <strong>cinco (5)</strong> meses a partir de su expedición,
                hasta que sea sustituido por un Certificado de Homologación definitivo,
                que expedirá la Oficina de Inspección del <img src="<?php echo Yii::getAlias('@web') ?>/themes/metronic/assets/img/certificados_inspecciones/logo-cuerpo-cert-rcb.jpg" style="width: 200px">.
            </p>
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-sm-12">
            <h5>
                Fecha Expedici&oacute;n:
                <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
            </h5>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h5>
                Fecha de Vencimiento: <i>5 meses (hasta la emisi&oacute;n del Certificado definitivo)</i>
            </h5>
        </div>
    </div>

    <br /><br /><br />

    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php echo $model->funcionario; ?>
                    <br />
                    Inspector Actuante
                </span>
            </td>
        </tr>
    </table>

</page>