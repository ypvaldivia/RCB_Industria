<style type="text/css">
    table {

        width: 100%;
        border-collapse: collapse;
        border-spacing: 0px;
        max-width: 100%;
    }

    table tr {
        margin: 5px auto;
    }

    table td {
        padding: 6px 2px;
        line-height: 1.42857;

    }

    table th {
        padding: 4px 2px;
        background: #bababa;
        font-weight: bold;
    }
</style>
<!-- BEGIN CONTAINER -->
<page backtop="10mm" backbottom="10mm" backleft="10mm" backright="10mm">
    <page_header>

    </page_header>
    <page_footer>
        <hr style="margin: 2px !important; color: #CCCCCC; margin-top:10px;font-weight:lighter;" />
        <table style="width: 100%; border:none;">
            <tr>
                <td style="text-align: left; width: 50%;color: #666666;">&nbsp;</td>
                <td style="text-align: right; width: 50%; color: #666666;">Registro Cubano de Buques</td>
            </tr>
        </table>
    </page_footer>

    <table style="border:none; width: 100%">
        <tr>
            <td style="width: 100%; align-content: center;">
                <img src="<?php echo Yii::getAlias('@web') ?>/uploads/images/bannerHeader.jpg" style="width: 99%">
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td style="width: 100%; align-content: center; font-weight: lighter">
                <h3 style="text-align: center;">
                    CERTIFICADO PROVISIONAL DE APROBACI&Oacute;N DE TIPO
                </h3>
            </td>
        </tr>
    </table>

    <table style="border:none;">
        <tr>
            <td style="width: 85%">
                &nbsp;
            </td>
            <td>
                <h5>
                    N&deg;:&nbsp;
                    <span style="text-decoration: underline">
                        <?php echo $serv->inspeccion0->numero_control; ?> - <?php echo $serv->numero; ?>
                    </span>
                </h5>
            </td>
        </tr>
        <tr>
            <td style="width: 70%">
                <h5>
                    <b>ESTE CERTIFICADO SE EMITE A:</b>
                    <br />
                    <br />
                    <b>SOLICITANTE:</b>
                    <span style="text-decoration: underline">
                        <?php echo $serv->producto0->propietario0->nombre; ?>
                    </span>
                </h5>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>

    <p align="justify">
        Por obtener resultados satisfactorios en las evaluaciones realizadas durante el Proceso de Aprobaci&oacute;n de Tipo,
        por ser conforme con <?= $model->servicio; ?> y cumplir con los requisitos del RCB Sociedad Clasificadora.
    </p>

    <p align="justify">
        Se le otorga al producto:
    </p>
    <p style="text-align: center; align-content: center; font-weight: bold">
        <?= strtoupper($serv->producto0->nombre); ?>
    </p>

    <br /><br />

    <p align="justify">
        El presente Certificado se expide en base al Informe de Inspección N&deg;: &nbsp;
        <span style="text-decoration: underline; font-weight: bold">
            <?= $serv->inspeccion0->numero_control; ?>
        </span>
    </p>

    <div class="row">
        <div class="col-sm-12">
            <p align="justify">
                El presente certificado tendrá una vigencia de <strong>cinco (5)</strong> meses a partir de su expedición,
                hasta que sea sustituido por un Certificado de Homologación definitivo,
                que expedirá la Oficina de Inspección del <img src="<?php echo Yii::getAlias('@web') ?>/themes/metronic/assets/img/certificados_inspecciones/logo-cuerpo-cert-rcb.jpg" style="width: 200px">.
            </p>
        </div>
    </div>

    <br /><br /><br />
    <div class="row">
        <div class="col-sm-12">
            <h5>
                Fecha Expedici&oacute;n:
                <span style="text-decoration: underline"><?php echo $model->expedicion; ?></span>
                <br /><br />
                Fecha Vencimiento:
                <span style="text-decoration: underline">5 meses (hasta la emisi&oacute;n del Certificado definitivo)</span>
            </h5>
        </div>
    </div>

    <br /><br /><br />
    <br /><br /><br />
    <br /><br /><br />


    <table style="border:none;">
        <tr>
            <td style="width: 60%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align: center; align-content: center">
                <hr style="width: 100%" />
                <span style="text-align: center">
                    <?php echo $model->funcionario; ?>
                    <br />
                    Inspector Actuante
                </span>
            </td>
        </tr>
    </table>

</page>