<?php

use app\assets\DataTablesAsset;
use app\models\AcreditacionesIndustriales;
use app\models\Oficinas;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Personal RCB
<small> Gesti&oacute;n de la informaci&oacute;n del Personal del RCB</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#"><i class="fa fa-cogs"></i>Estructura y Servicios </a>
<span class="breadcumb-item active">Listado de Personas</span>
<?php $this->endBlock(); ?>

<!-- BUSCADOR-->
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h6 class="card-title">
            <i class="fa fa-search"></i> B&uacute;squeda
        </h6>
    </div>
    <div class="card-body">
        <?php $form = ActiveForm::begin(['options' => ['class' => 'form-inline'],]) ?>
        <?php echo Html::activeDropDownList($model, 'oficina', ArrayHelper::map(Oficinas::find()->all(), 'id', 'codigo'), array('class' => 'form-control mr-2 select2me', 'style' => 'width:200px !important', 'prompt' => 'Seleccione la Oficina')); ?>
        <?php echo Html::activeDropDownList($model, 'servicio', ArrayHelper::map(AcreditacionesIndustriales::find()->all(), 'id', 'titulo_inspector'), array('class' => 'form-control mr-2 select2me', 'style' => 'width:200px !important', 'prompt' => 'Seleccione el Servicio')); ?>
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary mb-2 my-3']) ?>
        <?php ActiveForm::end() ?>
    </div>
</div>
<!-- END OF BUSCADOR-->
<br />
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-globe"></i>Gesti&oacute;n del Personal</h3>
    </div>
    <div class="card-body">
        <div class="table-toolbar">
            <div class="btn-group">
                <a href="<?php echo Url::to('/funcionarios/create') ?>" class="btn btn-success">Nuevo <i class="fa fa-plus"></i></a>
                <a id="eliminar" href="#" class="btn btn-danger">Eliminar seleccionados <i class="fa fa-trash"></i></a>
            </div>
            <div class="btn-group float-right">
                <button class="btn dropdown-toggle light-blue" data-toggle="dropdown">Exportar <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu float-right">
                    <li><a href="#" onClick="$('#main_table').tableExport({type:'png',escape:'false'});"> <i class="fa fa-camera"></i> PNG</a></li>
                    <li><a href="#" onClick="$('#main_table').tableExport({type:'pdf',escape:'false',htmlContent:'true'});"> <i class="fa fa-file"></i> PDF</a></li>
                    <li><a href="#" onClick="$('#main_table').tableExport({type:'excel',escape:'false'});"> <i class="fa fa-table"></i> XLS</a></li>
                </ul>
            </div>
        </div>
        <?php if (Yii::$app->session->hasFlash('success')) { ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
            </div>
        <?php } else if (Yii::$app->session->hasFlash('error')) { ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
            </div>
        <?php } ?>
        <?php if ($data != null) { ?>
            <table class="table table-striped table-bordered table-hover" id="main_table">
                <thead>
                    <tr>
                        <th class="table-checkbox">
                            <input id="select_all" type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                        </th>
                        <th> &nbsp;</th>
                        <th> Nombre Apellidos</th>
                        <th> C&oacute;digo</th>
                        <th> Oficina</th>
                        <th> Cargo</th>
                        <th> Acreditaciones</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($data as $value) {
                    ?>
                        <tr class="odd gradeX">
                            <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id ?>" /></td>
                            <td>
                                <?php
                                if ($value->inactivo == 1) {
                                    echo '<span class="label label-sm label-danger tooltips" title="Inactivo">&cross;</span>';
                                } else {
                                    echo '<span class="label label-sm label-success tooltips" title="Activo">&check;</span>';
                                }
                                ?>
                            </td>
                            <td><?php echo $value->nombre_apellidos ?></td>
                            <td><?php echo $value->codigo ?></td>
                            <td><?php echo $value->oficina0->codigo ?></td>
                            <td><?php echo $value->cargoFull ?></td>
                            <td>
                                <span class="label label-sm label-info tooltips" title="Totales"> Total:<?php echo $value->getTotalAcreditaciones(); ?></span> |
                                <span class="label label-sm label-success tooltips" title="Vigentes"><?php echo $value->getTotalAcreditacionesVigentes(); ?></span>
                                <span class="label label-sm label-warning tooltips" title="Pr&oacute;ximas a vencer"><?php echo $value->getTotalAcreditacionesPorVencer(); ?></span>
                                <span class="label label-sm label-danger tooltips" title="Vencidas"><?php echo $value->getTotalAcreditacionesVencidas(); ?></span>
                            </td>
                            <td>
                                <a href="<?php echo Url::to('/funcionarios/update', array('id' => $value->id)) ?>" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Editar</a>
                                <a href="<?php echo Url::to('/acreditaciones-inspectores/index', array('idInspector' => $value->id)) ?>" class="btn btn-primary btn-sm"><i class="icon-folder-open-alt"></i> Acreditaciones</a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } else { ?>
            <h4><span class="label label-danger"> No se encontraron resultados</span></h4>
        <?php } ?>
    </div>
</div>
<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
    } );
JS;
$this->registerJs($js); ?>