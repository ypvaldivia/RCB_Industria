<?php

use app\models\CargosFuncionarios;
use app\models\CategoriasFuncionarios;
use app\models\Oficinas;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption">
                    <?php if ($model->isNewRecord) { ?>
                        <i class="fa fa-plus"></i> Adicionar funcionario

                    <?php } else { ?>
                        <i class="fa fa-edit"></i> Editando funcionario ( <?php echo $model->nombre_apellidos ?> )
                    <?php } ?>
                </div>
                <div class="tools">
                    <a href="javascript:" class="collapse"></a>
                    <a href="javascript:" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">

                <?php if (Yii::$app->session->hasFlash('success')) { ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
                    </div>
                <?php } else if (Yii::$app->session->hasFlash('error')) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
                    </div>
                <?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
                    <div class="alert alert-warning alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
                    </div>
                <?php } ?>
                <!-- BEGIN FORM-->
                <?php
                $form = ActiveForm::begin([
                    'id' => 'funcionarios-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                    'htmlOptions' => ['enctype' => 'multipart/form-data']
                ]);
                ?> <div class="container">
                    <?php echo Html::errorSummary($model); ?>
                </div>
                <div class="form-body">

                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php echo Html::activeLabel($model, 'nombre_apellidos', array('class' => 'control-label')); ?>
                                        <?php echo Html::activeTextInput($model, 'nombre_apellidos', array('class' => 'form-control', 'placeholder' => 'nombre_apellidos', 'maxlength' => 100)); ?>
                                        <span class="help-block"><?php echo Html::error($model, 'nombre_apellidos', array('class' => 'text-danger')); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-8">
                                    <div class="form-group">
                                        <?php echo Html::activeLabel($model, 'codigo', array('class' => 'control-label')); ?>
                                        <?php echo Html::activeTextInput($model, 'codigo', array('class' => 'form-control', 'placeholder' => 'codigo', 'maxlength' => 2)); ?>
                                        <span class="help-block"><?php echo Html::error($model, 'codigo', array('class' => 'text-danger')); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-4">
                                    <div class="form-group">
                                        <?php echo Html::activeLabel($model, 'es_inspector', array('class' => 'control-label', 'style' => 'display:block')); ?>
                                        <div class="make-switch" data-on-label="<i class='icon-ok icon-white'></i>" data-off-label="<i class='icon-remove'></i>">
                                            <?php
                                            $checked = '';
                                            if (!$model->isNewRecord && $model->es_inspector == 1) {
                                                $checked = 'checked';
                                            }
                                            ?>
                                            <?php echo Html::activeCheckbox($model, 'es_inspector', array('class' => 'toggle', 'checked' => $checked)); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if (!$model->isNewRecord) { ?>
                                    <div class="col-md-3 col-xs-4">
                                        <div class="form-group">
                                            <?php echo Html::activeLabel($model, 'inactivo', array('class' => 'control-label', 'style' => 'display:block')); ?>
                                            <div class="make-switch" data-on-label="<i class='icon-ok icon-white'></i>" data-off-label="<i class='icon-remove'></i>">
                                                <?php
                                                $checked = '';
                                                if ($model->inactivo == 1) {
                                                    $checked = 'checked';
                                                }
                                                ?>
                                                <?php echo Html::activeCheckbox($model, 'inactivo', array('class' => 'toggle', 'checked' => $checked)); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php echo Html::activeLabel($model, 'oficina', array('class' => 'control-label')); ?>
                                        <?php echo Html::activeDropDownList($model, 'oficina0', ArrayHelper::map(Oficinas::find()->all(), 'id', 'codigo'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                        <span class="help-block"><?php echo Html::error($model, 'oficina', array('class' => 'text-danger')); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php echo Html::activeLabel($model, 'categoria', array('class' => 'control-label')); ?>
                                        <?php echo Html::activeDropDownList($model, 'categoria0', ArrayHelper::map(CategoriasFuncionarios::find()->all(), 'id', 'categoria'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                        <span class="help-block"><?php echo Html::error($model, 'categoria', array('class' => 'text-danger')); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?php echo Html::activeLabel($model, 'cargo', array('class' => 'control-label')); ?>
                                        <?php echo Html::activeDropDownList($model, 'cargo0', ArrayHelper::map(CargosFuncionarios::find()->all(), 'id', 'cargo'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                        <span class="help-block"><?php echo Html::error($model, 'cargo', array('class' => 'text-danger')); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">

                                <?php echo Html::activeLabel($model, 'imagen_firma', array('class' => 'control-label')); ?>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                        <?php if (!$model->imagen_firma) { ?>
                                            <img title="" src="<?= Yii::getAlias('@web/themes/sbadmin') ?>/assets/img/no-image.png" alt="NO hay imagen">

                                        <?php } else { ?>
                                            <img title="" src="<?= Yii::getAlias('@web/uploads') ?>/images/funcionarios/<?php echo $model->imagen_firma ?>" alt="">
                                        <?php  } ?>
                                    </div>
                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    <div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar Imagen</span>
                                            <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                            <?php echo $form->fileField($model, 'imagen_firma', array('id' => 'maxlength_alloptions', 'class' => 'default', 'maxlength' => 45)); ?>
                                        </span>
                                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Quitar</a>
                                    </div>
                                    <span class="help-block"><?php echo Html::error($model, 'imagen_firma', array('class' => 'text-danger')); ?></span>
                                </div>
                                <span class="label label-warning">Importante!</span>
                                <span>La previsualizaci&oacute;n de im&aacute;genes solo funciona en navegadores modernos.</span>
                            </div>

                        </div>
                    </div>
                    <hr />
                    <?php if ($model->isNewRecord || $model->usuario0 == null) { ?>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php echo Html::activeLabel($model, 'username', array('class' => 'control-label')); ?>
                                    <?php echo Html::activeTextInput($model, 'username', array('class' => 'form-control', 'placeholder' => 'Escriba el nombre de usuario', 'maxlength' => 100)); ?>
                                    <span class="help-block"><?php echo Html::error($model, 'username', array('class' => 'text-danger')); ?></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php echo Html::activeLabel($model, 'password', array('class' => 'control-label')); ?>
                                    <?php echo Html::activePasswordInput($model, 'password', array('class' => 'form-control', 'placeholder' => 'Escriba su contraseña', 'maxlength' => 100)); ?>
                                    <span class="help-block"><?php echo Html::error($model, 'password', array('class' => 'text-danger')); ?></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php echo Html::activeLabel($model, 'password2', array('class' => 'control-label')); ?>
                                    <?php echo Html::activePasswordInput($model, 'password2', array('class' => 'form-control', 'placeholder' => 'Repita la contraseña', 'maxlength' => 100)); ?>
                                    <span class="help-block"><?php echo Html::error($model, 'password2', array('class' => 'text-danger')); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php echo Html::activeLabel($model, 'correo', array('class' => 'control-label')); ?>
                                    <?php echo $form->emailField($model, 'correo', array('class' => 'form-control', 'placeholder' => 'Escriba su correo electrónico ', 'maxlength' => 100)); ?>
                                    <span class="help-block"><?php echo Html::error($model, 'correo', array('class' => 'text-danger')); ?></span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?php echo Html::activeLabel($model, 'rol', array('class' => 'control-label')); ?>
                                    <?php echo Html::activeDropDownList($model, 'rol', ArrayHelper::map(Role::getUserRoles(Yii::$app->$user->id), 'name', 'name'), array('class' => 'form-control select2me')); ?>
                                    <span class="help-block"><?php echo Html::error($model, 'rol', array('class' => 'text-danger')); ?></span>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <h3>Informaci&oacute;n de Usuario</h3>
                        <span class="label label-danger">
                            La informaci&oacute;n de este usuario debe ser actualizada en la secci&oacute;n CONFIGURACI&Oacute;N GENERAL con los permisos apropiados.
                        </span>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5><b>Usuario:</b></h5>
                                    <h5><?php echo $model->usuario0->username; ?></h5>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5><b>Correo:</b></h5>
                                    <h5><?php echo $model->usuario0->email; ?></h5>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a href="<?php echo Url::to('/funcionarios/index') ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>


<?php

$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));


$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));

$js = <<<JS
 FormComponents.init();
JS;
$this->registerJs($js);
?>