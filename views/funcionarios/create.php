<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Personal RCB<small> Gesti&oacute;n de la informaci&oacute;n del Personal del RCB</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#"><i class="fa fa-cogs"></i>Estructura y Servicios </a>
<li>
    <a href="<?php echo Url::to('/Funcionarios/index') ?>">Personal RCB </a>
    <i class="fa fa-angle-right"></i>
</li>
<li class="active">
    Nuevo Funcionario
</li>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model, 'buttons' => 'create'));
?>