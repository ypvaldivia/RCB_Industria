<?php

use app\assets\DataTablesAsset;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Inspectores
<small>Proximos Vencimientos</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a class="breadcumb-item" href="#"><i class="fa fa-briefcase"></i> Estructura </a>
<a class="breadcumb-item" href="#">B&uacute;squeda y Alertas </a>
<span class="breadcumb-item"> P&oacute;ximos Vencimientos</span>
<?php $this->endBlock(); ?>

<a href="<?php echo Url::to('/inspectores/proximos-vencimientos') ?>"></a>

<?php
if ($porVencer != false) {
?>
    <div class="row">
        <div class="col-md-12">
            <div class="card border-left-primary shadow">
                <div class="card-header">
                    <div class="caption"><i class="fa fa-globe"></i>Proximos Vencimientos </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="reload"></a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="main_table">
                        <thead>
                            <tr>
                                <th>Oficina</th>
                                <th colspan="2">Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($porVencer as $oficina) {
                            ?>
                                <tr class="odd gradeX">
                                    <td>
                                        <span class="label label-primary tooltips" data-original-title="Oficina">
                                            <strong>
                                                <?php echo $oficina->oficina; ?>
                                            </strong>
                                        </span>
                                    </td>
                                    <td colspan="2">
                                        <span class="label label-info tooltips" data-original-title="Cantidad de inspectores">
                                            <strong>
                                                <?php echo count($oficina->inspectores); ?>
                                            </strong>
                                        </span>
                                    </td>
                                </tr>

                                <?php foreach ($oficina->inspectores as $inspector) { ?>
                                    <thead class="bg-dark">
                                        <th><?php echo $inspector->nombre ?> (<?php echo $inspector->codigo ?>)</th>
                                        <th>Homologaci&oacute;n</th>
                                        <th>Vencimiento</th>
                                    </thead>
                                    <?php foreach ($inspector->homologaciones as $homologacion) { ?>
                                        <tr class="warning">
                                            <td></td>
                                            <td><?php echo $homologacion->servicio ?></td>
                                            <td><?php echo $homologacion->vence ?></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>

                                <?php
                                }
                                ?>

                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php
} else {
?>
    <div class="row">
        <div class="col-md-12">
            <div class="card border-left-primary shadow">
                <div class="card-header">
                    <div class="caption"><i class="fa fa-globe"></i>No se encontraron <strong>Resultados.</strong></div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="reload"></a>
                    </div>
                </div>
                <div class="card-body">

                    <table class="table table-bordered" id="main_table1">

                    </table>
                </div>
            </div>
        </div>
    </div>
<?php
}

DataTablesAsset::register($this);

$js = <<<JS
        $(document).ready(function() {
            var table = $('#main_table').DataTable( {            
                lengthChange: false,
                buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
                
            } );    
            table
                .buttons()
                .container()
                .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
        } );
    JS;
$this->registerJs($js); ?>