<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Inspectores
<small>Vencimientos</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a class="breadcumb-item" href="#"><i class="fa fa-briefcase"></i> Estructura </a>
<a class="breadcumb-item" href="#">B&uacute;squeda y Alertas </a>
<span class="breadcumb-item"> Vencimientos</span>
<?php $this->endBlock(); ?>

<a href="<?php echo Url::to('/inspectores/proximos-vencimientos') ?>"></a>

<?php
if ($vencidas != false) {
?>
    <div class="row">
        <div class="col-md-12">
            <div class="card border-left-primary shadow">
                <div class="card-header">
                    <div class="caption"><i class="fa fa-globe"></i>Proximos Vencimientos </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="reload"></a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered" id="main_table1">
                        <thead>
                            <tr>
                                <th>Oficina</th>
                                <th colspan="2">Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($vencidas as $oficina) { ?>
                                <tr class="odd gradeX">
                                    <td>
                                        <span class="label label-primary tooltips" data-original-title="Oficina">
                                            <strong>
                                                <?php echo $oficina->oficina; ?>
                                            </strong>
                                        </span>
                                    </td>
                                    <td colspan="2">
                                        <span class="label label-info tooltips" data-original-title="Cantidad de inspectores">
                                            <strong>
                                                <?php echo count($oficina->inspectores); ?>
                                            </strong>
                                        </span>
                                    </td>
                                </tr>

                                <?php foreach ($oficina->inspectores as $inspector) { ?>
                                    <thead class="bg-dark">
                                        <th><?php echo $inspector->nombre ?> (<?php echo $inspector->codigo ?>)</th>
                                        <th>Homologaci&oacute;n</th>
                                        <th>Vencimiento</th>
                                    </thead>

                                    <?php foreach ($inspector->homologaciones as $homologacion) { ?>
                                        <tr class="danger">
                                            <td></td>
                                            <td><?php echo $homologacion->servicio ?></td>
                                            <td><?php echo $homologacion->vence ?></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>

                                <?php
                                }
                                ?>

                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php
} else {
?>
    <div class="row">
        <div class="col-md-12">
            <div class="card border-left-primary shadow">
                <div class="card-header">
                    <div class="caption"><i class="fa fa-globe"></i>No se encontraron <strong>Resultados.</strong></div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="reload"></a>
                    </div>
                </div>
                <div class="card-body">

                    <table class="table table-bordered" id="main_table1">

                    </table>
                </div>
            </div>
        </div>
    </div>
<?php
}

$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/data-tables/DT_bootstrap.css', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/data-tables/jquery.dataTables.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/data-tables/DT_bootstrap.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/htmltable_export/tableExport.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/htmltable_export/html2canvas.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/htmltable_export/jquery.base64.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/htmltable_export/jspdf/libs/sprintf.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/htmltable_export/jspdf/jspdf.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/htmltable_export/jspdf/libs/base64.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/scripts/table-managed.js', Yii::$app->session->get('dependencias'));


$js = <<<JS
TableManaged.init();
JS;
$this->registerJs($js);
?>