<?php

use yii\helpers\Url;

?>
<?php $this->beginBlock('block_titulo_pagina'); ?>
Oficinas <small>Gesti&oacute;n de la informaci&oacute;n de Oficinas</small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<li>
    <i class="fa fa-briefcase"></i>
    <a href="#">Estructuras y Servicios - </a>
    <a href="<?php echo Url::to('/Oficinas/index') ?>">Oficinas </a>
    <a href="#">Actualizar </a>
</li>
<?php $this->endBlock(); ?>
<?php
echo $this->render('_form', array('model' => $model));
?>