<?php

use yii\helpers\Url;

?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Servicios de Industria
<small>Gesti&oacute;n de la informaci&oacute;n de los servicios que brinda el Grupo de Industria </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#" class="breadcrumb-item">Categor&iacute;as de Medios de Izado</a>
<a href="#" class="breadcrumb-item">Estructuras y Servicios</a>
<span class="breadcrumb-item active">Nuevo</span>
<?php $this->endBlock();

echo $this->render('_form', ['model' => $model, 'buttons' => 'create']);

?>