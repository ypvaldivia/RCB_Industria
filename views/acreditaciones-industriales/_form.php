<?php

use app\models\CategoriasAcreditaciones;
use app\models\NivelesInspeccion;
use app\models\TiposCertificados;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>
<div class="row">
    <div class="col-md-12">
        <div class="card border-left-primary shadow">
            <div class="card-header">
                <div class="caption"><i class="icon-reorder"></i>Servicio de Industria</div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                    <a href="javascript:;" class="reload"></a>
                </div>
            </div>
            <div class="card-body form">
                <!-- BEGIN FORM-->
                <?php
                $form = ActiveForm::begin([
                    'id' => 'acreditaciones-industriales-form',
                    'enableAjaxValidation' => false,
                    'enableClientValidation' => true,
                ]);
                ?> <div class="container">
                    <?php echo Html::errorSummary($model); ?>
                </div>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'codigo', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'codigo', array('class' => 'form-control', 'placeholder' => 'codigo', 'maxlength' => 45)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'codigo', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'titulo_inspector', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'titulo_inspector', array('class' => 'form-control', 'placeholder' => 'titulo_inspector', 'maxlength' => 150)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'titulo_inspector', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'titulo_anuario', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextInput($model, 'titulo_anuario', array('class' => 'form-control', 'placeholder' => 'titulo_anuario', 'maxlength' => 150)); ?>
                                <span class="help-block"><?php echo Html::error($model, 'titulo_anuario', array('class' => 'text-danger')); ?>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'map', array('class' => 'control-label')); ?>
                                <?php echo Html::activeTextArea($model, 'map', array('class' => 'form-control')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'map', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'nivelesInspeccion', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'nivelesInspeccion', ArrayHelper::map(NivelesInspeccion::find()->all(), 'id', 'nivel'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'niveles_inspeccion_id', array('class' => 'text-danger')); ?></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'tipo', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList(
                                    $model,
                                    'tipo',
                                    $model->getTipoAcreditacion(),
                                    array(
                                        'class' => 'form-control select2me',
                                        'data-placeholder' => 'Seleccione un elemento...',
                                        'options' => $model->getTipoSeleccionado(),
                                    )
                                ); ?>
                                <span class="help-block"><?php echo Html::error($model, 'tipo', array('class' => 'text-danger')); ?></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'categoria', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList(
                                    $model,
                                    'categoria',
                                    ArrayHelper::map(CategoriasAcreditaciones::find()->all(), 'id', 'grupo'),
                                    array(
                                        'class' => 'form-control select2me',
                                        'data-placeholder' => 'Seleccione un elemento...',
                                    )
                                ); ?>
                                <span class="help-block"><?php echo Html::error($model, 'categoria', array('class' => 'text-danger')); ?></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo Html::activeLabel($model, 'tipo_certificado', array('class' => 'control-label')); ?>
                                <?php echo Html::activeDropDownList($model, 'tipoCertificado', ArrayHelper::map(TiposCertificados::find()->all(), 'id', 'titulo'), array('class' => 'form-control select2me', 'data-placeholder' => 'Seleccione un elemento...')); ?>
                                <span class="help-block"><?php echo Html::error($model, 'tipo_certificado', array('class' => 'text-danger')); ?></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-actions fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-primary"><i class="icon-ok"></i> Guardar</button>
                                <a href="<?php echo Url::to('/acreditaciones-industriales/index') ?>" class="btn btn-default">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end() ?>
            </div>
            <!-- form -->
        </div>
    </div>
</div>


<?php

$this->registerCssFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/gritter/css/jquery.gritter.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/select2/select2_metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/clockface/css/clockface.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datepicker/css/datepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-timepicker/compiled/timepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-colorpicker/css/colorpicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-datetimepicker/css/datetimepicker.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-multi-select/css/multi-select.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/jquery-tags-input/jquery.tagsinput.css', Yii::$app->session->get('dependencias'));
$this->registerCssFile('@web/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/plugins/fuelux/js/spinner.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/ckeditor/ckeditor.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-fileupload/bootstrap-fileupload.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/select2/select2.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/clockface/js/clockface.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/moment.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-daterangepicker/daterangepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.input-ip-address-control-1.0.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.multi-select.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-multi-select/js/jquery.quicksearch.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery.pwstrength.bootstrap/src/pwstrength.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/jquery-tags-input/jquery.tagsinput.min.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-markdown/js/bootstrap-markdown.js', Yii::$app->session->get('dependencias'));
$this->registerJsFile('@web/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js', Yii::$app->session->get('dependencias'));

$this->registerJsFile('@web/scripts/form-components.js', Yii::$app->session->get('dependencias'));
