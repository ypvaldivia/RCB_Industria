<?php

use app\assets\DataTablesAsset;
use yii\helpers\Url;
use app\components\UserComponent;

?>

<?php $this->beginBlock('block_titulo_pagina'); ?>
Servicios de Industria <small>Gesti&oacute;n de la informaci&oacute;n de los servicios que brinda el Grupo de Industria </small>
<?php $this->endBlock(); ?>

<?php $this->beginBlock('block_migas'); ?>
<a href="#" class="breadcrumb-item">Categor&iacute;as de Medios de Izado</a>
<a href="#" class="breadcrumb-item">Estructuras y Servicios</a>
<span class="breadcrumb-item active">Servicios de Industria</span>
<?php $this->endBlock(); ?>

<?php if (Yii::$app->session->hasFlash('success')) { ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>&Eacute;xito!</strong> <?php echo Yii::$app->session->getFlash('success'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('error')) { ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error</strong> <?php echo Yii::$app->session->getFlash('error'); ?>
    </div>
<?php } else if (Yii::$app->session->hasFlash('warning')) { ?>
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Alerta!</strong> <?php echo Yii::$app->session->getFlash('warning'); ?>
    </div>
<?php } ?>

<br />
<div class="card border-left-primary shadow">
    <div class="card-header">
        <h3 class="card-title"><i class="fa fa-globe"></i>Gesti&oacute;n de Servicios de Industria
        </h3>
    </div>
    <div class="card-body">
        <?php if (UserComponent::isBoss()) { ?>
            <div class="table-toolbar">
                <div class="btn-group">
                    <a href="<?php echo Url::to('/acreditaciones-industriales/create') ?>" class="btn btn-success">Nuevo <i class="fa fa-plus"></i></a>
                    <a id="eliminar" href="#" class="btn btn-danger">Eliminar seleccionados <i class="fa fa-trash"></i></a>
                </div>
            </div>
        <?php } ?>
        <table class="table table-striped table-bordered table-hover table-responsive" id="main_table">
            <thead>
                <tr>
                    <?php if (UserComponent::isBoss()) { ?>
                        <th class="table-checkbox">
                            <input id="select_all" type="checkbox" class="group-checkable" />
                        </th>
                    <?php } ?>
                    <th>Servicio</th>
                    <th>Tipo de Inspecci&oacute;n</th>
                    <?php if (UserComponent::isBoss()) { ?>
                        <th>
                            &nbsp;
                        </th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($data as $value) {
                ?>
                    <tr class="odd gradeX">
                        <?php if (UserComponent::isBoss()) { ?>
                            <td><input type="checkbox" class="checkboxes" value="<?php echo $value->id ?>" /></td>
                        <?php } ?>
                        <td><?php echo $value->titulo_inspector ?></td>
                        <td><?php echo $value->tipo ?></td>
                        <?php if (UserComponent::isBoss()) { ?>
                            <td>
                                <a href="<?php echo Url::to(['/acreditaciones-industriales/update', 'id' => $value->id]) ?>" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Editar</a>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php
DataTablesAsset::register($this);

$js = <<<JS
    $(document).ready(function() {
        var table = $('#main_table').DataTable( {            
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
            
        } );    
        table
            .buttons()
            .container()
            .appendTo( '#main_table_wrapper .col-md-6:eq(0)' );
    } );
JS;
$this->registerJs($js); ?>