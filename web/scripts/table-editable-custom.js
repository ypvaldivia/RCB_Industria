var TableEditable = function () {

    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                //jqTds[0].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[0] + '">';
                //jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<input id="mask_date" type="text" class="form-control form-control input-medium date-picker" data-date-format="yyyy-mm-dd" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input id="mask_date" type="text" class="form-control form-control input-medium date-picker" data-date-format="yyyy-mm-dd" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<a class="btn default btn-xs green edit" href="">Guardar</a>&nbsp; <a class="btn default btn-xs red cancel" href="">Cancelar</a>';
                //jqTds[5].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                //oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
                //oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs purple tooltips edit" title="Actualizar fechas de acreditaci&oacute;n"><i class="icon-edit"></i> Editar</a>', nRow, 4, false);
                //oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 5, false);
                oTable.fnDraw();
                saveData(jqInputs[0].value, jqInputs[1].value, jqInputs[2].value);
            }
            
            /*
             * Funcion que salva via ajax los cambios en la bd de la fecha de emision y vencimiento de las 
             * homologaciones de cada inspetor.
             * Samy
             */
            
            function saveData(id, emision, vence){
                $.ajax({
                    type: "POST",
                    url: "?r=acreditacionesInspectores/update&id="+id+"",
                    data: {"id":id, "emision": emision, "vence": vence},
                        success: function(json_format){
                            if(json_format === 'true'){
                                window.location.reload(true)
                                //alertSuccess();
                            }
                            if(json_format === 'false'){
                                //alertError();
                            }
                        }, 
                        error: function(){
                            alertErrorServer();
                        }
                });
            }
            
            /*
             * Funciones para mostrar las alerta cuando se edita un campo en la base de datos
             */
            
            function alertSuccess(){
                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "positionClass": "toast-top-full-width",
                                    "onclick": null,
                                    "showDuration": "1000",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                  };
                toastr.success("La operacion ha sido efectuada exitosamente.", "Exito!", toastr.options);
                $("#data tr").click(function() {
                    $(this).toggleClass("success");
                });
            }
            
            function alertError(){
                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "positionClass": "toast-top-full-width",
                                    "onclick": null,
                                    "showDuration": "1000",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                  };
                toastr.error("La operacion no se ha efectuado correctamente.", "Error!", toastr.options);
            }
            
            function alertErrorServer(){
                toastr.options = {
                                    "closeButton": true,
                                    "debug": false,
                                    "positionClass": "toast-top-full-width",
                                    "onclick": null,
                                    "showDuration": "1000",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                  };
                toastr.error("Ha ocurrido un error fatal en el servidor. La operacion no se ha efectuado correctamente.", "Error!", toastr.options);
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate('<a href="javascript:;" class="btn default btn-xs purple tooltips edit" title="Actualizar fechas de acreditaci&oacute;n"><i class="icon-edit"></i> Editar</a>', nRow, 4, false);
                oTable.fnDraw();
            }

            var oTable = $('#main_table').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "Todos"] // change per page values here
                ],
                // set the initial value
                "iDisplayLength": 5,

                "sPaginationType": "bootstrap",
                "oLanguage": {
                    "sSearch": "Buscar por: _INPUT_ ",
                    "sInfo": "Mostrando desde _START_ a _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "No hay registros para mostrar",
                    "sInfoFiltered": "(filtrado de un maximo de _MAX_ resultados)",
                    "sZeroRecords": "No hay registros para mostrar",
                    /*"sLengthMenu": "Mostrar _MENU_ registros por pagina",*/
                    "sLengthMenu": "_MENU_ registros"
                },
                "aoColumnDefs": [{
                    'bSortable': false,
                    'aTargets': [0,4]
                }
                ]
            });

            jQuery('#main_table_wrapper .dataTables_filter input').addClass("form-control input-medium"); // modify table search input
            jQuery('#main_table_wrapper .dataTables_length select').addClass("form-control input-small"); // modify table per page dropdown
            jQuery('#main_table_wrapper .dataTables_length select').select2({
            showSearchInput : false //hide search box with special css class
            }); // initialize select2 dropdown

            var nEditing = null;

            $('#main_table_new').click(function (e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData(['', '', '', '',
                    '<a class="edit" href="">Editar</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editRow(oTable, nRow);
                nEditing = nRow;
            });

            $('#main_table a.delete').live('click', function (e) {
                e.preventDefault();

                if (confirm("Are you sure to delete this row ?") == false) {
                    return;
                }

                var nRow = $(this).parents('tr')[0];
                oTable.fnDeleteRow(nRow);
                alert("Deleted! Do not forget to do some ajax to sync with backend :)");
            });

            $('#main_table a.cancel').live('click', function (e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $('#main_table a.edit').live('click', function (e) {
                e.preventDefault();

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Guardar") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                    nEditing = null;
                    //alert("Updated! Do not forget to do some ajax to sync with backend :)");
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
        }

    };

}();