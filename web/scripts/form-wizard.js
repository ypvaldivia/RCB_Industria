var FormWizard = function () {


    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().bootstrapWizard) {
                return;
            }

            function format(state) {
                if (!state.id) return state.text; // optgroup
                //return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
                return state.text;
            }

            $("#country_list").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });
            
            /*
             * Aki se llenan los comboxes con estilos metronic capturados por el id
             */
            
            $("#nivelAprobacion").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });
            
            $("#funcionarios").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });
            
            $("#tipoInspeccion").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });
            
            $("#acreditacion").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });
            
            $("#entidad").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });
            
            $("#oficina").select2({
                placeholder: "Select",
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });

            var form = $('#wizardInspecciones-form');
            var error = $('.alert-danger', form);
            var success = $('.alert-success', form);

            form.validate({
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: true, // do not focus the last invalid input
                rules: {
                    //Configurar inspeccion
                    'FormWizardInspecciones[tipo_inspeccion]': {
                        //minlength: 5,
                        required: true
                    },
                    'FormWizardInspecciones[funcionarios][]': {
                        //minlength: 5,
                        required: true
                    },
                    'FormWizardInspecciones[tipo_homologacion]': {
                        //minlength: 5,
                        required: true
                    },
                    'FormWizardInspecciones[servicios_personas_industrias_id]': {
                        //minlength: 5,
                        required: true
                    },
                    //Personas
                    'FormWizardInspecciones[personas]': {
                        required: true
                    },
                    //Metadatos de inspeccion
                    'FormWizardInspecciones[codigo]': {
                        required: true,
                        //email: true
                    },
                    'FormWizardInspecciones[numero_control]': {
                        required: true
                    },
                    'FormWizardInspecciones[numero_visita]': {
                        required: true
                    },
                    'FormWizardInspecciones[fecha]': {
                        required: true
                    },
                    'FormWizardInspecciones[entidad]': {
                        required: true
                    },
                    'FormWizardInspecciones[oficina]': {
                        required: true
                    },
                    'FormWizardInspecciones[objeto]': {
                        required: true
                    },
                    //Descripcion
                    'FormWizardInspecciones[descripcion]': {
                        //required: true
                    }, 
                    'FormWizardInspecciones[pdf]': {
                        //required: true
                    }
//                    card_number: {
//                        minlength: 16,
//                        maxlength: 16,
//                        required: true
//                    },
//                    card_cvc: {
//                        digits: true,
//                        required: true,
//                        minlength: 3,
//                        maxlength: 4
//                    },
//                    card_expiry_date: {
//                        required: true
//                    },
//                    'payment[]': {
//                        required: true,
//                        minlength: 1
//                    }
                },

                messages: { // custom messages for radio buttons and checkboxes
//                    'payment[]': {
//                        required: "Please select at least one option",
//                        minlength: jQuery.format("Please select at least one option")
//                    }, 
                    'FormWizardInspecciones[funcionarios][]': {
                        required: "Please select at least one option",
                        minlength: jQuery.format("Please select at least one option")
                    },
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                        error.insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit   
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        //.closest('select2-choices').addClass('has-error');
                    .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                    //$('.select2-choices').addClass('has-error');
                        //.closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                        label
                            .closest('.form-group').removeClass('has-error').addClass('has-success');
                        label.remove(); // remove error label here
                    }else { // display success icon for other inputs
                        label
                            .addClass('valid') // mark the current input as valid and display OK icon
                        .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                    form.submit();
                }

            });

            var displayConfirm = function() {
                $('#tab5 .form-control-static', form).each(function(){
                    var input = $('[name="'+$(this).attr("data-display")+'"]', form);
                    if (input.is(":text") || input.is("textarea")) {
                        $(this).html(input.val());
                    } else if (input.is("select")) {
                        $(this).html(input.find('option:selected').text());
                    } else if (input.is(":radio") && input.is(":checked")) {
                        $(this).html(input.attr("data-title"));
                    } else if ($(this).attr("data-display") === 'FormWizardInspecciones[funcionarios][]') {
                        var funcionarios = [];
                        $('[name="FormWizardInspecciones[funcionarios][]"]').each(function(){
                            funcionarios.push($(this).attr('data-title'));
                        });
                        $(this).html(funcionarios.join("<br>"));
                    }
                });
            }

            var handleTitle = function(tab, navigation, index) {
                var total = navigation.find('li').length;
                var current = index + 1;
                // set wizard title
                $('.step-title', $('#form_wizard_1')).text('Paso ' + (index + 1) + ' de ' + total);
                // set done steps
                jQuery('li', $('#form_wizard_1')).removeClass("done");
                var li_list = navigation.find('li');
                for (var i = 0; i < index; i++) {
                    jQuery(li_list[i]).addClass("done");
                }

                if (current == 1) {
                    $('#form_wizard_1').find('.button-previous').hide();
                } else {
                    $('#form_wizard_1').find('.button-previous').show();
                }

                if (current >= total) {
                    $('#form_wizard_1').find('.button-next').hide();
                    $('#form_wizard_1').find('.button-submit').show();
                    displayConfirm();
                } else {
                    $('#form_wizard_1').find('.button-next').show();
                    $('#form_wizard_1').find('.button-submit').hide();
                }
                App.scrollTo($('.page-title'));
            }

            // default form wizard
            $('#form_wizard_1').bootstrapWizard({
                'nextSelector': '.button-next',
                'previousSelector': '.button-previous',
                onTabClick: function (tab, navigation, index, clickedIndex) {
                    success.hide();
                    error.hide();
                    if (form.valid() == false) {
                        return false;
                    }
                    handleTitle(tab, navigation, clickedIndex);
                },
                onNext: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    if (form.valid() == false) {
                        return false;
                    }

                    handleTitle(tab, navigation, index);
                },
                onPrevious: function (tab, navigation, index) {
                    success.hide();
                    error.hide();

                    handleTitle(tab, navigation, index);
                },
                onTabShow: function (tab, navigation, index) {
                    var total = navigation.find('li').length;
                    var current = index + 1;
                    var $percent = (current / total) * 100;
                    $('#form_wizard_1').find('.progress-bar').css({
                        width: $percent + '%'
                    });
                }
            });

            $('#form_wizard_1').find('.button-previous').hide();
            $('#form_wizard_1 .button-submit').click(function () {
                //alert('Finished! Hope you like it :)');
            }).hide();
        }

    };

}();