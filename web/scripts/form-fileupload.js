var FormFileUpload = (function () {
  return {
    //main function to initiate the module
    init: function () {
      // Basic
      $(".dropify").dropify({
        messages: {
          default: "Haga click o arrastre la imagen hasta aquí",
          replace: "Reemplazar",
          remove: "Eliminar",
          error: "Error",
        },
      });

      // Used events
      var drEvent = $("#input-file-events").dropify();

      drEvent.on("dropify.beforeClear", function (event, element) {
        return confirm(
          'Do you really want to delete "' + element.file.name + '" ?'
        );
      });

      drEvent.on("dropify.afterClear", function (event, element) {
        alert("File deleted");
      });

      drEvent.on("dropify.errors", function (event, element) {
        console.log("Has Errors");
      });

      var drDestroy = $("#input-file-to-destroy").dropify();
      drDestroy = drDestroy.data("dropify");
      $("#toggleDropify").on("click", function (e) {
        e.preventDefault();
        if (drDestroy.isDropified()) {
          drDestroy.destroy();
        } else {
          drDestroy.init();
        }
      });
    },
  };
})();
