/**
Custom module for you to write your own javascript functions
**/
var Custom = function () {

    // private functions & variables

    var myFunc = function() {

    }

    // public functions
    return {

        //main function
        init: function () {
            //initialize here something.
            console.log("Ejecutando custom.js");
        },

        addModalPersona: function(url){
            console.info("Ejecutando addModalPersona function :-)");
            $('#btn-save').click(function () {
                var nombre_appellidos = $('#PersonasAcreditadas_nombre_apellidos').val();
                var telefono = $('#PersonasAcreditadas_telefono_contacto').val();
                var correo = $('#PersonasAcreditadas_correo').val();
                var provincia = $('#PersonasAcreditadas_provincia0 option:selected').val();
                var entidad = $('#PersonasAcreditadas_entidad0 option:selected').val();
                var ci = $('#PersonasAcreditadas_ci').val();
                var persona = {
                    nombre_apellidos: nombre_appellidos,
                    telefono: telefono,
                    correo: correo,
                    provincia: provincia,
                    entidad: entidad,
                    ci: ci
                };
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "JSON",
                    data: {formData: persona},
                    success: function (json_format) {
                        $('#full').modal('hide'); //ocultar el modal
                        alert("Fue adicionada la persona: "+json_format.msg);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Error: "+ errorThrown);

                    }
                });
                //$('input').val(''); // vaciar los campos del formulario
                //$("#personas_homologar").load(location.href + " #personas_homologar>*", ""); // recargar el select2 con la nueva persona
                //$("#personas_desaprobadas").load(location.href + " #personas_desaprobadas>*", "");

            });
        },

        addModalServicio: function(url){
            $('#btn-save').click(function () {
                var departamento = $('#ServiciosLaboratoriosAcreditados_departamento').val();
                var servicio = $('#ServiciosLaboratoriosAcreditados_servicio').val();
                var entidad = $('#ServiciosLaboratoriosAcreditados_entidad0 option:selected').val();
                var provincia = $('#ServiciosLaboratoriosAcreditados_provincia0 option:selected').val();
                var servicio = {
                    departamento: departamento,
                    servicio: servicio,
                    entidad: entidad,
                    provincia: provincia,
                };
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "JSON",
                    data: {formData: servicio},
                    success: function (json_format) {
                        $('#fullServ').modal('hide'); //ocultar el modal
                        alert("Fue adicionado el servicio: "+json_format.msg);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR.responseText);

                    }
                });
                //$("#productos_homologar").load(location.href + " #productos_homologar>*", "");
                //$("#productos_desaprobados").load(location.href + " #productos_desaprobados>*", "");

            });
        },

        addModalProducto: function(url){
            $('#btn-save').click(function () {
                var nombre = $('#ProductosAcreditados_nombre').val();
                var propietario = $('#ProductosAcreditados_propietario0 option:selected').val();
                var provincia = $('#ProductosAcreditados_provincia0 option:selected').val();
                var producto = {
                    nombre: nombre,
                    propietario: propietario,
                    provincia: provincia,
                };
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "JSON",
                    data: {formData: producto},
                    success: function (json_format) {
                        $('#fullProducto').modal('hide'); //ocultar el modal
                        alert("Fue adicionado el producto: "+json_format.msg);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR.responseText);

                    }
                });
                //$("#productos_homologar").load(location.href + " #productos_homologar>*", "");
                //$("#productos_desaprobados").load(location.href + " #productos_desaprobados>*", "");

            });
        },

        addModalDac: function(url){
            $('#btn-save').click(function () {
                var nombre = $('#DacsAcreditados_nombre').val();
                var propietario = $('#DacsAcreditados_propietario0 option:selected').val();
                var provincia = $('#DacsAcreditados_provincia0 option:selected').val();
                var tipo = $('#DacsAcreditados_tipo0 option:selected').val();
                var ct_nominal = $('#DacsAcreditados_ct_nominal').val()
                var dac = {
                    nombre: nombre,
                    propietario: propietario,
                    provincia: provincia,
                    tipo: tipo,
                    ct_nominal: ct_nominal,
                };
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "JSON",
                    data: {formData: dac},
                    success: function (json_format) {
                        $('#fullDac').modal('hide'); //ocultar el modal
                        alert("Fue adicionado el Dispositivo: "+json_format.msg);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR.responseText);

                    }
                });
                //$('input').val(''); // vaciar los campos del formulario
                //$("#dacs_homologar").load(location.href + " #dacs_homologar>*", "");
                //$("#dacs_desaprobados").load(location.href + " #dacs_desaprobados>*", "");

            });
        },

        addModalMIT: function(url){
            $('#btn-save').click(function () {
                var nombre = $('#MediosIzadoAcreditados_nombre').val();
                var propietario = $('#MediosIzadoAcreditados_propietario0 option:selected').val();
                var provincia = $('#MediosIzadoAcreditados_provincia0 option:selected').val();
                var tipo = $('#MediosIzadoAcreditados_tipo0 option:selected').val();
                var categoria = $('#MediosIzadoAcreditados_categoria0 option:selected').val();
                var ci_nominal = $('#MediosIzadoAcreditados_ci_nominal').val()
                var ci_aux_nominal = $('#MediosIzadoAcreditados_ci_aux_nominal').val()
                var ci_aux1_nominal = $('#MediosIzadoAcreditados_ci_aux1_nominal').val()
                var dac = {
                    nombre: nombre,
                    propietario: propietario,
                    provincia: provincia,
                    tipo: tipo,
                    categoria: categoria,
                    ci_nominal: ci_nominal,
                    ci_aux_nominal: ci_aux_nominal,
                    ci_aux1_nominal: ci_aux1_nominal,
                };
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "JSON",
                    data: {formData: dac},
                    success: function (json_format) {
                        $('#fullMIT').modal('hide'); //ocultar el modal
                        alert("Fue adicionado el Medio de Izado: "+json_format.msg);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR.responseText);

                    }
                });
                //$('input').val(''); // vaciar los campos del formulario
                //$("#mits_homologar").load(location.href + " #mits_homologar>*", "");
                //$("#mits_desaprobados").load(location.href + " #mits_desaprobados>*", "");

            });
        },

        validateModalPersona:function() {
            console.info("Ejecutando validateModalPersona function :-)");

            var form1 = $('#personas-acreditadas-form-modal');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {

                    'PersonasAcreditadas[nombre_apellidos]': {
                        //minlength: 5,
                        required: true
                    },
                    'FormWizardInspecciones[funcionarios][]': {
                        //minlength: 5,
                        required: true
                    },
                    'FormWizardInspecciones[tipoInspeccion]': {
                        //minlength: 5,
                        required: true
                    },
                    'FormWizardInspecciones[acreditacion]': {
                        //minlength: 5,
                        required: true
                    },
                    //Personas
                    'FormWizardInspecciones[personas]': {
                        required: true
                    },
                    //Metadatos de inspeccion
                    'FormWizardInspecciones[codigo]': {
                        required: true,
                        //email: true
                    },
                    'FormWizardInspecciones[noControl]': {
                        required: true
                    },
                    'FormWizardInspecciones[noVisita]': {
                        required: true
                    },
                    'FormWizardInspecciones[fecha]': {
                        required: true
                    },
                    'FormWizardInspecciones[entidad]': {
                        required: true
                    },
                    'FormWizardInspecciones[oficina]': {
                        required: true
                    },
                    'FormWizardInspecciones[objeto]': {
                        required: true
                    },
                    //Detalles/pdf
                    'FormWizardInspecciones[descripcion]': {
                        //required: true
                    },
                    'FormWizardInspecciones[pdf]': {
                        //required: true
                    },
                    'Entidades[correo]': {
                        required: true,
                        email: true
                    }
                },
//                rules: {
//                    name: {
//                        minlength: 2,
//                        required: true
//                    },
//                    email: {
//                        required: true,
//                        email: true
//                    },
//                    url: {
//                        required: true,
//                        url: true
//                    },
//                    number: {
//                        required: true,
//                        number: true
//                    },
//                    digits: {
//                        required: true,
//                        digits: true
//                    },
//                    creditcard: {
//                        required: true,
//                        creditcard: true
//                    },
//                    occupation: {
//                        minlength: 5,
//                    },
//                    category: {
//                        required: true
//                    }
//                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                    //$('.select2-choices').addClass('has-error');
                },

                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    success1.show();
                    error1.hide();
                }
            });

        },


        //some helper function
        doSomeStuff: function () {
            myFunc();
        },

        InitPrint: function(){
            $(".btnPrint").printPage({
                message:"Espere, estamos enviando la vista a la impresora."
            });
        }

    };

}();

/***
Usage
***/
//Custom.init();
//Custom.addModalPersona();
