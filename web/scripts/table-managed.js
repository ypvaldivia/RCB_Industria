$(document).ready(function () {
  $("#main_table").DataTable({
    dom: "Bfrtip",
    buttons: ["excel", "pdf", "print"],
    language: esES,
  });
});
