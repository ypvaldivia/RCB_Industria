/**
 * Created by tesa on 8/13/15.
 */
var FormValidation = function () {

    var handleValidation = function() {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var form1 = $('#entidades-form');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                //Crud Entidades
                'Entidades[nombre]': {
                    required: true,
                    messages: "Por favor inserte el Nombre."
                },
                'Entidades[organismo0]': {
                    required: true,
                },
                'Entidades[telefono_contacto]': {
                    number: true
                },
                'Entidades[correo]': {
                    email: true
                },
                'Entidades[provincia0]': {
                    required: true,
                },
                'Entidades[oficinaAcreditada]': {
                    required: true,
                }
            },

            messages: { // custom messages for radio buttons and checkboxes
                'Entidades[nombre]': {
                    //required: "Por favor inserte el Nombre.",
                },

                membership: {
                    required: "Please select a Membership type"
                },
                service: {
                    required: "Please select  at least 2 types of Service",
                    minlength: jQuery.format("Please select  at least {0} types of Service")
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
                //$('.select2-choices').addClass('has-error');
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                success1.show();
                error1.hide();
            }
        });

    }

    return {
        //main function to initiate the module
        init: function () {

            handleValidation();

        }

    };

}();