/**
 * Created by tesa on 10/11/15.
 */

/**
 Core script to handle the entire theme and core functions
 **/
var appProducto = function () {

    var productoMI = function(){
        $('#MediosIzadoAcreditados_es_producto').change(function(){
            if($('#MediosIzadoAcreditados_es_producto').attr('checked')){
                $('#medios_izado').attr('hidden', '');
            }
            else{
                $('#medios_izado').removeAttr('hidden');
            }
        });
    }

    return {
        producto: function(){
            productoMI();
        }
    };

}();

